<?xml version="1.0"?>
<TYPE Type="UnsignedInt" UI="Edit" Revision="$Revision: 1.1 $" State="modified">
  <DEFINITION Label="None" Value="0"/>
  <DEFINITION Label="BasicDamageShield" Value="0"/>
  <DEFINITION Label="GooDamageShield" Value="0"/>
  <DEFINITION Label="WaterWall" Value="1"/>
  <DEFINITION Label="FireWall" Value="2"/>
  <DEFINITION Label="ThornWall" Value="3"/>
  <DEFINITION Label="LightningWall" Value="4"/>
  <LOG>Tue Mar 29 14:08:57 2005 (vizerie) Type Predef = 
       Wed Dez 18 14:58:00 2024 (riasan) add Label BasicDamageShield Value 0
       Wed Dez 18 14:58:00 2024 (riasan) add Label GooDamageShield Value 0</LOG>
</TYPE>
