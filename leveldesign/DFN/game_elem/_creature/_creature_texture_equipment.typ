<?xml version="1.0"?>
<TYPE Type="SignedInt" UI="NonEditableCombo" Default="Default" Min="-1" Max="4" Version="0.7" State="modified">
  <DEFINITION Label="Season" Value="-1"/>
  <DEFINITION Label="Default" Value="0"/>
  <DEFINITION Label="Lacustre/Low Quality/Young" Value="0"/>
  <DEFINITION Label="Desert/Medium Quality/Normal" Value="1"/>
  <DEFINITION Label="Jungle/High Quality/Old" Value="2"/>
  <DEFINITION Label="PrimR" Value="3"/>
  <DEFINITION Label="goo" Value="4"/>
  <DEFINITION Label="Storyline" Value="5"/>
  <DEFINITION Label="Chromatic" Value="6"/>
  <DEFINITION Label="Divine" Value="7"/>
  <LOG></LOG>
</TYPE>
