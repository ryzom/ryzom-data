scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[The Botoga's Drop (Desert 01)]],  
      Season = [[Summer]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8656174,  
      Name = [[La positive attitude]],  
      InstanceId = [[Client1_123]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[cette potion te donnera la peche toute la journée !]]
    },  
    {
      SheetId = 8638510,  
      Name = [[Plot item]],  
      InstanceId = [[Client1_135]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8634926,  
      Name = [[la positive attitude]],  
      InstanceId = [[Client1_136]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    }
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 4,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    EasterEgg = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LootSpawner = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    BossSpawner = 0,  
    TextManagerEntry = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    DefaultFeature = 0,  
    ChatSequence = 0,  
    WayPoint = 0,  
    Region = 0,  
    PlotItem = 0,  
    Road = 0,  
    ZoneTrigger = 1,  
    NpcCreature = 0,  
    ActivityStep = 1,  
    RegionVertex = 0,  
    ActionStep = 1,  
    EventType = 0,  
    Position = 0,  
    Location = 1,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    Npc = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Name = [[Permanent]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_29]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_27]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[building under construction 1]],  
              Position = {
                y = -1282.83374,  
                x = 21539.51953,  
                InstanceId = [[Client1_30]],  
                Class = [[Position]],  
                z = 74.99633789
              },  
              Angle = -2.5,  
              Base = [[palette.entities.botobjects.construction_site]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21513.8457,  
                    y = -1284.937256,  
                    z = 75.28242493,  
                    InstanceId = [[Client1_181]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_180]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21508.69531,  
                    y = -1289.896729,  
                    z = 76.36395264,  
                    InstanceId = [[Client1_184]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_183]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21507.24023,  
                    y = -1293.847656,  
                    z = 77.56345367,  
                    InstanceId = [[Client1_187]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_186]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21510.34961,  
                    y = -1298.824951,  
                    z = 79.02024078,  
                    InstanceId = [[Client1_190]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_189]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21529.21484,  
                    y = -1304.735107,  
                    z = 76.79948425,  
                    InstanceId = [[Client1_193]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_192]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21534.59961,  
                    y = -1293.025757,  
                    z = 74.77945709,  
                    InstanceId = [[Client1_196]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_195]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21521.27344,  
                    y = -1280.383179,  
                    z = 75.01699829,  
                    InstanceId = [[Client1_199]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_198]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21518.4043,  
                    y = -1281.800659,  
                    z = 75.0243454,  
                    InstanceId = [[Client1_202]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_201]]
                }
              },  
              Position = {
                Class = [[Position]],  
                x = 0,  
                y = 0,  
                z = 0,  
                InstanceId = [[Client1_177]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_178]]
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 210,  
      LocationId = [[Client1_14]],  
      Name = [[Act 1:Act 1]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_50]],  
              ActivitiesId = {
              },  
              Tattoo = 27,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 13,  
              HandsModel = 5605678,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 6,  
              HairColor = 1,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 12,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              HairType = 3118,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Position = {
                y = -1297.133301,  
                x = 21502.33008,  
                z = 78.55791473,  
                Class = [[Position]],  
                InstanceId = [[Client1_51]]
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              JacketModel = 0,  
              InheritPos = 1,  
              ArmModel = 0,  
              Name = [[Xarius]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              MorphTarget7 = 7,  
              MorphTarget3 = 5,  
              Angle = 3.212304831
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_96]],  
              ActivitiesId = {
              },  
              Tattoo = 23,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 12,  
              HandsModel = 5605678,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 13,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 5,  
              HandsColor = 1,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_173]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_174]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_175]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_176]],  
                        Entity = r2.RefId([[Client1_168]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                InstanceId = [[Client1_94]]
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 5605422,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Angle = -2.631920099,  
              MorphTarget3 = 4,  
              MorphTarget7 = 3,  
              Sex = 0,  
              JacketModel = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmModel = 0,  
              Name = [[Michael Knight]],  
              Position = {
                y = -1288.07251,  
                x = 21519.29102,  
                z = 75.39472198,  
                Class = [[Position]],  
                InstanceId = [[Client1_97]]
              },  
              WeaponLeftHand = 0,  
              ArmColor = 3,  
              SheetClient = [[basic_fyros_male.creature]],  
              HairType = 2606
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_100]],  
              ActivitiesId = {
              },  
              Tattoo = 7,  
              TrouserColor = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 3,  
              GabaritBreastSize = 14,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_115]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_116]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                InstanceId = [[Client1_98]]
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Angle = -2.631920099,  
              MorphTarget3 = 7,  
              MorphTarget7 = 0,  
              Sex = 1,  
              JacketModel = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmModel = 0,  
              Name = [[Lorrie]],  
              Position = {
                y = -1291.06958,  
                x = 21515.92969,  
                z = 76.13254547,  
                Class = [[Position]],  
                InstanceId = [[Client1_101]]
              },  
              WeaponLeftHand = 0,  
              ArmColor = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              HairType = 5623342
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_104]],  
              ActivitiesId = {
              },  
              Tattoo = 0,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_102]]
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 5653038,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Angle = -2.631920099,  
              MorphTarget3 = 7,  
              MorphTarget7 = 6,  
              Sex = 1,  
              JacketModel = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmModel = 0,  
              Name = [[Brenda Walch]],  
              Position = {
                y = -1291.792969,  
                x = 21520.58398,  
                z = 75.7554245,  
                Class = [[Position]],  
                InstanceId = [[Client1_105]]
              },  
              WeaponLeftHand = 0,  
              ArmColor = 2,  
              SheetClient = [[basic_matis_female.creature]],  
              HairType = 5166
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_108]],  
              ActivitiesId = {
              },  
              Tattoo = 20,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 8,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 7,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 13,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_203]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_205]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_178]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_106]]
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 5617710,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Angle = -2.631920099,  
              MorphTarget3 = 4,  
              MorphTarget7 = 2,  
              Sex = 1,  
              JacketModel = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmModel = 0,  
              Name = [[Steven Seagall]],  
              Position = {
                y = -1285.825195,  
                x = 21518.72461,  
                z = 75.24337006,  
                Class = [[Position]],  
                InstanceId = [[Client1_109]]
              },  
              WeaponLeftHand = 0,  
              ArmColor = 1,  
              SheetClient = [[basic_zorai_male.creature]],  
              HairType = 8238
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_112]],  
              ActivitiesId = {
              },  
              Tattoo = 14,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 8,  
              HandsModel = 5609774,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_110]]
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5653038,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Angle = -2.631920099,  
              MorphTarget3 = 3,  
              MorphTarget7 = 0,  
              Sex = 0,  
              JacketModel = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmModel = 0,  
              Name = [[Chuck Norris]],  
              Position = {
                y = -1290.37561,  
                x = 21519.69531,  
                z = 75.65680695,  
                Class = [[Position]],  
                InstanceId = [[Client1_113]]
              },  
              WeaponLeftHand = 0,  
              ArmColor = 0,  
              SheetClient = [[basic_matis_male.creature]],  
              HairType = 5422
            },  
            {
              InstanceId = [[Client1_139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_137]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              Name = [[Unleash the mitch !]],  
              Position = {
                y = -1285.130737,  
                x = 21525.12891,  
                z = 75.01805878,  
                Class = [[Position]],  
                InstanceId = [[Client1_140]]
              },  
              Angle = -2.698976278,  
              Base = [[palette.entities.creatures.cccdb7]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          InstanceId = [[Client1_31]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_59]],  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_60]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_61]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_62]],  
                    Entity = r2.RefId([[Client1_54]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_32]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone trigger 1]],  
          Position = {
            y = -1296.743896,  
            x = 21499.42578,  
            z = 78.23918152,  
            Class = [[Position]],  
            InstanceId = [[Client1_33]]
          },  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Places 1]],  
              Position = {
                y = -0.08911132813,  
                x = -0.501953125,  
                z = -0.337020874,  
                Class = [[Position]],  
                InstanceId = [[Client1_34]]
              },  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  Position = {
                    y = -6.318115234,  
                    x = 10.96484375,  
                    z = 2.014900208,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_38]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_37]]
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  Position = {
                    y = 9.609375,  
                    x = 0.146484375,  
                    z = -2.26550293,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_41]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_40]]
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  Position = {
                    y = 2.154174805,  
                    x = -8.037109375,  
                    z = -2.019935608,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_44]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_43]]
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  Position = {
                    y = -12.35095215,  
                    x = 1.490234375,  
                    z = 3.553787231,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_47]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_46]]
                }
              },  
              InstanceId = [[Client1_35]],  
              Deletable = 0
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_35]]
        },  
        {
          Type = [[None]],  
          Active = 0,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_52]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -1298.227295,  
            x = 21503.40234,  
            z = 79,  
            Class = [[Position]],  
            InstanceId = [[Client1_53]]
          },  
          Repeating = 0,  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_55]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_50]]),  
                  InstanceId = [[Client1_56]],  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_57]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          InstanceId = [[Client1_54]]
        },  
        {
          InstanceId = [[Client1_117]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_118]]
          },  
          Class = [[LootSpawner]],  
          Npc5Id = r2.RefId([[]]),  
          Npc1Id = r2.RefId([[Client1_100]]),  
          Npc3Id = r2.RefId([[]]),  
          Npc4Id = r2.RefId([[]]),  
          Base = [[palette.entities.botobjects.user_event]],  
          TriggerValue = 0,  
          _Seed = 1146751960,  
          InheritPos = 1,  
          EasterEggId = [[Client1_120]],  
          Name = [[Loot spawner 1]],  
          Position = {
            y = -1293.4646,  
            x = 21515.01758,  
            z = 76.74771118,  
            Class = [[Position]],  
            InstanceId = [[Client1_119]]
          },  
          Ghosts = {
          },  
          Active = 1,  
          Components = {
            {
              Item2Id = r2.RefId([[]]),  
              Active = 0,  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_121]]
              },  
              Class = [[EasterEgg]],  
              ItemQty = 1,  
              Item1Qty = 1,  
              Item3Qty = 1,  
              Base = [[palette.entities.botobjects.chest_wisdom_std_sel]],  
              Item2Qty = 1,  
              Item1Id = r2.RefId([[Client1_136]]),  
              InheritPos = 0,  
              Name = [[Item Chest 1]],  
              Position = {
                y = -1291.202148,  
                x = 21515.73242,  
                z = 76.17037201,  
                Class = [[Position]],  
                InstanceId = [[Client1_122]]
              },  
              _Seed = 1146751960,  
              InstanceId = [[Client1_120]],  
              Components = {
              },  
              Item3Id = r2.RefId([[]])
            }
          },  
          Npc2Id = r2.RefId([[]])
        },  
        {
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_142]]
          },  
          Class = [[BossSpawner]],  
          Guard5Id = r2.RefId([[]]),  
          Base = [[palette.entities.botobjects.user_event]],  
          TriggerValue = 0,  
          Guard2Id = r2.RefId([[]]),  
          InstanceId = [[Client1_141]],  
          Components = {
          },  
          Ghosts = {
          },  
          InheritPos = 1,  
          Guard1Id = r2.RefId([[Client1_96]]),  
          Name = [[Boss Spawner 1]],  
          Position = {
            y = -1288.442505,  
            x = 21526.27539,  
            z = 75,  
            Class = [[Position]],  
            InstanceId = [[Client1_143]]
          },  
          Guard3Id = r2.RefId([[]]),  
          BossId = r2.RefId([[Client1_139]]),  
          Guard4Id = r2.RefId([[]]),  
          _Seed = 1146752113
        },  
        {
          Type = [[None]],  
          Active = 0,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_166]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 2]],  
          Position = {
            y = -1289.197266,  
            x = 21524.73633,  
            z = 75,  
            Class = [[Position]],  
            InstanceId = [[Client1_167]]
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          InstanceId = [[Client1_168]],  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_169]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_108]]),  
                  InstanceId = [[Client1_170]],  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_171]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 0
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_11]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 1,  
        InstanceId = [[Client1_57]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bonjour ami ! derriere moi se trouvent plusieurs personnes, tu devras toutes les tuer afin de faire aparaitre un uber cadeau kipuxe ! Cependant ne te trompe pas et tue les bonnes personnes !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_171]],  
        Class = [[TextManagerEntry]],  
        Text = [[Unleash the Mitch !!!!!! game over !]]
      }
    }
  }
}