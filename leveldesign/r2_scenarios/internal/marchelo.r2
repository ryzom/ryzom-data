scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts22]],  
      Time = 0,  
      Name = [[camp]],  
      Season = [[fall]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    },  
    {
      InstanceId = [[Client1_1241]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts20]],  
      Time = 0,  
      Name = [[La passe de la mort]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    },  
    {
      InstanceId = [[Client1_2619]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts22]],  
      Time = 0,  
      Name = [[Sands of Solitude (Desert 22)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    },  
    {
      InstanceId = [[Client1_2780]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Primes14]],  
      Time = 0,  
      Name = [[grotte de l'oublie]],  
      Season = [[winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    },  
    {
      InstanceId = [[Client1_3947]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Forest12]],  
      Time = 0,  
      Name = [[la plaine du sage]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    },  
    {
      InstanceId = [[Client1_4061]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes03]],  
      Time = 0,  
      Name = [[(Sub Tropics 03)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthWestEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_3]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
      {
        Class = [[LogicEntityAction]],  
        InstanceId = [[Client1_882]],  
        Conditions = {
        },  
        Actions = {
        },  
        Event = {
          Class = [[EventType]],  
          InstanceId = [[Client1_883]],  
          Type = [[On Scenario Started]],  
          Value = r2.RefId([[]])
        },  
        Name = [[]]
      }
    },  
    Reactions = {
    }
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8657198,  
      Name = [[Pierre de liaison de la terre]],  
      InstanceId = [[Client1_1190]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8665390,  
      Name = [[Pierre de liaison de l'air]],  
      InstanceId = [[Client1_2582]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8632622,  
      Name = [[Pierre de liaison de l'eau]],  
      InstanceId = [[Client1_2583]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8662062,  
      Name = [[ampli kami]],  
      InstanceId = [[Client1_4019]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    }
  },  
  Name = [[pretre]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 2,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    ShortDescription = [[Un matis/fyros marche vers votre groupe, l'air mal en point .
un groupe de de partisant Kami/Karavan vient de les attaquer pour leur d�rob� un artefact sacr�e, qu'ils devaient remettre a un pretre pour sauv� un membre de leur guilde]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    GiveItem = 0,  
    EventType = 0,  
    ChatStep = 0,  
    LootSpawner = 0,  
    TextManagerEntry = 0,  
    PlotItem = 0,  
    NpcGrpFeature = 0,  
    ActionStep = 0,  
    Location = 0,  
    LogicEntityBehavior = 0,  
    Act = 4,  
    ChatSequence = 0,  
    ChatAction = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    DefaultFeature = 0,  
    RequestItem = 0,  
    Counter = 0,  
    Behavior = 0,  
    Region = 0,  
    ActivityStep = 1,  
    EasterEgg = 0,  
    Npc = 0,  
    ZoneTrigger = 0,  
    RegionVertex = 0,  
    Road = 0,  
    NpcCreature = 0,  
    Position = 0,  
    Timer = 0,  
    ActionType = 0,  
    NpcCustom = 0,  
    WayPoint = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_6]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      Version = 3,  
      Events = {
      },  
      Title = [[]],  
      WeatherValue = 0,  
      LocationId = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_57]],  
              Base = [[palette.entities.botobjects.bones_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_58]],  
                x = 26945.76563,  
                y = -1956.125,  
                z = -7.546875
              },  
              Angle = -1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_55]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_61]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_62]],  
                x = 26954.45313,  
                y = -1978.71875,  
                z = -7.796875
              },  
              Angle = 2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_59]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_65]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_66]],  
                x = 26949.73438,  
                y = -1980,  
                z = -7.328125
              },  
              Angle = 1.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_63]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_73]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_74]],  
                x = 26930.625,  
                y = -1980.078125,  
                z = -6.9375
              },  
              Angle = 0.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_71]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_81]],  
              Base = [[palette.entities.botobjects.counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_82]],  
                x = 26950.26563,  
                y = -1977.671875,  
                z = -7.515625
              },  
              Angle = 2.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_79]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[counter 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_85]],  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_86]],  
                x = 26939.125,  
                y = -1981.9375,  
                z = -6.984375
              },  
              Angle = 0.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_83]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_89]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_90]],  
                x = 26939.9375,  
                y = -1993.640625,  
                z = -7.90625
              },  
              Angle = 2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_87]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya II 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_93]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_94]],  
                x = 26978.51563,  
                y = -2003.890625,  
                z = -4.78125
              },  
              Angle = 2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_91]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_105]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_growth_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_106]],  
                x = 26965.20313,  
                y = -1964.03125,  
                z = -8.015625
              },  
              Angle = -2.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_103]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya growth II 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_109]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_110]],  
                x = 26959.57813,  
                y = -1955.171875,  
                z = -6.875
              },  
              Angle = -2.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_107]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_113]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_114]],  
                x = 26924.92188,  
                y = -1956.21875,  
                z = -5.28125
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_111]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga II 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_117]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_118]],  
                x = 26925.39063,  
                y = -2016.765625,  
                z = -2.734375
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_115]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga III 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_153]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_154]],  
                x = 26956.76563,  
                y = -1971.90625,  
                z = -8.5625
              },  
              Angle = 2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[barrier 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_157]],  
              Base = [[palette.entities.botobjects.barrier_T]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_158]],  
                x = 26949.65625,  
                y = -1969.75,  
                z = -7.796875
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_155]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[barrier(T) 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_161]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_162]],  
                x = 26955.4375,  
                y = -1969.578125,  
                z = -8.375
              },  
              Angle = -4.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_159]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[barrier 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_165]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_166]],  
                x = 26952.73438,  
                y = -1971.09375,  
                z = -8.109375
              },  
              Angle = -3.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_163]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[barrier 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_169]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_170]],  
                x = 26950.46875,  
                y = -1974.125,  
                z = -7.765625
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[barrier 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_173]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_174]],  
                x = 26948.21875,  
                y = -1989.484375,  
                z = -7.515625
              },  
              Angle = 2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_177]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_178]],  
                x = 26928.03125,  
                y = -1972.25,  
                z = -6.40625
              },  
              Angle = -0.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_175]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_207]],  
              Base = [[palette.entities.botobjects.pack_2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_208]],  
                x = 26950.25,  
                y = -1983.796875,  
                z = -7.296875
              },  
              Angle = 2.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_205]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[pack 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_211]],  
              Base = [[palette.entities.botobjects.pack_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_212]],  
                x = 26926.6875,  
                y = -1977.3125,  
                z = -6.765625
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_209]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[pack 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_215]],  
              Base = [[palette.entities.botobjects.bag_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_216]],  
                x = 26944.96875,  
                y = -1988.0625,  
                z = -7.34375
              },  
              Angle = 2.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_213]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bag 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_219]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_220]],  
                x = 26939.98438,  
                y = -1976.328125,  
                z = -7.046875
              },  
              Angle = -1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_217]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_281]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_280]],  
                x = 4.265625,  
                y = 26.828125,  
                z = -3.09375
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_283]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_284]],  
                    x = 26941.98438,  
                    y = -1939.21875,  
                    z = -4.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_286]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_287]],  
                    x = 26947.45313,  
                    y = -1941.15625,  
                    z = -5.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_289]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_290]],  
                    x = 26950.82813,  
                    y = -1943.703125,  
                    z = -7.375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_292]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_293]],  
                    x = 26950.3125,  
                    y = -1947.25,  
                    z = -7.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_295]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_296]],  
                    x = 26951.01563,  
                    y = -1956.78125,  
                    z = -5.25
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_448]],  
              Name = [[Route 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_447]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_450]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_451]],  
                    x = 26943.10938,  
                    y = -1966.921875,  
                    z = -7.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_453]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_454]],  
                    x = 26948.28125,  
                    y = -1960.921875,  
                    z = -7.71875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_456]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_457]],  
                    x = 26951.01563,  
                    y = -1953.515625,  
                    z = -7.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_459]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_460]],  
                    x = 26956.92188,  
                    y = -1937.109375,  
                    z = -8.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_465]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_466]],  
                    x = 26957.09375,  
                    y = -1930.640625,  
                    z = -7.15625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_468]],  
              Name = [[Route 3]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_467]],  
                x = -0.875,  
                y = -0.1875,  
                z = -0.15625
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_470]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_471]],  
                    x = 26934.39063,  
                    y = -1970.5,  
                    z = -6.375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_473]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_474]],  
                    x = 26940.71875,  
                    y = -1958.46875,  
                    z = -7.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_476]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_477]],  
                    x = 26943.70313,  
                    y = -1944.28125,  
                    z = -7.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_482]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_483]],  
                    x = 26953.78125,  
                    y = -1929.53125,  
                    z = -8.8125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_499]],  
              Name = [[Route 4]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_498]],  
                x = -0.03125,  
                y = -0.0625,  
                z = -0.015625
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_501]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_502]],  
                    x = 26936.04688,  
                    y = -1972.640625,  
                    z = -7.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_504]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_505]],  
                    x = 26937.17188,  
                    y = -1960.171875,  
                    z = -6.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_507]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_508]],  
                    x = 26945.79688,  
                    y = -1944.515625,  
                    z = -8.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_510]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_511]],  
                    x = 26952.90625,  
                    y = -1932.21875,  
                    z = -8.875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_513]],  
              Name = [[Route 5]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_512]],  
                x = -0.09375,  
                y = -0.09375,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_515]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_516]],  
                    x = 26942.54688,  
                    y = -1980.84375,  
                    z = -6.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_521]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_522]],  
                    x = 26942.375,  
                    y = -1970.6875,  
                    z = -6.953125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_524]],  
              Name = [[Route 6]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_523]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_526]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_527]],  
                    x = 26944.60938,  
                    y = -1981.109375,  
                    z = -7
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_529]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_530]],  
                    x = 26943.53125,  
                    y = -1969.28125,  
                    z = -6.953125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_535]],  
              Name = [[Route 7]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_534]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_537]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_538]],  
                    x = 26941.39063,  
                    y = -1982.75,  
                    z = -6.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_540]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_541]],  
                    x = 26937.42188,  
                    y = -1976.828125,  
                    z = -6.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_546]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_547]],  
                    x = 26941.07813,  
                    y = -1969.796875,  
                    z = -7.046875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_549]],  
              Name = [[Route 8]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_548]],  
                x = 6.71875,  
                y = -3.09375,  
                z = -0.109375
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_551]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_552]],  
                    x = 26934.40625,  
                    y = -1964.09375,  
                    z = -7.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_554]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_555]],  
                    x = 26935.79688,  
                    y = -1959.03125,  
                    z = -6.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_557]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_558]],  
                    x = 26935.4375,  
                    y = -1950.65625,  
                    z = -6.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_560]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_561]],  
                    x = 26948.46875,  
                    y = -1929.921875,  
                    z = -9.15625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1246]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1247]],  
                x = 25186.32813,  
                y = -2137.90625,  
                z = -11.90625
              },  
              Angle = 1.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1244]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1250]],  
              Base = [[palette.entities.botobjects.bones_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1251]],  
                x = 25164.54688,  
                y = -2100.734375,  
                z = -18.078125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1248]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1254]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1255]],  
                x = 25151.82813,  
                y = -2107.1875,  
                z = -17.828125
              },  
              Angle = -0.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1252]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya I 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1258]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1259]],  
                x = 25191.3125,  
                y = -2082.96875,  
                z = -20.015625
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1256]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya IV 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1274]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1275]],  
                x = 25223.23438,  
                y = -2134.921875,  
                z = -14.203125
              },  
              Angle = 0.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1272]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga I 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1282]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1283]],  
                x = 25239.04688,  
                y = -2011.515625,  
                z = -17.953125
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1280]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga II 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1286]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1287]],  
                x = 25332.21875,  
                y = -2008.421875,  
                z = -15.8125
              },  
              Angle = 2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1284]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya IV 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1290]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1291]],  
                x = 25415.75,  
                y = -1951.765625,  
                z = -19.6875
              },  
              Angle = -2.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1288]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya III 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1374]],  
              Name = [[Route 9]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1373]],  
                x = -0.28125,  
                y = 0,  
                z = -0.015625
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1376]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1377]],  
                    x = 25203.25,  
                    y = -2109.15625,  
                    z = -16.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1379]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1380]],  
                    x = 25202.79688,  
                    y = -2084.8125,  
                    z = -20.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1382]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1383]],  
                    x = 25203.29688,  
                    y = -2071.03125,  
                    z = -21.3125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1385]],  
              Name = [[Route 10]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1384]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1387]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1388]],  
                    x = 25203.17188,  
                    y = -2067.078125,  
                    z = -21.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1390]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1391]],  
                    x = 25208.21875,  
                    y = -2050.796875,  
                    z = -19.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1393]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1394]],  
                    x = 25218.375,  
                    y = -2036.375,  
                    z = -19.5625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1404]],  
              Name = [[Route 12]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1403]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1406]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1407]],  
                    x = 25300.57813,  
                    y = -2001.421875,  
                    z = -15.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1409]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1410]],  
                    x = 25302.9375,  
                    y = -2000.9375,  
                    z = -15.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1412]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1413]],  
                    x = 25308.79688,  
                    y = -2000.3125,  
                    z = -15.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1415]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1416]],  
                    x = 25311.07813,  
                    y = -2004.0625,  
                    z = -17.125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1562]],  
              Name = [[Route 13]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1564]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1565]],  
                    x = 25226.51563,  
                    y = -2030.984375,  
                    z = -18.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1567]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1568]],  
                    x = 25254.01563,  
                    y = -2019.296875,  
                    z = -16.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1570]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1571]],  
                    x = 25271.79688,  
                    y = -2006.65625,  
                    z = -16.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1573]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1574]],  
                    x = 25297.46875,  
                    y = -2001.25,  
                    z = -16.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1576]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1577]],  
                    x = 25299.0625,  
                    y = -2001.390625,  
                    z = -16.078125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1561]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1690]],  
              Name = [[Route 16]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1692]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1693]],  
                    x = 25335.45313,  
                    y = -2090.90625,  
                    z = -14.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1695]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1696]],  
                    x = 25350.65625,  
                    y = -2088.421875,  
                    z = -15.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1698]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1699]],  
                    x = 25354.40625,  
                    y = -2086.390625,  
                    z = -15.75
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1689]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1701]],  
              Name = [[Route 17]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1703]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1704]],  
                    x = 25330.82813,  
                    y = -2094.171875,  
                    z = -15.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1706]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1707]],  
                    x = 25347.6875,  
                    y = -2092.4375,  
                    z = -15.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1709]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1710]],  
                    x = 25356.0625,  
                    y = -2088.65625,  
                    z = -15.78125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1700]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1712]],  
              Name = [[Route 18]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1714]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1715]],  
                    x = 25379.29688,  
                    y = -2102.984375,  
                    z = -10.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1717]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1718]],  
                    x = 25360.09375,  
                    y = -2088.515625,  
                    z = -15.71875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1711]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1808]],  
              Name = [[Route 19]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1810]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1811]],  
                    x = 25342.34375,  
                    y = -2203.34375,  
                    z = -15.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1813]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1814]],  
                    x = 25344.45313,  
                    y = -2186.234375,  
                    z = -17.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1807]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1816]],  
              Name = [[Route 20]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1818]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1819]],  
                    x = 25349.64063,  
                    y = -2200.921875,  
                    z = -16.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1821]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1822]],  
                    x = 25346.48438,  
                    y = -2187.359375,  
                    z = -16.890625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1815]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1824]],  
              Name = [[Route 21]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1826]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1827]],  
                    x = 25360,  
                    y = -2196.5,  
                    z = -17.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1829]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1830]],  
                    x = 25347.60938,  
                    y = -2185.46875,  
                    z = -17.109375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1823]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1910]],  
              Name = [[Route 22]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1912]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1913]],  
                    x = 25461.84375,  
                    y = -2184.390625,  
                    z = -19.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1915]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1916]],  
                    x = 25459.71875,  
                    y = -2176.109375,  
                    z = -19.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1918]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1919]],  
                    x = 25456.1875,  
                    y = -2171.359375,  
                    z = -20.1875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1909]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1921]],  
              Name = [[Route 23]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1923]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1924]],  
                    x = 25463.35938,  
                    y = -2184.375,  
                    z = -19.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1926]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1927]],  
                    x = 25463.03125,  
                    y = -2174.625,  
                    z = -19.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1929]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1930]],  
                    x = 25455.85938,  
                    y = -2169.84375,  
                    z = -20.515625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1920]],  
                x = -0.21875,  
                y = -0.0625,  
                z = 0.109375
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1948]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1949]],  
                x = 25374.0625,  
                y = -2057.90625,  
                z = -17.0625
              },  
              Angle = 3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1946]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1952]],  
              Base = [[palette.entities.botobjects.giant_skull]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1953]],  
                x = 25404.625,  
                y = -2154.15625,  
                z = -14.265625
              },  
              Angle = 2.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1950]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[giant skull 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1956]],  
              Base = [[palette.entities.botobjects.bones_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1957]],  
                x = 25353.96875,  
                y = -2134.609375,  
                z = -15.671875
              },  
              Angle = 1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1954]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1960]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1961]],  
                x = 25378.78125,  
                y = -2132.25,  
                z = -16.578125
              },  
              Angle = 1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1958]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya I 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1964]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1965]],  
                x = 25319.35938,  
                y = -2066.0625,  
                z = -14.21875
              },  
              Angle = -0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1962]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga I 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1968]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1969]],  
                x = 25332.07813,  
                y = -2116,  
                z = -16.515625
              },  
              Angle = 0.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1966]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga III 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1972]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1973]],  
                x = 25454.23438,  
                y = -2150.734375,  
                z = -16.6875
              },  
              Angle = 2.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1970]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya IV 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1976]],  
              Base = [[palette.entities.botobjects.carrion_mammal]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1977]],  
                x = 25471.875,  
                y = -2169.203125,  
                z = -19.53125
              },  
              Angle = -1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1974]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[mammal carrion 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1984]],  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1985]],  
                x = 25471.6875,  
                y = -2166.109375,  
                z = -19.03125
              },  
              Angle = 3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1982]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1988]],  
              Base = [[palette.entities.botobjects.carapace_2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1989]],  
                x = 25531.85938,  
                y = -2166.875,  
                z = -16.296875
              },  
              Angle = -2.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1986]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[carapace 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1992]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1993]],  
                x = 25505.5,  
                y = -2168.78125,  
                z = -14.453125
              },  
              Angle = -1,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1990]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya III 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1996]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1997]],  
                x = 25528.70313,  
                y = -2256.484375,  
                z = -17.390625
              },  
              Angle = 1.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1994]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga II 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2038]],  
              Name = [[Route 24]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2040]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2041]],  
                    x = 26954.15625,  
                    y = -1927.8125,  
                    z = -8.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2043]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2044]],  
                    x = 26953.3125,  
                    y = -1916.734375,  
                    z = -9.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2046]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2047]],  
                    x = 26951.07813,  
                    y = -1908.078125,  
                    z = -7.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2049]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2050]],  
                    x = 26940.65625,  
                    y = -1895.265625,  
                    z = -6.09375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2037]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2076]],  
              Name = [[Route 25]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2075]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2078]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2079]],  
                    x = 25530.39063,  
                    y = -2237.328125,  
                    z = -18.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2081]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2082]],  
                    x = 25531.14063,  
                    y = -2220.40625,  
                    z = -17.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2084]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2085]],  
                    x = 25526.79688,  
                    y = -2204.28125,  
                    z = -17.34375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2087]],  
              Name = [[Route 26]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2086]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2089]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2090]],  
                    x = 25527.20313,  
                    y = -2244.828125,  
                    z = -19.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2092]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2093]],  
                    x = 25524.0625,  
                    y = -2209.859375,  
                    z = -17.28125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2095]],  
              Name = [[Route 27]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2094]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2097]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2098]],  
                    x = 25533.32813,  
                    y = -2241.125,  
                    z = -17.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2100]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2101]],  
                    x = 25530.875,  
                    y = -2208.328125,  
                    z = -17.265625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2103]],  
              Name = [[Route 28]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2102]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2105]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2106]],  
                    x = 25530.25,  
                    y = -2244.984375,  
                    z = -19.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2108]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2109]],  
                    x = 25528.23438,  
                    y = -2237.671875,  
                    z = -19.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2111]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2112]],  
                    x = 25526.67188,  
                    y = -2213.421875,  
                    z = -17.703125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_2204]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2203]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2206]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2207]],  
                    x = 25550.25,  
                    y = -2303.328125,  
                    z = -18.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2209]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2210]],  
                    x = 25523.42188,  
                    y = -2310.8125,  
                    z = -14.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2212]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2213]],  
                    x = 25520.875,  
                    y = -2326.5,  
                    z = -16.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2215]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2216]],  
                    x = 25494.85938,  
                    y = -2334.265625,  
                    z = -15.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2218]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2219]],  
                    x = 25479.84375,  
                    y = -2372.84375,  
                    z = -22.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2221]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2222]],  
                    x = 25546.14063,  
                    y = -2361.140625,  
                    z = -15.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2224]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2225]],  
                    x = 25571.53125,  
                    y = -2361.03125,  
                    z = -13.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2227]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2228]],  
                    x = 25577.98438,  
                    y = -2336.203125,  
                    z = -15.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2230]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2231]],  
                    x = 25648.09375,  
                    y = -2328.359375,  
                    z = -19.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2233]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2234]],  
                    x = 25656.07813,  
                    y = -2294.265625,  
                    z = -17.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2236]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2237]],  
                    x = 25632.40625,  
                    y = -2301.03125,  
                    z = -18.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2239]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2240]],  
                    x = 25580.9375,  
                    y = -2310.828125,  
                    z = -19.96875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2444]],  
              Base = [[palette.entities.botobjects.bones_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2445]],  
                x = 25507.34375,  
                y = -2083.90625,  
                z = -16.1875
              },  
              Angle = -1.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2442]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2448]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2449]],  
                x = 25560.25,  
                y = -2014.984375,  
                z = -17.71875
              },  
              Angle = -2.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2446]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bones 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2452]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2453]],  
                x = 25534.34375,  
                y = -1992.203125,  
                z = -16.734375
              },  
              Angle = -2.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2450]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya IV 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2456]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2457]],  
                x = 25509.82813,  
                y = -2066.078125,  
                z = -16.921875
              },  
              Angle = -0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2454]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya I 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2460]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2461]],  
                x = 25512.76563,  
                y = -1917.953125,  
                z = -15.859375
              },  
              Angle = -1,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2458]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga I 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2464]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2465]],  
                x = 25704.71875,  
                y = -2058.078125,  
                z = -16.890625
              },  
              Angle = 1.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2462]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga II 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2468]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_growth_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2469]],  
                x = 25672.20313,  
                y = -2082.640625,  
                z = -16.734375
              },  
              Angle = 1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2466]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya growth I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2472]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_growth_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2473]],  
                x = 25717.15625,  
                y = -2107.84375,  
                z = -13.984375
              },  
              Angle = 2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2470]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya growth II 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2476]],  
              Base = [[palette.entities.botobjects.carapace_bul]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2477]],  
                x = 25707.5,  
                y = -2121.703125,  
                z = -12.296875
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2474]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[carapace 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2480]],  
              Base = [[palette.entities.botobjects.carapace_2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2481]],  
                x = 25680.6875,  
                y = -1987.234375,  
                z = -17.078125
              },  
              Angle = -1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2478]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[carapace 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2484]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_growth_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2485]],  
                x = 25663.40625,  
                y = -2005.265625,  
                z = -17.234375
              },  
              Angle = -0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2482]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bothaya growth II 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2542]],  
              Name = [[Route 29]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2544]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2545]],  
                    x = 25674.4375,  
                    y = -1974.15625,  
                    z = -16.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2547]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2548]],  
                    x = 25689.3125,  
                    y = -1985.46875,  
                    z = -17.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2541]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_2661]],  
              Name = [[Place 2]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2663]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2664]],  
                    x = 26947.84375,  
                    y = -1985.671875,  
                    z = -7.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2666]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2667]],  
                    x = 26950.42188,  
                    y = -1973.453125,  
                    z = -7.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2669]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2670]],  
                    x = 26945.90625,  
                    y = -1965.578125,  
                    z = -7.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2672]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2673]],  
                    x = 26933.59375,  
                    y = -1964.703125,  
                    z = -6.609375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2675]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2676]],  
                    x = 26921.42188,  
                    y = -1986.109375,  
                    z = -7.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2678]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2679]],  
                    x = 26930.875,  
                    y = -1998.171875,  
                    z = -7.875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2660]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2800]],  
              Base = [[palette.entities.botobjects.stele]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2801]],  
                x = 31372.34375,  
                y = -22097.09375,  
                z = -21.515625
              },  
              Angle = -2.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2798]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[stele 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_3024]],  
              Name = [[Place 4]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3026]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3027]],  
                    x = 31264.76563,  
                    y = -22103.78125,  
                    z = -20.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3029]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3030]],  
                    x = 31298.70313,  
                    y = -22188.14063,  
                    z = -17.609375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3032]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3033]],  
                    x = 31193.17188,  
                    y = -22150.625,  
                    z = -22.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3035]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3036]],  
                    x = 31172.9375,  
                    y = -22126.09375,  
                    z = -19.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3038]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3039]],  
                    x = 31222.64063,  
                    y = -22084.54688,  
                    z = -17.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3023]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_3129]],  
              Name = [[Place 5]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3131]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3132]],  
                    x = 31359.6875,  
                    y = -22215.54688,  
                    z = -20.484375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3134]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3135]],  
                    x = 31440.23438,  
                    y = -22225.8125,  
                    z = -20.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3137]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3138]],  
                    x = 31479.54688,  
                    y = -22331.35938,  
                    z = -20.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3140]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3141]],  
                    x = 31359.71875,  
                    y = -22306.79688,  
                    z = -21.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3143]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3144]],  
                    x = 31327.01563,  
                    y = -22244.17188,  
                    z = -21.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_3146]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_3147]],  
                    x = 31307.59375,  
                    y = -22202.48438,  
                    z = -18.28125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3128]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3974]],  
              Base = [[palette.entities.botobjects.kami_altar]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3975]],  
                x = 29925.25,  
                y = -11043.45313,  
                z = -21.90625
              },  
              Angle = -2.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3972]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kami altar 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3978]],  
              Base = [[palette.entities.botobjects.kami_hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3979]],  
                x = 29908.92188,  
                y = -11036.28125,  
                z = -21.984375
              },  
              Angle = -0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3976]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kami hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3982]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3983]],  
                x = 29909.95313,  
                y = -11048.79688,  
                z = -22.078125
              },  
              Angle = -2.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3980]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3986]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3987]],  
                x = 29919.01563,  
                y = -11035.73438,  
                z = -21.953125
              },  
              Angle = -1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3984]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3990]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3991]],  
                x = 29921.89063,  
                y = -11048.875,  
                z = -21.96875
              },  
              Angle = -3.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3988]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3994]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3995]],  
                x = 29923.21875,  
                y = -11038.3125,  
                z = -21.9375
              },  
              Angle = -5.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3992]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4063]],  
              Name = [[Place 6]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4065]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4066]],  
                    x = 32794.0625,  
                    y = -1306.671875,  
                    z = -1.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4068]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4069]],  
                    x = 32765.71875,  
                    y = -1381.703125,  
                    z = 5.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4071]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4072]],  
                    x = 32783.85938,  
                    y = -1427.78125,  
                    z = 4.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4074]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4075]],  
                    x = 32828.65625,  
                    y = -1508.578125,  
                    z = 31.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4077]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4078]],  
                    x = 32888.71875,  
                    y = -1410.265625,  
                    z = -2.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4080]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4081]],  
                    x = 32823.17188,  
                    y = -1406.28125,  
                    z = 0.078125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4062]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4083]],  
              Name = [[Place 7]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4085]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4086]],  
                    x = 32816.70313,  
                    y = -1258.9375,  
                    z = -3.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4088]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4089]],  
                    x = 32792.34375,  
                    y = -1183.421875,  
                    z = 0.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4091]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4092]],  
                    x = 32866.28125,  
                    y = -1151.9375,  
                    z = -0.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4094]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4095]],  
                    x = 32883.65625,  
                    y = -1170.453125,  
                    z = -7.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4097]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4098]],  
                    x = 32843.75,  
                    y = -1214.140625,  
                    z = -6.65625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4082]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4100]],  
              Name = [[Place 8]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4102]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4103]],  
                    x = 32878.46875,  
                    y = -1142,  
                    z = -3.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4105]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4106]],  
                    x = 32819.14063,  
                    y = -1103.5625,  
                    z = 1.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4108]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4109]],  
                    x = 32840.89063,  
                    y = -946.421875,  
                    z = 0.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4111]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4112]],  
                    x = 32889.53125,  
                    y = -1054,  
                    z = -4.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4114]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4115]],  
                    x = 32894.57813,  
                    y = -1133.03125,  
                    z = -5.421875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4099]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4117]],  
              Name = [[Place 9]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4119]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4120]],  
                    x = 32911.45313,  
                    y = -1060.734375,  
                    z = -9
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4122]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4123]],  
                    x = 32909.29688,  
                    y = -955.921875,  
                    z = -0.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4125]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4126]],  
                    x = 33056.6875,  
                    y = -938.328125,  
                    z = 1.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4128]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4129]],  
                    x = 33042.15625,  
                    y = -1027.34375,  
                    z = 0.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4131]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4132]],  
                    x = 32963.70313,  
                    y = -1062.96875,  
                    z = -7.28125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4116]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4134]],  
              Name = [[Place 10]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4136]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4137]],  
                    x = 33095.70313,  
                    y = -1002,  
                    z = -1.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4139]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4140]],  
                    x = 33133.95313,  
                    y = -940.4375,  
                    z = -1.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4142]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4143]],  
                    x = 33172.04688,  
                    y = -950.78125,  
                    z = -0.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4145]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4146]],  
                    x = 33292.5625,  
                    y = -941.9375,  
                    z = 14.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4148]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4149]],  
                    x = 33343.84375,  
                    y = -1017.28125,  
                    z = 7.484375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4151]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4152]],  
                    x = 33257.45313,  
                    y = -1021.75,  
                    z = -5.8125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4133]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4154]],  
              Name = [[Place 11]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4156]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4157]],  
                    x = 33291.01563,  
                    y = -1284.609375,  
                    z = 1.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4159]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4160]],  
                    x = 33366,  
                    y = -1102.4375,  
                    z = -0.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4162]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4163]],  
                    x = 33347.10938,  
                    y = -1053.578125,  
                    z = 1.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4165]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4166]],  
                    x = 33283.65625,  
                    y = -1037.3125,  
                    z = -0.71875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4153]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4169]],  
              Base = [[palette.entities.botobjects.stele]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4170]],  
                x = 33350.10938,  
                y = -936.78125,  
                z = 16.375
              },  
              Angle = -1.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[stele de liaison]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_4326]],  
              Name = [[Route 30]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4325]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_4328]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4329]],  
                    x = 29915.92188,  
                    y = -11057.79688,  
                    z = -22.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_4331]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4332]],  
                    x = 29915.07813,  
                    y = -11045.34375,  
                    z = -22.03125
                  },  
                  InheritPos = 1
                }
              }
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Name = [[Permanent]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      InheritPos = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_8]]
    },  
    {
      Cost = 7,  
      Behavior = {
        InstanceId = [[Client1_10]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_699]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_702]],  
                Entity = r2.RefId([[Client1_678]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_701]],  
                  Type = [[Deactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_700]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      Version = 3,  
      Events = {
      },  
      Title = [[]],  
      WeatherValue = 0,  
      LocationId = [[Client1_14]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_17]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18]],  
                x = 26943.20313,  
                y = -1983.9375,  
                z = -7.015625
              },  
              Angle = 1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_15]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_783]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_786]],  
                        Entity = r2.RefId([[Client1_17]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_785]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_757]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_784]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_759]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_757]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_760]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_758]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_759]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_535]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 3,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Boeion]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_21]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_22]],  
                x = 26944.6875,  
                y = -1982.578125,  
                z = -7
              },  
              Angle = 2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_19]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_773]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_776]],  
                        Entity = r2.RefId([[Client1_21]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_775]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_749]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_774]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_752]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_749]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_750]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_751]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_752]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_524]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 10,  
              HairType = 5621806,  
              HairColor = 3,  
              Tattoo = 18,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Dycaon]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_25]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_26]],  
                x = 26934.25,  
                y = -1972.265625,  
                z = -6.859375
              },  
              Angle = -0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_23]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_788]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_791]],  
                        Entity = r2.RefId([[Client1_25]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_790]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_568]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_789]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_762]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_568]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_763]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_761]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_762]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_468]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 13,  
              HairType = 3118,  
              HairColor = 3,  
              Tattoo = 30,  
              EyesColor = 2,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Dyxius]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_29]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_30]],  
                x = 26951.4375,  
                y = -1978.046875,  
                z = -7.5625
              },  
              Angle = 3.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_27]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 0,  
              Tattoo = 3,  
              EyesColor = 6,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Apolaus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_33]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_34]],  
                x = 26948.70313,  
                y = -1978.59375,  
                z = -7.328125
              },  
              Angle = 1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_31]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 8,  
              HairType = 2862,  
              HairColor = 4,  
              Tattoo = 1,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Ulynix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_45]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_46]],  
                x = 26935.40625,  
                y = -1974.796875,  
                z = -6.953125
              },  
              Angle = 1.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_43]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_670]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_673]],  
                        Entity = r2.RefId([[Client1_45]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_672]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_566]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_671]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_649]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_566]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_648]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_649]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_650]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_499]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2051]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2052]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2038]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 9,  
              HairType = 5621806,  
              HairColor = 4,  
              Tattoo = 31,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 3,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Ulyron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_181]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_182]],  
                x = 26942.90625,  
                y = -1981.75,  
                z = -6.984375
              },  
              Angle = -0.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_179]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_778]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_781]],  
                        Entity = r2.RefId([[Client1_181]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_780]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_572]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_779]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_754]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_572]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_756]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_753]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_754]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_513]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 10,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 0,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 6,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Icalaus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_185]],  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_186]],  
                x = 26942.15625,  
                y = -1968.703125,  
                z = -7.3125
              },  
              Angle = 2.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_183]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_660]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_663]],  
                        Entity = r2.RefId([[Client1_185]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_662]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_591]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_661]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_598]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_591]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_596]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_597]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_598]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_448]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 16,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 7,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6755118,  
              Level = 0,  
              Name = [[Ican]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_d4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_202]],  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_204]],  
                x = 26940.42188,  
                y = -1968.984375,  
                z = -7.234375
              },  
              Angle = 0.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_203]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_665]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_668]],  
                        Entity = r2.RefId([[Client1_202]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_667]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_639]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_666]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_641]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_639]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_640]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_641]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_642]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_549]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 16,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 7,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6755118,  
              WeaponLeftHand = 0,  
              Name = [[Pleton]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_d4.creature]]
            },  
            {
              InstanceId = [[Client1_223]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_221]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Bloated Izam]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_224]],  
                x = 26927.23438,  
                y = -1942.84375,  
                z = -6.921875
              },  
              Angle = -1.012507915,  
              Base = [[palette.entities.creatures.cbbdb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_227]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_225]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_228]],  
                x = 26938.28125,  
                y = -1965.796875,  
                z = -7.109375
              },  
              Angle = 1.138624072,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_236]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_237]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_238]],  
                x = 26937.48438,  
                y = -1964.34375,  
                z = -7.03125
              },  
              Angle = 0.4928522408,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_246]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_247]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_248]],  
                x = 26939.64063,  
                y = -1964.265625,  
                z = -7.203125
              },  
              Angle = 3.058486223,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_256]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_257]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_258]],  
                x = 26939.3125,  
                y = -1962.9375,  
                z = -7.171875
              },  
              Angle = -1.339743495,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_265]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_263]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_266]],  
                x = 26954.28125,  
                y = -1971.59375,  
                z = -8.265625
              },  
              Name = [[Gruff Mektoub]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = -4.91204977,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_269]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_267]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_270]],  
                x = 26950.95313,  
                y = -1970.71875,  
                z = -7.921875
              },  
              Name = [[Gruff Mektoub]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = -4.867289066,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1114]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1136]],  
                x = 26939.4375,  
                y = -1907.765625,  
                z = -6.609375
              },  
              Angle = -2.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1115]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1116]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1117]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_281]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1119]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1121]],  
                        Entity = r2.RefId([[Client1_1114]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1122]],  
                          Type = [[Sit Down]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1124]],  
                        Entity = r2.RefId([[Client1_825]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1125]],  
                          Type = [[starts dialog]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1127]],  
                        Entity = r2.RefId([[Client1_678]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1128]],  
                          Type = [[Activate]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1129]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_1116]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_888]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1203]],  
                        Entity = r2.RefId([[Client1_886]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1202]],  
                          Type = [[starts dialog]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_889]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 2,  
              HairType = 5622062,  
              HairColor = 3,  
              Tattoo = 15,  
              EyesColor = 0,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 1,  
              MorphTarget4 = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 4,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Dymus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_610]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_741]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_742]],  
                  Emote = [[Die]],  
                  Who = r2.RefId([[Client1_1114]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_744]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_608]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_609]],  
            x = 26946.01563,  
            y = -1969.921875,  
            z = -7.484375
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Active = 0
        },  
        {
          Secondes = 5,  
          Behavior = {
            InstanceId = [[Client1_679]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_577]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_580]],  
                    Entity = r2.RefId([[Client1_185]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_579]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_597]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_675]],  
                    Entity = r2.RefId([[Client1_202]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_674]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_641]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_677]],  
                    Entity = r2.RefId([[Client1_45]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_676]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_649]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_765]],  
                    Entity = r2.RefId([[Client1_21]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_764]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_751]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_767]],  
                    Entity = r2.RefId([[Client1_17]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_766]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_758]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_769]],  
                    Entity = r2.RefId([[Client1_181]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_768]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_753]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_771]],  
                    Entity = r2.RefId([[Client1_25]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_770]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_761]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_578]],  
                  Type = [[On Trigger]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[Timer]],  
          Active = 0,  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Timer 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_680]],  
            x = 26951.34375,  
            y = -1965.84375,  
            z = -7.90625
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          InstanceId = [[Client1_678]],  
          Components = {
          },  
          Cyclic = 0
        },  
        {
          InstanceId = [[Client1_717]],  
          Behavior = {
            InstanceId = [[Client1_718]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_849]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_852]],  
                    Entity = r2.RefId([[Client1_610]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_851]],  
                      Type = [[starts dialog]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_850]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_719]],  
            x = 26955.5,  
            y = -1930.109375,  
            z = -8.078125
          },  
          _Zone = [[Client1_721]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_721]],  
              Name = [[Zone trigger 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_720]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_723]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_724]],  
                    x = 5.765625,  
                    y = -0.203125,  
                    z = 1.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_726]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_727]],  
                    x = -0.421875,  
                    y = 5.546875,  
                    z = -2.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_729]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_730]],  
                    x = -5.828125,  
                    y = 0.0625,  
                    z = -0.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_732]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_733]],  
                    x = 0.109375,  
                    y = -5.8125,  
                    z = -0.90625
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          },  
          Base = [[palette.entities.botobjects.campfire]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_825]],  
          Name = [[Dialog 2]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_832]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_833]],  
                  Emote = [[Enraged]],  
                  Who = r2.RefId([[Client1_185]]),  
                  Facing = r2.RefId([[Client1_45]]),  
                  Says = [[Client1_806]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_828]],  
              Time = 2,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_829]],  
                  Emote = [[Careless]],  
                  Who = r2.RefId([[Client1_17]]),  
                  Facing = r2.RefId([[Client1_21]]),  
                  Says = [[Client1_831]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_823]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_824]],  
            x = 26945.57813,  
            y = -1967.796875,  
            z = -7.5
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_886]],  
          Name = [[Dialog 3]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1204]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1205]],  
                  Emote = [[Sad]],  
                  Who = r2.RefId([[Client1_45]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1207]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1208]],  
              Time = 5,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1209]],  
                  Emote = [[Encouraging]],  
                  Who = r2.RefId([[Client1_45]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2058]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_884]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_2054]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2057]],  
                    Entity = r2.RefId([[Client1_45]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2056]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_2051]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_2055]],  
                  Type = [[end of chat]],  
                  Value = r2.RefId([[Client1_1208]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_885]],  
            x = 26963.79688,  
            y = -1931.90625,  
            z = -7.03125
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Active = 0
        },  
        {
          MissionText = [[prenez la pierre...  ]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[merci...]],  
          Behavior = {
            InstanceId = [[Client1_1188]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1192]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1195]],  
                    Entity = r2.RefId([[Client1_1114]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1194]],  
                      Type = [[Kill]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1193]],  
                  Type = [[succeeded]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 1,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.chest]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[prendre la pierre]],  
          MissionGiver = r2.RefId([[Client1_1114]]),  
          Item1Id = r2.RefId([[Client1_1190]]),  
          InheritPos = 1,  
          _Seed = 1141901798,  
          Name = [[Give Item Mission 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1189]],  
            x = 26961.29688,  
            y = -1934.234375,  
            z = -9
          },  
          InstanceId = [[Client1_1187]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          InstanceId = [[Client1_1217]],  
          Behavior = {
            InstanceId = [[Client1_1218]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1235]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1243]],  
                    Entity = r2.RefId([[Client1_1239]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1242]],  
                      Type = [[Start Act]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1236]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 3]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1219]],  
            x = 26941.82813,  
            y = -1891.71875,  
            z = -7
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1221]],  
              Name = [[Zone trigger 4]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1223]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1224]],  
                    x = 6.78125,  
                    y = -0.375,  
                    z = 0.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1226]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1227]],  
                    x = 33.390625,  
                    y = 27.03125,  
                    z = 1.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1229]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1230]],  
                    x = -25.15625,  
                    y = 22.0625,  
                    z = 1.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1232]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1233]],  
                    x = -5.609375,  
                    y = -17.140625,  
                    z = 0.078125
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1220]],  
                x = -0.296875,  
                y = -0.609375,  
                z = -0.03125
              }
            }
          },  
          _Zone = [[Client1_1221]]
        }
      },  
      Name = [[le poste relai]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      InheritPos = 1,  
      ManualWeather = 1,  
      InstanceId = [[Client1_12]]
    },  
    {
      Cost = 21,  
      Behavior = {
        InstanceId = [[Client1_1237]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1313]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1362]],  
                Entity = r2.RefId([[Client1_1337]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1361]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1364]],  
                Entity = r2.RefId([[Client1_1341]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1363]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1366]],  
                Entity = r2.RefId([[Client1_1345]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1365]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1368]],  
                Entity = r2.RefId([[Client1_1349]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1367]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1370]],  
                Entity = r2.RefId([[Client1_1353]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1369]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1372]],  
                Entity = r2.RefId([[Client1_1357]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1371]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1752]],  
                Entity = r2.RefId([[Client1_1749]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1751]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1314]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1428]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1453]],  
                Entity = r2.RefId([[Client1_1432]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1452]],  
                  Type = [[starts dialog]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1429]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1669]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1672]],  
                Entity = r2.RefId([[Client1_1609]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1671]],  
                  Type = [[Deactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1670]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1734]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1737]],  
                Entity = r2.RefId([[Client1_1721]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1736]],  
                  Type = [[Deactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1735]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1739]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1742]],  
                Entity = r2.RefId([[Client1_1725]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1741]],  
                  Type = [[Deactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1740]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      Name = [[La passe de la mort]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1337]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1338]],  
                x = 25311.82813,  
                y = -2005.890625,  
                z = -16.515625
              },  
              Angle = 1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1335]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 14,  
              HairType = 5621806,  
              HairColor = 5,  
              Tattoo = 30,  
              EyesColor = 7,  
              MorphTarget1 = 5,  
              MorphTarget2 = 1,  
              MorphTarget3 = 0,  
              MorphTarget4 = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              MorphTarget7 = 1,  
              MorphTarget8 = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Kyron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1341]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1342]],  
                x = 25313.125,  
                y = -2005.0625,  
                z = -16.375
              },  
              Angle = 2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1339]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 6,  
              HairType = 3118,  
              HairColor = 0,  
              Tattoo = 31,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 5,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 4,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Thebus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1345]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1346]],  
                x = 25304.32813,  
                y = -2003.5,  
                z = -15.921875
              },  
              Angle = -0.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1343]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5622062,  
              HairColor = 4,  
              Tattoo = 4,  
              EyesColor = 7,  
              MorphTarget1 = 0,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 7,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Kykos]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1349]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1350]],  
                x = 25300.4375,  
                y = -2010.921875,  
                z = -16.3125
              },  
              Angle = -1.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1347]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 10,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 17,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 5,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Apotheus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1353]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1354]],  
                x = 25311.48438,  
                y = -1998.828125,  
                z = -15.40625
              },  
              Angle = -3.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1351]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 2606,  
              HairColor = 3,  
              Tattoo = 9,  
              EyesColor = 3,  
              MorphTarget1 = 4,  
              MorphTarget2 = 2,  
              MorphTarget3 = 1,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Apothus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1357]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1358]],  
                x = 25316.95313,  
                y = -2002.09375,  
                z = -15.703125
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1355]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 14,  
              HairType = 5621806,  
              HairColor = 1,  
              Tattoo = 25,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 7,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Kridix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1419]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1420]],  
                x = 25201.57813,  
                y = -2111.734375,  
                z = -16.25
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1417]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1539]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1542]],  
                        Entity = r2.RefId([[Client1_1534]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1541]],  
                          Type = [[starts dialog]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1540]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_1508]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1421]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1424]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1425]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1426]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1374]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1479]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1480]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1385]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1503]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1504]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1562]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1508]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1509]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1404]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 5621806,  
              HairColor = 4,  
              Tattoo = 31,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 3,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Ulyron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1609]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1610]],  
                x = 25333.04688,  
                y = -2090.53125,  
                z = -14.234375
              },  
              Angle = -0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1607]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1731]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1732]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1690]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 4910,  
              HairColor = 1,  
              Tattoo = 28,  
              EyesColor = 3,  
              MorphTarget1 = 3,  
              MorphTarget2 = 0,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 5,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Sirgia]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1721]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1722]],  
                x = 25327.90625,  
                y = -2093.71875,  
                z = -14.8125
              },  
              Angle = -0.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1719]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1729]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1730]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1701]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 14,  
              HairType = 5622318,  
              HairColor = 0,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5596974,  
              WeaponLeftHand = 5637678,  
              Name = [[Miarni]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1725]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1726]],  
                x = 25383.20313,  
                y = -2111.453125,  
                z = -13.46875
              },  
              Angle = -4.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1723]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1727]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1728]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1712]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 11,  
              HairType = 5622830,  
              HairColor = 1,  
              Tattoo = 13,  
              EyesColor = 2,  
              MorphTarget1 = 0,  
              MorphTarget2 = 6,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 2,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Perni]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1749]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1750]],  
                x = 25315.1875,  
                y = -2012.40625,  
                z = -17.203125
              },  
              Angle = 1.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1747]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 1,  
              HairType = 4910,  
              HairColor = 4,  
              Tattoo = 27,  
              EyesColor = 1,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 5,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5597230,  
              WeaponLeftHand = 5637678,  
              Name = [[Miarniri]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1756]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1757]],  
                x = 25342.15625,  
                y = -2210.640625,  
                z = -16
              },  
              Angle = 1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1754]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1831]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1832]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1837]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1843]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1808]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 0,  
              HairType = 4910,  
              HairColor = 2,  
              Tattoo = 10,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 6,  
              MorphTarget5 = 7,  
              MorphTarget6 = 6,  
              MorphTarget7 = 2,  
              MorphTarget8 = 7,  
              JacketModel = 5653294,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Aggro = 15,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5597486,  
              WeaponLeftHand = 5637678,  
              Name = [[Robre]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1760]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1761]],  
                x = 25351.1875,  
                y = -2210.046875,  
                z = -16.34375
              },  
              Angle = -4.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1758]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1834]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1836]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1835]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1844]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1816]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 11,  
              HairType = 5166,  
              HairColor = 4,  
              Tattoo = 21,  
              EyesColor = 6,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 1,  
              MorphTarget8 = 1,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Aggro = 15,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Rochi]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1764]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1765]],  
                x = 25363.42188,  
                y = -2202.671875,  
                z = -16.6875
              },  
              Angle = -4.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1762]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1839]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1840]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1841]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1845]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1824]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 4910,  
              HairColor = 0,  
              Tattoo = 13,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 4,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Aggro = 15,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5596974,  
              WeaponLeftHand = 5637678,  
              Name = [[Frergio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1768]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1769]],  
                x = 25345.375,  
                y = -2182.71875,  
                z = -17.734375
              },  
              Angle = -1.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1766]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_3786]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_3788]],  
                        Entity = r2.RefId([[Client1_3782]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_3787]],  
                          Type = [[Decrement]],  
                          Value = [[]]
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_3785]],  
                      Type = [[death]],  
                      Value = [[]]
                    },  
                    Name = [[On Death]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4034]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4036]],  
                        Entity = r2.RefId([[Client1_4030]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4035]],  
                          Type = [[Decrement]],  
                          Value = [[]]
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4033]],  
                      Type = [[death]],  
                      Value = [[]]
                    },  
                    Name = [[On Death]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4045]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4047]],  
                        Entity = r2.RefId([[Client1_4041]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4046]],  
                          Type = [[Decrement]],  
                          Value = [[]]
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4044]],  
                      Type = [[death]],  
                      Value = [[]]
                    },  
                    Name = [[On Death]]
                  }
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 1,  
              HairType = 2606,  
              HairColor = 0,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Cetis]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1886]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1887]],  
                x = 25461.95313,  
                y = -2186.828125,  
                z = -19.171875
              },  
              Angle = 1.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1931]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1932]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1933]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1934]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1910]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 4,  
              HairType = 5622830,  
              HairColor = 2,  
              Tattoo = 16,  
              EyesColor = 7,  
              MorphTarget1 = 3,  
              MorphTarget2 = 2,  
              MorphTarget3 = 2,  
              MorphTarget4 = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5597486,  
              WeaponLeftHand = 5637678,  
              Name = [[Nirni]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1890]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1891]],  
                x = 25463.375,  
                y = -2187.296875,  
                z = -19.140625
              },  
              Angle = 1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1888]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1935]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1936]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1937]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1938]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1921]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 13,  
              HairType = 4910,  
              HairColor = 1,  
              Tattoo = 13,  
              EyesColor = 2,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Pegio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2061]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2062]],  
                x = 25532.82813,  
                y = -2243.265625,  
                z = -18.421875
              },  
              Angle = 2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2059]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2127]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2128]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2129]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2130]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2095]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 5622318,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 5637678,  
              Sex = 1,  
              WeaponRightHand = 5596206,  
              Level = 0,  
              Name = [[Trira]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2065]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2066]],  
                x = 25528,  
                y = -2247.34375,  
                z = -19.5
              },  
              Angle = 1.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2063]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2113]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2114]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2116]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2118]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2087]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 5934,  
              HairColor = 4,  
              Tattoo = 4,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 1,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 4,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 5637678,  
              Sex = 1,  
              WeaponRightHand = 5597230,  
              Level = 0,  
              Name = [[Triera]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2069]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2070]],  
                x = 25530.65625,  
                y = -2246.75,  
                z = -19.0625
              },  
              Angle = 1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2067]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2119]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2120]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2121]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2122]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2103]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 2,  
              EyesColor = 1,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              JacketModel = 5653294,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 5637678,  
              Sex = 0,  
              WeaponRightHand = 5597230,  
              Level = 0,  
              Name = [[Anibre]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2073]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2074]],  
                x = 25530.85938,  
                y = -2240.375,  
                z = -18.46875
              },  
              Angle = -1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2071]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2123]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2124]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2125]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2126]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2076]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 9,  
              HairType = 5422,  
              HairColor = 3,  
              Tattoo = 2,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 2,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 4,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              WeaponLeftHand = 5637678,  
              Sex = 0,  
              WeaponRightHand = 5596974,  
              Level = 0,  
              Name = [[Ciero]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2488]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2489]],  
                x = 25345.29688,  
                y = -2221,  
                z = -16.015625
              },  
              Angle = -1.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2486]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 14,  
              HairType = 5622574,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 2,  
              MorphTarget8 = 1,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5597230,  
              WeaponLeftHand = 5637678,  
              Name = [[Ciero]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2492]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2493]],  
                x = 25353.95313,  
                y = -2219.28125,  
                z = -16.09375
              },  
              Angle = -1.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2490]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 4910,  
              HairColor = 0,  
              Tattoo = 19,  
              EyesColor = 3,  
              MorphTarget1 = 3,  
              MorphTarget2 = 2,  
              MorphTarget3 = 1,  
              MorphTarget4 = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Pegio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2496]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2497]],  
                x = 25396.35938,  
                y = -2293.765625,  
                z = -16.640625
              },  
              Angle = 0.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2494]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5166,  
              HairColor = 3,  
              Tattoo = 4,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5597486,  
              WeaponLeftHand = 5637678,  
              Name = [[Vaera]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2500]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2501]],  
                x = 25400.25,  
                y = -2310.21875,  
                z = -17.421875
              },  
              Angle = 1.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2498]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 9,  
              HairType = 4910,  
              HairColor = 4,  
              Tattoo = 5,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 5,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5597486,  
              WeaponLeftHand = 5637678,  
              Name = [[Antoccio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2504]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2505]],  
                x = 25392.71875,  
                y = -2299.796875,  
                z = -16.59375
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2502]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5166,  
              HairColor = 0,  
              Tattoo = 7,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              MorphTarget7 = 5,  
              MorphTarget8 = 2,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Vara]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2508]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2509]],  
                x = 25643.40625,  
                y = -1988.09375,  
                z = -14.375
              },  
              Angle = -5.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2506]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 5,  
              HairType = 5622318,  
              HairColor = 0,  
              Tattoo = 13,  
              EyesColor = 0,  
              MorphTarget1 = 4,  
              MorphTarget2 = 2,  
              MorphTarget3 = 5,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Peccio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2512]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2513]],  
                x = 25643.48438,  
                y = -1995.40625,  
                z = -14.109375
              },  
              Angle = -5.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2510]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 14,  
              HairType = 5166,  
              HairColor = 1,  
              Tattoo = 28,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 0,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Vicha]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2516]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2517]],  
                x = 25688.78125,  
                y = -2004.234375,  
                z = -18.59375
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2514]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 13,  
              HairType = 5166,  
              HairColor = 0,  
              Tattoo = 7,  
              EyesColor = 2,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 2,  
              MorphTarget7 = 2,  
              MorphTarget8 = 0,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5597230,  
              WeaponLeftHand = 5637678,  
              Name = [[Pegio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2520]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2521]],  
                x = 25684.1875,  
                y = -2006.6875,  
                z = -18.25
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2518]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 2,  
              HairType = 5166,  
              HairColor = 4,  
              Tattoo = 3,  
              EyesColor = 4,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 5,  
              JacketModel = 5610542,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5596206,  
              WeaponLeftHand = 5637678,  
              Name = [[Gicho]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2524]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2525]],  
                x = 25642.84375,  
                y = -1991.703125,  
                z = -14.640625
              },  
              Angle = -5.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2522]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 14,  
              HairType = 5622574,  
              HairColor = 1,  
              Tattoo = 9,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 6,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5597486,  
              WeaponLeftHand = 5637678,  
              Name = [[Gisse]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2528]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2529]],  
                x = 25658.28125,  
                y = -1989.671875,  
                z = -15.28125
              },  
              Angle = -4.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2526]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5622318,  
              HairColor = 1,  
              Tattoo = 7,  
              EyesColor = 5,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 0,  
              JacketModel = 5653294,  
              TrouserModel = 5610030,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5597486,  
              WeaponLeftHand = 5637678,  
              Name = [[Trirara]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2532]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2533]],  
                x = 25658.01563,  
                y = -2002.046875,  
                z = -16.28125
              },  
              Angle = 1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2530]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 6,  
              HairType = 5622318,  
              HairColor = 1,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 1,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 3,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5608238,  
              ArmModel = 5610286,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5597230,  
              WeaponLeftHand = 5637678,  
              Name = [[Ciochi]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2536]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2537]],  
                x = 25672.10938,  
                y = -1973.5625,  
                z = -17
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2534]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2549]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2550]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2551]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2552]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2542]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2559]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2562]],  
                        Entity = r2.RefId([[Client1_2536]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2561]],  
                          Type = [[Sit Down]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2560]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_2551]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 2606,  
              HairColor = 3,  
              Tattoo = 7,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 2,  
              MorphTarget8 = 7,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Zean]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_1240]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_1432]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1433]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1434]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_1419]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1435]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_1430]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1455]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1458]],  
                    Entity = r2.RefId([[Client1_1419]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1457]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1425]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1456]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1431]],  
            x = 25197.57813,  
            y = -2111.3125,  
            z = -17
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Active = 0
        },  
        {
          InstanceId = [[Client1_1459]],  
          Behavior = {
            InstanceId = [[Client1_1460]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1477]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1482]],  
                    Entity = r2.RefId([[Client1_1419]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1481]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1479]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1478]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 5]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1461]],  
            x = 25206.78125,  
            y = -2080.984375,  
            z = -21
          },  
          _Zone = [[Client1_1463]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1463]],  
              Name = [[Zone trigger 6]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1462]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1465]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1466]],  
                    x = 8.09375,  
                    y = 2.75,  
                    z = 2.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1468]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1469]],  
                    x = 0,  
                    y = 5,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1471]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1472]],  
                    x = -21.84375,  
                    y = 4.25,  
                    z = 2.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1474]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1475]],  
                    x = -3.78125,  
                    y = 2.671875,  
                    z = -1.25
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          InstanceId = [[Client1_1483]],  
          Behavior = {
            InstanceId = [[Client1_1484]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1501]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1506]],  
                    Entity = r2.RefId([[Client1_1419]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1505]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1503]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1502]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 7]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1485]],  
            x = 25217.71875,  
            y = -2045.09375,  
            z = -19
          },  
          _Zone = [[Client1_1487]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1487]],  
              Name = [[Zone trigger 8]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1486]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1489]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1490]],  
                    x = 13.6875,  
                    y = -3.765625,  
                    z = 2.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1492]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1493]],  
                    x = 0,  
                    y = 5,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1495]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1496]],  
                    x = -13.140625,  
                    y = 11.265625,  
                    z = 0.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1498]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1499]],  
                    x = -0.8125,  
                    y = -1.1875,  
                    z = 0.515625
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          InstanceId = [[Client1_1510]],  
          Behavior = {
            InstanceId = [[Client1_1511]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1528]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1531]],  
                    Entity = r2.RefId([[Client1_1419]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1530]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1508]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1529]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 9]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1512]],  
            x = 25292.3125,  
            y = -2007.203125,  
            z = -18.484375
          },  
          _Zone = [[Client1_1514]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1514]],  
              Name = [[Zone trigger 10]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1513]],  
                x = 1.0625,  
                y = -4.671875,  
                z = -0.078125
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1516]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1517]],  
                    x = 5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1519]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1520]],  
                    x = -3.859375,  
                    y = 27.078125,  
                    z = 1.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1522]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1523]],  
                    x = -2.0625,  
                    y = 0.078125,  
                    z = 0.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1525]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1526]],  
                    x = 1.078125,  
                    y = -7.140625,  
                    z = 0.140625
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_1534]],  
          Name = [[Dialog 2]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1543]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1544]],  
                  Emote = [[Sad]],  
                  Who = r2.RefId([[Client1_1419]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4344]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_1532]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_4359]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_4362]],  
                    Entity = r2.RefId([[Client1_4345]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_4361]],  
                      Type = [[activate]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_4360]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1533]],  
            x = 25314.14063,  
            y = -1998.203125,  
            z = -15
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_1590]],  
          Behavior = {
            InstanceId = [[Client1_1591]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1674]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1677]],  
                    Entity = r2.RefId([[Client1_1609]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1676]],  
                      Type = [[Activate]],  
                      Value = r2.RefId([[]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1744]],  
                    Entity = r2.RefId([[Client1_1721]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1743]],  
                      Type = [[Activate]],  
                      Value = r2.RefId([[]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1746]],  
                    Entity = r2.RefId([[Client1_1725]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1745]],  
                      Type = [[Activate]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1675]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 11]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1592]],  
            x = 25358.57813,  
            y = -2084.375,  
            z = -17
          },  
          _Zone = [[Client1_1594]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1594]],  
              Name = [[Zone trigger 12]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1593]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1596]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1597]],  
                    x = 16.625,  
                    y = 2.515625,  
                    z = 3.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1599]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1600]],  
                    x = 0,  
                    y = 5,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1602]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1603]],  
                    x = -12.515625,  
                    y = 2.796875,  
                    z = 2.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1605]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1606]],  
                    x = 0,  
                    y = -5,  
                    z = 0
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_1772]],  
          Name = [[Dialog 4]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1773]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1774]],  
                  Emote = [[Enraged]],  
                  Who = r2.RefId([[Client1_1760]]),  
                  Facing = r2.RefId([[Client1_1768]]),  
                  Says = [[Client1_1775]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1798]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1799]],  
                  Emote = [[Enraged]],  
                  Who = r2.RefId([[Client1_1768]]),  
                  Facing = r2.RefId([[Client1_1760]]),  
                  Says = [[Client1_1800]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1801]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1802]],  
                  Emote = [[Sad]],  
                  Who = r2.RefId([[Client1_1760]]),  
                  Facing = r2.RefId([[Client1_1768]]),  
                  Says = [[Client1_1803]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1804]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1805]],  
                  Emote = [[Enraged]],  
                  Who = r2.RefId([[Client1_1768]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1806]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_1770]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1847]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1850]],  
                    Entity = r2.RefId([[Client1_1760]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1849]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1835]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1852]],  
                    Entity = r2.RefId([[Client1_1764]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1851]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1841]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1854]],  
                    Entity = r2.RefId([[Client1_1756]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1853]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1837]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1848]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1771]],  
            x = 25336.85938,  
            y = -2200.609375,  
            z = -16.375
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_1776]],  
          Behavior = {
            InstanceId = [[Client1_1777]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1794]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1797]],  
                    Entity = r2.RefId([[Client1_1772]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1796]],  
                      Type = [[starts dialog]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1795]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 15]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1778]],  
            x = 25360.96875,  
            y = -2237.890625,  
            z = -19
          },  
          _Zone = [[Client1_1780]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1780]],  
              Name = [[Zone trigger 16]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1779]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1782]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1783]],  
                    x = 12.265625,  
                    y = 2.265625,  
                    z = 2.375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1785]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1786]],  
                    x = 0,  
                    y = 5,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1788]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1789]],  
                    x = -15.109375,  
                    y = -2.578125,  
                    z = 2.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1791]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1792]],  
                    x = 0,  
                    y = -5,  
                    z = 0
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          InstanceId = [[Client1_1892]],  
          Behavior = {
            InstanceId = [[Client1_1893]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1940]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1943]],  
                    Entity = r2.RefId([[Client1_1886]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1942]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1933]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1945]],  
                    Entity = r2.RefId([[Client1_1890]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1944]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_1937]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1941]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 17]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1894]],  
            x = 25443,  
            y = -2161.875,  
            z = -21
          },  
          _Zone = [[Client1_1896]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1896]],  
              Name = [[Zone trigger 18]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1895]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1898]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1899]],  
                    x = 5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1901]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1902]],  
                    x = 1.171875,  
                    y = 16.234375,  
                    z = 4.3125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1904]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1905]],  
                    x = -5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1907]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1908]],  
                    x = 1.453125,  
                    y = -12.53125,  
                    z = 3.03125
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          InstanceId = [[Client1_2136]],  
          Behavior = {
            InstanceId = [[Client1_2137]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_2185]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2188]],  
                    Entity = r2.RefId([[Client1_2073]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2187]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_2125]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2190]],  
                    Entity = r2.RefId([[Client1_2069]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2189]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_2121]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2192]],  
                    Entity = r2.RefId([[Client1_2065]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2191]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_2116]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2194]],  
                    Entity = r2.RefId([[Client1_2061]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2193]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_2129]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2202]],  
                    Entity = r2.RefId([[Client1_2197]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2201]],  
                      Type = [[starts chat]],  
                      Value = r2.RefId([[Client1_2198]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_2186]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 19]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2138]],  
            x = 25520.26563,  
            y = -2190.71875,  
            z = -16.78125
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_2140]],  
              Name = [[Zone trigger 20]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2142]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2143]],  
                    x = 12.84375,  
                    y = 13.59375,  
                    z = 0.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2145]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2146]],  
                    x = -7.703125,  
                    y = 22.125,  
                    z = 0.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2148]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2149]],  
                    x = -10.015625,  
                    y = -2.40625,  
                    z = 1.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2151]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2152]],  
                    x = 0,  
                    y = -5,  
                    z = 0
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2139]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            }
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          _Zone = [[Client1_2140]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_2197]],  
          Name = [[Dialog 5]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2198]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2199]],  
                  Emote = [[Enraged]],  
                  Who = r2.RefId([[Client1_2073]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2200]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_2195]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2196]],  
            x = 25519.79688,  
            y = -2204.546875,  
            z = -15
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Active = 1
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2271]],  
          Name = [[Group 1]],  
          Components = {
            {
              InstanceId = [[Client1_2263]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2261]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2278]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2279]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2204]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2264]],  
                x = 25559.42188,  
                y = -2325.1875,  
                z = -17.046875
              },  
              Angle = -2.892874956,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2259]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2257]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2260]],  
                x = 25541.59375,  
                y = -2325.765625,  
                z = -16.90625
              },  
              Angle = 0.06230989471,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2270]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2269]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2274]],  
          Name = [[Group 2]],  
          Components = {
            {
              InstanceId = [[Client1_2267]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2265]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2283]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2284]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2204]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2268]],  
                x = 25625.84375,  
                y = -2316.40625,  
                z = -20.140625
              },  
              Angle = -2.912875175,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2247]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2245]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2248]],  
                x = 25635.84375,  
                y = -2318.8125,  
                z = -20.515625
              },  
              Angle = 2.986310005,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2273]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2272]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2277]],  
          Name = [[Group 3]],  
          Components = {
            {
              InstanceId = [[Client1_2243]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2241]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2281]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2282]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2204]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2244]],  
                x = 25518.5625,  
                y = -2345.609375,  
                z = -17.90625
              },  
              Angle = -3.112875223,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2251]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2249]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2252]],  
                x = 25509.34375,  
                y = -2338.59375,  
                z = -17.78125
              },  
              Angle = 0.06230989471,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2255]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2253]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2256]],  
                x = 25504.29688,  
                y = -2359.796875,  
                z = -20.609375
              },  
              Angle = 0.06230989471,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2276]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2275]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_2540]],  
          Name = [[Dialog 6]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2563]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2564]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_2528]]),  
                  Facing = r2.RefId([[Client1_2536]]),  
                  Says = [[Client1_4363]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2566]],  
              Time = 1,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2567]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_2536]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2573]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2571]],  
              Time = 1,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2572]],  
                  Emote = [[Enraged]],  
                  Who = r2.RefId([[Client1_2528]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2574]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_2538]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_2554]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2557]],  
                    Entity = r2.RefId([[Client1_2536]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2556]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_2551]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_2555]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2539]],  
            x = 25669.15625,  
            y = -1990.359375,  
            z = -17
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          MissionText = [[prener la pierre est sauver notre prêtre]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[merci, je vais vous attendre au poste relai]],  
          Behavior = {
            InstanceId = [[Client1_2580]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 1,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.chest]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[Prendre la pierre]],  
          MissionGiver = r2.RefId([[Client1_2536]]),  
          Item1Id = r2.RefId([[Client1_2582]]),  
          InheritPos = 1,  
          _Seed = 1142264881,  
          Name = [[Give Item Mission 3]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2581]],  
            x = 25673.9375,  
            y = -1980.484375,  
            z = -17
          },  
          InstanceId = [[Client1_2579]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_2586]],  
          Name = [[Dialog 7]],  
          Components = {
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_2584]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2585]],  
            x = 25665.35938,  
            y = -1975.828125,  
            z = 47
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_2587]],  
          Behavior = {
            InstanceId = [[Client1_2588]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_2605]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2608]],  
                    Entity = r2.RefId([[Client1_2540]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2607]],  
                      Type = [[starts dialog]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_2606]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 21]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2589]],  
            x = 25610.57813,  
            y = -1995.421875,  
            z = -17
          },  
          _Zone = [[Client1_2591]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_2591]],  
              Name = [[Zone trigger 22]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2590]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2593]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2594]],  
                    x = 5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2596]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2597]],  
                    x = -1.125,  
                    y = 9.78125,  
                    z = 2.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2599]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2600]],  
                    x = -5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2602]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2603]],  
                    x = -2.015625,  
                    y = -16.375,  
                    z = 0.09375
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        },  
        {
          InstanceId = [[Client1_2609]],  
          Behavior = {
            InstanceId = [[Client1_2610]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[LootSpawner]],  
          Npc5Id = r2.RefId([[]]),  
          Npc1Id = r2.RefId([[Client1_1768]]),  
          Npc3Id = r2.RefId([[]]),  
          Npc4Id = r2.RefId([[]]),  
          Base = [[palette.entities.botobjects.jar]],  
          _TriggerValue = 0,  
          InheritPos = 1,  
          _Seed = 1142265626,  
          Npc2Id = r2.RefId([[]]),  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2611]],  
            x = 25339.45313,  
            y = -2188.765625,  
            z = -17
          },  
          Components = {
            {
              Item2Id = r2.RefId([[]]),  
              Active = 0,  
              Behavior = {
                InstanceId = [[Client1_2613]],  
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[EasterEgg]],  
              ItemQty = 1,  
              Item1Qty = 1,  
              Item3Qty = 1,  
              Base = [[palette.entities.botobjects.jar]],  
              Item2Qty = 1,  
              Item1Id = r2.RefId([[Client1_2583]]),  
              InheritPos = 0,  
              Name = [[Easter Egg 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2614]],  
                x = 25340.45313,  
                y = -2187.765625,  
                z = -17
              },  
              _Seed = 1142265626,  
              InstanceId = [[Client1_2612]],  
              Components = {
              },  
              Item3Id = r2.RefId([[]])
            },  
            {
              TriggerValue = 0,  
              InstanceId = [[Client1_3782]],  
              Behavior = {
                InstanceId = [[Client1_3783]],  
                Class = [[LogicEntityBehavior]],  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_3790]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_3792]],  
                        Entity = r2.RefId([[Client1_2612]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_3791]],  
                          Type = [[activate]],  
                          Value = [[]]
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_3789]],  
                      Type = [[On Trigger]],  
                      Value = [[]]
                    },  
                    Name = [[On Trigger]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Counter]],  
              InheritPos = 1,  
              Name = [[Npc Counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3784]],  
                x = 25339.45313,  
                y = -2188.765625,  
                z = 0
              },  
              Value = 1,  
              Components = {
              },  
              Base = [[palette.entities.botobjects.milestone]]
            },  
            {
              TriggerValue = 0,  
              InstanceId = [[Client1_4030]],  
              Behavior = {
                InstanceId = [[Client1_4031]],  
                Class = [[LogicEntityBehavior]],  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4038]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4040]],  
                        Entity = r2.RefId([[Client1_2612]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4039]],  
                          Type = [[activate]],  
                          Value = [[]]
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4037]],  
                      Type = [[On Trigger]],  
                      Value = [[]]
                    },  
                    Name = [[On Trigger]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Counter]],  
              InheritPos = 1,  
              Name = [[Npc Counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4032]],  
                x = 25339.45313,  
                y = -2188.765625,  
                z = 0
              },  
              Value = 1,  
              Components = {
              },  
              Base = [[palette.entities.botobjects.milestone]]
            },  
            {
              TriggerValue = 0,  
              InstanceId = [[Client1_4041]],  
              Behavior = {
                InstanceId = [[Client1_4042]],  
                Class = [[LogicEntityBehavior]],  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4049]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4051]],  
                        Entity = r2.RefId([[Client1_2612]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4050]],  
                          Type = [[activate]],  
                          Value = [[]]
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4048]],  
                      Type = [[On Trigger]],  
                      Value = [[]]
                    },  
                    Name = [[On Trigger]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Counter]],  
              InheritPos = 1,  
              Name = [[Npc Counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4043]],  
                x = 25339.45313,  
                y = -2188.765625,  
                z = 0
              },  
              Value = 1,  
              Components = {
              },  
              Base = [[palette.entities.botobjects.milestone]]
            }
          },  
          Ghosts = {
          },  
          EasterEggId = [[Client1_2612]],  
          Name = [[Loot spawner 1]]
        },  
        {
          MissionText = [[trouver les pierres]],  
          Item2Id = r2.RefId([[Client1_2582]]),  
          MissionSucceedText = [[magnifique, rentrons au camp]],  
          Behavior = {
            InstanceId = [[Client1_4346]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_4349]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_4352]],  
                    Entity = r2.RefId([[Client1_2617]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_4351]],  
                      Type = [[Start Act]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_4350]],  
                  Type = [[succeeded]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[RequestItem]],  
          Item1Qty = 1,  
          Item3Qty = 1,  
          Base = [[palette.entities.botobjects.chest]],  
          Repeatable = 1,  
          Item2Qty = 1,  
          ContextualText = [[montrer les pierres]],  
          MissionGiver = r2.RefId([[Client1_1419]]),  
          Item1Id = r2.RefId([[Client1_1190]]),  
          _Seed = 1142434951,  
          InheritPos = 1,  
          InstanceId = [[Client1_4345]],  
          Name = [[Mission: Request Item 2]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4347]],  
            x = 25308.3125,  
            y = -1990.40625,  
            z = -15
          },  
          WaitValidationText = [[montrer les moi]],  
          Active = 0,  
          Components = {
          },  
          Item3Id = r2.RefId([[Client1_2583]])
        }
      },  
      Title = [[]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1238]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[Client1_1241]],  
      ManualWeather = 1,  
      InstanceId = [[Client1_1239]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_2615]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_2617]],  
      ManualWeather = 1,  
      LocationId = [[Client1_2619]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_2616]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Name = [[retour au poste relai]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_2622]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2620]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2623]],  
                x = 26954.54688,  
                y = -1971.4375,  
                z = -8.28125
              },  
              Angle = 1.514036655,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2626]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2624]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2627]],  
                x = 26951.07813,  
                y = -1970.53125,  
                z = -7.9375
              },  
              Angle = 1.514036655,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2630]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2631]],  
                x = 26951.65625,  
                y = -1978.3125,  
                z = -7.546875
              },  
              Angle = 3.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2628]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 8,  
              HairType = 3118,  
              HairColor = 2,  
              Tattoo = 5,  
              EyesColor = 6,  
              MorphTarget1 = 0,  
              MorphTarget2 = 5,  
              MorphTarget3 = 1,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 5,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Iorius]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2634]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2635]],  
                x = 26947.28125,  
                y = -1977.125,  
                z = -7.3125
              },  
              Angle = 1.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2632]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2680]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2681]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 4,  
              HairType = 2862,  
              HairColor = 0,  
              Tattoo = 1,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 5,  
              MorphTarget3 = 1,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 3,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Iolus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2638]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2639]],  
                x = 26945.8125,  
                y = -1981.515625,  
                z = -7.03125
              },  
              Angle = 1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2636]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2688]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2689]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 9,  
              HairType = 2350,  
              HairColor = 4,  
              Tattoo = 24,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 3,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Detheus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2642]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2643]],  
                x = 26943.21875,  
                y = -1979.984375,  
                z = -7
              },  
              Angle = 1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2640]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2686]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2687]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 0,  
              HairType = 2350,  
              HairColor = 2,  
              Tattoo = 9,  
              EyesColor = 2,  
              MorphTarget1 = 3,  
              MorphTarget2 = 4,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 1,  
              MorphTarget8 = 5,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Thean]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2646]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2647]],  
                x = 26941.875,  
                y = -1985.078125,  
                z = -7.03125
              },  
              Angle = 1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2644]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2690]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2691]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 4,  
              HairType = 3118,  
              HairColor = 3,  
              Tattoo = 4,  
              EyesColor = 5,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 5,  
              MorphTarget4 = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Xylion]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2650]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2651]],  
                x = 26936.71875,  
                y = -1975.515625,  
                z = -6.984375
              },  
              Angle = 1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2648]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2684]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2685]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 1,  
              HairType = 5621806,  
              HairColor = 1,  
              Tattoo = 1,  
              EyesColor = 6,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 3,  
              MorphTarget4 = 6,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              MorphTarget7 = 5,  
              MorphTarget8 = 2,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Iodix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2654]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2655]],  
                x = 26941.10938,  
                y = -1973.78125,  
                z = -7.140625
              },  
              Angle = 1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2652]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2682]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2683]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 12,  
              HairType = 5621806,  
              HairColor = 1,  
              Tattoo = 18,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 2,  
              MorphTarget8 = 2,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Iolaus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2658]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2659]],  
                x = 26937.20313,  
                y = -1980.234375,  
                z = -6.984375
              },  
              Angle = 1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2656]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2692]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2693]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2661]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 1,  
              EyesColor = 4,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 6,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Thecaon]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2696]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2697]],  
                x = 26947.10938,  
                y = -1965.90625,  
                z = -7.625
              },  
              Angle = 1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2694]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 4,  
              Tattoo = 1,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 0,  
              MorphTarget3 = 3,  
              MorphTarget4 = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 0,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Zean]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2700]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2701]],  
                x = 26948.5625,  
                y = -1965.84375,  
                z = -7.71875
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2698]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 2,  
              HairType = 5621806,  
              HairColor = 5,  
              Tattoo = 15,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              WeaponLeftHand = 0,  
              Name = [[Ulyron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          InstanceId = [[Client1_2618]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_2707]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2708]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2709]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_2696]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4052]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2712]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2713]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_2700]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2715]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_2705]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_2794]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2797]],  
                    Entity = r2.RefId([[Client1_2778]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2796]],  
                      Type = [[Start Act]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_2795]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2706]],  
            x = 26952.23438,  
            y = -1965.3125,  
            z = -9
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_2754]],  
          Behavior = {
            InstanceId = [[Client1_2755]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_2772]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_2775]],  
                    Entity = r2.RefId([[Client1_2707]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_2774]],  
                      Type = [[starts dialog]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_2773]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 23]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2756]],  
            x = 26947.6875,  
            y = -1966.0625,  
            z = -7.640625
          },  
          _Zone = [[Client1_2758]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_2758]],  
              Name = [[Zone trigger 24]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2757]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2760]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2761]],  
                    x = 2.640625,  
                    y = 0.546875,  
                    z = 0.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2763]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2764]],  
                    x = -0.296875,  
                    y = 2.03125,  
                    z = 0.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2766]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2767]],  
                    x = -2.03125,  
                    y = 0.15625,  
                    z = 0.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2769]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2770]],  
                    x = 0.359375,  
                    y = -1.0625,  
                    z = -0.09375
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          }
        }
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Version = 3
    },  
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_2776]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_3939]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_3940]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_2778]],  
      ManualWeather = 1,  
      LocationId = [[Client1_2780]],  
      WeatherValue = 505,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_2777]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Name = [[les grottes de l'oubli]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2779]]
        }
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Version = 3
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_3943]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_3945]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[Client1_3947]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_3944]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_3966]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_3967]],  
                x = 29916.73438,  
                y = -11043.0625,  
                z = -21.96875
              },  
              Angle = -1.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_3964]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Obscure Faun 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_4323]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4324]],  
                x = 29916.46875,  
                y = -11061.29688,  
                z = -22.171875
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4321]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4005]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4008]],  
                        Entity = r2.RefId([[Client1_4001]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4007]],  
                          Type = [[starts dialog]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4006]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_4335]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4335]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4336]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4326]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 2,  
              HairType = 5621806,  
              HairColor = 5,  
              Tattoo = 21,  
              EyesColor = 0,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 6,  
              MorphTarget8 = 2,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Ulyron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          InstanceId = [[Client1_3946]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_4001]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_4002]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_4003]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_4323]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4337]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_4009]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_4010]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_4323]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4338]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_4012]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_4013]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_4323]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4339]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_3999]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_4021]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_4024]],  
                    Entity = r2.RefId([[Client1_4016]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_4023]],  
                      Type = [[activate]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_4022]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4000]],  
            x = 29912.34375,  
            y = -11044.51563,  
            z = -21.96875
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          MissionText = [[prener cet objet]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[merci pour tout]],  
          Behavior = {
            InstanceId = [[Client1_4017]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 1,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.chest]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[prendre l'objet]],  
          MissionGiver = r2.RefId([[Client1_3966]]),  
          Item1Id = r2.RefId([[Client1_4019]]),  
          InheritPos = 1,  
          _Seed = 1142348272,  
          Name = [[Give Item Mission 4]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4018]],  
            x = 29918.39063,  
            y = -11047.53125,  
            z = -22.015625
          },  
          InstanceId = [[Client1_4016]],  
          Active = 0,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        }
      },  
      Counters = {
      },  
      Name = [[la plaine du sage]],  
      Title = [[]],  
      Events = {
      }
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_4057]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_4221]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_4224]],  
                Entity = r2.RefId([[Client1_4213]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_4223]],  
                  Type = [[Deactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_4222]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_4233]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_4236]],  
                Entity = r2.RefId([[Client1_4175]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_4235]],  
                  Type = [[deactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_4234]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_4059]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[Client1_4061]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_4058]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_4173]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4174]],  
                x = 33346.09375,  
                y = -934.1875,  
                z = 16.328125
              },  
              Angle = -1.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4216]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4219]],  
                        Entity = r2.RefId([[Client1_4213]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4218]],  
                          Type = [[Activate]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4217]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 0,  
              HairType = 5621806,  
              HairColor = 0,  
              Tattoo = 16,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 3,  
              MorphTarget7 = 6,  
              MorphTarget8 = 5,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Gaan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              InstanceId = [[Client1_4213]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4211]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4179]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4182]],  
                        Entity = r2.RefId([[Client1_4173]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4181]],  
                          Type = [[Activate]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4226]],  
                        Entity = r2.RefId([[Client1_4213]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4225]],  
                          Type = [[Deactivate]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4180]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_4228]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_4231]],  
                        Entity = r2.RefId([[Client1_4175]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_4230]],  
                          Type = [[activate]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_4229]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Assault Kirosta]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4214]],  
                x = 33346.0625,  
                y = -933.765625,  
                z = 16.3125
              },  
              Angle = -1.590563416,  
              Base = [[palette.entities.creatures.ckfrb4]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_4060]]
        },  
        {
          MissionText = [[vous avez passé le test]],  
          Item2Id = r2.RefId([[Client1_2582]]),  
          MissionSucceedText = [[au revoir]],  
          Behavior = {
            InstanceId = [[Client1_4176]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_4238]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_4241]],  
                    Entity = r2.RefId([[Client1_3945]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_4240]],  
                      Type = [[Start Act]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_4239]],  
                  Type = [[succeeded]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[RequestItem]],  
          Item1Qty = 1,  
          Item3Qty = 1,  
          Base = [[palette.entities.botobjects.chest]],  
          Repeatable = 0,  
          Item2Qty = 1,  
          ContextualText = [[donner les pierres]],  
          MissionGiver = r2.RefId([[Client1_4173]]),  
          Item1Id = r2.RefId([[Client1_1190]]),  
          _Seed = 1142350776,  
          InheritPos = 1,  
          InstanceId = [[Client1_4175]],  
          Name = [[Mission: Request Item 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4177]],  
            x = 33346.3125,  
            y = -928.421875,  
            z = 16.1875
          },  
          Active = 0,  
          WaitValidationText = [[donné moi les pierre que je les place sur la stele]],  
          Components = {
          },  
          Item3Id = r2.RefId([[Client1_2583]])
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_4185]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_4186]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_4187]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_4173]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4188]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_4183]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4184]],  
            x = 33340.10938,  
            y = -933.359375,  
            z = 16.140625
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_4189]],  
          Behavior = {
            InstanceId = [[Client1_4190]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_4207]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_4210]],  
                    Entity = r2.RefId([[Client1_4185]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_4209]],  
                      Type = [[starts dialog]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_4208]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger 25]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4191]],  
            x = 33343.3125,  
            y = -934.984375,  
            z = 16.296875
          },  
          _Zone = [[Client1_4193]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_4193]],  
              Name = [[Zone trigger 26]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4192]],  
                x = -1.515625,  
                y = -0.375,  
                z = -0.015625
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4195]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4196]],  
                    x = 15.359375,  
                    y = 0.03125,  
                    z = -0.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4198]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4199]],  
                    x = 6.484375,  
                    y = 5.578125,  
                    z = 0.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4201]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4202]],  
                    x = -5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_4204]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4205]],  
                    x = 4.90625,  
                    y = -6.890625,  
                    z = 0.171875
                  },  
                  InheritPos = 1
                }
              },  
              Deletable = 0
            }
          },  
          Base = [[palette.entities.botobjects.campfire]]
        }
      },  
      Counters = {
      },  
      Name = [[la stele de liaison]],  
      Title = [[]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_618]],  
        Count = 31,  
        Text = [[Ulyron, un bless�!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_744]],  
        Count = 20,  
        Text = [[Aid� moi...  Je faisais parti d'une expidition d'estin� � sauv� notre grand pr�tre Kami...  Des Matis nous ont attaqu�... Ils nous ont derob� des artefact... Les pierre de liaison... J'ai reussi a m'enfuir avec l'une d'elle...  R�cup�rer les deux autres et sauv� notre grand pr�tre...  Les pierre de liaison vous menerons jusqu'a un pr�tre Kami...  Il est le seul a pouvoir faire quelque chose...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_796]],  
        Count = 2,  
        Text = [[Les gardes ont reper� quelqu'un...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_799]],  
        Count = 15,  
        Text = [[Ican � reper� quelqu'un...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_805]],  
        Count = 1,  
        Text = [[Ulryon, un bless�!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_806]],  
        Count = 19,  
        Text = [[Ulyron, un bless�!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_814]],  
        Count = 11,  
        Text = [[yooooo]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_830]],  
        Count = 4,  
        Text = [[Ican � rep�r� quelqu'un!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_831]],  
        Count = 6,  
        Text = [[Ican � rep�r� quelqu'un...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1207]],  
        Count = 10,  
        Text = [[Il est mort...  ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1435]],  
        Count = 4,  
        Text = [[suivez moi!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1545]],  
        Count = 3,  
        Text = [[Mon dieu... quel carnage... Uniquement dfes civils... Ils n'avais pas une chance. suivez ce chemin sous l'arche les emprentes de pas montre qu'il ce sont dirig� vers la. Je reste ici pour les enter�.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1686]],  
        Count = 5,  
        Text = [[Une Ambuscade!!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1753]],  
        Count = 1,  
        Text = [[Mon dieu... quel carnage... Uniquement des civils... Ils n'avais pas une chance. suivons ce chemin sous l'arche les emprentes de pas montre qu'il ce sont dirig� vers la. ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1775]],  
        Count = 7,  
        Text = [[donne nous la pierre!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1800]],  
        Count = 5,  
        Text = [[Jamais!!!! Plut�t mourir!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1803]],  
        Count = 6,  
        Text = [[Si tel est ton choix.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1806]],  
        Count = 6,  
        Text = [[Tuer le!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2058]],  
        Count = 1,  
        Text = [[Il vous faut trouv� les autres pierres... Cet homme venait du nord ouest, ou il y � la passe de la mort. C'est surement la que les Matis on tendu leur embuscade. J'irais avec vous pour essay� de decouvrir ou ils sont parti. Suiver moi. Notre peuple compte sur vous.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2200]],  
        Count = 3,  
        Text = [[Des Firos! Tuer les!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2568]],  
        Count = 2,  
        Text = [[Donne nous la pierre! Tout tes fr�res sont mort, il est inutile de r�sist�. Pourquoi continuer � souffrir, je te promet une mort rapide si tu m'ob�is.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2573]],  
        Count = 3,  
        Text = [[Aid� moi!!! Je vous en pris!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2574]],  
        Count = 4,  
        Text = [[quesque... ne les laisser pas approcher la pierre!! Tuer les!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2575]],  
        Count = 1,  
        Text = [[Donne nous la pierre! Tous tes fr�res sont mort, il est inutile de r�sist�. Pourquoi continuer � souffrir, je te promet une mort rapide si tu m'ob�is.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2714]],  
        Count = 1,  
        Text = [[Maintenant que vous avez les trois pierres de liaison, il vous faut les placer dans la stele de liaison. Elle se trouve dans les chemin sinueux.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2715]],  
        Count = 3,  
        Text = [[je vais vous y conduire.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_3693]],  
        Count = 2,  
        Text = [[Mon dieu!! Ou somme nous tomb�!!! trouvons la st�le de liaison au plus vite ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4052]],  
        Count = 4,  
        Text = [[Maintenant que vous avez les trois pierres de liaison, il vous faut les placer dans la stele de liaison. Elle se trouve dans les grottes de l'oubli]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4188]],  
        Count = 4,  
        Text = [[bonjour, �trang�. je suis le gardien de la stele de liaison, passer mon epreuve et je vous permetrais de l'utiliser]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4337]],  
        Count = 1,  
        Text = [[pourquoi avez vous utilis?les pierres de liaison?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4338]],  
        Count = 1,  
        Text = [[un grand pr?re de tyr est malade, nous avons besion de vous pour le guerrir, grand sage.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4339]],  
        Count = 2,  
        Text = [[voila qui est maleheureux. Je vais me rendre a pyr pour le soigner donc. merci d'?re venu jusqu'ici pour un des notre. voila une recompense.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4344]],  
        Count = 2,  
        Text = [[Mon dieu... quel carnage... Uniquement des civils... Ils n'avais pas une chance. suivez ce chemin sous l'arche les emprentes de pas montre qu'il ce sont dirigé vers la. Je vous attend ici, ramener les pierres.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_4363]],  
        Count = 1,  
        Text = [[Donne nous la pierre! Tous tes fr?es sont mort, il est inutile de r?ist? Pourquoi continuer ?souffrir, je te promet une mort rapide si tu m'ob?s.]]
      }
    }
  }
}