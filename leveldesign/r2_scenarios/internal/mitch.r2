scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_7229]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Jungle28]],  
      Time = 0,  
      Name = [[(Jungle 28)]],  
      Season = [[winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_7220]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_7218]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8637742,  
      Name = [[plop]],  
      InstanceId = [[Client1_7277]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    }
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_7219]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    InstanceId = [[Client1_7216]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 137,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    ShortDescription = [[]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    ChatAction = 0,  
    PlotItem = 0,  
    Road = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    EasterEgg = 0,  
    ActivityStep = 1,  
    EventType = 0,  
    ActionStep = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    WayPoint = 0
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_7221]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 0,  
      Name = [[Permanent]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_7235]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_7234]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_7237]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -12559.76563,  
                    x = 31733.01563,  
                    InstanceId = [[Client1_7238]],  
                    Class = [[Position]],  
                    z = -0.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_7240]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -12558.75,  
                    x = 31719.84375,  
                    InstanceId = [[Client1_7241]],  
                    Class = [[Position]],  
                    z = 0.96875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_7260]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_7259]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_7262]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -12559.35938,  
                    x = 31705.75,  
                    InstanceId = [[Client1_7263]],  
                    Class = [[Position]],  
                    z = -0.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_7265]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -12560.35938,  
                    x = 31672.90625,  
                    InstanceId = [[Client1_7266]],  
                    Class = [[Position]],  
                    z = -0.8125
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_7224]]
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7222]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[]],  
      ManualWeather = 0,  
      InstanceId = [[Client1_7223]]
    },  
    {
      Cost = 5,  
      Behavior = {
        InstanceId = [[Client1_7225]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 0,  
      Name = [[Act 1:Act 1]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 452,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_7232]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5605678,  
              FeetColor = 2,  
              GabaritBreastSize = 2,  
              GabaritHeight = 10,  
              HairColor = 4,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7230]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_7273]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_7275]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_7276]],  
                        Entity = r2.RefId([[Client1_7270]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_7274]],  
                      Value = r2.RefId([[Client1_7243]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_7288]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts chat]],  
                          InstanceId = [[Client1_7290]],  
                          Value = r2.RefId([[Client1_7284]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_7291]],  
                        Entity = r2.RefId([[Client1_7283]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_7289]],  
                      Value = r2.RefId([[Client1_7267]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_7307]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_7308]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Start Act]],  
                          InstanceId = [[Client1_7309]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_7310]],  
                        Entity = r2.RefId([[Client1_7304]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_7242]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_7243]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_7235]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_7267]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_7260]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 0.125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Aecaon]],  
              Position = {
                y = -12560.125,  
                x = 31734.90625,  
                InstanceId = [[Client1_7233]],  
                Class = [[Position]],  
                z = -0.546875
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_7270]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 5609774,  
              FeetColor = 4,  
              GabaritBreastSize = 6,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 4,  
              AutoSpawn = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 14,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7268]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.15625,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 5610542,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Miarni]],  
              Position = {
                y = -12552.89063,  
                x = 31714.60938,  
                InstanceId = [[Client1_7271]],  
                Class = [[Position]],  
                z = -0.921875
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 1,  
              Tattoo = 19
            }
          },  
          InstanceId = [[Client1_7228]]
        },  
        {
          Item2Id = r2.RefId([[]]),  
          Active = 0,  
          Behavior = {
            InstanceId = [[Client1_7279]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_7298]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[opened]],  
                  InstanceId = [[Client1_7299]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[Kill]],  
                      InstanceId = [[Client1_7300]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_7301]],  
                    Entity = r2.RefId([[Client1_7232]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[EasterEgg]],  
          ItemQty = 1,  
          Item1Qty = 1,  
          Item3Qty = 1,  
          Base = [[palette.entities.botobjects.jar]],  
          Item2Qty = 1,  
          Item1Id = r2.RefId([[Client1_7277]]),  
          InheritPos = 1,  
          Name = [[Easter Egg 1]],  
          Position = {
            y = -12553.28125,  
            x = 31710.60938,  
            InstanceId = [[Client1_7280]],  
            Class = [[Position]],  
            z = 1
          },  
          _Seed = 1142266812,  
          InstanceId = [[Client1_7278]],  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_7283]],  
          Behavior = {
            InstanceId = [[Client1_7281]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_7293]],  
                Actions = {
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_7295]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_7296]],  
                    Entity = r2.RefId([[Client1_7278]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_7294]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -12549.29688,  
            x = 31712.70313,  
            InstanceId = [[Client1_7282]],  
            Class = [[Position]],  
            z = -0.578125
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_7284]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_7285]],  
                  Who = r2.RefId([[Client1_7270]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_7286]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 1
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7226]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[Client1_7229]],  
      ManualWeather = 1,  
      InstanceId = [[Client1_7227]]
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_7302]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_7304]],  
      LocationId = [[Client1_7229]],  
      ManualWeather = 1,  
      WeatherValue = 585,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7303]],  
        Class = [[Position]],  
        z = 0
      },  
      Name = [[Act 2:Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_7313]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 2,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 2,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7311]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.078125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 8,  
              MorphTarget3 = 3,  
              MorphTarget7 = 1,  
              ArmModel = 0,  
              Position = {
                y = -12563.73438,  
                x = 31727.57813,  
                InstanceId = [[Client1_7314]],  
                Class = [[Position]],  
                z = -0.8125
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Icaps]],  
              Sex = 0,  
              JacketModel = 5606446,  
              WeaponLeftHand = 0,  
              ArmColor = 5,  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          InstanceId = [[Client1_7305]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_7317]],  
          Behavior = {
            InstanceId = [[Client1_7315]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -12568.01563,  
            x = 31724.90625,  
            InstanceId = [[Client1_7316]],  
            Class = [[Position]],  
            z = 1
          },  
          Active = 1,  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_7318]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dance]],  
                  InstanceId = [[Client1_7319]],  
                  Who = r2.RefId([[Client1_7313]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_7320]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 1,  
              InstanceId = [[Client1_7321]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dance]],  
                  InstanceId = [[Client1_7322]],  
                  Who = r2.RefId([[Client1_7313]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_7323]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 1,  
              InstanceId = [[Client1_7324]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dance]],  
                  InstanceId = [[Client1_7325]],  
                  Who = r2.RefId([[Client1_7313]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_7331]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 1,  
              InstanceId = [[Client1_7328]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dance]],  
                  InstanceId = [[Client1_7329]],  
                  Who = r2.RefId([[Client1_7313]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_7330]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        }
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Version = 3
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_7217]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_7286]],  
        Class = [[TextManagerEntry]],  
        Text = [[abracadabra !!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_7320]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't stop this feeling !!!!!]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_7323]],  
        Class = [[TextManagerEntry]],  
        Text = [[deep inside of meeeee .....]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_7330]],  
        Class = [[TextManagerEntry]],  
        Text = [[what you did to me ...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_7331]],  
        Class = [[TextManagerEntry]],  
        Text = [[girl you just don't realise]]
      }
    }
  }
}