scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_627]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Jungle28]],  
      Time = 0,  
      Name = [[(Jungle 28)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_618]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_616]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_617]],  
    Class = [[Position]],  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 3,  
    Road = 0,  
    TextManagerEntry = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    TextManager = 0,  
    WayPoint = 0,  
    Position = 0,  
    Location = 0,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_614]],  
    Class = [[MapDescription]],  
    LevelId = 3,  
    LocationId = 137,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_619]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_621]],  
      ManualWeather = 0,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_620]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_870]],  
              Class = [[Region]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_869]],  
                x = 0.6875,  
                y = -0.15625,  
                z = -0.171875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_872]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -12554.8125,  
                    x = 31636.5,  
                    InstanceId = [[Client1_873]],  
                    Class = [[Position]],  
                    z = -0.71875
                  }
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_890]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_891]],  
                    x = 31637.79688,  
                    y = -12555.76563,  
                    z = -0.5625
                  },  
                  InheritPos = 1
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_875]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -12561.25,  
                    x = 31644.59375,  
                    InstanceId = [[Client1_876]],  
                    Class = [[Position]],  
                    z = -0.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_878]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -12564.29688,  
                    x = 31639.5625,  
                    InstanceId = [[Client1_879]],  
                    Class = [[Position]],  
                    z = -0.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_881]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -12555.625,  
                    x = 31631.64063,  
                    InstanceId = [[Client1_882]],  
                    Class = [[Position]],  
                    z = -0.296875
                  }
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_887]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_888]],  
                    x = 31632.78125,  
                    y = -12554.23438,  
                    z = -0.296875
                  },  
                  InheritPos = 1
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_884]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -12552.42188,  
                    x = 31634.46875,  
                    InstanceId = [[Client1_885]],  
                    Class = [[Position]],  
                    z = -0.46875
                  }
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_893]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_895]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_896]],  
                    x = 31631.1875,  
                    y = -12556.65625,  
                    z = -0.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_898]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_899]],  
                    x = 31636.75,  
                    y = -12563.04688,  
                    z = -0.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_901]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_902]],  
                    x = 31636.5625,  
                    y = -12566.8125,  
                    z = -0.828125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_892]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_622]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_623]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_625]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[Client1_627]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_624]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_626]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 3,  
        InstanceId = [[Client1_665]],  
        Class = [[TextManagerEntry]],  
        Text = [[blabla]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_668]],  
        Class = [[TextManagerEntry]],  
        Text = [[blibli]]
      }
    },  
    InstanceId = [[Client1_615]]
  }
}