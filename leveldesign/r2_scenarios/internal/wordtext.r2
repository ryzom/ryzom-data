scenario = {
  AccessRules = [[]],  
  Locations = {
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 50,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    MaxEntities = 1000,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_5]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_9]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_14]],  
              Base = [[palette.entities.creatures.cpfdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_15]],  
                x = 28927.64063,  
                y = -1917.625,  
                z = 75.109375
              },  
              Angle = -1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_12]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Budding Shooki]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19]],  
                x = 28932.67188,  
                y = -1910.859375,  
                z = 75.40625
              },  
              Angle = -1.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_16]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_22]],  
              Base = [[palette.entities.creatures.ckjib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_23]],  
                x = 28803.92188,  
                y = -2046.21875,  
                z = 75
              },  
              Angle = 0.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_20]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipesta]]
            }
          },  
          InstanceId = [[Client1_11]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}