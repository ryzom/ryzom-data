scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Name = [[opo]],  
      Time = 0,  
      InstanceId = [[Client1_12]],  
      Season = [[summer]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Primes20]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_3]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Versions = {
    Scenario = 2,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    Location = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 1,  
    LocationId = 180,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      LocationId = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Version = 3,  
      Name = [[Permanent]],  
      Title = [[]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      ManualWeather = 1,  
      LocationId = [[Client1_12]],  
      WeatherValue = 420,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_13]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Bloated Izam]],  
              Position = {
                y = -22095.8125,  
                x = 35810.54688,  
                InstanceId = [[Client1_16]],  
                Class = [[Position]],  
                z = -17.84375
              },  
              Angle = -2.75,  
              Base = [[palette.entities.creatures.cbbdb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_19]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_17]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              Position = {
                y = -22104.59375,  
                x = 35800.29688,  
                InstanceId = [[Client1_20]],  
                Class = [[Position]],  
                z = -14.703125
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.creatures.chcdb7]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_11]]
        }
      },  
      Counters = {
      },  
      Version = 3,  
      Name = [[Act 1:popo]],  
      Title = [[]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}