scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_298]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Forest21]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Passage of the Pit (Forests 21)]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    }
  },  
  InstanceId = [[Client1_289]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_287]]
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_288]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 5,  
    MapDescription = 0,  
    Position = 0,  
    Location = 1,  
    TextManager = 0,  
    LogicEntityBehavior = 1,  
    DefaultFeature = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 1,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_285]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_292]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_290]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_291]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_293]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Permanent]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_296]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_294]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_295]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 644,  
      LocationId = [[Client1_298]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_297]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 1:Act 1]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_286]]
  }
}