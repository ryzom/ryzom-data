scenario = {
  AccessRules = [[]],  
  Locations = {
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_3]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    ShortDescription = [[]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_6]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      Version = 3,  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      LocationId = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Name = [[Permanent]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      InheritPos = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_8]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_10]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      Version = 3,  
      Events = {
      },  
      Title = [[Act 1]],  
      WeatherValue = 0,  
      LocationId = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_16]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_17]],  
                x = 31350.67188,  
                y = -1307.828125,  
                z = -6.015625
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_14]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 8,  
              HairType = 5621806,  
              HairColor = 5,  
              Tattoo = 13,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 3,  
              MorphTarget8 = 5,  
              Sex = 0,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Dioros]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_20]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_21]],  
                x = 31350.92188,  
                y = -1311.0625,  
                z = -5.734375
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 5166,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              MorphTarget7 = 6,  
              MorphTarget8 = 1,  
              Sex = 0,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Lichi]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        }
      },  
      Name = [[Act 1]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      InheritPos = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_12]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}