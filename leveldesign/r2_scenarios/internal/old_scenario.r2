scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 20,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_55]],  
              Name = [[no repeat]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_57]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_58]],  
                    x = 26053.46875,  
                    y = -1980.6875,  
                    z = 14.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_60]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_61]],  
                    x = 26053.48438,  
                    y = -1980.640625,  
                    z = 14.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_63]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_64]],  
                    x = 26051.90625,  
                    y = -1986.875,  
                    z = 13.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_66]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_67]],  
                    x = 26050.60938,  
                    y = -2004.546875,  
                    z = 14.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_69]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_70]],  
                    x = 26096.73438,  
                    y = -1998.46875,  
                    z = 6.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_72]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_73]],  
                    x = 26108.95313,  
                    y = -1980.125,  
                    z = 4.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_75]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_76]],  
                    x = 26072.84375,  
                    y = -1986.3125,  
                    z = 12.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_78]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_79]],  
                    x = 26058.17188,  
                    y = -1980.1875,  
                    z = 14.234375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_54]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_118]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_117]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_120]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_121]],  
                    x = 26022.70313,  
                    y = -2011.5625,  
                    z = 13.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_123]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_124]],  
                    x = 26026.67188,  
                    y = -2012.265625,  
                    z = 14
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_126]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_127]],  
                    x = 26027.23438,  
                    y = -2005.546875,  
                    z = 14.421875
                  },  
                  InheritPos = 1
                }
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_4]],  
      Events = {
      },  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_150]]
      },  
      ExportList = {
      }
    },  
    {
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_80]],  
        [[Client1_128]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_10]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_11]],  
                x = 26016.875,  
                y = -2010.1875,  
                z = 14.171875
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_8]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_108]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_109]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_110]],  
                            Emote = [[]],  
                            Who = r2.RefId([[Client1_10]]),  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_111]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) sec says poim...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_142]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_144]],  
                        Entity = r2.RefId([[Client1_14]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_143]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_112]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_141]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_108]])
                    },  
                    Name = [[Event 1 : on 'end of chat sequence Chat1' event]]
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 10,  
              HairType = 2862,  
              HairColor = 5,  
              Tattoo = 26,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Name = [[sec]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_14]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_15]],  
                x = 26018.46875,  
                y = -2007.171875,  
                z = 14.375
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_12]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_112]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_113]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_114]],  
                            Emote = [[Panick]],  
                            Who = r2.RefId([[Client1_14]]),  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_115]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) rask�uuu says TEEE...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_146]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_148]],  
                        Entity = r2.RefId([[Client1_18]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_147]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_145]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_112]])
                    },  
                    Name = [[Event 1 : on 'end of chat sequence Chat1' event]]
                  }
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 3,  
              HairType = 5621806,  
              HairColor = 1,  
              Tattoo = 21,  
              EyesColor = 7,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 7,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 2,  
              FeetColor = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Name = [[rask�uuu]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_18]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19]],  
                x = 26021.89063,  
                y = -2011.640625,  
                z = 13.859375
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_16]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_128]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_129]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_118]]),  
                        Name = [[Activity 1 : Follow Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_100]],  
                    Name = [[Chat1]],  
                    Components = {
                    }
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_101]],  
                    Name = [[Chat2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_102]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_103]],  
                            Emote = [[]],  
                            Who = r2.RefId([[Client1_18]]),  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_107]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) ..... to the maXXX says moi ...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_131]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_133]],  
                        Entity = r2.RefId([[Client1_18]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_132]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_101]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_130]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_129]])
                    },  
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Follow Route Route 1 without time limit' event]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_137]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_139]],  
                        Entity = r2.RefId([[Client1_10]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_138]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_108]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_136]],  
                      Type = [[end of chat step]],  
                      Value = r2.RefId([[Client1_102]])
                    },  
                    Name = [[Event 2 : on 'end of chat step Chat 1 : (after  1sec) ..... to the maXXX says moi ...' event]]
                  }
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 14,  
              HairType = 5623086,  
              HairColor = 5,  
              Tattoo = 21,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              JacketModel = 0,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 1,  
              FeetColor = 4,  
              Sex = 0,  
              WeaponRightHand = 0,  
              Name = [[..... to the maXXX]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_128]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            }
          },  
          InstanceId = [[Client1_7]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_47]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_24]],  
              Base = [[palette.entities.creatures.ckhdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_25]],  
                x = 26061.89063,  
                y = -1982.359375,  
                z = 12.828125
              },  
              Angle = 1.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_22]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_80]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_81]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Hard Kipee]],  
              ActivitiesId = {
                [[Client1_80]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_36]],  
              Base = [[palette.entities.creatures.ckhdb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_37]],  
                x = 26066.89063,  
                y = -1988.046875,  
                z = 11.6875
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_34]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_40]],  
              Base = [[palette.entities.creatures.ckhdb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_41]],  
                x = 26073.40625,  
                y = -1984.296875,  
                z = 12.4375
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_38]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_28]],  
              Base = [[palette.entities.creatures.ckhdb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_29]],  
                x = 26070.5,  
                y = -1992.171875,  
                z = 11.84375
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_26]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Moderate  Kipee]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_32]],  
              Base = [[palette.entities.creatures.ckhdb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_33]],  
                x = 26067.53125,  
                y = -1984.578125,  
                z = 12.359375
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_30]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Vulgar  Kipee]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_46]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_45]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            }
          }
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_6]],  
      Events = {
      },  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_151]]
      },  
      ExportList = {
      }
    },  
    {
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_94]],  
        [[Client1_96]],  
        [[Client1_98]]
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_84]],  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_85]],  
                x = 26079.875,  
                y = -1991.03125,  
                z = 9.640625
              },  
              Angle = -2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_82]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_94]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_95]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 6700334,  
              HairColor = 5,  
              Tattoo = 10,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 6,  
              MorphTarget3 = 6,  
              MorphTarget4 = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 6699822,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5595950,  
              Name = [[desert guard 1]],  
              Sex = 1,  
              ActivitiesId = {
                [[Client1_94]]
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_88]],  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_89]],  
                x = 26067.42188,  
                y = -1990.296875,  
                z = 11.578125
              },  
              Angle = -2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_86]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_96]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_97]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander no repeat for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 3,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 17,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 3,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6756142,  
              Name = [[no repeat]],  
              Sex = 1,  
              ActivitiesId = {
                [[Client1_96]]
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_92]],  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_93]],  
                x = 26055.98438,  
                y = -1993.0625,  
                z = 13.359375
              },  
              Angle = -1.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_90]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_98]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_99]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander no repeat without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 12,  
              HairType = 5604398,  
              HairColor = 5,  
              Tattoo = 2,  
              EyesColor = 4,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 2,  
              JacketModel = 6701870,  
              TrouserModel = 5604654,  
              FeetModel = 6699310,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = [[run]],  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5595694,  
              Name = [[desert guard 3]],  
              Sex = 0,  
              ActivitiesId = {
                [[Client1_98]]
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_21]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_20]],  
      States = {
      },  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_152]]
      },  
      ExportList = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_104]],  
        Count = 3,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_105]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_106]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_107]],  
        Count = 4,  
        Text = [[moi rask ??? HO YEAH \:D/]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_111]],  
        Count = 3,  
        Text = [[poimp poimp poimp je joue de la sie musicale avec le sosie de julien leperse]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_115]],  
        Count = 4,  
        Text = [[TEEE BBAAYOOOOO]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_116]],  
        Count = 1,  
        Text = [[TEEE BBAAYOOOOO]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}