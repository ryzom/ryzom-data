scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_384]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[The Botoga's Drop (Desert 01)]],  
      Season = [[Winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_375]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_373]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_374]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_371]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    RegionVertex = 0,  
    TextManager = 0,  
    Npc = 0,  
    Position = 0,  
    Location = 1,  
    Road = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_378]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_376]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_377]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              Position = {
                y = 0,  
                x = 0,  
                z = 0,  
                Class = [[Position]],  
                InstanceId = [[Client1_385]]
              },  
              Class = [[Road]],  
              InstanceId = [[Client1_386]],  
              Points = {
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1299.435181,  
                    x = 21504.6543,  
                    z = 79.54059601,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_389]]
                  },  
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_388]]
                },  
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1302.547729,  
                    x = 21505.1875,  
                    z = 80.69772339,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_392]]
                  },  
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_391]]
                },  
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1307.092163,  
                    x = 21481.77734,  
                    z = 78.80776978,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_395]]
                  },  
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_394]]
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              Position = {
                y = 0,  
                x = 0,  
                z = 0,  
                Class = [[Position]],  
                InstanceId = [[Client1_402]]
              },  
              Class = [[Road]],  
              InstanceId = [[Client1_403]],  
              Points = {
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1295.741577,  
                    x = 21501.54102,  
                    z = 78.0508194,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_406]]
                  },  
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_405]]
                },  
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1299.120117,  
                    x = 21481.86914,  
                    z = 77.11299896,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_409]]
                  },  
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_408]]
                },  
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1287.078003,  
                    x = 21474.03516,  
                    z = 75.19921875,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_412]]
                  },  
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_411]]
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              Position = {
                y = 0,  
                x = 0,  
                z = 0,  
                Class = [[Position]],  
                InstanceId = [[Client1_417]]
              },  
              Class = [[Region]],  
              InstanceId = [[Client1_418]],  
              Points = {
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1280.991943,  
                    x = 21508.94336,  
                    z = 75.04260254,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_421]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_420]]
                },  
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1285.463989,  
                    x = 21505.72656,  
                    z = 75.4831543,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_424]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_423]]
                },  
                {
                  InheritPos = 1,  
                  Position = {
                    y = -1289.119873,  
                    x = 21513.55664,  
                    z = 75.94019318,  
                    Class = [[Position]],  
                    InstanceId = [[Client1_427]]
                  },  
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_426]]
                }
              }
            }
          },  
          InstanceId = [[Client1_379]]
        }
      },  
      ManualWeather = 0,  
      LocationId = [[]]
    },  
    {
      InstanceId = [[Client1_382]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_380]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Act 1:Act 1]],  
      WeatherValue = 118,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_381]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_398]],  
              ActivitiesId = {
              },  
              Tattoo = 11,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 10,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 12,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_396]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_400]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_401]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_386]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 5605422,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              HairType = 2862,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 4,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              ArmModel = 0,  
              Name = [[Deseus]],  
              Position = {
                y = -1298.699219,  
                x = 21500.4082,  
                z = 78.96826172,  
                Class = [[Position]],  
                InstanceId = [[Client1_399]]
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Angle = 0.2890985608
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_415]],  
              ActivitiesId = {
              },  
              Tattoo = 8,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 9,  
              HandsModel = 5613870,  
              FeetColor = 1,  
              GabaritBreastSize = 11,  
              GabaritHeight = 6,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 13,  
              HandsColor = 5,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_413]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_428]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_429]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_418]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              HairType = 7470,  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 2,  
              JacketModel = 0,  
              InheritPos = 1,  
              ArmModel = 0,  
              Name = [[Be'Riplan]],  
              Position = {
                y = -1283.891602,  
                x = 21515.79297,  
                z = 75.18018341,  
                Class = [[Position]],  
                InstanceId = [[Client1_416]]
              },  
              Sex = 1,  
              MorphTarget7 = 5,  
              MorphTarget3 = 1,  
              Angle = 2.753823996
            },  
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Angle = -0.9905377626,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_454]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 21571.6875,  
                y = -1272.87854,  
                z = 75.61502075,  
                InstanceId = [[Client1_457]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_456]],  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 11,  
              HairColor = 2,  
              Tattoo = 16,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 6,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 5,  
              Sex = 0,  
              HairType = 5621806,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              JacketColor = 0,  
              TrouserColor = 4,  
              FeetColor = 5,  
              HandsColor = 5,  
              ArmColor = 5,  
              SheetClient = [[basic_fyros_male.creature]],  
              Name = [[Icanix]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              Angle = -0.9905377626,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_458]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 21611.5,  
                y = -1214.121338,  
                z = 76.96479797,  
                InstanceId = [[Client1_461]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_460]],  
              Name = [[Ardent Genius 1]]
            }
          },  
          InstanceId = [[Client1_383]]
        }
      },  
      ManualWeather = 1,  
      LocationId = [[Client1_384]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_372]],  
    Texts = {
    }
  }
}