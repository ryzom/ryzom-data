scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 15,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[no repeat]],  
              InstanceId = [[Client1_55]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_54]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_57]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1980.6875,  
                    x = 26053.46875,  
                    InstanceId = [[Client1_58]],  
                    Class = [[Position]],  
                    z = 14.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_60]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1980.640625,  
                    x = 26053.48438,  
                    InstanceId = [[Client1_61]],  
                    Class = [[Position]],  
                    z = 14.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_63]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1986.875,  
                    x = 26051.90625,  
                    InstanceId = [[Client1_64]],  
                    Class = [[Position]],  
                    z = 13.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_66]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2004.546875,  
                    x = 26050.60938,  
                    InstanceId = [[Client1_67]],  
                    Class = [[Position]],  
                    z = 14.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_69]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1998.46875,  
                    x = 26096.73438,  
                    InstanceId = [[Client1_70]],  
                    Class = [[Position]],  
                    z = 6.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_72]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1980.125,  
                    x = 26108.95313,  
                    InstanceId = [[Client1_73]],  
                    Class = [[Position]],  
                    z = 4.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_75]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1986.3125,  
                    x = 26072.84375,  
                    InstanceId = [[Client1_76]],  
                    Class = [[Position]],  
                    z = 12.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_78]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1980.1875,  
                    x = 26058.17188,  
                    InstanceId = [[Client1_79]],  
                    Class = [[Position]],  
                    z = 14.234375
                  }
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_118]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_120]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_121]],  
                    x = 26022.70313,  
                    y = -2011.5625,  
                    z = 13.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_123]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_124]],  
                    x = 26026.67188,  
                    y = -2012.265625,  
                    z = 14
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_126]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_127]],  
                    x = 26027.23438,  
                    y = -2005.546875,  
                    z = 14.421875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_117]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 8,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_80]],  
        [[Client1_128]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_10]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 0,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_135]],  
                    LogicEntityAction = [[Client1_137]],  
                    ActionStep = [[Client1_139]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of chat step Chat 1 : (after  3sec) ..... to the maXXX says moi ...' event of ..... to the maXXX]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_108]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_109]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_110]],  
                            Emote = [[]],  
                            Who = r2.RefId([[Client1_10]]),  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_111]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) sec says poim...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_142]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_144]],  
                        Entity = r2.RefId([[Client1_14]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_143]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_112]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_141]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_108]])
                    },  
                    Name = [[Event 1 : on 'end of chat sequence Chat1' event]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 0,  
              Angle = 0.296875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                y = -2010.1875,  
                x = 26016.875,  
                InstanceId = [[Client1_11]],  
                Class = [[Position]],  
                z = 14.171875
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[sec]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_14]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 3,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_140]],  
                    LogicEntityAction = [[Client1_142]],  
                    ActionStep = [[Client1_144]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of chat Chat1' event of sec]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_112]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_113]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_114]],  
                            Emote = [[Panick]],  
                            Who = r2.RefId([[Client1_14]]),  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_115]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) rask�uuu says TEEE...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_146]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_148]],  
                        Entity = r2.RefId([[Client1_18]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_147]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_145]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_112]])
                    },  
                    Name = [[Event 1 : on 'end of chat sequence Chat1' event]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 7,  
              FeetModel = 0,  
              Angle = 0.296875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                y = -2007.171875,  
                x = 26018.46875,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 14.375
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[rask�uuu]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 0,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_18]],  
              ActivitiesId = {
                [[Client1_128]]
              },  
              HairType = 5623086,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_134]],  
                    LogicEntityAction = [[Client1_131]],  
                    ActionStep = [[Client1_133]],  
                    Name = [[Trigger 1 : Begin chat Chat2 on 'End of activity step Activity 1 : Follow Route Route 1 without time limit' event of ..... to the maXXX]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_149]],  
                    LogicEntityAction = [[Client1_146]],  
                    ActionStep = [[Client1_148]],  
                    Name = [[Trigger 2 : Die on 'End of chat Chat1' event of rask�uuu]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_100]],  
                    Name = [[Chat1]],  
                    Components = {
                    }
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_101]],  
                    Name = [[Chat2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_102]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_103]],  
                            Emote = [[]],  
                            Who = r2.RefId([[Client1_18]]),  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_107]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) ..... to the maXXX says moi ...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_131]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_133]],  
                        Entity = r2.RefId([[Client1_18]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_132]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_101]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_130]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_129]])
                    },  
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Follow Route Route 1 without time limit' event]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_137]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_139]],  
                        Entity = r2.RefId([[Client1_10]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_138]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_108]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_136]],  
                      Type = [[end of chat step]],  
                      Value = r2.RefId([[Client1_102]])
                    },  
                    Name = [[Event 2 : on 'end of chat step Chat 1 : (after  1sec) ..... to the maXXX says moi ...' event]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_128]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_129]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_118]]),  
                        Name = [[Activity 1 : Follow Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Angle = 0.296875,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19]],  
                x = 26021.89063,  
                y = -2011.640625,  
                z = 13.859375
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[..... to the maXXX]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 21
            }
          },  
          InstanceId = [[Client1_7]]
        },  
        {
          InstanceId = [[Client1_47]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_46]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_24]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_22]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_80]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_81]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Hard Kipee]],  
              Position = {
                y = -1982.359375,  
                x = 26061.89063,  
                InstanceId = [[Client1_25]],  
                Class = [[Position]],  
                z = 12.828125
              },  
              Angle = 1.633158326,  
              Base = [[palette.entities.creatures.ckhdb4]],  
              ActivitiesId = {
                [[Client1_80]]
              }
            },  
            {
              InstanceId = [[Client1_36]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_34]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]],  
              Position = {
                y = -1988.046875,  
                x = 26066.89063,  
                InstanceId = [[Client1_37]],  
                Class = [[Position]],  
                z = 11.6875
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckhdb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_40]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_38]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]],  
              Position = {
                y = -1984.296875,  
                x = 26073.40625,  
                InstanceId = [[Client1_41]],  
                Class = [[Position]],  
                z = 12.4375
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckhdb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_28]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_26]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Moderate  Kipee]],  
              Position = {
                y = -1992.171875,  
                x = 26070.5,  
                InstanceId = [[Client1_29]],  
                Class = [[Position]],  
                z = 11.84375
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckhdb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_32]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_30]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Vulgar  Kipee]],  
              Position = {
                y = -1984.578125,  
                x = 26067.53125,  
                InstanceId = [[Client1_33]],  
                Class = [[Position]],  
                z = 12.359375
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckhdb2]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 3,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_94]],  
        [[Client1_96]],  
        [[Client1_98]]
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_84]],  
              ActivitiesId = {
                [[Client1_94]]
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 0,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_82]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_94]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_95]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5603886,  
              Angle = -2.140625,  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Tattoo = 10,  
              MorphTarget3 = 6,  
              MorphTarget7 = 6,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[desert guard 1]],  
              Position = {
                y = -1991.03125,  
                x = 26079.875,  
                InstanceId = [[Client1_85]],  
                Class = [[Position]],  
                z = 9.640625
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_88]],  
              ActivitiesId = {
                [[Client1_96]]
              },  
              HairType = 6700334,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 6699822,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 13,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_86]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_96]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_97]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander no repeat for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Angle = -2.140625,  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Tattoo = 17,  
              MorphTarget3 = 0,  
              MorphTarget7 = 7,  
              Sex = 1,  
              WeaponRightHand = 6756142,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[no repeat]],  
              Position = {
                y = -1990.296875,  
                x = 26067.42188,  
                InstanceId = [[Client1_89]],  
                Class = [[Position]],  
                z = 11.578125
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_92]],  
              ActivitiesId = {
                [[Client1_98]]
              },  
              HairType = 5604398,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 7,  
              HandsModel = 5604142,  
              FeetColor = 5,  
              GabaritBreastSize = 12,  
              GabaritHeight = 13,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 11,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_90]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_98]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_99]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[Activity 1 : Wander no repeat without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Speed = [[run]],  
              Angle = -1.390625,  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              Tattoo = 2,  
              MorphTarget3 = 5,  
              MorphTarget7 = 0,  
              Sex = 0,  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[desert guard 3]],  
              Position = {
                y = -1993.0625,  
                x = 26055.98438,  
                InstanceId = [[Client1_93]],  
                Class = [[Position]],  
                z = 13.359375
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          InstanceId = [[Client1_21]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_20]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_104]],  
        Count = 3,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_105]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_106]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_107]],  
        Count = 4,  
        Text = [[moi rask ??? HO YEAH \:D/]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_111]],  
        Count = 3,  
        Text = [[poimp poimp poimp je joue de la sie musicale avec le sosie de julien leperse]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_115]],  
        Count = 4,  
        Text = [[TEEE BBAAYOOOOO]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_116]],  
        Count = 1,  
        Text = [[TEEE BBAAYOOOOO]]
      }
    }
  }
}