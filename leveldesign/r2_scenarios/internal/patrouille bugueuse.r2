scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes06]],  
      Time = 0,  
      Name = [[(Sub Tropics 06)]],  
      Season = [[winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EastEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_3]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 5,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    ShortDescription = [[]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    Road = 0,  
    TextManager = 0,  
    WayPoint = 0,  
    ActivityStep = 1,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    NpcPlant = 0,  
    Position = 0,  
    Location = 0,  
    NpcCreature = 0,  
    LogicEntityBehavior = 0,  
    NpcCustom = 0
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_6]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 25,  
      Name = [[Permanent]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_17]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_15]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[frogs III 1]],  
              Position = {
                y = -1079.109375,  
                x = 35945.85938,  
                InstanceId = [[Client1_18]],  
                Class = [[Position]],  
                z = 9.8125
              },  
              Angle = 0.1875,  
              Base = [[palette.entities.botobjects.fx_ju_grenouillec3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_21]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_19]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[rotasects I 1]],  
              Position = {
                y = -1091.75,  
                x = 35844.65625,  
                InstanceId = [[Client1_22]],  
                Class = [[Position]],  
                z = 14.078125
              },  
              Angle = -0.078125,  
              Base = [[palette.entities.botobjects.fx_ju_rotasecte]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_25]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_23]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hummingbird 1]],  
              Position = {
                y = -1028.078125,  
                x = 35833.28125,  
                InstanceId = [[Client1_26]],  
                Class = [[Position]],  
                z = 8.71875
              },  
              Angle = -1.265625,  
              Base = [[palette.entities.botobjects.fx_fo_colibrisb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_29]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_27]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              Position = {
                y = -1056,  
                x = 35881.15625,  
                InstanceId = [[Client1_30]],  
                Class = [[Position]],  
                z = -1.9375
              },  
              Angle = 0,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_33]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_31]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1051.265625,  
                x = 35868.32813,  
                InstanceId = [[Client1_34]],  
                Class = [[Position]],  
                z = -0.671875
              },  
              Angle = -1.015625,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_37]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_35]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 1]],  
              Position = {
                y = -1056.3125,  
                x = 35858.625,  
                InstanceId = [[Client1_38]],  
                Class = [[Position]],  
                z = 1.3125
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_41]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_39]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 1]],  
              Position = {
                y = -1072.625,  
                x = 35864.95313,  
                InstanceId = [[Client1_42]],  
                Class = [[Position]],  
                z = 1.5625
              },  
              Angle = 0.90625,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -1061.859375,  
                x = 35885.73438,  
                InstanceId = [[Client1_46]],  
                Class = [[Position]],  
                z = -1.828125
              },  
              Angle = -2.65625,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_49]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 1]],  
              Position = {
                y = -1070.734375,  
                x = 35889.9375,  
                InstanceId = [[Client1_50]],  
                Class = [[Position]],  
                z = -1.9375
              },  
              Angle = -2.84375,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_53]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_51]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[dead camp fire 1]],  
              Position = {
                y = -1066.53125,  
                x = 35876.15625,  
                InstanceId = [[Client1_54]],  
                Class = [[Position]],  
                z = -1.875
              },  
              Angle = -2.84375,  
              Base = [[palette.entities.botobjects.campfire_out]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_57]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_55]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1058.890625,  
                x = 35881.32813,  
                InstanceId = [[Client1_58]],  
                Class = [[Position]],  
                z = -2.03125
              },  
              Angle = -2.84375,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_61]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_59]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 1]],  
              Position = {
                y = -1055.546875,  
                x = 35880.45313,  
                InstanceId = [[Client1_62]],  
                Class = [[Position]],  
                z = -1.921875
              },  
              Angle = -2.75,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_65]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_63]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 2]],  
              Position = {
                y = -1056.84375,  
                x = 35881.39063,  
                InstanceId = [[Client1_66]],  
                Class = [[Position]],  
                z = -1.984375
              },  
              Angle = -2.75,  
              Base = [[palette.entities.botobjects.bag_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_69]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_67]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 1]],  
              Position = {
                y = -1052.453125,  
                x = 35880.75,  
                InstanceId = [[Client1_70]],  
                Class = [[Position]],  
                z = -1.5
              },  
              Angle = -2.75,  
              Base = [[palette.entities.botobjects.pack_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_73]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_71]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 2]],  
              Position = {
                y = -1051.234375,  
                x = 35877.98438,  
                InstanceId = [[Client1_74]],  
                Class = [[Position]],  
                z = -1.390625
              },  
              Angle = -2.75,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_76]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_78]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1076.53125,  
                    x = 35878.78125,  
                    InstanceId = [[Client1_79]],  
                    Class = [[Position]],  
                    z = -1.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_81]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1133.078125,  
                    x = 35881.26563,  
                    InstanceId = [[Client1_82]],  
                    Class = [[Position]],  
                    z = 5.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_84]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1174.640625,  
                    x = 35864.76563,  
                    InstanceId = [[Client1_85]],  
                    Class = [[Position]],  
                    z = 6.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_87]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1229.28125,  
                    x = 35807.0625,  
                    InstanceId = [[Client1_88]],  
                    Class = [[Position]],  
                    z = -1.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_90]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1209.859375,  
                    x = 35721.64063,  
                    InstanceId = [[Client1_91]],  
                    Class = [[Position]],  
                    z = -1.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_93]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1216.84375,  
                    x = 35620.09375,  
                    InstanceId = [[Client1_94]],  
                    Class = [[Position]],  
                    z = -3.734375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_102]],  
              Class = [[Region]],  
              Position = {
                y = 3.34375,  
                x = 4.859375,  
                InstanceId = [[Client1_101]],  
                Class = [[Position]],  
                z = -0.203125
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_104]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1044.3125,  
                    x = 35859,  
                    InstanceId = [[Client1_105]],  
                    Class = [[Position]],  
                    z = 1.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_107]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1066.828125,  
                    x = 35850.71875,  
                    InstanceId = [[Client1_108]],  
                    Class = [[Position]],  
                    z = 5.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_110]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1079.890625,  
                    x = 35856.26563,  
                    InstanceId = [[Client1_111]],  
                    Class = [[Position]],  
                    z = 6.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_113]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1084.875,  
                    x = 35871.42188,  
                    InstanceId = [[Client1_114]],  
                    Class = [[Position]],  
                    z = 0.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_116]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1079.40625,  
                    x = 35902.40625,  
                    InstanceId = [[Client1_117]],  
                    Class = [[Position]],  
                    z = -2.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_119]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1056.65625,  
                    x = 35901,  
                    InstanceId = [[Client1_120]],  
                    Class = [[Position]],  
                    z = 0.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_122]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1046.453125,  
                    x = 35886.45313,  
                    InstanceId = [[Client1_123]],  
                    Class = [[Position]],  
                    z = 0.5
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_313]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_311]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -982.3125,  
                x = 35555,  
                InstanceId = [[Client1_314]],  
                Class = [[Position]],  
                z = -0.109375
              },  
              Angle = -1.40625,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_317]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_315]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -992.921875,  
                x = 35569.40625,  
                InstanceId = [[Client1_318]],  
                Class = [[Position]],  
                z = 0.171875
              },  
              Angle = -2.453125,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_321]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_319]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[stele 1]],  
              Position = {
                y = -1010.015625,  
                x = 35561.375,  
                InstanceId = [[Client1_322]],  
                Class = [[Position]],  
                z = -0.734375
              },  
              Angle = -1.828125,  
              Base = [[palette.entities.botobjects.stele]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_325]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_323]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1013.265625,  
                x = 35543.5,  
                InstanceId = [[Client1_326]],  
                Class = [[Position]],  
                z = -0.796875
              },  
              Angle = -0.90625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_338]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_339]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 2]],  
              Position = {
                y = -986.84375,  
                x = 35611.10938,  
                InstanceId = [[Client1_340]],  
                Class = [[Position]],  
                z = 12.265625
              },  
              Angle = -1.859375,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_348]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_349]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 3]],  
              Position = {
                y = -997.796875,  
                x = 35600.5625,  
                InstanceId = [[Client1_350]],  
                Class = [[Position]],  
                z = 8.4375
              },  
              Angle = -1.859375,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_353]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_351]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[giant skull 1]],  
              Position = {
                y = -1189.890625,  
                x = 35533.5625,  
                InstanceId = [[Client1_354]],  
                Class = [[Position]],  
                z = -0.03125
              },  
              Angle = 1.140625,  
              Base = [[palette.entities.botobjects.giant_skull]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_357]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_355]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[carapace 1]],  
              Position = {
                y = -1203.578125,  
                x = 35526.375,  
                InstanceId = [[Client1_358]],  
                Class = [[Position]],  
                z = -0.640625
              },  
              Angle = 1.09375,  
              Base = [[palette.entities.botobjects.carapace_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_361]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_359]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 1]],  
              Position = {
                y = -1009.859375,  
                x = 35567.64063,  
                InstanceId = [[Client1_362]],  
                Class = [[Position]],  
                z = -0.515625
              },  
              Angle = -0.984375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_370]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_371]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 2]],  
              Position = {
                y = -1007.859375,  
                x = 35571.1875,  
                InstanceId = [[Client1_372]],  
                Class = [[Position]],  
                z = -0.203125
              },  
              Angle = -0.984375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_413]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_415]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1003.59375,  
                    x = 35575.48438,  
                    InstanceId = [[Client1_416]],  
                    Class = [[Position]],  
                    z = 0.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_418]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -986.6875,  
                    x = 35574.67188,  
                    InstanceId = [[Client1_419]],  
                    Class = [[Position]],  
                    z = 0.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_421]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -971.28125,  
                    x = 35555.89063,  
                    InstanceId = [[Client1_422]],  
                    Class = [[Position]],  
                    z = -0.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_424]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -974.875,  
                    x = 35533.84375,  
                    InstanceId = [[Client1_425]],  
                    Class = [[Position]],  
                    z = -0.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_427]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -998.65625,  
                    x = 35534.29688,  
                    InstanceId = [[Client1_428]],  
                    Class = [[Position]],  
                    z = 0.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_430]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1012.828125,  
                    x = 35536.8125,  
                    InstanceId = [[Client1_431]],  
                    Class = [[Position]],  
                    z = -0.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_433]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1020.921875,  
                    x = 35543.04688,  
                    InstanceId = [[Client1_434]],  
                    Class = [[Position]],  
                    z = -0.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_436]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1015.984375,  
                    x = 35557.95313,  
                    InstanceId = [[Client1_437]],  
                    Class = [[Position]],  
                    z = -0.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_439]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1008.859375,  
                    x = 35574.51563,  
                    InstanceId = [[Client1_440]],  
                    Class = [[Position]],  
                    z = 0.140625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_412]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_444]],  
              Class = [[Road]],  
              Points = {
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_443]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 3]],  
              InstanceId = [[Client1_465]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_464]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_467]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1051.640625,  
                    x = 35526.79688,  
                    InstanceId = [[Client1_468]],  
                    Class = [[Position]],  
                    z = 4.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_470]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1050.390625,  
                    x = 35530.60938,  
                    InstanceId = [[Client1_471]],  
                    Class = [[Position]],  
                    z = 3.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_473]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1044.78125,  
                    x = 35528.35938,  
                    InstanceId = [[Client1_474]],  
                    Class = [[Position]],  
                    z = 2.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_476]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1045.890625,  
                    x = 35524.3125,  
                    InstanceId = [[Client1_477]],  
                    Class = [[Position]],  
                    z = 4.375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_506]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_505]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_508]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1053.875,  
                    x = 35528.51563,  
                    InstanceId = [[Client1_509]],  
                    Class = [[Position]],  
                    z = 4.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_511]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1052.515625,  
                    x = 35524.73438,  
                    InstanceId = [[Client1_512]],  
                    Class = [[Position]],  
                    z = 5.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_514]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1042.90625,  
                    x = 35523.375,  
                    InstanceId = [[Client1_515]],  
                    Class = [[Position]],  
                    z = 4.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_517]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1043.171875,  
                    x = 35529.09375,  
                    InstanceId = [[Client1_518]],  
                    Class = [[Position]],  
                    z = 2.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_520]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1052.375,  
                    x = 35532.20313,  
                    InstanceId = [[Client1_521]],  
                    Class = [[Position]],  
                    z = 2.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_523]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1053.9375,  
                    x = 35528.54688,  
                    InstanceId = [[Client1_524]],  
                    Class = [[Position]],  
                    z = 4.34375
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[]],  
      ManualWeather = 0,  
      InstanceId = [[Client1_8]]
    },  
    {
      Cost = 57,  
      Behavior = {
        InstanceId = [[Client1_10]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 1,  
      Name = [[Act 1:Act 1]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 351,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_126]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 2,  
              GabaritHeight = 8,  
              HairColor = 2,  
              EyesColor = 5,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_124]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 28,  
              MorphTarget3 = 1,  
              MorphTarget7 = 6,  
              ArmModel = 0,  
              Position = {
                y = -1069.9375,  
                x = 35874.26563,  
                InstanceId = [[Client1_127]],  
                Class = [[Position]],  
                z = -1.421875
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 3,  
              JacketModel = 0,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Frerne]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 5,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_130]],  
              ActivitiesId = {
              },  
              HairType = 5623598,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 12,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 0,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 4,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 9,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_128]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 29,  
              MorphTarget3 = 3,  
              MorphTarget7 = 2,  
              ArmModel = 0,  
              Position = {
                y = -1070.828125,  
                x = 35878.51563,  
                InstanceId = [[Client1_131]],  
                Class = [[Position]],  
                z = -1.859375
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 0,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Mac'Rippsey]],  
              Sex = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 2,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_134]],  
              ActivitiesId = {
              },  
              HairType = 5623854,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_132]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 3,  
              MorphTarget3 = 1,  
              MorphTarget7 = 4,  
              ArmModel = 0,  
              Position = {
                y = -1067.421875,  
                x = 35879.84375,  
                InstanceId = [[Client1_135]],  
                Class = [[Position]],  
                z = -2
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 5618734,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Sun]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_138]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 12,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 11,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_136]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 24,  
              MorphTarget3 = 0,  
              MorphTarget7 = 6,  
              ArmModel = 0,  
              Position = {
                y = -1059.40625,  
                x = 35871.60938,  
                InstanceId = [[Client1_139]],  
                Class = [[Position]],  
                z = -1.515625
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 4,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Xados]],  
              Sex = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 4,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_142]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 9,  
              HandsModel = 5606958,  
              FeetColor = 4,  
              GabaritBreastSize = 7,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 8,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_140]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 9,  
              MorphTarget3 = 2,  
              MorphTarget7 = 2,  
              ArmModel = 5607470,  
              Position = {
                y = -1070.609375,  
                x = 35880.34375,  
                InstanceId = [[Client1_143]],  
                Class = [[Position]],  
                z = -1.984375
              },  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 5607726,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Iolaus]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 5,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_146]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 4,  
              HandsModel = 5611054,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 7,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5608750,  
              GabaritLegsWidth = 0,  
              HandsColor = 5,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_144]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 5610798,  
              Speed = 0,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.guards.m_guard_45]],  
              Tattoo = 23,  
              MorphTarget3 = 5,  
              MorphTarget7 = 5,  
              ArmModel = 5611566,  
              Position = {
                y = -1072.96875,  
                x = 35879,  
                InstanceId = [[Client1_147]],  
                Class = [[Position]],  
                z = -1.796875
              },  
              WeaponRightHand = 5598254,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 6,  
              JacketModel = 5609262,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Sirgio]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 2,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_150]],  
              ActivitiesId = {
              },  
              HairType = 6722094,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 1,  
              HandsModel = 6721326,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6722350,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_148]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 6720814,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6723630,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6765358,  
              Level = 0,  
              Name = [[Mac'Wiley]],  
              Position = {
                y = -1073.171875,  
                x = 35869.6875,  
                InstanceId = [[Client1_151]],  
                Class = [[Position]],  
                z = -0.046875
              },  
              ArmModel = 6722862,  
              MorphTarget7 = 7,  
              MorphTarget3 = 1,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_154]],  
              ActivitiesId = {
              },  
              HairType = 6732846,  
              TrouserColor = 2,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 3,  
              HandsModel = 6732078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 11,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 6733102,  
              GabaritLegsWidth = 5,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_152]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6731566,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.z_guard_245]],  
              SheetClient = [[basic_zorai_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6734382,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6772270,  
              Level = 0,  
              Name = [[Ce-Ni]],  
              Position = {
                y = -1069.8125,  
                x = 35870.625,  
                InstanceId = [[Client1_155]],  
                Class = [[Position]],  
                z = -0.625
              },  
              ArmModel = 6733614,  
              MorphTarget7 = 3,  
              MorphTarget3 = 2,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_158]],  
              ActivitiesId = {
              },  
              HairType = 6711342,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 3,  
              HandsModel = 6710830,  
              FeetColor = 5,  
              GabaritBreastSize = 2,  
              GabaritHeight = 6,  
              HairColor = 5,  
              EyesColor = 1,  
              TrouserModel = 6711854,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_156]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 6710318,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.m_guard_245]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6712878,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6761262,  
              Level = 0,  
              Name = [[Liche]],  
              Position = {
                y = -1071.671875,  
                x = 35872.25,  
                InstanceId = [[Client1_159]],  
                Class = [[Position]],  
                z = -0.875
              },  
              ArmModel = 6712110,  
              MorphTarget7 = 1,  
              MorphTarget3 = 3,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_162]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 4,  
              HandsModel = 6700078,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 14,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_160]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6756910,  
              Level = 0,  
              Name = [[Detheus]],  
              Position = {
                y = -1071.640625,  
                x = 35871,  
                InstanceId = [[Client1_163]],  
                Class = [[Position]],  
                z = -0.53125
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 5,  
              MorphTarget3 = 1,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_166]],  
              ActivitiesId = {
              },  
              HairType = 6711086,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6710830,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 12,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6711854,  
              GabaritLegsWidth = 3,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_164]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 6710062,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.m_guard_245]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6712622,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6760750,  
              Level = 0,  
              Name = [[Pechi]],  
              Position = {
                y = -1071.9375,  
                x = 35876.79688,  
                InstanceId = [[Client1_167]],  
                Class = [[Position]],  
                z = -1.6875
              },  
              ArmModel = 6712366,  
              MorphTarget7 = 4,  
              MorphTarget3 = 0,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_170]],  
              ActivitiesId = {
              },  
              HairType = 6732590,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 5,  
              HandsModel = 6732334,  
              FeetColor = 5,  
              GabaritBreastSize = 1,  
              GabaritHeight = 4,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 6733102,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_168]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 6731566,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.z_guard_245]],  
              SheetClient = [[basic_zorai_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6734382,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6770734,  
              Level = 0,  
              Name = [[Ki]],  
              Position = {
                y = -1074.265625,  
                x = 35872.57813,  
                InstanceId = [[Client1_171]],  
                Class = [[Position]],  
                z = -0.765625
              },  
              ArmModel = 6733614,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_174]],  
              ActivitiesId = {
              },  
              HairType = 6722094,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 6721582,  
              FeetColor = 5,  
              GabaritBreastSize = 7,  
              GabaritHeight = 6,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_172]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 6721070,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6723630,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6765614,  
              Level = 0,  
              Name = [[Ba'Roley]],  
              Position = {
                y = -1074,  
                x = 35874.23438,  
                InstanceId = [[Client1_175]],  
                Class = [[Position]],  
                z = -1.171875
              },  
              ArmModel = 6723118,  
              MorphTarget7 = 5,  
              MorphTarget3 = 3,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_178]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 6699822,  
              FeetColor = 2,  
              GabaritBreastSize = 2,  
              GabaritHeight = 8,  
              HairColor = 2,  
              EyesColor = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 10,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_176]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6756142,  
              Level = 0,  
              Name = [[Dynix]],  
              Position = {
                y = -1075.59375,  
                x = 35871.21875,  
                InstanceId = [[Client1_179]],  
                Class = [[Position]],  
                z = -0.296875
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 2,  
              MorphTarget3 = 0,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_182]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 11,  
              HandsModel = 6699822,  
              FeetColor = 0,  
              GabaritBreastSize = 2,  
              GabaritHeight = 13,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 14,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_180]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6755886,  
              Level = 0,  
              Name = [[Aecaon]],  
              Position = {
                y = -1075.28125,  
                x = 35875.5,  
                InstanceId = [[Client1_183]],  
                Class = [[Position]],  
                z = -1.3125
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 7,  
              MorphTarget3 = 3,  
              Tattoo = 27
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_186]],  
              ActivitiesId = {
              },  
              HairType = 6711342,  
              TrouserColor = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 3,  
              HandsModel = 6710830,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 6,  
              TrouserModel = 6711598,  
              GabaritLegsWidth = 3,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_184]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              FeetModel = 6710062,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.m_guard_245]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6712622,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6761262,  
              Level = 0,  
              Name = [[Ciochi]],  
              Position = {
                y = -1049.296875,  
                x = 35874.46875,  
                InstanceId = [[Client1_187]],  
                Class = [[Position]],  
                z = -1.015625
              },  
              ArmModel = 6712110,  
              MorphTarget7 = 1,  
              MorphTarget3 = 0,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_190]],  
              ActivitiesId = {
              },  
              HairType = 6732846,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 10,  
              HandsModel = 6732334,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6733358,  
              GabaritLegsWidth = 7,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_188]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 6731566,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.z_guard_245]],  
              SheetClient = [[basic_zorai_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6734126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6772270,  
              Level = 0,  
              Name = [[Cuo]],  
              Position = {
                y = -1076.359375,  
                x = 35870.07813,  
                InstanceId = [[Client1_191]],  
                Class = [[Position]],  
                z = 0.109375
              },  
              ArmModel = 6733614,  
              MorphTarget7 = 4,  
              MorphTarget3 = 5,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_194]],  
              ActivitiesId = {
              },  
              HairType = 6722094,  
              TrouserColor = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 12,  
              HandsModel = 6721582,  
              FeetColor = 0,  
              GabaritBreastSize = 4,  
              GabaritHeight = 12,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 13,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_192]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6721070,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6723630,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6766126,  
              Level = 0,  
              Name = [[O'Cauty]],  
              Position = {
                y = -1074.765625,  
                x = 35874.375,  
                InstanceId = [[Client1_195]],  
                Class = [[Position]],  
                z = -1.140625
              },  
              ArmModel = 6722862,  
              MorphTarget7 = 2,  
              MorphTarget3 = 5,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_198]],  
              ActivitiesId = {
              },  
              HairType = 6732590,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 1,  
              HandsModel = 6732078,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 1,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6733102,  
              GabaritLegsWidth = 11,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_196]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 1,  
              FeetModel = 6731822,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.z_guard_245]],  
              SheetClient = [[basic_zorai_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6734126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6770734,  
              Level = 0,  
              Name = [[Duai]],  
              Position = {
                y = -1070.46875,  
                x = 35875.6875,  
                InstanceId = [[Client1_199]],  
                Class = [[Position]],  
                z = -1.59375
              },  
              ArmModel = 6733870,  
              MorphTarget7 = 2,  
              MorphTarget3 = 0,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_202]],  
              ActivitiesId = {
              },  
              HairType = 6711342,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 6710830,  
              FeetColor = 3,  
              GabaritBreastSize = 5,  
              GabaritHeight = 4,  
              HairColor = 2,  
              EyesColor = 5,  
              TrouserModel = 6711854,  
              GabaritLegsWidth = 13,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_200]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 5,  
              FeetModel = 6710318,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.m_guard_245]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6712878,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6761518,  
              Level = 0,  
              Name = [[Gine]],  
              Position = {
                y = -1056.1875,  
                x = 35876.26563,  
                InstanceId = [[Client1_203]],  
                Class = [[Position]],  
                z = -1.984375
              },  
              ArmModel = 6712110,  
              MorphTarget7 = 0,  
              MorphTarget3 = 5,  
              Tattoo = 25
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_206]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 13,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 10,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_204]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6755374,  
              Level = 0,  
              Name = [[Iolus]],  
              Position = {
                y = -1067.609375,  
                x = 35872.875,  
                InstanceId = [[Client1_207]],  
                Class = [[Position]],  
                z = -1.34375
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 4,  
              MorphTarget3 = 5,  
              Tattoo = 23
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_210]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 5,  
              HandsModel = 6705454,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 4,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 3,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_208]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 6753326,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753326,  
              Level = 0,  
              Name = [[Boean]],  
              Position = {
                y = -1076.1875,  
                x = 35872.65625,  
                InstanceId = [[Client1_211]],  
                Class = [[Position]],  
                z = -0.640625
              },  
              ArmModel = 6706734,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 19
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_214]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 2,  
              HandsModel = 6705710,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 1,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_212]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 6705198,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_slash_f2.creature]],  
              WeaponRightHand = 6754606,  
              Level = 0,  
              Name = [[Zexius]],  
              Position = {
                y = -1077.15625,  
                x = 35871.375,  
                InstanceId = [[Client1_215]],  
                Class = [[Position]],  
                z = -0.234375
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 4,  
              MorphTarget3 = 5,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_218]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 11,  
              HandsModel = 6705454,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 9,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_216]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 6752302,  
              Level = 0,  
              Name = [[Icarius]],  
              Position = {
                y = -1076.046875,  
                x = 35873.89063,  
                InstanceId = [[Client1_219]],  
                Class = [[Position]],  
                z = -0.984375
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 2,  
              MorphTarget3 = 3,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_222]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 9,  
              HandsModel = 6705454,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 9,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_220]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753838,  
              Level = 0,  
              Name = [[Aeseus]],  
              Position = {
                y = -1077.84375,  
                x = 35872.89063,  
                InstanceId = [[Client1_223]],  
                Class = [[Position]],  
                z = -0.609375
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_226]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 6705454,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 1,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 5,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_224]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 0,  
              FeetModel = 6705198,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753582,  
              Level = 0,  
              Name = [[Ulyion]],  
              Position = {
                y = -1078.109375,  
                x = 35871.78125,  
                InstanceId = [[Client1_227]],  
                Class = [[Position]],  
                z = -0.296875
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 0,  
              MorphTarget3 = 5,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_230]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 11,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_228]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 6753326,  
              JacketModel = 6706990,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753326,  
              Level = 0,  
              Name = [[Xathus]],  
              Position = {
                y = -1076.890625,  
                x = 35873.15625,  
                InstanceId = [[Client1_231]],  
                Class = [[Position]],  
                z = -0.75
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 5,  
              MorphTarget3 = 2,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_234]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6705710,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 4,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_232]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 0,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753582,  
              Level = 0,  
              Name = [[Delaus]],  
              Position = {
                y = -1076.171875,  
                x = 35874.78125,  
                InstanceId = [[Client1_235]],  
                Class = [[Position]],  
                z = -1.125
              },  
              ArmModel = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 6,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_238]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 8,  
              HandsModel = 6705710,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 14,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_236]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 6,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 6753070,  
              JacketModel = 6707246,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753070,  
              Level = 0,  
              Name = [[Iorius]],  
              Position = {
                y = -1077.234375,  
                x = 35874.23438,  
                InstanceId = [[Client1_239]],  
                Class = [[Position]],  
                z = -0.953125
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 6,  
              MorphTarget3 = 7,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_242]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 3,  
              HandsModel = 6705710,  
              FeetColor = 5,  
              GabaritBreastSize = 1,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 11,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_240]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -1.28125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753838,  
              Level = 0,  
              Name = [[Iocus]],  
              Position = {
                y = -1075.921875,  
                x = 35875.875,  
                InstanceId = [[Client1_243]],  
                Class = [[Position]],  
                z = -1.34375
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 21
            },  
            {
              InstanceId = [[Client1_305]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_303]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcPlant]],  
              InheritPos = 1,  
              Name = [[Devastating Shooki]],  
              Position = {
                y = -1119.09375,  
                x = 35924.5,  
                InstanceId = [[Client1_306]],  
                Class = [[Position]],  
                z = 12.453125
              },  
              Angle = 0.8751707673,  
              Base = [[palette.entities.creatures.cpfdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_309]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_307]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Ferocious Shalah]],  
              Position = {
                y = -1147.484375,  
                x = 35846,  
                InstanceId = [[Client1_310]],  
                Class = [[Position]],  
                z = 14.265625
              },  
              Angle = 0.8751707673,  
              Base = [[palette.entities.creatures.chsdf3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_329]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_327]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -1005.125,  
                x = 35613.26563,  
                InstanceId = [[Client1_330]],  
                Class = [[Position]],  
                z = 12.59375
              },  
              Angle = -1.859375,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_500]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 8,  
              HandsModel = 6700078,  
              FeetColor = 2,  
              GabaritBreastSize = 5,  
              GabaritHeight = 14,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_498]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_502]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_503]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_504]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_465]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_525]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_506]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = 1.53125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6755374,  
              Level = 0,  
              Name = [[Pila]],  
              Position = {
                y = -1048.390625,  
                x = 35533.54688,  
                InstanceId = [[Client1_501]],  
                Class = [[Position]],  
                z = 2
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 29
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          InstanceId = [[Client1_282]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_281]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_97]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_95]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_99]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_100]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Kincherketh]],  
              Position = {
                y = -1208.296875,  
                x = 35638.45313,  
                InstanceId = [[Client1_98]],  
                Class = [[Position]],  
                z = -0.203125
              },  
              Angle = -0.09515181184,  
              Base = [[palette.entities.creatures.ckddf7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_250]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_248]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great  Kipesta]],  
              Position = {
                y = -1213.921875,  
                x = 35652.70313,  
                InstanceId = [[Client1_251]],  
                Class = [[Position]],  
                z = 0.078125
              },  
              Angle = 0.7066888809,  
              Base = [[palette.entities.creatures.ckjdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_278]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_276]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Kizarak]],  
              Position = {
                y = -1210.4375,  
                x = 35662.17188,  
                InstanceId = [[Client1_279]],  
                Class = [[Position]],  
                z = 1.046875
              },  
              Angle = 0.4912400544,  
              Base = [[palette.entities.creatures.ckcdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_254]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_252]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great  Kipucka]],  
              Position = {
                y = -1201.1875,  
                x = 35658.75,  
                InstanceId = [[Client1_255]],  
                Class = [[Position]],  
                z = 3.84375
              },  
              Angle = 0.7066888809,  
              Base = [[palette.entities.creatures.ckedf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_262]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_260]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Kincher]],  
              Position = {
                y = -1195.015625,  
                x = 35649.57813,  
                InstanceId = [[Client1_263]],  
                Class = [[Position]],  
                z = 5.578125
              },  
              Angle = 0.4912400544,  
              Base = [[palette.entities.creatures.ckddf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_246]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_244]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great  Kiban]],  
              Position = {
                y = -1224.578125,  
                x = 35641.4375,  
                InstanceId = [[Client1_247]],  
                Class = [[Position]],  
                z = -0.546875
              },  
              Angle = 0.7066888809,  
              Base = [[palette.entities.creatures.ckgdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_270]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_268]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Kirosta]],  
              Position = {
                y = -1226.40625,  
                x = 35622.48438,  
                InstanceId = [[Client1_271]],  
                Class = [[Position]],  
                z = -3.671875
              },  
              Angle = 0.4912400544,  
              Base = [[palette.entities.creatures.ckfdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_258]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_256]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Kidinak]],  
              Position = {
                y = -1220.375,  
                x = 35660.07813,  
                InstanceId = [[Client1_259]],  
                Class = [[Position]],  
                z = 0.46875
              },  
              Angle = 1.200056076,  
              Base = [[palette.entities.creatures.ckadf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_274]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_272]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Kipee]],  
              Position = {
                y = -1231.84375,  
                x = 35645.375,  
                InstanceId = [[Client1_275]],  
                Class = [[Position]],  
                z = 0.5
              },  
              Angle = 0.4912400544,  
              Base = [[palette.entities.creatures.ckhdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_266]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_264]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Kizoar]],  
              Position = {
                y = -1249.25,  
                x = 35611.1875,  
                InstanceId = [[Client1_267]],  
                Class = [[Position]],  
                z = -0.296875
              },  
              Angle = 0.4912400544,  
              Base = [[palette.entities.creatures.ckidf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_285]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_283]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Kipeeketh]],  
              Position = {
                y = -1240.03125,  
                x = 35612.78125,  
                InstanceId = [[Client1_286]],  
                Class = [[Position]],  
                z = -0.984375
              },  
              Angle = 0.3750819564,  
              Base = [[palette.entities.creatures.ckhdf7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_289]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_287]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Nymton]],  
              Position = {
                y = -1229.5625,  
                x = 35653.9375,  
                InstanceId = [[Client1_290]],  
                Class = [[Position]],  
                z = 0.640625
              },  
              Angle = 0.3750819564,  
              Base = [[palette.entities.creatures.ckfdf5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_293]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_291]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Pakuh]],  
              Position = {
                y = -1199.046875,  
                x = 35669.73438,  
                InstanceId = [[Client1_294]],  
                Class = [[Position]],  
                z = 5.40625
              },  
              Angle = 0.3750819564,  
              Base = [[palette.entities.creatures.ckedf5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_297]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_295]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Cuttleketh]],  
              Position = {
                y = -1215.40625,  
                x = 35675.15625,  
                InstanceId = [[Client1_298]],  
                Class = [[Position]],  
                z = 0.984375
              },  
              Angle = -0.5401511192,  
              Base = [[palette.entities.creatures.cckdf7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_301]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_299]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Poncha]],  
              Position = {
                y = -1208.515625,  
                x = 35676.14063,  
                InstanceId = [[Client1_302]],  
                Class = [[Position]],  
                z = 2.015625
              },  
              Angle = -0.5401511192,  
              Base = [[palette.entities.creatures.ccidf5]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_280]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_411]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_410]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_403]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 13,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 0,  
              GabaritHeight = 10,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_401]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_441]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_442]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_413]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_463]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_444]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 13,  
              MorphTarget3 = 0,  
              MorphTarget7 = 3,  
              ArmModel = 6701358,  
              Position = {
                y = -1013.765625,  
                x = 35577.57813,  
                InstanceId = [[Client1_404]],  
                Class = [[Position]],  
                z = 0.34375
              },  
              WeaponRightHand = 6755118,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              JacketModel = 6701870,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Xytis]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_388]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 3,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 2,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_389]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 2,  
              MorphTarget3 = 2,  
              MorphTarget7 = 2,  
              ArmModel = 6701614,  
              Position = {
                y = -1014.640625,  
                x = 35576.71875,  
                InstanceId = [[Client1_390]],  
                Class = [[Position]],  
                z = 0.171875
              },  
              WeaponRightHand = 6755886,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              InheritPos = 1,  
              JacketModel = 6702126,  
              Name = [[Eulion]],  
              Sex = 1,  
              WeaponLeftHand = 0,  
              ArmColor = 2,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_379]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 3,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 2,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_377]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 2,  
              MorphTarget3 = 2,  
              MorphTarget7 = 2,  
              ArmModel = 6701614,  
              Position = {
                y = -1011.34375,  
                x = 35576.67188,  
                InstanceId = [[Client1_380]],  
                Class = [[Position]],  
                z = 0.3125
              },  
              WeaponRightHand = 6755886,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 2,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Zeton]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 2,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_375]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 8,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 3,  
              GabaritHeight = 11,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_373]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 29,  
              MorphTarget3 = 6,  
              MorphTarget7 = 6,  
              ArmModel = 6701358,  
              Position = {
                y = -1013.15625,  
                x = 35575.28125,  
                InstanceId = [[Client1_376]],  
                Class = [[Position]],  
                z = 0.046875
              },  
              WeaponRightHand = 6755374,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Aekos]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 5,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_398]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 8,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 3,  
              GabaritHeight = 11,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_399]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 29,  
              MorphTarget3 = 6,  
              MorphTarget7 = 6,  
              ArmModel = 6701358,  
              Position = {
                y = -1014.578125,  
                x = 35574.25,  
                InstanceId = [[Client1_400]],  
                Class = [[Position]],  
                z = -0.125
              },  
              WeaponRightHand = 6755374,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              InheritPos = 1,  
              JacketModel = 6702126,  
              Name = [[Ioros]],  
              Sex = 1,  
              WeaponLeftHand = 0,  
              ArmColor = 5,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_407]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 3,  
              HandsModel = 6700078,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 3,  
              HairColor = 5,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 5,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_405]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 22,  
              MorphTarget3 = 1,  
              MorphTarget7 = 3,  
              ArmModel = 6701358,  
              Position = {
                y = -1015.578125,  
                x = 35575.39063,  
                InstanceId = [[Client1_408]],  
                Class = [[Position]],  
                z = -0.03125
              },  
              WeaponRightHand = 6755630,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Icaps]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 3,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_409]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_11]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[Client1_14]],  
      ManualWeather = 1,  
      InstanceId = [[Client1_12]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}