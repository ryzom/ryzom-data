scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_4798]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts07]],  
      Time = 0,  
      Name = [[Dunes of forgetfulness (Desert 07)]],  
      Season = [[fall]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_4789]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_4787]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4788]],  
    Class = [[Position]],  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    Road = 0,  
    TextManager = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ActivityStep = 1,  
    Position = 0,  
    Location = 0,  
    NpcCustom = 0,  
    LogicEntityBehavior = 0,  
    WayPoint = 0
  },  
  Description = {
    InstanceId = [[Client1_4785]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 44,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_4790]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 50,  
      InstanceId = [[Client1_4792]],  
      ManualWeather = 0,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_4791]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_4801]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4799]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              Position = {
                y = -1145.140625,  
                x = 26022.95313,  
                InstanceId = [[Client1_4802]],  
                Class = [[Position]],  
                z = 80.84375
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4805]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4803]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[giant skull 1]],  
              Position = {
                y = -1148.203125,  
                x = 26015.14063,  
                InstanceId = [[Client1_4806]],  
                Class = [[Position]],  
                z = 81
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.giant_skull]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4809]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4807]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[carapace 1]],  
              Position = {
                y = -1120.34375,  
                x = 26017.09375,  
                InstanceId = [[Client1_4810]],  
                Class = [[Position]],  
                z = 80
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.carapace_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4813]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4811]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[carapace 2]],  
              Position = {
                y = -1161.875,  
                x = 26024.10938,  
                InstanceId = [[Client1_4814]],  
                Class = [[Position]],  
                z = 82.3125
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.carapace_bul]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4817]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4815]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 1]],  
              Position = {
                y = -1158.109375,  
                x = 26011.59375,  
                InstanceId = [[Client1_4818]],  
                Class = [[Position]],  
                z = 81.5
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.bones_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4821]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4819]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 2]],  
              Position = {
                y = -1119.078125,  
                x = 25991.6875,  
                InstanceId = [[Client1_4822]],  
                Class = [[Position]],  
                z = 78.296875
              },  
              Angle = -0.5,  
              Base = [[palette.entities.botobjects.bones]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4825]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4823]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -1189.78125,  
                x = 25956.01563,  
                InstanceId = [[Client1_4826]],  
                Class = [[Position]],  
                z = 77.765625
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4834]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4835]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 2]],  
              Position = {
                y = -1175.625,  
                x = 25983.125,  
                InstanceId = [[Client1_4836]],  
                Class = [[Position]],  
                z = 83.8125
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4839]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4837]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -1177.078125,  
                x = 25999.4375,  
                InstanceId = [[Client1_4840]],  
                Class = [[Position]],  
                z = 84.90625
              },  
              Angle = 2.359375,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4843]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4841]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[stele 1]],  
              Position = {
                y = -1160.125,  
                x = 25955.1875,  
                InstanceId = [[Client1_4844]],  
                Class = [[Position]],  
                z = 78.890625
              },  
              Angle = 0.359375,  
              Base = [[palette.entities.botobjects.stele]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4847]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4845]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1252.03125,  
                x = 25981.375,  
                InstanceId = [[Client1_4848]],  
                Class = [[Position]],  
                z = 83.78125
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4860]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4861]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 2]],  
              Position = {
                y = -1186.921875,  
                x = 26018.82813,  
                InstanceId = [[Client1_4862]],  
                Class = [[Position]],  
                z = 85.375
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4870]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4871]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 3]],  
              Position = {
                y = -1185.5,  
                x = 26010.17188,  
                InstanceId = [[Client1_4872]],  
                Class = [[Position]],  
                z = 85.46875
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4875]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4873]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 1]],  
              Position = {
                y = -1175.0625,  
                x = 25989.01563,  
                InstanceId = [[Client1_4876]],  
                Class = [[Position]],  
                z = 84.140625
              },  
              Angle = 0.96875,  
              Base = [[palette.entities.botobjects.bag_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4879]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4877]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 2]],  
              Position = {
                y = -1175.796875,  
                x = 25989.17188,  
                InstanceId = [[Client1_4880]],  
                Class = [[Position]],  
                z = 84.3125
              },  
              Angle = 0.96875,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4883]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4881]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 1]],  
              Position = {
                y = -1176.96875,  
                x = 25998.20313,  
                InstanceId = [[Client1_4884]],  
                Class = [[Position]],  
                z = 84.828125
              },  
              Angle = 0.96875,  
              Base = [[palette.entities.botobjects.pack_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4887]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4885]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 2]],  
              Position = {
                y = -1162.09375,  
                x = 25959.625,  
                InstanceId = [[Client1_4888]],  
                Class = [[Position]],  
                z = 78.28125
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.botobjects.pack_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4891]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4889]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 3]],  
              Position = {
                y = -1188.6875,  
                x = 25965.28125,  
                InstanceId = [[Client1_4892]],  
                Class = [[Position]],  
                z = 78.578125
              },  
              Angle = 1.015625,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4895]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4893]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 4]],  
              Position = {
                y = -1182.21875,  
                x = 26017.28125,  
                InstanceId = [[Client1_4896]],  
                Class = [[Position]],  
                z = 85.453125
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.pack_4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4899]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4897]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 5]],  
              Position = {
                y = -1183.578125,  
                x = 26024.54688,  
                InstanceId = [[Client1_4900]],  
                Class = [[Position]],  
                z = 85.09375
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.pack_5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4908]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4909]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 6]],  
              Position = {
                y = -1183.390625,  
                x = 26030.07813,  
                InstanceId = [[Client1_4910]],  
                Class = [[Position]],  
                z = 84.625
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.pack_5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4913]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4911]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1169.84375,  
                x = 26025.5,  
                InstanceId = [[Client1_4914]],  
                Class = [[Position]],  
                z = 83.1875
              },  
              Angle = 0.9375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4917]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4915]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -1173.5,  
                x = 26016.79688,  
                InstanceId = [[Client1_4918]],  
                Class = [[Position]],  
                z = 84.484375
              },  
              Angle = 0.9375,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4921]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4919]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chest 1]],  
              Position = {
                y = -1172.1875,  
                x = 26025.51563,  
                InstanceId = [[Client1_4922]],  
                Class = [[Position]],  
                z = 83.546875
              },  
              Angle = 0.9375,  
              Base = [[palette.entities.botobjects.chest]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4925]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4923]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 1]],  
              Position = {
                y = -1185.234375,  
                x = 26036.98438,  
                InstanceId = [[Client1_4926]],  
                Class = [[Position]],  
                z = 83.9375
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4929]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4927]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[dead camp fire 1]],  
              Position = {
                y = -1196.84375,  
                x = 26074.8125,  
                InstanceId = [[Client1_4930]],  
                Class = [[Position]],  
                z = 75.296875
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.campfire_out]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4933]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4931]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1181.796875,  
                x = 26019.90625,  
                InstanceId = [[Client1_4934]],  
                Class = [[Position]],  
                z = 85.28125
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4937]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4935]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire base 1]],  
              Position = {
                y = -1177.890625,  
                x = 26010.67188,  
                InstanceId = [[Client1_4938]],  
                Class = [[Position]],  
                z = 85.15625
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.fire_base]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4941]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4939]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[jar 1]],  
              Position = {
                y = -1172.40625,  
                x = 26016.01563,  
                InstanceId = [[Client1_4942]],  
                Class = [[Position]],  
                z = 84.359375
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.jar]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4945]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4943]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[old chest 1]],  
              Position = {
                y = -1171.875,  
                x = 26028.79688,  
                InstanceId = [[Client1_4946]],  
                Class = [[Position]],  
                z = 82.921875
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.chest_old]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4949]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4947]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1177.671875,  
                x = 26006.0625,  
                InstanceId = [[Client1_4950]],  
                Class = [[Position]],  
                z = 85.109375
              },  
              Angle = 0.578125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4958]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4959]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              Position = {
                y = -1172.265625,  
                x = 25990.10938,  
                InstanceId = [[Client1_4960]],  
                Class = [[Position]],  
                z = 83.421875
              },  
              Angle = 0.578125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4963]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4961]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[working chariot 1]],  
              Position = {
                y = -1148.953125,  
                x = 26026.92188,  
                InstanceId = [[Client1_4964]],  
                Class = [[Position]],  
                z = 80.453125
              },  
              Angle = 0.484375,  
              Base = [[palette.entities.botobjects.chariot_working]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_5050]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5052]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5053]],  
                    x = 26053,  
                    y = -1121.359375,  
                    z = 72.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5055]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5056]],  
                    x = 26061.71875,  
                    y = -1144.25,  
                    z = 70.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5058]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5059]],  
                    x = 26070.4375,  
                    y = -1186.75,  
                    z = 67.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5061]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1230.234375,  
                    x = 26060.73438,  
                    InstanceId = [[Client1_5062]],  
                    Class = [[Position]],  
                    z = 72.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5064]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5065]],  
                    x = 26032.29688,  
                    y = -1269.59375,  
                    z = 68.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5285]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5286]],  
                    x = 25979.98438,  
                    y = -1278.3125,  
                    z = 76.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5067]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5068]],  
                    x = 25920.03125,  
                    y = -1260.859375,  
                    z = 70.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5483]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5484]],  
                    x = 25857.92188,  
                    y = -1210.734375,  
                    z = 65.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5486]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5487]],  
                    x = 25872.09375,  
                    y = -1119.1875,  
                    z = 70.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5070]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5071]],  
                    x = 25946.20313,  
                    y = -1076.671875,  
                    z = 73.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5073]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5074]],  
                    x = 25984.34375,  
                    y = -1078.859375,  
                    z = 64.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5076]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5077]],  
                    x = 26022.48438,  
                    y = -1098.46875,  
                    z = 72.03125
                  }
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5049]],  
                x = 37.359375,  
                y = 1.78125,  
                z = 4.859375
              }
            },  
            {
              InstanceId = [[Client1_5258]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5256]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bothaya I 1]],  
              Position = {
                y = -1220.984375,  
                x = 26056.23438,  
                InstanceId = [[Client1_5259]],  
                Class = [[Position]],  
                z = 70.71875
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5262]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5260]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bothaya growth I 1]],  
              Position = {
                y = -1223.6875,  
                x = 26037.9375,  
                InstanceId = [[Client1_5263]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_growth_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5266]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5264]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bothaya growth II 1]],  
              Position = {
                y = -1233.0625,  
                x = 26042.51563,  
                InstanceId = [[Client1_5267]],  
                Class = [[Position]],  
                z = 75.40625
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_growth_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5270]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5268]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga I 1]],  
              Position = {
                y = -1166.59375,  
                x = 26074,  
                InstanceId = [[Client1_5271]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5274]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5272]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga II 1]],  
              Position = {
                y = -1195.28125,  
                x = 26045.5625,  
                InstanceId = [[Client1_5275]],  
                Class = [[Position]],  
                z = 80.796875
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5278]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5276]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga III 1]],  
              Position = {
                y = -1193.484375,  
                x = 26095.10938,  
                InstanceId = [[Client1_5279]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5282]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5280]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[loojine I 1]],  
              Position = {
                y = -1202.640625,  
                x = 26051.46875,  
                InstanceId = [[Client1_5283]],  
                Class = [[Position]],  
                z = 75.6875
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.fy_s2_lovejail_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5289]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5287]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[runic circle 1]],  
              Position = {
                y = -1217.8125,  
                x = 25996.17188,  
                InstanceId = [[Client1_5290]],  
                Class = [[Position]],  
                z = 69.3125
              },  
              Angle = -1.703125,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5476]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5474]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami hut 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5477]],  
                x = 26018.79688,  
                y = -1233.03125,  
                z = 74.875
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5480]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5478]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami standard 1]],  
              Position = {
                y = -1227.375,  
                x = 26029.34375,  
                InstanceId = [[Client1_5481]],  
                Class = [[Position]],  
                z = 75.4375
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.botobjects.kami_standard]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5522]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5520]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              Position = {
                y = -1135.40625,  
                x = 26043.6875,  
                InstanceId = [[Client1_5523]],  
                Class = [[Position]],  
                z = 80.71875
              },  
              Angle = -3,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5526]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5524]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living gateway 1]],  
              Position = {
                y = -1107.546875,  
                x = 25955.90625,  
                InstanceId = [[Client1_5527]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = -1.296875,  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5530]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5528]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 1]],  
              Position = {
                y = -1141.125,  
                x = 25983.60938,  
                InstanceId = [[Client1_5531]],  
                Class = [[Position]],  
                z = 73.125
              },  
              Angle = -1.984375,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5534]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5532]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami hut 2]],  
              Position = {
                y = -1114.640625,  
                x = 25944.01563,  
                InstanceId = [[Client1_5535]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = -0.40625,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5604]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5602]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 2]],  
              Position = {
                y = -1148.734375,  
                x = 25951.71875,  
                InstanceId = [[Client1_5605]],  
                Class = [[Position]],  
                z = 80
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5617]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5618]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 5]],  
              Position = {
                y = -1114.625,  
                x = 25994.96875,  
                InstanceId = [[Client1_5619]],  
                Class = [[Position]],  
                z = 79.625
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5627]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5628]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 6]],  
              Position = {
                y = -1112.0625,  
                x = 25987.04688,  
                InstanceId = [[Client1_5629]],  
                Class = [[Position]],  
                z = 79.34375
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_5853]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5855]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5856]],  
                    x = 25975.71875,  
                    y = -1129.125,  
                    z = 73.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5858]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5859]],  
                    x = 25987.25,  
                    y = -1141.328125,  
                    z = 73.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5861]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5862]],  
                    x = 26020.0625,  
                    y = -1134.4375,  
                    z = 79.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5864]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5865]],  
                    x = 26053.34375,  
                    y = -1121.671875,  
                    z = 83.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5867]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5868]],  
                    x = 26088.70313,  
                    y = -1139.609375,  
                    z = 78.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5870]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5871]],  
                    x = 26090.79688,  
                    y = -1167.515625,  
                    z = 74.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5873]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5874]],  
                    x = 26085.65625,  
                    y = -1189.90625,  
                    z = 74.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5876]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5877]],  
                    x = 26055.4375,  
                    y = -1233.359375,  
                    z = 72.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5879]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5880]],  
                    x = 26041.5625,  
                    y = -1246.734375,  
                    z = 76.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5882]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5883]],  
                    x = 26003.78125,  
                    y = -1249.953125,  
                    z = 76.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5885]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5886]],  
                    x = 25972.32813,  
                    y = -1222.796875,  
                    z = 78
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5888]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5889]],  
                    x = 25940.25,  
                    y = -1178.515625,  
                    z = 77.484375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5891]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5892]],  
                    x = 25943.89063,  
                    y = -1121.125,  
                    z = 75.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5894]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5895]],  
                    x = 25970.59375,  
                    y = -1130.875,  
                    z = 74.171875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5852]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_4793]],  
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 65,  
      Behavior = {
        InstanceId = [[Client1_4794]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 3,  
      InstanceId = [[Client1_4796]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 325,  
      LocationId = [[Client1_4798]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_4795]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_4851]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4849]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4852]],  
                x = 26028.17188,  
                y = -1188.203125,  
                z = 84.6875
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_4967]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4965]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5083]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5084]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              Level = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Kridix]],  
              Position = {
                y = -1176.8125,  
                x = 26028.34375,  
                InstanceId = [[Client1_4968]],  
                Class = [[Position]],  
                z = 83.875
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_4976]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4977]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5085]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5086]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Abyros]],  
              Position = {
                y = -1176.625,  
                x = 26026.21875,  
                InstanceId = [[Client1_4978]],  
                Class = [[Position]],  
                z = 84.125
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_4986]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4987]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5081]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5082]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Aethus]],  
              Position = {
                y = -1179.390625,  
                x = 26033.59375,  
                InstanceId = [[Client1_4988]],  
                Class = [[Position]],  
                z = 83.515625
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_4996]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4997]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5078]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5079]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Diorius]],  
              Position = {
                y = -1171.578125,  
                x = 26031.60938,  
                InstanceId = [[Client1_4998]],  
                Class = [[Position]],  
                z = 82.234375
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_5006]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5007]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5087]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5088]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Dyxius]],  
              Position = {
                y = -1173.484375,  
                x = 26022.60938,  
                InstanceId = [[Client1_5008]],  
                Class = [[Position]],  
                z = 84.140625
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_5016]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5017]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5047]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_5048]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5093]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Iosse]],  
              Position = {
                y = -1167.734375,  
                x = 26029.5625,  
                InstanceId = [[Client1_5018]],  
                Class = [[Position]],  
                z = 81.9375
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_5026]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5027]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5089]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5090]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Melus]],  
              Position = {
                y = -1169.21875,  
                x = 26021.35938,  
                InstanceId = [[Client1_5028]],  
                Class = [[Position]],  
                z = 83.703125
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_5036]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5037]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5091]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5092]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 0.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Zelus]],  
              Position = {
                y = -1166.515625,  
                x = 26022.35938,  
                InstanceId = [[Client1_5038]],  
                Class = [[Position]],  
                z = 83.25
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5096]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5094]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5098]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5099]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              Sex = 1,  
              ArmColor = 2,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Sirgia]],  
              Position = {
                y = -1210.5,  
                x = 26014.64063,  
                InstanceId = [[Client1_5097]],  
                Class = [[Position]],  
                z = 73.34375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5109]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5110]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5111]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5112]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Miaero]],  
              Position = {
                y = -1212.015625,  
                x = 26008.82813,  
                InstanceId = [[Client1_5113]],  
                Class = [[Position]],  
                z = 71.734375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5123]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5124]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5125]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5126]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Ciochi]],  
              Position = {
                y = -1214,  
                x = 26015.9375,  
                InstanceId = [[Client1_5127]],  
                Class = [[Position]],  
                z = 72.53125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5137]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5138]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5139]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5140]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Pegio]],  
              Position = {
                y = -1207.5,  
                x = 26009.07813,  
                InstanceId = [[Client1_5141]],  
                Class = [[Position]],  
                z = 73.6875
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5151]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5152]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5153]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5154]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Chiabre]],  
              Position = {
                y = -1206,  
                x = 26013.34375,  
                InstanceId = [[Client1_5155]],  
                Class = [[Position]],  
                z = 75.28125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5165]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5166]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5167]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5168]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Ciogio]],  
              Position = {
                y = -1216.25,  
                x = 26020.375,  
                InstanceId = [[Client1_5169]],  
                Class = [[Position]],  
                z = 73.078125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5179]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5180]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5181]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5182]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Roero]],  
              Position = {
                y = -1203.921875,  
                x = 26002.95313,  
                InstanceId = [[Client1_5183]],  
                Class = [[Position]],  
                z = 74.609375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5193]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5194]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5195]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5196]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Lirni]],  
              Position = {
                y = -1202.125,  
                x = 26006.92188,  
                InstanceId = [[Client1_5197]],  
                Class = [[Position]],  
                z = 76.484375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5207]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5208]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5209]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5210]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Niro]],  
              Position = {
                y = -1202.015625,  
                x = 26017.125,  
                InstanceId = [[Client1_5211]],  
                Class = [[Position]],  
                z = 78.171875
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_5251]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5252]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5253]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5254]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.953125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Ciosse]],  
              Position = {
                y = -1206.671875,  
                x = 26020.10938,  
                InstanceId = [[Client1_5255]],  
                Class = [[Position]],  
                z = 75.90625
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5293]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5291]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5295]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5296]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              Sex = 0,  
              ArmColor = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Be'Ledacan]],  
              Position = {
                y = -1212.15625,  
                x = 26080.70313,  
                InstanceId = [[Client1_5294]],  
                Class = [[Position]],  
                z = 75.640625
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5306]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5307]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5308]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5309]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Ba'Neppy]],  
              Position = {
                y = -1216.28125,  
                x = 26081.64063,  
                InstanceId = [[Client1_5310]],  
                Class = [[Position]],  
                z = 75.515625
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5320]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5321]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5322]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5323]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Mac'Wiley]],  
              Position = {
                y = -1211.28125,  
                x = 26088.4375,  
                InstanceId = [[Client1_5324]],  
                Class = [[Position]],  
                z = 75.8125
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5334]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5335]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5336]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5337]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[O'Arty]],  
              Position = {
                y = -1200.75,  
                x = 26083.125,  
                InstanceId = [[Client1_5338]],  
                Class = [[Position]],  
                z = 75.53125
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5348]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5349]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5350]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5351]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Mac'darrroy]],  
              Position = {
                y = -1207.515625,  
                x = 26085.78125,  
                InstanceId = [[Client1_5352]],  
                Class = [[Position]],  
                z = 75.796875
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5362]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5363]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5364]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5365]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Be'Reier]],  
              Position = {
                y = -1220.546875,  
                x = 26076.82813,  
                InstanceId = [[Client1_5366]],  
                Class = [[Position]],  
                z = 74.8125
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5376]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5377]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5378]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5379]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Mac'Laughan]],  
              Position = {
                y = -1204.4375,  
                x = 26075.82813,  
                InstanceId = [[Client1_5380]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5390]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5391]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5392]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5393]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Ba'Massey]],  
              Position = {
                y = -1214.375,  
                x = 26072.92188,  
                InstanceId = [[Client1_5394]],  
                Class = [[Position]],  
                z = 74.25
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5404]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5405]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5406]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5407]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Be'Daghan]],  
              Position = {
                y = -1218.359375,  
                x = 26091.29688,  
                InstanceId = [[Client1_5408]],  
                Class = [[Position]],  
                z = 75.625
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5418]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5419]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5420]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5421]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Mac'Nary]],  
              Position = {
                y = -1224.09375,  
                x = 26087.5625,  
                InstanceId = [[Client1_5422]],  
                Class = [[Position]],  
                z = 75.34375
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_5465]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5466]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5467]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5468]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Ba'dughan]],  
              Position = {
                y = -1225.953125,  
                x = 26082.79688,  
                InstanceId = [[Client1_5469]],  
                Class = [[Position]],  
                z = 75.15625
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              InstanceId = [[Client1_5472]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5470]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami banner 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5473]],  
                x = 26020.5,  
                y = -1231.484375,  
                z = 74.9375
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.botobjects.banner_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5490]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5488]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5504]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5505]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ardent Genius 1]],  
              Position = {
                y = -1230.796875,  
                x = 26014.51563,  
                InstanceId = [[Client1_5491]],  
                Class = [[Position]],  
                z = 73.625
              },  
              Angle = 1.265625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5494]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5492]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5502]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5503]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ardent Genius 2]],  
              Position = {
                y = -1230.546875,  
                x = 26008.07813,  
                InstanceId = [[Client1_5495]],  
                Class = [[Position]],  
                z = 72.734375
              },  
              Angle = 1.265625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5498]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5496]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5500]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5501]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burning Salamander 1]],  
              Position = {
                y = -1229.21875,  
                x = 26012.57813,  
                InstanceId = [[Client1_5499]],  
                Class = [[Position]],  
                z = 72.984375
              },  
              Angle = 1.265625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5508]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5506]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5510]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5511]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Diaphanous Salamander 1]],  
              Position = {
                y = -1211.640625,  
                x = 26027.28125,  
                InstanceId = [[Client1_5509]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              Angle = 1.265625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_3_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5514]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5512]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 1]],  
              Position = {
                y = -1228.1875,  
                x = 26029.60938,  
                InstanceId = [[Client1_5515]],  
                Class = [[Position]],  
                z = 75.609375
              },  
              Angle = 1.265625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5518]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5516]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 1]],  
              Position = {
                y = -1229.796875,  
                x = 26033.9375,  
                InstanceId = [[Client1_5519]],  
                Class = [[Position]],  
                z = 76.015625
              },  
              Angle = 1.625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5538]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5536]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5540]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5541]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spectral Genius 1]],  
              Position = {
                y = -1132.109375,  
                x = 25962.70313,  
                InstanceId = [[Client1_5539]],  
                Class = [[Position]],  
                z = 75.578125
              },  
              Angle = -0.40625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5608]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5606]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 4]],  
              Position = {
                y = -1117.65625,  
                x = 26001.25,  
                InstanceId = [[Client1_5609]],  
                Class = [[Position]],  
                z = 79.671875
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_4797]],  
        },  
        {
          InstanceId = [[Client1_5574]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_5573]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_5572]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_5544]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5542]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5566]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5567]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 1]],  
              Position = {
                y = -1137.609375,  
                x = 25966.15625,  
                InstanceId = [[Client1_5545]],  
                Class = [[Position]],  
                z = 75.640625
              },  
              Angle = -0.40625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5563]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5564]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5570]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5571]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 3]],  
              Position = {
                y = -1136.59375,  
                x = 25964.25,  
                InstanceId = [[Client1_5565]],  
                Class = [[Position]],  
                z = 75.984375
              },  
              Angle = -0.40625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5553]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5554]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5568]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5569]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 2]],  
              Position = {
                y = -1138.703125,  
                x = 25964.71875,  
                InstanceId = [[Client1_5555]],  
                Class = [[Position]],  
                z = 76.1875
              },  
              Angle = -0.40625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_5601]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_5600]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_5599]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_5597]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5595]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5630]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5631]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 2]],  
              Position = {
                y = -1131.015625,  
                x = 25987.6875,  
                InstanceId = [[Client1_5598]],  
                Class = [[Position]],  
                z = 74.5
              },  
              Angle = -0.59375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5577]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5575]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5579]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5580]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 2]],  
              Position = {
                y = -1128.421875,  
                x = 25982.15625,  
                InstanceId = [[Client1_5578]],  
                Class = [[Position]],  
                z = 74.015625
              },  
              Angle = -0.59375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_5590]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5591]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5592]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_5593]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5050]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 3]],  
              Position = {
                y = -1125.546875,  
                x = 25985.39063,  
                InstanceId = [[Client1_5594]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = -0.59375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_5851]],  
          Name = [[Group 3]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5847]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5848]],  
                x = 25974.07813,  
                y = -1125.4375,  
                z = 74.171875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5845]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5899]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5900]],  
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5853]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 0,  
              Tattoo = 10,  
              EyesColor = 3,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5619246,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5636654,  
              WeaponLeftHand = 0,  
              Name = [[Chi]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5712]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5714]],  
                x = 25976.40625,  
                y = -1120.75,  
                z = 75.203125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5713]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Hiang]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5634]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5635]],  
                x = 25974.28125,  
                y = -1121.5625,  
                z = 74.875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5632]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5603630,  
              WeaponLeftHand = 0,  
              Name = [[Ja-Zun]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5682]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5684]],  
                x = 25972.64063,  
                y = -1122.484375,  
                z = 74.65625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5683]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Shuai-Chon]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5692]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5694]],  
                x = 25971.09375,  
                y = -1123.84375,  
                z = 74.46875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5693]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Ki]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5702]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5704]],  
                x = 25969.4375,  
                y = -1125.109375,  
                z = 74.390625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5703]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Tei-Woo]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5722]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5724]],  
                x = 25968.17188,  
                y = -1127,  
                z = 74.40625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5723]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Vo-Nuang]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5732]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5734]],  
                x = 25966.23438,  
                y = -1125.25,  
                z = 74.609375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5733]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Tao]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5742]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5744]],  
                x = 25967.42188,  
                y = -1123.09375,  
                z = 74.625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5743]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Kungi]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5752]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5754]],  
                x = 25969.25,  
                y = -1121.90625,  
                z = 74.734375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5753]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Hen]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5762]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5764]],  
                x = 25971.21875,  
                y = -1120.5625,  
                z = 75
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5763]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Nao]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5772]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5774]],  
                x = 25973.07813,  
                y = -1119.21875,  
                z = 75.375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5773]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Liangi]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5782]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5784]],  
                x = 25975.375,  
                y = -1118.703125,  
                z = 75.671875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5783]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Geng]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5792]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5794]],  
                x = 25973.875,  
                y = -1115.875,  
                z = 76.265625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5793]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Cuo]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5802]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5804]],  
                x = 25971.3125,  
                y = -1116.296875,  
                z = 75.875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5803]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Vao]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5812]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5814]],  
                x = 25969.0625,  
                y = -1117.296875,  
                z = 75.484375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5813]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Ba-Nung]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5822]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5824]],  
                x = 25967.07813,  
                y = -1119,  
                z = 75.09375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5823]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Chaoi]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5832]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5834]],  
                x = 25964.625,  
                y = -1119.90625,  
                z = 74.984375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5833]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Fuan]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_5842]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5844]],  
                x = 25963.25,  
                y = -1122.375,  
                z = 74.828125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_5843]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5603630,  
              Name = [[Dao]],  
              Level = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_5850]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_5849]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_4786]]
  }
}