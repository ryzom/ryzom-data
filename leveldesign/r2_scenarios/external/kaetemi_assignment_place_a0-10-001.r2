scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_8]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 21,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_10]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    ActivityStep = 0,  
    NpcCustom = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_215]],  
        [[Client1_380]],  
        [[Client1_382]],  
        [[Client1_390]],  
        [[Client1_396]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_33]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_34]],  
                x = 26933.98438,  
                y = -2023.703125,  
                z = -1.578125
              },  
              Angle = 1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_31]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Circle Thingy]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_37]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_38]],  
                x = 26940.89063,  
                y = -2023.21875,  
                z = -1.625
              },  
              Angle = 1.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_35]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Circle Bones]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_41]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_42]],  
                x = 26933.8125,  
                y = -2023.640625,  
                z = -1.65625
              },  
              Angle = -1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_39]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_45]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_46]],  
                x = 26880.67188,  
                y = -1999.0625,  
                z = -10.8125
              },  
              Angle = 0.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_43]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_53]],  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_54]],  
                x = 27074.79688,  
                y = -2031.46875,  
                z = 14.5
              },  
              Angle = 2.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_51]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_57]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_58]],  
                x = 26977.71875,  
                y = -1957.875,  
                z = -3.125
              },  
              Angle = 2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_55]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_61]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_62]],  
                x = 26977.67188,  
                y = -1963.703125,  
                z = -5.828125
              },  
              Angle = -3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_59]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_65]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_66]],  
                x = 26971.5,  
                y = -1958.703125,  
                z = -4.765625
              },  
              Angle = 2.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_63]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_69]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_70]],  
                x = 26973.26563,  
                y = -1964.5,  
                z = -7.21875
              },  
              Angle = -2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_67]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_73]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_74]],  
                x = 27005.5625,  
                y = -2040.25,  
                z = -5.953125
              },  
              Angle = 1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_71]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_77]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_78]],  
                x = 27006.5625,  
                y = -2048.53125,  
                z = -9.15625
              },  
              Angle = 2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_75]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_81]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_82]],  
                x = 27000.84375,  
                y = -2041.796875,  
                z = -8.59375
              },  
              Angle = 2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_79]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_85]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_86]],  
                x = 26970.32813,  
                y = -1973.109375,  
                z = -9.515625
              },  
              Angle = -3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_83]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_89]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_90]],  
                x = 26970.89063,  
                y = -1978.234375,  
                z = -8.953125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_87]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_93]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_94]],  
                x = 26971.34375,  
                y = -1983.578125,  
                z = -8.171875
              },  
              Angle = -3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_91]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_97]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_98]],  
                x = 26972.46875,  
                y = -1988.578125,  
                z = -7.546875
              },  
              Angle = -5.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_95]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_101]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_102]],  
                x = 26974.25,  
                y = -1993.703125,  
                z = -6.734375
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_99]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_105]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_106]],  
                x = 26976.32813,  
                y = -1998.703125,  
                z = -5.65625
              },  
              Angle = -5.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_103]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_109]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_110]],  
                x = 26978.70313,  
                y = -2003.546875,  
                z = -4.3125
              },  
              Angle = -2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_107]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_113]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_114]],  
                x = 26981.3125,  
                y = -2008.015625,  
                z = -3.0625
              },  
              Angle = -5.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_111]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_117]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_118]],  
                x = 26984.46875,  
                y = -2012.265625,  
                z = -2.140625
              },  
              Angle = -2.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_115]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_121]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_122]],  
                x = 26987.75,  
                y = -2016.3125,  
                z = -1.75
              },  
              Angle = -5.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_119]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_125]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_126]],  
                x = 26990.96875,  
                y = -2020.21875,  
                z = -2.546875
              },  
              Angle = -2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_123]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 11]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_129]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_130]],  
                x = 26994.32813,  
                y = -2024.140625,  
                z = -4.375
              },  
              Angle = -5.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_127]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 12]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_133]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_134]],  
                x = 26997.57813,  
                y = -2028.15625,  
                z = -5.859375
              },  
              Angle = -2.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_131]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 13]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_137]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_138]],  
                x = 27000.75,  
                y = -2032.140625,  
                z = -6.0625
              },  
              Angle = -5.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_135]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 14]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_140]],  
              Name = [[Guard Zone]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_142]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_143]],  
                    x = 26971.53125,  
                    y = -1973.046875,  
                    z = -9.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_145]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_146]],  
                    x = 26984.1875,  
                    y = -1969.265625,  
                    z = -6.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_148]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_149]],  
                    x = 27023.07813,  
                    y = -2026.4375,  
                    z = 4.203125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_151]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_152]],  
                    x = 27003.48438,  
                    y = -2032.265625,  
                    z = -4.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_154]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_155]],  
                    x = 26989.59375,  
                    y = -2015.15625,  
                    z = -1.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_157]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_158]],  
                    x = 26977.79688,  
                    y = -1994.75,  
                    z = -5.8125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_139]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_252]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_253]],  
                x = 26976.625,  
                y = -1973.203125,  
                z = -9.1875
              },  
              Angle = -2.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_250]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_256]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_257]],  
                x = 27004.48438,  
                y = -2028.375,  
                z = -3.171875
              },  
              Angle = 2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_254]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kami watchtower 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_272]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_273]],  
                x = 26960.34375,  
                y = -1918.140625,  
                z = -10.578125
              },  
              Angle = -1.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_270]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[goo spot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_276]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_277]],  
                x = 26952.71875,  
                y = -1920.5625,  
                z = -10.1875
              },  
              Angle = -1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_274]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[goo smoke 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_280]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_281]],  
                x = 26970.5,  
                y = -1918.671875,  
                z = -9.390625
              },  
              Angle = -2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_278]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[goo smoke 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_284]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_285]],  
                x = 26948.76563,  
                y = -1909.21875,  
                z = -7.3125
              },  
              Angle = -1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_282]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_288]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_289]],  
                x = 26973.17188,  
                y = -1909.375,  
                z = -7.578125
              },  
              Angle = -2.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_286]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_292]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_293]],  
                x = 26959.78125,  
                y = -1907.34375,  
                z = -7.65625
              },  
              Angle = -1.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_290]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_314]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_316]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_317]],  
                    x = 26943.21875,  
                    y = -2015.03125,  
                    z = -3.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_319]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_320]],  
                    x = 26954.79688,  
                    y = -1995.5,  
                    z = -7.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_322]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_323]],  
                    x = 26954.5,  
                    y = -1970.984375,  
                    z = -7.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_325]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_326]],  
                    x = 26910.78125,  
                    y = -1980.9375,  
                    z = -6.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_328]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_329]],  
                    x = 26908.15625,  
                    y = -2007.53125,  
                    z = -7.984375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_313]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_332]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_333]],  
                x = 26951.89063,  
                y = -2010.578125,  
                z = -7.65625
              },  
              Angle = 2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_330]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_336]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_337]],  
                x = 26956.46875,  
                y = -2001.140625,  
                z = -8.203125
              },  
              Angle = -2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_334]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_340]],  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_341]],  
                x = 26950.14063,  
                y = -2004.484375,  
                z = -7.765625
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_338]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6705198,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 0,  
              Name = [[Tribe Leader]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client1_12]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_185]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_169]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_170]],  
                x = 26996.59375,  
                y = -2000.03125,  
                z = 0.484375
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_390]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_395]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_140]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6756910,  
              Name = [[Guard Leader]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_294]]
              },  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_181]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_182]],  
                x = 26995.0625,  
                y = -2002.515625,  
                z = 0.0625
              },  
              Angle = 0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_179]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6717998,  
              TrouserModel = 6716974,  
              FeetModel = 6715950,  
              HandsModel = 6716462,  
              ArmModel = 6717486,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6758446,  
              Name = [[Guard]],  
              WeaponLeftHand = 6758446,  
              ActivitiesId = {
              },  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_177]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_178]],  
                x = 26996.0625,  
                y = -2002.359375,  
                z = 0.34375
              },  
              Angle = 1.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_175]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6717998,  
              TrouserModel = 6716974,  
              FeetModel = 6715950,  
              HandsModel = 6716462,  
              ArmModel = 6717486,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6758446,  
              Name = [[Guard]],  
              WeaponLeftHand = 6758446,  
              ActivitiesId = {
              },  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_173]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_174]],  
                x = 26996.54688,  
                y = -2002.640625,  
                z = 0.453125
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6717998,  
              TrouserModel = 6716974,  
              FeetModel = 6715950,  
              HandsModel = 6716462,  
              ArmModel = 6717486,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6758446,  
              Name = [[Guard]],  
              WeaponLeftHand = 6758446,  
              ActivitiesId = {
              },  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          ActivitiesId = {
            [[Client1_390]]
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_184]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_183]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_247]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_243]],  
              Base = [[palette.entities.creatures.ckdie4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_244]],  
                x = 27062.51563,  
                y = -2021.375,  
                z = 15.265625
              },  
              Angle = 1.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_241]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_396]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_397]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_140]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              ActivitiesId = {
                [[Client1_296]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_219]],  
              Base = [[palette.entities.creatures.ckdie4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_220]],  
                x = 27057.71875,  
                y = -2058.4375,  
                z = 4.53125
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_217]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_227]],  
              Base = [[palette.entities.creatures.ckdie4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_228]],  
                x = 27062.75,  
                y = -2050.265625,  
                z = 10.15625
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_225]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_231]],  
              Base = [[palette.entities.creatures.ckdie4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_232]],  
                x = 27058.375,  
                y = -2042.0625,  
                z = 14.25
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_229]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_235]],  
              Base = [[palette.entities.creatures.ckdie4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_236]],  
                x = 27060.71875,  
                y = -2035.671875,  
                z = 14.90625
              },  
              Angle = 2.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_233]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_239]],  
              Base = [[palette.entities.creatures.ckdie4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_240]],  
                x = 27060.09375,  
                y = -2029.5,  
                z = 14.859375
              },  
              Angle = 1.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_237]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
            [[Client1_396]]
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_246]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_245]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_400]],  
          Name = [[Group 3]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_352]],  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_353]],  
                x = 26943.625,  
                y = -1998,  
                z = -7.9375
              },  
              Angle = -2.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_350]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_382]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_383]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_314]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 2,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6724654,  
              ArmModel = 6725678,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              Name = [[Nialeki]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_382]]
              },  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_376]],  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_377]],  
                x = 26942.57813,  
                y = -1996.5625,  
                z = -7.984375
              },  
              Angle = -2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_374]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_380]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_381]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_314]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 2,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6724654,  
              ArmModel = 6725678,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              Name = [[Kyalani]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_380]]
              },  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_372]],  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_373]],  
                x = 26943.42188,  
                y = -1995.484375,  
                z = -7.921875
              },  
              Angle = -2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_370]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_378]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_379]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_314]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 2,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6724654,  
              ArmModel = 6725678,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              Name = [[Tylini]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_378]]
              },  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_399]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_398]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_11]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_9]],  
    Texts = {
    }
  }
}