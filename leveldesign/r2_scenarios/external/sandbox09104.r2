scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_547]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_549]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    Position = 0,  
    TextManager = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 8,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_550]],  
      ActivitiesIds = {
        [[Client1_592]],  
        [[Client1_620]],  
        [[Client1_626]],  
        [[Client1_632]],  
        [[Client1_638]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_556]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_554]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spawn Decoration]],  
              Position = {
                y = -1994.6875,  
                x = 26951.15625,  
                InstanceId = [[Client1_557]],  
                Class = [[Position]],  
                z = -7.625
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_564]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_562]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kitin Sandbox]],  
              Position = {
                y = -2032.734375,  
                x = 27076.71875,  
                InstanceId = [[Client1_565]],  
                Class = [[Position]],  
                z = 14.96875
              },  
              Angle = 2.59375,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_568]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_566]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              Position = {
                y = -2012.46875,  
                x = 26935.59375,  
                InstanceId = [[Client1_569]],  
                Class = [[Position]],  
                z = -3.125
              },  
              Angle = 0.703125,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kami Region]],  
              InstanceId = [[Client1_579]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_578]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_581]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2015.015625,  
                    x = 26941.07813,  
                    InstanceId = [[Client1_582]],  
                    Class = [[Position]],  
                    z = -3.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_584]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1990.53125,  
                    x = 26935.17188,  
                    InstanceId = [[Client1_585]],  
                    Class = [[Position]],  
                    z = -7.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_587]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1975.796875,  
                    x = 26960.73438,  
                    InstanceId = [[Client1_588]],  
                    Class = [[Position]],  
                    z = -8.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_590]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2001.890625,  
                    x = 26966.4375,  
                    InstanceId = [[Client1_591]],  
                    Class = [[Position]],  
                    z = -7.921875
                  }
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_614]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_615]],  
                x = 26956.54688,  
                y = -2019.421875,  
                z = -8.890625
              },  
              Angle = -1.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_612]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[This is Spawn]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_618]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_619]],  
                x = 26963.09375,  
                y = -2000.171875,  
                z = -8.1875
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_616]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_620]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_621]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_620]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_624]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_625]],  
                x = 26956.71875,  
                y = -1979.53125,  
                z = -7.765625
              },  
              Angle = 0.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_622]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_626]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_627]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_626]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_630]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_631]],  
                x = 26940.03125,  
                y = -1991.234375,  
                z = -7.671875
              },  
              Angle = 2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_628]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_632]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_633]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_632]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_636]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_637]],  
                x = 26943.23438,  
                y = -2001.09375,  
                z = -7.71875
              },  
              Angle = -2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_634]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_638]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_639]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_638]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 4]]
            }
          },  
          InstanceId = [[Client1_551]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_548]]
  }
}