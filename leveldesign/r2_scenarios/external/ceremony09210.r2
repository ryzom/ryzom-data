scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    LogicEntityReaction = 0,  
    ActionStep = 0,  
    NpcCustom = 0,  
    ActionType = 0,  
    EventType = 0,  
    Position = 0
  },  
  Acts = {
    {
      Cost = 43,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      InstanceId = [[Client1_1135]],  
      ActivitiesIds = {
        [[Client1_1438]],  
        [[Client1_1517]]
      },  
      Title = [[Ceremony]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1139]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Exit Tower]],  
              Position = {
                y = -1967.8125,  
                x = 27940.65625,  
                InstanceId = [[Client1_1142]],  
                Class = [[Position]],  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1143]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 1]],  
              Position = {
                y = -1851.296875,  
                x = 27899.29688,  
                InstanceId = [[Client1_1146]],  
                Class = [[Position]],  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1147]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 1]],  
              Position = {
                y = -1851.203125,  
                x = 27895.9375,  
                InstanceId = [[Client1_1150]],  
                Class = [[Position]],  
                z = -13.875
              },  
              Angle = -2.203125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1151]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Tower 1]],  
              Position = {
                y = -1850.9375,  
                x = 27931.39063,  
                InstanceId = [[Client1_1154]],  
                Class = [[Position]],  
                z = -14.5
              },  
              Angle = -1.71875,  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1155]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 2]],  
              Position = {
                y = -1844.8125,  
                x = 27922.95313,  
                InstanceId = [[Client1_1158]],  
                Class = [[Position]],  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1159]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 3]],  
              Position = {
                y = -1853.234375,  
                x = 27937.125,  
                InstanceId = [[Client1_1162]],  
                Class = [[Position]],  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1163]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 1]],  
              Position = {
                y = -1838.734375,  
                x = 27892.96875,  
                InstanceId = [[Client1_1166]],  
                Class = [[Position]],  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1167]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 2]],  
              Position = {
                y = -1840.8125,  
                x = 27937.21875,  
                InstanceId = [[Client1_1170]],  
                Class = [[Position]],  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1173]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1171]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 1]],  
              Position = {
                y = -1856.328125,  
                x = 27930.85938,  
                InstanceId = [[Client1_1174]],  
                Class = [[Position]],  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1177]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1175]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 2]],  
              Position = {
                y = -1853.578125,  
                x = 27898.125,  
                InstanceId = [[Client1_1178]],  
                Class = [[Position]],  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1181]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1179]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 2]],  
              Position = {
                y = -1887.59375,  
                x = 27942.53125,  
                InstanceId = [[Client1_1182]],  
                Class = [[Position]],  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1185]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1183]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 3]],  
              Position = {
                y = -1901.703125,  
                x = 27941.15625,  
                InstanceId = [[Client1_1186]],  
                Class = [[Position]],  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1189]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1187]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 4]],  
              Position = {
                y = -1896.890625,  
                x = 27943.17188,  
                InstanceId = [[Client1_1190]],  
                Class = [[Position]],  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1193]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1191]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 3]],  
              Position = {
                y = -1900.859375,  
                x = 27939.875,  
                InstanceId = [[Client1_1194]],  
                Class = [[Position]],  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1197]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1195]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Spawn Point]],  
              Position = {
                y = -1878.25,  
                x = 27915.09375,  
                InstanceId = [[Client1_1198]],  
                Class = [[Position]],  
                z = -10.640625
              },  
              Angle = 1.625,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1209]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1207]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Circle 1]],  
              Position = {
                y = -1877.34375,  
                x = 27895.6875,  
                InstanceId = [[Client1_1210]],  
                Class = [[Position]],  
                z = -13.0625
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1213]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1211]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 3]],  
              Position = {
                y = -1893.28125,  
                x = 27964.14063,  
                InstanceId = [[Client1_1214]],  
                Class = [[Position]],  
                z = -13.71875
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Home Region]],  
              InstanceId = [[Client1_1216]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1215]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1218]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1906.75,  
                    x = 27932.96875,  
                    InstanceId = [[Client1_1219]],  
                    Class = [[Position]],  
                    z = -7.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1221]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1881.984375,  
                    x = 27932.32813,  
                    InstanceId = [[Client1_1222]],  
                    Class = [[Position]],  
                    z = -9.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1224]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1863.34375,  
                    x = 27935.84375,  
                    InstanceId = [[Client1_1225]],  
                    Class = [[Position]],  
                    z = -11.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1861.46875,  
                    x = 27890.98438,  
                    InstanceId = [[Client1_1228]],  
                    Class = [[Position]],  
                    z = -14.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1869.015625,  
                    x = 27883.375,  
                    InstanceId = [[Client1_1231]],  
                    Class = [[Position]],  
                    z = -13.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1903.015625,  
                    x = 27887.78125,  
                    InstanceId = [[Client1_1234]],  
                    Class = [[Position]],  
                    z = -10.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1901.34375,  
                    x = 27898.15625,  
                    InstanceId = [[Client1_1237]],  
                    Class = [[Position]],  
                    z = -7.859375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1263]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1261]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -2034.015625,  
                x = 28058.39063,  
                InstanceId = [[Client1_1264]],  
                Class = [[Position]],  
                z = 9.828125
              },  
              Angle = 0.078125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1267]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1265]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 2]],  
              Position = {
                y = -1974.828125,  
                x = 28140.42188,  
                InstanceId = [[Client1_1268]],  
                Class = [[Position]],  
                z = 15.875
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1271]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1269]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 3]],  
              Position = {
                y = -1946.765625,  
                x = 28223.21875,  
                InstanceId = [[Client1_1272]],  
                Class = [[Position]],  
                z = 32.015625
              },  
              Angle = 0.515625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1359]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1357]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Hut 1]],  
              Position = {
                y = -1838.203125,  
                x = 28334.4375,  
                InstanceId = [[Client1_1360]],  
                Class = [[Position]],  
                z = 33.25
              },  
              Angle = -2.921875,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1363]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1361]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Watchtower 1]],  
              Position = {
                y = -1830.140625,  
                x = 28311.25,  
                InstanceId = [[Client1_1364]],  
                Class = [[Position]],  
                z = 33.375
              },  
              Angle = -1.546875,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1367]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1365]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Watchtower 2]],  
              Position = {
                y = -1849.75,  
                x = 28312.84375,  
                InstanceId = [[Client1_1368]],  
                Class = [[Position]],  
                z = 33.796875
              },  
              Angle = 2.46875,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Security Region]],  
              InstanceId = [[Client1_1425]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1424]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1427]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1828.4375,  
                    x = 28310.14063,  
                    InstanceId = [[Client1_1428]],  
                    Class = [[Position]],  
                    z = 33.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1430]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1825.09375,  
                    x = 28335.92188,  
                    InstanceId = [[Client1_1431]],  
                    Class = [[Position]],  
                    z = 34.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1433]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1855.75,  
                    x = 28338.23438,  
                    InstanceId = [[Client1_1434]],  
                    Class = [[Position]],  
                    z = 34.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1436]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1851.625,  
                    x = 28311.59375,  
                    InstanceId = [[Client1_1437]],  
                    Class = [[Position]],  
                    z = 34.484375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1446]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1444]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Stele 1]],  
              Position = {
                y = -1901.8125,  
                x = 28189.9375,  
                InstanceId = [[Client1_1447]],  
                Class = [[Position]],  
                z = 38.21875
              },  
              Angle = 0.375,  
              Base = [[palette.entities.botobjects.stele]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1454]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1452]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Spot 1]],  
              Position = {
                y = -1909.078125,  
                x = 28168.67188,  
                InstanceId = [[Client1_1455]],  
                Class = [[Position]],  
                z = 24.125
              },  
              Angle = 0.5,  
              Base = [[palette.entities.botobjects.spot_goo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1458]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1456]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Smoke 1]],  
              Position = {
                y = -1907,  
                x = 28171.23438,  
                InstanceId = [[Client1_1459]],  
                Class = [[Position]],  
                z = 27.375
              },  
              Angle = 0.640625,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1462]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1460]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[End Tent 1]],  
              Position = {
                y = -1756.09375,  
                x = 28247.25,  
                InstanceId = [[Client1_1463]],  
                Class = [[Position]],  
                z = 28.78125
              },  
              Angle = -2.078125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1466]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1464]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[End Hut 1]],  
              Position = {
                y = -1752.5,  
                x = 28236.39063,  
                InstanceId = [[Client1_1467]],  
                Class = [[Position]],  
                z = 27.875
              },  
              Angle = -1.234375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1470]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1468]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[End Wind Turbine 1]],  
              Position = {
                y = -1816.8125,  
                x = 28208.625,  
                InstanceId = [[Client1_1471]],  
                Class = [[Position]],  
                z = 37.34375
              },  
              Angle = 2.005622864,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[End Region]],  
              InstanceId = [[Client1_1476]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1475]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1478]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1770.234375,  
                    x = 28231.60938,  
                    InstanceId = [[Client1_1479]],  
                    Class = [[Position]],  
                    z = 31.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1481]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1743.65625,  
                    x = 28224.67188,  
                    InstanceId = [[Client1_1482]],  
                    Class = [[Position]],  
                    z = 28.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1484]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1743.390625,  
                    x = 28249.42188,  
                    InstanceId = [[Client1_1485]],  
                    Class = [[Position]],  
                    z = 29.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1487]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1772.125,  
                    x = 28255.21875,  
                    InstanceId = [[Client1_1488]],  
                    Class = [[Position]],  
                    z = 31.703125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Goo Region]],  
              InstanceId = [[Client1_1490]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1489]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1492]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1903.984375,  
                    x = 28207.23438,  
                    InstanceId = [[Client1_1493]],  
                    Class = [[Position]],  
                    z = 34.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1495]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1887.84375,  
                    x = 28178.79688,  
                    InstanceId = [[Client1_1496]],  
                    Class = [[Position]],  
                    z = 40.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1498]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.09375,  
                    x = 28167.8125,  
                    InstanceId = [[Client1_1499]],  
                    Class = [[Position]],  
                    z = 22.953125
                  }
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1502]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1503]],  
                x = 27934.375,  
                y = -1960.359375,  
                z = -2.828125
              },  
              Angle = 1.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1500]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ora]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1506]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1507]],  
                x = 28066.14063,  
                y = -2024.9375,  
                z = 10.390625
              },  
              Angle = 0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1504]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ora]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1510]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1511]],  
                x = 28140.64063,  
                y = -1985.578125,  
                z = 15.984375
              },  
              Angle = -0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1508]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ora]]
            }
          },  
          InstanceId = [[Client1_1136]]
        },  
        {
          InstanceId = [[Client1_1260]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Ceremony Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1259]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1258]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_1240]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1238]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1513]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1515]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1514]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1517]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1512]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ceremony Master]],  
              Position = {
                y = -1876.734375,  
                x = 27908.625,  
                InstanceId = [[Client1_1241]],  
                Class = [[Position]],  
                z = -11.28125
              },  
              Angle = 0,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1256]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1254]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6736430,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 4,  
              Name = [[Ceremony Guard]],  
              Position = {
                y = -1874.671875,  
                x = 27907.17188,  
                InstanceId = [[Client1_1257]],  
                Class = [[Position]],  
                z = -11.9375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1252]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1250]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6736430,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 4,  
              Name = [[Ceremony Guard]],  
              Position = {
                y = -1878.078125,  
                x = 27907.75,  
                InstanceId = [[Client1_1253]],  
                Class = [[Position]],  
                z = -10.984375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 12
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1474]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Security Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1473]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1472]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1411]],  
              ActivitiesId = {
                [[Client1_1438]],  
                [[Client1_1517]]
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 14,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 14,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1409]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1516]],  
                    LogicEntityAction = [[Client1_1513]],  
                    ActionStep = [[Client1_1515]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[INITI]],  
                    InstanceId = [[Client1_1438]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1439]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1425]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1517]],  
                    Name = [[Homes]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1518]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1216]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = 3,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 6767150,  
              ArmColor = 4,  
              Name = [[Security Guard Leader]],  
              Position = {
                y = -1838.734375,  
                x = 28320.46875,  
                InstanceId = [[Client1_1412]],  
                Class = [[Position]],  
                z = 31.4375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 18
            },  
            {
              InstanceId = [[Client1_1419]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1417]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Kami]],  
              Position = {
                y = -1847.515625,  
                x = 28327.75,  
                InstanceId = [[Client1_1420]],  
                Class = [[Position]],  
                z = 33.890625
              },  
              Angle = 3,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1415]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1413]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Kami]],  
              Position = {
                y = -1833.5625,  
                x = 28326.82813,  
                InstanceId = [[Client1_1416]],  
                Class = [[Position]],  
                z = 33.6875
              },  
              Angle = 3.279252768,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1403]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1401]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 6767150,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1841.140625,  
                x = 28323.90625,  
                InstanceId = [[Client1_1404]],  
                Class = [[Position]],  
                z = 33.046875
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1407]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1405]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = 3,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 6767150,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1844.4375,  
                x = 28323.70313,  
                InstanceId = [[Client1_1408]],  
                Class = [[Position]],  
                z = 33
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1399]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1397]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 6767150,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1838.140625,  
                x = 28323.8125,  
                InstanceId = [[Client1_1400]],  
                Class = [[Position]],  
                z = 33.078125
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1395]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1393]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 6767150,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1835.515625,  
                x = 28323.92188,  
                InstanceId = [[Client1_1396]],  
                Class = [[Position]],  
                z = 33.296875
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1391]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1389]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 6767150,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1832.75,  
                x = 28323.57813,  
                InstanceId = [[Client1_1392]],  
                Class = [[Position]],  
                z = 33.375
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      ManualWeather = 1,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1133]]
  }
}