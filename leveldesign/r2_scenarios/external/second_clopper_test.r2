scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1523]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1525]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Region = 0,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    Act = 1,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
        [[Client1_1738]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_1728]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1727]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1730]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2012.328125,  
                    x = 26948.64063,  
                    InstanceId = [[Client1_1731]],  
                    Class = [[Position]],  
                    z = -6.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1733]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2031.375,  
                    x = 26945.10938,  
                    InstanceId = [[Client1_1734]],  
                    Class = [[Position]],  
                    z = -2.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1736]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2037.40625,  
                    x = 26961.89063,  
                    InstanceId = [[Client1_1737]],  
                    Class = [[Position]],  
                    z = -9.140625
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_1527]]
        },  
        {
          InstanceId = [[Client1_1646]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1645]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1644]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_1543]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1541]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1738]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1739]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1728]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Clopperketh]],  
              Position = {
                y = -2027.984375,  
                x = 26950.01563,  
                InstanceId = [[Client1_1544]],  
                Class = [[Position]],  
                z = -4.28125
              },  
              Angle = 0.4375,  
              Base = [[palette.entities.creatures.ccbdc7]],  
              ActivitiesId = {
                [[Client1_1738]]
              }
            },  
            {
              InstanceId = [[Client1_1622]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1620]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.421875,  
                x = 26955.15625,  
                InstanceId = [[Client1_1623]],  
                Class = [[Position]],  
                z = -7.703125
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1586]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1584]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.625,  
                x = 26954.75,  
                InstanceId = [[Client1_1587]],  
                Class = [[Position]],  
                z = -7.390625
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1582]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1580]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.953125,  
                x = 26954.75,  
                InstanceId = [[Client1_1583]],  
                Class = [[Position]],  
                z = -7.359375
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1590]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1588]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2027.859375,  
                x = 26954.46875,  
                InstanceId = [[Client1_1591]],  
                Class = [[Position]],  
                z = -7.296875
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1602]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1600]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2029.796875,  
                x = 26955.26563,  
                InstanceId = [[Client1_1603]],  
                Class = [[Position]],  
                z = -7.609375
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1598]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1596]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.34375,  
                x = 26954.71875,  
                InstanceId = [[Client1_1599]],  
                Class = [[Position]],  
                z = -7.4375
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1606]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1604]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2027.40625,  
                x = 26954.82813,  
                InstanceId = [[Client1_1607]],  
                Class = [[Position]],  
                z = -7.609375
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1578]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1576]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2030.359375,  
                x = 26954.54688,  
                InstanceId = [[Client1_1579]],  
                Class = [[Position]],  
                z = -7.046875
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1574]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1572]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2031.640625,  
                x = 26954.73438,  
                InstanceId = [[Client1_1575]],  
                Class = [[Position]],  
                z = -7.015625
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1562]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1560]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2030.234375,  
                x = 26955.14063,  
                InstanceId = [[Client1_1563]],  
                Class = [[Position]],  
                z = -7.4375
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1618]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1616]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2029.109375,  
                x = 26955.40625,  
                InstanceId = [[Client1_1619]],  
                Class = [[Position]],  
                z = -7.78125
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1626]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1624]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2027.4375,  
                x = 26955.04688,  
                InstanceId = [[Client1_1627]],  
                Class = [[Position]],  
                z = -7.75
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1614]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1612]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2030.3125,  
                x = 26955.42188,  
                InstanceId = [[Client1_1615]],  
                Class = [[Position]],  
                z = -7.640625
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1630]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1628]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.78125,  
                x = 26955.57813,  
                InstanceId = [[Client1_1631]],  
                Class = [[Position]],  
                z = -7.90625
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1634]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1632]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.9375,  
                x = 26955.29688,  
                InstanceId = [[Client1_1635]],  
                Class = [[Position]],  
                z = -7.734375
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1570]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1568]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2029.921875,  
                x = 26954.67188,  
                InstanceId = [[Client1_1571]],  
                Class = [[Position]],  
                z = -7.1875
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1558]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1556]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.734375,  
                x = 26955.60938,  
                InstanceId = [[Client1_1559]],  
                Class = [[Position]],  
                z = -7.90625
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1610]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1608]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2027.90625,  
                x = 26955.04688,  
                InstanceId = [[Client1_1611]],  
                Class = [[Position]],  
                z = -7.703125
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1566]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1564]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2030.140625,  
                x = 26954.875,  
                InstanceId = [[Client1_1567]],  
                Class = [[Position]],  
                z = -7.28125
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1547]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1545]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2029.09375,  
                x = 26953.96875,  
                InstanceId = [[Client1_1548]],  
                Class = [[Position]],  
                z = -6.828125
              },  
              Angle = 0.4375,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1642]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1640]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2029.109375,  
                x = 26954.96875,  
                InstanceId = [[Client1_1643]],  
                Class = [[Position]],  
                z = -7.5
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1594]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1592]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2030.25,  
                x = 26954.5625,  
                InstanceId = [[Client1_1595]],  
                Class = [[Position]],  
                z = -7.0625
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1554]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1552]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2026.234375,  
                x = 26954.70313,  
                InstanceId = [[Client1_1555]],  
                Class = [[Position]],  
                z = -7.625
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1638]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1636]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Menacing Clopper]],  
              Position = {
                y = -2028.84375,  
                x = 26954.9375,  
                InstanceId = [[Client1_1639]],  
                Class = [[Position]],  
                z = -7.515625
              },  
              Angle = 0.546875,  
              Base = [[palette.entities.creatures.ccbdc4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1693]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1691]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2022.171875,  
                x = 26949.35938,  
                InstanceId = [[Client1_1694]],  
                Class = [[Position]],  
                z = -4.671875
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1689]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1687]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2022.015625,  
                x = 26949.25,  
                InstanceId = [[Client1_1690]],  
                Class = [[Position]],  
                z = -4.640625
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1669]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1667]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.921875,  
                x = 26951.46875,  
                InstanceId = [[Client1_1670]],  
                Class = [[Position]],  
                z = -5.828125
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1677]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1675]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.5,  
                x = 26951.125,  
                InstanceId = [[Client1_1678]],  
                Class = [[Position]],  
                z = -5.640625
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1685]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1683]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2021.671875,  
                x = 26949.28125,  
                InstanceId = [[Client1_1686]],  
                Class = [[Position]],  
                z = -4.75
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1681]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1679]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2022.640625,  
                x = 26950.35938,  
                InstanceId = [[Client1_1682]],  
                Class = [[Position]],  
                z = -5.234375
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1673]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1671]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.125,  
                x = 26950.57813,  
                InstanceId = [[Client1_1674]],  
                Class = [[Position]],  
                z = -5.328125
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1665]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1663]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.46875,  
                x = 26951.34375,  
                InstanceId = [[Client1_1666]],  
                Class = [[Position]],  
                z = -5.8125
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1705]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1703]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.078125,  
                x = 26950.1875,  
                InstanceId = [[Client1_1706]],  
                Class = [[Position]],  
                z = -5.046875
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1697]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1695]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2022.796875,  
                x = 26950.15625,  
                InstanceId = [[Client1_1698]],  
                Class = [[Position]],  
                z = -5.078125
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1717]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1715]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2021.765625,  
                x = 26948.90625,  
                InstanceId = [[Client1_1718]],  
                Class = [[Position]],  
                z = -4.484375
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1713]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1711]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2022.03125,  
                x = 26949.67188,  
                InstanceId = [[Client1_1714]],  
                Class = [[Position]],  
                z = -4.921875
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1657]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1655]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.21875,  
                x = 26950.84375,  
                InstanceId = [[Client1_1658]],  
                Class = [[Position]],  
                z = -5.5
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1701]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1699]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.1875,  
                x = 26949.73438,  
                InstanceId = [[Client1_1702]],  
                Class = [[Position]],  
                z = -4.734375
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1649]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1647]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.375,  
                x = 26951.3125,  
                InstanceId = [[Client1_1650]],  
                Class = [[Position]],  
                z = -5.796875
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1661]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1659]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.6875,  
                x = 26951.15625,  
                InstanceId = [[Client1_1662]],  
                Class = [[Position]],  
                z = -5.640625
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1709]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1707]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.34375,  
                x = 26951,  
                InstanceId = [[Client1_1710]],  
                Class = [[Position]],  
                z = -5.578125
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1653]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1651]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.125,  
                x = 26951.14063,  
                InstanceId = [[Client1_1654]],  
                Class = [[Position]],  
                z = -5.71875
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1725]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1723]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.9375,  
                x = 26950.28125,  
                InstanceId = [[Client1_1726]],  
                Class = [[Position]],  
                z = -4.984375
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1721]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1719]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]],  
              Position = {
                y = -2023.953125,  
                x = 26950.64063,  
                InstanceId = [[Client1_1722]],  
                Class = [[Position]],  
                z = -5.234375
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.creatures.ccbdc3]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1762]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = -18.34375,  
            x = -17.6875,  
            InstanceId = [[Client1_1761]],  
            Class = [[Position]],  
            z = -3.921875
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_1742]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 13,  
              HandsModel = 5604142,  
              FeetColor = 2,  
              GabaritBreastSize = 6,  
              GabaritHeight = 2,  
              HairColor = 4,  
              EyesColor = 3,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1740]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 5603886,  
              Angle = 0.171875,  
              Base = [[palette.entities.npcs.guards.f_guard_95]],  
              Tattoo = 10,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Sex = 0,  
              WeaponRightHand = 6755118,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_blunt_c4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[desert guard 1]],  
              Position = {
                y = -2028.484375,  
                x = 26935.4375,  
                InstanceId = [[Client1_1743]],  
                Class = [[Position]],  
                z = -1.53125
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_1746]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 3,  
              GabaritBreastSize = 4,  
              GabaritHeight = 6,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1744]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 1,  
              FeetModel = 5653038,  
              Angle = 0.171875,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_70]],  
              Tattoo = 19,  
              MorphTarget3 = 3,  
              MorphTarget7 = 6,  
              Sex = 1,  
              WeaponRightHand = 6759214,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              Sheet = [[ring_light_melee_slash_c2.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[forest milicia 1]],  
              Position = {
                y = -2029.953125,  
                x = 26934.65625,  
                InstanceId = [[Client1_1747]],  
                Class = [[Position]],  
                z = -1.625
              },  
              JacketModel = 5610542,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1750]],  
              ActivitiesId = {
              },  
              HairType = 5422,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 5609774,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 7,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 6714158,  
              GabaritLegsWidth = 4,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1748]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 6713134,  
              Angle = 0.171875,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_70]],  
              Tattoo = 4,  
              MorphTarget3 = 7,  
              MorphTarget7 = 5,  
              Sex = 0,  
              WeaponRightHand = 5597230,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_slash_c2.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[forest milicia 2]],  
              Position = {
                y = -2028.640625,  
                x = 26933.625,  
                InstanceId = [[Client1_1751]],  
                Class = [[Position]],  
                z = -1.53125
              },  
              JacketModel = 6715182,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_1754]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 2,  
              HandsModel = 6713646,  
              FeetColor = 1,  
              GabaritBreastSize = 10,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 6,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1752]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 6713134,  
              Angle = 0.171875,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_70]],  
              Tattoo = 25,  
              MorphTarget3 = 5,  
              MorphTarget7 = 3,  
              Sex = 1,  
              WeaponRightHand = 6757166,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 0,  
              Sheet = [[ring_light_melee_blunt_c2.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[forest milicia 3]],  
              Position = {
                y = -2027.046875,  
                x = 26934.03125,  
                InstanceId = [[Client1_1755]],  
                Class = [[Position]],  
                z = -1.484375
              },  
              JacketModel = 6715182,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1758]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 4,  
              HandsModel = 6713646,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 13,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6714158,  
              GabaritLegsWidth = 12,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1756]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 5653038,  
              Angle = 0.171875,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_70]],  
              Tattoo = 17,  
              MorphTarget3 = 6,  
              MorphTarget7 = 6,  
              Sex = 0,  
              WeaponRightHand = 6758190,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_light_melee_pierce_c2.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[forest milicia 4]],  
              Position = {
                y = -2026.15625,  
                x = 26935.04688,  
                InstanceId = [[Client1_1759]],  
                Class = [[Position]],  
                z = -1.46875
              },  
              JacketModel = 6715182,  
              WeaponLeftHand = 6758190,  
              ArmModel = 0,  
              SheetClient = [[basic_matis_male.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1760]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_1526]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_1524]],  
    Texts = {
    }
  }
}