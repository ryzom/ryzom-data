scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client1_1275]],  
      Season = [[fall]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts03]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_1266]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    MapDescription = 0,  
    Position = 0,  
    Location = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1264]],  
    Class = [[MapDescription]],  
    LevelId = 3,  
    LocationId = 41,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[An army of Kitin is invading Kaethia! Can they be stopped?]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1267]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1269]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1268]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1270]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1271]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[Act HERE]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1273]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1272]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 238,  
      LocationId = [[Client1_1275]],  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1274]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:Nothing here]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1265]]
  }
}