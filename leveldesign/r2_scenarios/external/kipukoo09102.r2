scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_869]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_871]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    NpcCustom = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 10,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_878]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_879]],  
                x = 26956.1875,  
                y = -2013.53125,  
                z = -8.640625
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_876]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_882]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_883]],  
                x = 26958.40625,  
                y = -2022.515625,  
                z = -9.359375
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_880]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_886]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_887]],  
                x = 26954.125,  
                y = -2004.546875,  
                z = -8.25
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_890]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_891]],  
                x = 26960.5,  
                y = -2031.515625,  
                z = -9.40625
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_888]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_894]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_895]],  
                x = 26952.09375,  
                y = -1995.390625,  
                z = -7.71875
              },  
              Angle = -2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_892]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_898]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_899]],  
                x = 26958.23438,  
                y = -2000.640625,  
                z = -8.34375
              },  
              Angle = -1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_896]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 6]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_902]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_903]],  
                x = 26963.625,  
                y = -2006.65625,  
                z = -8.625
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_900]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 7]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_906]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_907]],  
                x = 26965.9375,  
                y = -2015.9375,  
                z = -9.078125
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_904]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 8]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_910]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_911]],  
                x = 26963.89063,  
                y = -2023.6875,  
                z = -9.9375
              },  
              Angle = 2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_908]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 9]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_918]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_919]],  
                x = 26959.96875,  
                y = -2010.59375,  
                z = -8.828125
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_916]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 0,  
              EyesColor = 3,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]]
            }
          },  
          InstanceId = [[Client1_873]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_872]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_870]],  
    Texts = {
    }
  }
}