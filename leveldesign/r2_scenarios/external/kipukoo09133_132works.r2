scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_869]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_871]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    NpcCustom = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    TextManagerEntry = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 46,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
        [[Client1_1046]],  
        [[Client1_1052]],  
        [[Client1_1057]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_878]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_876]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 1]],  
              Position = {
                y = -2013.53125,  
                x = 26956.1875,  
                InstanceId = [[Client1_879]],  
                Class = [[Position]],  
                z = -8.640625
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_882]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_880]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 2]],  
              Position = {
                y = -2021.859375,  
                x = 26958.20313,  
                InstanceId = [[Client1_883]],  
                Class = [[Position]],  
                z = -9.296875
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_886]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_884]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 3]],  
              Position = {
                y = -2005.234375,  
                x = 26953.98438,  
                InstanceId = [[Client1_887]],  
                Class = [[Position]],  
                z = -8.234375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_890]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_888]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 4]],  
              Position = {
                y = -2030.15625,  
                x = 26960.29688,  
                InstanceId = [[Client1_891]],  
                Class = [[Position]],  
                z = -9.4375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_894]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_892]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 5]],  
              Position = {
                y = -1996.828125,  
                x = 26952,  
                InstanceId = [[Client1_895]],  
                Class = [[Position]],  
                z = -7.703125
              },  
              Angle = -2.890625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_898]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_896]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 6]],  
              Position = {
                y = -2002.328125,  
                x = 26957.53125,  
                InstanceId = [[Client1_899]],  
                Class = [[Position]],  
                z = -8.390625
              },  
              Angle = -1.859375,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_902]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_900]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 7]],  
              Position = {
                y = -2007.515625,  
                x = 26962.92188,  
                InstanceId = [[Client1_903]],  
                Class = [[Position]],  
                z = -8.671875
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_906]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_904]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 8]],  
              Position = {
                y = -2015.890625,  
                x = 26965.01563,  
                InstanceId = [[Client1_907]],  
                Class = [[Position]],  
                z = -9.109375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_910]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_908]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 9]],  
              Position = {
                y = -2022.71875,  
                x = 26962.4375,  
                InstanceId = [[Client1_911]],  
                Class = [[Position]],  
                z = -9.78125
              },  
              Angle = 2.265625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_922]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_920]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 10]],  
              Position = {
                y = -1988.453125,  
                x = 26952.21875,  
                InstanceId = [[Client1_923]],  
                Class = [[Position]],  
                z = -7.328125
              },  
              Angle = 2.828125,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_926]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_924]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 11]],  
              Position = {
                y = -1980.453125,  
                x = 26955.26563,  
                InstanceId = [[Client1_927]],  
                Class = [[Position]],  
                z = -7.671875
              },  
              Angle = 2.6875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_930]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_928]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 12]],  
              Position = {
                y = -1967.78125,  
                x = 26966.8125,  
                InstanceId = [[Client1_931]],  
                Class = [[Position]],  
                z = -8.703125
              },  
              Angle = 2.109375,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_934]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_932]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 13]],  
              Position = {
                y = -1973.21875,  
                x = 26960.125,  
                InstanceId = [[Client1_935]],  
                Class = [[Position]],  
                z = -8.546875
              },  
              Angle = 2.375,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_950]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_948]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              Position = {
                y = -1964.21875,  
                x = 26978.60938,  
                InstanceId = [[Client1_951]],  
                Class = [[Position]],  
                z = -5.921875
              },  
              Angle = -1.875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_954]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_952]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan banner 1]],  
              Position = {
                y = -2009.765625,  
                x = 26971.85938,  
                InstanceId = [[Client1_955]],  
                Class = [[Position]],  
                z = -7.078125
              },  
              Angle = -2.21875,  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_958]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_956]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan banner 2]],  
              Position = {
                y = -1992.5,  
                x = 26961.35938,  
                InstanceId = [[Client1_959]],  
                Class = [[Position]],  
                z = -7.84375
              },  
              Angle = -0.6875,  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_962]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_960]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 14]],  
              Position = {
                y = -2037.015625,  
                x = 26965.0625,  
                InstanceId = [[Client1_963]],  
                Class = [[Position]],  
                z = -9.515625
              },  
              Angle = -2.171875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_966]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_964]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 15]],  
              Position = {
                y = -2039.265625,  
                x = 26973.09375,  
                InstanceId = [[Client1_967]],  
                Class = [[Position]],  
                z = -9.203125
              },  
              Angle = -1.5,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_970]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_968]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 16]],  
              Position = {
                y = -2037.6875,  
                x = 26981.67188,  
                InstanceId = [[Client1_971]],  
                Class = [[Position]],  
                z = -8.5625
              },  
              Angle = -1.265625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_974]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_972]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 17]],  
              Position = {
                y = -2035.234375,  
                x = 26989.98438,  
                InstanceId = [[Client1_975]],  
                Class = [[Position]],  
                z = -8.484375
              },  
              Angle = -1.296875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_994]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_992]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              Position = {
                y = -2035.5,  
                x = 27006.5,  
                InstanceId = [[Client1_995]],  
                Class = [[Position]],  
                z = -3.609375
              },  
              Angle = -2.109375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_998]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_996]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              Position = {
                y = -2035.15625,  
                x = 27000.53125,  
                InstanceId = [[Client1_999]],  
                Class = [[Position]],  
                z = -6.6875
              },  
              Angle = -2.109375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1002]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1000]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              Position = {
                y = -2046.828125,  
                x = 27006.67188,  
                InstanceId = [[Client1_1003]],  
                Class = [[Position]],  
                z = -8.609375
              },  
              Angle = -2.578125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1006]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1004]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 5]],  
              Position = {
                y = -2038.6875,  
                x = 26995.20313,  
                InstanceId = [[Client1_1007]],  
                Class = [[Position]],  
                z = -8.578125
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1010]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1008]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan banner 3]],  
              Position = {
                y = -2029.25,  
                x = 26970.92188,  
                InstanceId = [[Client1_1011]],  
                Class = [[Position]],  
                z = -10.234375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1014]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1012]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 6]],  
              Position = {
                y = -1965.578125,  
                x = 26967.89063,  
                InstanceId = [[Client1_1015]],  
                Class = [[Position]],  
                z = -8.0625
              },  
              Angle = -3.046875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1018]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1016]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 7]],  
              Position = {
                y = -1972.1875,  
                x = 26975.82813,  
                InstanceId = [[Client1_1019]],  
                Class = [[Position]],  
                z = -9.25
              },  
              Angle = -2.015625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1022]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1020]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 8]],  
              Position = {
                y = -1968.796875,  
                x = 26982.67188,  
                InstanceId = [[Client1_1023]],  
                Class = [[Position]],  
                z = -7.078125
              },  
              Angle = -1.453125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_1033]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1032]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1035]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1986.453125,  
                    x = 26961.78125,  
                    InstanceId = [[Client1_1036]],  
                    Class = [[Position]],  
                    z = -7.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1038]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1980.53125,  
                    x = 26989.34375,  
                    InstanceId = [[Client1_1039]],  
                    Class = [[Position]],  
                    z = -5.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1041]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.65625,  
                    x = 27003.54688,  
                    InstanceId = [[Client1_1042]],  
                    Class = [[Position]],  
                    z = -0.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1044]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2031.6875,  
                    x = 26975.875,  
                    InstanceId = [[Client1_1045]],  
                    Class = [[Position]],  
                    z = -9.578125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1050]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1048]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1052]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1053]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kipukoo]],  
              Position = {
                y = -2028.375,  
                x = 27075.875,  
                InstanceId = [[Client1_1051]],  
                Class = [[Position]],  
                z = 14.171875
              },  
              Angle = 2.1875,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_1052]]
              }
            },  
            {
              InstanceId = [[Client1_1074]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1072]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[protector 1]],  
              Position = {
                y = -2009.21875,  
                x = 26959.03125,  
                InstanceId = [[Client1_1075]],  
                Class = [[Position]],  
                z = -8.78125
              },  
              Angle = -2.984375,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1078]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1076]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[voice of Jena 1]],  
              Position = {
                y = -2012.6875,  
                x = 26960.28125,  
                InstanceId = [[Client1_1079]],  
                Class = [[Position]],  
                z = -8.890625
              },  
              Angle = -2.984375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1082]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1080]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[protector 2]],  
              Position = {
                y = -2016.3125,  
                x = 26961.03125,  
                InstanceId = [[Client1_1083]],  
                Class = [[Position]],  
                z = -9.171875
              },  
              Angle = -2.984375,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1086]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1084]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              Position = {
                y = -2055.796875,  
                x = 26942.48438,  
                InstanceId = [[Client1_1087]],  
                Class = [[Position]],  
                z = -7.40625
              },  
              Angle = 1.9375,  
              Base = [[palette.entities.creatures.chcdb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1090]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1088]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ibakus]],  
              Position = {
                y = -2052.3125,  
                x = 26954.35938,  
                InstanceId = [[Client1_1091]],  
                Class = [[Position]],  
                z = -8.453125
              },  
              Angle = 2.546875,  
              Base = [[palette.entities.creatures.chgdb5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1094]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1092]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goaketh]],  
              Position = {
                y = -1936.625,  
                x = 26912.53125,  
                InstanceId = [[Client1_1095]],  
                Class = [[Position]],  
                z = -3.484375
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.creatures.cccdb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1098]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1096]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Bodokin]],  
              Position = {
                y = -2062.6875,  
                x = 26959.85938,  
                InstanceId = [[Client1_1099]],  
                Class = [[Position]],  
                z = -9.75
              },  
              Angle = 2.28125,  
              Base = [[palette.entities.creatures.chbfb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1102]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1100]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ora]],  
              Position = {
                y = -2061.578125,  
                x = 26941.375,  
                InstanceId = [[Client1_1103]],  
                Class = [[Position]],  
                z = -6.171875
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.creatures.chdfb5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1106]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1104]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Igakya]],  
              Position = {
                y = -2060.421875,  
                x = 26950.59375,  
                InstanceId = [[Client1_1107]],  
                Class = [[Position]],  
                z = -9.015625
              },  
              Angle = 1.75,  
              Base = [[palette.entities.creatures.cbajb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1110]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1108]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zoann]],  
              Position = {
                y = -2064.546875,  
                x = 26946.26563,  
                InstanceId = [[Client1_1111]],  
                Class = [[Position]],  
                z = -6.9375
              },  
              Angle = 1.75,  
              Base = [[palette.entities.creatures.chfjb5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1114]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1112]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Rakya]],  
              Position = {
                y = -2001.765625,  
                x = 26885.54688,  
                InstanceId = [[Client1_1115]],  
                Class = [[Position]],  
                z = -9.90625
              },  
              Angle = 2.125,  
              Base = [[palette.entities.creatures.ccejb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1118]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1116]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Messakan]],  
              Position = {
                y = -2059.765625,  
                x = 26952.45313,  
                InstanceId = [[Client1_1119]],  
                Class = [[Position]],  
                z = -9.25
              },  
              Angle = 2.125,  
              Base = [[palette.entities.creatures.chflb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1122]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1120]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              Position = {
                y = -2058.21875,  
                x = 26941.21875,  
                InstanceId = [[Client1_1123]],  
                Class = [[Position]],  
                z = -7.140625
              },  
              Angle = 2.234375,  
              Base = [[palette.entities.creatures.chcdb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1126]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1124]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              Position = {
                y = -2055.453125,  
                x = 26946.84375,  
                InstanceId = [[Client1_1127]],  
                Class = [[Position]],  
                z = -8.03125
              },  
              Angle = 2.234375,  
              Base = [[palette.entities.creatures.chcdb7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1130]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1128]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goaketh]],  
              Position = {
                y = -1932.90625,  
                x = 26974.03125,  
                InstanceId = [[Client1_1131]],  
                Class = [[Position]],  
                z = -4.625
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.cccdb7]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_873]]
        },  
        {
          InstanceId = [[Client1_1056]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = -15.703125,  
            x = 58.53125,  
            InstanceId = [[Client1_1055]],  
            Class = [[Position]],  
            z = 3.609375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1054]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_918]],  
              ActivitiesId = {
                [[Client1_1057]]
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_916]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1057]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1058]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Angle = -2.96875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756398,  
              ArmColor = 1,  
              Name = [[Guard]],  
              Position = {
                y = -1791.296875,  
                x = 26914.84375,  
                InstanceId = [[Client1_919]],  
                Class = [[Position]],  
                z = -23.3125
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              InstanceId = [[Client1_1026]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1024]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1046]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1047]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kipukoo]],  
              Position = {
                y = -1991.234375,  
                x = 26928.14063,  
                InstanceId = [[Client1_1027]],  
                Class = [[Position]],  
                z = -5.234375
              },  
              Angle = -2.96875,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_1046]]
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_872]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_870]],  
    Texts = {
      {
        Count = 1,  
        InstanceId = [[Client1_1067]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      }
    }
  }
}