scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 23,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    LogicEntityReaction = 0,  
    ActionStep = 0,  
    NpcCustom = 0,  
    ActionType = 0,  
    EventType = 0,  
    Position = 0
  },  
  Acts = {
    {
      Cost = 45,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[ACT 0: Permanent]],  
      ActivitiesIds = {
        [[Client1_106]],  
        [[Client1_108]],  
        [[Client1_110]],  
        [[Client1_155]],  
        [[Client1_157]],  
        [[Client1_159]],  
        [[Client1_304]],  
        [[Client1_528]],  
        [[Client1_530]],  
        [[Client1_532]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_57]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_55]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Totem]],  
              Position = {
                y = -2390.1875,  
                x = 26016.78125,  
                InstanceId = [[Client1_58]],  
                Class = [[Position]],  
                z = -9.609375
              },  
              Angle = 2.8125,  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_61]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_59]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Totem]],  
              Position = {
                y = -2376.5625,  
                x = 26012.625,  
                InstanceId = [[Client1_62]],  
                Class = [[Position]],  
                z = -10.171875
              },  
              Angle = -2.390625,  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_65]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_63]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Gateway]],  
              Position = {
                y = -2383.625,  
                x = 26014.73438,  
                InstanceId = [[Client1_66]],  
                Class = [[Position]],  
                z = -9.953125
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Kae Kirosta]],  
              InstanceId = [[Client1_76]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_78]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2570.328125,  
                    x = 25971,  
                    InstanceId = [[Client1_79]],  
                    Class = [[Position]],  
                    z = -9.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_81]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2582.25,  
                    x = 26005.14063,  
                    InstanceId = [[Client1_82]],  
                    Class = [[Position]],  
                    z = -9.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_84]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2624.765625,  
                    x = 25978.34375,  
                    InstanceId = [[Client1_85]],  
                    Class = [[Position]],  
                    z = -11.203125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_96]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_94]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_108]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_109]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_540]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_542]],  
                        Entity = r2.RefId([[Client1_263]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_541]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_543]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_539]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              Position = {
                y = -2584.78125,  
                x = 25982.75,  
                InstanceId = [[Client1_97]],  
                Class = [[Position]],  
                z = -10.625
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client1_108]]
              }
            },  
            {
              InstanceId = [[Client1_100]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_98]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_110]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_111]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_551]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_553]],  
                        Entity = r2.RefId([[Client1_263]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_552]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_543]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_550]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              Position = {
                y = -2591.0625,  
                x = 25985.28125,  
                InstanceId = [[Client1_101]],  
                Class = [[Position]],  
                z = -10.4375
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client1_110]]
              }
            },  
            {
              InstanceId = [[Client1_104]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_102]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_106]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_107]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_546]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_548]],  
                        Entity = r2.RefId([[Client1_263]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_547]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_543]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_545]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              Position = {
                y = -2593.0625,  
                x = 25977.82813,  
                InstanceId = [[Client1_105]],  
                Class = [[Position]],  
                z = -10.125
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client1_106]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Kae]],  
              InstanceId = [[Client1_124]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_126]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2624.765625,  
                    x = 25910.76563,  
                    InstanceId = [[Client1_127]],  
                    Class = [[Position]],  
                    z = -9.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_129]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2651.59375,  
                    x = 25911.59375,  
                    InstanceId = [[Client1_130]],  
                    Class = [[Position]],  
                    z = -10.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_132]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2673.125,  
                    x = 25893.60938,  
                    InstanceId = [[Client1_133]],  
                    Class = [[Position]],  
                    z = -9.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_135]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2654.453125,  
                    x = 25870.8125,  
                    InstanceId = [[Client1_136]],  
                    Class = [[Position]],  
                    z = -10.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_138]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2602.671875,  
                    x = 25871.70313,  
                    InstanceId = [[Client1_139]],  
                    Class = [[Position]],  
                    z = -8.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_141]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2599.6875,  
                    x = 25890.39063,  
                    InstanceId = [[Client1_142]],  
                    Class = [[Position]],  
                    z = -10.15625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_123]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_145]],  
              ActivitiesId = {
                [[Client1_159]]
              },  
              HairType = 8494,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 4,  
              HandsModel = 5617966,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 9,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_143]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_159]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_160]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 5617710,  
              Angle = 3.09375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 31,  
              MorphTarget3 = 5,  
              MorphTarget7 = 5,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Ni-Zo Long]],  
              Position = {
                y = -2625.40625,  
                x = 25880.98438,  
                InstanceId = [[Client1_146]],  
                Class = [[Position]],  
                z = -9.875
              },  
              JacketModel = 0,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_149]],  
              ActivitiesId = {
                [[Client1_157]]
              },  
              HairType = 9006,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 3,  
              HandsModel = 5617966,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 11,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_147]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_157]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_158]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 0,  
              Angle = 3.09375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 9,  
              MorphTarget3 = 2,  
              MorphTarget7 = 4,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[How-Long]],  
              Position = {
                y = -2628.21875,  
                x = 25886.32813,  
                InstanceId = [[Client1_150]],  
                Class = [[Position]],  
                z = -9.78125
              },  
              JacketModel = 5618734,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_153]],  
              ActivitiesId = {
                [[Client1_155]]
              },  
              HairType = 5624366,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 1,  
              HandsModel = 5617966,  
              FeetColor = 2,  
              GabaritBreastSize = 12,  
              GabaritHeight = 2,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 10,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_155]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_156]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Angle = 3.09375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 24,  
              MorphTarget3 = 3,  
              MorphTarget7 = 5,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[Ve-Ri Long]],  
              Position = {
                y = -2627.859375,  
                x = 25877.96875,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = -9.890625
              },  
              JacketModel = 5618734,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              InstanceId = [[Client1_163]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_161]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Tower]],  
              Position = {
                y = -2639.640625,  
                x = 25916.75,  
                InstanceId = [[Client1_164]],  
                Class = [[Position]],  
                z = -12.90625
              },  
              Angle = 0.125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_167]],  
              ActivitiesId = {
              },  
              HairType = 9006,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 0,  
              HandsModel = 5616430,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 5,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 5616942,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_165]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 5616174,  
              Angle = 0.265625,  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Tattoo = 27,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Sex = 0,  
              WeaponRightHand = 5636654,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[Zo-Kae Guard]],  
              Position = {
                y = -2641.390625,  
                x = 25938.67188,  
                InstanceId = [[Client1_168]],  
                Class = [[Position]],  
                z = -12.03125
              },  
              JacketModel = 5617454,  
              WeaponLeftHand = 0,  
              ArmModel = 5619758,  
              SheetClient = [[basic_zorai_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_171]],  
              ActivitiesId = {
              },  
              HairType = 8750,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 5619246,  
              FeetColor = 2,  
              GabaritBreastSize = 2,  
              GabaritHeight = 7,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 5619502,  
              GabaritLegsWidth = 13,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_169]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 5618990,  
              Angle = 0.046875,  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Tattoo = 10,  
              MorphTarget3 = 1,  
              MorphTarget7 = 6,  
              Sex = 0,  
              WeaponRightHand = 5636654,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Zo-Kae Guard]],  
              Position = {
                y = -2630.390625,  
                x = 25937.01563,  
                InstanceId = [[Client1_172]],  
                Class = [[Position]],  
                z = -10.328125
              },  
              JacketModel = 5620014,  
              WeaponLeftHand = 0,  
              ArmModel = 5619758,  
              SheetClient = [[basic_zorai_male.creature]]
            },  
            {
              InstanceId = [[Client1_179]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_177]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_536]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_537]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_190]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Guard]],  
              Position = {
                y = -2629.59375,  
                x = 25866.73438,  
                InstanceId = [[Client1_180]],  
                Class = [[Position]],  
                z = -9.1875
              },  
              Angle = 0.109375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              ActivitiesId = {
                [[Client1_536]]
              }
            },  
            {
              InstanceId = [[Client1_183]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_181]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_534]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_535]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_190]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Guard]],  
              Position = {
                y = -2619.796875,  
                x = 25867.09375,  
                InstanceId = [[Client1_184]],  
                Class = [[Position]],  
                z = -8.421875
              },  
              Angle = 0.109375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              ActivitiesId = {
                [[Client1_534]]
              }
            },  
            {
              InstanceId = [[Client1_187]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_185]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Preacher]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_188]],  
                x = 25870.5,  
                y = -2624.953125,  
                z = -9.109375
              },  
              Angle = 0,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Kae Kami]],  
              InstanceId = [[Client1_190]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_189]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_192]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2653.53125,  
                    x = 25869.25,  
                    InstanceId = [[Client1_193]],  
                    Class = [[Position]],  
                    z = -10.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_195]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2648.4375,  
                    x = 25843.70313,  
                    InstanceId = [[Client1_196]],  
                    Class = [[Position]],  
                    z = -8.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_198]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2615.15625,  
                    x = 25842.6875,  
                    InstanceId = [[Client1_199]],  
                    Class = [[Position]],  
                    z = -6.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_201]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2600.8125,  
                    x = 25851.26563,  
                    InstanceId = [[Client1_202]],  
                    Class = [[Position]],  
                    z = -8.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_204]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2597.6875,  
                    x = 25869.89063,  
                    InstanceId = [[Client1_205]],  
                    Class = [[Position]],  
                    z = -9.34375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_208]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_206]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ki-Teen Goo Source]],  
              Position = {
                y = -2465.53125,  
                x = 26177.84375,  
                InstanceId = [[Client1_209]],  
                Class = [[Position]],  
                z = -5.0625
              },  
              Angle = -1.671875,  
              Base = [[palette.entities.botobjects.spot_goo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_212]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_210]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ki-Teen Goo]],  
              Position = {
                y = -2531.5625,  
                x = 26033.42188,  
                InstanceId = [[Client1_213]],  
                Class = [[Position]],  
                z = -10.5
              },  
              Angle = -0.421875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Ki-Teen Region]],  
              InstanceId = [[Client1_215]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_214]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_217]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2565.421875,  
                    x = 26146.34375,  
                    InstanceId = [[Client1_218]],  
                    Class = [[Position]],  
                    z = -14.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_220]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2538.4375,  
                    x = 26151.57813,  
                    InstanceId = [[Client1_221]],  
                    Class = [[Position]],  
                    z = -13.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_223]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2503.734375,  
                    x = 26117.75,  
                    InstanceId = [[Client1_224]],  
                    Class = [[Position]],  
                    z = -13.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_226]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2443.875,  
                    x = 26114.32813,  
                    InstanceId = [[Client1_227]],  
                    Class = [[Position]],  
                    z = -14.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_229]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2396.28125,  
                    x = 26170.29688,  
                    InstanceId = [[Client1_230]],  
                    Class = [[Position]],  
                    z = -14.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_232]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2409.734375,  
                    x = 26175.5625,  
                    InstanceId = [[Client1_233]],  
                    Class = [[Position]],  
                    z = -12.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_235]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2427.25,  
                    x = 26208.46875,  
                    InstanceId = [[Client1_236]],  
                    Class = [[Position]],  
                    z = -12.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_238]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2509.21875,  
                    x = 26211.14063,  
                    InstanceId = [[Client1_239]],  
                    Class = [[Position]],  
                    z = -13.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_241]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2515.5625,  
                    x = 26203.17188,  
                    InstanceId = [[Client1_242]],  
                    Class = [[Position]],  
                    z = -13.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_244]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2544.953125,  
                    x = 26174.85938,  
                    InstanceId = [[Client1_245]],  
                    Class = [[Position]],  
                    z = -14.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_247]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2563.359375,  
                    x = 26173.42188,  
                    InstanceId = [[Client1_248]],  
                    Class = [[Position]],  
                    z = -14.34375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Region]],  
              InstanceId = [[Client1_494]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_493]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_496]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2420.625,  
                    x = 26013.78125,  
                    InstanceId = [[Client1_497]],  
                    Class = [[Position]],  
                    z = -7.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_499]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2421.703125,  
                    x = 25995.75,  
                    InstanceId = [[Client1_500]],  
                    Class = [[Position]],  
                    z = -6.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_502]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2398.78125,  
                    x = 25985.64063,  
                    InstanceId = [[Client1_503]],  
                    Class = [[Position]],  
                    z = -10.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_505]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2377.109375,  
                    x = 26012.71875,  
                    InstanceId = [[Client1_506]],  
                    Class = [[Position]],  
                    z = -10.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_508]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2367.828125,  
                    x = 26020.92188,  
                    InstanceId = [[Client1_509]],  
                    Class = [[Position]],  
                    z = -10.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_511]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2365.78125,  
                    x = 26053.98438,  
                    InstanceId = [[Client1_512]],  
                    Class = [[Position]],  
                    z = -6.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_514]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2386.28125,  
                    x = 26056.70313,  
                    InstanceId = [[Client1_515]],  
                    Class = [[Position]],  
                    z = -6.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_517]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2391.8125,  
                    x = 26054.03125,  
                    InstanceId = [[Client1_518]],  
                    Class = [[Position]],  
                    z = -6.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_520]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2390.875,  
                    x = 26015.39063,  
                    InstanceId = [[Client1_521]],  
                    Class = [[Position]],  
                    z = -9.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_523]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2395.765625,  
                    x = 26011.07813,  
                    InstanceId = [[Client1_524]],  
                    Class = [[Position]],  
                    z = -10.296875
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_5]]
        },  
        {
          InstanceId = [[Client1_303]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Ki-Teen]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_302]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_263]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_261]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_538]],  
                    LogicEntityAction = [[Client1_540]],  
                    ActionStep = [[Client1_542]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_549]],  
                    LogicEntityAction = [[Client1_546]],  
                    ActionStep = [[Client1_548]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_554]],  
                    LogicEntityAction = [[Client1_551]],  
                    ActionStep = [[Client1_553]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_304]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_525]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_215]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_543]],  
                    Name = [[Seq2]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_544]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ki-Teen Leader]],  
              Position = {
                y = -2516.84375,  
                x = 26174.21875,  
                InstanceId = [[Client1_264]],  
                Class = [[Position]],  
                z = -12.625
              },  
              Angle = -1.828125,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_304]],  
                [[Client1_543]]
              }
            },  
            {
              InstanceId = [[Client1_275]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_273]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Poisoned Kipucka]],  
              Position = {
                y = -2522.796875,  
                x = 26162.1875,  
                InstanceId = [[Client1_276]],  
                Class = [[Position]],  
                z = -12.9375
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_283]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_281]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Poisoned Kipucka]],  
              Position = {
                y = -2526.6875,  
                x = 26167.67188,  
                InstanceId = [[Client1_284]],  
                Class = [[Position]],  
                z = -12.84375
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_287]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_285]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]],  
              Position = {
                y = -2537.25,  
                x = 26164.15625,  
                InstanceId = [[Client1_288]],  
                Class = [[Position]],  
                z = -13.296875
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_291]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_289]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]],  
              Position = {
                y = -2535.671875,  
                x = 26168.98438,  
                InstanceId = [[Client1_292]],  
                Class = [[Position]],  
                z = -13.03125
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_271]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_269]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2531.546875,  
                x = 26170.79688,  
                InstanceId = [[Client1_272]],  
                Class = [[Position]],  
                z = -12.875
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_267]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_265]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2530.546875,  
                x = 26164.01563,  
                InstanceId = [[Client1_268]],  
                Class = [[Position]],  
                z = -12.890625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_295]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_293]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2532.625,  
                x = 26160.4375,  
                InstanceId = [[Client1_296]],  
                Class = [[Position]],  
                z = -12.890625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_299]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_297]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2535.796875,  
                x = 26159.5625,  
                InstanceId = [[Client1_300]],  
                Class = [[Position]],  
                z = -13.0625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_279]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_277]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]],  
              Position = {
                y = -2540.109375,  
                x = 26161.125,  
                InstanceId = [[Client1_280]],  
                Class = [[Position]],  
                z = -13.765625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_301]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_446]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Kinrey]],  
          Position = {
            y = -1.546875,  
            x = 3.0625,  
            InstanceId = [[Client1_445]],  
            Class = [[Position]],  
            z = -0.375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_444]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_418]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_416]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_571]],  
                    LogicEntityAction = [[Client1_565]],  
                    ActionStep = [[Client1_570]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_579]],  
                    LogicEntityAction = [[Client1_576]],  
                    ActionStep = [[Client1_578]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_588]],  
                    LogicEntityAction = [[Client1_556]],  
                    ActionStep = [[Client1_587]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_565]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_567]],  
                        Entity = r2.RefId([[Client1_469]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_566]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_557]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_570]],  
                        Entity = r2.RefId([[Client1_446]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_569]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_562]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_573]],  
                        Entity = r2.RefId([[Client1_492]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_572]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_559]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_564]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_532]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_533]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_494]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_562]],  
                    Name = [[Seq2]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_563]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kipurey Leader]],  
              Position = {
                y = -2385.046875,  
                x = 26032.79688,  
                InstanceId = [[Client1_419]],  
                Class = [[Position]],  
                z = -8.078125
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_532]],  
                [[Client1_562]]
              }
            },  
            {
              InstanceId = [[Client1_434]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_432]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -2403.71875,  
                x = 26001.21875,  
                InstanceId = [[Client1_435]],  
                Class = [[Position]],  
                z = -12.078125
              },  
              Angle = -3.0625,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_430]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_428]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]],  
              Position = {
                y = -2403.71875,  
                x = 26007.73438,  
                InstanceId = [[Client1_431]],  
                Class = [[Position]],  
                z = -10.953125
              },  
              Angle = 3.125,  
              Base = [[palette.entities.creatures.ckbif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_438]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_436]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -2412.03125,  
                x = 26000.20313,  
                InstanceId = [[Client1_439]],  
                Class = [[Position]],  
                z = -11.28125
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_426]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_424]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]],  
              Position = {
                y = -2412.59375,  
                x = 26006.46875,  
                InstanceId = [[Client1_427]],  
                Class = [[Position]],  
                z = -10
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.creatures.ckbif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_442]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_440]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -2418.359375,  
                x = 25996.6875,  
                InstanceId = [[Client1_443]],  
                Class = [[Position]],  
                z = -8.328125
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_469]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Kizarak]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_468]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_467]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_422]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_420]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_568]],  
                    LogicEntityAction = [[Client1_565]],  
                    ActionStep = [[Client1_567]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_582]],  
                    LogicEntityAction = [[Client1_576]],  
                    ActionStep = [[Client1_581]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_591]],  
                    LogicEntityAction = [[Client1_556]],  
                    ActionStep = [[Client1_590]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_556]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_587]],  
                        Entity = r2.RefId([[Client1_446]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_586]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_562]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_590]],  
                        Entity = r2.RefId([[Client1_469]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_589]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_557]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_593]],  
                        Entity = r2.RefId([[Client1_492]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_592]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_559]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_555]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_530]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_531]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_494]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_557]],  
                    Name = [[Seq2]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_558]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kipurak Leader]],  
              Position = {
                y = -2376.109375,  
                x = 26037.90625,  
                InstanceId = [[Client1_423]],  
                Class = [[Position]],  
                z = -8.34375
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_530]],  
                [[Client1_557]]
              }
            },  
            {
              InstanceId = [[Client1_453]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_451]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]],  
              Position = {
                y = -2394.90625,  
                x = 26005.28125,  
                InstanceId = [[Client1_454]],  
                Class = [[Position]],  
                z = -11.515625
              },  
              Angle = -2.75,  
              Base = [[palette.entities.creatures.ckcif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_465]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_463]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kizarak]],  
              Position = {
                y = -2400.21875,  
                x = 25999.9375,  
                InstanceId = [[Client1_466]],  
                Class = [[Position]],  
                z = -13.828125
              },  
              Angle = -2.453125,  
              Base = [[palette.entities.creatures.ckcif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_461]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_459]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kizarak]],  
              Position = {
                y = -2395.875,  
                x = 25996.98438,  
                InstanceId = [[Client1_462]],  
                Class = [[Position]],  
                z = -12.34375
              },  
              Angle = -2.28125,  
              Base = [[palette.entities.creatures.ckcif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_449]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_447]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]],  
              Position = {
                y = -2390.734375,  
                x = 25995.0625,  
                InstanceId = [[Client1_450]],  
                Class = [[Position]],  
                z = -11.453125
              },  
              Angle = -2.1875,  
              Base = [[palette.entities.creatures.ckcif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_457]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_455]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kizarak]],  
              Position = {
                y = -2394.40625,  
                x = 25989.76563,  
                InstanceId = [[Client1_458]],  
                Class = [[Position]],  
                z = -11.3125
              },  
              Angle = -1.984375,  
              Base = [[palette.entities.creatures.ckcif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_492]],  
          ActivitiesId = {
            [[Client1_526]],  
            [[Client1_527]]
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Kipesta]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_491]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_490]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_414]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_412]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_574]],  
                    LogicEntityAction = [[Client1_565]],  
                    ActionStep = [[Client1_573]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_585]],  
                    LogicEntityAction = [[Client1_576]],  
                    ActionStep = [[Client1_584]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_594]],  
                    LogicEntityAction = [[Client1_556]],  
                    ActionStep = [[Client1_593]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_576]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_578]],  
                        Entity = r2.RefId([[Client1_446]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_577]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_562]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_581]],  
                        Entity = r2.RefId([[Client1_469]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_580]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_557]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_584]],  
                        Entity = r2.RefId([[Client1_492]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_583]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_559]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_575]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_528]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_529]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_494]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_559]],  
                    Name = [[Seq2]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_560]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kipusta Leader]],  
              Position = {
                y = -2379.78125,  
                x = 26027.45313,  
                InstanceId = [[Client1_415]],  
                Class = [[Position]],  
                z = -9.140625
              },  
              Angle = -2.375,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_528]],  
                [[Client1_559]]
              }
            },  
            {
              InstanceId = [[Client1_472]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_470]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipesta]],  
              Position = {
                y = -2410.5,  
                x = 25999.3125,  
                InstanceId = [[Client1_473]],  
                Class = [[Position]],  
                z = -11.890625
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckjif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_476]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_474]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipesta]],  
              Position = {
                y = -2400.40625,  
                x = 25994.15625,  
                InstanceId = [[Client1_477]],  
                Class = [[Position]],  
                z = -13.453125
              },  
              Angle = -2.75,  
              Base = [[palette.entities.creatures.ckjif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_488]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_486]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipesta]],  
              Position = {
                y = -2411.984375,  
                x = 25995.21875,  
                InstanceId = [[Client1_489]],  
                Class = [[Position]],  
                z = -12.1875
              },  
              Angle = 2.703125,  
              Base = [[palette.entities.creatures.ckjif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_484]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_482]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipesta]],  
              Position = {
                y = -2407.8125,  
                x = 25990.57813,  
                InstanceId = [[Client1_485]],  
                Class = [[Position]],  
                z = -11.21875
              },  
              Angle = -2.75,  
              Base = [[palette.entities.creatures.ckjif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_480]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_478]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipesta]],  
              Position = {
                y = -2409.78125,  
                x = 25992.04688,  
                InstanceId = [[Client1_481]],  
                Class = [[Position]],  
                z = -11.46875
              },  
              Angle = -2.75,  
              Base = [[palette.entities.creatures.ckjif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 2,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act I: The Ki-Teens]],  
      ActivitiesIds = {
        [[Client1_534]],  
        [[Client1_536]],  
        [[Client1_543]],  
        [[Client1_557]],  
        [[Client1_559]],  
        [[Client1_562]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_396]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_394]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Wall]],  
              Position = {
                y = -2425.390625,  
                x = 25974.10938,  
                InstanceId = [[Client1_397]],  
                Class = [[Position]],  
                z = -8.515625
              },  
              Angle = -2.9375,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_408]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_406]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Wall]],  
              Position = {
                y = -2417.203125,  
                x = 25970.92188,  
                InstanceId = [[Client1_409]],  
                Class = [[Position]],  
                z = -10.484375
              },  
              Angle = -1.796875,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client1_410]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Version = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_411]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Title = [[Act II: Zo-Ki-Teen]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}