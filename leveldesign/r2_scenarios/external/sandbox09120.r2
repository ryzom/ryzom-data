scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_547]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_549]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_592]],  
        [[Client1_620]],  
        [[Client1_626]],  
        [[Client1_632]],  
        [[Client1_638]],  
        [[Client1_706]],  
        [[Client1_771]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_556]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_554]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spawn Decoration]],  
              Position = {
                y = -1994.6875,  
                x = 26951.15625,  
                InstanceId = [[Client1_557]],  
                Class = [[Position]],  
                z = -7.625
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_564]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_562]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kitin Sandbox]],  
              Position = {
                y = -2032.734375,  
                x = 27076.71875,  
                InstanceId = [[Client1_565]],  
                Class = [[Position]],  
                z = 14.96875
              },  
              Angle = 2.59375,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_568]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_566]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              Position = {
                y = -2020.375,  
                x = 26942.40625,  
                InstanceId = [[Client1_569]],  
                Class = [[Position]],  
                z = -2.09375
              },  
              Angle = 1.069644094,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kami Region]],  
              InstanceId = [[Client1_579]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_581]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2015.015625,  
                    x = 26941.07813,  
                    InstanceId = [[Client1_582]],  
                    Class = [[Position]],  
                    z = -3.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_584]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1990.53125,  
                    x = 26935.17188,  
                    InstanceId = [[Client1_585]],  
                    Class = [[Position]],  
                    z = -7.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_587]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1975.796875,  
                    x = 26960.73438,  
                    InstanceId = [[Client1_588]],  
                    Class = [[Position]],  
                    z = -8.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_590]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2001.890625,  
                    x = 26966.4375,  
                    InstanceId = [[Client1_591]],  
                    Class = [[Position]],  
                    z = -7.921875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_578]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_614]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_612]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[This is Spawn]],  
              Position = {
                y = -2019.421875,  
                x = 26956.54688,  
                InstanceId = [[Client1_615]],  
                Class = [[Position]],  
                z = -8.890625
              },  
              Angle = -1.046875,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_618]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_616]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_620]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_621]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spectral Genius 1]],  
              Position = {
                y = -2000.171875,  
                x = 26963.09375,  
                InstanceId = [[Client1_619]],  
                Class = [[Position]],  
                z = -8.1875
              },  
              Angle = -0.28125,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              ActivitiesId = {
                [[Client1_620]]
              }
            },  
            {
              InstanceId = [[Client1_624]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_622]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_626]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_627]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spectral Genius 2]],  
              Position = {
                y = -1979.53125,  
                x = 26956.71875,  
                InstanceId = [[Client1_625]],  
                Class = [[Position]],  
                z = -7.765625
              },  
              Angle = 0.984375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              ActivitiesId = {
                [[Client1_626]]
              }
            },  
            {
              InstanceId = [[Client1_630]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_628]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_632]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_633]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spectral Genius 3]],  
              Position = {
                y = -1991.234375,  
                x = 26940.03125,  
                InstanceId = [[Client1_631]],  
                Class = [[Position]],  
                z = -7.671875
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              ActivitiesId = {
                [[Client1_632]]
              }
            },  
            {
              InstanceId = [[Client1_636]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_634]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_638]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_639]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spectral Genius 4]],  
              Position = {
                y = -2001.09375,  
                x = 26943.23438,  
                InstanceId = [[Client1_637]],  
                Class = [[Position]],  
                z = -7.71875
              },  
              Angle = -2.03125,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              ActivitiesId = {
                [[Client1_638]]
              }
            },  
            {
              InstanceId = [[Client1_681]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_679]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan gateway 1]],  
              Position = {
                y = -1888.71875,  
                x = 26955.85938,  
                InstanceId = [[Client1_682]],  
                Class = [[Position]],  
                z = -6.6875
              },  
              Angle = -1.109375,  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_685]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_683]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 1]],  
              Position = {
                y = -1883.9375,  
                x = 26958.875,  
                InstanceId = [[Client1_686]],  
                Class = [[Position]],  
                z = -6.953125
              },  
              Angle = -0.140625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_689]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_687]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 2]],  
              Position = {
                y = -1888.078125,  
                x = 26950.0625,  
                InstanceId = [[Client1_690]],  
                Class = [[Position]],  
                z = -6.75
              },  
              Angle = 1.1875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_693]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_691]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 3]],  
              Position = {
                y = -1882.34375,  
                x = 26946.51563,  
                InstanceId = [[Client1_694]],  
                Class = [[Position]],  
                z = -6.953125
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_697]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_695]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 4]],  
              Position = {
                y = -1877.109375,  
                x = 26956.46875,  
                InstanceId = [[Client1_698]],  
                Class = [[Position]],  
                z = -7.609375
              },  
              Angle = -2.328125,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_701]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_699]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 5]],  
              Position = {
                y = -1876.3125,  
                x = 26950.20313,  
                InstanceId = [[Client1_702]],  
                Class = [[Position]],  
                z = -7.078125
              },  
              Angle = -1.046875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_775]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 10,  
              GabaritHeight = 5,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 9,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_773]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 6699566,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 7,  
              JacketModel = 6701870,  
              WeaponRightHand = 6756398,  
              ArmColor = 2,  
              Name = [[desert guard 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_776]],  
                x = 26952.67188,  
                y = -1976.84375,  
                z = -7.515625
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 23
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_779]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 6,  
              HandsModel = 6700078,  
              FeetColor = 5,  
              GabaritBreastSize = 4,  
              GabaritHeight = 11,  
              HairColor = 2,  
              EyesColor = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_777]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756142,  
              ArmColor = 0,  
              Name = [[desert guard 2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_780]],  
                x = 26965.20313,  
                y = -1992.8125,  
                z = -7.71875
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_783]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 1,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 13,  
              HairColor = 5,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_781]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756654,  
              ArmColor = 4,  
              Name = [[desert guard 3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_784]],  
                x = 26945.82813,  
                y = -2014.28125,  
                z = -4.734375
              },  
              Sex = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 5,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_787]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 8,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 12,  
              GabaritHeight = 11,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_785]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 5,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 0,  
              JacketModel = 6701870,  
              WeaponRightHand = 6755886,  
              ArmColor = 5,  
              Name = [[desert guard 4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_788]],  
                x = 26933.25,  
                y = -1997.046875,  
                z = -8.125
              },  
              Sex = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 6,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_791]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 10,  
              HandsModel = 6699822,  
              FeetColor = 2,  
              GabaritBreastSize = 13,  
              GabaritHeight = 13,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 13,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_789]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              JacketModel = 6701870,  
              WeaponRightHand = 6756910,  
              ArmColor = 4,  
              Name = [[desert guard 5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_792]],  
                x = 26948.32813,  
                y = -1979.171875,  
                z = -7.09375
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 6,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_795]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 6700078,  
              FeetColor = 5,  
              GabaritBreastSize = 8,  
              GabaritHeight = 1,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 8,  
              HandsColor = 1,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_793]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 5,  
              JacketModel = 6701870,  
              WeaponRightHand = 6756654,  
              ArmColor = 2,  
              Name = [[desert guard 6]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_796]],  
                x = 26962.34375,  
                y = -1978.390625,  
                z = -8.375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 0,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_799]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 2,  
              HandsModel = 6699822,  
              FeetColor = 5,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_797]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 3,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755886,  
              ArmColor = 0,  
              Name = [[desert guard 7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_800]],  
                x = 26957.54688,  
                y = -1974.21875,  
                z = -8.171875
              },  
              Sex = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 1,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_803]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 2,  
              HandsModel = 6699822,  
              FeetColor = 0,  
              GabaritBreastSize = 12,  
              GabaritHeight = 14,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 14,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_801]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 6,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756142,  
              ArmColor = 0,  
              Name = [[desert guard 8]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_804]],  
                x = 26925.45313,  
                y = -1986.40625,  
                z = -7.046875
              },  
              Sex = 1,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_807]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 0,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 9,  
              GabaritHeight = 11,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 13,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_805]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755118,  
              ArmColor = 2,  
              Name = [[desert guard 9]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_808]],  
                x = 26963.21875,  
                y = -2006.546875,  
                z = -8.578125
              },  
              Sex = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 6,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_811]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 5,  
              HandsModel = 6700078,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 1,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_809]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 6699310,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755118,  
              ArmColor = 1,  
              Name = [[desert guard 10]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_812]],  
                x = 26929.29688,  
                y = -1988,  
                z = -7.25
              },  
              Sex = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_815]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 3,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 12,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_813]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 2,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755374,  
              ArmColor = 1,  
              Name = [[desert guard 11]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_816]],  
                x = 26942.04688,  
                y = -1982.546875,  
                z = -6.890625
              },  
              Sex = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 2,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_819]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 2,  
              HandsModel = 6699822,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 6,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 2,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_817]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              JacketModel = 6701870,  
              WeaponRightHand = 6755374,  
              ArmColor = 4,  
              Name = [[desert guard 12]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_820]],  
                x = 26952.51563,  
                y = -2010.921875,  
                z = -7.828125
              },  
              Sex = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 0,  
              Tattoo = 15
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_823]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 13,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 11,  
              GabaritHeight = 3,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_821]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 0,  
              FeetModel = 6699566,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 7,  
              JacketModel = 6701870,  
              WeaponRightHand = 6755886,  
              ArmColor = 2,  
              Name = [[desert guard 13]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_824]],  
                x = 26963.3125,  
                y = -1983.75,  
                z = -7.859375
              },  
              Sex = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 1,  
              Tattoo = 14
            }
          },  
          InstanceId = [[Client1_551]]
        },  
        {
          InstanceId = [[Client1_705]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Karavan Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_704]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_674]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_672]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_706]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_707]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[voice of Jena 1]],  
              Position = {
                y = -1885.625,  
                x = 26954,  
                InstanceId = [[Client1_675]],  
                Class = [[Position]],  
                z = -6.859375
              },  
              Angle = -1.109375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              ActivitiesId = {
                [[Client1_706]]
              }
            },  
            {
              InstanceId = [[Client1_646]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_644]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 1]],  
              Position = {
                y = -1879.015625,  
                x = 26953.45313,  
                InstanceId = [[Client1_647]],  
                Class = [[Position]],  
                z = -7.1875
              },  
              Angle = -1.640625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_650]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_648]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 2]],  
              Position = {
                y = -1879.46875,  
                x = 26954.625,  
                InstanceId = [[Client1_651]],  
                Class = [[Position]],  
                z = -7.25
              },  
              Angle = -1.640625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_654]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_652]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 3]],  
              Position = {
                y = -1879.28125,  
                x = 26952,  
                InstanceId = [[Client1_655]],  
                Class = [[Position]],  
                z = -7.078125
              },  
              Angle = -1.921875,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_658]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_656]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 4]],  
              Position = {
                y = -1880.6875,  
                x = 26950.57813,  
                InstanceId = [[Client1_659]],  
                Class = [[Position]],  
                z = -6.984375
              },  
              Angle = -1.640625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_662]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_660]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 5]],  
              Position = {
                y = -1882.21875,  
                x = 26949.67188,  
                InstanceId = [[Client1_663]],  
                Class = [[Position]],  
                z = -6.90625
              },  
              Angle = -1.53125,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_666]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_664]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 6]],  
              Position = {
                y = -1884.21875,  
                x = 26948.76563,  
                InstanceId = [[Client1_667]],  
                Class = [[Position]],  
                z = -6.875
              },  
              Angle = -1.140625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_670]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_668]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 7]],  
              Position = {
                y = -1886.21875,  
                x = 26948.73438,  
                InstanceId = [[Client1_671]],  
                Class = [[Position]],  
                z = -6.78125
              },  
              Angle = -0.890625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_703]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_770]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Kitin Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_769]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_710]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_708]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_771]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_772]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kipuckookoo]],  
              Position = {
                y = -2009.265625,  
                x = 27064.79688,  
                InstanceId = [[Client1_711]],  
                Class = [[Position]],  
                z = 14.75
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_771]]
              }
            },  
            {
              InstanceId = [[Client1_718]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_716]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -2004.640625,  
                x = 27056.98438,  
                InstanceId = [[Client1_719]],  
                Class = [[Position]],  
                z = 14.59375
              },  
              Angle = 2.875,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_714]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_712]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -2010.296875,  
                x = 27056.1875,  
                InstanceId = [[Client1_715]],  
                Class = [[Position]],  
                z = 14.65625
              },  
              Angle = 2.875,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_730]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_728]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              Position = {
                y = -2008.59375,  
                x = 27049.67188,  
                InstanceId = [[Client1_731]],  
                Class = [[Position]],  
                z = 12.546875
              },  
              Angle = 2.015625,  
              Base = [[palette.entities.creatures.ckeif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_726]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_724]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              Position = {
                y = -2000.984375,  
                x = 27051.54688,  
                InstanceId = [[Client1_727]],  
                Class = [[Position]],  
                z = 12.59375
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.creatures.ckeif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_722]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_720]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              Position = {
                y = -1995.03125,  
                x = 27052.15625,  
                InstanceId = [[Client1_723]],  
                Class = [[Position]],  
                z = 12.53125
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.creatures.ckeif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_766]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_764]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -2051.125,  
                x = 27074.39063,  
                InstanceId = [[Client1_767]],  
                Class = [[Position]],  
                z = 14.015625
              },  
              Angle = -2.65625,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_762]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_760]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -2044.625,  
                x = 27083.78125,  
                InstanceId = [[Client1_763]],  
                Class = [[Position]],  
                z = 14.90625
              },  
              Angle = -1.890625,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_758]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_756]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -2039.90625,  
                x = 27091.45313,  
                InstanceId = [[Client1_759]],  
                Class = [[Position]],  
                z = 14.25
              },  
              Angle = 0.75,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_754]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_752]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -2034.328125,  
                x = 27094.54688,  
                InstanceId = [[Client1_755]],  
                Class = [[Position]],  
                z = 14.296875
              },  
              Angle = 1.65625,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_750]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_748]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -2027.15625,  
                x = 27093.20313,  
                InstanceId = [[Client1_751]],  
                Class = [[Position]],  
                z = 13.796875
              },  
              Angle = 1.65625,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_746]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_744]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -2021.265625,  
                x = 27067.76563,  
                InstanceId = [[Client1_747]],  
                Class = [[Position]],  
                z = 15.3125
              },  
              Angle = -0.515625,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_742]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_740]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -2017.3125,  
                x = 27070.79688,  
                InstanceId = [[Client1_743]],  
                Class = [[Position]],  
                z = 14.421875
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_738]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_736]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -2025.0625,  
                x = 27064.60938,  
                InstanceId = [[Client1_739]],  
                Class = [[Position]],  
                z = 15.359375
              },  
              Angle = -1.84375,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_734]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_732]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -2030.828125,  
                x = 27061.39063,  
                InstanceId = [[Client1_735]],  
                Class = [[Position]],  
                z = 14.90625
              },  
              Angle = -1.484375,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_768]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_550]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_548]],  
    Texts = {
    }
  }
}