scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManagerEntry = 0,  
    TextManager = 0,  
    ActivityStep = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client1_184]],  
        [[Client1_186]],  
        [[Client1_188]],  
        [[Client2_92]],  
        [[Client2_94]],  
        [[Client2_96]],  
        [[Client1_230]],
        [[Client2_510]]
      },  
      Title = [[Act 0]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client2_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_13]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_510]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_511]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scowling Gingo]],  
              Position = {
                y = -1971.078125,  
                x = 22612.1875,  
                InstanceId = [[Client2_16]],  
                Class = [[Position]],  
                z = 22.875
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.creatures.ccadb3]],  
              ActivitiesId = {
                [[Client2_510]]
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 18,  
        InstanceId = [[Client1_328]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_329]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_330]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_331]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_332]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_344]],  
        Class = [[TextManagerEntry]],  
        Text = [[These are dire times, I fear that we might face hard times to come.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_345]],  
        Class = [[TextManagerEntry]],  
        Text = [[I fear that the others might not do well now, I wonder how they are...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_349]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_350]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_355]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_356]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_357]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_293]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_294]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_366]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_295]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_296]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_364]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_369]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_370]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_371]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need... (should be "to see")]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_372]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_373]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client2_381]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitins are coming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_384]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_385]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_386]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_387]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_390]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_391]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_394]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_395]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_399]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_400]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_401]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_402]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_407]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_408]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_439]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have a huge problem!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_440]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have a huge problem!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_462]],  
        Class = [[TextManagerEntry]],  
        Text = [[What happened?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_463]],  
        Class = [[TextManagerEntry]],  
        Text = [[What happened?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_466]],  
        Class = [[TextManagerEntry]],  
        Text = [[There might be Kitins incoming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_467]],  
        Class = [[TextManagerEntry]],  
        Text = [[There might be Kitins incoming!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_470]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Not the Kitins!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_471]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Not the Kitins!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_475]],  
        Class = [[TextManagerEntry]],  
        Text = [[*I keep hearing something coming from the tunnel...*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_476]],  
        Class = [[TextManagerEntry]],  
        Text = [[*I keep hearing something coming from the tunnel...*]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_479]],  
        Class = [[TextManagerEntry]],  
        Text = [[*Could it be ... ?!*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_480]],  
        Class = [[TextManagerEntry]],  
        Text = [[*Could it be ... ?!*]]
      }
    }
  }
}