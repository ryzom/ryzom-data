scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_8]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_10]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    ActivityStep = 0,  
    NpcCustom = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
        [[Client1_215]],  
        [[Client1_380]],  
        [[Client1_382]],  
        [[Client1_390]],  
        [[Client1_396]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_33]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_31]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Circle Thingy]],  
              Position = {
                y = -2023.703125,  
                x = 26933.98438,  
                InstanceId = [[Client1_34]],  
                Class = [[Position]],  
                z = -1.578125
              },  
              Angle = 1.0625,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_37]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_35]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Circle Bones]],  
              Position = {
                y = -2023.21875,  
                x = 26940.89063,  
                InstanceId = [[Client1_38]],  
                Class = [[Position]],  
                z = -1.625
              },  
              Angle = 1.6875,  
              Base = [[palette.entities.botobjects.bones]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_41]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_39]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 1]],  
              Position = {
                y = -2023.640625,  
                x = 26933.8125,  
                InstanceId = [[Client1_42]],  
                Class = [[Position]],  
                z = -1.65625
              },  
              Angle = -1.71875,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              Position = {
                y = -1999.0625,  
                x = 26880.67188,  
                InstanceId = [[Client1_46]],  
                Class = [[Position]],  
                z = -10.8125
              },  
              Angle = 0.171875,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_53]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_51]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              Position = {
                y = -2031.46875,  
                x = 27074.79688,  
                InstanceId = [[Client1_54]],  
                Class = [[Position]],  
                z = 14.5
              },  
              Angle = 2.375,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_57]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_55]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              Position = {
                y = -1957.875,  
                x = 26977.71875,  
                InstanceId = [[Client1_58]],  
                Class = [[Position]],  
                z = -3.125
              },  
              Angle = 2.203125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_61]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_59]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              Position = {
                y = -1963.703125,  
                x = 26977.67188,  
                InstanceId = [[Client1_62]],  
                Class = [[Position]],  
                z = -5.828125
              },  
              Angle = -3.046875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_65]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_63]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              Position = {
                y = -1958.703125,  
                x = 26971.5,  
                InstanceId = [[Client1_66]],  
                Class = [[Position]],  
                z = -4.765625
              },  
              Angle = 2.8125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_69]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_67]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              Position = {
                y = -1964.5,  
                x = 26973.26563,  
                InstanceId = [[Client1_70]],  
                Class = [[Position]],  
                z = -7.21875
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_73]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_71]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 5]],  
              Position = {
                y = -2040.25,  
                x = 27005.5625,  
                InstanceId = [[Client1_74]],  
                Class = [[Position]],  
                z = -5.953125
              },  
              Angle = 1.765625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_77]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_75]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 6]],  
              Position = {
                y = -2048.53125,  
                x = 27006.5625,  
                InstanceId = [[Client1_78]],  
                Class = [[Position]],  
                z = -9.15625
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_81]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_79]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 7]],  
              Position = {
                y = -2041.796875,  
                x = 27000.84375,  
                InstanceId = [[Client1_82]],  
                Class = [[Position]],  
                z = -8.59375
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_85]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_83]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 1]],  
              Position = {
                y = -1973.109375,  
                x = 26970.32813,  
                InstanceId = [[Client1_86]],  
                Class = [[Position]],  
                z = -9.515625
              },  
              Angle = -3.03125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_89]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_87]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 2]],  
              Position = {
                y = -1978.234375,  
                x = 26970.89063,  
                InstanceId = [[Client1_90]],  
                Class = [[Position]],  
                z = -8.953125
              },  
              Angle = 0.078125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_93]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_91]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 3]],  
              Position = {
                y = -1983.578125,  
                x = 26971.34375,  
                InstanceId = [[Client1_94]],  
                Class = [[Position]],  
                z = -8.171875
              },  
              Angle = -3.03125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_97]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_95]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 4]],  
              Position = {
                y = -1988.578125,  
                x = 26972.46875,  
                InstanceId = [[Client1_98]],  
                Class = [[Position]],  
                z = -7.546875
              },  
              Angle = -5.953125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_101]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_99]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 5]],  
              Position = {
                y = -1993.703125,  
                x = 26974.25,  
                InstanceId = [[Client1_102]],  
                Class = [[Position]],  
                z = -6.734375
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_105]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_103]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 6]],  
              Position = {
                y = -1998.703125,  
                x = 26976.32813,  
                InstanceId = [[Client1_106]],  
                Class = [[Position]],  
                z = -5.65625
              },  
              Angle = -5.828125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_109]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_107]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 7]],  
              Position = {
                y = -2003.546875,  
                x = 26978.70313,  
                InstanceId = [[Client1_110]],  
                Class = [[Position]],  
                z = -4.3125
              },  
              Angle = -2.609375,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_113]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_111]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 8]],  
              Position = {
                y = -2008.015625,  
                x = 26981.3125,  
                InstanceId = [[Client1_114]],  
                Class = [[Position]],  
                z = -3.0625
              },  
              Angle = -5.640625,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_117]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_115]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 9]],  
              Position = {
                y = -2012.265625,  
                x = 26984.46875,  
                InstanceId = [[Client1_118]],  
                Class = [[Position]],  
                z = -2.140625
              },  
              Angle = -2.453125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_121]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_119]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 10]],  
              Position = {
                y = -2016.3125,  
                x = 26987.75,  
                InstanceId = [[Client1_122]],  
                Class = [[Position]],  
                z = -1.75
              },  
              Angle = -5.453125,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_125]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_123]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 11]],  
              Position = {
                y = -2020.21875,  
                x = 26990.96875,  
                InstanceId = [[Client1_126]],  
                Class = [[Position]],  
                z = -2.546875
              },  
              Angle = -2.46875,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_129]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_127]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 12]],  
              Position = {
                y = -2024.140625,  
                x = 26994.32813,  
                InstanceId = [[Client1_130]],  
                Class = [[Position]],  
                z = -4.375
              },  
              Angle = -5.625,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_133]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_131]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 13]],  
              Position = {
                y = -2028.15625,  
                x = 26997.57813,  
                InstanceId = [[Client1_134]],  
                Class = [[Position]],  
                z = -5.859375
              },  
              Angle = -2.484375,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_137]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_135]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[living wall 14]],  
              Position = {
                y = -2032.140625,  
                x = 27000.75,  
                InstanceId = [[Client1_138]],  
                Class = [[Position]],  
                z = -6.0625
              },  
              Angle = -5.625,  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Guard Zone]],  
              InstanceId = [[Client1_140]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_142]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1973.046875,  
                    x = 26971.53125,  
                    InstanceId = [[Client1_143]],  
                    Class = [[Position]],  
                    z = -9.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_145]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1969.265625,  
                    x = 26984.1875,  
                    InstanceId = [[Client1_146]],  
                    Class = [[Position]],  
                    z = -6.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_148]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2026.4375,  
                    x = 27023.07813,  
                    InstanceId = [[Client1_149]],  
                    Class = [[Position]],  
                    z = 4.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_151]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2032.265625,  
                    x = 27003.48438,  
                    InstanceId = [[Client1_152]],  
                    Class = [[Position]],  
                    z = -4.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_154]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2015.15625,  
                    x = 26989.59375,  
                    InstanceId = [[Client1_155]],  
                    Class = [[Position]],  
                    z = -1.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_157]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1994.75,  
                    x = 26977.79688,  
                    InstanceId = [[Client1_158]],  
                    Class = [[Position]],  
                    z = -5.8125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_139]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_252]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_250]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              Position = {
                y = -1973.203125,  
                x = 26976.625,  
                InstanceId = [[Client1_253]],  
                Class = [[Position]],  
                z = -9.1875
              },  
              Angle = -2.28125,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_256]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_254]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 2]],  
              Position = {
                y = -2028.375,  
                x = 27004.48438,  
                InstanceId = [[Client1_257]],  
                Class = [[Position]],  
                z = -3.171875
              },  
              Angle = 2.09375,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_272]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_270]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo spot 1]],  
              Position = {
                y = -1918.140625,  
                x = 26960.34375,  
                InstanceId = [[Client1_273]],  
                Class = [[Position]],  
                z = -10.578125
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.botobjects.spot_goo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_276]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_274]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 1]],  
              Position = {
                y = -1920.5625,  
                x = 26952.71875,  
                InstanceId = [[Client1_277]],  
                Class = [[Position]],  
                z = -10.1875
              },  
              Angle = -1.5625,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_280]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_278]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 2]],  
              Position = {
                y = -1918.671875,  
                x = 26970.5,  
                InstanceId = [[Client1_281]],  
                Class = [[Position]],  
                z = -9.390625
              },  
              Angle = -2.75,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_284]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_282]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 8]],  
              Position = {
                y = -1909.21875,  
                x = 26948.76563,  
                InstanceId = [[Client1_285]],  
                Class = [[Position]],  
                z = -7.3125
              },  
              Angle = -1.53125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_288]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_286]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 9]],  
              Position = {
                y = -1909.375,  
                x = 26973.17188,  
                InstanceId = [[Client1_289]],  
                Class = [[Position]],  
                z = -7.578125
              },  
              Angle = -2.453125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_292]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_290]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 10]],  
              Position = {
                y = -1907.34375,  
                x = 26959.78125,  
                InstanceId = [[Client1_293]],  
                Class = [[Position]],  
                z = -7.65625
              },  
              Angle = -1.21875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_314]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_316]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2015.03125,  
                    x = 26943.21875,  
                    InstanceId = [[Client1_317]],  
                    Class = [[Position]],  
                    z = -3.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_319]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1995.5,  
                    x = 26954.79688,  
                    InstanceId = [[Client1_320]],  
                    Class = [[Position]],  
                    z = -7.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_322]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1970.984375,  
                    x = 26954.5,  
                    InstanceId = [[Client1_323]],  
                    Class = [[Position]],  
                    z = -7.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_325]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1980.9375,  
                    x = 26910.78125,  
                    InstanceId = [[Client1_326]],  
                    Class = [[Position]],  
                    z = -6.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_328]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2007.53125,  
                    x = 26908.15625,  
                    InstanceId = [[Client1_329]],  
                    Class = [[Position]],  
                    z = -7.984375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_313]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_332]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_330]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -2010.578125,  
                x = 26951.89063,  
                InstanceId = [[Client1_333]],  
                Class = [[Position]],  
                z = -7.65625
              },  
              Angle = 2.109375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_336]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_334]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -2001.140625,  
                x = 26956.46875,  
                InstanceId = [[Client1_337]],  
                Class = [[Position]],  
                z = -8.203125
              },  
              Angle = -2.890625,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_340]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_338]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6705198,  
              Angle = 2.578125,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6707246,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Tribe Leader]],  
              Position = {
                y = -2004.484375,  
                x = 26950.14063,  
                InstanceId = [[Client1_341]],  
                Class = [[Position]],  
                z = -7.765625
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 12
            }
          },  
          InstanceId = [[Client1_12]]
        },  
        {
          InstanceId = [[Client1_185]],  
          ActivitiesId = {
            [[Client1_390]]
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_184]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_183]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_169]],  
              ActivitiesId = {
                [[Client1_294]]
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_167]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_390]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_395]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_140]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Angle = 1.59375,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Guard Leader]],  
              Position = {
                y = -2000.03125,  
                x = 26996.59375,  
                InstanceId = [[Client1_170]],  
                Class = [[Position]],  
                z = 0.484375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_181]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_179]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 0.953125,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 1,  
              Name = [[Guard]],  
              Position = {
                y = -2002.515625,  
                x = 26995.0625,  
                InstanceId = [[Client1_182]],  
                Class = [[Position]],  
                z = 0.0625
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_177]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_175]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 1.234375,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 1,  
              Name = [[Guard]],  
              Position = {
                y = -2002.359375,  
                x = 26996.0625,  
                InstanceId = [[Client1_178]],  
                Class = [[Position]],  
                z = 0.34375
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_173]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_171]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 1.59375,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 1,  
              Name = [[Guard]],  
              Position = {
                y = -2002.640625,  
                x = 26996.54688,  
                InstanceId = [[Client1_174]],  
                Class = [[Position]],  
                z = 0.453125
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 3
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_247]],  
          ActivitiesId = {
            [[Client1_396]]
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_246]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_245]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_243]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_241]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_396]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_397]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_140]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              Position = {
                y = -2021.375,  
                x = 27062.51563,  
                InstanceId = [[Client1_244]],  
                Class = [[Position]],  
                z = 15.265625
              },  
              Angle = 1.46875,  
              Base = [[palette.entities.creatures.ckdie4]],  
              ActivitiesId = {
                [[Client1_296]]
              }
            },  
            {
              InstanceId = [[Client1_219]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_217]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              Position = {
                y = -2058.4375,  
                x = 27057.71875,  
                InstanceId = [[Client1_220]],  
                Class = [[Position]],  
                z = 4.53125
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.creatures.ckdie4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_227]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_225]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              Position = {
                y = -2050.265625,  
                x = 27062.75,  
                InstanceId = [[Client1_228]],  
                Class = [[Position]],  
                z = 10.15625
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.creatures.ckdie4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_231]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_229]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              Position = {
                y = -2042.0625,  
                x = 27058.375,  
                InstanceId = [[Client1_232]],  
                Class = [[Position]],  
                z = 14.25
              },  
              Angle = 1.3125,  
              Base = [[palette.entities.creatures.ckdie4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_235]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_233]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              Position = {
                y = -2035.671875,  
                x = 27060.71875,  
                InstanceId = [[Client1_236]],  
                Class = [[Position]],  
                z = 14.90625
              },  
              Angle = 2.015625,  
              Base = [[palette.entities.creatures.ckdie4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_239]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_237]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kincher]],  
              Position = {
                y = -2029.5,  
                x = 27060.09375,  
                InstanceId = [[Client1_240]],  
                Class = [[Position]],  
                z = 14.859375
              },  
              Angle = 1.015625,  
              Base = [[palette.entities.creatures.ckdie4]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_400]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_399]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_398]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_352]],  
              ActivitiesId = {
                [[Client1_382]]
              },  
              HairType = 5621550,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6724654,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_350]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_382]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_383]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_314]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -2.984375,  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6725678,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Nialeki]],  
              Position = {
                y = -1998,  
                x = 26943.625,  
                InstanceId = [[Client1_353]],  
                Class = [[Position]],  
                z = -7.9375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_376]],  
              ActivitiesId = {
                [[Client1_380]]
              },  
              HairType = 5621550,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6724654,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_374]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_380]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_381]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -2.859375,  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6725678,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Kyalani]],  
              Position = {
                y = -1996.5625,  
                x = 26942.57813,  
                InstanceId = [[Client1_377]],  
                Class = [[Position]],  
                z = -7.984375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_372]],  
              ActivitiesId = {
                [[Client1_378]]
              },  
              HairType = 5621550,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6724654,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_370]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_378]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_379]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -2.859375,  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6725678,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6728750,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Tylini]],  
              Position = {
                y = -1995.484375,  
                x = 26943.42188,  
                InstanceId = [[Client1_373]],  
                Class = [[Position]],  
                z = -7.921875
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 2
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_11]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_9]],  
    Texts = {
    }
  }
}