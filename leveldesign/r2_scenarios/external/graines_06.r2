scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_2554]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts13]],  
      Time = 0,  
      Name = [[Dunes of Temperance]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    },  
    {
      InstanceId = [[Client1_2559]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts20]],  
      Time = 0,  
      Name = [[Labyrinth]],  
      Season = [[summer]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_2545]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_2543]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  Name = [[Dangeureuses Graines]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8657966,  
      Name = [[Coffre Sacré des Kamis]],  
      InstanceId = [[Client1_2780]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[Ce coffre contient un trésor donné par le Maitre Kami du village il y a bien longtemps. Scellé par une puissante magie, la légende racontre que des esprits maléfiques ont enfermés dedans et qu'ouvrir ce coffre les referais vivre.]]
    }
  },  
  VersionName = [[0.1.0]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_2544]],  
    Class = [[Position]],  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    UserTrigger = 0,  
    EventType = 0,  
    ChatStep = 0,  
    TextManagerEntry = 0,  
    PlotItem = 0,  
    TalkTo = 0,  
    NpcGrpFeature = 0,  
    ActionStep = 0,  
    Location = 0,  
    LogicEntityBehavior = 0,  
    ConditionStep = 0,  
    Act = 3,  
    ChatSequence = 0,  
    ChatAction = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    Behavior = 0,  
    Region = 0,  
    ActivityStep = 1,  
    Npc = 0,  
    ConditionType = 0,  
    ZoneTrigger = 0,  
    RegionVertex = 0,  
    NpcCreature = 0,  
    WayPoint = 0,  
    Position = 0,  
    Timer = 0,  
    ActionType = 0,  
    Road = 0,  
    NpcCustom = 0
  },  
  Description = {
    InstanceId = [[Client1_2541]],  
    Class = [[MapDescription]],  
    LevelId = 2,  
    LocationId = 56,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[Dangeureuse Graine]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_2546]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 21,  
      InstanceId = [[Client1_2548]],  
      ManualWeather = 0,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_2547]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          InstanceId = [[Client1_2549]],  
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_2873]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2871]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami hut 1]],  
              Position = {
                y = -1913.34375,  
                x = 28945.45313,  
                InstanceId = [[Client1_2874]],  
                Class = [[Position]],  
                z = 74.546875
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2967]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2965]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -2056.984375,  
                x = 28825.75,  
                InstanceId = [[Client1_2968]],  
                Class = [[Position]],  
                z = 78.046875
              },  
              Angle = 5.109375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2983]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2981]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[dead camp fire 1]],  
              Position = {
                y = -2076.375,  
                x = 28840.75,  
                InstanceId = [[Client1_2984]],  
                Class = [[Position]],  
                z = 74.703125
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.campfire_out]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2987]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2985]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 1]],  
              Position = {
                y = -2069.5625,  
                x = 28847.17188,  
                InstanceId = [[Client1_2988]],  
                Class = [[Position]],  
                z = 74.390625
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_2999]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2997]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 3]],  
              Position = {
                y = -2120.015625,  
                x = 28820.32813,  
                InstanceId = [[Client1_3000]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3003]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3001]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 4]],  
              Position = {
                y = -2119.359375,  
                x = 28817.04688,  
                InstanceId = [[Client1_3004]],  
                Class = [[Position]],  
                z = 75.125
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3007]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3005]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 5]],  
              Position = {
                y = -2118.703125,  
                x = 28813.71875,  
                InstanceId = [[Client1_3008]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3011]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3009]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 6]],  
              Position = {
                y = -2118.046875,  
                x = 28810.46875,  
                InstanceId = [[Client1_3012]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3019]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3017]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 8]],  
              Position = {
                y = -2116.6875,  
                x = 28803.79688,  
                InstanceId = [[Client1_3020]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3023]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3021]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 9]],  
              Position = {
                y = -2116.078125,  
                x = 28800.5625,  
                InstanceId = [[Client1_3024]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = 1.390625,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3039]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3037]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 13]],  
              Position = {
                y = -2113.609375,  
                x = 28786.15625,  
                InstanceId = [[Client1_3040]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Angle = 1.421875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3043]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3041]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 14]],  
              Position = {
                y = -2113.15625,  
                x = 28782.84375,  
                InstanceId = [[Client1_3044]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = 1.4375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3047]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3045]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 15]],  
              Position = {
                y = -2112.71875,  
                x = 28779.53125,  
                InstanceId = [[Client1_3048]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = 1.4375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3051]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3049]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 16]],  
              Position = {
                y = -2112.328125,  
                x = 28776.20313,  
                InstanceId = [[Client1_3052]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = 1.46875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3055]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3053]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 17]],  
              Position = {
                y = -2112,  
                x = 28772.84375,  
                InstanceId = [[Client1_3056]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = 1.484375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3059]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3057]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 18]],  
              Position = {
                y = -2111.75,  
                x = 28769.54688,  
                InstanceId = [[Client1_3060]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = 1.5,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3063]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3061]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              Position = {
                y = -2118.03125,  
                x = 28821.98438,  
                InstanceId = [[Client1_3064]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = -0.390625,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3077]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3078]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 19]],  
              Position = {
                y = -2117.375,  
                x = 28807.125,  
                InstanceId = [[Client1_3079]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3082]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3080]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami hut 2]],  
              Position = {
                y = -2046.09375,  
                x = 28930.45313,  
                InstanceId = [[Client1_3083]],  
                Class = [[Position]],  
                z = 82
              },  
              Angle = 2.21875,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3086]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3084]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              Position = {
                y = -2086.296875,  
                x = 28798.5,  
                InstanceId = [[Client1_3087]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = 4.5625,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3094]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3092]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 2]],  
              Position = {
                y = -2111.65625,  
                x = 28770.4375,  
                InstanceId = [[Client1_3095]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = -1.359375,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Maraudeur 3]],  
              InstanceId = [[Client1_3669]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3671]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2246.921875,  
                    x = 28581.59375,  
                    InstanceId = [[Client1_3672]],  
                    Class = [[Position]],  
                    z = 74.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3674]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2248.984375,  
                    x = 28793.40625,  
                    InstanceId = [[Client1_3675]],  
                    Class = [[Position]],  
                    z = 74.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3680]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2203.109375,  
                    x = 28825.14063,  
                    InstanceId = [[Client1_3681]],  
                    Class = [[Position]],  
                    z = 75.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3683]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2166.359375,  
                    x = 28820.5,  
                    InstanceId = [[Client1_3684]],  
                    Class = [[Position]],  
                    z = 74.796875
                  }
                }
              },  
              Position = {
                y = 6.71875,  
                x = -25.9375,  
                InstanceId = [[Client1_3668]],  
                Class = [[Position]],  
                z = 0.171875
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Maraudeur 2]],  
              InstanceId = [[Client1_3908]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3910]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2243.796875,  
                    x = 28555.29688,  
                    InstanceId = [[Client1_3911]],  
                    Class = [[Position]],  
                    z = 74.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3913]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2245.578125,  
                    x = 28768.65625,  
                    InstanceId = [[Client1_3914]],  
                    Class = [[Position]],  
                    z = 74.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3916]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2196.0625,  
                    x = 28804.35938,  
                    InstanceId = [[Client1_3917]],  
                    Class = [[Position]],  
                    z = 75.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3919]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2160.203125,  
                    x = 28798.14063,  
                    InstanceId = [[Client1_3920]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                }
              },  
              Position = {
                y = -0.25,  
                x = -0.5625,  
                InstanceId = [[Client1_3907]],  
                Class = [[Position]],  
                z = -0.046875
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Maraudeur 1]],  
              InstanceId = [[Client1_3925]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3927]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2249.15625,  
                    x = 28555.09375,  
                    InstanceId = [[Client1_3928]],  
                    Class = [[Position]],  
                    z = 74.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3930]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2251.09375,  
                    x = 28770.21875,  
                    InstanceId = [[Client1_3931]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3933]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2197.265625,  
                    x = 28809.60938,  
                    InstanceId = [[Client1_3934]],  
                    Class = [[Position]],  
                    z = 75.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3936]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2161.265625,  
                    x = 28802.29688,  
                    InstanceId = [[Client1_3937]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                }
              },  
              Position = {
                y = 1.578125,  
                x = -0.34375,  
                InstanceId = [[Client1_3924]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Maraudeur 4]],  
              InstanceId = [[Client1_3942]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3944]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2233.390625,  
                    x = 28555.15625,  
                    InstanceId = [[Client1_3945]],  
                    Class = [[Position]],  
                    z = 75.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3947]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2235.78125,  
                    x = 28766,  
                    InstanceId = [[Client1_3948]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3950]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2192.8125,  
                    x = 28793.60938,  
                    InstanceId = [[Client1_3951]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3953]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2157.125,  
                    x = 28789.6875,  
                    InstanceId = [[Client1_3954]],  
                    Class = [[Position]],  
                    z = 73.3125
                  }
                }
              },  
              Position = {
                y = -2.5625,  
                x = 0.671875,  
                InstanceId = [[Client1_3941]],  
                Class = [[Position]],  
                z = -0.046875
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place Entree Village]],  
              InstanceId = [[Client1_4227]],  
              Class = [[Region]],  
              Position = {
                y = -2.28125,  
                x = 0.25,  
                InstanceId = [[Client1_4226]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4229]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2124.375,  
                    x = 28769.23438,  
                    InstanceId = [[Client1_4230]],  
                    Class = [[Position]],  
                    z = 74.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4232]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2135.09375,  
                    x = 28821.17188,  
                    InstanceId = [[Client1_4233]],  
                    Class = [[Position]],  
                    z = 74.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4235]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2106.25,  
                    x = 28829.04688,  
                    InstanceId = [[Client1_4236]],  
                    Class = [[Position]],  
                    z = 75.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4238]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2089.828125,  
                    x = 28770.20313,  
                    InstanceId = [[Client1_4239]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Xathusa]],  
              InstanceId = [[Client1_4385]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4384]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4387]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2249.5,  
                    x = 28582.17188,  
                    InstanceId = [[Client1_4388]],  
                    Class = [[Position]],  
                    z = 77.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4440]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2215.640625,  
                    x = 28648.6875,  
                    InstanceId = [[Client1_4441]],  
                    Class = [[Position]],  
                    z = 73.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4390]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2226.46875,  
                    x = 28755.79688,  
                    InstanceId = [[Client1_4391]],  
                    Class = [[Position]],  
                    z = 75.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4393]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2202.28125,  
                    x = 28780.65625,  
                    InstanceId = [[Client1_4394]],  
                    Class = [[Position]],  
                    z = 75.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4396]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2114.609375,  
                    x = 28793.04688,  
                    InstanceId = [[Client1_4397]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4399]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2069.328125,  
                    x = 28827.85938,  
                    InstanceId = [[Client1_4400]],  
                    Class = [[Position]],  
                    z = 75.578125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Garde Icarius]],  
              InstanceId = [[Client1_4482]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4481]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4484]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2251.828125,  
                    x = 28572.34375,  
                    InstanceId = [[Client1_4485]],  
                    Class = [[Position]],  
                    z = 74.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4487]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2240.484375,  
                    x = 28567.375,  
                    InstanceId = [[Client1_4488]],  
                    Class = [[Position]],  
                    z = 75
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place Yubos]],  
              InstanceId = [[Client1_4526]],  
              Class = [[Region]],  
              Position = {
                y = 0.75,  
                x = 15.140625,  
                InstanceId = [[Client1_4525]],  
                Class = [[Position]],  
                z = 4.765625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4528]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2252.140625,  
                    x = 28573.09375,  
                    InstanceId = [[Client1_4529]],  
                    Class = [[Position]],  
                    z = 73.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4531]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2256.90625,  
                    x = 28574.07813,  
                    InstanceId = [[Client1_4532]],  
                    Class = [[Position]],  
                    z = 73.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4534]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2257,  
                    x = 28570.96875,  
                    InstanceId = [[Client1_4535]],  
                    Class = [[Position]],  
                    z = 72.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4537]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2251.984375,  
                    x = 28569.75,  
                    InstanceId = [[Client1_4538]],  
                    Class = [[Position]],  
                    z = 74
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Patrouille Gardes]],  
              InstanceId = [[Client1_4756]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4758]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2072.8125,  
                    x = 28902.40625,  
                    InstanceId = [[Client1_4759]],  
                    Class = [[Position]],  
                    z = 75.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4761]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2062.078125,  
                    x = 28937.89063,  
                    InstanceId = [[Client1_4762]],  
                    Class = [[Position]],  
                    z = 78.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4764]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1996.796875,  
                    x = 28956.29688,  
                    InstanceId = [[Client1_4765]],  
                    Class = [[Position]],  
                    z = 74.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4767]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1927.15625,  
                    x = 28940.64063,  
                    InstanceId = [[Client1_4768]],  
                    Class = [[Position]],  
                    z = 74.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4770]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1915.078125,  
                    x = 28889.71875,  
                    InstanceId = [[Client1_4771]],  
                    Class = [[Position]],  
                    z = 75.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4773]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1940.46875,  
                    x = 28823.79688,  
                    InstanceId = [[Client1_4774]],  
                    Class = [[Position]],  
                    z = 77.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4776]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1989.921875,  
                    x = 28798.875,  
                    InstanceId = [[Client1_4777]],  
                    Class = [[Position]],  
                    z = 75.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4779]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2078.390625,  
                    x = 28821.35938,  
                    InstanceId = [[Client1_4780]],  
                    Class = [[Position]],  
                    z = 75.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4782]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2083.671875,  
                    x = 28875.0625,  
                    InstanceId = [[Client1_4783]],  
                    Class = [[Position]],  
                    z = 74.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4785]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2072.765625,  
                    x = 28902.51563,  
                    InstanceId = [[Client1_4786]],  
                    Class = [[Position]],  
                    z = 75.46875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4755]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Xathusa vers tours]],  
              InstanceId = [[Client1_4825]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4827]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2067.1875,  
                    x = 28828.5,  
                    InstanceId = [[Client1_4828]],  
                    Class = [[Position]],  
                    z = 76.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4830]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2060.015625,  
                    x = 28826.98438,  
                    InstanceId = [[Client1_4831]],  
                    Class = [[Position]],  
                    z = 77.703125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4824]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route Vers le Grand Maitre Kami]],  
              InstanceId = [[Client1_4936]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4935]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4938]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2096.796875,  
                    x = 28813.1875,  
                    InstanceId = [[Client1_4939]],  
                    Class = [[Position]],  
                    z = 75.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4941]],  
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4942]],  
                    x = 28899.25,  
                    y = -2049.71875,  
                    z = 79.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4944]],  
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_4945]],  
                    x = 28916.03125,  
                    y = -2040.9375,  
                    z = 82.171875
                  }
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_5014]],  
              Name = [[Route dans Tente Kami]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5016]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5017]],  
                    x = 28917.5,  
                    y = -2041.375,  
                    z = 82.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5019]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5020]],  
                    x = 28929.10938,  
                    y = -2044.671875,  
                    z = 82.03125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5013]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_5032]],  
              Name = [[Route sort de la tente du Kami]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5034]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5035]],  
                    x = 28930.67188,  
                    y = -2045.46875,  
                    z = 81.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5037]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5038]],  
                    x = 28922.1875,  
                    y = -2038.078125,  
                    z = 82.859375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5031]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_5065]],  
              Name = [[Route Repli General]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5067]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5068]],  
                    x = 28919.42188,  
                    y = -2046.59375,  
                    z = 81.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5070]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5071]],  
                    x = 28819.82813,  
                    y = -2088.921875,  
                    z = 75.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5073]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5074]],  
                    x = 28793.15625,  
                    y = -2116.8125,  
                    z = 74.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5076]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5077]],  
                    x = 28778.42188,  
                    y = -2208.390625,  
                    z = 75.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5079]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5080]],  
                    x = 28744.29688,  
                    y = -2229.34375,  
                    z = 75.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_5082]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_5083]],  
                    x = 28521.51563,  
                    y = -2238.390625,  
                    z = 71.65625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_5064]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          Ghosts = {
          }
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 63,  
      Behavior = {
        InstanceId = [[Client1_2550]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_4471]],  
            Actions = {
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_4473]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_4474]],  
                Entity = r2.RefId([[Client1_4479]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_4475]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_4476]],  
                Entity = r2.RefId([[Client1_4349]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_4550]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_4551]],  
                Entity = r2.RefId([[Client1_4512]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_4552]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_4553]],  
                Entity = r2.RefId([[Client1_4516]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_4554]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_4555]],  
                Entity = r2.RefId([[Client1_4523]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_4472]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[L'attaque du village]],  
      ActivitiesIds = {
      },  
      StaticCost = 2,  
      InstanceId = [[Client1_2552]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 796,  
      LocationId = [[Client1_2554]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_2551]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_2885]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2883]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              ActivitiesId = {
              },  
              Name = [[Hote Kami des Dunes Sauvages]],  
              Position = {
                y = -1916.734375,  
                x = 28943.48438,  
                InstanceId = [[Client1_2886]],  
                Class = [[Position]],  
                z = 74.703125
              },  
              BotAttackable = 0,  
              Angle = -2.21875,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              PlayerAttackable = 0
            },  
            {
              InstanceId = [[Client1_3090]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3088]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami banner 1]],  
              Position = {
                y = -2046.34375,  
                x = 28930.8125,  
                InstanceId = [[Client1_3091]],  
                Class = [[Position]],  
                z = 81.984375
              },  
              Angle = 2.328125,  
              Base = [[palette.entities.botobjects.banner_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3132]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3130]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -1955.34375,  
                x = 28860.46875,  
                InstanceId = [[Client1_3133]],  
                Class = [[Position]],  
                z = 76.5
              },  
              Angle = -3.03125,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_4349]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 5,  
              HandsModel = 6703150,  
              FeetColor = 2,  
              GabaritBreastSize = 6,  
              GabaritHeight = 6,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4347]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4404]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4405]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4406]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4407]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4385]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4821]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4822]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4823]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4825]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4835]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4582]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_4584]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_4585]],  
                        Entity = r2.RefId([[Client1_4574]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[deactivate]],  
                          InstanceId = [[Client1_4923]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_4924]],  
                        Entity = r2.RefId([[Client1_4918]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_4583]],  
                      Value = r2.RefId([[Client1_4407]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4839]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Deactivate]],  
                          InstanceId = [[Client1_4841]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_4842]],  
                        Entity = r2.RefId([[Client1_4349]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_4840]],  
                      Value = r2.RefId([[Client1_4823]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              SheetClient = [[basic_fyros_female.creature]],  
              FeetModel = 6702638,  
              Speed = 1,  
              Angle = 2.6875,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Level = 0,  
              ArmColor = 1,  
              BotAttackable = 1,  
              Sex = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              JacketModel = 6704686,  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              Name = [[Xathusa, Dompteuse de Yubos]],  
              Position = {
                y = -2254.078125,  
                x = 28575.92188,  
                InstanceId = [[Client1_4350]],  
                Class = [[Position]],  
                z = 75.6875
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 1,  
              MorphTarget3 = 1,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4479]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 6699822,  
              FeetColor = 0,  
              GabaritBreastSize = 13,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4477]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4492]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4494]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4495]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4496]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4482]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4541]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              SheetClient = [[basic_fyros_male.creature]],  
              FeetModel = 6699310,  
              Speed = 1,  
              Angle = 0.796875,  
              Base = [[palette.entities.npcs.guards.f_guard_145]],  
              WeaponLeftHand = 0,  
              ArmColor = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_d4.creature]],  
              WeaponRightHand = 6756142,  
              Level = 0,  
              Name = [[Garde Icarius]],  
              Position = {
                y = -2254.328125,  
                x = 28575.46875,  
                InstanceId = [[Client1_4480]],  
                Class = [[Position]],  
                z = 75.359375
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 20
            }
          },  
          InstanceId = [[Client1_2553]]
        },  
        {
          InstanceId = [[Client1_3342]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 1]],  
          Position = {
            y = -0.125,  
            x = -138.953125,  
            InstanceId = [[Client1_3341]],  
            Class = [[Position]],  
            z = -3.234375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3340]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_3338]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 13,  
              GabaritHeight = 2,  
              HairColor = 4,  
              EyesColor = 0,  
              AutoSpawn = 0,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3336]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3811]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3814]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3841]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3842]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3925]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4055]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4952]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4953]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4965]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5087]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5088]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5089]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              FeetModel = 6699310,  
              Speed = 1,  
              Angle = -0.0625,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_220]],  
              WeaponLeftHand = 6773294,  
              Level = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_tank_pierce_f2.creature]],  
              WeaponRightHand = 6753838,  
              ArmColor = 4,  
              Name = [[Chef d'Escouade Maraudeur]],  
              Position = {
                y = -2240.140625,  
                x = 28668.98438,  
                InstanceId = [[Client1_3339]],  
                Class = [[Position]],  
                z = 75.390625
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 2,  
              MorphTarget3 = 2,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_3302]],  
              ActivitiesId = {
              },  
              HairType = 8238,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 5,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 6,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 2,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 9,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3300]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 6734894,  
              Speed = 1,  
              Angle = 0.078125,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              SheetClient = [[basic_zorai_female.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 4,  
              JacketModel = 6736942,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 3,  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              WeaponRightHand = 6936622,  
              Level = 0,  
              Name = [[Mercenaire Maraudeur]],  
              Position = {
                y = -2237.015625,  
                x = 28662.67188,  
                InstanceId = [[Client1_3303]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              ArmModel = 6736430,  
              MorphTarget7 = 1,  
              MorphTarget3 = 2,  
              Tattoo = 5
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_3635]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 2]],  
          Position = {
            y = 0.5,  
            x = -146.125,  
            InstanceId = [[Client1_3634]],  
            Class = [[Position]],  
            z = -1.609375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3633]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_3380]],  
              ActivitiesId = {
              },  
              HairType = 8750,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 13,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 5,  
              HairColor = 1,  
              EyesColor = 1,  
              AutoSpawn = 0,  
              TrouserModel = 6730286,  
              GabaritLegsWidth = 11,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3381]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3815]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3818]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3843]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3844]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3908]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4056]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4966]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4967]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4968]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5090]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5091]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Repeat Road]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5092]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              SheetClient = [[basic_zorai_male.creature]],  
              FeetModel = 6734894,  
              Speed = 1,  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.bandits.z_mage_aoe_220]],  
              WeaponLeftHand = 0,  
              Level = 0,  
              JacketModel = 6736942,  
              Sex = 0,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_magic_aoe_electricity_f2.creature]],  
              WeaponRightHand = 6936622,  
              ArmColor = 4,  
              Name = [[Chef d'Escouade Maraudeur]],  
              Position = {
                y = -2240.828125,  
                x = 28675.9375,  
                InstanceId = [[Client1_3382]],  
                Class = [[Position]],  
                z = 75.21875
              },  
              ArmModel = 6736430,  
              MorphTarget7 = 2,  
              MorphTarget3 = 7,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_3570]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 13,  
              GabaritHeight = 2,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3571]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699310,  
              Speed = 1,  
              Angle = -0.0625,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              WeaponLeftHand = 6773294,  
              ArmColor = 4,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_tank_pierce_f2.creature]],  
              WeaponRightHand = 6753838,  
              Level = 0,  
              Name = [[Mercenaire Maraudeur]],  
              Position = {
                y = -2239.515625,  
                x = 28673.54688,  
                InstanceId = [[Client1_3572]],  
                Class = [[Position]],  
                z = 75
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 2,  
              MorphTarget3 = 2,  
              Tattoo = 28
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_3638]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 3]],  
          Position = {
            y = 1.0625,  
            x = -138,  
            InstanceId = [[Client1_3637]],  
            Class = [[Position]],  
            z = -2.390625
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3636]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_3440]],  
              ActivitiesId = {
              },  
              HairType = 6722094,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 12,  
              HandsModel = 6721582,  
              FeetColor = 4,  
              GabaritBreastSize = 11,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 1,  
              AutoSpawn = 0,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3441]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3819]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3822]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3845]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3846]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3942]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4057]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4969]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4970]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4971]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5093]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5094]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5095]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              FeetModel = 6721070,  
              Speed = 1,  
              Angle = -0.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_tank_220]],  
              WeaponLeftHand = 6775086,  
              Level = 0,  
              JacketModel = 6723630,  
              Sex = 0,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]],  
              WeaponRightHand = 6764846,  
              ArmColor = 4,  
              Name = [[Chef d'Escouade Maraudeur]],  
              Position = {
                y = -2240.6875,  
                x = 28664.23438,  
                InstanceId = [[Client1_3442]],  
                Class = [[Position]],  
                z = 75
              },  
              ArmModel = 6723118,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_3410]],  
              ActivitiesId = {
              },  
              HairType = 6722094,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 2,  
              HandsModel = 6721582,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3411]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              FeetModel = 6721070,  
              Speed = 1,  
              Angle = -0.421875,  
              Base = [[palette.entities.npcs.bandits.t_melee_tank_220]],  
              SheetClient = [[basic_tryker_male.creature]],  
              WeaponLeftHand = 6775086,  
              ArmColor = 4,  
              JacketModel = 6723630,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              Sheet = [[ring_melee_tank_slash_f2.creature]],  
              WeaponRightHand = 6764846,  
              Level = 0,  
              Name = [[Mercenaire Maraudeur]],  
              Position = {
                y = -2241.828125,  
                x = 28668.375,  
                InstanceId = [[Client1_3412]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              ArmModel = 6723118,  
              MorphTarget7 = 0,  
              MorphTarget3 = 7,  
              Tattoo = 19
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_3667]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 4 (Akilia)]],  
          Position = {
            y = 10.40625,  
            x = -134.90625,  
            InstanceId = [[Client1_3666]],  
            Class = [[Position]],  
            z = -2.59375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3665]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_3646]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 11,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              AutoSpawn = 0,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 5,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3647]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4797]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_4802]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_4803]],  
                        Entity = r2.RefId([[Client1_4801]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_4798]],  
                      Value = r2.RefId([[Client1_3848]]),  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_5009]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_5010]],  
                      Value = r2.RefId([[Client1_4058]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_5011]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_5012]],  
                        Entity = r2.RefId([[Client1_5001]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_5108]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5111]],  
                        Entity = r2.RefId([[Client1_3667]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5110]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5028]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5113]],  
                        Entity = r2.RefId([[Client1_5045]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5112]],  
                          Type = [[starts dialog]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_5109]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_4973]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_5115]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5118]],  
                        Entity = r2.RefId([[Client1_3342]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5117]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5087]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5120]],  
                        Entity = r2.RefId([[Client1_3635]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5119]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5090]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5122]],  
                        Entity = r2.RefId([[Client1_3638]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5121]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5093]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5124]],  
                        Entity = r2.RefId([[Client1_3667]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5123]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5027]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5126]],  
                        Entity = r2.RefId([[Client1_3960]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5125]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5098]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5128]],  
                        Entity = r2.RefId([[Client1_3984]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5127]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5101]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5130]],  
                        Entity = r2.RefId([[Client1_3991]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5129]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_5104]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_5116]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_5042]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_5132]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_5135]],  
                        Entity = r2.RefId([[Client1_2552]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_5134]],  
                          Type = [[Start Act]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_5133]],  
                      Type = [[end of activity sequence]],  
                      Value = r2.RefId([[Client1_5027]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3823]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3826]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3847]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3848]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3669]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4058]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4972]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4973]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4974]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5027]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5096]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5097]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5028]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5029]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5014]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5030]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5042]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5032]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              SheetClient = [[basic_tryker_female.creature]],  
              FeetModel = 6726702,  
              Speed = 1,  
              Angle = -0.09375,  
              Base = [[palette.entities.npcs.bandits.t_light_melee_220]],  
              WeaponLeftHand = 6763566,  
              ArmColor = 4,  
              JacketModel = 6728750,  
              Sex = 1,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_pierce_f4.creature]],  
              WeaponRightHand = 6763566,  
              Level = 3,  
              Name = [[Akilia Tempete de Cendre]],  
              Position = {
                y = -2248.4375,  
                x = 28665.5,  
                InstanceId = [[Client1_3648]],  
                Class = [[Position]],  
                z = 75.3125
              },  
              ArmModel = 6728238,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 12,  
              PlayerAttackable = 0,  
              BotAttackable = 0
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_3655]],  
              ActivitiesId = {
              },  
              HairType = 5623854,  
              TrouserColor = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 1,  
              HandsModel = 6737710,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 10,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 6738222,  
              GabaritLegsWidth = 2,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3653]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              FeetModel = 6731822,  
              Speed = 1,  
              Angle = -0.078125,  
              Base = [[palette.entities.npcs.bandits.z_melee_dd_220]],  
              SheetClient = [[basic_zorai_male.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 5,  
              JacketModel = 6734126,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              WeaponRightHand = 6772270,  
              Level = 0,  
              Name = [[Garde rapprochee d'Akilia]],  
              Position = {
                y = -2247.4375,  
                x = 28662.125,  
                InstanceId = [[Client1_3656]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              ArmModel = 6738990,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_3659]],  
              ActivitiesId = {
              },  
              HairType = 5622318,  
              TrouserColor = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 3,  
              HandsModel = 6716206,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 9,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 6716718,  
              GabaritLegsWidth = 12,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3657]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 6715694,  
              Speed = 1,  
              Angle = -0.078125,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              SheetClient = [[basic_matis_female.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 3,  
              JacketModel = 6717742,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              WeaponRightHand = 6760238,  
              Level = 0,  
              Name = [[Garde rapprochee d'Akilia]],  
              Position = {
                y = -2248.65625,  
                x = 28663.01563,  
                InstanceId = [[Client1_3660]],  
                Class = [[Position]],  
                z = 75.21875
              },  
              ArmModel = 6717230,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 5
            }
          },  
          Cost = 0
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_3688]],  
          Behavior = {
            InstanceId = [[Client1_3689]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_3713]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_3764]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_3765]],  
                    Entity = r2.RefId([[Client1_3342]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4246]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4247]],  
                    Entity = r2.RefId([[Client1_3635]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4248]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4249]],  
                    Entity = r2.RefId([[Client1_3638]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4250]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4251]],  
                    Entity = r2.RefId([[Client1_3667]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4252]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4253]],  
                    Entity = r2.RefId([[Client1_3960]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4254]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4255]],  
                    Entity = r2.RefId([[Client1_3984]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4256]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4257]],  
                    Entity = r2.RefId([[Client1_3991]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4320]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4321]],  
                    Entity = r2.RefId([[Client1_4317]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Resume]],  
                      InstanceId = [[Client1_4322]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4323]],  
                    Entity = r2.RefId([[Client1_4317]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_4414]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4415]],  
                    Entity = r2.RefId([[Client1_4634]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Resume]],  
                      InstanceId = [[Client1_4637]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4638]],  
                    Entity = r2.RefId([[Client1_4634]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_3714]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[Lancement du Raid Maraudeurs]],  
          Position = {
            y = -2250.578125,  
            x = 28534.07813,  
            InstanceId = [[Client1_3690]],  
            Class = [[Position]],  
            z = 72.90625
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.jar]]
        },  
        {
          InstanceId = [[Client1_3960]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 5]],  
          Position = {
            y = 17.4375,  
            x = 5.03125,  
            InstanceId = [[Client1_3959]],  
            Class = [[Position]],  
            z = 1.265625
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3958]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_3294]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 6713902,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 1,  
              AutoSpawn = 0,  
              TrouserModel = 6708782,  
              GabaritLegsWidth = 4,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3292]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3961]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3962]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3963]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3964]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3925]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3966]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4975]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4976]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4977]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5098]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5099]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5100]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              SheetClient = [[basic_matis_female.creature]],  
              FeetModel = 6713390,  
              Speed = 1,  
              Angle = -0.03125,  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              WeaponLeftHand = 0,  
              Level = 0,  
              JacketModel = 6715438,  
              Sex = 1,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 4,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              WeaponRightHand = 6935086,  
              ArmColor = 4,  
              Name = [[Chef D'Escouade Maraudeur]],  
              Position = {
                y = -2255.8125,  
                x = 28523.04688,  
                InstanceId = [[Client1_3295]],  
                Class = [[Position]],  
                z = 70.75
              },  
              ArmModel = 6714926,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_3298]],  
              ActivitiesId = {
              },  
              HairType = 8750,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 13,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 5,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 6730286,  
              GabaritLegsWidth = 11,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3296]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6734894,  
              Speed = 1,  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.bandits.z_mage_aoe_220]],  
              SheetClient = [[basic_zorai_male.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 4,  
              JacketModel = 6736942,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_magic_aoe_electricity_f2.creature]],  
              WeaponRightHand = 6936622,  
              Level = 0,  
              Name = [[Mercenaire Maraudeur]],  
              Position = {
                y = -2253.890625,  
                x = 28523.0625,  
                InstanceId = [[Client1_3299]],  
                Class = [[Position]],  
                z = 70.734375
              },  
              ArmModel = 6736430,  
              MorphTarget7 = 2,  
              MorphTarget3 = 7,  
              Tattoo = 12
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_3984]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 6]],  
          Position = {
            y = 0.765625,  
            x = 3.4375,  
            InstanceId = [[Client1_3983]],  
            Class = [[Position]],  
            z = 0.359375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3982]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_3530]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 6716462,  
              FeetColor = 4,  
              GabaritBreastSize = 9,  
              GabaritHeight = 4,  
              HairColor = 2,  
              EyesColor = 1,  
              AutoSpawn = 0,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 9,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3531]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3985]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3986]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3987]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3988]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3908]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4059]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4978]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4979]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4980]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5101]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5102]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5103]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              SheetClient = [[basic_matis_male.creature]],  
              FeetModel = 6715950,  
              Speed = 1,  
              Angle = -0.03125,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              WeaponLeftHand = 0,  
              Level = 0,  
              JacketModel = 6717998,  
              Sex = 0,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              WeaponRightHand = 6762030,  
              ArmColor = 4,  
              Name = [[Chef d'Escouade Maraudeur]],  
              Position = {
                y = -2238.546875,  
                x = 28521.82813,  
                InstanceId = [[Client1_3532]],  
                Class = [[Position]],  
                z = 71.65625
              },  
              ArmModel = 6717486,  
              MorphTarget7 = 7,  
              MorphTarget3 = 3,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_3610]],  
              ActivitiesId = {
              },  
              HairType = 5622318,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 6716462,  
              FeetColor = 4,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 4,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3611]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6710318,  
              Speed = 1,  
              Angle = -0.09375,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              SheetClient = [[basic_matis_female.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 4,  
              JacketModel = 6717998,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 2,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              WeaponRightHand = 6762030,  
              Level = 0,  
              Name = [[Mercenaire Maraudeur]],  
              Position = {
                y = -2242.25,  
                x = 28521.1875,  
                InstanceId = [[Client1_3612]],  
                Class = [[Position]],  
                z = 71.640625
              },  
              ArmModel = 6717486,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 0
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_3991]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Escouade 7]],  
          Position = {
            y = -9.84375,  
            x = 4.6875,  
            InstanceId = [[Client1_3990]],  
            Class = [[Position]],  
            z = 1.03125
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_3989]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_3420]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 13,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 7,  
              AutoSpawn = 0,  
              TrouserModel = 6698030,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3421]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3992]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3993]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3994]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_3995]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_3669]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4060]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4981]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4982]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4983]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4936]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4984]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_5104]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5105]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_5065]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_5106]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              SheetClient = [[basic_fyros_female.creature]],  
              FeetModel = 6702382,  
              Speed = 1,  
              Angle = -0.03125,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_220]],  
              WeaponLeftHand = 0,  
              Level = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              Sheet = [[ring_magic_curser_stun_f2.creature]],  
              WeaponRightHand = 6934318,  
              ArmColor = 4,  
              Name = [[Chef d'Escouade Maraudeur]],  
              Position = {
                y = -2230.3125,  
                x = 28525.01563,  
                InstanceId = [[Client1_3422]],  
                Class = [[Position]],  
                z = 71.4375
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 6,  
              MorphTarget3 = 3,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_3510]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 13,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 6698030,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3511]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6702382,  
              Speed = 1,  
              Angle = -0.03125,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 4,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              Sheet = [[ring_magic_curser_stun_f2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Mercenaire Maraudeur]],  
              Position = {
                y = -2231.109375,  
                x = 28522.07813,  
                InstanceId = [[Client1_3512]],  
                Class = [[Position]],  
                z = 71.34375
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 6,  
              MorphTarget3 = 3,  
              Tattoo = 4
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_4245]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Groupe Chef des Gardes]],  
          Position = {
            y = 45.359375,  
            x = 37.0625,  
            InstanceId = [[Client1_4244]],  
            Class = [[Position]],  
            z = 1.875
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_4243]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4078]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 13,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4079]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4568]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4570]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4569]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4571]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              SheetClient = [[basic_fyros_male.creature]],  
              FeetModel = 6699566,  
              Speed = 1,  
              Angle = -1.765625,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Level = 0,  
              ArmColor = 0,  
              JacketModel = 6702126,  
              Sex = 0,  
              NoRespawn = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              WeaponRightHand = 6756142,  
              WeaponLeftHand = 0,  
              Name = [[Chef des Gardes Aethus]],  
              Position = {
                y = -2113.3125,  
                x = 28791.53125,  
                InstanceId = [[Client1_4080]],  
                Class = [[Position]],  
                z = 74.640625
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4655]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4656]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Level = 0,  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5595950,  
              JacketModel = 5605166,  
              Name = [[Garde Icanix]],  
              Position = {
                y = -2159.296875,  
                x = 28751.60938,  
                InstanceId = [[Client1_4657]],  
                Class = [[Position]],  
                z = 73.0625
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4669]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4670]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              Level = 0,  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5595950,  
              JacketModel = 5605166,  
              Name = [[Garde Diorus]],  
              Position = {
                y = -2160.859375,  
                x = 28761.40625,  
                InstanceId = [[Client1_4671]],  
                Class = [[Position]],  
                z = 73.140625
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4699]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4700]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Level = 0,  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5595950,  
              JacketModel = 5605166,  
              Name = [[Garde Zelus]],  
              Position = {
                y = -2106.046875,  
                x = 28790.95313,  
                InstanceId = [[Client1_4701]],  
                Class = [[Position]],  
                z = 75.65625
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4689]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 2,  
              EyesColor = 6,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4690]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Level = 0,  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5595950,  
              JacketModel = 5605166,  
              Name = [[Garde Deudix]],  
              Position = {
                y = -2106.53125,  
                x = 28790.51563,  
                InstanceId = [[Client1_4691]],  
                Class = [[Position]],  
                z = 75.625
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 15
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_4679]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4680]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              Level = 0,  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5595950,  
              JacketModel = 5605166,  
              Name = [[Garde Kyseus]],  
              Position = {
                y = -2112.53125,  
                x = 28790.4375,  
                InstanceId = [[Client1_4681]],  
                Class = [[Position]],  
                z = 74.828125
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 7,  
              MorphTarget3 = 7,  
              Tattoo = 4
            }
          },  
          Cost = 0
        },  
        {
          Secondes = 10,  
          Behavior = {
            InstanceId = [[Client1_4318]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4325]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4327]],  
                      Value = r2.RefId([[Client1_3841]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4328]],  
                    Entity = r2.RefId([[Client1_3342]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4329]],  
                      Value = r2.RefId([[Client1_3843]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4330]],  
                    Entity = r2.RefId([[Client1_3635]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4331]],  
                      Value = r2.RefId([[Client1_3845]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4332]],  
                    Entity = r2.RefId([[Client1_3638]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4333]],  
                      Value = r2.RefId([[Client1_3847]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4334]],  
                    Entity = r2.RefId([[Client1_3667]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4335]],  
                      Value = r2.RefId([[Client1_3963]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4336]],  
                    Entity = r2.RefId([[Client1_3960]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4337]],  
                      Value = r2.RefId([[Client1_3987]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4338]],  
                    Entity = r2.RefId([[Client1_3984]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4339]],  
                      Value = r2.RefId([[Client1_3994]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4340]],  
                    Entity = r2.RefId([[Client1_3991]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Deactivate]],  
                      InstanceId = [[Client1_4341]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4342]],  
                    Entity = r2.RefId([[Client1_4317]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[On Trigger]],  
                  InstanceId = [[Client1_4326]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[Timer]],  
          Cyclic = 0,  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Timer PoP Maraudeur]],  
          Position = {
            y = -2250.484375,  
            x = 28532.07813,  
            InstanceId = [[Client1_4319]],  
            Class = [[Position]],  
            z = 73
          },  
          Components = {
          },  
          InstanceId = [[Client1_4317]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_4410]],  
          Behavior = {
            InstanceId = [[Client1_4408]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4423]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4497]],  
                      Value = r2.RefId([[Client1_4495]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4498]],  
                    Entity = r2.RefId([[Client1_4479]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4542]],  
                      Value = r2.RefId([[Client1_4406]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4543]],  
                    Entity = r2.RefId([[Client1_4349]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4547]],  
                      Value = r2.RefId([[Client1_4544]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4548]],  
                    Entity = r2.RefId([[Client1_4520]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[start of chat]],  
                  InstanceId = [[Client1_4424]],  
                  Value = r2.RefId([[Client1_4445]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_4500]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Stand Up]],  
                      InstanceId = [[Client1_4502]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4503]],  
                    Entity = r2.RefId([[Client1_4479]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of chat]],  
                  InstanceId = [[Client1_4501]],  
                  Value = r2.RefId([[Client1_4416]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_4703]],  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_4704]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Stand Up]],  
                      InstanceId = [[Client1_4705]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4706]],  
                    Entity = r2.RefId([[Client1_4349]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Xathusa et Icarius decouvre les maraudeurs]],  
          Position = {
            y = -2254.625,  
            x = 28586.0625,  
            InstanceId = [[Client1_4409]],  
            Class = [[Position]],  
            z = 79.390625
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_4416]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Point]],  
                  InstanceId = [[Client1_4417]],  
                  Who = r2.RefId([[Client1_4349]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_3646]]),  
                  Says = [[Client1_4504]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 1,  
              InstanceId = [[Client1_4445]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4446]],  
                  Who = r2.RefId([[Client1_4479]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_4349]]),  
                  Says = [[Client1_4457]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 1,  
              InstanceId = [[Client1_4418]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4419]],  
                  Who = r2.RefId([[Client1_4349]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4458]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 39,  
              InstanceId = [[Client1_4556]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4557]],  
                  Who = r2.RefId([[Client1_4349]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4558]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_4559]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4560]],  
                  Who = r2.RefId([[Client1_4349]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4563]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_4461]],  
          Behavior = {
            InstanceId = [[Client1_4459]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4615]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Stand Up]],  
                      InstanceId = [[Client1_4617]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4618]],  
                    Entity = r2.RefId([[Client1_4479]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[start of chat]],  
                  InstanceId = [[Client1_4616]],  
                  Value = r2.RefId([[Client1_4631]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_4627]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_4628]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4629]],  
                      Value = r2.RefId([[Client1_4621]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4630]],  
                    Entity = r2.RefId([[Client1_4520]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Gardien des Dunes suprit par le joueur]],  
          Position = {
            y = -2254.375,  
            x = 28589.1875,  
            InstanceId = [[Client1_4460]],  
            Class = [[Position]],  
            z = 79.078125
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_4462]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4463]],  
                  Who = r2.RefId([[Client1_4479]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_4349]]),  
                  Says = [[Client1_4468]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_4465]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4466]],  
                  Who = r2.RefId([[Client1_4349]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_4479]]),  
                  Says = [[Client1_4469]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_4631]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4632]],  
                  Who = r2.RefId([[Client1_4479]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4633]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          InstanceId = [[Client1_4520]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Groupe Yubo de Xathusa]],  
          Position = {
            y = 0.40625,  
            x = 5.640625,  
            InstanceId = [[Client1_4519]],  
            Class = [[Position]],  
            z = 3.203125
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_4518]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_4512]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4510]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4845]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Deactivate]],  
                          InstanceId = [[Client1_4847]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_4848]],  
                        Entity = r2.RefId([[Client1_4520]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_4846]],  
                      Value = r2.RefId([[Client1_4546]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4539]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4608]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4526]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4544]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4545]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4385]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4546]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4825]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4621]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_4622]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4526]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Feed In Zone]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4625]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              Position = {
                y = -2253.453125,  
                x = 28575.85938,  
                InstanceId = [[Client1_4513]],  
                Class = [[Position]],  
                z = 75.75
              },  
              Angle = -2.264665365,  
              Base = [[palette.entities.creatures.chddb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4516]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4514]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              Position = {
                y = -2255.578125,  
                x = 28576.10938,  
                InstanceId = [[Client1_4517]],  
                Class = [[Position]],  
                z = 75.4375
              },  
              Angle = 1.970519662,  
              Base = [[palette.entities.creatures.chddb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_4523]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4521]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Timorous Yubo]],  
              Position = {
                y = -2252.265625,  
                x = 28574.60938,  
                InstanceId = [[Client1_4524]],  
                Class = [[Position]],  
                z = 75.375
              },  
              Angle = -1.956665277,  
              Base = [[palette.entities.creatures.chddb1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_4574]],  
          Behavior = {
            InstanceId = [[Client1_4572]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4587]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4589]],  
                      Value = r2.RefId([[Client1_4569]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4590]],  
                    Entity = r2.RefId([[Client1_4245]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4794]],  
                      Value = r2.RefId([[Client1_4792]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4795]],  
                    Entity = r2.RefId([[Client1_4754]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4836]],  
                      Value = r2.RefId([[Client1_4822]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4837]],  
                    Entity = r2.RefId([[Client1_4349]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_4588]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Chef des Gardes et Xathusa]],  
          Position = {
            y = -2070.171875,  
            x = 28823.17188,  
            InstanceId = [[Client1_4573]],  
            Class = [[Position]],  
            z = 77
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_4575]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4576]],  
                  Who = r2.RefId([[Client1_4349]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_4078]]),  
                  Says = [[Client1_4577]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_4578]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4579]],  
                  Who = r2.RefId([[Client1_4078]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4580]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          InstanceId = [[Client1_4591]],  
          Behavior = {
            InstanceId = [[Client1_4592]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4610]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_4612]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4613]],  
                    Entity = r2.RefId([[Client1_4461]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                  {
                    InstanceId = [[Client1_4620]],  
                    Entity = r2.RefId([[Client1_4520]]),  
                    Class = [[ConditionStep]],  
                    Condition = {
                      Type = [[is in activity sequence]],  
                      InstanceId = [[Client1_4619]],  
                      Value = r2.RefId([[Client1_4539]]),  
                      Class = [[ConditionType]]
                    }
                  }
                },  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_4611]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger - Joueur surprenant Xathusa et Icarius]],  
          Position = {
            y = -2253.046875,  
            x = 28589.10938,  
            InstanceId = [[Client1_4593]],  
            Class = [[Position]],  
            z = 79.140625
          },  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Zone trigger Xathusa et Icarius]],  
              InstanceId = [[Client1_4595]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4597]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 48.359375,  
                    x = 22.421875,  
                    InstanceId = [[Client1_4598]],  
                    Class = [[Position]],  
                    z = -1.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4600]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 44.6875,  
                    x = -5.203125,  
                    InstanceId = [[Client1_4601]],  
                    Class = [[Position]],  
                    z = -4.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4603]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -19.328125,  
                    x = -8.484375,  
                    InstanceId = [[Client1_4604]],  
                    Class = [[Position]],  
                    z = -3.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4606]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -21.015625,  
                    x = 20.640625,  
                    InstanceId = [[Client1_4607]],  
                    Class = [[Position]],  
                    z = -4.859375
                  }
                }
              },  
              Deletable = 0,  
              Position = {
                y = 0.171875,  
                x = -0.046875,  
                InstanceId = [[Client1_4594]],  
                Class = [[Position]],  
                z = 0.03125
              }
            }
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          _Zone = [[Client1_4595]]
        },  
        {
          Secondes = 3,  
          Behavior = {
            InstanceId = [[Client1_4635]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4640]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_4642]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4643]],  
                    Entity = r2.RefId([[Client1_4410]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_4921]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4922]],  
                    Entity = r2.RefId([[Client1_4918]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[On Trigger]],  
                  InstanceId = [[Client1_4641]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[Timer]],  
          Cyclic = 0,  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Timer Decouverte Maraudeur]],  
          Position = {
            y = -2253.390625,  
            x = 28586.25,  
            InstanceId = [[Client1_4636]],  
            Class = [[Position]],  
            z = 79
          },  
          Components = {
          },  
          InstanceId = [[Client1_4634]],  
          Base = [[palette.entities.botobjects.campfire]],  
          Active = 0
        },  
        {
          InstanceId = [[Client1_4754]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Patrouille Gardes dans village]],  
          Position = {
            y = 1.875,  
            x = 70.171875,  
            InstanceId = [[Client1_4753]],  
            Class = [[Position]],  
            z = 0.453125
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4724]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4725]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4790]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4791]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4756]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_4792]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_4793]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_4227]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 8,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              ArmModel = 5604910,  
              Position = {
                y = -2073.203125,  
                x = 28833.39063,  
                InstanceId = [[Client1_4726]],  
                Class = [[Position]],  
                z = 75.3125
              },  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5605166,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Garde Iocus]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              ArmColor = 0,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4749]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4750]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 16,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              ArmModel = 5604910,  
              Position = {
                y = -2073.109375,  
                x = 28832.21875,  
                InstanceId = [[Client1_4751]],  
                Class = [[Position]],  
                z = 75.484375
              },  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5605166,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Garde Dean]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              ArmColor = 0,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_4739]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4740]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 14,  
              MorphTarget3 = 7,  
              MorphTarget7 = 7,  
              ArmModel = 5604910,  
              Position = {
                y = -2071.953125,  
                x = 28832.34375,  
                InstanceId = [[Client1_4741]],  
                Class = [[Position]],  
                z = 75.546875
              },  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              JacketModel = 5605166,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Garde Eun]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              ArmColor = 0,  
              Level = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4714]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4715]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 28,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              ArmModel = 5604910,  
              Position = {
                y = -2071.640625,  
                x = 28833.40625,  
                InstanceId = [[Client1_4716]],  
                Class = [[Position]],  
                z = 75.4375
              },  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5605166,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Garde Deulus]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              ArmColor = 0,  
              Level = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_4752]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_4801]],  
          Behavior = {
            InstanceId = [[Client1_4799]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Akilia]],  
          Position = {
            y = -2157.5,  
            x = 28799.35938,  
            InstanceId = [[Client1_4800]],  
            Class = [[Position]],  
            z = 75
          },  
          Active = 0,  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_4804]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Malicious]],  
                  InstanceId = [[Client1_4805]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4927]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 9,  
              InstanceId = [[Client1_4806]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4807]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4810]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_4849]],  
          Behavior = {
            InstanceId = [[Client1_4850]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4870]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_4872]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4873]],  
                    Entity = r2.RefId([[Client1_4868]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                  {
                    InstanceId = [[Client1_4926]],  
                    Entity = r2.RefId([[Client1_4520]]),  
                    Class = [[ConditionStep]],  
                    Condition = {
                      Type = [[is in activity sequence]],  
                      InstanceId = [[Client1_4925]],  
                      Value = r2.RefId([[Client1_4539]]),  
                      Class = [[ConditionType]]
                    }
                  }
                },  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_4871]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_4899]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_4907]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4908]],  
                    Entity = r2.RefId([[Client1_4906]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                  {
                    InstanceId = [[Client1_4910]],  
                    Entity = r2.RefId([[Client1_3667]]),  
                    Class = [[ConditionStep]],  
                    Condition = {
                      Type = [[is in activity sequence]],  
                      InstanceId = [[Client1_4909]],  
                      Value = r2.RefId([[Client1_3847]]),  
                      Class = [[ConditionType]]
                    }
                  }
                },  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_4900]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger - Entree Village]],  
          Position = {
            y = -2113.828125,  
            x = 28793.51563,  
            InstanceId = [[Client1_4851]],  
            Class = [[Position]],  
            z = 74.96875
          },  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Zone trigger entrée village]],  
              InstanceId = [[Client1_4853]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4855]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -3.875,  
                    x = 5.328125,  
                    InstanceId = [[Client1_4856]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4858]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 2.078125,  
                    x = 6.75,  
                    InstanceId = [[Client1_4859]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4861]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.21875,  
                    x = -5.609375,  
                    InstanceId = [[Client1_4862]],  
                    Class = [[Position]],  
                    z = -0.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4864]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1.53125,  
                    x = -6.78125,  
                    InstanceId = [[Client1_4865]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 0,  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4852]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          _Zone = [[Client1_4853]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_4868]],  
          Behavior = {
            InstanceId = [[Client1_4866]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Texte garde Diorus concernant Icarius]],  
          Position = {
            y = -2117.171875,  
            x = 28798.26563,  
            InstanceId = [[Client1_4867]],  
            Class = [[Position]],  
            z = 75
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_4874]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4875]],  
                  Who = r2.RefId([[Client1_4669]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4878]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_4876]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4877]],  
                  Who = r2.RefId([[Client1_4669]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4879]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          InstanceId = [[Client1_4881]],  
          Behavior = {
            InstanceId = [[Client1_4882]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Zone trigger - Tour de garde]],  
          Position = {
            y = -2068.5625,  
            x = 28830.04688,  
            InstanceId = [[Client1_4883]],  
            Class = [[Position]],  
            z = 77
          },  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Zone trigger Tours de garde]],  
              InstanceId = [[Client1_4885]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4887]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 22.609375,  
                    x = 13.25,  
                    InstanceId = [[Client1_4888]],  
                    Class = [[Position]],  
                    z = -4.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4890]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 22.609375,  
                    x = -22.359375,  
                    InstanceId = [[Client1_4891]],  
                    Class = [[Position]],  
                    z = -2.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4893]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -15.625,  
                    x = -23.484375,  
                    InstanceId = [[Client1_4894]],  
                    Class = [[Position]],  
                    z = -2
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_4896]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -15.5,  
                    x = 14.25,  
                    InstanceId = [[Client1_4897]],  
                    Class = [[Position]],  
                    z = -1.828125
                  }
                }
              },  
              Deletable = 0,  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_4884]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          _Zone = [[Client1_4885]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_4906]],  
          Behavior = {
            InstanceId = [[Client1_4904]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Feu au cul :p]],  
          Position = {
            y = -2117.34375,  
            x = 28800.0625,  
            InstanceId = [[Client1_4905]],  
            Class = [[Position]],  
            z = 75
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_4911]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4912]],  
                  Who = r2.RefId([[Client1_4669]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4915]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_4913]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_4914]],  
                  Who = r2.RefId([[]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_4916]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          MissionText = [[Allez prevenir le <mission_target> pour moi ! Faites le et le village sera peut etre sauvé]],  
          MissionSucceedText = [[Sonnez l'alarme !!!]],  
          Behavior = {
            InstanceId = [[Client1_4919]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          WaitValidationText = [[Bon sang... <mission_giver> vous a dit ça ?]],  
          Base = [[palette.entities.botobjects.chest]],  
          Repeatable = 0,  
          ContextualText = [[Accepter <mission_target>]],  
          MissionGiver = r2.RefId([[Client1_4479]]),  
          MissionTarget = r2.RefId([[Client1_4078]]),  
          InheritPos = 1,  
          Class = [[TalkTo]],  
          Name = [[Mission: Talk to 1]],  
          Position = {
            y = -2242.546875,  
            x = 28566.23438,  
            InstanceId = [[Client1_4920]],  
            Class = [[Position]],  
            z = 74.96875
          },  
          Active = 0,  
          InstanceId = [[Client1_4918]],  
          Components = {
          },  
          _Seed = 1142286801
        },  
        {
          InstanceId = [[Client1_4934]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = -0.109375,  
            x = 0,  
            InstanceId = [[Client1_4933]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_4932]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_4930]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 5,  
              HandsModel = 5604142,  
              FeetColor = 1,  
              GabaritBreastSize = 7,  
              GabaritHeight = 6,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 7,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_4928]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              WeaponLeftHand = 0,  
              FeetModel = 5603886,  
              Speed = 1,  
              Angle = 6.321272373,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              NoRespawn = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Iolus - Serviteur des Kamis]],  
              Position = {
                y = -2043.75,  
                x = 28933.96875,  
                InstanceId = [[Client1_4931]],  
                Class = [[Position]],  
                z = 81.578125
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 6,  
              MorphTarget3 = 0,  
              Tattoo = 20
            },  
            {
              InstanceId = [[Client1_3102]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3100]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gardien Kami]],  
              Position = {
                y = -2042.921875,  
                x = 28930.46875,  
                InstanceId = [[Client1_3103]],  
                Class = [[Position]],  
                z = 82.015625
              },  
              NoRespawn = 1,  
              Angle = 2.234375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3106]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3104]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gardien Kami]],  
              Position = {
                y = -2045.4375,  
                x = 28927.3125,  
                InstanceId = [[Client1_3107]],  
                Class = [[Position]],  
                z = 82.015625
              },  
              NoRespawn = 1,  
              Angle = 2.234375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_3110]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_3108]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Grand Maitre Kami]],  
              Position = {
                y = -2043.5625,  
                x = 28928.1875,  
                InstanceId = [[Client1_3111]],  
                Class = [[Position]],  
                z = 82.0625
              },  
              NoRespawn = 1,  
              Angle = 2.25,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_4946]],  
          Behavior = {
            InstanceId = [[Client1_4947]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_4950]],  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_4951]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4985]],  
                      Value = r2.RefId([[Client1_4952]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4986]],  
                    Entity = r2.RefId([[Client1_3342]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4987]],  
                      Value = r2.RefId([[Client1_4966]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4988]],  
                    Entity = r2.RefId([[Client1_3635]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4989]],  
                      Value = r2.RefId([[Client1_4969]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4990]],  
                    Entity = r2.RefId([[Client1_3638]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4991]],  
                      Value = r2.RefId([[Client1_4972]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4992]],  
                    Entity = r2.RefId([[Client1_3667]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4993]],  
                      Value = r2.RefId([[Client1_4975]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4994]],  
                    Entity = r2.RefId([[Client1_3960]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4995]],  
                      Value = r2.RefId([[Client1_4978]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4996]],  
                    Entity = r2.RefId([[Client1_3984]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_4997]],  
                      Value = r2.RefId([[Client1_4982]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_4998]],  
                    Entity = r2.RefId([[Client1_3991]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[Etape Deux du raid]],  
          Position = {
            y = -2035.390625,  
            x = 28922.82813,  
            InstanceId = [[Client1_4948]],  
            Class = [[Position]],  
            z = 82.203125
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.jar]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_5001]],  
          Behavior = {
            InstanceId = [[Client1_4999]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog Akilia Prendre objet dans tente Kami]],  
          Position = {
            y = -2037.390625,  
            x = 28919.90625,  
            InstanceId = [[Client1_5000]],  
            Class = [[Position]],  
            z = 81
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Components = {
            {
              Time = 9,  
              InstanceId = [[Client1_5002]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_5003]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5004]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_5005]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_5006]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5007]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_5045]],  
          Name = [[Dialog Tente Kami]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_5046]],  
              Time = 1,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_5047]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5050]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_5048]],  
              Time = 10,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_5049]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5055]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_5053]],  
              Time = 2,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_5054]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_3655]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5058]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_5056]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_5057]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5061]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_5059]],  
              Time = 9,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_5060]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_3646]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_5063]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Active = 0,  
          Behavior = {
            InstanceId = [[Client1_5043]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_5044]],  
            x = 28925.07813,  
            y = -2034.296875,  
            z = 83
          },  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Raid sur le village]]
    },  
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_2555]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[Recherche D�sesp�r�e]],  
      ActivitiesIds = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_2557]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 847,  
      LocationId = [[Client1_2559]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_2556]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2558]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Recherche Désespérée]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 11,  
        InstanceId = [[Client1_4420]],  
        Class = [[TextManagerEntry]],  
        Text = [[Ha ! Des bandits !]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_4421]],  
        Class = [[TextManagerEntry]],  
        Text = [[Vite mes yubos ! Fuyons !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4447]],  
        Class = [[TextManagerEntry]],  
        Text = [[Courez prevenir le village Xathusa !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4450]],  
        Class = [[TextManagerEntry]],  
        Text = [[Allez prevenir le village Xathusa ! ]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_4457]],  
        Class = [[TextManagerEntry]],  
        Text = [[Allez prevenir le village Xathusa ! Je vais tenter de les ralentir....]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_4458]],  
        Class = [[TextManagerEntry]],  
        Text = [[Vite mes petits yubos ! Courez aussi vite que vous le pouvez !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4464]],  
        Class = [[TextManagerEntry]],  
        Text = [[Je trouve que vous vous occupez merveilleusement bien de ces yubos]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4467]],  
        Class = [[TextManagerEntry]],  
        Text = [[*troubl�e* Ho]]
      },  
      {
        Count = 8,  
        InstanceId = [[Client1_4468]],  
        Class = [[TextManagerEntry]],  
        Text = [[Je trouve que vous vous occupez merveilleusement bien de ces yubos... Ils en ont de la chance *soupir*]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_4469]],  
        Class = [[TextManagerEntry]],  
        Text = [[*troublee* Je..heu.. ho voila quelqu'un]]
      },  
      {
        Count = 12,  
        InstanceId = [[Client1_4504]],  
        Class = [[TextManagerEntry]],  
        Text = [[Haaaa ! Des bandits !!!]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_4558]],  
        Class = [[TextManagerEntry]],  
        Text = [[Des Bandits nous attaques !]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_4563]],  
        Class = [[TextManagerEntry]],  
        Text = [[Des Bandits nous Attaques !!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4567]],  
        Class = [[TextManagerEntry]],  
        Text = [[Par tout Atys ! Sonnez l'alarme !!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_4577]],  
        Class = [[TextManagerEntry]],  
        Text = [[Chef ! Des Bandits nous attaques !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4580]],  
        Class = [[TextManagerEntry]],  
        Text = [[Par tout Atys ! Faites sonnez l'alarme !!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_4633]],  
        Class = [[TextManagerEntry]],  
        Text = [[He bien ! Que faites vous ici ? et pourquoi nous deranger de la sorte ?!?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_4810]],  
        Class = [[TextManagerEntry]],  
        Text = [[Tuez les touuuuuuuuusssss !!!!!!!!!!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_4878]],  
        Class = [[TextManagerEntry]],  
        Text = [[H� vous !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_4915]],  
        Class = [[TextManagerEntry]],  
        Text = [[Alors vous avez vu Icarius ?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_4927]],  
        Class = [[TextManagerEntry]],  
        Text = [[A l'assaut !!! Pas de pitit����� !!!!]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_5004]],  
        Class = [[TextManagerEntry]],  
        Text = [[Tuez ces Kami ! Le coffre se trouve � l'int�rieur]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_5007]],  
        Class = [[TextManagerEntry]],  
        Text = [[Tuez les et le coffre sera � nous !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_5050]],  
        Count = 1,  
        Text = [[Allons chercher ce coffre]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_5055]],  
        Count = 2,  
        Text = [[Mais ou se cache t il ?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_5058]],  
        Count = 1,  
        Text = [[Ici je l'ai trouv� !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_5061]],  
        Count = 1,  
        Text = [[Donne le moi idiot ! ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_5062]],  
        Count = 1,  
        Text = [[On a le coffre !! Replis G�n�ral !!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_5063]],  
        Count = 1,  
        Text = [[On a le coffre !! Replis G�n�ral !!!]]
      }
    },  
    InstanceId = [[Client1_2542]]
  }
}