scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_693]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 2,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_695]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    TextManager = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ActivityStep = 1,  
    Position = 0,  
    NpcCustom = 0,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 20,  
      Behavior = {
        InstanceId = [[Client1_696]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_698]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_697]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_714]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_715]],  
                x = 22792.1875,  
                y = -1288.6875,  
                z = 74.453125
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_712]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_718]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_719]],  
                x = 22789.9375,  
                y = -1289.265625,  
                z = 74.515625
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_716]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_722]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_723]],  
                x = 22769.28125,  
                y = -1294.3125,  
                z = 71.8125
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_720]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[runic circle 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_726]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_727]],  
                x = 22780.64063,  
                y = -1269.5625,  
                z = 75.140625
              },  
              Angle = -0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_724]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_730]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_731]],  
                x = 22788.21875,  
                y = -1265.28125,  
                z = 75.109375
              },  
              Angle = -1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_728]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_733]],  
              Name = [[Local City Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_735]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_736]],  
                    x = 22780.45313,  
                    y = -1290.171875,  
                    z = 74.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_738]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_739]],  
                    x = 22802.26563,  
                    y = -1283.203125,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_741]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_742]],  
                    x = 22797.5,  
                    y = -1264.90625,  
                    z = 75.09375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_732]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_749]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_750]],  
                x = 22799.25,  
                y = -1250.65625,  
                z = 75.109375
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_747]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_753]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_754]],  
                x = 22803.375,  
                y = -1262.953125,  
                z = 75.125
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_751]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_757]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_758]],  
                x = 22805.78125,  
                y = -1273.59375,  
                z = 75.09375
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_755]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_761]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_762]],  
                x = 22808.5,  
                y = -1288.140625,  
                z = 74.796875
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_759]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 4]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_765]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_766]],  
                x = 22807.53125,  
                y = -1280.703125,  
                z = 75.078125
              },  
              Angle = 0.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_763]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 8,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 31,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6755374,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_769]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_770]],  
                x = 22804.64063,  
                y = -1268.4375,  
                z = 75.09375
              },  
              Angle = 0.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_767]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 4,  
              Tattoo = 28,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 3,  
              WeaponRightHand = 6755630,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_773]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_774]],  
                x = 22801.0625,  
                y = -1256.828125,  
                z = 75.078125
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_771]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 14,  
              HairType = 6700334,  
              HairColor = 5,  
              Tattoo = 5,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 1,  
              WeaponRightHand = 6755374,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_777]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_778]],  
                x = 22809,  
                y = -1299.265625,  
                z = 72.25
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_775]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 2,  
              HairType = 6700334,  
              HairColor = 1,  
              Tattoo = 10,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 0,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_781]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_782]],  
                x = 22795.40625,  
                y = -1244.28125,  
                z = 74.8125
              },  
              Angle = 0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_779]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 0,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 1,  
              WeaponRightHand = 6755374,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_785]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_786]],  
                x = 22801.76563,  
                y = -1300.234375,  
                z = 72.328125
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_783]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_789]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_790]],  
                x = 22800.54688,  
                y = -1288.5,  
                z = 74.34375
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_787]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tent 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_793]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_794]],  
                x = 22789.71875,  
                y = -1300.21875,  
                z = 73.875
              },  
              Angle = 1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_791]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tent 3]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_829]],  
              Name = [[Right Attack Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_831]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_832]],  
                    x = 22880.23438,  
                    y = -1308.90625,  
                    z = 74.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_834]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_835]],  
                    x = 22927.07813,  
                    y = -1251.203125,  
                    z = 72.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_837]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_838]],  
                    x = 22959.92188,  
                    y = -1282.625,  
                    z = 75.796875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_828]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_840]],  
              Name = [[Left Attack Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_842]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_843]],  
                    x = 22890.01563,  
                    y = -1189.25,  
                    z = 72.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_845]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_846]],  
                    x = 22845.92188,  
                    y = -1148.109375,  
                    z = 75.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_848]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_849]],  
                    x = 22916.25,  
                    y = -1089.453125,  
                    z = 76.96875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_839]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_851]],  
              Name = [[Kitin Home Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_853]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_854]],  
                    x = 22978.75,  
                    y = -1105.828125,  
                    z = 77.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_856]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_857]],  
                    x = 23061.98438,  
                    y = -1191.75,  
                    z = 75.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_859]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_860]],  
                    x = 23065.32813,  
                    y = -1096.359375,  
                    z = 75.546875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_850]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_699]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_825]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_821]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_822]],  
                x = 22799.98438,  
                y = -1279.21875,  
                z = 75.109375
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_819]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_826]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_827]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 5,  
              HairType = 6700334,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 5,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Fighter]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_809]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_810]],  
                x = 22800.46875,  
                y = -1281.71875,  
                z = 75.046875
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_807]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 1,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 20,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 3,  
              WeaponRightHand = 6755886,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Fighter]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_817]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_818]],  
                x = 22799.03125,  
                y = -1276.859375,  
                z = 75.078125
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_815]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 9,  
              HairType = 6700334,  
              HairColor = 4,  
              Tattoo = 23,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 0,  
              WeaponRightHand = 6756654,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Fighter]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_824]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_823]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_694]]
  }
}