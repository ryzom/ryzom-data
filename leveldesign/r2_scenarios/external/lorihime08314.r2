scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    LogicEntityReaction = 0,  
    Npc = 0,  
    Region = 0,  
    ActivityStep = 0,  
    ActionStep = 0,  
    Road = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    EventType = 0,  
    Position = 0,  
    Behavior = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    ChatAction = 0
  },  
  Acts = {
    {
      Cost = 41,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client1_184]],  
        [[Client1_186]],  
        [[Client1_188]],  
        [[Client2_92]],  
        [[Client2_94]],  
        [[Client2_96]],  
        [[Client1_230]]
      },  
      Title = [[Act 0]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_14]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_15]],  
                x = 22497.82813,  
                y = -1976.75,  
                z = 38.0625
              },  
              Angle = 1.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_12]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[LORI camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_26]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_27]],  
                x = 22484.51563,  
                y = -1980.171875,  
                z = 38.25
              },  
              Angle = -0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_24]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[LORI hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_42]],  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_43]],  
                x = 22497.125,  
                y = -1975.75,  
                z = 38.140625
              },  
              Angle = -1.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_40]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_352]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_353]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_354]],  
                            Emote = [[]],  
                            Who = [[Client1_42]],  
                            Facing = r2.RefId([[Client1_50]]),  
                            Says = [[Client1_357]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 8,  
              HairType = 5622062,  
              HairColor = 0,  
              Tattoo = 12,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 6703406,  
              FeetModel = 6702382,  
              HandsModel = 0,  
              ArmModel = 5606190,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 1,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Fihl]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_46]],  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_47]],  
                x = 22496.76563,  
                y = -1977.859375,  
                z = 38.125
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_44]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_346]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_347]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_348]],  
                            Emote = [[]],  
                            Who = [[Client1_46]],  
                            Facing = r2.RefId([[Client1_50]]),  
                            Says = [[Client1_349]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_351]],  
                    Name = [[Chat2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_363]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_364]],  
                            Emote = [[]],  
                            Who = [[Client1_46]],  
                            Facing = r2.RefId([[Client1_280]]),  
                            Says = [[Client1_365]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 8,  
              HairType = 2606,  
              HairColor = 0,  
              Tattoo = 12,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 6702894,  
              ArmModel = 5606190,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 4,  
              FeetColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Dae]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_50]],  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_51]],  
                x = 22496.46875,  
                y = -1976.578125,  
                z = 38.203125
              },  
              Angle = -0.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_48]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_337]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_340]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_341]],  
                            Emote = [[]],  
                            Who = [[Client1_50]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_344]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_342]],  
                        Time = 6,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_343]],  
                            Emote = [[]],  
                            Who = [[Client1_50]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_345]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_360]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_359]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 2,  
              HairType = 5621806,  
              HairColor = 1,  
              Tattoo = 12,  
              EyesColor = 1,  
              MorphTarget1 = 4,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 3,  
              Sex = 0,  
              JacketModel = 6704430,  
              TrouserModel = 5605934,  
              FeetModel = 6702382,  
              HandsModel = 0,  
              ArmModel = 6703918,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Dahv]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_58]],  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_59]],  
                x = 22487.48438,  
                y = -1980.8125,  
                z = 38.046875
              },  
              Angle = -0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_56]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 2,  
              HairType = 5622062,  
              HairColor = 0,  
              Tattoo = 12,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              Sex = 0,  
              JacketModel = 6706990,  
              TrouserModel = 6703406,  
              FeetModel = 6702382,  
              HandsModel = 6702894,  
              ArmModel = 6706734,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Dim]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_101]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_102]],  
                x = 22551.15625,  
                y = -2010.53125,  
                z = 31.046875
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_99]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 5,  
              HairType = 6700334,  
              HairColor = 3,  
              Tattoo = 21,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              BotAttackable = 0,  
              WeaponRightHand = 6755886,  
              Name = [[Gorge Guard]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_105]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_106]],  
                x = 22551.82813,  
                y = -1985.8125,  
                z = 31.71875
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_103]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 5,  
              HairType = 6700334,  
              HairColor = 1,  
              Tattoo = 16,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 6,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 0,  
              ArmColor = 3,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              BotAttackable = 0,  
              WeaponRightHand = 6755374,  
              Name = [[Gorge Guard]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_blunt_e4.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_11]],  
              Base = [[palette.entities.creatures.ccadb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_12]],  
                x = 22594.75,  
                y = -1987.0625,  
                z = 25.96875
              },  
              Angle = 2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_9]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_15]],  
              Base = [[palette.entities.creatures.ccadb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_16]],  
                x = 22612.1875,  
                y = -1971.078125,  
                z = 22.875
              },  
              Angle = -2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_13]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scowling Gingo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client4_3]],  
              Base = [[palette.entities.botobjects.fx_tr_pollen]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client4_4]],  
                x = 22648.84375,  
                y = -1855.953125,  
                z = 16.34375
              },  
              Angle = -1.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client4_1]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[The Tunnel]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_121]],  
              Base = [[palette.entities.botobjects.bag_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_122]],  
                x = 22487.625,  
                y = -1975.828125,  
                z = 38.265625
              },  
              Angle = 2.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_119]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[LORI bag 2 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_129]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_130]],  
                x = 22494.79688,  
                y = -1990.265625,  
                z = 37.421875
              },  
              Angle = 0.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_127]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_133]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_134]],  
                x = 22498.125,  
                y = -1999.375,  
                z = 35.84375
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_131]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client3_7]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client3_8]],  
                x = 22482.14063,  
                y = -1973.296875,  
                z = 39.125
              },  
              Angle = -0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client3_5]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_39]],  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_40]],  
                x = 22275.60938,  
                y = -1876,  
                z = -22.78125
              },  
              Angle = 0.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_37]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[KAE Building]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_51]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_52]],  
                x = 22450.40625,  
                y = -1806.265625,  
                z = 0.625
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_49]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[KAE Tower This Way]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_59]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_60]],  
                x = 22460.625,  
                y = -1808.625,  
                z = 1.09375
              },  
              Angle = -0.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_57]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6756910,  
              Name = [[Kaeno Tower Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f1.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_141]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_142]],  
                x = 22496.65625,  
                y = -1957.5,  
                z = 36.890625
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_139]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[LORI mekbar 1 - ROOMKILLER]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_145]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_146]],  
                x = 22497.625,  
                y = -1961.28125,  
                z = 37.09375
              },  
              Angle = -2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[LORI mekbar 2 - USES SPACE]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_63]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_64]],  
                x = 22355.82813,  
                y = -1804.921875,  
                z = -11.828125
              },  
              Angle = -1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_61]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[KAE Building Square]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_149]],  
              Base = [[palette.entities.creatures.chhdd2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_150]],  
                x = 22495.9375,  
                y = -1961.09375,  
                z = 37.078125
              },  
              Angle = -2.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_147]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_153]],  
              Base = [[palette.entities.creatures.chhdd2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_154]],  
                x = 22494.59375,  
                y = -1958.3125,  
                z = 36.765625
              },  
              Angle = -0.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client2_66]],  
              Name = [[Kaeno Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_65]],  
                x = -0.96875,  
                y = 0.3125,  
                z = -0.15625
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_68]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_69]],  
                    x = 22354.60938,  
                    y = -1809.359375,  
                    z = -12.203125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_71]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_72]],  
                    x = 22404.71875,  
                    y = -1838.515625,  
                    z = -9.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_74]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_75]],  
                    x = 22308.78125,  
                    y = -1861.5,  
                    z = -16.09375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_157]],  
              Base = [[palette.entities.creatures.chmdd2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_158]],  
                x = 22527.07813,  
                y = -1962.3125,  
                z = 32.09375
              },  
              Angle = -1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_155]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_186]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_187]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              ActivitiesId = {
                [[Client1_186]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_161]],  
              Base = [[palette.entities.creatures.chmdd2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_162]],  
                x = 22530.4375,  
                y = -1960.59375,  
                z = 31.984375
              },  
              Angle = -1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_159]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_188]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_189]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              ActivitiesId = {
                [[Client1_188]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_165]],  
              Base = [[palette.entities.creatures.chmdd2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_166]],  
                x = 22529.23438,  
                y = -1958.96875,  
                z = 32.046875
              },  
              Angle = -1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_163]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_184]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_185]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              ActivitiesId = {
                [[Client1_184]]
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_168]],  
              Name = [[Hime Raspal Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_167]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_170]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_171]],  
                    x = 22517.57813,  
                    y = -1953.609375,  
                    z = 32.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_173]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_174]],  
                    x = 22523.5,  
                    y = -1969.796875,  
                    z = 32.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_176]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_177]],  
                    x = 22541.09375,  
                    y = -1962.265625,  
                    z = 31.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_179]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_180]],  
                    x = 22536.78125,  
                    y = -1952.75,  
                    z = 31.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_182]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_183]],  
                    x = 22526.15625,  
                    y = -1952.640625,  
                    z = 32.34375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_82]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_83]],  
                x = 22365.84375,  
                y = -1824.65625,  
                z = -7.3125
              },  
              Angle = 2.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_80]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_92]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_93]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 10,  
              HairType = 5621806,  
              HairColor = 4,  
              Tattoo = 10,  
              EyesColor = 6,  
              MorphTarget1 = 0,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Kaethus]],  
              ActivitiesId = {
                [[Client2_92]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_86]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_87]],  
                x = 22369.46875,  
                y = -1829.953125,  
                z = -6.859375
              },  
              Angle = 2.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_84]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_96]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_97]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 5,  
              HairType = 5622062,  
              HairColor = 0,  
              Tattoo = 25,  
              EyesColor = 4,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 3,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              MorphTarget7 = 1,  
              MorphTarget8 = 5,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Kaethon]],  
              ActivitiesId = {
                [[Client2_96]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_90]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_91]],  
                x = 22362.5,  
                y = -1827.15625,  
                z = -7.34375
              },  
              Angle = 2.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_88]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_94]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_95]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 4,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 13,  
              EyesColor = 5,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 6,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Kaethi]],  
              ActivitiesId = {
                [[Client2_94]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_108]],  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_109]],  
                x = 22321.07813,  
                y = -1835.3125,  
                z = -12.015625
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_106]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6705198,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Kaeno Resource Manager]],  
              ActivitiesId = {
              },  
              Notes = [[This character takes care of the resources that the destroyed village Kaeno needs.]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_112]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_113]],  
                x = 22318.67188,  
                y = -1835.546875,  
                z = -12.390625
              },  
              Angle = -0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_110]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[KAE Resource Building]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_116]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_117]],  
                x = 22656.04688,  
                y = -1783.796875,  
                z = 13.21875
              },  
              Angle = -2.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_114]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[-KAE Blocks so you]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_120]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_121]],  
                x = 22656.0625,  
                y = -1793.828125,  
                z = 13.5625
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_118]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[-KAE To Other Side]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_132]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_133]],  
                x = 22679.875,  
                y = -1892.453125,  
                z = 16.4375
              },  
              Angle = -2.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_130]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[-KAE Dont Misrun]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_144]],  
              Base = [[palette.entities.botobjects.totem_bird]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_145]],  
                x = 22537.17188,  
                y = -1834.109375,  
                z = 4.96875
              },  
              Angle = -0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_142]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[KAE Totem]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_214]],  
              Name = [[Hime Yubo Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_216]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_217]],  
                    x = 22514.92188,  
                    y = -2005.296875,  
                    z = 31.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_219]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_220]],  
                    x = 22506.90625,  
                    y = -1986.8125,  
                    z = 36.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_222]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_223]],  
                    x = 22533.75,  
                    y = -1985.78125,  
                    z = 32.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_225]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_226]],  
                    x = 22534.125,  
                    y = -2011.84375,  
                    z = 32.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_228]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_229]],  
                    x = 22520.67188,  
                    y = -2020.296875,  
                    z = 32.84375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_213]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client2_159]],  
              Name = [[Goari near Kaeno]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_158]],  
                x = -0.953125,  
                y = 0.265625,  
                z = 0.109375
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_161]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_162]],  
                    x = 22481.625,  
                    y = -1795.9375,  
                    z = 1.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_164]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_165]],  
                    x = 22465.23438,  
                    y = -1829.796875,  
                    z = 1.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_167]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_168]],  
                    x = 22488.96875,  
                    y = -1821.1875,  
                    z = 3.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_170]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_171]],  
                    x = 22496.875,  
                    y = -1798.59375,  
                    z = 1.328125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_184]],  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_185]],  
                x = 22463.85938,  
                y = -1814.75,  
                z = 1.78125
              },  
              Angle = 1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_182]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_283]],  
              Name = [[Hime to Kaeno]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_285]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_286]],  
                    x = 22501,  
                    y = -1987.40625,  
                    z = 37.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_288]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_289]],  
                    x = 22577.40625,  
                    y = -1993.265625,  
                    z = 27.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_291]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_292]],  
                    x = 22621.92188,  
                    y = -1960.734375,  
                    z = 23.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_294]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_295]],  
                    x = 22653.51563,  
                    y = -1854.234375,  
                    z = 15.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_297]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_298]],  
                    x = 22611.90625,  
                    y = -1813.578125,  
                    z = 9.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_300]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_301]],  
                    x = 22552.375,  
                    y = -1838.359375,  
                    z = 4.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_303]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_304]],  
                    x = 22521.51563,  
                    y = -1801.96875,  
                    z = 4.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_306]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_307]],  
                    x = 22486.5625,  
                    y = -1805.78125,  
                    z = 1.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_309]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_310]],  
                    x = 22486.40625,  
                    y = -1874.234375,  
                    z = 0.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_312]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_313]],  
                    x = 22432.10938,  
                    y = -1874.03125,  
                    z = -3.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_315]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_316]],  
                    x = 22378.46875,  
                    y = -1831.140625,  
                    z = -7.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_318]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_319]],  
                    x = 22325.10938,  
                    y = -1835.859375,  
                    z = -11.84375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_282]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client2_225]],  
              Name = [[Tunnel Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_224]],  
                x = -2,  
                y = -1.5625,  
                z = -0.59375
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_227]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_228]],  
                    x = 22645.875,  
                    y = -1878.390625,  
                    z = 19.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_230]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_231]],  
                    x = 22656.0625,  
                    y = -1875.3125,  
                    z = 19.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_233]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_234]],  
                    x = 22674.07813,  
                    y = -1843.90625,  
                    z = 14.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_236]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_237]],  
                    x = 22634.26563,  
                    y = -1821,  
                    z = 14.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_239]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_240]],  
                    x = 22628.46875,  
                    y = -1826.15625,  
                    z = 13.109375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_335]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_336]],  
                x = 22535.5625,  
                y = -1870.875,  
                z = 7.78125
              },  
              Angle = 1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_333]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[KAE Message Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client2_346]],  
              Name = [[Kaeno Messenger To Guard]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_345]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_348]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_349]],  
                    x = 22541.5,  
                    y = -1879.59375,  
                    z = 9.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_351]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_352]],  
                    x = 22545,  
                    y = -1866,  
                    z = 8.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_354]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_355]],  
                    x = 22542,  
                    y = -1832.515625,  
                    z = 4.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_357]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_358]],  
                    x = 22521.5,  
                    y = -1804.71875,  
                    z = 5.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_360]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_361]],  
                    x = 22462.625,  
                    y = -1808.890625,  
                    z = 0.71875
                  },  
                  InheritPos = 1
                }
              }
            }
          },  
          InstanceId = [[Client1_5]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_212]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_196]],  
              Base = [[palette.entities.creatures.chddb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_197]],  
                x = 22522.73438,  
                y = -2004.21875,  
                z = 33.09375
              },  
              Angle = -1.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_194]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Sprightly Yubo]],  
              ActivitiesId = {
                [[Client1_230]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_192]],  
              Base = [[palette.entities.creatures.chddb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_193]],  
                x = 22521.53125,  
                y = -2003.796875,  
                z = 33.03125
              },  
              Angle = -1.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_190]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_230]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_231]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Timorous Yubo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_204]],  
              Base = [[palette.entities.creatures.chddb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_205]],  
                x = 22521.90625,  
                y = -2005.390625,  
                z = 32.984375
              },  
              Angle = -1.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_202]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_200]],  
              Base = [[palette.entities.creatures.chddb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_201]],  
                x = 22521.9375,  
                y = -2002.890625,  
                z = 33.109375
              },  
              Angle = -1.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_198]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_211]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_210]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_4]],  
      Events = {
      },  
      ManualWeather = 0
    },  
    {
      Cost = 7,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client2_172]],  
        [[Client2_178]],  
        [[Client2_180]],  
        [[Client2_190]],  
        [[Client2_196]],  
        [[Client1_272]],  
        [[Client1_274]]
      },  
      Title = [[Act I: Before Things]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_152]],  
              Base = [[palette.entities.creatures.cbbdb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_153]],  
                x = 22476.9375,  
                y = -1813.09375,  
                z = 2.09375
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_150]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_180]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_181]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Bloated Izam]],  
              ActivitiesId = {
                [[Client2_180]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_176]],  
              Base = [[palette.entities.creatures.cbbdb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_177]],  
                x = 22475.53125,  
                y = -1819.65625,  
                z = 2.0625
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_174]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_178]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_179]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Gorged Izam]],  
              ActivitiesId = {
                [[Client2_178]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_200]],  
              Base = [[palette.entities.creatures.cpfdb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_201]],  
                x = 22494.79688,  
                y = -1835.953125,  
                z = 1.234375
              },  
              Angle = 2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_198]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Sluggish Shooki]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_266]],  
              Base = [[palette.entities.creatures.cccdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_267]],  
                x = 22486.375,  
                y = -1815,  
                z = 2.25
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_264]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_274]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_275]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Baying Goari]],  
              ActivitiesId = {
                [[Client1_274]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_270]],  
              Base = [[palette.entities.creatures.cccdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_271]],  
                x = 22487.20313,  
                y = -1810.203125,  
                z = 1.640625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_268]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_272]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_273]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Baying Goari]],  
              ActivitiesId = {
                [[Client1_272]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_284]],  
              Base = [[palette.entities.creatures.cpfdb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_285]],  
                x = 22636.57813,  
                y = -1806.671875,  
                z = 12.859375
              },  
              Angle = 2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_282]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Drowsy Shooki]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_288]],  
              Base = [[palette.entities.creatures.cpfdf4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_289]],  
                x = 22633.54688,  
                y = -1809.046875,  
                z = 12.015625
              },  
              Angle = 2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_286]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Devastating Shooki]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_6]],  
      Events = {
      },  
      ManualWeather = 0
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      InstanceId = [[Client1_276]],  
      WeatherValue = 0,  
      Title = [[Act II: The Traveling Helpers]],  
      ActivitiesIds = {
        [[Client1_320]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_280]],  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_281]],  
                x = 22499.54688,  
                y = -1987.640625,  
                z = 37.28125
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_278]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_320]],  
                    Name = [[sInit]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_321]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client2_366]]),  
                        ActivityZoneId = r2.RefId([[Client1_283]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_322]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_323]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_323]],  
                    Name = [[ATKAE]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_324]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_325]],  
                            Emote = [[]],  
                            Who = [[Client1_280]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_371]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_326]],  
                        Time = 5,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_327]],  
                            Emote = [[Puzzled]],  
                            Who = [[Client1_280]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_372]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client2_362]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client2_363]],  
                            Emote = [[]],  
                            Who = [[Client1_280]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_364]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client2_366]],  
                    Name = [[athim]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client2_367]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client2_368]],  
                            Emote = [[]],  
                            Who = [[Client1_280]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_369]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 5,  
              HairType = 2350,  
              HairColor = 1,  
              Tattoo = 0,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 7,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6704430,  
              TrouserModel = 6706222,  
              FeetModel = 6702382,  
              HandsModel = 6703150,  
              ArmModel = 6706478,  
              JacketColor = 3,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Daghn]],  
              ActivitiesId = {
                [[Client1_320]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          InstanceId = [[Client1_277]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      States = {
      }
    },  
    {
      Cost = 9,  
      Class = [[Act]],  
      InstanceId = [[Client2_202]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
        [[Client2_248]],  
        [[Client2_274]],  
        [[Client2_276]],  
        [[Client2_278]],  
        [[Client2_279]]
      },  
      Version = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_268]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_269]],  
                x = 22645.34375,  
                y = -1877.40625,  
                z = 18.875
              },  
              Angle = -1.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_266]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client2_325]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client2_327]],  
                        Entity = r2.RefId([[Client2_206]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client2_326]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client2_278]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client2_324]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_276]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_277]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Hime Assault Kirosta]],  
              ActivitiesId = {
                [[Client2_276]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_272]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_273]],  
                x = 22631,  
                y = -1826.625,  
                z = 12.453125
              },  
              Angle = -1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_270]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client2_330]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client2_332]],  
                        Entity = r2.RefId([[Client2_206]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client2_331]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client2_279]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client2_329]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_274]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_275]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Kaeno Assault Kirosta]],  
              ActivitiesId = {
                [[Client2_274]]
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_343]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_344]],  
                x = 22529.54688,  
                y = -1882.640625,  
                z = 6.484375
              },  
              Angle = -0.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_341]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 5607726,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5607470,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5595694,  
              Name = [[Kaeno Messenger]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b3.creature]]
            }
          },  
          InstanceId = [[Client2_203]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client2_243]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_206]],  
              Base = [[palette.entities.creatures.ckeic4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_207]],  
                x = 22651.39063,  
                y = -1852.015625,  
                z = 15.3125
              },  
              Angle = -1.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_204]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client2_323]],  
                    LogicEntityAction = [[Client2_325]],  
                    ActionStep = [[Client2_327]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client2_328]],  
                    LogicEntityAction = [[Client2_330]],  
                    ActionStep = [[Client2_332]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client2_290]],  
                    Name = [[start]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client2_291]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client2_292]],  
                            Emote = [[]],  
                            Who = [[Client2_59]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_296]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_248]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_249]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_278]],  
                    Name = [[HIME]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_280]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_279]],  
                    Name = [[KAENO]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_281]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Raider Kipucker]],  
              ActivitiesId = {
                [[Client2_248]],  
                [[Client2_278]],  
                [[Client2_279]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_210]],  
              Base = [[palette.entities.creatures.ckeic3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_211]],  
                x = 22645.79688,  
                y = -1856.21875,  
                z = 16.234375
              },  
              Angle = -1.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_208]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Raider Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_214]],  
              Base = [[palette.entities.creatures.ckeic3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_215]],  
                x = 22658.54688,  
                y = -1852.390625,  
                z = 14.84375
              },  
              Angle = -1.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_212]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Raider Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_218]],  
              Base = [[palette.entities.creatures.ckeic2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_219]],  
                x = 22651.78125,  
                y = -1863.328125,  
                z = 17.625
              },  
              Angle = -1.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_216]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_246]],  
              Base = [[palette.entities.creatures.ckeic2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_247]],  
                x = 22640.375,  
                y = -1850.671875,  
                z = 13.921875
              },  
              Angle = 2.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_244]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_264]],  
              Base = [[palette.entities.creatures.ckeic2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_265]],  
                x = 22647.53125,  
                y = -1871.015625,  
                z = 17.515625
              },  
              Angle = -1.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_262]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client2_242]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client2_241]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Title = [[Act III: Kitins Arrive]],  
      States = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client2_321]],  
      WeatherValue = 0,  
      Title = [[Act IV: Blastercalm]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client2_322]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_328]],  
        Count = 9,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_329]],  
        Count = 10,  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_330]],  
        Count = 1,  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_331]],  
        Count = 3,  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_332]],  
        Count = 1,  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_344]],  
        Count = 1,  
        Text = [[These are dire times, I fear that we might face hard times to come.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_345]],  
        Count = 1,  
        Text = [[I fear that the others might not do well now, I wonder how they are...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_349]],  
        Count = 3,  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_350]],  
        Count = 1,  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_355]],  
        Count = 2,  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_356]],  
        Count = 1,  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_357]],  
        Count = 2,  
        Text = [[Daghn can do it, I know he can!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_293]],  
        Count = 1,  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_294]],  
        Count = 1,  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_365]],  
        Count = 2,  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_366]],  
        Count = 1,  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_295]],  
        Count = 1,  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_296]],  
        Count = 1,  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_364]],  
        Count = 3,  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_365]],  
        Count = 1,  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_369]],  
        Count = 3,  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_370]],  
        Count = 1,  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_371]],  
        Count = 1,  
        Text = [[I have come too see what aid you need... (should be "to see")]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_372]],  
        Count = 2,  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_373]],  
        Count = 1,  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}