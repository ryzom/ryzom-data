scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Position = 0,  
    TextManager = 0,  
    NpcCustom = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 23,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      InstanceId = [[Client1_1135]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1139]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Exit Tower]],  
              Position = {
                y = -1967.8125,  
                x = 27940.65625,  
                InstanceId = [[Client1_1142]],  
                Class = [[Position]],  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1143]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 1]],  
              Position = {
                y = -1851.296875,  
                x = 27899.29688,  
                InstanceId = [[Client1_1146]],  
                Class = [[Position]],  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1147]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 1]],  
              Position = {
                y = -1851.203125,  
                x = 27895.9375,  
                InstanceId = [[Client1_1150]],  
                Class = [[Position]],  
                z = -13.875
              },  
              Angle = -2.203125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1151]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Tower 1]],  
              Position = {
                y = -1850.9375,  
                x = 27931.39063,  
                InstanceId = [[Client1_1154]],  
                Class = [[Position]],  
                z = -14.5
              },  
              Angle = -1.71875,  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1155]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 2]],  
              Position = {
                y = -1844.8125,  
                x = 27922.95313,  
                InstanceId = [[Client1_1158]],  
                Class = [[Position]],  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1159]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 3]],  
              Position = {
                y = -1853.234375,  
                x = 27937.125,  
                InstanceId = [[Client1_1162]],  
                Class = [[Position]],  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1163]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 1]],  
              Position = {
                y = -1838.734375,  
                x = 27892.96875,  
                InstanceId = [[Client1_1166]],  
                Class = [[Position]],  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1167]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 2]],  
              Position = {
                y = -1840.8125,  
                x = 27937.21875,  
                InstanceId = [[Client1_1170]],  
                Class = [[Position]],  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1173]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1171]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 1]],  
              Position = {
                y = -1856.328125,  
                x = 27930.85938,  
                InstanceId = [[Client1_1174]],  
                Class = [[Position]],  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1177]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1175]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 2]],  
              Position = {
                y = -1853.578125,  
                x = 27898.125,  
                InstanceId = [[Client1_1178]],  
                Class = [[Position]],  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1181]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1179]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 2]],  
              Position = {
                y = -1887.59375,  
                x = 27942.53125,  
                InstanceId = [[Client1_1182]],  
                Class = [[Position]],  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1185]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1183]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 3]],  
              Position = {
                y = -1901.703125,  
                x = 27941.15625,  
                InstanceId = [[Client1_1186]],  
                Class = [[Position]],  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1189]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1187]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 4]],  
              Position = {
                y = -1896.890625,  
                x = 27943.17188,  
                InstanceId = [[Client1_1190]],  
                Class = [[Position]],  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1193]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1191]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 3]],  
              Position = {
                y = -1900.859375,  
                x = 27939.875,  
                InstanceId = [[Client1_1194]],  
                Class = [[Position]],  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1197]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1195]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Spawn Point]],  
              Position = {
                y = -1878.25,  
                x = 27915.09375,  
                InstanceId = [[Client1_1198]],  
                Class = [[Position]],  
                z = -10.640625
              },  
              Angle = 1.625,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1209]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1207]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Circle 1]],  
              Position = {
                y = -1877.34375,  
                x = 27895.6875,  
                InstanceId = [[Client1_1210]],  
                Class = [[Position]],  
                z = -13.0625
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1213]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1211]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 3]],  
              Position = {
                y = -1893.28125,  
                x = 27964.14063,  
                InstanceId = [[Client1_1214]],  
                Class = [[Position]],  
                z = -13.71875
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Home Region]],  
              InstanceId = [[Client1_1216]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1215]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1218]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1906.75,  
                    x = 27932.96875,  
                    InstanceId = [[Client1_1219]],  
                    Class = [[Position]],  
                    z = -7.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1221]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1881.984375,  
                    x = 27932.32813,  
                    InstanceId = [[Client1_1222]],  
                    Class = [[Position]],  
                    z = -9.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1224]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1863.34375,  
                    x = 27935.84375,  
                    InstanceId = [[Client1_1225]],  
                    Class = [[Position]],  
                    z = -11.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1861.46875,  
                    x = 27890.98438,  
                    InstanceId = [[Client1_1228]],  
                    Class = [[Position]],  
                    z = -14.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1869.015625,  
                    x = 27883.375,  
                    InstanceId = [[Client1_1231]],  
                    Class = [[Position]],  
                    z = -13.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1903.015625,  
                    x = 27887.78125,  
                    InstanceId = [[Client1_1234]],  
                    Class = [[Position]],  
                    z = -10.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1901.34375,  
                    x = 27898.15625,  
                    InstanceId = [[Client1_1237]],  
                    Class = [[Position]],  
                    z = -7.859375
                  }
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1263]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1264]],  
                x = 28058.39063,  
                y = -2034.015625,  
                z = 9.828125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1261]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1267]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1268]],  
                x = 28140.42188,  
                y = -1974.828125,  
                z = 15.875
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1265]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[watch tower 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1271]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1272]],  
                x = 28223.21875,  
                y = -1946.765625,  
                z = 32.015625
              },  
              Angle = 0.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1269]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[watch tower 3]]
            }
          },  
          InstanceId = [[Client1_1136]]
        },  
        {
          InstanceId = [[Client1_1260]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Ceremony Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1259]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1258]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_1240]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1238]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ceremony Master]],  
              Position = {
                y = -1876.734375,  
                x = 27908.625,  
                InstanceId = [[Client1_1241]],  
                Class = [[Position]],  
                z = -11.28125
              },  
              Angle = 0,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1256]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1254]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 4,  
              Name = [[Ceremony Guard]],  
              Position = {
                y = -1874.671875,  
                x = 27907.17188,  
                InstanceId = [[Client1_1257]],  
                Class = [[Position]],  
                z = -11.9375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1252]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1250]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 4,  
              Name = [[Ceremony Guard]],  
              Position = {
                y = -1878.078125,  
                x = 27907.75,  
                InstanceId = [[Client1_1253]],  
                Class = [[Position]],  
                z = -10.984375
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 12
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      ManualWeather = 1,  
      Version = 1,  
      Title = [[Ceremony]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1133]]
  }
}