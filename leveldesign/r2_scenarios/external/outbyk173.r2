scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 27,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    Npc = 0,  
    ChatAction = 0,  
    Region = 0,  
    ActivityStep = 0,  
    ActionStep = 0,  
    EventType = 0,  
    LogicEntityReaction = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Road = 0,  
    Position = 0,  
    Behavior = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    MapDescription = 0
  },  
  Acts = {
    {
      Cost = 22,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
        [[Client1_138]],  
        [[Client1_163]],  
        [[Client1_166]],  
        [[Client1_358]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_30]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kitin Cave]],  
              Position = {
                y = -2218.90625,  
                x = 29704.92188,  
                InstanceId = [[Client1_31]],  
                Class = [[Position]],  
                z = -17.859375
              },  
              Angle = 2.671875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_34]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_32]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Show]],  
              Position = {
                y = -2123.0625,  
                x = 29676.78125,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = -16.984375
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_38]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_36]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Homin Camp]],  
              Position = {
                y = -2176.5625,  
                x = 29649.26563,  
                InstanceId = [[Client1_39]],  
                Class = [[Position]],  
                z = -16.8125
              },  
              Angle = -0.484375,  
              Base = [[palette.entities.botobjects.roadsign]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -2051.046875,  
                x = 29551.48438,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = -22.125
              },  
              Angle = -2.9375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -2057.046875,  
                x = 29476.76563,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = -22.375
              },  
              Angle = 0.53125,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_54]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_52]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Bandit Tower]],  
              Position = {
                y = -2093.921875,  
                x = 29518.67188,  
                InstanceId = [[Client1_55]],  
                Class = [[Position]],  
                z = -23.9375
              },  
              Angle = -4.75,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_58]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -2026.875,  
                x = 29488.76563,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = -19.3125
              },  
              Angle = 0.125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1882]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1880]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -2010.75,  
                x = 29498.64063,  
                InstanceId = [[Client1_1883]],  
                Class = [[Position]],  
                z = -18.765625
              },  
              Angle = -1.28125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1886]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1884]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              Position = {
                y = -2017.03125,  
                x = 29517.92188,  
                InstanceId = [[Client1_1887]],  
                Class = [[Position]],  
                z = -20.125
              },  
              Angle = -2.375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_90]],  
              ActivitiesId = {
                [[Client1_166]]
              },  
              HairType = 5621806,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 11,  
              GabaritHeight = 10,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_88]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_166]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_167]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 5,  
              FeetModel = 0,  
              Angle = -1.0625,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Roodkapje]],  
              Position = {
                y = -2026.359375,  
                x = 29508.84375,  
                InstanceId = [[Client1_91]],  
                Class = [[Position]],  
                z = -21
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 5,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_94]],  
              ActivitiesId = {
                [[Client1_138]]
              },  
              HairType = 2350,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 10,  
              GabaritHeight = 9,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_92]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_138]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_165]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Angle = 1.984375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Ik kan al lezen]],  
              Position = {
                y = -2045.875,  
                x = 29515.59375,  
                InstanceId = [[Client1_95]],  
                Class = [[Position]],  
                z = -21.828125
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_98]],  
              ActivitiesId = {
                [[Client1_163]]
              },  
              HairType = 2606,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 9,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 4,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_96]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_163]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_164]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Angle = 0.578125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[Grote Smurf]],  
              Position = {
                y = -2046.65625,  
                x = 29508.95313,  
                InstanceId = [[Client1_99]],  
                Class = [[Position]],  
                z = -21.765625
              },  
              Sex = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 6,  
              Tattoo = 7
            },  
            {
              InheritPos = 1,  
              Name = [[Town]],  
              InstanceId = [[Client1_141]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_143]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2057.015625,  
                    x = 29508.5,  
                    InstanceId = [[Client1_144]],  
                    Class = [[Position]],  
                    z = -21.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_146]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2057.015625,  
                    x = 29508.5,  
                    InstanceId = [[Client1_147]],  
                    Class = [[Position]],  
                    z = -21.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_149]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2040.40625,  
                    x = 29469.6875,  
                    InstanceId = [[Client1_150]],  
                    Class = [[Position]],  
                    z = -22.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_152]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2023.03125,  
                    x = 29504.9375,  
                    InstanceId = [[Client1_153]],  
                    Class = [[Position]],  
                    z = -20.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_155]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.5,  
                    x = 29532.5625,  
                    InstanceId = [[Client1_156]],  
                    Class = [[Position]],  
                    z = -20.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_158]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2068.90625,  
                    x = 29549.25,  
                    InstanceId = [[Client1_159]],  
                    Class = [[Position]],  
                    z = -22.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2063.609375,  
                    x = 29523.15625,  
                    InstanceId = [[Client1_162]],  
                    Class = [[Position]],  
                    z = -22.296875
                  }
                }
              },  
              Position = {
                y = 0.09375,  
                x = 0.21875,  
                InstanceId = [[Client1_140]],  
                Class = [[Position]],  
                z = -0.015625
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Bandit Place]],  
              InstanceId = [[Client1_191]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_190]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_193]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2242.34375,  
                    x = 29534.01563,  
                    InstanceId = [[Client1_194]],  
                    Class = [[Position]],  
                    z = -20.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_196]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2242.546875,  
                    x = 29533.96875,  
                    InstanceId = [[Client1_197]],  
                    Class = [[Position]],  
                    z = -20.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_199]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2212.703125,  
                    x = 29527.03125,  
                    InstanceId = [[Client1_200]],  
                    Class = [[Position]],  
                    z = -19.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_202]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2216.140625,  
                    x = 29503.76563,  
                    InstanceId = [[Client1_203]],  
                    Class = [[Position]],  
                    z = -18.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_205]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.953125,  
                    x = 29506.0625,  
                    InstanceId = [[Client1_206]],  
                    Class = [[Position]],  
                    z = -19.265625
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_295]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_293]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              Position = {
                y = -2243.34375,  
                x = 29519.23438,  
                InstanceId = [[Client1_296]],  
                Class = [[Position]],  
                z = -22.078125
              },  
              Angle = 1.65625,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kitin Camp]],  
              InstanceId = [[Client1_300]],  
              Class = [[Region]],  
              Position = {
                y = -0.265625,  
                x = -0.03125,  
                InstanceId = [[Client1_299]],  
                Class = [[Position]],  
                z = 0.796875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_302]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2225.984375,  
                    x = 29903.57813,  
                    InstanceId = [[Client1_303]],  
                    Class = [[Position]],  
                    z = -9.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_305]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2203.046875,  
                    x = 29777.8125,  
                    InstanceId = [[Client1_306]],  
                    Class = [[Position]],  
                    z = -16.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_308]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2127.296875,  
                    x = 29777.375,  
                    InstanceId = [[Client1_309]],  
                    Class = [[Position]],  
                    z = -17.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_311]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2142.65625,  
                    x = 29905.3125,  
                    InstanceId = [[Client1_312]],  
                    Class = [[Position]],  
                    z = -14.15625
                  }
                }
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_345]],  
              ActivitiesId = {
                [[Client1_358]]
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_343]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_404]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_405]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_406]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_407]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_358]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_359]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_348]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_403]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_404]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Speed = [[run]],  
              Angle = -1.484375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Tribe Leader]],  
              Position = {
                y = -2013.671875,  
                x = 29495.01563,  
                InstanceId = [[Client1_346]],  
                Class = [[Position]],  
                z = -18.921875
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 12
            },  
            {
              InheritPos = 1,  
              Name = [[Somewhere to Leader]],  
              InstanceId = [[Client1_348]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_347]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_350]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2017.796875,  
                    x = 29495.92188,  
                    InstanceId = [[Client1_351]],  
                    Class = [[Position]],  
                    z = -18.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_353]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2019.984375,  
                    x = 29497.125,  
                    InstanceId = [[Client1_354]],  
                    Class = [[Position]],  
                    z = -19.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_356]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2023.578125,  
                    x = 29500.01563,  
                    InstanceId = [[Client1_357]],  
                    Class = [[Position]],  
                    z = -19.9375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Tower to Leader]],  
              InstanceId = [[Client1_414]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_416]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2073.828125,  
                    x = 29520.78125,  
                    InstanceId = [[Client1_417]],  
                    Class = [[Position]],  
                    z = -24.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_419]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2046.453125,  
                    x = 29518.3125,  
                    InstanceId = [[Client1_420]],  
                    Class = [[Position]],  
                    z = -21.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_422]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2024.75,  
                    x = 29500.82813,  
                    InstanceId = [[Client1_423]],  
                    Class = [[Position]],  
                    z = -20.171875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_413]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Leader to Bandits]],  
              InstanceId = [[Client1_447]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_449]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2073.53125,  
                    x = 29509.17188,  
                    InstanceId = [[Client1_450]],  
                    Class = [[Position]],  
                    z = -23
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_452]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2090.1875,  
                    x = 29506.60938,  
                    InstanceId = [[Client1_453]],  
                    Class = [[Position]],  
                    z = -22.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_455]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2112.890625,  
                    x = 29484.26563,  
                    InstanceId = [[Client1_456]],  
                    Class = [[Position]],  
                    z = -22.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_458]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2194.328125,  
                    x = 29495.40625,  
                    InstanceId = [[Client1_459]],  
                    Class = [[Position]],  
                    z = -17.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_461]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2189.015625,  
                    x = 29497.625,  
                    InstanceId = [[Client1_462]],  
                    Class = [[Position]],  
                    z = -16.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_464]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2187.765625,  
                    x = 29497.1875,  
                    InstanceId = [[Client1_465]],  
                    Class = [[Position]],  
                    z = -17.046875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_446]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Bandits To Leader]],  
              InstanceId = [[Client1_475]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_474]],  
                Class = [[Position]],  
                z = 0.015625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_477]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2111.4375,  
                    x = 29485,  
                    InstanceId = [[Client1_478]],  
                    Class = [[Position]],  
                    z = -22.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_480]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2088.078125,  
                    x = 29507.39063,  
                    InstanceId = [[Client1_481]],  
                    Class = [[Position]],  
                    z = -22.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_483]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2024.75,  
                    x = 29500.82813,  
                    InstanceId = [[Client1_484]],  
                    Class = [[Position]],  
                    z = -20.15625
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_525]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_523]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kami Sentinel]],  
              Position = {
                y = -2103.578125,  
                x = 29468.51563,  
                InstanceId = [[Client1_526]],  
                Class = [[Position]],  
                z = -24.203125
              },  
              Angle = -0.984375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_529]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_527]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kami Background]],  
              Position = {
                y = -2102.796875,  
                x = 29466.95313,  
                InstanceId = [[Client1_530]],  
                Class = [[Position]],  
                z = -23.90625
              },  
              Angle = -0.984375,  
              Base = [[palette.entities.botobjects.tr_s2_bamboo_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_537]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_535]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kitin Tower]],  
              Position = {
                y = -2186.8125,  
                x = 29621.89063,  
                InstanceId = [[Client1_538]],  
                Class = [[Position]],  
                z = -17.65625
              },  
              Angle = 0.828125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_541]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_539]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              Position = {
                y = -2208.171875,  
                x = 29673.1875,  
                InstanceId = [[Client1_542]],  
                Class = [[Position]],  
                z = -18
              },  
              Angle = 0.34375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_545]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_543]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              Position = {
                y = -2222.78125,  
                x = 29679.0625,  
                InstanceId = [[Client1_546]],  
                Class = [[Position]],  
                z = -19.484375
              },  
              Angle = 1.109375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_549]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_547]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              Position = {
                y = -2226.046875,  
                x = 29690.01563,  
                InstanceId = [[Client1_550]],  
                Class = [[Position]],  
                z = -18.390625
              },  
              Angle = 0.96875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_553]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_551]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              Position = {
                y = -2111.59375,  
                x = 29680.32813,  
                InstanceId = [[Client1_554]],  
                Class = [[Position]],  
                z = -17.15625
              },  
              Angle = -1.734375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_573]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_571]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin totem 1]],  
              Position = {
                y = -2132.53125,  
                x = 29747.34375,  
                InstanceId = [[Client1_574]],  
                Class = [[Position]],  
                z = -20.328125
              },  
              Angle = -1.640625,  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Boss Zone]],  
              InstanceId = [[Client1_18940]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_18939]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18942]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2143.09375,  
                    x = 29906.3125,  
                    InstanceId = [[Client1_18943]],  
                    Class = [[Position]],  
                    z = -14.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18945]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2147.359375,  
                    x = 29927.65625,  
                    InstanceId = [[Client1_18946]],  
                    Class = [[Position]],  
                    z = -17.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18948]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2170.078125,  
                    x = 29927.0625,  
                    InstanceId = [[Client1_18949]],  
                    Class = [[Position]],  
                    z = -18.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18951]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2175.078125,  
                    x = 29906.45313,  
                    InstanceId = [[Client1_18952]],  
                    Class = [[Position]],  
                    z = -17.546875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kitins To Town]],  
              InstanceId = [[Client1_18968]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18970]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2172.953125,  
                    x = 29748.5625,  
                    InstanceId = [[Client1_18971]],  
                    Class = [[Position]],  
                    z = -18.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18973]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2205.25,  
                    x = 29744.26563,  
                    InstanceId = [[Client1_18974]],  
                    Class = [[Position]],  
                    z = -17.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18976]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2217.875,  
                    x = 29731.60938,  
                    InstanceId = [[Client1_18977]],  
                    Class = [[Position]],  
                    z = -18.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18979]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2220.671875,  
                    x = 29720.89063,  
                    InstanceId = [[Client1_18980]],  
                    Class = [[Position]],  
                    z = -17.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18982]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2221.8125,  
                    x = 29701.6875,  
                    InstanceId = [[Client1_18983]],  
                    Class = [[Position]],  
                    z = -17.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18985]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2217.59375,  
                    x = 29701.67188,  
                    InstanceId = [[Client1_18986]],  
                    Class = [[Position]],  
                    z = -18.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18988]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2194.390625,  
                    x = 29677.60938,  
                    InstanceId = [[Client1_18989]],  
                    Class = [[Position]],  
                    z = -19.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18991]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2179.296875,  
                    x = 29648.35938,  
                    InstanceId = [[Client1_18992]],  
                    Class = [[Position]],  
                    z = -17.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18994]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2177.78125,  
                    x = 29634.65625,  
                    InstanceId = [[Client1_18995]],  
                    Class = [[Position]],  
                    z = -17.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_18997]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2174.6875,  
                    x = 29616.59375,  
                    InstanceId = [[Client1_18998]],  
                    Class = [[Position]],  
                    z = -17.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19000]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2174.796875,  
                    x = 29597.875,  
                    InstanceId = [[Client1_19001]],  
                    Class = [[Position]],  
                    z = -18.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19003]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2212.109375,  
                    x = 29562.25,  
                    InstanceId = [[Client1_19004]],  
                    Class = [[Position]],  
                    z = -17.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19006]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2204.859375,  
                    x = 29545.04688,  
                    InstanceId = [[Client1_19007]],  
                    Class = [[Position]],  
                    z = -19.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19009]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2163.078125,  
                    x = 29514.78125,  
                    InstanceId = [[Client1_19010]],  
                    Class = [[Position]],  
                    z = -18.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19012]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2112.15625,  
                    x = 29484.65625,  
                    InstanceId = [[Client1_19013]],  
                    Class = [[Position]],  
                    z = -22.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19015]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2086.71875,  
                    x = 29509.125,  
                    InstanceId = [[Client1_19016]],  
                    Class = [[Position]],  
                    z = -23.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19018]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2051.796875,  
                    x = 29519.90625,  
                    InstanceId = [[Client1_19019]],  
                    Class = [[Position]],  
                    z = -21.609375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_18967]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kitin Tower To Town]],  
              InstanceId = [[Client1_19021]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_19020]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19023]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2177.421875,  
                    x = 29627.9375,  
                    InstanceId = [[Client1_19024]],  
                    Class = [[Position]],  
                    z = -17.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19026]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2174.25,  
                    x = 29614.51563,  
                    InstanceId = [[Client1_19027]],  
                    Class = [[Position]],  
                    z = -17.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19029]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2174.015625,  
                    x = 29596.90625,  
                    InstanceId = [[Client1_19030]],  
                    Class = [[Position]],  
                    z = -18.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19032]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2212.546875,  
                    x = 29562,  
                    InstanceId = [[Client1_19033]],  
                    Class = [[Position]],  
                    z = -17.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19035]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2204.84375,  
                    x = 29544.51563,  
                    InstanceId = [[Client1_19036]],  
                    Class = [[Position]],  
                    z = -19.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19038]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2159.625,  
                    x = 29513.32813,  
                    InstanceId = [[Client1_19039]],  
                    Class = [[Position]],  
                    z = -19.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19041]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2111.46875,  
                    x = 29483.89063,  
                    InstanceId = [[Client1_19042]],  
                    Class = [[Position]],  
                    z = -23.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19044]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2092.875,  
                    x = 29506.73438,  
                    InstanceId = [[Client1_19045]],  
                    Class = [[Position]],  
                    z = -22.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_19047]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2094.21875,  
                    x = 29517.8125,  
                    InstanceId = [[Client1_19048]],  
                    Class = [[Position]],  
                    z = -23.8125
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      ManualWeather = 0,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act I: Peace Mode]],  
      ActivitiesIds = {
        [[Client1_341]],  
        [[Client1_388]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_298]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      States = {
      }
    },  
    {
      Cost = 6,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act II: Bandits are Silly]],  
      ActivitiesIds = {
        [[Client1_207]],  
        [[Client1_213]],  
        [[Client1_424]],  
        [[Client1_472]],  
        [[Client1_18860]],  
        [[Client1_18862]],  
        [[Client1_18864]],  
        [[Client1_18866]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_411]],  
              ActivitiesId = {
                [[Client1_424]],  
                [[Client1_472]]
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              Notes = [[REPLACE WITH REAL GUARD]],  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_409]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[spnic]],  
                    InstanceId = [[Client1_424]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_425]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_414]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_426]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_466]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_447]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_471]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[sBKIL]],  
                    InstanceId = [[Client1_472]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_473]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_475]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_485]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[cbone]],  
                    InstanceId = [[Client1_427]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_428]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_429]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_430]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_432]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_433]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_434]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_435]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_436]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_437]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_439]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_440]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_443]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_441]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_442]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_444]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_467]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_468]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_469]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[Cbkil]],  
                    InstanceId = [[Client1_486]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_487]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_488]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_489]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_490]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_491]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_492]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 9,  
                        InstanceId = [[Client1_493]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_494]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_495]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[cWDON]],  
                    InstanceId = [[Client1_506]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_507]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_508]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_517]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[cwher]],  
                    InstanceId = [[Client1_510]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_511]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_512]],  
                            Who = [[Client1_411]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_520]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_521]],  
                            Who = [[Client1_211]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_522]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_498]],  
                    Name = [[]],  
                    InstanceId = [[Client1_18844]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_18843]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Angle = -2.09375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[Town Guard]],  
              Position = {
                y = -2088.59375,  
                x = 29518.8125,  
                InstanceId = [[Client1_412]],  
                Class = [[Position]],  
                z = -23.890625
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_174]],  
              ActivitiesId = {
                [[Client1_18866]]
              },  
              HairType = 6958,  
              TrouserColor = 2,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 5613870,  
              FeetColor = 5,  
              GabaritBreastSize = 7,  
              GabaritHeight = 2,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 8,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_172]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_18866]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_18867]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 7,  
              FeetModel = 5653550,  
              Angle = 1.96875,  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 5614638,  
              WeaponRightHand = 6935342,  
              ArmColor = 1,  
              Name = [[Follower of the Noob]],  
              Position = {
                y = -2236.625,  
                x = 29524.71875,  
                InstanceId = [[Client1_175]],  
                Class = [[Position]],  
                z = -22.875
              },  
              Sex = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 3,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_186]],  
              ActivitiesId = {
                [[Client1_18860]]
              },  
              HairType = 6702,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 9,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 4,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_184]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_18860]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_18861]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 5653550,  
              Angle = 1.25,  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5614638,  
              WeaponRightHand = 6935342,  
              ArmColor = 3,  
              Name = [[Noob Newbie]],  
              Position = {
                y = -2228.125,  
                x = 29514.07813,  
                InstanceId = [[Client1_187]],  
                Class = [[Position]],  
                z = -21.96875
              },  
              Sex = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 0,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_178]],  
              ActivitiesId = {
                [[Client1_18864]]
              },  
              HairType = 7214,  
              TrouserColor = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_176]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_18864]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_18865]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 5653550,  
              Angle = 1.765625,  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 6935342,  
              ArmColor = 5,  
              Name = [[Wannabe Noob]],  
              Position = {
                y = -2229.40625,  
                x = 29522.9375,  
                InstanceId = [[Client1_179]],  
                Class = [[Position]],  
                z = -22.921875
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 1,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_182]],  
              ActivitiesId = {
                [[Client1_18862]]
              },  
              HairType = 5623342,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 13,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 11,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_180]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_18862]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_18863]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5653550,  
              Angle = 1.765625,  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              JacketModel = 5614638,  
              WeaponRightHand = 6935342,  
              ArmColor = 3,  
              Name = [[Noob Worshipper]],  
              Position = {
                y = -2236.25,  
                x = 29512.10938,  
                InstanceId = [[Client1_183]],  
                Class = [[Position]],  
                z = -21.53125
              },  
              Sex = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 1
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_211]],  
              ActivitiesId = {
                [[Client1_213]]
              },  
              HairType = 5623598,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 7,  
              HandsModel = 6724398,  
              FeetColor = 2,  
              GabaritBreastSize = 5,  
              GabaritHeight = 10,  
              HairColor = 2,  
              EyesColor = 6,  
              TrouserModel = 6724910,  
              GabaritLegsWidth = 12,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_209]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_498]],  
                    Name = [[]],  
                    InstanceId = [[Client1_18850]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_18849]]
                  },  
                  {
                    LogicEntityAction = [[Client1_498]],  
                    Name = [[]],  
                    InstanceId = [[Client1_18853]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_18852]]
                  },  
                  {
                    LogicEntityAction = [[Client1_498]],  
                    Name = [[]],  
                    InstanceId = [[Client1_18856]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_18855]]
                  },  
                  {
                    LogicEntityAction = [[Client1_498]],  
                    Name = [[]],  
                    InstanceId = [[Client1_18859]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_18858]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[cspam]],  
                    InstanceId = [[Client1_18838]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 10,  
                        InstanceId = [[Client1_18839]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_18840]],  
                            Who = [[Client1_211]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_18841]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_498]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_497]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_18842]],  
                          Value = r2.RefId([[Client1_472]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_18843]],  
                        Entity = r2.RefId([[Client1_411]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Kill]],  
                          InstanceId = [[Client1_18848]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_18849]],  
                        Entity = r2.RefId([[Client1_182]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Kill]],  
                          InstanceId = [[Client1_18851]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_18852]],  
                        Entity = r2.RefId([[Client1_174]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Kill]],  
                          InstanceId = [[Client1_18854]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_18855]],  
                        Entity = r2.RefId([[Client1_178]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Kill]],  
                          InstanceId = [[Client1_18857]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_18858]],  
                        Entity = r2.RefId([[Client1_186]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_213]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_214]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_18838]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[Activity 1 : Wander Place 2 for 20sec]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 6724142,  
              Angle = 1.921875,  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_220]],  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_acid_f3.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 6725422,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 6,  
              JacketModel = 6725934,  
              WeaponRightHand = 6935854,  
              ArmColor = 5,  
              Name = [[NOOB THE GREAT]],  
              Position = {
                y = -2230.171875,  
                x = 29517.0625,  
                InstanceId = [[Client1_212]],  
                Class = [[Position]],  
                z = -22.25
              },  
              Sex = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 2,  
              Tattoo = 15
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_188]],  
      States = {
      }
    },  
    {
      Cost = 28,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act III: Kitins Invade]],  
      ActivitiesIds = {
        [[Client1_18868]],  
        [[Client1_18953]],  
        [[Client1_18955]],  
        [[Client1_18957]],  
        [[Client1_19049]],  
        [[Client1_19095]],  
        [[Client1_19097]],  
        [[Client1_19100]],  
        [[Client1_19103]],  
        [[Client1_19106]],  
        [[Client1_19107]],  
        [[Client1_19110]],  
        [[Client1_19154]],  
        [[Client1_19156]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_533]],  
              ActivitiesId = {
                [[Client1_19095]],  
                [[Client1_19097]],  
                [[Client1_19156]]
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_531]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_19095]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19096]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[sLead]],  
                    InstanceId = [[Client1_19097]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19098]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_414]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19099]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[sGuar]],  
                    InstanceId = [[Client1_19156]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19157]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_19091]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19094]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19093]]
                  },  
                  {
                    LogicEntityAction = [[Client1_19160]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19165]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19164]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              SheetClient = [[basic_fyros_female.creature]],  
              Angle = 1.125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              ArmModel = 6701614,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Town Guard]],  
              Position = {
                y = -2088.8125,  
                x = 29518.6875,  
                InstanceId = [[Client1_534]],  
                Class = [[Position]],  
                z = -23.875
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_18961]],  
              ActivitiesId = {
                [[Client1_19049]],  
                [[Client1_19154]]
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18959]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_19160]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19158]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19162]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[cGrdd]],  
                    InstanceId = [[Client1_19052]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_19053]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19054]],  
                            Who = [[Client1_18961]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19057]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_19055]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19056]],  
                            Who = [[Client1_533]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19058]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_19060]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19061]],  
                            Who = [[Client1_18961]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19062]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_19064]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19065]],  
                            Who = [[Client1_533]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19066]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[cLead]],  
                    InstanceId = [[Client1_19070]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_19071]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19072]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19077]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_19078]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19079]],  
                            Who = [[Client1_533]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19080]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_19082]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19083]],  
                            Who = [[Client1_18961]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19084]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_19086]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_19087]],  
                            Who = [[Client1_345]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19088]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_19091]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_19090]],  
                      Value = r2.RefId([[Client1_19051]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19092]],  
                          Value = r2.RefId([[Client1_19097]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19093]],  
                        Entity = r2.RefId([[Client1_533]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19112]],  
                          Value = r2.RefId([[Client1_19100]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19113]],  
                        Entity = r2.RefId([[Client1_18896]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_19160]],  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_19159]],  
                      Value = r2.RefId([[Client1_19086]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19161]],  
                          Value = r2.RefId([[Client1_19154]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19162]],  
                        Entity = r2.RefId([[Client1_18961]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19163]],  
                          Value = r2.RefId([[Client1_19156]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19164]],  
                        Entity = r2.RefId([[Client1_533]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_19049]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19050]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_19021]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[15]],  
                        InstanceId = [[Client1_19051]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_19052]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19068]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_414]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19069]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_19070]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[sGuar]],  
                    InstanceId = [[Client1_19154]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19155]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Angle = 1.109375,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Tower Guard]],  
              Position = {
                y = -2182.765625,  
                x = 29625.42188,  
                InstanceId = [[Client1_18962]],  
                Class = [[Position]],  
                z = -18.09375
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 12
            }
          },  
          InstanceId = [[Client1_189]]
        },  
        {
          InstanceId = [[Client1_18837]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group III: Medium]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_18836]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_18835]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_593]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_591]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_18868]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_18869]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[sInva]],  
                    InstanceId = [[Client1_19106]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19109]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_19128]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_19127]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19131]],  
                          Value = r2.RefId([[Client1_19100]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19132]],  
                        Entity = r2.RefId([[Client1_18896]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_19123]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19126]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19125]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]],  
              Position = {
                y = -2178.171875,  
                x = 29845.70313,  
                InstanceId = [[Client1_594]],  
                Class = [[Position]],  
                z = -20.78125
              },  
              Angle = 2.96875,  
              Base = [[palette.entities.creatures.ckdib4]],  
              ActivitiesId = {
                [[Client1_18868]],  
                [[Client1_19106]]
              }
            },  
            {
              InstanceId = [[Client1_597]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_595]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              Position = {
                y = -2173.109375,  
                x = 29858.32813,  
                InstanceId = [[Client1_598]],  
                Class = [[Position]],  
                z = -20.96875
              },  
              Angle = 2.96875,  
              Base = [[palette.entities.creatures.ckdib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18801]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_599]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              Position = {
                y = -2186.40625,  
                x = 29855.26563,  
                InstanceId = [[Client1_18802]],  
                Class = [[Position]],  
                z = -20.9375
              },  
              Angle = 2.796875,  
              Base = [[palette.entities.creatures.ckdib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18805]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18803]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2165.46875,  
                x = 29873.34375,  
                InstanceId = [[Client1_18806]],  
                Class = [[Position]],  
                z = -19.46875
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18809]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18807]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2181.015625,  
                x = 29868.03125,  
                InstanceId = [[Client1_18810]],  
                Class = [[Position]],  
                z = -19.578125
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18813]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18811]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2195.0625,  
                x = 29864.53125,  
                InstanceId = [[Client1_18814]],  
                Class = [[Position]],  
                z = -19.625
              },  
              Angle = 2.4375,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18817]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18815]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              Position = {
                y = -2157.671875,  
                x = 29879.6875,  
                InstanceId = [[Client1_18818]],  
                Class = [[Position]],  
                z = -17.34375
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.creatures.ckdib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18821]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18819]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              Position = {
                y = -2173.375,  
                x = 29876.5625,  
                InstanceId = [[Client1_18822]],  
                Class = [[Position]],  
                z = -18.125
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.creatures.ckdib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18825]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18823]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              Position = {
                y = -2191.96875,  
                x = 29871.40625,  
                InstanceId = [[Client1_18826]],  
                Class = [[Position]],  
                z = -17.71875
              },  
              Angle = 2.65625,  
              Base = [[palette.entities.creatures.ckdib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18829]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18827]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              Position = {
                y = -2203.46875,  
                x = 29866.625,  
                InstanceId = [[Client1_18830]],  
                Class = [[Position]],  
                z = -17.765625
              },  
              Angle = 2.25,  
              Base = [[palette.entities.creatures.ckdib1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_18896]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group I: Small]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_18895]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_18894]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_18872]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18870]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_19091]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19114]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19113]]
                  },  
                  {
                    LogicEntityAction = [[Client1_19128]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19133]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19132]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_19118]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_19117]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19119]],  
                          Value = r2.RefId([[Client1_19103]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19120]],  
                        Entity = r2.RefId([[Client1_18923]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_18957]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_18958]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[sInva]],  
                    InstanceId = [[Client1_19100]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19102]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]],  
              Position = {
                y = -2180.578125,  
                x = 29787.14063,  
                InstanceId = [[Client1_18873]],  
                Class = [[Position]],  
                z = -17.078125
              },  
              Angle = -3.0625,  
              Base = [[palette.entities.creatures.ckdib4]],  
              ActivitiesId = {
                [[Client1_18957]],  
                [[Client1_19100]]
              }
            },  
            {
              InstanceId = [[Client1_18876]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18874]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              Position = {
                y = -2174.890625,  
                x = 29791.90625,  
                InstanceId = [[Client1_18877]],  
                Class = [[Position]],  
                z = -15.71875
              },  
              Angle = -3.0625,  
              Base = [[palette.entities.creatures.ckdib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18880]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18878]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              Position = {
                y = -2184.328125,  
                x = 29792.1875,  
                InstanceId = [[Client1_18881]],  
                Class = [[Position]],  
                z = -15.484375
              },  
              Angle = -3.0625,  
              Base = [[palette.entities.creatures.ckdib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18892]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18890]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2187.59375,  
                x = 29799.14063,  
                InstanceId = [[Client1_18893]],  
                Class = [[Position]],  
                z = -14.5
              },  
              Angle = -3.125,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18888]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18886]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2180.359375,  
                x = 29798.28125,  
                InstanceId = [[Client1_18889]],  
                Class = [[Position]],  
                z = -13.703125
              },  
              Angle = -3.015625,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18884]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18882]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2173.765625,  
                x = 29798.21875,  
                InstanceId = [[Client1_18885]],  
                Class = [[Position]],  
                z = -13.296875
              },  
              Angle = -3.015625,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_18923]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group II: Small]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_18922]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_18921]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_18899]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18897]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_19118]],  
                    Name = [[]],  
                    InstanceId = [[Client1_19121]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_19120]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_19123]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_19122]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_19124]],  
                          Value = r2.RefId([[Client1_19106]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_19125]],  
                        Entity = r2.RefId([[Client1_18837]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_18955]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_18956]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[sInva]],  
                    InstanceId = [[Client1_19103]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19105]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]],  
              Position = {
                y = -2156.3125,  
                x = 29800,  
                InstanceId = [[Client1_18900]],  
                Class = [[Position]],  
                z = -19.1875
              },  
              Angle = 3.078125,  
              Base = [[palette.entities.creatures.ckdib4]],  
              ActivitiesId = {
                [[Client1_18955]],  
                [[Client1_19103]]
              }
            },  
            {
              InstanceId = [[Client1_18907]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18905]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              Position = {
                y = -2159.5625,  
                x = 29804.15625,  
                InstanceId = [[Client1_18908]],  
                Class = [[Position]],  
                z = -19.03125
              },  
              Angle = 3.078125,  
              Base = [[palette.entities.creatures.ckdib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18919]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18917]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2163.828125,  
                x = 29805.57813,  
                InstanceId = [[Client1_18920]],  
                Class = [[Position]],  
                z = -17.546875
              },  
              Angle = 3.078125,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18915]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18913]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2155.890625,  
                x = 29807.92188,  
                InstanceId = [[Client1_18916]],  
                Class = [[Position]],  
                z = -19.84375
              },  
              Angle = 3.078125,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18903]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18901]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              Position = {
                y = -2152.25,  
                x = 29805.59375,  
                InstanceId = [[Client1_18904]],  
                Class = [[Position]],  
                z = -19.921875
              },  
              Angle = 3.078125,  
              Base = [[palette.entities.creatures.ckdib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18911]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18909]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                y = -2147.53125,  
                x = 29810.73438,  
                InstanceId = [[Client1_18912]],  
                Class = [[Position]],  
                z = -20.765625
              },  
              Angle = 3.078125,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_18938]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group IV: Boss]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_18937]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_18936]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_561]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_559]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_18953]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_18954]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_18940]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[sInva]],  
                    InstanceId = [[Client1_19110]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_19111]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kinchey]],  
              Position = {
                y = -2159.015625,  
                x = 29921.25,  
                InstanceId = [[Client1_562]],  
                Class = [[Position]],  
                z = -20.953125
              },  
              Angle = -3.0625,  
              Base = [[palette.entities.creatures.ckdpf7]],  
              ActivitiesId = {
                [[Client1_18953]],  
                [[Client1_19110]]
              }
            },  
            {
              InstanceId = [[Client1_18934]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18932]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lethal Kincher]],  
              Position = {
                y = -2167.71875,  
                x = 29918.39063,  
                InstanceId = [[Client1_18935]],  
                Class = [[Position]],  
                z = -19.796875
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckdgf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18930]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18928]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lethal Kincher]],  
              Position = {
                y = -2159.265625,  
                x = 29908.84375,  
                InstanceId = [[Client1_18931]],  
                Class = [[Position]],  
                z = -17.015625
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckdgf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18926]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_18924]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lethal Kincher]],  
              Position = {
                y = -2149.765625,  
                x = 29919.64063,  
                InstanceId = [[Client1_18927]],  
                Class = [[Position]],  
                z = -19.53125
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.creatures.ckdgf4]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_297]],  
      ManualWeather = 0,  
      States = {
      }
    },  
    {
      Cost = 4,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Title = [[Act IV: Nothing]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_19136]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_19134]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kincher]],  
              Position = {
                y = -2186.6875,  
                x = 29580.75,  
                InstanceId = [[Client1_19137]],  
                Class = [[Position]],  
                z = -17.1875
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.creatures.ckdif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_19140]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_19138]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]],  
              Position = {
                y = -2171.984375,  
                x = 29584.92188,  
                InstanceId = [[Client1_19141]],  
                Class = [[Position]],  
                z = -17.09375
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.creatures.ckdif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_19148]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_19146]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kincher]],  
              Position = {
                y = -2162.578125,  
                x = 29587.96875,  
                InstanceId = [[Client1_19149]],  
                Class = [[Position]],  
                z = -19.015625
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.creatures.ckdif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_19152]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_19150]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kincher]],  
              Position = {
                y = -2168.953125,  
                x = 29591.14063,  
                InstanceId = [[Client1_19153]],  
                Class = [[Position]],  
                z = -19.40625
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.creatures.ckdif1]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_18964]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_18963]],  
      ManualWeather = 0,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Title = [[Act V: Unknown]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_18966]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_18965]],  
      ManualWeather = 0,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 26,  
        InstanceId = [[Client1_221]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_222]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_223]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_224]],  
        Class = [[TextManagerEntry]],  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_225]],  
        Class = [[TextManagerEntry]],  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_407]],  
        Class = [[TextManagerEntry]],  
        Text = [[Good morning to everyone! What a great day today!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_408]],  
        Class = [[TextManagerEntry]],  
        Text = [[Good morning to everyone! What a great day today!]]
      },  
      {
        Count = 11,  
        InstanceId = [[Client1_430]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bad news!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_431]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bad news!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_434]],  
        Class = [[TextManagerEntry]],  
        Text = [[What's wrong? Did something bad happen?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_437]],  
        Class = [[TextManagerEntry]],  
        Text = [[There's a group of bandit's that just arrived near our camp, they will probly try to take over this place!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_438]],  
        Class = [[TextManagerEntry]],  
        Text = [[There's a group of bandit's that just arrived near our camp, they will probly try to take over this place!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_443]],  
        Class = [[TextManagerEntry]],  
        Text = [[We must do something immediately!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_444]],  
        Class = [[TextManagerEntry]],  
        Text = [[Everyone! Please, go find this group of bandits, and kill them!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_445]],  
        Class = [[TextManagerEntry]],  
        Text = [[Everyone! Please, go find this group of bandits, and kill them!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_469]],  
        Class = [[TextManagerEntry]],  
        Text = [[Follow me!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_470]],  
        Class = [[TextManagerEntry]],  
        Text = [[Follow me!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_489]],  
        Class = [[TextManagerEntry]],  
        Text = [[The bandits are gone!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_492]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you all for helping us out with this! You were of great help for us!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_495]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will see you all later. Goodbye.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_509]],  
        Class = [[TextManagerEntry]],  
        Text = [[Well Done!]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_513]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is the bandit's camp.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_514]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is the bandit's camp.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_517]],  
        Class = [[TextManagerEntry]],  
        Text = [[Well Done! You killed their leader!]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_522]],  
        Class = [[TextManagerEntry]],  
        Text = [[All you will die right here and now! GET THEM!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_18841]],  
        Class = [[TextManagerEntry]],  
        Text = [[Muahahahahahaaaa!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19057]],  
        Class = [[TextManagerEntry]],  
        Text = [[Anyone here? There might be a problem!]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_19058]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, I'm coming! What's wrong?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19059]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, I'm coming! What's wrong?]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_19062]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitins! They're acting strange again!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19063]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitins! They're acting strange again!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_19066]],  
        Class = [[TextManagerEntry]],  
        Text = [[We should go see the Town Leader.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19067]],  
        Class = [[TextManagerEntry]],  
        Text = [[We should go see the Town Leader.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19073]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19074]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19075]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_19076]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hello there, did anything happen?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19077]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hello there, did anything happen?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_19080]],  
        Class = [[TextManagerEntry]],  
        Text = [[We're here to report some very bad news.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19081]],  
        Class = [[TextManagerEntry]],  
        Text = [[We're here to report some very bad news.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_19084]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitin are showing strange behavior again, they might be here soon!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19085]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitin are showing strange behavior again, they might be here soon!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_19088]],  
        Class = [[TextManagerEntry]],  
        Text = [[That's terrible! Please, everyone, be very careful! If they come here, they have to be killed immediately!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_19089]],  
        Class = [[TextManagerEntry]],  
        Text = [[That's terrible! Please, everyone, be very careful! If they come here, they have to be killed immediately!]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}