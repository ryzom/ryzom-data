scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    Npc = 0,  
    ActionStep = 0,  
    Region = 0,  
    ActivityStep = 0,  
    LogicEntityReaction = 0,  
    Road = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    EventType = 0,  
    Position = 0,  
    Behavior = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    ChatAction = 0
  },  
  Acts = {
    {
      Cost = 41,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client1_184]],  
        [[Client1_186]],  
        [[Client1_188]],  
        [[Client2_92]],  
        [[Client2_94]],  
        [[Client2_96]],  
        [[Client1_230]]
      },  
      Title = [[Act 0]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_14]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI camp fire 1]],  
              Position = {
                y = -1976.75,  
                x = 22497.82813,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 38.0625
              },  
              Angle = 1.6875,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI hut 1]],  
              Position = {
                y = -1980.171875,  
                x = 22484.51563,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 38.25
              },  
              Angle = -0.203125,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_352]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_353]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_354]],  
                            Who = [[Client1_42]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_50]]),  
                            Says = [[Client1_357]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6702382,  
              Angle = -1.390625,  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[Fihl]],  
              Position = {
                y = -1975.75,  
                x = 22497.125,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = 38.140625
              },  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 5606190,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_46]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 5,  
              HandsModel = 6702894,  
              FeetColor = 5,  
              GabaritBreastSize = 8,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 4,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_346]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_347]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_348]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_50]]),  
                            Says = [[Client1_349]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[Chat2]],  
                    InstanceId = [[Client1_351]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_363]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_364]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_280]]),  
                            Says = [[Client1_365]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5605422,  
              Angle = 0.46875,  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[Dae]],  
              Position = {
                y = -1977.859375,  
                x = 22496.76563,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 38.125
              },  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 5606190,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_50]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 3,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 2,  
              GabaritHeight = 2,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 3,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_337]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_340]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_341]],  
                            Who = [[Client1_50]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_344]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 6,  
                        InstanceId = [[Client1_342]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_343]],  
                            Who = [[Client1_50]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_345]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_360]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[]],  
                      InstanceId = [[Client1_359]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Angle = -0.234375,  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Tattoo = 12,  
              MorphTarget3 = 1,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              JacketModel = 6704430,  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Dahv]],  
              Position = {
                y = -1976.578125,  
                x = 22496.46875,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 38.203125
              },  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6703918,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_58]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6702382,  
              Angle = -0.265625,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              JacketModel = 6706990,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Dim]],  
              Position = {
                y = -1980.8125,  
                x = 22487.48438,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = 38.046875
              },  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6706734,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_101]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 12,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_99]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Angle = 0.1875,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 21,  
              MorphTarget3 = 0,  
              MorphTarget7 = 2,  
              Position = {
                y = -2010.53125,  
                x = 22551.15625,  
                InstanceId = [[Client1_102]],  
                Class = [[Position]],  
                z = 31.046875
              },  
              Sex = 1,  
              WeaponRightHand = 6755886,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[Gorge Guard]],  
              BotAttackable = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701358,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_105]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 12,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 1,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_103]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 6699310,  
              Angle = 0.296875,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 16,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              Position = {
                y = -1985.8125,  
                x = 22551.82813,  
                InstanceId = [[Client1_106]],  
                Class = [[Position]],  
                z = 31.71875
              },  
              Sex = 1,  
              WeaponRightHand = 6755374,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Gorge Guard]],  
              BotAttackable = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_e4.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client2_11]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_9]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                y = -1987.0625,  
                x = 22594.75,  
                InstanceId = [[Client2_12]],  
                Class = [[Position]],  
                z = 25.96875
              },  
              Angle = 2.265625,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_13]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scowling Gingo]],  
              Position = {
                y = -1971.078125,  
                x = 22612.1875,  
                InstanceId = [[Client2_16]],  
                Class = [[Position]],  
                z = 22.875
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.creatures.ccadb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client4_3]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client4_1]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[The Tunnel]],  
              Position = {
                y = -1855.953125,  
                x = 22648.84375,  
                InstanceId = [[Client4_4]],  
                Class = [[Position]],  
                z = 16.34375
              },  
              Angle = -1.390625,  
              Base = [[palette.entities.botobjects.fx_tr_pollen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_121]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_119]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI bag 2 1]],  
              Position = {
                y = -1975.828125,  
                x = 22487.625,  
                InstanceId = [[Client1_122]],  
                Class = [[Position]],  
                z = 38.265625
              },  
              Angle = 2.25,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_129]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_127]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1990.265625,  
                x = 22494.79688,  
                InstanceId = [[Client1_130]],  
                Class = [[Position]],  
                z = 37.421875
              },  
              Angle = 0.234375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_133]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_131]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1999.375,  
                x = 22498.125,  
                InstanceId = [[Client1_134]],  
                Class = [[Position]],  
                z = 35.84375
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_7]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_5]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1973.296875,  
                x = 22482.14063,  
                InstanceId = [[Client3_8]],  
                Class = [[Position]],  
                z = 39.125
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_39]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_37]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Building]],  
              Position = {
                y = -1876,  
                x = 22275.60938,  
                InstanceId = [[Client2_40]],  
                Class = [[Position]],  
                z = -22.78125
              },  
              Angle = 0.640625,  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_51]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_49]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Tower This Way]],  
              Position = {
                y = -1806.265625,  
                x = 22450.40625,  
                InstanceId = [[Client2_52]],  
                Class = [[Position]],  
                z = 0.625
              },  
              Angle = 0.09375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client2_59]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_57]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Angle = -0.1875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Kaeno Tower Guard]],  
              Position = {
                y = -1808.625,  
                x = 22460.625,  
                InstanceId = [[Client2_60]],  
                Class = [[Position]],  
                z = 1.09375
              },  
              Sheet = [[ring_guard_melee_tank_slash_f1.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client1_141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_139]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI mekbar 1 - ROOMKILLER]],  
              Position = {
                y = -1957.5,  
                x = 22496.65625,  
                InstanceId = [[Client1_142]],  
                Class = [[Position]],  
                z = 36.890625
              },  
              Angle = 0.203125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_143]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI mekbar 2 - USES SPACE]],  
              Position = {
                y = -1961.28125,  
                x = 22497.625,  
                InstanceId = [[Client1_146]],  
                Class = [[Position]],  
                z = 37.09375
              },  
              Angle = -2.859375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_63]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_61]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Building Square]],  
              Position = {
                y = -1804.921875,  
                x = 22355.82813,  
                InstanceId = [[Client2_64]],  
                Class = [[Position]],  
                z = -11.828125
              },  
              Angle = -1.359375,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_147]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1961.09375,  
                x = 22495.9375,  
                InstanceId = [[Client1_150]],  
                Class = [[Position]],  
                z = 37.078125
              },  
              Angle = -2.640625,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1958.3125,  
                x = 22494.59375,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = 36.765625
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Region]],  
              InstanceId = [[Client2_66]],  
              Class = [[Region]],  
              Position = {
                y = 0.3125,  
                x = -0.96875,  
                InstanceId = [[Client2_65]],  
                Class = [[Position]],  
                z = -0.15625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_68]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1809.359375,  
                    x = 22354.60938,  
                    InstanceId = [[Client2_69]],  
                    Class = [[Position]],  
                    z = -12.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_71]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1838.515625,  
                    x = 22404.71875,  
                    InstanceId = [[Client2_72]],  
                    Class = [[Position]],  
                    z = -9.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_74]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1861.5,  
                    x = 22308.78125,  
                    InstanceId = [[Client2_75]],  
                    Class = [[Position]],  
                    z = -16.09375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_155]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_186]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_187]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1962.3125,  
                x = 22527.07813,  
                InstanceId = [[Client1_158]],  
                Class = [[Position]],  
                z = 32.09375
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_186]]
              }
            },  
            {
              InstanceId = [[Client1_161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_159]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_188]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_189]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1960.59375,  
                x = 22530.4375,  
                InstanceId = [[Client1_162]],  
                Class = [[Position]],  
                z = 31.984375
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_188]]
              }
            },  
            {
              InstanceId = [[Client1_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_163]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_184]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_185]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1958.96875,  
                x = 22529.23438,  
                InstanceId = [[Client1_166]],  
                Class = [[Position]],  
                z = 32.046875
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_184]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Raspal Region]],  
              InstanceId = [[Client1_168]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_167]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1953.609375,  
                    x = 22517.57813,  
                    InstanceId = [[Client1_171]],  
                    Class = [[Position]],  
                    z = 32.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_173]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1969.796875,  
                    x = 22523.5,  
                    InstanceId = [[Client1_174]],  
                    Class = [[Position]],  
                    z = 32.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_176]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1962.265625,  
                    x = 22541.09375,  
                    InstanceId = [[Client1_177]],  
                    Class = [[Position]],  
                    z = 31.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_179]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1952.75,  
                    x = 22536.78125,  
                    InstanceId = [[Client1_180]],  
                    Class = [[Position]],  
                    z = 31.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_182]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1952.640625,  
                    x = 22526.15625,  
                    InstanceId = [[Client1_183]],  
                    Class = [[Position]],  
                    z = 32.34375
                  }
                }
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client2_82]],  
              ActivitiesId = {
                [[Client2_92]]
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 7,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_80]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_92]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_93]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 10,  
              MorphTarget3 = 6,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Kaethus]],  
              Position = {
                y = -1824.65625,  
                x = 22365.84375,  
                InstanceId = [[Client2_83]],  
                Class = [[Position]],  
                z = -7.3125
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client2_86]],  
              ActivitiesId = {
                [[Client2_96]]
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 10,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 5,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_84]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_96]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_374]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 5605422,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 25,  
              MorphTarget3 = 3,  
              MorphTarget7 = 1,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[Kaethon]],  
              Position = {
                y = -1829.953125,  
                x = 22369.46875,  
                InstanceId = [[Client2_87]],  
                Class = [[Position]],  
                z = -6.859375
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client2_90]],  
              ActivitiesId = {
                [[Client2_94]]
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 8,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 2,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_88]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_94]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_375]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 5605422,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 13,  
              MorphTarget3 = 0,  
              MorphTarget7 = 6,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Kaethi]],  
              Position = {
                y = -1827.15625,  
                x = 22362.5,  
                InstanceId = [[Client2_91]],  
                Class = [[Position]],  
                z = -7.34375
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client2_108]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              Notes = [[This character takes care of the resources that the destroyed village Kaeno needs.]],  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_106]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Angle = -0.28125,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Kaeno Resource Manager]],  
              Position = {
                y = -1835.3125,  
                x = 22321.07813,  
                InstanceId = [[Client2_109]],  
                Class = [[Position]],  
                z = -12.015625
              },  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client2_112]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_110]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Resource Building]],  
              Position = {
                y = -1835.546875,  
                x = 22318.67188,  
                InstanceId = [[Client2_113]],  
                Class = [[Position]],  
                z = -12.390625
              },  
              Angle = -0.21875,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_116]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_114]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[-KAE Blocks so you]],  
              Position = {
                y = -1783.796875,  
                x = 22656.04688,  
                InstanceId = [[Client2_117]],  
                Class = [[Position]],  
                z = 13.21875
              },  
              Angle = -2.6875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_120]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_118]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[-KAE To Other Side]],  
              Position = {
                y = -1793.828125,  
                x = 22656.0625,  
                InstanceId = [[Client2_121]],  
                Class = [[Position]],  
                z = 13.5625
              },  
              Angle = 0.390625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_132]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_130]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[-KAE Dont Misrun]],  
              Position = {
                y = -1892.453125,  
                x = 22679.875,  
                InstanceId = [[Client2_133]],  
                Class = [[Position]],  
                z = 16.4375
              },  
              Angle = -2.6875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_144]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_142]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Totem]],  
              Position = {
                y = -1834.109375,  
                x = 22537.17188,  
                InstanceId = [[Client2_145]],  
                Class = [[Position]],  
                z = 4.96875
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.totem_bird]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Yubo Region]],  
              InstanceId = [[Client1_214]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_216]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2005.296875,  
                    x = 22514.92188,  
                    InstanceId = [[Client1_217]],  
                    Class = [[Position]],  
                    z = 31.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_219]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1986.8125,  
                    x = 22506.90625,  
                    InstanceId = [[Client1_220]],  
                    Class = [[Position]],  
                    z = 36.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_222]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1985.78125,  
                    x = 22533.75,  
                    InstanceId = [[Client1_223]],  
                    Class = [[Position]],  
                    z = 32.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_225]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2011.84375,  
                    x = 22534.125,  
                    InstanceId = [[Client1_226]],  
                    Class = [[Position]],  
                    z = 32.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_228]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2020.296875,  
                    x = 22520.67188,  
                    InstanceId = [[Client1_229]],  
                    Class = [[Position]],  
                    z = 32.84375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_213]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Goari near Kaeno]],  
              InstanceId = [[Client2_159]],  
              Class = [[Region]],  
              Position = {
                y = 0.265625,  
                x = -0.953125,  
                InstanceId = [[Client2_158]],  
                Class = [[Position]],  
                z = 0.109375
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1795.9375,  
                    x = 22481.625,  
                    InstanceId = [[Client2_162]],  
                    Class = [[Position]],  
                    z = 1.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_164]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1829.796875,  
                    x = 22465.23438,  
                    InstanceId = [[Client2_165]],  
                    Class = [[Position]],  
                    z = 1.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_167]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1821.1875,  
                    x = 22488.96875,  
                    InstanceId = [[Client2_168]],  
                    Class = [[Position]],  
                    z = 3.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1798.59375,  
                    x = 22496.875,  
                    InstanceId = [[Client2_171]],  
                    Class = [[Position]],  
                    z = 1.328125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client2_184]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_182]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              Position = {
                y = -1814.75,  
                x = 22463.85938,  
                InstanceId = [[Client2_185]],  
                Class = [[Position]],  
                z = 1.78125
              },  
              Angle = 1.5625,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime to Kaeno]],  
              InstanceId = [[Client1_283]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_285]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1987.40625,  
                    x = 22501,  
                    InstanceId = [[Client1_286]],  
                    Class = [[Position]],  
                    z = 37.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_288]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1993.265625,  
                    x = 22577.40625,  
                    InstanceId = [[Client1_289]],  
                    Class = [[Position]],  
                    z = 27.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_291]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1960.734375,  
                    x = 22621.92188,  
                    InstanceId = [[Client1_292]],  
                    Class = [[Position]],  
                    z = 23.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_294]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1854.234375,  
                    x = 22653.51563,  
                    InstanceId = [[Client1_295]],  
                    Class = [[Position]],  
                    z = 15.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_297]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1813.578125,  
                    x = 22611.90625,  
                    InstanceId = [[Client1_298]],  
                    Class = [[Position]],  
                    z = 9.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_300]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1838.359375,  
                    x = 22552.375,  
                    InstanceId = [[Client1_301]],  
                    Class = [[Position]],  
                    z = 4.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_303]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1801.96875,  
                    x = 22521.51563,  
                    InstanceId = [[Client1_304]],  
                    Class = [[Position]],  
                    z = 4.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_306]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1805.78125,  
                    x = 22486.5625,  
                    InstanceId = [[Client1_307]],  
                    Class = [[Position]],  
                    z = 1.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_309]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1874.234375,  
                    x = 22486.40625,  
                    InstanceId = [[Client1_310]],  
                    Class = [[Position]],  
                    z = 0.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_312]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1874.03125,  
                    x = 22432.10938,  
                    InstanceId = [[Client1_313]],  
                    Class = [[Position]],  
                    z = -3.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_315]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1831.140625,  
                    x = 22378.46875,  
                    InstanceId = [[Client1_316]],  
                    Class = [[Position]],  
                    z = -7.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_318]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1835.859375,  
                    x = 22325.10938,  
                    InstanceId = [[Client1_319]],  
                    Class = [[Position]],  
                    z = -11.84375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_282]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Tunnel Region]],  
              InstanceId = [[Client2_225]],  
              Class = [[Region]],  
              Position = {
                y = -1.5625,  
                x = -2,  
                InstanceId = [[Client2_224]],  
                Class = [[Position]],  
                z = -0.59375
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1878.390625,  
                    x = 22645.875,  
                    InstanceId = [[Client2_228]],  
                    Class = [[Position]],  
                    z = 19.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1875.3125,  
                    x = 22656.0625,  
                    InstanceId = [[Client2_231]],  
                    Class = [[Position]],  
                    z = 19.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1843.90625,  
                    x = 22674.07813,  
                    InstanceId = [[Client2_234]],  
                    Class = [[Position]],  
                    z = 14.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1821,  
                    x = 22634.26563,  
                    InstanceId = [[Client2_237]],  
                    Class = [[Position]],  
                    z = 14.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_239]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1826.15625,  
                    x = 22628.46875,  
                    InstanceId = [[Client2_240]],  
                    Class = [[Position]],  
                    z = 13.109375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client2_335]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_333]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Message Tower]],  
              Position = {
                y = -1870.875,  
                x = 22535.5625,  
                InstanceId = [[Client2_336]],  
                Class = [[Position]],  
                z = 7.78125
              },  
              Angle = 1.296875,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Messenger To Guard]],  
              InstanceId = [[Client2_346]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_345]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_348]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1879.59375,  
                    x = 22541.5,  
                    InstanceId = [[Client2_349]],  
                    Class = [[Position]],  
                    z = 9.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_351]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1866,  
                    x = 22545,  
                    InstanceId = [[Client2_352]],  
                    Class = [[Position]],  
                    z = 8.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_354]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1832.515625,  
                    x = 22542,  
                    InstanceId = [[Client2_355]],  
                    Class = [[Position]],  
                    z = 4.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_357]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1804.71875,  
                    x = 22521.5,  
                    InstanceId = [[Client2_358]],  
                    Class = [[Position]],  
                    z = 5.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_360]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1808.890625,  
                    x = 22462.625,  
                    InstanceId = [[Client2_361]],  
                    Class = [[Position]],  
                    z = 0.71875
                  }
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client2_418]],  
              Name = [[Kaeno Tower To Manager]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_420]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_421]],  
                    x = 22481.09375,  
                    y = -1851.78125,  
                    z = 1.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_423]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_424]],  
                    x = 22475.53125,  
                    y = -1873.859375,  
                    z = 0.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_426]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_427]],  
                    x = 22445.09375,  
                    y = -1873.609375,  
                    z = 0
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_429]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_430]],  
                    x = 22401.9375,  
                    y = -1854.796875,  
                    z = -6.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_432]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_433]],  
                    x = 22325.14063,  
                    y = -1836.203125,  
                    z = -11.90625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_417]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_5]]
        },  
        {
          InstanceId = [[Client1_212]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_211]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_196]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_194]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Sprightly Yubo]],  
              Position = {
                y = -2004.21875,  
                x = 22522.73438,  
                InstanceId = [[Client1_197]],  
                Class = [[Position]],  
                z = 33.09375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb4]],  
              ActivitiesId = {
                [[Client1_230]]
              }
            },  
            {
              InstanceId = [[Client1_192]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_190]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_230]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_231]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Timorous Yubo]],  
              Position = {
                y = -2003.796875,  
                x = 22521.53125,  
                InstanceId = [[Client1_193]],  
                Class = [[Position]],  
                z = 33.03125
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_204]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_202]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              Position = {
                y = -2005.390625,  
                x = 22521.90625,  
                InstanceId = [[Client1_205]],  
                Class = [[Position]],  
                z = 32.984375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_200]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_198]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              Position = {
                y = -2002.890625,  
                x = 22521.9375,  
                InstanceId = [[Client1_201]],  
                Class = [[Position]],  
                z = 33.109375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_210]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_4]],  
      Events = {
      },  
      ManualWeather = 0
    },  
    {
      Cost = 7,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client2_172]],  
        [[Client2_178]],  
        [[Client2_180]],  
        [[Client2_190]],  
        [[Client2_196]],  
        [[Client1_272]],  
        [[Client1_274]]
      },  
      Title = [[Act I: Before Things]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client2_152]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_150]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_180]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_181]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Bloated Izam]],  
              Position = {
                y = -1813.09375,  
                x = 22476.9375,  
                InstanceId = [[Client2_153]],  
                Class = [[Position]],  
                z = 2.09375
              },  
              Angle = 1.9375,  
              Base = [[palette.entities.creatures.cbbdb3]],  
              ActivitiesId = {
                [[Client2_180]]
              }
            },  
            {
              InstanceId = [[Client2_176]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_174]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_178]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_179]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gorged Izam]],  
              Position = {
                y = -1819.65625,  
                x = 22475.53125,  
                InstanceId = [[Client2_177]],  
                Class = [[Position]],  
                z = 2.0625
              },  
              Angle = -1.25,  
              Base = [[palette.entities.creatures.cbbdb1]],  
              ActivitiesId = {
                [[Client2_178]]
              }
            },  
            {
              InstanceId = [[Client2_200]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_198]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Sluggish Shooki]],  
              Position = {
                y = -1835.953125,  
                x = 22494.79688,  
                InstanceId = [[Client2_201]],  
                Class = [[Position]],  
                z = 1.234375
              },  
              Angle = 2.515625,  
              Base = [[palette.entities.creatures.cpfdb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_266]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_264]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_274]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_275]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Goari]],  
              Position = {
                y = -1815,  
                x = 22486.375,  
                InstanceId = [[Client1_267]],  
                Class = [[Position]],  
                z = 2.25
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.creatures.cccdb4]],  
              ActivitiesId = {
                [[Client1_274]]
              }
            },  
            {
              InstanceId = [[Client1_270]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_268]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_272]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_273]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Goari]],  
              Position = {
                y = -1810.203125,  
                x = 22487.20313,  
                InstanceId = [[Client1_271]],  
                Class = [[Position]],  
                z = 1.640625
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.creatures.cccdb4]],  
              ActivitiesId = {
                [[Client1_272]]
              }
            },  
            {
              InstanceId = [[Client2_284]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_282]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Drowsy Shooki]],  
              Position = {
                y = -1806.671875,  
                x = 22636.57813,  
                InstanceId = [[Client2_285]],  
                Class = [[Position]],  
                z = 12.859375
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.creatures.cpfdb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_288]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_286]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Devastating Shooki]],  
              Position = {
                y = -1809.046875,  
                x = 22633.54688,  
                InstanceId = [[Client2_289]],  
                Class = [[Position]],  
                z = 12.015625
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.creatures.cpfdf4]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_6]],  
      Events = {
      },  
      ManualWeather = 0
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      InstanceId = [[Client1_276]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
        [[Client1_320]]
      },  
      Title = [[Act II: The Traveling Helpers]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_280]],  
              ActivitiesId = {
                [[Client1_320]]
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 5,  
              GabaritHeight = 13,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_278]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[sInit]],  
                    InstanceId = [[Client1_320]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_321]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_366]]),  
                        ActivityZoneId = r2.RefId([[Client1_283]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_322]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_323]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[ATKAE]],  
                    InstanceId = [[Client1_323]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_324]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_325]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_371]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 5,  
                        InstanceId = [[Client1_326]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Puzzled]],  
                            InstanceId = [[Client1_327]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_372]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_362]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_363]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_364]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[athim]],  
                    InstanceId = [[Client2_366]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_367]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_368]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_369]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 6702382,  
              Angle = 1.5,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 0,  
              MorphTarget3 = 0,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              JacketModel = 6704430,  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[Daghn]],  
              Position = {
                y = -1987.640625,  
                x = 22499.54688,  
                InstanceId = [[Client1_281]],  
                Class = [[Position]],  
                z = 37.28125
              },  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6706478,  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          InstanceId = [[Client1_277]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    },  
    {
      Cost = 9,  
      Class = [[Act]],  
      InstanceId = [[Client2_202]],  
      WeatherValue = 0,  
      Title = [[Act III: Kitins Arrive]],  
      ActivitiesIds = {
        [[Client2_248]],  
        [[Client2_274]],  
        [[Client2_276]],  
        [[Client2_278]],  
        [[Client2_279]],  
        [[Client2_376]],  
        [[Client2_403]],  
        [[Client2_405]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client2_268]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_266]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_410]],  
                    Name = [[]],  
                    InstanceId = [[Client2_416]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_415]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_325]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_326]],  
                          Value = r2.RefId([[Client2_278]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_327]],  
                        Entity = r2.RefId([[Client2_206]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client2_324]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_276]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_277]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_225]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[invas]],  
                    InstanceId = [[Client2_403]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_404]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_214]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Hime Assault Kirosta]],  
              Position = {
                y = -1877.40625,  
                x = 22645.34375,  
                InstanceId = [[Client2_269]],  
                Class = [[Position]],  
                z = 18.875
              },  
              Angle = -1.890625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client2_276]],  
                [[Client2_403]]
              }
            },  
            {
              InstanceId = [[Client2_272]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_270]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_410]],  
                    Name = [[]],  
                    InstanceId = [[Client2_413]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_412]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_330]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_331]],  
                          Value = r2.RefId([[Client2_279]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_332]],  
                        Entity = r2.RefId([[Client2_206]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client2_329]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_274]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_275]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_225]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[invas]],  
                    InstanceId = [[Client2_405]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_406]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kaeno Assault Kirosta]],  
              Position = {
                y = -1826.625,  
                x = 22631,  
                InstanceId = [[Client2_273]],  
                Class = [[Position]],  
                z = 12.453125
              },  
              Angle = -1.484375,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client2_274]],  
                [[Client2_405]]
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_343]],  
              ActivitiesId = {
                [[Client2_376]]
              },  
              HairType = 5621550,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 5606958,  
              FeetColor = 3,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_341]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_376]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_377]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_346]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[15]],  
                        InstanceId = [[Client2_396]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_378]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_434]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_418]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_435]],  
                        Type = [[None]],  
                        TimeLimitValue = [[15]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[tower]],  
                    InstanceId = [[Client2_378]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_379]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_380]],  
                            Who = [[Client2_343]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_381]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_382]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_383]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_387]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_388]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_389]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_390]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_392]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_393]],  
                            Who = [[Client2_343]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_394]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_397]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_398]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_401]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client2_436]],  
                    Name = [[manag]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client2_437]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client2_438]],  
                            Emote = [[]],  
                            Who = [[Client2_343]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_440]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_410]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client2_409]],  
                      Value = r2.RefId([[Client2_396]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_411]],  
                          Value = r2.RefId([[Client2_405]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_412]],  
                        Entity = r2.RefId([[Client2_272]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_414]],  
                          Value = r2.RefId([[Client2_403]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_415]],  
                        Entity = r2.RefId([[Client2_268]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 5606702,  
              Angle = -0.578125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595694,  
              ArmColor = 3,  
              Name = [[Kaeno Messenger]],  
              Position = {
                y = -1882.640625,  
                x = 22529.54688,  
                InstanceId = [[Client2_344]],  
                Class = [[Position]],  
                z = 6.484375
              },  
              Sheet = [[ring_guard_melee_tank_slash_b3.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client2_203]]
        },  
        {
          InstanceId = [[Client2_243]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client2_242]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client2_206]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_204]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_325]],  
                    Name = [[]],  
                    InstanceId = [[Client2_323]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_327]]
                  },  
                  {
                    LogicEntityAction = [[Client2_330]],  
                    Name = [[]],  
                    InstanceId = [[Client2_328]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_332]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[start]],  
                    InstanceId = [[Client2_290]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_291]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_292]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_408]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[init]],  
                    InstanceId = [[Client2_248]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_249]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_290]]),  
                        ActivityZoneId = r2.RefId([[Client2_225]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[HIME]],  
                    InstanceId = [[Client2_278]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_280]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_214]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[KAENO]],  
                    InstanceId = [[Client2_279]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_281]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Raider Kipucker]],  
              Position = {
                y = -1852.015625,  
                x = 22651.39063,  
                InstanceId = [[Client2_207]],  
                Class = [[Position]],  
                z = 15.3125
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.ckeic4]],  
              ActivitiesId = {
                [[Client2_248]],  
                [[Client2_278]],  
                [[Client2_279]]
              }
            },  
            {
              InstanceId = [[Client2_210]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_208]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Raider Kipucker]],  
              Position = {
                y = -1856.21875,  
                x = 22645.79688,  
                InstanceId = [[Client2_211]],  
                Class = [[Position]],  
                z = 16.234375
              },  
              Angle = -1.03125,  
              Base = [[palette.entities.creatures.ckeic3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_214]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_212]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Raider Kipucker]],  
              Position = {
                y = -1852.390625,  
                x = 22658.54688,  
                InstanceId = [[Client2_215]],  
                Class = [[Position]],  
                z = 14.84375
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.creatures.ckeic3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_218]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_216]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              Position = {
                y = -1863.328125,  
                x = 22651.78125,  
                InstanceId = [[Client2_219]],  
                Class = [[Position]],  
                z = 17.625
              },  
              Angle = -1.234375,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_246]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_244]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              Position = {
                y = -1850.671875,  
                x = 22640.375,  
                InstanceId = [[Client2_247]],  
                Class = [[Position]],  
                z = 13.921875
              },  
              Angle = 2.484375,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_264]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_262]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              Position = {
                y = -1871.015625,  
                x = 22647.53125,  
                InstanceId = [[Client2_265]],  
                Class = [[Position]],  
                z = 17.515625
              },  
              Angle = -1.890625,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client2_241]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client2_321]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
      },  
      Title = [[Act IV: Blastercalm]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client2_322]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 13,  
        InstanceId = [[Client1_328]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_329]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_330]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_331]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_332]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_344]],  
        Class = [[TextManagerEntry]],  
        Text = [[These are dire times, I fear that we might face hard times to come.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_345]],  
        Class = [[TextManagerEntry]],  
        Text = [[I fear that the others might not do well now, I wonder how they are...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_349]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_350]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_355]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_356]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_357]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_293]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_294]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_366]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_295]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_296]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_364]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_369]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_370]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_371]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need... (should be "to see")]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_372]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_373]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_381]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitins are coming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_384]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_385]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_386]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_387]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_390]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_391]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_394]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_395]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_399]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_400]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_401]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_402]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_407]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_408]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_439]],  
        Count = 1,  
        Text = [[We have a huge problem!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client2_440]],  
        Count = 1,  
        Text = [[We have a huge problem!]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}