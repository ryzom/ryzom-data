scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_22]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_24]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    ActivityStep = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_25]],  
      ActivitiesIds = {
        [[Client1_1336]],  
        [[Client1_1521]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1340]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1338]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1521]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1522]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Clopperketh]],  
              Position = {
                y = -2182.59375,  
                x = 30520.34375,  
                InstanceId = [[Client1_1341]],  
                Class = [[Position]],  
                z = 78.40625
              },  
              Angle = -1.265625,  
              Base = [[palette.entities.creatures.ccbdc7]],  
              ActivitiesId = {
                [[Client1_1521]]
              }
            }
          },  
          InstanceId = [[Client1_26]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_23]]
  }
}