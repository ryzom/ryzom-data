scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 14,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      InstanceId = [[Client1_1135]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1141]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 27940.65625,  
                y = -1967.8125,  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1139]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Exit]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1145]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1146]],  
                x = 27899.29688,  
                y = -1851.296875,  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[house ruin 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1149]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1150]],  
                x = 27895.9375,  
                y = -1851.203125,  
                z = -13.875
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1147]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1153]],  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1154]],  
                x = 27931.39063,  
                y = -1850.9375,  
                z = -14.5
              },  
              Angle = -1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tower ruin 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1157]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1158]],  
                x = 27922.95313,  
                y = -1844.8125,  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1155]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1161]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1162]],  
                x = 27937.125,  
                y = -1853.234375,  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1159]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1165]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1166]],  
                x = 27892.96875,  
                y = -1838.734375,  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1163]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo smoke 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1169]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1170]],  
                x = 27937.21875,  
                y = -1840.8125,  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo smoke 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1173]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1174]],  
                x = 27930.85938,  
                y = -1856.328125,  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1177]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1178]],  
                x = 27898.125,  
                y = -1853.578125,  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1175]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1181]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1182]],  
                x = 27942.53125,  
                y = -1887.59375,  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1179]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[house ruin 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1185]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1186]],  
                x = 27941.15625,  
                y = -1901.703125,  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1183]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[house ruin 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1189]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1190]],  
                x = 27943.17188,  
                y = -1896.890625,  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1187]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1193]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1194]],  
                x = 27939.875,  
                y = -1900.859375,  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1191]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 3]]
            }
          },  
          InstanceId = [[Client1_1136]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 1,  
      Version = 1,  
      Title = [[]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1133]]
  }
}