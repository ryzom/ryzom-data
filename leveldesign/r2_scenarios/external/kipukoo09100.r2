scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_869]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_871]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_872]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_878]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_879]],  
                x = 26956.1875,  
                y = -2013.53125,  
                z = -8.640625
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_876]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_882]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_883]],  
                x = 26958.40625,  
                y = -2022.515625,  
                z = -9.359375
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_880]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_886]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_887]],  
                x = 26954.125,  
                y = -2004.546875,  
                z = -8.25
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_890]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_891]],  
                x = 26960.5,  
                y = -2031.515625,  
                z = -9.40625
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_888]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_894]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_895]],  
                x = 26952.09375,  
                y = -1995.390625,  
                z = -7.71875
              },  
              Angle = -2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_892]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 5]]
            }
          },  
          InstanceId = [[Client1_873]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_874]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_875]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_870]]
  }
}