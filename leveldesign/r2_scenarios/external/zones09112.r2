scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_22]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_24]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    TextManager = 0,  
    Position = 0,  
    Region = 0,  
    Road = 0,  
    WayPoint = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_31]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_29]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]],  
              Position = {
                y = -2299.8125,  
                x = 30626.82813,  
                InstanceId = [[Client1_32]],  
                Class = [[Position]],  
                z = 78.109375
              },  
              Angle = -1.453125,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_35]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_33]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[runic circle 1]],  
              Position = {
                y = -2301.375,  
                x = 30610.96875,  
                InstanceId = [[Client1_36]],  
                Class = [[Position]],  
                z = 77.484375
              },  
              Angle = 0.265625,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y0]],  
              InstanceId = [[Client1_246]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_245]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_248]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.15625,  
                    x = 30830.48438,  
                    InstanceId = [[Client1_249]],  
                    Class = [[Position]],  
                    z = 66.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_251]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.515625,  
                    x = 30830.375,  
                    InstanceId = [[Client1_252]],  
                    Class = [[Position]],  
                    z = 79.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_254]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.6875,  
                    x = 30831.84375,  
                    InstanceId = [[Client1_255]],  
                    Class = [[Position]],  
                    z = 79.390625
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_258]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_256]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]],  
              Position = {
                y = -2240.1875,  
                x = 30831.40625,  
                InstanceId = [[Client1_259]],  
                Class = [[Position]],  
                z = 65.046875
              },  
              Angle = 3.109375,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_262]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_260]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 3]],  
              Position = {
                y = -2130.625,  
                x = 30831.03125,  
                InstanceId = [[Client1_263]],  
                Class = [[Position]],  
                z = 70.359375
              },  
              Angle = -3.09375,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_266]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_264]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 4]],  
              Position = {
                y = -2020.71875,  
                x = 30830.98438,  
                InstanceId = [[Client1_267]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = 3.125,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_270]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_268]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 5]],  
              Position = {
                y = -1910.953125,  
                x = 30831.09375,  
                InstanceId = [[Client1_271]],  
                Class = [[Position]],  
                z = 79.09375
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X5]],  
              InstanceId = [[Client1_273]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_272]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_275]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.546875,  
                    x = 30830.53125,  
                    InstanceId = [[Client1_276]],  
                    Class = [[Position]],  
                    z = 79.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_278]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.5,  
                    x = 30375.34375,  
                    InstanceId = [[Client1_279]],  
                    Class = [[Position]],  
                    z = 45.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_281]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1795.296875,  
                    x = 30373.40625,  
                    InstanceId = [[Client1_282]],  
                    Class = [[Position]],  
                    z = 45.703125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_285]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_283]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 6]],  
              Position = {
                y = -1808.203125,  
                x = 30466.29688,  
                InstanceId = [[Client1_286]],  
                Class = [[Position]],  
                z = 51.453125
              },  
              Angle = -1.515625,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_289]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_287]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 7]],  
              Position = {
                y = -1800,  
                x = 30557.5625,  
                InstanceId = [[Client1_290]],  
                Class = [[Position]],  
                z = 58.421875
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_293]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_291]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 8]],  
              Position = {
                y = -1800.109375,  
                x = 30648.54688,  
                InstanceId = [[Client1_294]],  
                Class = [[Position]],  
                z = 67.453125
              },  
              Angle = -1.625,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_297]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_295]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 9]],  
              Position = {
                y = -1800.28125,  
                x = 30739.10938,  
                InstanceId = [[Client1_298]],  
                Class = [[Position]],  
                z = 72.125
              },  
              Angle = -1.46875,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y1]],  
              InstanceId = [[Client1_300]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_299]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_302]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.515625,  
                    x = 30739.15625,  
                    InstanceId = [[Client1_303]],  
                    Class = [[Position]],  
                    z = 72.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_305]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2360.140625,  
                    x = 30739.54688,  
                    InstanceId = [[Client1_306]],  
                    Class = [[Position]],  
                    z = 75.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_308]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2359.015625,  
                    x = 30740.98438,  
                    InstanceId = [[Client1_309]],  
                    Class = [[Position]],  
                    z = 74.71875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X0]],  
              InstanceId = [[Client1_311]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_310]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_313]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.1875,  
                    x = 30830.45313,  
                    InstanceId = [[Client1_314]],  
                    Class = [[Position]],  
                    z = 66.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_316]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.71875,  
                    x = 30374.9375,  
                    InstanceId = [[Client1_317]],  
                    Class = [[Position]],  
                    z = 54.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_319]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.046875,  
                    x = 30376.25,  
                    InstanceId = [[Client1_320]],  
                    Class = [[Position]],  
                    z = 54.28125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y5]],  
              InstanceId = [[Client1_322]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_321]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_324]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.84375,  
                    x = 30374.90625,  
                    InstanceId = [[Client1_325]],  
                    Class = [[Position]],  
                    z = 54.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_327]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.5625,  
                    x = 30375.375,  
                    InstanceId = [[Client1_328]],  
                    Class = [[Position]],  
                    z = 45.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_330]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.4375,  
                    x = 30374.45313,  
                    InstanceId = [[Client1_331]],  
                    Class = [[Position]],  
                    z = 45.5625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y4]],  
              InstanceId = [[Client1_333]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_332]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_335]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1805.234375,  
                    x = 30466.57813,  
                    InstanceId = [[Client1_336]],  
                    Class = [[Position]],  
                    z = 53.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_338]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.640625,  
                    x = 30466.6875,  
                    InstanceId = [[Client1_339]],  
                    Class = [[Position]],  
                    z = 65.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_341]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2348.90625,  
                    x = 30465.92188,  
                    InstanceId = [[Client1_342]],  
                    Class = [[Position]],  
                    z = 65.953125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y3]],  
              InstanceId = [[Client1_344]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_343]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_346]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.5,  
                    x = 30557.5625,  
                    InstanceId = [[Client1_347]],  
                    Class = [[Position]],  
                    z = 58.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_349]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.453125,  
                    x = 30557.39063,  
                    InstanceId = [[Client1_350]],  
                    Class = [[Position]],  
                    z = 79.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_352]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2351.546875,  
                    x = 30559.8125,  
                    InstanceId = [[Client1_353]],  
                    Class = [[Position]],  
                    z = 79.03125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y2]],  
              InstanceId = [[Client1_355]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_354]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_357]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.53125,  
                    x = 30648.59375,  
                    InstanceId = [[Client1_358]],  
                    Class = [[Position]],  
                    z = 67.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_360]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.375,  
                    x = 30648.65625,  
                    InstanceId = [[Client1_361]],  
                    Class = [[Position]],  
                    z = 78.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_363]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2352.03125,  
                    x = 30649.98438,  
                    InstanceId = [[Client1_364]],  
                    Class = [[Position]],  
                    z = 79.3125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X4]],  
              InstanceId = [[Client1_366]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_365]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_368]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1910.9375,  
                    x = 30830.32813,  
                    InstanceId = [[Client1_369]],  
                    Class = [[Position]],  
                    z = 79.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_371]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1910.765625,  
                    x = 30375.29688,  
                    InstanceId = [[Client1_372]],  
                    Class = [[Position]],  
                    z = 49.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_374]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1909.78125,  
                    x = 30372.59375,  
                    InstanceId = [[Client1_375]],  
                    Class = [[Position]],  
                    z = 49.3125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X3]],  
              InstanceId = [[Client1_377]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_376]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_379]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2020.75,  
                    x = 30830.35938,  
                    InstanceId = [[Client1_380]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_382]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2020.109375,  
                    x = 30375.17188,  
                    InstanceId = [[Client1_383]],  
                    Class = [[Position]],  
                    z = 47.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_385]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2021.671875,  
                    x = 30372.10938,  
                    InstanceId = [[Client1_386]],  
                    Class = [[Position]],  
                    z = 46.796875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X2]],  
              InstanceId = [[Client1_388]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_387]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_390]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2130.75,  
                    x = 30830.29688,  
                    InstanceId = [[Client1_391]],  
                    Class = [[Position]],  
                    z = 70.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_393]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2130.140625,  
                    x = 30375.125,  
                    InstanceId = [[Client1_394]],  
                    Class = [[Position]],  
                    z = 53.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_396]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2132.734375,  
                    x = 30371.5,  
                    InstanceId = [[Client1_397]],  
                    Class = [[Position]],  
                    z = 53.265625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X1]],  
              InstanceId = [[Client1_399]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_398]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_401]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2240.359375,  
                    x = 30830.5,  
                    InstanceId = [[Client1_402]],  
                    Class = [[Position]],  
                    z = 65.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_404]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2239.90625,  
                    x = 30375.0625,  
                    InstanceId = [[Client1_405]],  
                    Class = [[Position]],  
                    z = 55.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_407]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2241.359375,  
                    x = 30372.09375,  
                    InstanceId = [[Client1_408]],  
                    Class = [[Position]],  
                    z = 55.578125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AA]],  
              InstanceId = [[Client1_410]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_409]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_412]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.859375,  
                    x = 30829.85938,  
                    InstanceId = [[Client1_413]],  
                    Class = [[Position]],  
                    z = 66.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_415]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.84375,  
                    x = 30740.42188,  
                    InstanceId = [[Client1_416]],  
                    Class = [[Position]],  
                    z = 74.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_418]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.765625,  
                    x = 30739.71875,  
                    InstanceId = [[Client1_419]],  
                    Class = [[Position]],  
                    z = 67.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_421]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.8125,  
                    x = 30829.25,  
                    InstanceId = [[Client1_422]],  
                    Class = [[Position]],  
                    z = 65.375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BA]],  
              InstanceId = [[Client1_424]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_423]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_426]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.75,  
                    x = 30829.25,  
                    InstanceId = [[Client1_427]],  
                    Class = [[Position]],  
                    z = 65.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_429]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.578125,  
                    x = 30830,  
                    InstanceId = [[Client1_430]],  
                    Class = [[Position]],  
                    z = 70.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_432]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.15625,  
                    x = 30740.8125,  
                    InstanceId = [[Client1_433]],  
                    Class = [[Position]],  
                    z = 73.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_435]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2238.15625,  
                    x = 30739.92188,  
                    InstanceId = [[Client1_436]],  
                    Class = [[Position]],  
                    z = 68.71875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CA]],  
              InstanceId = [[Client1_438]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_437]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_440]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.5,  
                    x = 30740.07813,  
                    InstanceId = [[Client1_441]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_443]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2130,  
                    x = 30829.125,  
                    InstanceId = [[Client1_444]],  
                    Class = [[Position]],  
                    z = 70.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_446]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2022.234375,  
                    x = 30829.79688,  
                    InstanceId = [[Client1_447]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_449]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.03125,  
                    x = 30741.46875,  
                    InstanceId = [[Client1_450]],  
                    Class = [[Position]],  
                    z = 77.421875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DA]],  
              InstanceId = [[Client1_452]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_451]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_454]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.6875,  
                    x = 30741.03125,  
                    InstanceId = [[Client1_455]],  
                    Class = [[Position]],  
                    z = 77.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_457]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.640625,  
                    x = 30740.14063,  
                    InstanceId = [[Client1_458]],  
                    Class = [[Position]],  
                    z = 76.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_460]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.640625,  
                    x = 30828.6875,  
                    InstanceId = [[Client1_461]],  
                    Class = [[Position]],  
                    z = 79.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_463]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2018.671875,  
                    x = 30829.45313,  
                    InstanceId = [[Client1_464]],  
                    Class = [[Position]],  
                    z = 75.3125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EA]],  
              InstanceId = [[Client1_466]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_465]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_468]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.34375,  
                    x = 30829.07813,  
                    InstanceId = [[Client1_469]],  
                    Class = [[Position]],  
                    z = 79.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_471]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.375,  
                    x = 30740.78125,  
                    InstanceId = [[Client1_472]],  
                    Class = [[Position]],  
                    z = 76.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_474]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.515625,  
                    x = 30739.84375,  
                    InstanceId = [[Client1_475]],  
                    Class = [[Position]],  
                    z = 71.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_477]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.21875,  
                    x = 30828.84375,  
                    InstanceId = [[Client1_478]],  
                    Class = [[Position]],  
                    z = 78.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EB]],  
              InstanceId = [[Client1_503]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_502]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_505]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1909.796875,  
                    x = 30738.17188,  
                    InstanceId = [[Client1_506]],  
                    Class = [[Position]],  
                    z = 76.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_508]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.109375,  
                    x = 30650.40625,  
                    InstanceId = [[Client1_509]],  
                    Class = [[Position]],  
                    z = 62.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_511]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.3125,  
                    x = 30649.20313,  
                    InstanceId = [[Client1_512]],  
                    Class = [[Position]],  
                    z = 67.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_514]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.46875,  
                    x = 30737.5625,  
                    InstanceId = [[Client1_515]],  
                    Class = [[Position]],  
                    z = 72.09375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EC]],  
              InstanceId = [[Client1_517]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_516]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_519]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1909.96875,  
                    x = 30647.57813,  
                    InstanceId = [[Client1_520]],  
                    Class = [[Position]],  
                    z = 61.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_522]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.125,  
                    x = 30559.28125,  
                    InstanceId = [[Client1_523]],  
                    Class = [[Position]],  
                    z = 58.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_525]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.90625,  
                    x = 30558.15625,  
                    InstanceId = [[Client1_526]],  
                    Class = [[Position]],  
                    z = 58.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_528]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.390625,  
                    x = 30647.32813,  
                    InstanceId = [[Client1_529]],  
                    Class = [[Position]],  
                    z = 67.25
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[ED]],  
              InstanceId = [[Client1_531]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_530]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_533]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1909.8125,  
                    x = 30556.90625,  
                    InstanceId = [[Client1_534]],  
                    Class = [[Position]],  
                    z = 58.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_536]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.15625,  
                    x = 30468.5625,  
                    InstanceId = [[Client1_537]],  
                    Class = [[Position]],  
                    z = 51.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_539]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1805.328125,  
                    x = 30467.29688,  
                    InstanceId = [[Client1_540]],  
                    Class = [[Position]],  
                    z = 53.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_542]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.328125,  
                    x = 30556.59375,  
                    InstanceId = [[Client1_543]],  
                    Class = [[Position]],  
                    z = 58.296875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EE]],  
              InstanceId = [[Client1_545]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_544]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_547]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.109375,  
                    x = 30465.01563,  
                    InstanceId = [[Client1_548]],  
                    Class = [[Position]],  
                    z = 51.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_550]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.046875,  
                    x = 30376.75,  
                    InstanceId = [[Client1_551]],  
                    Class = [[Position]],  
                    z = 49.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_553]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1802.09375,  
                    x = 30376.14063,  
                    InstanceId = [[Client1_554]],  
                    Class = [[Position]],  
                    z = 45.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_556]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1805.40625,  
                    x = 30465.14063,  
                    InstanceId = [[Client1_557]],  
                    Class = [[Position]],  
                    z = 53.15625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DE]],  
              InstanceId = [[Client1_559]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_558]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_561]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.015625,  
                    x = 30376.25,  
                    InstanceId = [[Client1_562]],  
                    Class = [[Position]],  
                    z = 49.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_564]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.34375,  
                    x = 30465.09375,  
                    InstanceId = [[Client1_565]],  
                    Class = [[Position]],  
                    z = 51.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_567]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.125,  
                    x = 30466.14063,  
                    InstanceId = [[Client1_568]],  
                    Class = [[Position]],  
                    z = 55.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_570]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.4375,  
                    x = 30377.26563,  
                    InstanceId = [[Client1_571]],  
                    Class = [[Position]],  
                    z = 47.828125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CE]],  
              InstanceId = [[Client1_573]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_572]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_575]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.046875,  
                    x = 30376.5625,  
                    InstanceId = [[Client1_576]],  
                    Class = [[Position]],  
                    z = 47.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_578]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.546875,  
                    x = 30375.65625,  
                    InstanceId = [[Client1_579]],  
                    Class = [[Position]],  
                    z = 53.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_581]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.9375,  
                    x = 30464.59375,  
                    InstanceId = [[Client1_582]],  
                    Class = [[Position]],  
                    z = 55.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_584]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.6875,  
                    x = 30465.875,  
                    InstanceId = [[Client1_585]],  
                    Class = [[Position]],  
                    z = 55.53125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BE]],  
              InstanceId = [[Client1_587]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_586]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_589]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.1875,  
                    x = 30376.20313,  
                    InstanceId = [[Client1_590]],  
                    Class = [[Position]],  
                    z = 53.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_592]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2237.75,  
                    x = 30375.42188,  
                    InstanceId = [[Client1_593]],  
                    Class = [[Position]],  
                    z = 56.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_595]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.390625,  
                    x = 30463.85938,  
                    InstanceId = [[Client1_596]],  
                    Class = [[Position]],  
                    z = 66.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_598]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.875,  
                    x = 30464.98438,  
                    InstanceId = [[Client1_599]],  
                    Class = [[Position]],  
                    z = 54.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AE]],  
              InstanceId = [[Client1_601]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_600]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_603]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.453125,  
                    x = 30375.73438,  
                    InstanceId = [[Client1_604]],  
                    Class = [[Position]],  
                    z = 55.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_606]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2347.4375,  
                    x = 30376.375,  
                    InstanceId = [[Client1_607]],  
                    Class = [[Position]],  
                    z = 54.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_609]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.71875,  
                    x = 30463.10938,  
                    InstanceId = [[Client1_610]],  
                    Class = [[Position]],  
                    z = 65.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_612]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2242.859375,  
                    x = 30465.29688,  
                    InstanceId = [[Client1_613]],  
                    Class = [[Position]],  
                    z = 66.765625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AD]],  
              InstanceId = [[Client1_615]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_614]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_617]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.140625,  
                    x = 30468.14063,  
                    InstanceId = [[Client1_618]],  
                    Class = [[Position]],  
                    z = 66.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_620]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.578125,  
                    x = 30556.03125,  
                    InstanceId = [[Client1_621]],  
                    Class = [[Position]],  
                    z = 70.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_623]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2348.734375,  
                    x = 30556.5,  
                    InstanceId = [[Client1_624]],  
                    Class = [[Position]],  
                    z = 79.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_626]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.90625,  
                    x = 30467.78125,  
                    InstanceId = [[Client1_627]],  
                    Class = [[Position]],  
                    z = 65.96875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AC Spawn]],  
              InstanceId = [[Client1_629]],  
              Class = [[Region]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_628]],  
                x = 0.75,  
                y = 0,  
                z = -0.28125
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_631]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.015625,  
                    x = 30558.375,  
                    InstanceId = [[Client1_632]],  
                    Class = [[Position]],  
                    z = 69.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_634]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.828125,  
                    x = 30647.35938,  
                    InstanceId = [[Client1_635]],  
                    Class = [[Position]],  
                    z = 68.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_637]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.171875,  
                    x = 30647.92188,  
                    InstanceId = [[Client1_638]],  
                    Class = [[Position]],  
                    z = 78.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_640]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.453125,  
                    x = 30558.59375,  
                    InstanceId = [[Client1_641]],  
                    Class = [[Position]],  
                    z = 79.21875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AB]],  
              InstanceId = [[Client1_643]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_642]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_645]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.171875,  
                    x = 30649.6875,  
                    InstanceId = [[Client1_646]],  
                    Class = [[Position]],  
                    z = 68.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_648]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.828125,  
                    x = 30738.1875,  
                    InstanceId = [[Client1_649]],  
                    Class = [[Position]],  
                    z = 67.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_651]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.5625,  
                    x = 30738.96875,  
                    InstanceId = [[Client1_652]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_654]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.84375,  
                    x = 30649.59375,  
                    InstanceId = [[Client1_655]],  
                    Class = [[Position]],  
                    z = 78.53125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BB]],  
              InstanceId = [[Client1_657]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_656]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_659]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.578125,  
                    x = 30738.76563,  
                    InstanceId = [[Client1_660]],  
                    Class = [[Position]],  
                    z = 68.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_662]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.640625,  
                    x = 30738.73438,  
                    InstanceId = [[Client1_663]],  
                    Class = [[Position]],  
                    z = 73.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_665]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2130.96875,  
                    x = 30649.625,  
                    InstanceId = [[Client1_666]],  
                    Class = [[Position]],  
                    z = 73.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_668]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.09375,  
                    x = 30649.57813,  
                    InstanceId = [[Client1_669]],  
                    Class = [[Position]],  
                    z = 68.90625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BC]],  
              InstanceId = [[Client1_671]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_670]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_673]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.234375,  
                    x = 30647.51563,  
                    InstanceId = [[Client1_674]],  
                    Class = [[Position]],  
                    z = 69.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_676]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2238.953125,  
                    x = 30558.64063,  
                    InstanceId = [[Client1_677]],  
                    Class = [[Position]],  
                    z = 69.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_679]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.828125,  
                    x = 30558.21875,  
                    InstanceId = [[Client1_680]],  
                    Class = [[Position]],  
                    z = 65.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_682]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.140625,  
                    x = 30647.20313,  
                    InstanceId = [[Client1_683]],  
                    Class = [[Position]],  
                    z = 73.15625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BD]],  
              InstanceId = [[Client1_711]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_710]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_713]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2238.59375,  
                    x = 30556.73438,  
                    InstanceId = [[Client1_714]],  
                    Class = [[Position]],  
                    z = 69.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_716]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.109375,  
                    x = 30468.29688,  
                    InstanceId = [[Client1_717]],  
                    Class = [[Position]],  
                    z = 65.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_719]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2132.078125,  
                    x = 30467.54688,  
                    InstanceId = [[Client1_720]],  
                    Class = [[Position]],  
                    z = 54.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_722]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2130.984375,  
                    x = 30555.9375,  
                    InstanceId = [[Client1_723]],  
                    Class = [[Position]],  
                    z = 64.8125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CD]],  
              InstanceId = [[Client1_725]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_724]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_727]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.703125,  
                    x = 30467.48438,  
                    InstanceId = [[Client1_728]],  
                    Class = [[Position]],  
                    z = 54.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_730]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.84375,  
                    x = 30467.10938,  
                    InstanceId = [[Client1_731]],  
                    Class = [[Position]],  
                    z = 55.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_733]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.1875,  
                    x = 30556.4375,  
                    InstanceId = [[Client1_734]],  
                    Class = [[Position]],  
                    z = 59.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_736]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.640625,  
                    x = 30556.625,  
                    InstanceId = [[Client1_737]],  
                    Class = [[Position]],  
                    z = 64.875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CC]],  
              InstanceId = [[Client1_739]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_738]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_741]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.875,  
                    x = 30558.03125,  
                    InstanceId = [[Client1_742]],  
                    Class = [[Position]],  
                    z = 65.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_744]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.140625,  
                    x = 30647.15625,  
                    InstanceId = [[Client1_745]],  
                    Class = [[Position]],  
                    z = 73.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_747]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2022.171875,  
                    x = 30647.51563,  
                    InstanceId = [[Client1_748]],  
                    Class = [[Position]],  
                    z = 61.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_750]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.515625,  
                    x = 30558.78125,  
                    InstanceId = [[Client1_751]],  
                    Class = [[Position]],  
                    z = 59.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CB]],  
              InstanceId = [[Client1_753]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_752]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_755]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.046875,  
                    x = 30649.46875,  
                    InstanceId = [[Client1_756]],  
                    Class = [[Position]],  
                    z = 73.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_758]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.484375,  
                    x = 30737.10938,  
                    InstanceId = [[Client1_759]],  
                    Class = [[Position]],  
                    z = 74.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_761]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.5625,  
                    x = 30738.3125,  
                    InstanceId = [[Client1_762]],  
                    Class = [[Position]],  
                    z = 77.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_764]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2022.15625,  
                    x = 30650.51563,  
                    InstanceId = [[Client1_765]],  
                    Class = [[Position]],  
                    z = 61.5
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DB]],  
              InstanceId = [[Client1_793]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_792]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_795]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.921875,  
                    x = 30737.98438,  
                    InstanceId = [[Client1_796]],  
                    Class = [[Position]],  
                    z = 77.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_798]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.515625,  
                    x = 30738.35938,  
                    InstanceId = [[Client1_799]],  
                    Class = [[Position]],  
                    z = 76.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_801]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.875,  
                    x = 30651.09375,  
                    InstanceId = [[Client1_802]],  
                    Class = [[Position]],  
                    z = 62.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_804]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.609375,  
                    x = 30649.48438,  
                    InstanceId = [[Client1_805]],  
                    Class = [[Position]],  
                    z = 61.1875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DC]],  
              InstanceId = [[Client1_807]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_806]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_809]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.0625,  
                    x = 30648.07813,  
                    InstanceId = [[Client1_810]],  
                    Class = [[Position]],  
                    z = 60.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_812]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.734375,  
                    x = 30558.875,  
                    InstanceId = [[Client1_813]],  
                    Class = [[Position]],  
                    z = 59.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_815]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.171875,  
                    x = 30558.17188,  
                    InstanceId = [[Client1_816]],  
                    Class = [[Position]],  
                    z = 58.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_818]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.734375,  
                    x = 30647.5625,  
                    InstanceId = [[Client1_819]],  
                    Class = [[Position]],  
                    z = 62.046875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DD]],  
              InstanceId = [[Client1_821]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_820]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_823]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.703125,  
                    x = 30556.71875,  
                    InstanceId = [[Client1_824]],  
                    Class = [[Position]],  
                    z = 58.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_826]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.703125,  
                    x = 30469.03125,  
                    InstanceId = [[Client1_827]],  
                    Class = [[Position]],  
                    z = 52.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_829]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2018.671875,  
                    x = 30466.92188,  
                    InstanceId = [[Client1_830]],  
                    Class = [[Position]],  
                    z = 55.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_832]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.453125,  
                    x = 30556.23438,  
                    InstanceId = [[Client1_833]],  
                    Class = [[Position]],  
                    z = 59.453125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[XF Extra]],  
              InstanceId = [[Client1_1092]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1091]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1094]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.125,  
                    x = 30374.1875,  
                    InstanceId = [[Client1_1095]],  
                    Class = [[Position]],  
                    z = 54.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1097]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2351.09375,  
                    x = 30280.79688,  
                    InstanceId = [[Client1_1098]],  
                    Class = [[Position]],  
                    z = 41.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1100]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2062.859375,  
                    x = 30290.78125,  
                    InstanceId = [[Client1_1101]],  
                    Class = [[Position]],  
                    z = 49.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1103]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1979.765625,  
                    x = 30372.90625,  
                    InstanceId = [[Client1_1104]],  
                    Class = [[Position]],  
                    z = 48.4375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1107]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1105]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan mirador 1]],  
              Position = {
                y = -2295.25,  
                x = 30596.85938,  
                InstanceId = [[Client1_1108]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = 1.65625,  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1111]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1109]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              Position = {
                y = -2301.578125,  
                x = 30611.67188,  
                InstanceId = [[Client1_1112]],  
                Class = [[Position]],  
                z = 77.515625
              },  
              Angle = 1.578125,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1115]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1113]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan device 1]],  
              Position = {
                y = -2290.171875,  
                x = 30593.1875,  
                InstanceId = [[Client1_1116]],  
                Class = [[Position]],  
                z = 74.46875
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.karavan_device]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1119]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1117]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 1]],  
              Position = {
                y = -2295.15625,  
                x = 30607.54688,  
                InstanceId = [[Client1_1120]],  
                Class = [[Position]],  
                z = 75.734375
              },  
              Angle = 2.078125,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1123]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1121]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 2]],  
              Position = {
                y = -2305.15625,  
                x = 30606.65625,  
                InstanceId = [[Client1_1124]],  
                Class = [[Position]],  
                z = 77.71875
              },  
              Angle = -2.375,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1127]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1125]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 3]],  
              Position = {
                y = -2306.578125,  
                x = 30611.60938,  
                InstanceId = [[Client1_1128]],  
                Class = [[Position]],  
                z = 78.40625
              },  
              Angle = -1.40625,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1131]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1129]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 4]],  
              Position = {
                y = -2302.734375,  
                x = 30616.5625,  
                InstanceId = [[Client1_1132]],  
                Class = [[Position]],  
                z = 78
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1135]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1133]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 5]],  
              Position = {
                y = -2297.890625,  
                x = 30615.96875,  
                InstanceId = [[Client1_1136]],  
                Class = [[Position]],  
                z = 76.796875
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1137]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 6]],  
              Position = {
                y = -2294.953125,  
                x = 30612.96875,  
                InstanceId = [[Client1_1140]],  
                Class = [[Position]],  
                z = 75.90625
              },  
              Angle = 1.0625,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1141]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan wall 7]],  
              Position = {
                y = -2299.078125,  
                x = 30604.64063,  
                InstanceId = [[Client1_1144]],  
                Class = [[Position]],  
                z = 76.390625
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1145]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan banner 1]],  
              Position = {
                y = -2302.75,  
                x = 30609.60938,  
                InstanceId = [[Client1_1148]],  
                Class = [[Position]],  
                z = 77.625
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1171]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1172]],  
                x = 30615.76563,  
                y = -2305.921875,  
                z = 78.578125
              },  
              Angle = -2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1169]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bones 1 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1175]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1176]],  
                x = 30608.9375,  
                y = -2307.703125,  
                z = 78.421875
              },  
              Angle = -0.2810809612,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1173]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bones 1 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1179]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1180]],  
                x = 30603.6875,  
                y = -2302.671875,  
                z = 76.921875
              },  
              Angle = 1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1177]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bones 1 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1183]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1184]],  
                x = 30604.625,  
                y = -2296.921875,  
                z = 76.078125
              },  
              Angle = 1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1181]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bones 1 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1187]],  
              Base = [[palette.entities.botobjects.carapace_2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1188]],  
                x = 30603.67188,  
                y = -2277.859375,  
                z = 71.59375
              },  
              Angle = -0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1185]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[carapace 2 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1191]],  
              Base = [[palette.entities.botobjects.carapace_bul]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1192]],  
                x = 30606.42188,  
                y = -2278.25,  
                z = 71.59375
              },  
              Angle = 1.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1189]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[carapace 1 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1195]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1196]],  
                x = 30617.39063,  
                y = -2296.390625,  
                z = 76.359375
              },  
              Angle = 0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1193]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1199]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1200]],  
                x = 30617.46875,  
                y = -2296.375,  
                z = 76.359375
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1197]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1203]],  
              Base = [[palette.entities.botobjects.campfire_out]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1204]],  
                x = 30629.57813,  
                y = -2303.546875,  
                z = 79.171875
              },  
              Angle = 1.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1201]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[dead camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1207]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1208]],  
                x = 30629.75,  
                y = -2303.4375,  
                z = 79.15625
              },  
              Angle = 1.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1205]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1211]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1212]],  
                x = 30629.90625,  
                y = -2303.625,  
                z = 79.171875
              },  
              Angle = 0.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1209]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1215]],  
              Base = [[palette.entities.botobjects.campfire_out]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1216]],  
                x = 30629.71875,  
                y = -2303.84375,  
                z = 79.1875
              },  
              Angle = 2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1213]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[dead camp fire 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1219]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1220]],  
                x = 30629.65625,  
                y = -2303.8125,  
                z = 79.21875
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1217]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1223]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1224]],  
                x = 30630,  
                y = -2303.90625,  
                z = 79.21875
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1221]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1227]],  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1228]],  
                x = 30629.46875,  
                y = -2302.703125,  
                z = 79
              },  
              Angle = -0.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1225]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fallen jar 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1231]],  
              Base = [[palette.entities.botobjects.chariot_working]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1232]],  
                x = 30633.21875,  
                y = -2304.3125,  
                z = 78.90625
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1229]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[working chariot 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1235]],  
              Base = [[palette.entities.botobjects.chest_old]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1236]],  
                x = 30636.07813,  
                y = -2305.984375,  
                z = 78.59375
              },  
              Angle = 1.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1233]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[old chest 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1239]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1240]],  
                x = 30629.98438,  
                y = -2315.390625,  
                z = 79.5625
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1237]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1243]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1244]],  
                x = 30626.35938,  
                y = -2316.484375,  
                z = 79.515625
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1241]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1247]],  
              Base = [[palette.entities.botobjects.stump]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1248]],  
                x = 30656.10938,  
                y = -2322.390625,  
                z = 75.78125
              },  
              Angle = 2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1245]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[large stump 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1251]],  
              Base = [[palette.entities.botobjects.landslide_lake]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1252]],  
                x = 30652.17188,  
                y = -2298.578125,  
                z = 73.203125
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1249]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[lake landslide 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1259]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1260]],  
                x = 30632.84375,  
                y = -2312.140625,  
                z = 79.421875
              },  
              Angle = 1,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1257]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[house ruin 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1263]],  
              Base = [[palette.entities.botobjects.banner_kami]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1264]],  
                x = 30633.98438,  
                y = -2300.1875,  
                z = 77.75
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1261]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kami banner 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1267]],  
              Base = [[palette.entities.botobjects.fx_tr_pollen]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1268]],  
                x = 30629.65625,  
                y = -2299.90625,  
                z = 78.25
              },  
              Angle = 0.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1265]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[yellow pollen 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1271]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1272]],  
                x = 30611.76563,  
                y = -2300.46875,  
                z = 77.3125
              },  
              Angle = 0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1269]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo spot 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1275]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1276]],  
                x = 30591.5,  
                y = -2337.84375,  
                z = 80.3125
              },  
              Angle = 1,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1273]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo spot 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1279]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1280]],  
                x = 30580.73438,  
                y = -2333.671875,  
                z = 80.28125
              },  
              Angle = 0.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1277]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo spot 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1283]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1284]],  
                x = 30582.01563,  
                y = -2344.609375,  
                z = 78.890625
              },  
              Angle = -5.695780754,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1281]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo spot 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1287]],  
              Base = [[palette.entities.botobjects.fx_de_vapeurs]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1288]],  
                x = 30583.9375,  
                y = -2338.1875,  
                z = 79.953125
              },  
              Angle = -1.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1285]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[smoke 1]]
            }
          },  
          InstanceId = [[Client1_26]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_25]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_23]],  
    Texts = {
    }
  }
}