scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client1_1287]],  
      Season = [[winter]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts24]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_1278]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    MapDescription = 0,  
    Position = 0,  
    Location = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1276]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 61,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[The yearly anti-yubo ceremony!]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1279]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1281]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1280]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1282]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1283]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1285]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1284]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 909,  
      LocationId = [[Client1_1287]],  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1286]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:sdfsdf]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1277]]
  }
}