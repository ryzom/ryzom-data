scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_547]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_549]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Position = 0,  
    TextManager = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 22,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
        [[Client1_592]],  
        [[Client1_620]],  
        [[Client1_626]],  
        [[Client1_632]],  
        [[Client1_638]],  
        [[Client1_706]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_556]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_557]],  
                x = 26951.15625,  
                y = -1994.6875,  
                z = -7.625
              },  
              Angle = -2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_554]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Spawn Decoration]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_564]],  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_565]],  
                x = 27076.71875,  
                y = -2032.734375,  
                z = 14.96875
              },  
              Angle = 2.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_562]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kitin Sandbox]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_568]],  
              Base = [[palette.entities.botobjects.totem_kami]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_569]],  
                x = 26942.40625,  
                y = -2020.375,  
                z = -2.09375
              },  
              Angle = 1.069644094,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_566]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_579]],  
              Name = [[Kami Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_581]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_582]],  
                    x = 26941.07813,  
                    y = -2015.015625,  
                    z = -3.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_584]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_585]],  
                    x = 26935.17188,  
                    y = -1990.53125,  
                    z = -7.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_587]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_588]],  
                    x = 26960.73438,  
                    y = -1975.796875,  
                    z = -8.484375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_590]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_591]],  
                    x = 26966.4375,  
                    y = -2001.890625,  
                    z = -7.921875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_578]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_614]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_615]],  
                x = 26956.54688,  
                y = -2019.421875,  
                z = -8.890625
              },  
              Angle = -1.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_612]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[This is Spawn]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_618]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_619]],  
                x = 26963.09375,  
                y = -2000.171875,  
                z = -8.1875
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_616]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_620]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_621]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_620]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_624]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_625]],  
                x = 26956.71875,  
                y = -1979.53125,  
                z = -7.765625
              },  
              Angle = 0.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_622]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_626]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_627]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_626]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_630]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_631]],  
                x = 26940.03125,  
                y = -1991.234375,  
                z = -7.671875
              },  
              Angle = 2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_628]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_632]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_633]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_632]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_636]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_637]],  
                x = 26943.23438,  
                y = -2001.09375,  
                z = -7.71875
              },  
              Angle = -2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_634]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_638]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_639]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_638]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_666]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_667]],  
                x = 26948.76563,  
                y = -1884.21875,  
                z = -6.875
              },  
              Angle = -1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_664]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 6]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_670]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_671]],  
                x = 26948.73438,  
                y = -1886.21875,  
                z = -6.78125
              },  
              Angle = -0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_668]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 7]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_681]],  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_682]],  
                x = 26955.85938,  
                y = -1888.71875,  
                z = -6.6875
              },  
              Angle = -1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_679]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan gateway 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_685]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_686]],  
                x = 26958.875,  
                y = -1883.9375,  
                z = -6.953125
              },  
              Angle = -0.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_683]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_689]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_690]],  
                x = 26950.0625,  
                y = -1888.078125,  
                z = -6.75
              },  
              Angle = 1.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_687]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_693]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_694]],  
                x = 26946.51563,  
                y = -1882.34375,  
                z = -6.953125
              },  
              Angle = -0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_691]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_697]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_698]],  
                x = 26956.46875,  
                y = -1877.109375,  
                z = -7.609375
              },  
              Angle = -2.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_695]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_701]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_702]],  
                x = 26950.20313,  
                y = -1876.3125,  
                z = -7.078125
              },  
              Angle = -1.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_699]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 5]]
            }
          },  
          InstanceId = [[Client1_551]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_705]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_674]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_675]],  
                x = 26954,  
                y = -1885.625,  
                z = -6.859375
              },  
              Angle = -1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_672]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_706]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_707]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_706]]
              },  
              InheritPos = 1,  
              Name = [[voice of Jena 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_646]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_647]],  
                x = 26953.45313,  
                y = -1879.015625,  
                z = -7.1875
              },  
              Angle = -1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_644]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_650]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_651]],  
                x = 26954.625,  
                y = -1879.46875,  
                z = -7.25
              },  
              Angle = -1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_648]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_654]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_655]],  
                x = 26952,  
                y = -1879.28125,  
                z = -7.078125
              },  
              Angle = -1.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_652]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_658]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_659]],  
                x = 26950.57813,  
                y = -1880.6875,  
                z = -6.984375
              },  
              Angle = -1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_656]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_662]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_663]],  
                x = 26949.67188,  
                y = -1882.21875,  
                z = -6.90625
              },  
              Angle = -1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_660]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 5]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_704]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_703]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_550]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_548]],  
    Texts = {
    }
  }
}