scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 23,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 27,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
        [[Client1_106]],  
        [[Client1_108]],  
        [[Client1_110]],  
        [[Client1_155]],  
        [[Client1_157]],  
        [[Client1_159]]
      },  
      Title = [[ACT 0: Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_57]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_58]],  
                x = 26016.78125,  
                y = -2390.1875,  
                z = -9.609375
              },  
              Angle = 2.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_55]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Totem]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_61]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_62]],  
                x = 26012.625,  
                y = -2376.5625,  
                z = -10.171875
              },  
              Angle = -2.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_59]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Totem]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_65]],  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_66]],  
                x = 26014.73438,  
                y = -2383.625,  
                z = -9.953125
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_63]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Gateway]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_76]],  
              Name = [[Zo-Kae Kirosta]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_75]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_78]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_79]],  
                    x = 25971,  
                    y = -2570.328125,  
                    z = -9.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_81]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_82]],  
                    x = 26005.14063,  
                    y = -2582.25,  
                    z = -9.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_84]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_85]],  
                    x = 25978.34375,  
                    y = -2624.765625,  
                    z = -11.203125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_96]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_97]],  
                x = 25982.75,  
                y = -2584.78125,  
                z = -10.625
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_94]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_108]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_109]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              ActivitiesId = {
                [[Client1_108]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_100]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_101]],  
                x = 25985.28125,  
                y = -2591.0625,  
                z = -10.4375
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_98]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_110]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_111]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              ActivitiesId = {
                [[Client1_110]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_104]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_105]],  
                x = 25977.82813,  
                y = -2593.0625,  
                z = -10.125
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_102]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_106]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_107]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              ActivitiesId = {
                [[Client1_106]]
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_124]],  
              Name = [[Zo-Kae]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_123]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_126]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_127]],  
                    x = 25910.76563,  
                    y = -2624.765625,  
                    z = -9.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_129]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_130]],  
                    x = 25911.59375,  
                    y = -2651.59375,  
                    z = -10.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_132]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_133]],  
                    x = 25893.60938,  
                    y = -2673.125,  
                    z = -9.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_135]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_136]],  
                    x = 25870.8125,  
                    y = -2654.453125,  
                    z = -10.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_138]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_139]],  
                    x = 25871.70313,  
                    y = -2602.671875,  
                    z = -8.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_141]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_142]],  
                    x = 25890.39063,  
                    y = -2599.6875,  
                    z = -10.15625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_145]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_146]],  
                x = 25880.98438,  
                y = -2625.40625,  
                z = -9.875
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_159]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_160]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 5,  
              HairType = 8494,  
              HairColor = 3,  
              Tattoo = 31,  
              EyesColor = 7,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 5617710,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Ni-Zo Long]],  
              ActivitiesId = {
                [[Client1_159]]
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_149]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_150]],  
                x = 25886.32813,  
                y = -2628.21875,  
                z = -9.78125
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_147]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_157]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_158]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 10,  
              HairType = 9006,  
              HairColor = 3,  
              Tattoo = 9,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 5618734,  
              TrouserModel = 5618222,  
              FeetModel = 0,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[How-Long]],  
              ActivitiesId = {
                [[Client1_157]]
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_153]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_154]],  
                x = 25877.96875,  
                y = -2627.859375,  
                z = -9.890625
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_155]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_156]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 12,  
              HairType = 5624366,  
              HairColor = 5,  
              Tattoo = 24,  
              EyesColor = 4,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 3,  
              MorphTarget4 = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 5,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 5618734,  
              TrouserModel = 5618222,  
              FeetModel = 0,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 0,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Ve-Ri Long]],  
              ActivitiesId = {
                [[Client1_155]]
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_163]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_164]],  
                x = 25916.75,  
                y = -2639.640625,  
                z = -12.90625
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_161]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_167]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_168]],  
                x = 25938.67188,  
                y = -2641.390625,  
                z = -12.03125
              },  
              Angle = 0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_165]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 1,  
              HairType = 9006,  
              HairColor = 5,  
              Tattoo = 27,  
              EyesColor = 6,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              Sex = 0,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5636654,  
              Name = [[Zo-Kae Guard]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_zorai_male.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_171]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_172]],  
                x = 25937.01563,  
                y = -2630.390625,  
                z = -10.328125
              },  
              Angle = 0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_169]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 2,  
              HairType = 8750,  
              HairColor = 4,  
              Tattoo = 10,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 2,  
              MorphTarget3 = 1,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 4,  
              Sex = 0,  
              JacketModel = 5620014,  
              TrouserModel = 5619502,  
              FeetModel = 5618990,  
              HandsModel = 5619246,  
              ArmModel = 5619758,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5636654,  
              Name = [[Zo-Kae Guard]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_zorai_male.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_179]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_180]],  
                x = 25866.73438,  
                y = -2629.59375,  
                z = -9.1875
              },  
              Angle = 0.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_177]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Guard]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_183]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_184]],  
                x = 25867.09375,  
                y = -2619.796875,  
                z = -8.421875
              },  
              Angle = 0.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_181]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Guard]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_187]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_188]],  
                x = 25870.03125,  
                y = -2624.890625,  
                z = -9.03125
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_185]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Preacher]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_190]],  
              Name = [[Zo-Kae Kami]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_192]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_193]],  
                    x = 25869.25,  
                    y = -2653.53125,  
                    z = -10.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_195]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_196]],  
                    x = 25843.70313,  
                    y = -2648.4375,  
                    z = -8.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_198]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_199]],  
                    x = 25842.6875,  
                    y = -2615.15625,  
                    z = -6.3125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_201]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_202]],  
                    x = 25851.26563,  
                    y = -2600.8125,  
                    z = -8.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_204]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_205]],  
                    x = 25869.89063,  
                    y = -2597.6875,  
                    z = -9.34375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_189]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_208]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_209]],  
                x = 26177.84375,  
                y = -2465.53125,  
                z = -5.0625
              },  
              Angle = -1.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_206]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ki-Teen Goo Source]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_212]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_213]],  
                x = 26033.42188,  
                y = -2531.5625,  
                z = -10.5
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_210]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ki-Teen Goo]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_215]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_217]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_218]],  
                    x = 26146.34375,  
                    y = -2565.421875,  
                    z = -14.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_220]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_221]],  
                    x = 26151.57813,  
                    y = -2538.4375,  
                    z = -13.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_223]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_224]],  
                    x = 26117.75,  
                    y = -2503.734375,  
                    z = -13.484375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_226]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_227]],  
                    x = 26114.32813,  
                    y = -2443.875,  
                    z = -14.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_229]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_230]],  
                    x = 26170.29688,  
                    y = -2396.28125,  
                    z = -14.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_232]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_233]],  
                    x = 26175.5625,  
                    y = -2409.734375,  
                    z = -12.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_235]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_236]],  
                    x = 26208.46875,  
                    y = -2427.25,  
                    z = -12.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_238]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_239]],  
                    x = 26211.14063,  
                    y = -2509.21875,  
                    z = -13.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_241]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_242]],  
                    x = 26203.17188,  
                    y = -2515.5625,  
                    z = -13.609375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_244]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_245]],  
                    x = 26174.85938,  
                    y = -2544.953125,  
                    z = -14.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_247]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_248]],  
                    x = 26173.42188,  
                    y = -2563.359375,  
                    z = -14.34375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_214]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_267]],  
              Base = [[palette.entities.creatures.ckegb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_268]],  
                x = 26164.01563,  
                y = -2530.546875,  
                z = -12.890625
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_265]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_271]],  
              Base = [[palette.entities.creatures.ckegb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_272]],  
                x = 26170.79688,  
                y = -2531.546875,  
                z = -12.875
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_269]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_279]],  
              Base = [[palette.entities.creatures.ckegb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_280]],  
                x = 26161.125,  
                y = -2540.109375,  
                z = -13.765625
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_277]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_287]],  
              Base = [[palette.entities.creatures.ckegb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_288]],  
                x = 26164.15625,  
                y = -2537.25,  
                z = -13.296875
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_285]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_291]],  
              Base = [[palette.entities.creatures.ckegb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_292]],  
                x = 26168.98438,  
                y = -2535.671875,  
                z = -13.03125
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_289]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_295]],  
              Base = [[palette.entities.creatures.ckegb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_296]],  
                x = 26160.4375,  
                y = -2532.625,  
                z = -12.890625
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_293]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_299]],  
              Base = [[palette.entities.creatures.ckegb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_300]],  
                x = 26159.5625,  
                y = -2535.796875,  
                z = -13.0625
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_297]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]]
            }
          },  
          InstanceId = [[Client1_5]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_303]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_263]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_264]],  
                x = 26174.21875,  
                y = -2516.84375,  
                z = -12.625
              },  
              Angle = -1.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_261]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ki-Teen Leader]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_275]],  
              Base = [[palette.entities.creatures.ckegb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_276]],  
                x = 26162.1875,  
                y = -2522.796875,  
                z = -12.9375
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_273]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Poisoned Kipucka]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_283]],  
              Base = [[palette.entities.creatures.ckegb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_284]],  
                x = 26167.67188,  
                y = -2526.6875,  
                z = -12.84375
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_281]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Poisoned Kipucka]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_302]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_301]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_6]],  
      ActivitiesIds = {
      },  
      Title = [[Act I: Nothing]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}