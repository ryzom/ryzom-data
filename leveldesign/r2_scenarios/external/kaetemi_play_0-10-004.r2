scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_130]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_132]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    TextManager = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    NpcCustom = 0,  
    Position = 0,  
    ActivityStep = 1,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 40,  
      Behavior = {
        InstanceId = [[Client1_133]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_135]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_134]],  
        Class = [[Position]],  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Mira Region]],  
              InstanceId = [[Client1_150]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_152]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1329.953125,  
                    x = 21543.64063,  
                    InstanceId = [[Client1_153]],  
                    Class = [[Position]],  
                    z = 95.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_155]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1342.625,  
                    x = 21597.29688,  
                    InstanceId = [[Client1_156]],  
                    Class = [[Position]],  
                    z = 110.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_158]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1402.625,  
                    x = 21601.54688,  
                    InstanceId = [[Client1_159]],  
                    Class = [[Position]],  
                    z = 123.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1387.296875,  
                    x = 21528.46875,  
                    InstanceId = [[Client1_162]],  
                    Class = [[Position]],  
                    z = 102.703125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_149]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_163]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[#01 Mira Tower]],  
              Position = {
                y = -1376.65625,  
                x = 21579.59375,  
                InstanceId = [[Client1_166]],  
                Class = [[Position]],  
                z = 114.046875
              },  
              Angle = 2.359375,  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_167]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[#00 Kamivan]],  
              Position = {
                y = -1376.65625,  
                x = 21579.57813,  
                InstanceId = [[Client1_170]],  
                Class = [[Position]],  
                z = 114.015625
              },  
              Angle = 0.9375,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaetin Region]],  
              InstanceId = [[Client1_188]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_190]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1134.828125,  
                    x = 21467.35938,  
                    InstanceId = [[Client1_191]],  
                    Class = [[Position]],  
                    z = 73.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_193]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1075.375,  
                    x = 21509.28125,  
                    InstanceId = [[Client1_194]],  
                    Class = [[Position]],  
                    z = 73.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_196]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1008.75,  
                    x = 21448.5625,  
                    InstanceId = [[Client1_197]],  
                    Class = [[Position]],  
                    z = 61.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_199]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1038.078125,  
                    x = 21344.5625,  
                    InstanceId = [[Client1_200]],  
                    Class = [[Position]],  
                    z = 61.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_202]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1121.53125,  
                    x = 21356.03125,  
                    InstanceId = [[Client1_203]],  
                    Class = [[Position]],  
                    z = 62.375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_187]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_374]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_375]],  
                x = 21550.4375,  
                y = -1380.546875,  
                z = 105.109375
              },  
              Angle = 0.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_372]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Mira Runic Circle]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_378]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_379]],  
                x = 21567.4375,  
                y = -1379.53125,  
                z = 110.125
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_376]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Mira Fyros Tent]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_382]],  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_383]],  
                x = 21582.60938,  
                y = -1371.6875,  
                z = 114.234375
              },  
              Angle = 5.340707302,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_380]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Mira Cosmetics Tent]]
            }
          },  
          InstanceId = [[Client1_136]]
        },  
        {
          InstanceId = [[Client1_268]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Kaetin Boss Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_267]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_266]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_206]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_204]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_273]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_274]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_188]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kaeteketh]],  
              Position = {
                y = -1080.203125,  
                x = 21432.40625,  
                InstanceId = [[Client1_207]],  
                Class = [[Position]],  
                z = 66.296875
              },  
              Angle = 5.390625,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_216]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_214]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -1087.546875,  
                x = 21444.46875,  
                InstanceId = [[Client1_217]],  
                Class = [[Position]],  
                z = 69.359375
              },  
              Angle = -0.953125,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_212]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_210]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -1092.59375,  
                x = 21434.9375,  
                InstanceId = [[Client1_213]],  
                Class = [[Position]],  
                z = 68.46875
              },  
              Angle = -0.953125,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_228]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_226]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              Position = {
                y = -1097.765625,  
                x = 21455.8125,  
                InstanceId = [[Client1_229]],  
                Class = [[Position]],  
                z = 73.15625
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_224]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_222]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              Position = {
                y = -1103.296875,  
                x = 21448.35938,  
                InstanceId = [[Client1_225]],  
                Class = [[Position]],  
                z = 72.96875
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_220]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_218]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              Position = {
                y = -1108.859375,  
                x = 21442.01563,  
                InstanceId = [[Client1_221]],  
                Class = [[Position]],  
                z = 73.421875
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_244]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_242]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -1104.25,  
                x = 21462.48438,  
                InstanceId = [[Client1_245]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_240]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_238]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -1109.296875,  
                x = 21456.96875,  
                InstanceId = [[Client1_241]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_236]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_234]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -1113.15625,  
                x = 21450.70313,  
                InstanceId = [[Client1_237]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_232]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_230]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              Position = {
                y = -1117.578125,  
                x = 21444.76563,  
                InstanceId = [[Client1_233]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = -0.96875,  
              Base = [[palette.entities.creatures.ckeif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_264]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_262]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -1108.15625,  
                x = 21470.40625,  
                InstanceId = [[Client1_265]],  
                Class = [[Position]],  
                z = 75.375
              },  
              Angle = -1.140625,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_260]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_258]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -1114.484375,  
                x = 21465.07813,  
                InstanceId = [[Client1_261]],  
                Class = [[Position]],  
                z = 75.296875
              },  
              Angle = -1.140625,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_256]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_254]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -1117.90625,  
                x = 21457.82813,  
                InstanceId = [[Client1_257]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = -0.828125,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_252]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_250]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -1122.125,  
                x = 21452.20313,  
                InstanceId = [[Client1_253]],  
                Class = [[Position]],  
                z = 75.125
              },  
              Angle = -0.828125,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_248]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_246]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              Position = {
                y = -1124.9375,  
                x = 21446.25,  
                InstanceId = [[Client1_249]],  
                Class = [[Position]],  
                z = 75.328125
              },  
              Angle = -0.828125,  
              Base = [[palette.entities.creatures.ckeif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_361]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Mira Guards]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_360]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_359]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_281]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 8,  
              GabaritHeight = 2,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_279]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_364]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_365]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_150]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 6699566,  
              Angle = 1.46875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701358,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Mira Desert Guard]],  
              Position = {
                y = -1362.671875,  
                x = 21573.4375,  
                InstanceId = [[Client1_282]],  
                Class = [[Position]],  
                z = 110.28125
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 11
            },  
            {
              InstanceId = [[Client1_321]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_319]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Mossy Colossus]],  
              Position = {
                y = -1357.03125,  
                x = 21584.28125,  
                InstanceId = [[Client1_322]],  
                Class = [[Position]],  
                z = 113.0625
              },  
              Angle = 3.015625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_313]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_311]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Hairy Colossus]],  
              Position = {
                y = -1361.046875,  
                x = 21581.48438,  
                InstanceId = [[Client1_314]],  
                Class = [[Position]],  
                z = 112.796875
              },  
              Angle = 2.453125,  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_317]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_315]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Horned Faun]],  
              Position = {
                y = -1359.359375,  
                x = 21575.48438,  
                InstanceId = [[Client1_318]],  
                Class = [[Position]],  
                z = 110.96875
              },  
              Angle = 2.390625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_325]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_323]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Obscure Faun]],  
              Position = {
                y = -1358.296875,  
                x = 21574.625,  
                InstanceId = [[Client1_326]],  
                Class = [[Position]],  
                z = 110.65625
              },  
              Angle = 3.015625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_285]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_283]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Karavan Emissary]],  
              Position = {
                y = -1362.65625,  
                x = 21574.71875,  
                InstanceId = [[Client1_286]],  
                Class = [[Position]],  
                z = 110.734375
              },  
              Angle = 1.5,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_357]],  
              ActivitiesId = {
              },  
              HairType = 6722094,  
              TrouserColor = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 4,  
              HandsModel = 6721326,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 3,  
              HairColor = 2,  
              EyesColor = 6,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 9,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_355]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 6720814,  
              Angle = 0.78125,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6723118,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 2,  
              JacketModel = 6723374,  
              WeaponRightHand = 6766894,  
              ArmColor = 1,  
              Name = [[Mira Lakeland Guard]],  
              Position = {
                y = -1359.828125,  
                x = 21568.84375,  
                InstanceId = [[Client1_358]],  
                Class = [[Position]],  
                z = 108.328125
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 5,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_349]],  
              ActivitiesId = {
              },  
              HairType = 6711086,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 6710574,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 3,  
              EyesColor = 6,  
              TrouserModel = 6711598,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_347]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 6710318,  
              Angle = 2.296875,  
              Base = [[palette.entities.npcs.guards.m_guard_245]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_matis_female.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6712110,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 6712878,  
              WeaponRightHand = 6761774,  
              ArmColor = 4,  
              Name = [[Mira Forest Guard]],  
              Position = {
                y = -1360.328125,  
                x = 21571.875,  
                InstanceId = [[Client1_350]],  
                Class = [[Position]],  
                z = 109.65625
              },  
              Sex = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 0,  
              Tattoo = 22
            },  
            {
              InstanceId = [[Client1_337]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_335]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Karavan Guard]],  
              Position = {
                y = -1367.703125,  
                x = 21573.82813,  
                InstanceId = [[Client1_338]],  
                Class = [[Position]],  
                z = 110.609375
              },  
              Angle = 1.734375,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_353]],  
              ActivitiesId = {
              },  
              HairType = 6732590,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 1,  
              HandsModel = 6732078,  
              FeetColor = 5,  
              GabaritBreastSize = 8,  
              GabaritHeight = 5,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 6733102,  
              GabaritLegsWidth = 1,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_351]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 6731822,  
              Angle = 1.96875,  
              Base = [[palette.entities.npcs.guards.z_guard_245]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6733614,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 3,  
              JacketModel = 6734382,  
              WeaponRightHand = 6770734,  
              ArmColor = 4,  
              Name = [[Mira Jungle Guard]],  
              Position = {
                y = -1359.203125,  
                x = 21570.65625,  
                InstanceId = [[Client1_354]],  
                Class = [[Position]],  
                z = 109.125
              },  
              Sex = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 1,  
              Tattoo = 20
            },  
            {
              InstanceId = [[Client1_297]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_295]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Voice of Jena]],  
              Position = {
                y = -1360.078125,  
                x = 21573.96875,  
                InstanceId = [[Client1_298]],  
                Class = [[Position]],  
                z = 110.40625
              },  
              Angle = 1.5,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_289]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_287]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Karavan Guard]],  
              Position = {
                y = -1361.46875,  
                x = 21574.40625,  
                InstanceId = [[Client1_290]],  
                Class = [[Position]],  
                z = 110.609375
              },  
              Angle = 1.5,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_333]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_331]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Karavan Emissary]],  
              Position = {
                y = -1368.25,  
                x = 21575.39063,  
                InstanceId = [[Client1_334]],  
                Class = [[Position]],  
                z = 111.203125
              },  
              Angle = 1.734375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_293]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_291]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Karavan Protector]],  
              Position = {
                y = -1360.65625,  
                x = 21574.0625,  
                InstanceId = [[Client1_294]],  
                Class = [[Position]],  
                z = 110.46875
              },  
              Angle = 1.5,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_329]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_327]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Spectral Genius]],  
              Position = {
                y = -1357.96875,  
                x = 21578.71875,  
                InstanceId = [[Client1_330]],  
                Class = [[Position]],  
                z = 111.890625
              },  
              Angle = 3.015625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_345]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_343]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Voice of Jena]],  
              Position = {
                y = -1365.703125,  
                x = 21574.89063,  
                InstanceId = [[Client1_346]],  
                Class = [[Position]],  
                z = 110.890625
              },  
              Angle = 1.734375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_341]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_339]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Karavan Protector]],  
              Position = {
                y = -1366.421875,  
                x = 21573.32813,  
                InstanceId = [[Client1_342]],  
                Class = [[Position]],  
                z = 110.328125
              },  
              Angle = 1.734375,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_309]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_307]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Diaphanous Salamander]],  
              Position = {
                y = -1361.34375,  
                x = 21575.76563,  
                InstanceId = [[Client1_310]],  
                Class = [[Position]],  
                z = 111.078125
              },  
              Angle = 2,  
              Base = [[palette.entities.npcs.kami.kami_preacher_3_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_301]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_299]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Ardent Genius]],  
              Position = {
                y = -1363.171875,  
                x = 21576.53125,  
                InstanceId = [[Client1_302]],  
                Class = [[Position]],  
                z = 111.375
              },  
              Angle = 2,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_305]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_303]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mira Burning Salamander]],  
              Position = {
                y = -1362.625,  
                x = 21576.5,  
                InstanceId = [[Client1_306]],  
                Class = [[Position]],  
                z = 111.34375
              },  
              Angle = 2,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_f]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Name = [[Permanent]],  
      Title = [[Permanent]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_131]]
  }
}