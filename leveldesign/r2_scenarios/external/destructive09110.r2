scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_2181]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_2183]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    LogicEntityReaction = 0,  
    EventType = 0,  
    Behavior = 0,  
    Position = 0,  
    Npc = 0,  
    ActionType = 0,  
    MapDescription = 0,  
    ActionStep = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_2184]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2185]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client1_2203]],  
      WeatherValue = 0,  
      Title = [[Act 1]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2204]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      States = {
      }
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_2186]],  
      ActivitiesIds = {
        [[Client1_2192]],  
        [[Client1_2214]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2209]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2210]],  
                x = 26947.89063,  
                y = -2020.484375,  
                z = -4.109375
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2207]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2214]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2215]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_2211]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_2211]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_2212]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_2213]],  
                            Emote = [[]],  
                            Who = [[Client1_2209]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2205]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2217]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2219]],  
                        Entity = r2.RefId([[Client1_2209]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2218]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_2214]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2216]],  
                      Type = [[end of chat step]],  
                      Value = r2.RefId([[Client1_2212]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2220]],  
                    LogicEntityAction = [[Client1_2217]],  
                    ActionStep = [[Client1_2219]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
                [[Client1_2214]]
              },  
              InheritPos = 1,  
              Name = [[voice of Jena 1]]
            }
          },  
          InstanceId = [[Client1_2187]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act 2]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_2197]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_2205]],  
        Class = [[TextManagerEntry]],  
        Text = [[LOOP]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_2206]],  
        Class = [[TextManagerEntry]],  
        Text = [[LOOP]]
      }
    },  
    InstanceId = [[Client1_2182]]
  }
}