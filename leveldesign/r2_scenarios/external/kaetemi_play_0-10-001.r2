scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_130]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_132]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    ActivityStep = 1,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 20,  
      Behavior = {
        InstanceId = [[Client1_133]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Permanent]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_134]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_141]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]],  
              Position = {
                y = -1363.609375,  
                x = 21571.10938,  
                InstanceId = [[Client1_144]],  
                Class = [[Position]],  
                z = 109.3125
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_145]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]],  
              Position = {
                y = -1362.328125,  
                x = 21568.9375,  
                InstanceId = [[Client1_148]],  
                Class = [[Position]],  
                z = 108.375
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Spawn Region]],  
              InstanceId = [[Client1_150]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_149]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_152]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1329.953125,  
                    x = 21543.64063,  
                    InstanceId = [[Client1_153]],  
                    Class = [[Position]],  
                    z = 95.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_155]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1342.625,  
                    x = 21597.29688,  
                    InstanceId = [[Client1_156]],  
                    Class = [[Position]],  
                    z = 110.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_158]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1402.625,  
                    x = 21601.54688,  
                    InstanceId = [[Client1_159]],  
                    Class = [[Position]],  
                    z = 123.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1387.296875,  
                    x = 21528.46875,  
                    InstanceId = [[Client1_162]],  
                    Class = [[Position]],  
                    z = 102.703125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_163]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan mirador 1]],  
              Position = {
                y = -1376.65625,  
                x = 21579.59375,  
                InstanceId = [[Client1_166]],  
                Class = [[Position]],  
                z = 114.046875
              },  
              Angle = 2.359375,  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_167]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              Position = {
                y = -1376.65625,  
                x = 21579.57813,  
                InstanceId = [[Client1_170]],  
                Class = [[Position]],  
                z = 114.015625
              },  
              Angle = 2.359375,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_181]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_179]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -1359.828125,  
                x = 21557.07813,  
                InstanceId = [[Client1_182]],  
                Class = [[Position]],  
                z = 103.5625
              },  
              Angle = 2.109375,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaetin Region]],  
              InstanceId = [[Client1_188]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_187]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_190]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1134.828125,  
                    x = 21467.35938,  
                    InstanceId = [[Client1_191]],  
                    Class = [[Position]],  
                    z = 73.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_193]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1075.375,  
                    x = 21509.28125,  
                    InstanceId = [[Client1_194]],  
                    Class = [[Position]],  
                    z = 73.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_196]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1008.75,  
                    x = 21448.5625,  
                    InstanceId = [[Client1_197]],  
                    Class = [[Position]],  
                    z = 61.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_199]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1038.078125,  
                    x = 21344.5625,  
                    InstanceId = [[Client1_200]],  
                    Class = [[Position]],  
                    z = 61.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_202]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1121.53125,  
                    x = 21356.03125,  
                    InstanceId = [[Client1_203]],  
                    Class = [[Position]],  
                    z = 62.375
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_136]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_268]],  
          Name = [[Group 1]],  
          Components = {
            {
              InstanceId = [[Client1_206]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_204]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_273]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_274]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_188]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kaeteketh]],  
              Position = {
                y = -1080.203125,  
                x = 21432.40625,  
                InstanceId = [[Client1_207]],  
                Class = [[Position]],  
                z = 66.296875
              },  
              Angle = 5.39306736,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_216]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_217]],  
                x = 21444.46875,  
                y = -1087.546875,  
                z = 69.359375
              },  
              Angle = -0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_214]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_212]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_213]],  
                x = 21434.9375,  
                y = -1092.59375,  
                z = 68.46875
              },  
              Angle = -0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_210]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_228]],  
              Base = [[palette.entities.creatures.ckeif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_229]],  
                x = 21455.8125,  
                y = -1097.765625,  
                z = 73.15625
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_226]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_224]],  
              Base = [[palette.entities.creatures.ckeif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_225]],  
                x = 21448.35938,  
                y = -1103.296875,  
                z = 72.96875
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_222]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_220]],  
              Base = [[palette.entities.creatures.ckeif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_221]],  
                x = 21442.01563,  
                y = -1108.859375,  
                z = 73.421875
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_218]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_244]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_245]],  
                x = 21462.48438,  
                y = -1104.25,  
                z = 74.5625
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_242]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_240]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_241]],  
                x = 21456.96875,  
                y = -1109.296875,  
                z = 74.578125
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_238]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_236]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_237]],  
                x = 21450.70313,  
                y = -1113.15625,  
                z = 74.578125
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_234]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_232]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_233]],  
                x = 21444.76563,  
                y = -1117.578125,  
                z = 74.875
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_230]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_264]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_265]],  
                x = 21470.40625,  
                y = -1108.15625,  
                z = 75.375
              },  
              Angle = -1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_262]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_260]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_261]],  
                x = 21465.07813,  
                y = -1114.484375,  
                z = 75.296875
              },  
              Angle = -1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_258]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_256]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_257]],  
                x = 21457.82813,  
                y = -1117.90625,  
                z = 75.09375
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_254]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_252]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_253]],  
                x = 21452.20313,  
                y = -1122.125,  
                z = 75.125
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_250]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_248]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_249]],  
                x = 21446.25,  
                y = -1124.9375,  
                z = 75.328125
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_246]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_267]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_266]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_135]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_131]],  
    Texts = {
    }
  }
}