scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_2181]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_2183]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    ChatAction = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    Position = 0,  
    Behavior = 0,  
    ChatStep = 0,  
    LogicEntityReaction = 0,  
    Npc = 0,  
    ActionType = 0,  
    ActionStep = 0,  
    EventType = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2185]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_2184]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client1_2203]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2204]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act 2]],  
      ActivitiesIds = {
        [[Client1_2192]],  
        [[Client1_2214]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_2209]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2207]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_2217]],  
                    Name = [[]],  
                    InstanceId = [[Client1_2220]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_2219]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2225]],  
                    LogicEntityAction = [[Client1_2222]],  
                    ActionStep = [[Client1_2224]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2230]],  
                    LogicEntityAction = [[Client1_2227]],  
                    ActionStep = [[Client1_2229]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_2211]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_2212]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_2213]],  
                            Who = [[Client1_2209]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2205]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2217]],  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_2216]],  
                      Value = r2.RefId([[Client1_2212]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_2218]],  
                          Value = r2.RefId([[Client1_2214]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_2219]],  
                        Entity = r2.RefId([[Client1_2209]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2222]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2224]],  
                        Entity = r2.RefId([[Client1_2209]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2223]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_2214]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2221]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_2215]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2227]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2229]],  
                        Entity = r2.RefId([[Client1_2209]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2228]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_2214]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2226]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_2211]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_2214]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_2215]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_2211]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[voice of Jena 1]],  
              Position = {
                y = -2020.484375,  
                x = 26947.89063,  
                InstanceId = [[Client1_2210]],  
                Class = [[Position]],  
                z = -4.109375
              },  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_b]],  
              ActivitiesId = {
                [[Client1_2214]]
              }
            }
          },  
          InstanceId = [[Client1_2187]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_2186]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2182]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_2197]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_2205]],  
        Class = [[TextManagerEntry]],  
        Text = [[LOOP]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_2206]],  
        Class = [[TextManagerEntry]],  
        Text = [[LOOP]]
      }
    }
  }
}