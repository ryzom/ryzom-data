scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    LogicEntityReaction = 0,  
    MapDescription = 0,  
    Region = 0,  
    ActivityStep = 0,  
    ActionStep = 0,  
    Road = 0,  
    EventType = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Behavior = 0,  
    Position = 0,  
    Npc = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    ChatAction = 0
  },  
  Acts = {
    {
      Cost = 41,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client1_184]],  
        [[Client1_186]],  
        [[Client1_188]],  
        [[Client2_92]],  
        [[Client2_94]],  
        [[Client2_96]],  
        [[Client1_230]],  
        [[Client2_512]],  
        [[Client2_510]],  
        [[Client1_376]]
      },  
      Title = [[Act 0]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_14]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI camp fire 1]],  
              Position = {
                y = -1976.75,  
                x = 22497.82813,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 38.0625
              },  
              Angle = 1.6875,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI hut 1]],  
              Position = {
                y = -1980.171875,  
                x = 22484.51563,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 38.25
              },  
              Angle = -0.203125,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_352]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_353]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_354]],  
                            Who = [[Client1_42]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_50]]),  
                            Says = [[Client1_357]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6702382,  
              Angle = -1.390625,  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[Fihl]],  
              Position = {
                y = -1975.75,  
                x = 22497.125,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = 38.140625
              },  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              ArmModel = 5606190,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_46]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 5,  
              HandsModel = 6702894,  
              FeetColor = 5,  
              GabaritBreastSize = 8,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 4,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_346]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_347]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_348]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_50]]),  
                            Says = [[Client1_349]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[Chat2]],  
                    InstanceId = [[Client1_351]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_363]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_364]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_280]]),  
                            Says = [[Client1_365]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5605422,  
              Angle = 0.46875,  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[Dae]],  
              Position = {
                y = -1977.859375,  
                x = 22496.76563,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 38.125
              },  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              ArmModel = 5606190,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_50]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 3,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 2,  
              GabaritHeight = 2,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 3,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_337]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_340]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_341]],  
                            Who = [[Client1_50]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_344]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 6,  
                        InstanceId = [[Client1_342]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_343]],  
                            Who = [[Client1_50]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_345]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_360]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[]],  
                      InstanceId = [[Client1_359]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Angle = -0.234375,  
              Base = [[palette.entities.npcs.civils.f_civil_70]],  
              Tattoo = 12,  
              MorphTarget3 = 1,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              JacketModel = 6704430,  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Dahv]],  
              Position = {
                y = -1976.578125,  
                x = 22496.46875,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 38.203125
              },  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_58]],  
              ActivitiesId = {
              },  
              HairType = 8494,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6702382,  
              Angle = -0.265625,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              JacketModel = 6706990,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Dim]],  
              Position = {
                y = -1980.8125,  
                x = 22487.48438,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = 38.046875
              },  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              ArmModel = 6706734,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_101]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_99]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Angle = 0.1875,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              Position = {
                y = -2010.53125,  
                x = 22551.15625,  
                InstanceId = [[Client1_102]],  
                Class = [[Position]],  
                z = 31.046875
              },  
              Sex = 1,  
              WeaponRightHand = 6755886,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[Gorge Guard]],  
              BotAttackable = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_105]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_103]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Angle = 0.296875,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              Position = {
                y = -1985.8125,  
                x = 22551.82813,  
                InstanceId = [[Client1_106]],  
                Class = [[Position]],  
                z = 31.71875
              },  
              Sex = 1,  
              WeaponRightHand = 6755374,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Gorge Guard]],  
              BotAttackable = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_e4.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client2_11]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_9]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_512]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client2_513]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_482]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                y = -1987.0625,  
                x = 22594.75,  
                InstanceId = [[Client2_12]],  
                Class = [[Position]],  
                z = 25.96875
              },  
              Angle = 2.265625,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
                [[Client2_512]]
              }
            },  
            {
              InstanceId = [[Client2_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_13]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_510]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client2_511]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_482]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scowling Gingo]],  
              Position = {
                y = -1971.078125,  
                x = 22612.1875,  
                InstanceId = [[Client2_16]],  
                Class = [[Position]],  
                z = 22.875
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.creatures.ccadb3]],  
              ActivitiesId = {
                [[Client2_510]]
              }
            },  
            {
              InstanceId = [[Client4_3]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client4_1]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[The Tunnel]],  
              Position = {
                y = -1855.953125,  
                x = 22648.84375,  
                InstanceId = [[Client4_4]],  
                Class = [[Position]],  
                z = 16.34375
              },  
              Angle = -1.390625,  
              Base = [[palette.entities.botobjects.fx_tr_pollen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_121]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_119]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI bag 2 1]],  
              Position = {
                y = -1975.828125,  
                x = 22487.625,  
                InstanceId = [[Client1_122]],  
                Class = [[Position]],  
                z = 38.265625
              },  
              Angle = 2.25,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_129]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_127]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1990.265625,  
                x = 22494.79688,  
                InstanceId = [[Client1_130]],  
                Class = [[Position]],  
                z = 37.421875
              },  
              Angle = 0.234375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_133]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_131]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1999.375,  
                x = 22498.125,  
                InstanceId = [[Client1_134]],  
                Class = [[Position]],  
                z = 35.84375
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_7]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_5]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1973.296875,  
                x = 22482.14063,  
                InstanceId = [[Client3_8]],  
                Class = [[Position]],  
                z = 39.125
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_39]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_37]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Building]],  
              Position = {
                y = -1876,  
                x = 22275.60938,  
                InstanceId = [[Client2_40]],  
                Class = [[Position]],  
                z = -22.78125
              },  
              Angle = 0.640625,  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_51]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_49]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Tower This Way]],  
              Position = {
                y = -1806.265625,  
                x = 22450.40625,  
                InstanceId = [[Client2_52]],  
                Class = [[Position]],  
                z = 0.625
              },  
              Angle = 0.09375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client2_59]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_57]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Angle = -0.1875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Kaeno Tower Guard]],  
              Position = {
                y = -1808.625,  
                x = 22460.625,  
                InstanceId = [[Client2_60]],  
                Class = [[Position]],  
                z = 1.09375
              },  
              Sheet = [[ring_guard_melee_tank_slash_f1.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client1_141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_139]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI mekbar 1 - ROOMKILLER]],  
              Position = {
                y = -1957.5,  
                x = 22496.65625,  
                InstanceId = [[Client1_142]],  
                Class = [[Position]],  
                z = 36.890625
              },  
              Angle = 0.203125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_143]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI mekbar 2 - USES SPACE]],  
              Position = {
                y = -1961.28125,  
                x = 22497.625,  
                InstanceId = [[Client1_146]],  
                Class = [[Position]],  
                z = 37.09375
              },  
              Angle = -2.859375,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_63]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_61]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Building Square]],  
              Position = {
                y = -1804.921875,  
                x = 22355.82813,  
                InstanceId = [[Client2_64]],  
                Class = [[Position]],  
                z = -11.828125
              },  
              Angle = -1.359375,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_147]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1961.09375,  
                x = 22495.9375,  
                InstanceId = [[Client1_150]],  
                Class = [[Position]],  
                z = 37.078125
              },  
              Angle = -2.640625,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1958.3125,  
                x = 22494.59375,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = 36.765625
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Region]],  
              InstanceId = [[Client2_66]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_68]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1809.359375,  
                    x = 22354.60938,  
                    InstanceId = [[Client2_69]],  
                    Class = [[Position]],  
                    z = -12.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_71]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1838.515625,  
                    x = 22404.71875,  
                    InstanceId = [[Client2_72]],  
                    Class = [[Position]],  
                    z = -9.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_74]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1861.5,  
                    x = 22308.78125,  
                    InstanceId = [[Client2_75]],  
                    Class = [[Position]],  
                    z = -16.09375
                  }
                }
              },  
              Position = {
                y = 0.3125,  
                x = -0.96875,  
                InstanceId = [[Client2_65]],  
                Class = [[Position]],  
                z = -0.15625
              }
            },  
            {
              InstanceId = [[Client1_157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_155]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_186]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_187]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1962.3125,  
                x = 22527.07813,  
                InstanceId = [[Client1_158]],  
                Class = [[Position]],  
                z = 32.09375
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_186]]
              }
            },  
            {
              InstanceId = [[Client1_161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_159]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_188]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_189]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1960.59375,  
                x = 22530.4375,  
                InstanceId = [[Client1_162]],  
                Class = [[Position]],  
                z = 31.984375
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_188]]
              }
            },  
            {
              InstanceId = [[Client1_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_163]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_184]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_185]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1958.96875,  
                x = 22529.23438,  
                InstanceId = [[Client1_166]],  
                Class = [[Position]],  
                z = 32.046875
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_184]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Raspal Region]],  
              InstanceId = [[Client1_168]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1953.609375,  
                    x = 22517.57813,  
                    InstanceId = [[Client1_171]],  
                    Class = [[Position]],  
                    z = 32.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_173]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1969.796875,  
                    x = 22523.5,  
                    InstanceId = [[Client1_174]],  
                    Class = [[Position]],  
                    z = 32.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_176]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1962.265625,  
                    x = 22541.09375,  
                    InstanceId = [[Client1_177]],  
                    Class = [[Position]],  
                    z = 31.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_179]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1952.75,  
                    x = 22536.78125,  
                    InstanceId = [[Client1_180]],  
                    Class = [[Position]],  
                    z = 31.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_182]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1952.640625,  
                    x = 22526.15625,  
                    InstanceId = [[Client1_183]],  
                    Class = [[Position]],  
                    z = 32.34375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_167]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_82]],  
              ActivitiesId = {
                [[Client2_92]]
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_80]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_92]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_93]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 10,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Kaethus]],  
              Position = {
                y = -1824.65625,  
                x = 22365.84375,  
                InstanceId = [[Client2_83]],  
                Class = [[Position]],  
                z = -7.3125
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_86]],  
              ActivitiesId = {
                [[Client2_96]]
              },  
              HairType = 5621550,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_84]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_96]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_374]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[Kaethon]],  
              Position = {
                y = -1829.953125,  
                x = 22369.46875,  
                InstanceId = [[Client2_87]],  
                Class = [[Position]],  
                z = -6.859375
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_90]],  
              ActivitiesId = {
                [[Client2_94]]
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 8,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 2,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_88]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_94]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_375]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 13,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Kaethi]],  
              Position = {
                y = -1827.15625,  
                x = 22362.5,  
                InstanceId = [[Client2_91]],  
                Class = [[Position]],  
                z = -7.34375
              },  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client2_108]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              Notes = [[This character takes care of the resources that the destroyed village Kaeno needs.]],  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_106]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Angle = -0.28125,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Kaeno Resource Manager]],  
              Position = {
                y = -1835.3125,  
                x = 22321.07813,  
                InstanceId = [[Client2_109]],  
                Class = [[Position]],  
                z = -12.015625
              },  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client2_112]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_110]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Resource Building]],  
              Position = {
                y = -1835.546875,  
                x = 22318.67188,  
                InstanceId = [[Client2_113]],  
                Class = [[Position]],  
                z = -12.390625
              },  
              Angle = -0.21875,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_116]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_114]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[-KAE Blocks so you]],  
              Position = {
                y = -1783.796875,  
                x = 22656.04688,  
                InstanceId = [[Client2_117]],  
                Class = [[Position]],  
                z = 13.21875
              },  
              Angle = -2.6875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_120]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_118]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[-KAE To Other Side]],  
              Position = {
                y = -1793.828125,  
                x = 22656.0625,  
                InstanceId = [[Client2_121]],  
                Class = [[Position]],  
                z = 13.5625
              },  
              Angle = 0.390625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_132]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_130]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[-KAE Dont Misrun]],  
              Position = {
                y = -1892.453125,  
                x = 22679.875,  
                InstanceId = [[Client2_133]],  
                Class = [[Position]],  
                z = 16.4375
              },  
              Angle = -2.6875,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_144]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_142]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Totem]],  
              Position = {
                y = -1834.109375,  
                x = 22537.17188,  
                InstanceId = [[Client2_145]],  
                Class = [[Position]],  
                z = 4.96875
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.totem_bird]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Yubo Region]],  
              InstanceId = [[Client1_214]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_213]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_216]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2005.296875,  
                    x = 22514.92188,  
                    InstanceId = [[Client1_217]],  
                    Class = [[Position]],  
                    z = 31.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_219]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1986.8125,  
                    x = 22506.90625,  
                    InstanceId = [[Client1_220]],  
                    Class = [[Position]],  
                    z = 36.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_222]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1985.78125,  
                    x = 22533.75,  
                    InstanceId = [[Client1_223]],  
                    Class = [[Position]],  
                    z = 32.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_225]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2011.84375,  
                    x = 22534.125,  
                    InstanceId = [[Client1_226]],  
                    Class = [[Position]],  
                    z = 32.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_228]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2020.296875,  
                    x = 22520.67188,  
                    InstanceId = [[Client1_229]],  
                    Class = [[Position]],  
                    z = 32.84375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Goari Region]],  
              InstanceId = [[Client2_159]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1786.953125,  
                    x = 22509.51563,  
                    InstanceId = [[Client2_162]],  
                    Class = [[Position]],  
                    z = 5.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_164]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1806.578125,  
                    x = 22497.70313,  
                    InstanceId = [[Client2_165]],  
                    Class = [[Position]],  
                    z = 5.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_167]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1805.515625,  
                    x = 22524.42188,  
                    InstanceId = [[Client2_168]],  
                    Class = [[Position]],  
                    z = 9.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1784.84375,  
                    x = 22530.84375,  
                    InstanceId = [[Client2_171]],  
                    Class = [[Position]],  
                    z = 8.328125
                  }
                }
              },  
              Position = {
                y = -2.09375,  
                x = -2.1875,  
                InstanceId = [[Client2_158]],  
                Class = [[Position]],  
                z = -3.8125
              }
            },  
            {
              InstanceId = [[Client2_184]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_182]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              Position = {
                y = -1814.75,  
                x = 22463.85938,  
                InstanceId = [[Client2_185]],  
                Class = [[Position]],  
                z = 1.78125
              },  
              Angle = 1.5625,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime to Kaeno]],  
              InstanceId = [[Client1_283]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_282]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_285]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1987.40625,  
                    x = 22501,  
                    InstanceId = [[Client1_286]],  
                    Class = [[Position]],  
                    z = 37.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_288]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1993.265625,  
                    x = 22577.40625,  
                    InstanceId = [[Client1_289]],  
                    Class = [[Position]],  
                    z = 27.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_291]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1960.734375,  
                    x = 22621.92188,  
                    InstanceId = [[Client1_292]],  
                    Class = [[Position]],  
                    z = 23.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_294]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1854.234375,  
                    x = 22653.51563,  
                    InstanceId = [[Client1_295]],  
                    Class = [[Position]],  
                    z = 15.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_297]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1813.578125,  
                    x = 22611.90625,  
                    InstanceId = [[Client1_298]],  
                    Class = [[Position]],  
                    z = 9.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_300]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1838.359375,  
                    x = 22552.375,  
                    InstanceId = [[Client1_301]],  
                    Class = [[Position]],  
                    z = 4.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_303]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1801.96875,  
                    x = 22521.51563,  
                    InstanceId = [[Client1_304]],  
                    Class = [[Position]],  
                    z = 4.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_306]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1805.78125,  
                    x = 22486.5625,  
                    InstanceId = [[Client1_307]],  
                    Class = [[Position]],  
                    z = 1.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_309]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1874.234375,  
                    x = 22486.40625,  
                    InstanceId = [[Client1_310]],  
                    Class = [[Position]],  
                    z = 0.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_312]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1874.03125,  
                    x = 22432.10938,  
                    InstanceId = [[Client1_313]],  
                    Class = [[Position]],  
                    z = -3.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_315]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1831.140625,  
                    x = 22378.46875,  
                    InstanceId = [[Client1_316]],  
                    Class = [[Position]],  
                    z = -7.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_318]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1835.859375,  
                    x = 22325.10938,  
                    InstanceId = [[Client1_319]],  
                    Class = [[Position]],  
                    z = -11.84375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Tunnel Region]],  
              InstanceId = [[Client2_225]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1878.390625,  
                    x = 22645.875,  
                    InstanceId = [[Client2_228]],  
                    Class = [[Position]],  
                    z = 19.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1875.3125,  
                    x = 22656.0625,  
                    InstanceId = [[Client2_231]],  
                    Class = [[Position]],  
                    z = 19.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1843.90625,  
                    x = 22674.07813,  
                    InstanceId = [[Client2_234]],  
                    Class = [[Position]],  
                    z = 14.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1821,  
                    x = 22634.26563,  
                    InstanceId = [[Client2_237]],  
                    Class = [[Position]],  
                    z = 14.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_239]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1826.15625,  
                    x = 22628.46875,  
                    InstanceId = [[Client2_240]],  
                    Class = [[Position]],  
                    z = 13.109375
                  }
                }
              },  
              Position = {
                y = -1.5625,  
                x = -2,  
                InstanceId = [[Client2_224]],  
                Class = [[Position]],  
                z = -0.59375
              }
            },  
            {
              InstanceId = [[Client2_335]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_333]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[KAE Message Tower]],  
              Position = {
                y = -1870.875,  
                x = 22535.5625,  
                InstanceId = [[Client2_336]],  
                Class = [[Position]],  
                z = 7.78125
              },  
              Angle = 1.296875,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Messenger To Guard]],  
              InstanceId = [[Client2_346]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_348]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1879.59375,  
                    x = 22541.5,  
                    InstanceId = [[Client2_349]],  
                    Class = [[Position]],  
                    z = 9.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_351]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1866,  
                    x = 22545,  
                    InstanceId = [[Client2_352]],  
                    Class = [[Position]],  
                    z = 8.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_354]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1832.515625,  
                    x = 22542,  
                    InstanceId = [[Client2_355]],  
                    Class = [[Position]],  
                    z = 4.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_357]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1804.71875,  
                    x = 22521.5,  
                    InstanceId = [[Client2_358]],  
                    Class = [[Position]],  
                    z = 5.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_360]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1808.890625,  
                    x = 22462.625,  
                    InstanceId = [[Client2_361]],  
                    Class = [[Position]],  
                    z = 0.71875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_345]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Tower To Manager]],  
              InstanceId = [[Client2_418]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_417]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_420]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1851.78125,  
                    x = 22481.09375,  
                    InstanceId = [[Client2_421]],  
                    Class = [[Position]],  
                    z = 1.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_423]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1873.859375,  
                    x = 22475.53125,  
                    InstanceId = [[Client2_424]],  
                    Class = [[Position]],  
                    z = 0.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_426]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1873.609375,  
                    x = 22445.09375,  
                    InstanceId = [[Client2_427]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_429]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1854.796875,  
                    x = 22401.9375,  
                    InstanceId = [[Client2_430]],  
                    Class = [[Position]],  
                    z = -6.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_432]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1836.203125,  
                    x = 22325.14063,  
                    InstanceId = [[Client2_433]],  
                    Class = [[Position]],  
                    z = -11.90625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Izam Region]],  
              InstanceId = [[Client2_444]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_446]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1809.390625,  
                    x = 22491.8125,  
                    InstanceId = [[Client2_447]],  
                    Class = [[Position]],  
                    z = 1.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_449]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1819.28125,  
                    x = 22488.89063,  
                    InstanceId = [[Client2_450]],  
                    Class = [[Position]],  
                    z = 3.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_452]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1832.921875,  
                    x = 22492.85938,  
                    InstanceId = [[Client2_453]],  
                    Class = [[Position]],  
                    z = 1.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_455]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1848.671875,  
                    x = 22467.26563,  
                    InstanceId = [[Client2_456]],  
                    Class = [[Position]],  
                    z = 1.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_458]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1817.328125,  
                    x = 22462.57813,  
                    InstanceId = [[Client2_459]],  
                    Class = [[Position]],  
                    z = 2.40625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_443]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Gingo Region]],  
              InstanceId = [[Client2_482]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_484]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1966.390625,  
                    x = 22608.53125,  
                    InstanceId = [[Client2_485]],  
                    Class = [[Position]],  
                    z = 24.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_487]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1973.25,  
                    x = 22632.95313,  
                    InstanceId = [[Client2_488]],  
                    Class = [[Position]],  
                    z = 21.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_490]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1984.140625,  
                    x = 22625.17188,  
                    InstanceId = [[Client2_491]],  
                    Class = [[Position]],  
                    z = 22.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_493]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1988.25,  
                    x = 22607.20313,  
                    InstanceId = [[Client2_494]],  
                    Class = [[Position]],  
                    z = 23.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_496]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1994.4375,  
                    x = 22603.0625,  
                    InstanceId = [[Client2_497]],  
                    Class = [[Position]],  
                    z = 24.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_499]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2007.828125,  
                    x = 22603.1875,  
                    InstanceId = [[Client2_500]],  
                    Class = [[Position]],  
                    z = 25.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_502]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2008.453125,  
                    x = 22594.39063,  
                    InstanceId = [[Client2_503]],  
                    Class = [[Position]],  
                    z = 25.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_505]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1971.796875,  
                    x = 22570.01563,  
                    InstanceId = [[Client2_506]],  
                    Class = [[Position]],  
                    z = 28.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_508]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1973.390625,  
                    x = 22602.9375,  
                    InstanceId = [[Client2_509]],  
                    Class = [[Position]],  
                    z = 23.109375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_481]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_438]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_439]],  
                x = 22283.67188,  
                y = -1871.34375,  
                z = -21.34375
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_436]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_442]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_443]],  
                x = 22281.76563,  
                y = -1868.640625,  
                z = -21.0625
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_440]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_446]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_447]],  
                x = 22278.79688,  
                y = -1867.4375,  
                z = -20.875
              },  
              Angle = 0.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_444]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_450]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_451]],  
                x = 22273.85938,  
                y = -1867.5625,  
                z = -21.078125
              },  
              Angle = 0.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_448]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_454]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_455]],  
                x = 22322.64063,  
                y = -1832.03125,  
                z = -11.15625
              },  
              Angle = -0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_452]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 5]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_458]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_459]],  
                x = 22459.73438,  
                y = -1811.390625,  
                z = 1.796875
              },  
              Angle = -0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_456]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 6]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_462]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_463]],  
                x = 22540.64063,  
                y = -1865.34375,  
                z = 7.546875
              },  
              Angle = 1.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_460]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 7]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_466]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_467]],  
                x = 22650.54688,  
                y = -1789.5,  
                z = 12
              },  
              Angle = -2.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_464]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_470]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_471]],  
                x = 22646.85938,  
                y = -1780.265625,  
                z = 12.453125
              },  
              Angle = -2.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_468]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_474]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_475]],  
                x = 22654.28125,  
                y = -1798.171875,  
                z = 14.78125
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_472]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_478]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_479]],  
                x = 22673.67188,  
                y = -1888.046875,  
                z = 17.65625
              },  
              Angle = -2.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_476]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_482]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_483]],  
                x = 22673.65625,  
                y = -1894.625,  
                z = 16.640625
              },  
              Angle = -2.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_480]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fire 5]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_486]],  
              Base = [[palette.entities.botobjects.fx_ju_solbirthd]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_487]],  
                x = 22602.71875,  
                y = -1980.109375,  
                z = 22.75
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_484]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[colorful pollen 1]]
            }
          },  
          InstanceId = [[Client1_5]]
        },  
        {
          InstanceId = [[Client1_212]],  
          ActivitiesId = {
            [[Client1_374]],  
            [[Client1_375]]
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_211]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_196]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_194]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_376]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_377]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_214]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Sprightly Yubo]],  
              Position = {
                y = -2004.21875,  
                x = 22522.73438,  
                InstanceId = [[Client1_197]],  
                Class = [[Position]],  
                z = 33.09375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb4]],  
              ActivitiesId = {
                [[Client1_230]],  
                [[Client1_376]]
              }
            },  
            {
              InstanceId = [[Client1_192]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_190]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_230]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_231]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Timorous Yubo]],  
              Position = {
                y = -2003.796875,  
                x = 22521.53125,  
                InstanceId = [[Client1_193]],  
                Class = [[Position]],  
                z = 33.03125
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_204]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_202]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              Position = {
                y = -2005.390625,  
                x = 22521.90625,  
                InstanceId = [[Client1_205]],  
                Class = [[Position]],  
                z = 32.984375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_200]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_198]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              Position = {
                y = -2002.890625,  
                x = 22521.9375,  
                InstanceId = [[Client1_201]],  
                Class = [[Position]],  
                z = 33.109375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.chddb3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_210]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 9,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client2_172]],  
        [[Client2_178]],  
        [[Client2_180]],  
        [[Client2_190]],  
        [[Client2_196]],  
        [[Client1_272]],  
        [[Client1_274]],  
        [[Client1_428]],  
        [[Client1_434]]
      },  
      Title = [[Act I: Before Things]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client2_152]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_150]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_180]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_181]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_444]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Bloated Izam]],  
              Position = {
                y = -1827.453125,  
                x = 22472.42188,  
                InstanceId = [[Client2_153]],  
                Class = [[Position]],  
                z = 1.671875
              },  
              Angle = 1.9375,  
              Base = [[palette.entities.creatures.cbbdb3]],  
              ActivitiesId = {
                [[Client2_180]]
              }
            },  
            {
              InstanceId = [[Client2_176]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_174]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_178]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_179]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_444]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gorged Izam]],  
              Position = {
                y = -1821.546875,  
                x = 22479.625,  
                InstanceId = [[Client2_177]],  
                Class = [[Position]],  
                z = 2.078125
              },  
              Angle = -1.25,  
              Base = [[palette.entities.creatures.cbbdb1]],  
              ActivitiesId = {
                [[Client2_178]]
              }
            },  
            {
              InstanceId = [[Client2_200]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_198]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Sluggish Shooki]],  
              Position = {
                y = -1835.953125,  
                x = 22494.79688,  
                InstanceId = [[Client2_201]],  
                Class = [[Position]],  
                z = 1.234375
              },  
              Angle = 2.515625,  
              Base = [[palette.entities.creatures.cpfdb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_266]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_264]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_274]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_379]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_159]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Goari]],  
              Position = {
                y = -1803.015625,  
                x = 22509.3125,  
                InstanceId = [[Client1_267]],  
                Class = [[Position]],  
                z = 1.609375
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.creatures.cccdb4]],  
              ActivitiesId = {
                [[Client1_274]]
              }
            },  
            {
              InstanceId = [[Client1_270]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_268]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_272]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_380]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_159]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Goari]],  
              Position = {
                y = -1796.265625,  
                x = 22515.60938,  
                InstanceId = [[Client1_271]],  
                Class = [[Position]],  
                z = 4.0625
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.creatures.cccdb4]],  
              ActivitiesId = {
                [[Client1_272]]
              }
            },  
            {
              InstanceId = [[Client2_284]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_282]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Drowsy Shooki]],  
              Position = {
                y = -1806.671875,  
                x = 22636.57813,  
                InstanceId = [[Client2_285]],  
                Class = [[Position]],  
                z = 12.859375
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.creatures.cpfdb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_288]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_286]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Devastating Shooki]],  
              Position = {
                y = -1809.046875,  
                x = 22633.54688,  
                InstanceId = [[Client2_289]],  
                Class = [[Position]],  
                z = 12.015625
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.creatures.cpfdf4]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_426]],  
              ActivitiesId = {
                [[Client1_428]]
              },  
              HairType = 5612590,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 14,  
              HandsModel = 5612334,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 1,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 14,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_424]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_428]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_429]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_159]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 5612078,  
              Angle = -2.84375,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_zorai_male.creature]],  
              ArmModel = 5613102,  
              WeaponLeftHand = 0,  
              JacketModel = 5613358,  
              Position = {
                y = -1799.578125,  
                x = 22510.64063,  
                InstanceId = [[Client1_427]],  
                Class = [[Position]],  
                z = 2.5625
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5600814,  
              ArmColor = 5,  
              Name = [[Oversized Tryker]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_432]],  
              ActivitiesId = {
                [[Client1_434]]
              },  
              HairType = 5612590,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 5612334,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 0,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_430]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_434]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_435]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_159]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 5612078,  
              Angle = 3.125,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 5613102,  
              WeaponLeftHand = 0,  
              JacketModel = 5613358,  
              Position = {
                y = -1799.046875,  
                x = 22509.96875,  
                InstanceId = [[Client1_433]],  
                Class = [[Position]],  
                z = 2.3125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5600814,  
              ArmColor = 5,  
              Name = [[Real Tryker]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 12
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      InstanceId = [[Client1_276]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
        [[Client1_320]]
      },  
      Title = [[Act II: The Traveling Helpers]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_280]],  
              ActivitiesId = {
                [[Client1_320]]
              },  
              HairType = 6702,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 5,  
              GabaritHeight = 13,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_278]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[ATKAE]],  
                    InstanceId = [[Client1_323]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_324]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_325]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_371]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 5,  
                        InstanceId = [[Client1_326]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Puzzled]],  
                            InstanceId = [[Client1_327]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_372]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_362]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_363]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_364]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[athim]],  
                    InstanceId = [[Client2_366]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_367]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_368]],  
                            Who = [[Client1_280]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_369]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[sInit]],  
                    InstanceId = [[Client1_320]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_321]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_322]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 6702382,  
              Angle = 1.5,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 0,  
              MorphTarget3 = 0,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              JacketModel = 6704430,  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[Daghn]],  
              Position = {
                y = -1987.640625,  
                x = 22499.54688,  
                InstanceId = [[Client1_281]],  
                Class = [[Position]],  
                z = 37.28125
              },  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              ArmModel = 6706478,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_tryker_male.creature]]
            }
          },  
          InstanceId = [[Client1_277]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    },  
    {
      Cost = 9,  
      Class = [[Act]],  
      InstanceId = [[Client2_202]],  
      WeatherValue = 0,  
      Title = [[Act III: Kitins Arrive]],  
      ActivitiesIds = {
        [[Client2_248]],  
        [[Client2_274]],  
        [[Client2_276]],  
        [[Client2_278]],  
        [[Client2_279]],  
        [[Client2_376]],  
        [[Client2_403]],  
        [[Client2_405]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client2_268]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_266]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_276]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_277]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_214]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[invas]],  
                    InstanceId = [[Client2_403]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_404]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_214]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_325]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_326]],  
                          Value = r2.RefId([[Client2_278]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_327]],  
                        Entity = r2.RefId([[Client2_206]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client2_324]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Hime Assault Kirosta]],  
              Position = {
                y = -1877.40625,  
                x = 22645.34375,  
                InstanceId = [[Client2_269]],  
                Class = [[Position]],  
                z = 18.875
              },  
              Angle = -1.890625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client2_276]],  
                [[Client2_403]]
              }
            },  
            {
              InstanceId = [[Client2_272]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_270]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_274]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_275]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[invas]],  
                    InstanceId = [[Client2_405]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_406]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_330]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_331]],  
                          Value = r2.RefId([[Client2_279]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_332]],  
                        Entity = r2.RefId([[Client2_206]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client2_329]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kaeno Assault Kirosta]],  
              Position = {
                y = -1826.625,  
                x = 22631,  
                InstanceId = [[Client2_273]],  
                Class = [[Position]],  
                z = 12.453125
              },  
              Angle = -1.484375,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client2_274]],  
                [[Client2_405]]
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_343]],  
              ActivitiesId = {
                [[Client2_376]]
              },  
              HairType = 5621550,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 5606958,  
              FeetColor = 3,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_341]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[tower]],  
                    InstanceId = [[Client2_378]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_379]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_380]],  
                            Who = [[Client2_343]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_381]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_382]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_383]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_387]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_388]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_389]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_390]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_392]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_393]],  
                            Who = [[Client2_343]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_394]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_397]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_398]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_401]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[manag]],  
                    InstanceId = [[Client2_436]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_437]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_438]],  
                            Who = [[Client2_343]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_439]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_460]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_461]],  
                            Who = [[Client2_108]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_462]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_464]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_465]],  
                            Who = [[Client2_343]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_466]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_468]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Panick]],  
                            InstanceId = [[Client2_469]],  
                            Who = [[Client2_108]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_470]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[start]],  
                    InstanceId = [[Client2_472]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_473]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_474]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_475]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_477]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_478]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_479]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_410]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client2_409]],  
                      Value = r2.RefId([[Client2_396]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_376]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_377]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_472]]),  
                        ActivityZoneId = r2.RefId([[Client2_346]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[15]],  
                        InstanceId = [[Client2_396]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_378]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_434]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_418]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[15]],  
                        InstanceId = [[Client2_435]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_436]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_378]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 5606702,  
              Angle = -0.578125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595694,  
              ArmColor = 3,  
              Name = [[Kaeno Messenger]],  
              Position = {
                y = -1882.640625,  
                x = 22529.54688,  
                InstanceId = [[Client2_344]],  
                Class = [[Position]],  
                z = 6.484375
              },  
              Sheet = [[ring_guard_melee_tank_slash_b3.creature]],  
              ArmModel = 5607470,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client2_203]]
        },  
        {
          InstanceId = [[Client2_243]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client2_242]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client2_206]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_204]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[init]],  
                    InstanceId = [[Client2_248]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_249]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_225]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[HIME]],  
                    InstanceId = [[Client2_278]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_280]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_214]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[KAENO]],  
                    InstanceId = [[Client2_279]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_281]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[start]],  
                    InstanceId = [[Client2_290]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client2_291]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_292]],  
                            Who = [[Client2_59]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_408]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_325]],  
                    Name = [[]],  
                    InstanceId = [[Client2_323]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_327]]
                  },  
                  {
                    LogicEntityAction = [[Client2_330]],  
                    Name = [[]],  
                    InstanceId = [[Client2_328]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_332]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[1337 Kipucker]],  
              Position = {
                y = -1852.015625,  
                x = 22651.39063,  
                InstanceId = [[Client2_207]],  
                Class = [[Position]],  
                z = 15.3125
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.creatures.ckeic4]],  
              ActivitiesId = {
                [[Client2_248]],  
                [[Client2_278]],  
                [[Client2_279]]
              }
            },  
            {
              InstanceId = [[Client2_210]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_208]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Fragzor Kipucker]],  
              Position = {
                y = -1856.21875,  
                x = 22645.79688,  
                InstanceId = [[Client2_211]],  
                Class = [[Position]],  
                z = 16.234375
              },  
              Angle = -1.03125,  
              Base = [[palette.entities.creatures.ckeic3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_214]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_212]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lord Kipucker]],  
              Position = {
                y = -1852.390625,  
                x = 22658.54688,  
                InstanceId = [[Client2_215]],  
                Class = [[Position]],  
                z = 14.84375
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.creatures.ckeic3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_218]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_216]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Qsdfqsjf Kipucker]],  
              Position = {
                y = -1863.328125,  
                x = 22651.78125,  
                InstanceId = [[Client2_219]],  
                Class = [[Position]],  
                z = 17.625
              },  
              Angle = -1.234375,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_246]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_244]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Master Kipucker]],  
              Position = {
                y = -1850.671875,  
                x = 22640.375,  
                InstanceId = [[Client2_247]],  
                Class = [[Position]],  
                z = 13.921875
              },  
              Angle = 2.484375,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_264]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_262]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pwnage Kipucker]],  
              Position = {
                y = -1871.015625,  
                x = 22647.53125,  
                InstanceId = [[Client2_265]],  
                Class = [[Position]],  
                z = 17.515625
              },  
              Angle = -1.890625,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client2_241]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client2_321]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
      },  
      Title = [[Act IV: Test Act]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client2_322]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    },  
    {
      Cost = 7,  
      Class = [[Act]],  
      InstanceId = [[Client1_381]],  
      WeatherValue = 0,  
      Title = [[Act V: Bandits Appear From Nowhere]],  
      ActivitiesIds = {
        [[Client1_422]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_382]]
        },  
        {
          InstanceId = [[Client1_421]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = -23.265625,  
            x = -83.59375,  
            InstanceId = [[Client1_420]],  
            Class = [[Position]],  
            z = -13.703125
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_417]],  
              ActivitiesId = {
                [[Client1_422]]
              },  
              HairType = 5623598,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6705710,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_415]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_422]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_423]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6705198,  
              Angle = -1,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6707246,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Evil Leader]],  
              Position = {
                y = -1844.046875,  
                x = 22369.90625,  
                InstanceId = [[Client1_418]],  
                Class = [[Position]],  
                z = -6.578125
              },  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              ArmModel = 6706734,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_389]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 3,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_387]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -1.296875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 3,  
              Name = [[Evil Sengosha]],  
              Position = {
                y = -1841.546875,  
                x = 22367.73438,  
                InstanceId = [[Client1_390]],  
                Class = [[Position]],  
                z = -6.640625
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_393]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_391]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -1.296875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 2,  
              Name = [[Evil Blastercalm]],  
              Position = {
                y = -1844.171875,  
                x = 22367.07813,  
                InstanceId = [[Client1_394]],  
                Class = [[Position]],  
                z = -6.65625
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_405]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_403]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -1.21875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 1,  
              Name = [[Evil Kaetemi]],  
              Position = {
                y = -1847.125,  
                x = 22368.15625,  
                InstanceId = [[Client1_406]],  
                Class = [[Position]],  
                z = -6.921875
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_385]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_383]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -1.296875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 4,  
              Name = [[Evil Lorihime]],  
              Position = {
                y = -1842.9375,  
                x = 22367.17188,  
                InstanceId = [[Client1_386]],  
                Class = [[Position]],  
                z = -6.609375
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_409]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_407]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -1.21875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 0,  
              Name = [[Evil Bandit]],  
              Position = {
                y = -1846.1875,  
                x = 22367.76563,  
                InstanceId = [[Client1_410]],  
                Class = [[Position]],  
                z = -6.796875
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_413]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_411]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -1.171875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 0,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6736942,  
              WeaponRightHand = 6936622,  
              ArmColor = 5,  
              Name = [[Evil Homin]],  
              Position = {
                y = -1845.15625,  
                x = 22367.125,  
                InstanceId = [[Client1_414]],  
                Class = [[Position]],  
                z = -6.6875
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_419]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 18,  
        InstanceId = [[Client1_328]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_329]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_330]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_331]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_332]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_344]],  
        Class = [[TextManagerEntry]],  
        Text = [[These are dire times, I fear that we might face hard times to come.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_345]],  
        Class = [[TextManagerEntry]],  
        Text = [[I fear that the others might not do well now, I wonder how they are...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_349]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_350]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_355]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_356]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_357]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_293]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_294]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_366]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_295]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_296]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_364]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_369]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_370]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_371]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need... (should be "to see")]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_372]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_373]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client2_381]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitins are coming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_384]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_385]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_386]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_387]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_390]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_391]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_394]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_395]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_399]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_400]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_401]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_402]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_407]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_408]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_439]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have a huge problem!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_440]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have a huge problem!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_462]],  
        Class = [[TextManagerEntry]],  
        Text = [[What happened?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_463]],  
        Class = [[TextManagerEntry]],  
        Text = [[What happened?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_466]],  
        Class = [[TextManagerEntry]],  
        Text = [[There might be Kitins incoming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_467]],  
        Class = [[TextManagerEntry]],  
        Text = [[There might be Kitins incoming!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_470]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Not the Kitins!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_471]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Not the Kitins!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_475]],  
        Class = [[TextManagerEntry]],  
        Text = [[*I keep hearing something coming from the tunnel...*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_476]],  
        Class = [[TextManagerEntry]],  
        Text = [[*I keep hearing something coming from the tunnel...*]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_479]],  
        Class = [[TextManagerEntry]],  
        Text = [[*Could it be ... ?!*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_480]],  
        Class = [[TextManagerEntry]],  
        Text = [[*Could it be ... ?!*]]
      }
    }
  }
}