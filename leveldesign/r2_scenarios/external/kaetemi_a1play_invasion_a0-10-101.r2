scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_693]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 2,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_695]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    Npc = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ActionStep = 0,  
    Position = 0,  
    EventType = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    LogicEntityReaction = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Behavior = {
        InstanceId = [[Client1_696]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Permanent]],  
      Version = 3,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[Permanent]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_697]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_714]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_712]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]],  
              Position = {
                y = -1288.6875,  
                x = 22792.1875,  
                InstanceId = [[Client1_715]],  
                Class = [[Position]],  
                z = 74.453125
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_718]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_716]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]],  
              Position = {
                y = -1289.265625,  
                x = 22789.9375,  
                InstanceId = [[Client1_719]],  
                Class = [[Position]],  
                z = 74.515625
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_722]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_720]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[runic circle 1]],  
              Position = {
                y = -1294.3125,  
                x = 22769.28125,  
                InstanceId = [[Client1_723]],  
                Class = [[Position]],  
                z = 71.8125
              },  
              Angle = 0.46875,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_726]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_724]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1269.5625,  
                x = 22780.64063,  
                InstanceId = [[Client1_727]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              Angle = -0.375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_730]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_728]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1265.28125,  
                x = 22788.21875,  
                InstanceId = [[Client1_731]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = -1.421875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Local Town Region]],  
              InstanceId = [[Client1_733]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_732]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_735]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1290.171875,  
                    x = 22780.45313,  
                    InstanceId = [[Client1_736]],  
                    Class = [[Position]],  
                    z = 74.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_738]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1283.203125,  
                    x = 22802.26563,  
                    InstanceId = [[Client1_739]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_741]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1264.90625,  
                    x = 22797.5,  
                    InstanceId = [[Client1_742]],  
                    Class = [[Position]],  
                    z = 75.09375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_749]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_747]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 1]],  
              Position = {
                y = -1250.65625,  
                x = 22799.25,  
                InstanceId = [[Client1_750]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = 0.296875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_753]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_751]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 2]],  
              Position = {
                y = -1262.953125,  
                x = 22803.375,  
                InstanceId = [[Client1_754]],  
                Class = [[Position]],  
                z = 75.125
              },  
              Angle = 0.296875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_757]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_755]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 3]],  
              Position = {
                y = -1273.59375,  
                x = 22805.78125,  
                InstanceId = [[Client1_758]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = 0.203125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_761]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_759]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 4]],  
              Position = {
                y = -1288.140625,  
                x = 22808.5,  
                InstanceId = [[Client1_762]],  
                Class = [[Position]],  
                z = 74.796875
              },  
              Angle = 0.203125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_765]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 4,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 11,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_763]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1047]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1046]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 6699310,  
              Angle = 0.140625,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755374,  
              ArmColor = 2,  
              Name = [[Kaethia Guard]],  
              Position = {
                y = -1280.703125,  
                x = 22807.53125,  
                InstanceId = [[Client1_766]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              Sex = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_769]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 6,  
              HandsModel = 6699822,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 1,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 1,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_767]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1050]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1049]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 3,  
              FeetModel = 6699566,  
              Angle = 0.171875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755630,  
              ArmColor = 0,  
              Name = [[Kaethia Guard]],  
              Position = {
                y = -1268.4375,  
                x = 22804.64063,  
                InstanceId = [[Client1_770]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Sex = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 2,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_773]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 0,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 12,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_771]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1053]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1052]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = 0.390625,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              JacketModel = 6701870,  
              WeaponRightHand = 6755374,  
              ArmColor = 4,  
              Name = [[Kaethia Guard]],  
              Position = {
                y = -1256.828125,  
                x = 22801.0625,  
                InstanceId = [[Client1_774]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 0,  
              Tattoo = 5
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_777]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 12,  
              HandsModel = 6699822,  
              FeetColor = 0,  
              GabaritBreastSize = 2,  
              GabaritHeight = 4,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 5,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_775]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1056]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1055]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              JacketModel = 6701870,  
              WeaponRightHand = 6756398,  
              ArmColor = 1,  
              Name = [[Kaethia Guard]],  
              Position = {
                y = -1299.265625,  
                x = 22809,  
                InstanceId = [[Client1_778]],  
                Class = [[Position]],  
                z = 72.25
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 0,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_781]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 6,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 13,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_779]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1059]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1058]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Angle = 0.375,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755374,  
              ArmColor = 5,  
              Name = [[Kaethia Guard]],  
              Position = {
                y = -1244.28125,  
                x = 22795.40625,  
                InstanceId = [[Client1_782]],  
                Class = [[Position]],  
                z = 74.8125
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 4,  
              Tattoo = 0
            },  
            {
              InstanceId = [[Client1_785]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_783]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 1]],  
              Position = {
                y = -1300.234375,  
                x = 22801.76563,  
                InstanceId = [[Client1_786]],  
                Class = [[Position]],  
                z = 72.328125
              },  
              Angle = 2.4375,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_789]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_787]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 2]],  
              Position = {
                y = -1288.5,  
                x = 22800.54688,  
                InstanceId = [[Client1_790]],  
                Class = [[Position]],  
                z = 74.34375
              },  
              Angle = -2.203125,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_793]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_791]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 3]],  
              Position = {
                y = -1300.21875,  
                x = 22789.71875,  
                InstanceId = [[Client1_794]],  
                Class = [[Position]],  
                z = 73.875
              },  
              Angle = 1.109375,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Right Attack Region]],  
              InstanceId = [[Client1_829]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_828]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_831]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1308.90625,  
                    x = 22880.23438,  
                    InstanceId = [[Client1_832]],  
                    Class = [[Position]],  
                    z = 74.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_834]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1251.203125,  
                    x = 22927.07813,  
                    InstanceId = [[Client1_835]],  
                    Class = [[Position]],  
                    z = 72.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_837]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1282.625,  
                    x = 22959.92188,  
                    InstanceId = [[Client1_838]],  
                    Class = [[Position]],  
                    z = 75.796875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Left Attack Region]],  
              InstanceId = [[Client1_840]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_839]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_842]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1189.25,  
                    x = 22890.01563,  
                    InstanceId = [[Client1_843]],  
                    Class = [[Position]],  
                    z = 72.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_845]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1148.109375,  
                    x = 22845.92188,  
                    InstanceId = [[Client1_846]],  
                    Class = [[Position]],  
                    z = 75.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_848]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1089.453125,  
                    x = 22916.25,  
                    InstanceId = [[Client1_849]],  
                    Class = [[Position]],  
                    z = 76.96875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kitin Home Region]],  
              InstanceId = [[Client1_851]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_850]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_853]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1101.015625,  
                    x = 22989.45313,  
                    InstanceId = [[Client1_854]],  
                    Class = [[Position]],  
                    z = 76.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_856]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1170.84375,  
                    x = 23069.15625,  
                    InstanceId = [[Client1_857]],  
                    Class = [[Position]],  
                    z = 75.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_859]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1096.359375,  
                    x = 23065.32813,  
                    InstanceId = [[Client1_860]],  
                    Class = [[Position]],  
                    z = 75.546875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kitin Front Defense]],  
              InstanceId = [[Client1_944]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_943]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_946]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1172.5625,  
                    x = 23068.76563,  
                    InstanceId = [[Client1_947]],  
                    Class = [[Position]],  
                    z = 75.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_949]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1194.078125,  
                    x = 23067.375,  
                    InstanceId = [[Client1_950]],  
                    Class = [[Position]],  
                    z = 75.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_952]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1094.78125,  
                    x = 22960.46875,  
                    InstanceId = [[Client1_953]],  
                    Class = [[Position]],  
                    z = 76.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_955]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1100.8125,  
                    x = 22986.29688,  
                    InstanceId = [[Client1_956]],  
                    Class = [[Position]],  
                    z = 76.828125
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_699]]
        },  
        {
          InstanceId = [[Client1_825]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Local Town Fighters]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_824]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_823]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_821]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 6,  
              HandsModel = 6700078,  
              FeetColor = 5,  
              GabaritBreastSize = 5,  
              GabaritHeight = 8,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 1,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_819]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1062]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1061]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1065]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1064]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1068]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1067]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1161]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1160]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1204]],  
                    LogicEntityAction = [[Client1_1201]],  
                    ActionStep = [[Client1_1203]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Homin]],  
                    InstanceId = [[Client1_826]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_930]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[Kitin]],  
                    InstanceId = [[Client1_931]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_932]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 6699566,  
              Angle = 0,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 5,  
              JacketModel = 6701870,  
              WeaponRightHand = 6756398,  
              ArmColor = 3,  
              Name = [[Kaethia Fighter]],  
              Position = {
                y = -1279.21875,  
                x = 22799.98438,  
                InstanceId = [[Client1_822]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Sex = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 5,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_809]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 12,  
              HandsModel = 6699822,  
              FeetColor = 3,  
              GabaritBreastSize = 1,  
              GabaritHeight = 6,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_807]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 6699310,  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              JacketModel = 6701870,  
              WeaponRightHand = 6755886,  
              ArmColor = 2,  
              Name = [[Kaethia Fighter]],  
              Position = {
                y = -1281.71875,  
                x = 22800.46875,  
                InstanceId = [[Client1_810]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              Sex = 1,  
              MorphTarget7 = 2,  
              MorphTarget3 = 2,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_817]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_815]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701358,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 7,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756654,  
              ArmColor = 2,  
              Name = [[Kaethia Fighter]],  
              Position = {
                y = -1276.859375,  
                x = 22799.03125,  
                InstanceId = [[Client1_818]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              Sex = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 23
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_927]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Kitin Patrol Kincher]],  
          Position = {
            y = 11.1875,  
            x = 7.046875,  
            InstanceId = [[Client1_926]],  
            Class = [[Position]],  
            z = 0.0625
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_925]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_887]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_885]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1071]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1070]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1074]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1073]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1077]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1076]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1080]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1079]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1083]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1082]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1086]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1085]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1089]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1088]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1092]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1091]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1095]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1094]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1098]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1097]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1164]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1163]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1207]],  
                    LogicEntityAction = [[Client1_1201]],  
                    ActionStep = [[Client1_1206]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Kitin]],  
                    InstanceId = [[Client1_928]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_929]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[Homin]],  
                    InstanceId = [[Client1_933]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_934]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kincher]],  
              Position = {
                y = -1149.203125,  
                x = 23030.34375,  
                InstanceId = [[Client1_888]],  
                Class = [[Position]],  
                z = 74.40625
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.ckdif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_895]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_893]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kincher]],  
              Position = {
                y = -1148.71875,  
                x = 23035.35938,  
                InstanceId = [[Client1_896]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.ckdif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_903]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_901]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]],  
              Position = {
                y = -1140.296875,  
                x = 23031.03125,  
                InstanceId = [[Client1_904]],  
                Class = [[Position]],  
                z = 74.53125
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.ckdif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_899]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_897]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]],  
              Position = {
                y = -1136.671875,  
                x = 23025.4375,  
                InstanceId = [[Client1_900]],  
                Class = [[Position]],  
                z = 74.1875
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.ckdif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_907]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_905]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]],  
              Position = {
                y = -1147.25,  
                x = 23040.29688,  
                InstanceId = [[Client1_908]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.ckdif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_911]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_909]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kincher]],  
              Position = {
                y = -1130.046875,  
                x = 23022.1875,  
                InstanceId = [[Client1_912]],  
                Class = [[Position]],  
                z = 74.3125
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.creatures.ckdif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_915]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_913]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kincher]],  
              Position = {
                y = -1134.09375,  
                x = 23030.89063,  
                InstanceId = [[Client1_916]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.creatures.ckdif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_919]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_917]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kincher]],  
              Position = {
                y = -1139.09375,  
                x = 23038.15625,  
                InstanceId = [[Client1_920]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.creatures.ckdif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_923]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_921]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kincher]],  
              Position = {
                y = -1143.078125,  
                x = 23044.85938,  
                InstanceId = [[Client1_924]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.creatures.ckdif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_891]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_889]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kincher]],  
              Position = {
                y = -1143.0625,  
                x = 23026.32813,  
                InstanceId = [[Client1_892]],  
                Class = [[Position]],  
                z = 74.0625
              },  
              Angle = -2.125,  
              Base = [[palette.entities.creatures.ckdif2]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1011]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Defense Elite Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1010]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1009]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_991]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_989]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1101]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1100]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1104]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1103]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1107]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1106]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1110]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1109]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1167]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1166]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1240]],  
                    LogicEntityAction = [[Client1_1237]],  
                    ActionStep = [[Client1_1239]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1035]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1036]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1183]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1184]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]],  
              Position = {
                y = -1121.046875,  
                x = 23021.40625,  
                InstanceId = [[Client1_992]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -2.25,  
              Base = [[palette.entities.creatures.ckbif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_983]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_981]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]],  
              Position = {
                y = -1114.359375,  
                x = 23013.14063,  
                InstanceId = [[Client1_984]],  
                Class = [[Position]],  
                z = 75.734375
              },  
              Angle = -2.34375,  
              Base = [[palette.entities.creatures.ckcif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_995]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_993]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]],  
              Position = {
                y = -1127.859375,  
                x = 23025.5625,  
                InstanceId = [[Client1_996]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = -2.71875,  
              Base = [[palette.entities.creatures.ckbif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_987]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_985]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]],  
              Position = {
                y = -1146.921875,  
                x = 23048.3125,  
                InstanceId = [[Client1_988]],  
                Class = [[Position]],  
                z = 75.21875
              },  
              Angle = -2.25,  
              Base = [[palette.entities.creatures.ckcif4]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1014]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Defense Boss Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1013]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1012]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_937]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_935]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1113]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1112]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1116]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1115]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1119]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1118]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1122]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1121]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1170]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1169]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1243]],  
                    LogicEntityAction = [[Client1_1237]],  
                    ActionStep = [[Client1_1242]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1037]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1038]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1185]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1186]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kipukoo]],  
              Position = {
                y = -1110.65625,  
                x = 23035.45313,  
                InstanceId = [[Client1_938]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = -2.3125,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_999]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_997]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -1112.71875,  
                x = 23023.34375,  
                InstanceId = [[Client1_1000]],  
                Class = [[Position]],  
                z = 75.375
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1003]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1001]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -1120.015625,  
                x = 23033.54688,  
                InstanceId = [[Client1_1004]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1007]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1005]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              Position = {
                y = -1123.546875,  
                x = 23044.60938,  
                InstanceId = [[Client1_1008]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = -2.359375,  
              Base = [[palette.entities.creatures.ckeif4]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1017]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Front Defense Left]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1016]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1015]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_959]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_957]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1125]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1124]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1128]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1127]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1131]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1130]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1173]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1172]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1228]],  
                    LogicEntityAction = [[Client1_1225]],  
                    ActionStep = [[Client1_1227]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1031]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1032]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_944]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1192]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1193]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]],  
              Position = {
                y = -1113.125,  
                x = 22986.3125,  
                InstanceId = [[Client1_960]],  
                Class = [[Position]],  
                z = 75.84375
              },  
              Angle = -2.296875,  
              Base = [[palette.entities.creatures.ckbif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_963]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_961]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1107.875,  
                x = 22986.26563,  
                InstanceId = [[Client1_964]],  
                Class = [[Position]],  
                z = 76.890625
              },  
              Angle = -2.53125,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_967]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_965]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1112.328125,  
                x = 22993.67188,  
                InstanceId = [[Client1_968]],  
                Class = [[Position]],  
                z = 76.21875
              },  
              Angle = -2.53125,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1020]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Front Defense Right]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1019]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1018]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_971]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_969]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1134]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1133]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1137]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1136]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1140]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1139]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1176]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1175]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1231]],  
                    LogicEntityAction = [[Client1_1225]],  
                    ActionStep = [[Client1_1230]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1033]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1034]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_944]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1194]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1195]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]],  
              Position = {
                y = -1161.921875,  
                x = 23038.65625,  
                InstanceId = [[Client1_972]],  
                Class = [[Position]],  
                z = 75.1875
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.creatures.ckbif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_975]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_973]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1156.21875,  
                x = 23036.90625,  
                InstanceId = [[Client1_976]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_979]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_977]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1161.390625,  
                x = 23045.375,  
                InstanceId = [[Client1_980]],  
                Class = [[Position]],  
                z = 75.25
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1023]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Front Attack Left]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1022]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1021]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_863]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_861]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1143]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1142]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1146]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1145]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1149]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1148]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1179]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1178]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1216]],  
                    LogicEntityAction = [[Client1_1213]],  
                    ActionStep = [[Client1_1215]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1029]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1030]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_840]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1187]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1188]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kirosta]],  
              Position = {
                y = -1166.484375,  
                x = 22873.39063,  
                InstanceId = [[Client1_864]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = 3.924025774,  
              Base = [[palette.entities.creatures.ckfif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_867]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_865]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              Position = {
                y = -1159.359375,  
                x = 22878.26563,  
                InstanceId = [[Client1_868]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.creatures.ckfif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_871]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_869]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              Position = {
                y = -1167.96875,  
                x = 22879.57813,  
                InstanceId = [[Client1_872]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.creatures.ckfif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1026]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Front Attack Right]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1025]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1024]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_875]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_873]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1152]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1151]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1155]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1154]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1044]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1158]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1157]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1182]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1181]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1219]],  
                    LogicEntityAction = [[Client1_1213]],  
                    ActionStep = [[Client1_1218]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1027]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1028]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_829]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1190]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1191]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kirosta]],  
              Position = {
                y = -1290.140625,  
                x = 22903.70313,  
                InstanceId = [[Client1_876]],  
                Class = [[Position]],  
                z = 75.4375
              },  
              Angle = 2.046875,  
              Base = [[palette.entities.creatures.ckfif4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_879]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_877]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              Position = {
                y = -1287.28125,  
                x = 22915.78125,  
                InstanceId = [[Client1_880]],  
                Class = [[Position]],  
                z = 75.46875
              },  
              Angle = 2.65625,  
              Base = [[palette.entities.creatures.ckfif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_883]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_881]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              Position = {
                y = -1292.1875,  
                x = 22911.0625,  
                InstanceId = [[Client1_884]],  
                Class = [[Position]],  
                z = 76.140625
              },  
              Angle = 2.65625,  
              Base = [[palette.entities.creatures.ckfif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_698]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1039]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_1044]],  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_1043]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]],  
            Actions = {
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1045]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1046]],  
                Entity = r2.RefId([[Client1_765]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1048]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1049]],  
                Entity = r2.RefId([[Client1_769]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1051]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1052]],  
                Entity = r2.RefId([[Client1_773]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1054]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1055]],  
                Entity = r2.RefId([[Client1_777]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1057]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1058]],  
                Entity = r2.RefId([[Client1_781]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1060]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1061]],  
                Entity = r2.RefId([[Client1_821]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1063]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1064]],  
                Entity = r2.RefId([[Client1_809]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1066]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1067]],  
                Entity = r2.RefId([[Client1_817]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1069]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1070]],  
                Entity = r2.RefId([[Client1_887]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1072]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1073]],  
                Entity = r2.RefId([[Client1_895]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1075]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1076]],  
                Entity = r2.RefId([[Client1_903]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1078]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1079]],  
                Entity = r2.RefId([[Client1_899]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1081]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1082]],  
                Entity = r2.RefId([[Client1_907]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1084]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1085]],  
                Entity = r2.RefId([[Client1_911]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1087]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1088]],  
                Entity = r2.RefId([[Client1_915]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1090]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1091]],  
                Entity = r2.RefId([[Client1_919]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1093]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1094]],  
                Entity = r2.RefId([[Client1_923]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1096]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1097]],  
                Entity = r2.RefId([[Client1_891]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1099]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1100]],  
                Entity = r2.RefId([[Client1_991]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1102]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1103]],  
                Entity = r2.RefId([[Client1_983]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1105]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1106]],  
                Entity = r2.RefId([[Client1_995]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1108]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1109]],  
                Entity = r2.RefId([[Client1_987]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1111]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1112]],  
                Entity = r2.RefId([[Client1_937]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1114]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1115]],  
                Entity = r2.RefId([[Client1_999]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1117]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1118]],  
                Entity = r2.RefId([[Client1_1003]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1120]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1121]],  
                Entity = r2.RefId([[Client1_1007]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1123]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1124]],  
                Entity = r2.RefId([[Client1_959]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1126]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1127]],  
                Entity = r2.RefId([[Client1_963]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1129]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1130]],  
                Entity = r2.RefId([[Client1_967]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1132]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1133]],  
                Entity = r2.RefId([[Client1_971]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1135]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1136]],  
                Entity = r2.RefId([[Client1_975]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1138]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1139]],  
                Entity = r2.RefId([[Client1_979]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1141]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1142]],  
                Entity = r2.RefId([[Client1_863]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1144]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1145]],  
                Entity = r2.RefId([[Client1_867]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1147]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1148]],  
                Entity = r2.RefId([[Client1_871]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1150]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1151]],  
                Entity = r2.RefId([[Client1_875]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1153]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1154]],  
                Entity = r2.RefId([[Client1_879]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Activate]],  
                  InstanceId = [[Client1_1156]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_1157]],  
                Entity = r2.RefId([[Client1_883]]),  
                Class = [[ActionStep]]
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1160]],  
                Entity = r2.RefId([[Client1_825]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1159]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_826]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1163]],  
                Entity = r2.RefId([[Client1_927]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1162]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_928]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1166]],  
                Entity = r2.RefId([[Client1_1011]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1165]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1035]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1169]],  
                Entity = r2.RefId([[Client1_1014]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1168]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1037]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1172]],  
                Entity = r2.RefId([[Client1_1017]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1171]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1031]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1175]],  
                Entity = r2.RefId([[Client1_1020]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1174]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1033]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1178]],  
                Entity = r2.RefId([[Client1_1023]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1177]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1029]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1181]],  
                Entity = r2.RefId([[Client1_1026]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1180]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1027]])
                }
              }
            },  
            Conditions = {
            }
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1041]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1042]]
        }
      },  
      Name = [[Act I: Reset Scenario]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_1040]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Title = [[Act I: Reset Scenario]],  
      ManualWeather = 0,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1196]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1201]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1203]],  
                Entity = r2.RefId([[Client1_825]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1202]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_931]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1206]],  
                Entity = r2.RefId([[Client1_927]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1205]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_933]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1200]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act II: Invasion Main]],  
      InstanceId = [[Client1_1198]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1197]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act II: Invasion Main]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1199]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1208]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1213]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1215]],  
                Entity = r2.RefId([[Client1_1023]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1214]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1187]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1218]],  
                Entity = r2.RefId([[Client1_1026]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1217]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1190]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1212]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act III: Front Attack Launch]],  
      InstanceId = [[Client1_1210]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1209]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act III: Front Attack Launch]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1211]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1220]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1225]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1227]],  
                Entity = r2.RefId([[Client1_1017]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1226]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1192]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1230]],  
                Entity = r2.RefId([[Client1_1020]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1229]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1194]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1224]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act IV: Front Defence Run]],  
      InstanceId = [[Client1_1222]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1221]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act IV: Front Defence Run]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1223]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      States = {
      }
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1232]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1237]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1239]],  
                Entity = r2.RefId([[Client1_1011]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1238]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1183]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1242]],  
                Entity = r2.RefId([[Client1_1014]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1241]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1185]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1236]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act V: Boss Invasion]],  
      InstanceId = [[Client1_1234]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1233]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act V: Boss Invasion]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1235]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_694]],  
    Texts = {
    }
  }
}