scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    EventType = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    NpcCustom = 0,  
    LogicEntityReaction = 0,  
    Region = 0,  
    ActivityStep = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Npc = 0,  
    ActionStep = 0,  
    Road = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    Position = 0
  },  
  Acts = {
    {
      Cost = 47,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_1438]],  
        [[Client1_1517]],  
        [[Client1_1582]],  
        [[Client1_1584]],  
        [[Client1_1586]],  
        [[Client1_1588]],  
        [[Client1_1762]],  
        [[Client1_1764]],  
        [[Client1_1765]],  
        [[Client1_1766]],  
        [[Client1_1767]],  
        [[Client1_1768]],  
        [[Client1_1769]],  
        [[Client1_1896]],  
        [[Client1_1898]],  
        [[Client1_1900]]
      },  
      Title = [[Ceremony]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1139]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[1Exit Tower]],  
              Position = {
                y = -1967.8125,  
                x = 27940.65625,  
                InstanceId = [[Client1_1142]],  
                Class = [[Position]],  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1143]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 1]],  
              Position = {
                y = -1851.296875,  
                x = 27899.29688,  
                InstanceId = [[Client1_1146]],  
                Class = [[Position]],  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1147]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 1]],  
              Position = {
                y = -1851.203125,  
                x = 27895.9375,  
                InstanceId = [[Client1_1150]],  
                Class = [[Position]],  
                z = -13.875
              },  
              Angle = -2.203125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1151]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Tower 1]],  
              Position = {
                y = -1850.9375,  
                x = 27931.39063,  
                InstanceId = [[Client1_1154]],  
                Class = [[Position]],  
                z = -14.5
              },  
              Angle = -1.71875,  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1155]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 2]],  
              Position = {
                y = -1844.8125,  
                x = 27922.95313,  
                InstanceId = [[Client1_1158]],  
                Class = [[Position]],  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1159]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 3]],  
              Position = {
                y = -1853.234375,  
                x = 27937.125,  
                InstanceId = [[Client1_1162]],  
                Class = [[Position]],  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1163]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 1]],  
              Position = {
                y = -1838.734375,  
                x = 27892.96875,  
                InstanceId = [[Client1_1166]],  
                Class = [[Position]],  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1167]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 2]],  
              Position = {
                y = -1840.8125,  
                x = 27937.21875,  
                InstanceId = [[Client1_1170]],  
                Class = [[Position]],  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1173]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1171]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 1]],  
              Position = {
                y = -1856.328125,  
                x = 27930.85938,  
                InstanceId = [[Client1_1174]],  
                Class = [[Position]],  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1177]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1175]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 2]],  
              Position = {
                y = -1853.578125,  
                x = 27898.125,  
                InstanceId = [[Client1_1178]],  
                Class = [[Position]],  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1181]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1179]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 2]],  
              Position = {
                y = -1887.59375,  
                x = 27942.53125,  
                InstanceId = [[Client1_1182]],  
                Class = [[Position]],  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1185]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1183]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 3]],  
              Position = {
                y = -1901.703125,  
                x = 27941.15625,  
                InstanceId = [[Client1_1186]],  
                Class = [[Position]],  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1189]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1187]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 4]],  
              Position = {
                y = -1896.890625,  
                x = 27943.17188,  
                InstanceId = [[Client1_1190]],  
                Class = [[Position]],  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1193]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1191]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 3]],  
              Position = {
                y = -1900.859375,  
                x = 27939.875,  
                InstanceId = [[Client1_1194]],  
                Class = [[Position]],  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1197]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1195]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Spawn Point]],  
              Position = {
                y = -1878.25,  
                x = 27915.09375,  
                InstanceId = [[Client1_1198]],  
                Class = [[Position]],  
                z = -10.640625
              },  
              Angle = 1.625,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1209]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1207]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Circle 1]],  
              Position = {
                y = -1877.34375,  
                x = 27895.6875,  
                InstanceId = [[Client1_1210]],  
                Class = [[Position]],  
                z = -13.0625
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1213]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1211]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 3]],  
              Position = {
                y = -1893.28125,  
                x = 27964.14063,  
                InstanceId = [[Client1_1214]],  
                Class = [[Position]],  
                z = -13.71875
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Home Region]],  
              InstanceId = [[Client1_1216]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1218]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1906.75,  
                    x = 27932.96875,  
                    InstanceId = [[Client1_1219]],  
                    Class = [[Position]],  
                    z = -7.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1221]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1881.984375,  
                    x = 27932.32813,  
                    InstanceId = [[Client1_1222]],  
                    Class = [[Position]],  
                    z = -9.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1224]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1863.34375,  
                    x = 27935.84375,  
                    InstanceId = [[Client1_1225]],  
                    Class = [[Position]],  
                    z = -11.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1861.46875,  
                    x = 27890.98438,  
                    InstanceId = [[Client1_1228]],  
                    Class = [[Position]],  
                    z = -14.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1877.3125,  
                    x = 27907.89063,  
                    InstanceId = [[Client1_1231]],  
                    Class = [[Position]],  
                    z = -11.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1905.828125,  
                    x = 27885.70313,  
                    InstanceId = [[Client1_1234]],  
                    Class = [[Position]],  
                    z = -9.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1904.609375,  
                    x = 27896.57813,  
                    InstanceId = [[Client1_1237]],  
                    Class = [[Position]],  
                    z = -6.671875
                  }
                }
              },  
              Position = {
                y = 1.109375,  
                x = -0.5625,  
                InstanceId = [[Client1_1215]],  
                Class = [[Position]],  
                z = -0.234375
              }
            },  
            {
              InstanceId = [[Client1_1263]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1261]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[2Begin Tower]],  
              Position = {
                y = -2034.015625,  
                x = 28058.39063,  
                InstanceId = [[Client1_1264]],  
                Class = [[Position]],  
                z = 9.828125
              },  
              Angle = 0.078125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1267]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1265]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3Continue Tower]],  
              Position = {
                y = -1974.828125,  
                x = 28140.42188,  
                InstanceId = [[Client1_1268]],  
                Class = [[Position]],  
                z = 15.875
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1271]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1269]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[4Finish Tower]],  
              Position = {
                y = -1946.765625,  
                x = 28223.21875,  
                InstanceId = [[Client1_1272]],  
                Class = [[Position]],  
                z = 32.015625
              },  
              Angle = 0.515625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1359]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1357]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Hut 1]],  
              Position = {
                y = -1838.203125,  
                x = 28334.4375,  
                InstanceId = [[Client1_1360]],  
                Class = [[Position]],  
                z = 33.25
              },  
              Angle = -2.921875,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1363]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1361]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Watchtower 1]],  
              Position = {
                y = -1830.140625,  
                x = 28311.25,  
                InstanceId = [[Client1_1364]],  
                Class = [[Position]],  
                z = 33.375
              },  
              Angle = -1.546875,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1367]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1365]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Watchtower 2]],  
              Position = {
                y = -1849.75,  
                x = 28312.84375,  
                InstanceId = [[Client1_1368]],  
                Class = [[Position]],  
                z = 33.796875
              },  
              Angle = 2.46875,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Security Region]],  
              InstanceId = [[Client1_1425]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1427]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1828.4375,  
                    x = 28310.14063,  
                    InstanceId = [[Client1_1428]],  
                    Class = [[Position]],  
                    z = 33.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1430]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1825.09375,  
                    x = 28335.92188,  
                    InstanceId = [[Client1_1431]],  
                    Class = [[Position]],  
                    z = 34.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1433]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1855.75,  
                    x = 28338.23438,  
                    InstanceId = [[Client1_1434]],  
                    Class = [[Position]],  
                    z = 34.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1436]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1851.625,  
                    x = 28311.59375,  
                    InstanceId = [[Client1_1437]],  
                    Class = [[Position]],  
                    z = 34.484375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1424]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_1446]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1444]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Stele 1]],  
              Position = {
                y = -1901.8125,  
                x = 28189.9375,  
                InstanceId = [[Client1_1447]],  
                Class = [[Position]],  
                z = 38.21875
              },  
              Angle = 0.375,  
              Base = [[palette.entities.botobjects.stele]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1454]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1452]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Spot 1]],  
              Position = {
                y = -1909.078125,  
                x = 28168.67188,  
                InstanceId = [[Client1_1455]],  
                Class = [[Position]],  
                z = 24.125
              },  
              Angle = 0.5,  
              Base = [[palette.entities.botobjects.spot_goo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1458]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1456]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Goo Smoke 1]],  
              Position = {
                y = -1907,  
                x = 28171.23438,  
                InstanceId = [[Client1_1459]],  
                Class = [[Position]],  
                z = 27.375
              },  
              Angle = 0.640625,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1462]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1460]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[End Tent 1]],  
              Position = {
                y = -1756.09375,  
                x = 28247.25,  
                InstanceId = [[Client1_1463]],  
                Class = [[Position]],  
                z = 28.78125
              },  
              Angle = -2.078125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1466]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1464]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[End Hut 1]],  
              Position = {
                y = -1752.5,  
                x = 28236.39063,  
                InstanceId = [[Client1_1467]],  
                Class = [[Position]],  
                z = 27.875
              },  
              Angle = -1.234375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1470]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1468]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[End Wind Turbine 1]],  
              Position = {
                y = -1816.8125,  
                x = 28208.625,  
                InstanceId = [[Client1_1471]],  
                Class = [[Position]],  
                z = 37.34375
              },  
              Angle = 2,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[End Region]],  
              InstanceId = [[Client1_1476]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1478]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1770.234375,  
                    x = 28231.60938,  
                    InstanceId = [[Client1_1479]],  
                    Class = [[Position]],  
                    z = 31.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1481]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1743.65625,  
                    x = 28224.67188,  
                    InstanceId = [[Client1_1482]],  
                    Class = [[Position]],  
                    z = 28.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1484]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1743.390625,  
                    x = 28249.42188,  
                    InstanceId = [[Client1_1485]],  
                    Class = [[Position]],  
                    z = 29.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1487]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1772.125,  
                    x = 28255.21875,  
                    InstanceId = [[Client1_1488]],  
                    Class = [[Position]],  
                    z = 31.703125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1475]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Goo Region]],  
              InstanceId = [[Client1_1490]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1492]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1903.984375,  
                    x = 28207.23438,  
                    InstanceId = [[Client1_1493]],  
                    Class = [[Position]],  
                    z = 34.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1495]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1887.84375,  
                    x = 28178.79688,  
                    InstanceId = [[Client1_1496]],  
                    Class = [[Position]],  
                    z = 40.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1498]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.09375,  
                    x = 28167.8125,  
                    InstanceId = [[Client1_1499]],  
                    Class = [[Position]],  
                    z = 22.953125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1489]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_1502]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1500]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1788]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1789]],  
                          Value = r2.RefId([[Client1_1764]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1790]],  
                        Entity = r2.RefId([[Client1_1240]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1907]],  
                          Value = r2.RefId([[Client1_1898]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1908]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_1787]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1582]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1583]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1572]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[1Exit Yubo]],  
              Position = {
                y = -1958.234375,  
                x = 27935.98438,  
                InstanceId = [[Client1_1503]],  
                Class = [[Position]],  
                z = -2.390625
              },  
              Angle = 1.875,  
              Base = [[palette.entities.creatures.chdfb5]],  
              ActivitiesId = {
                [[Client1_1582]]
              }
            },  
            {
              InstanceId = [[Client1_1506]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1504]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1584]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1585]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1561]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[2Begin Yubo]],  
              Position = {
                y = -2024.9375,  
                x = 28066.14063,  
                InstanceId = [[Client1_1507]],  
                Class = [[Position]],  
                z = 10.390625
              },  
              Angle = 0.21875,  
              Base = [[palette.entities.creatures.chdfb5]],  
              ActivitiesId = {
                [[Client1_1584]]
              }
            },  
            {
              InstanceId = [[Client1_1510]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1508]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1586]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1587]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1550]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3Continue Yubo]],  
              Position = {
                y = -1986.859375,  
                x = 28142.64063,  
                InstanceId = [[Client1_1511]],  
                Class = [[Position]],  
                z = 15.640625
              },  
              Angle = -0.84375,  
              Base = [[palette.entities.creatures.chdfb5]],  
              ActivitiesId = {
                [[Client1_1586]]
              }
            },  
            {
              InstanceId = [[Client1_1536]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1534]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1588]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1589]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1539]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[4Finish Yubo]],  
              Position = {
                y = -1941.84375,  
                x = 28232.875,  
                InstanceId = [[Client1_1537]],  
                Class = [[Position]],  
                z = 32
              },  
              Angle = 0.46875,  
              Base = [[palette.entities.creatures.chdfb5]],  
              ActivitiesId = {
                [[Client1_1588]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[4Finish Region]],  
              InstanceId = [[Client1_1539]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1538]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1541]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1948.859375,  
                    x = 28239.03125,  
                    InstanceId = [[Client1_1542]],  
                    Class = [[Position]],  
                    z = 32.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1544]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1943.828125,  
                    x = 28229.09375,  
                    InstanceId = [[Client1_1545]],  
                    Class = [[Position]],  
                    z = 32.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1547]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1930.3125,  
                    x = 28228.89063,  
                    InstanceId = [[Client1_1548]],  
                    Class = [[Position]],  
                    z = 31.359375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[3Continue Region]],  
              InstanceId = [[Client1_1550]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1549]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1552]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1997.5625,  
                    x = 28136.89063,  
                    InstanceId = [[Client1_1553]],  
                    Class = [[Position]],  
                    z = 17.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1555]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1982.0625,  
                    x = 28142.25,  
                    InstanceId = [[Client1_1556]],  
                    Class = [[Position]],  
                    z = 15.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1558]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1985.6875,  
                    x = 28157.4375,  
                    InstanceId = [[Client1_1559]],  
                    Class = [[Position]],  
                    z = 17.6875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[2Begin Region]],  
              InstanceId = [[Client1_1561]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1560]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1563]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2044.46875,  
                    x = 28080.29688,  
                    InstanceId = [[Client1_1564]],  
                    Class = [[Position]],  
                    z = 12.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1566]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2010.921875,  
                    x = 28063.35938,  
                    InstanceId = [[Client1_1567]],  
                    Class = [[Position]],  
                    z = 10.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1569]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2033.34375,  
                    x = 28065.32813,  
                    InstanceId = [[Client1_1570]],  
                    Class = [[Position]],  
                    z = 9.296875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[1Exit Region]],  
              InstanceId = [[Client1_1572]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1571]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1574]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1933.953125,  
                    x = 27949.21875,  
                    InstanceId = [[Client1_1575]],  
                    Class = [[Position]],  
                    z = -4.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1577]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1962.640625,  
                    x = 27910.0625,  
                    InstanceId = [[Client1_1578]],  
                    Class = [[Position]],  
                    z = -6.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1580]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1960.96875,  
                    x = 27937.53125,  
                    InstanceId = [[Client1_1581]],  
                    Class = [[Position]],  
                    z = -2.703125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[1Exit Route]],  
              InstanceId = [[Client1_1777]],  
              Class = [[Road]],  
              Position = {
                y = 6.015625,  
                x = -2.90625,  
                InstanceId = [[Client1_1776]],  
                Class = [[Position]],  
                z = -0.859375
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1779]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1941.515625,  
                    x = 27924.75,  
                    InstanceId = [[Client1_1780]],  
                    Class = [[Position]],  
                    z = -7.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1782]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1946.21875,  
                    x = 27928,  
                    InstanceId = [[Client1_1783]],  
                    Class = [[Position]],  
                    z = -6.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1785]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1952.25,  
                    x = 27931.78125,  
                    InstanceId = [[Client1_1786]],  
                    Class = [[Position]],  
                    z = -4.15625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[2Begin Route]],  
              InstanceId = [[Client1_1793]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1795]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2010.578125,  
                    x = 28080.17188,  
                    InstanceId = [[Client1_1796]],  
                    Class = [[Position]],  
                    z = 11.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1798]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2016.609375,  
                    x = 28078.25,  
                    InstanceId = [[Client1_1799]],  
                    Class = [[Position]],  
                    z = 11.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1801]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2023.15625,  
                    x = 28072.59375,  
                    InstanceId = [[Client1_1802]],  
                    Class = [[Position]],  
                    z = 10.5
                  }
                }
              },  
              Position = {
                y = 4.15625,  
                x = -1.484375,  
                InstanceId = [[Client1_1792]],  
                Class = [[Position]],  
                z = -0.203125
              }
            },  
            {
              InheritPos = 1,  
              Name = [[3Continue Route]],  
              InstanceId = [[Client1_1804]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1806]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2016.203125,  
                    x = 28131.46875,  
                    InstanceId = [[Client1_1807]],  
                    Class = [[Position]],  
                    z = 16.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1809]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2010.671875,  
                    x = 28133.875,  
                    InstanceId = [[Client1_1810]],  
                    Class = [[Position]],  
                    z = 16.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1812]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2000.625,  
                    x = 28138.32813,  
                    InstanceId = [[Client1_1813]],  
                    Class = [[Position]],  
                    z = 16.328125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1803]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[4Finish Route]],  
              InstanceId = [[Client1_1815]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1817]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1967.625,  
                    x = 28256.70313,  
                    InstanceId = [[Client1_1818]],  
                    Class = [[Position]],  
                    z = 32.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1820]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1956.25,  
                    x = 28249.21875,  
                    InstanceId = [[Client1_1821]],  
                    Class = [[Position]],  
                    z = 31.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1823]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1947.09375,  
                    x = 28239.5625,  
                    InstanceId = [[Client1_1824]],  
                    Class = [[Position]],  
                    z = 32.21875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1814]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[2Begin Guard Zone]],  
              InstanceId = [[Client1_1839]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1838]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1841]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1947.546875,  
                    x = 28036.375,  
                    InstanceId = [[Client1_1842]],  
                    Class = [[Position]],  
                    z = 2.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1844]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1968.34375,  
                    x = 28040.25,  
                    InstanceId = [[Client1_1845]],  
                    Class = [[Position]],  
                    z = 4.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1847]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1963.3125,  
                    x = 28021.15625,  
                    InstanceId = [[Client1_1848]],  
                    Class = [[Position]],  
                    z = 3.234375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[3Continue Guard Zone]],  
              InstanceId = [[Client1_1864]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1863]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1866]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2026.140625,  
                    x = 28122.45313,  
                    InstanceId = [[Client1_1867]],  
                    Class = [[Position]],  
                    z = 17.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1869]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2031.15625,  
                    x = 28146.875,  
                    InstanceId = [[Client1_1870]],  
                    Class = [[Position]],  
                    z = 16.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1872]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2042.21875,  
                    x = 28122.5625,  
                    InstanceId = [[Client1_1873]],  
                    Class = [[Position]],  
                    z = 16.9375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[4Finish Guard Zone]],  
              InstanceId = [[Client1_1875]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1874]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1877]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1982.90625,  
                    x = 28253.78125,  
                    InstanceId = [[Client1_1878]],  
                    Class = [[Position]],  
                    z = 30.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1880]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1998.84375,  
                    x = 28297.65625,  
                    InstanceId = [[Client1_1881]],  
                    Class = [[Position]],  
                    z = 29.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1883]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1962.09375,  
                    x = 28286.04688,  
                    InstanceId = [[Client1_1884]],  
                    Class = [[Position]],  
                    z = 32.359375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[1Exit Guard Zone]],  
              InstanceId = [[Client1_1886]],  
              Class = [[Region]],  
              Position = {
                y = -0.578125,  
                x = 0.015625,  
                InstanceId = [[Client1_1885]],  
                Class = [[Position]],  
                z = 0.140625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1888]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1951.5625,  
                    x = 27907.42188,  
                    InstanceId = [[Client1_1889]],  
                    Class = [[Position]],  
                    z = -8.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1891]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1930.265625,  
                    x = 27937.10938,  
                    InstanceId = [[Client1_1892]],  
                    Class = [[Position]],  
                    z = -6.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1894]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1915.125,  
                    x = 27921.6875,  
                    InstanceId = [[Client1_1895]],  
                    Class = [[Position]],  
                    z = -10.65625
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_1136]]
        },  
        {
          InstanceId = [[Client1_1260]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Ceremony Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1259]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_1240]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1238]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[exit]],  
                    InstanceId = [[Client1_1762]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[40]],  
                        InstanceId = [[Client1_1763]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_1590]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1770]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1777]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1775]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_1619]]),  
                        ActivityZoneId = r2.RefId([[Client1_1572]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[begin]],  
                    InstanceId = [[Client1_1764]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1825]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_1630]]),  
                        ActivityZoneId = r2.RefId([[Client1_1793]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1826]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_1639]]),  
                        ActivityZoneId = r2.RefId([[Client1_1561]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq3]],  
                    InstanceId = [[Client1_1765]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Seq4]],  
                    InstanceId = [[Client1_1766]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Seq5]],  
                    InstanceId = [[Client1_1767]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Seq6]],  
                    InstanceId = [[Client1_1768]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Seq7]],  
                    InstanceId = [[Client1_1769]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[begin]],  
                    InstanceId = [[Client1_1590]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 9,  
                        InstanceId = [[Client1_1591]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1592]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1593]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1595]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1596]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1601]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1603]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1604]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1605]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1607]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1608]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1609]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1611]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1612]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1613]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1615]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1616]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1617]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1771]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1772]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1773]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[EXIT1]],  
                    InstanceId = [[Client1_1619]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_1620]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1621]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1624]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_1626]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1627]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1628]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[exit2]],  
                    InstanceId = [[Client1_1630]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_1631]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1632]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1633]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 9,  
                        InstanceId = [[Client1_1643]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1644]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1645]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[begi1]],  
                    InstanceId = [[Client1_1639]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_1640]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1641]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1647]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[begi2]],  
                    InstanceId = [[Client1_1649]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_1650]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1651]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1654]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[cont1]],  
                    InstanceId = [[Client1_1656]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_1657]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1658]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1659]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[cont2]],  
                    InstanceId = [[Client1_1661]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_1662]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_1663]],  
                            Who = [[Client1_1240]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1664]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1513]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_1512]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1514]],  
                          Value = r2.RefId([[Client1_1517]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1515]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1903]],  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_1902]],  
                      Value = r2.RefId([[Client1_1771]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1904]],  
                          Value = r2.RefId([[Client1_1896]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1905]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1788]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1791]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1790]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ceremony Master]],  
              Position = {
                y = -1876.734375,  
                x = 27908.625,  
                InstanceId = [[Client1_1241]],  
                Class = [[Position]],  
                z = -11.28125
              },  
              Angle = 0,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
                [[Client1_1762]],  
                [[Client1_1764]],  
                [[Client1_1765]],  
                [[Client1_1766]],  
                [[Client1_1767]],  
                [[Client1_1768]],  
                [[Client1_1769]]
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1256]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1254]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6936622,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Ceremony Guard]],  
              Position = {
                y = -1874.671875,  
                x = 27907.17188,  
                InstanceId = [[Client1_1257]],  
                Class = [[Position]],  
                z = -11.9375
              },  
              JacketModel = 6736942,  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1252]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6735406,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6735918,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1250]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6734894,  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Tattoo = 12,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6936622,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Ceremony Guard]],  
              Position = {
                y = -1878.078125,  
                x = 27907.75,  
                InstanceId = [[Client1_1253]],  
                Class = [[Position]],  
                z = -10.984375
              },  
              JacketModel = 6736942,  
              ArmModel = 6736430,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1258]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_1474]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Security Group]],  
          Position = {
            y = -91.5625,  
            x = -400.84375,  
            InstanceId = [[Client1_1473]],  
            Class = [[Position]],  
            z = -46.71875
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1411]],  
              ActivitiesId = {
                [[Client1_1438]],  
                [[Client1_1517]],  
                [[Client1_1896]],  
                [[Client1_1898]],  
                [[Client1_1900]]
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 14,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 14,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1409]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[1exit]],  
                    InstanceId = [[Client1_1438]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1439]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1886]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[Homes]],  
                    InstanceId = [[Client1_1517]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1518]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1216]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[2begi]],  
                    InstanceId = [[Client1_1896]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1897]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1839]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[3cont]],  
                    InstanceId = [[Client1_1898]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1899]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1864]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[4fini]],  
                    InstanceId = [[Client1_1900]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1901]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1875]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1903]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1906]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1905]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1788]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1909]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1908]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = 3,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Tattoo = 18,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6767150,  
              Sex = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Security Guard Leader]],  
              Position = {
                y = -1827.46875,  
                x = 28323.67188,  
                InstanceId = [[Client1_1412]],  
                Class = [[Position]],  
                z = 35.90625
              },  
              JacketModel = 6728750,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InstanceId = [[Client1_1419]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1417]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Kami]],  
              Position = {
                y = -1851.375,  
                x = 28314.67188,  
                InstanceId = [[Client1_1420]],  
                Class = [[Position]],  
                z = 37.140625
              },  
              Angle = 3,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1415]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1413]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Security Kami]],  
              Position = {
                y = -1838.828125,  
                x = 28333.5625,  
                InstanceId = [[Client1_1416]],  
                Class = [[Position]],  
                z = 39.078125
              },  
              Angle = 3.265625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1403]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1401]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6767150,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1836.71875,  
                x = 28321.70313,  
                InstanceId = [[Client1_1404]],  
                Class = [[Position]],  
                z = 36.375
              },  
              JacketModel = 6728750,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1407]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1405]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = 3,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6767150,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1837.84375,  
                x = 28318.65625,  
                InstanceId = [[Client1_1408]],  
                Class = [[Position]],  
                z = 36.140625
              },  
              JacketModel = 6728750,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1399]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1397]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6767150,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1835.90625,  
                x = 28324.67188,  
                InstanceId = [[Client1_1400]],  
                Class = [[Position]],  
                z = 36.765625
              },  
              JacketModel = 6728750,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1395]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1393]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6767150,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1834.015625,  
                x = 28326.8125,  
                InstanceId = [[Client1_1396]],  
                Class = [[Position]],  
                z = 37.25
              },  
              JacketModel = 6728750,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1391]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1389]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -3.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6767150,  
              Sex = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Security Guard]],  
              Position = {
                y = -1832.828125,  
                x = 28329.82813,  
                InstanceId = [[Client1_1392]],  
                Class = [[Position]],  
                z = 37.703125
              },  
              JacketModel = 6728750,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1472]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_1533]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Civilian Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1532]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1525]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1523]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_1755]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Chat2]],  
                    InstanceId = [[Client1_1756]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Chat3]],  
                    InstanceId = [[Client1_1757]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Chat4]],  
                    InstanceId = [[Client1_1758]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Chat5]],  
                    InstanceId = [[Client1_1759]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Chat6]],  
                    InstanceId = [[Client1_1760]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  },  
                  {
                    Name = [[Chat7]],  
                    InstanceId = [[Client1_1761]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Angle = -2.359375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 6,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 0,  
              Sex = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Krannus]],  
              Position = {
                y = -1873.171875,  
                x = 27910.3125,  
                InstanceId = [[Client1_1526]],  
                Class = [[Position]],  
                z = -11.703125
              },  
              JacketModel = 5606446,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1529]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1527]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = -2.125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 2,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[Kaori]],  
              Position = {
                y = -1873.71875,  
                x = 27912.67188,  
                InstanceId = [[Client1_1530]],  
                Class = [[Position]],  
                z = -11.3125
              },  
              JacketModel = 0,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1521]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1519]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = -2.203125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 2,  
              MorphTarget3 = 3,  
              MorphTarget7 = 4,  
              WeaponRightHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[Kyallih]],  
              Position = {
                y = -1875.1875,  
                x = 27911.53125,  
                InstanceId = [[Client1_1522]],  
                Class = [[Position]],  
                z = -11.28125
              },  
              JacketModel = 0,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1531]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 1,  
      InstanceId = [[Client1_1135]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_1133]],  
    Texts = {
      {
        Count = 5,  
        InstanceId = [[Client1_1593]],  
        Class = [[TextManagerEntry]],  
        Text = [[Welcome to the Anti-Yubo Ceremony.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1594]],  
        Class = [[TextManagerEntry]],  
        Text = [[Welcome to the Anti-Yubo Ceremony.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1597]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please listen to what I am going to say.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1598]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please listen to what I am going to say.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1599]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please listen to what I am going to tell you.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1600]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please listen to what I am going to tell you.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1601]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please listen carefully.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1602]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please listen carefully.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1605]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have 4 special trained Yubo.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1606]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have 4 special trained Yubo.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1609]],  
        Class = [[TextManagerEntry]],  
        Text = [[They each have a Tower.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1610]],  
        Class = [[TextManagerEntry]],  
        Text = [[They each have a Tower.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1613]],  
        Class = [[TextManagerEntry]],  
        Text = [[Your task in this Ceremony is to kill them all.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1614]],  
        Class = [[TextManagerEntry]],  
        Text = [[Your task in this Ceremony is to kill them all.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_1617]],  
        Class = [[TextManagerEntry]],  
        Text = [[We ask you not to disturb the Ceremony.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1618]],  
        Class = [[TextManagerEntry]],  
        Text = [[We ask you not to disturb the Ceremony.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1622]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is the first Yubo]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1623]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is the first Yubo]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_1624]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is the first Yubo.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1625]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is the first Yubo.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_1628]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please kill him to continue.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1629]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please kill him to continue.]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_1633]],  
        Class = [[TextManagerEntry]],  
        Text = [[Well done! Please follow!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1634]],  
        Class = [[TextManagerEntry]],  
        Text = [[Well done! Please follow!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_1642]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_1645]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please do not kill them before I arrive.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1646]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please do not kill them before I arrive.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1647]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please kill the second Yubo to continue.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1648]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please kill the second Yubo to continue.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1652]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, please follow me.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1653]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, please follow me.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1654]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you. Please follow me.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1655]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you. Please follow me.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1659]],  
        Class = [[TextManagerEntry]],  
        Text = [[And here is the second Yubo.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1660]],  
        Class = [[TextManagerEntry]],  
        Text = [[And here is the second Yubo.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1664]],  
        Class = [[TextManagerEntry]],  
        Text = [[And now the last one.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1665]],  
        Class = [[TextManagerEntry]],  
        Text = [[And now the last one.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1773]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please follow me.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1774]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please follow me.]]
      }
    }
  }
}