scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Position = 0,  
    TextManager = 0,  
    NpcCustom = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 20,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Title = [[Ceremony]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1139]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Exit Tower]],  
              Position = {
                y = -1967.8125,  
                x = 27940.65625,  
                InstanceId = [[Client1_1142]],  
                Class = [[Position]],  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1143]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 1]],  
              Position = {
                y = -1851.296875,  
                x = 27899.29688,  
                InstanceId = [[Client1_1146]],  
                Class = [[Position]],  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1147]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 1]],  
              Position = {
                y = -1851.203125,  
                x = 27895.9375,  
                InstanceId = [[Client1_1150]],  
                Class = [[Position]],  
                z = -13.875
              },  
              Angle = -2.203125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1151]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Tower 1]],  
              Position = {
                y = -1850.9375,  
                x = 27931.39063,  
                InstanceId = [[Client1_1154]],  
                Class = [[Position]],  
                z = -14.5
              },  
              Angle = -1.71875,  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1155]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 2]],  
              Position = {
                y = -1844.8125,  
                x = 27922.95313,  
                InstanceId = [[Client1_1158]],  
                Class = [[Position]],  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1159]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 3]],  
              Position = {
                y = -1853.234375,  
                x = 27937.125,  
                InstanceId = [[Client1_1162]],  
                Class = [[Position]],  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1163]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 1]],  
              Position = {
                y = -1838.734375,  
                x = 27892.96875,  
                InstanceId = [[Client1_1166]],  
                Class = [[Position]],  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1167]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 2]],  
              Position = {
                y = -1840.8125,  
                x = 27937.21875,  
                InstanceId = [[Client1_1170]],  
                Class = [[Position]],  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1173]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1171]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 1]],  
              Position = {
                y = -1856.328125,  
                x = 27930.85938,  
                InstanceId = [[Client1_1174]],  
                Class = [[Position]],  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1177]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1175]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 2]],  
              Position = {
                y = -1853.578125,  
                x = 27898.125,  
                InstanceId = [[Client1_1178]],  
                Class = [[Position]],  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1181]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1179]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 2]],  
              Position = {
                y = -1887.59375,  
                x = 27942.53125,  
                InstanceId = [[Client1_1182]],  
                Class = [[Position]],  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1185]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1183]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn House 3]],  
              Position = {
                y = -1901.703125,  
                x = 27941.15625,  
                InstanceId = [[Client1_1186]],  
                Class = [[Position]],  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1189]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1187]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Landslide 4]],  
              Position = {
                y = -1896.890625,  
                x = 27943.17188,  
                InstanceId = [[Client1_1190]],  
                Class = [[Position]],  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1193]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1191]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Fire 3]],  
              Position = {
                y = -1900.859375,  
                x = 27939.875,  
                InstanceId = [[Client1_1194]],  
                Class = [[Position]],  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1197]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1195]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Spawn Point]],  
              Position = {
                y = -1878.25,  
                x = 27915.09375,  
                InstanceId = [[Client1_1198]],  
                Class = [[Position]],  
                z = -10.640625
              },  
              Angle = 1.625,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1209]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1207]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Home Circle 1]],  
              Position = {
                y = -1877.34375,  
                x = 27895.6875,  
                InstanceId = [[Client1_1210]],  
                Class = [[Position]],  
                z = -13.0625
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1213]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1211]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burn Goo 3]],  
              Position = {
                y = -1893.28125,  
                x = 27964.14063,  
                InstanceId = [[Client1_1214]],  
                Class = [[Position]],  
                z = -13.71875
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1216]],  
              Name = [[Home Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1218]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1219]],  
                    x = 27932.96875,  
                    y = -1906.75,  
                    z = -7.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1221]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1222]],  
                    x = 27932.32813,  
                    y = -1881.984375,  
                    z = -9.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1224]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1225]],  
                    x = 27935.84375,  
                    y = -1863.34375,  
                    z = -11.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1227]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1228]],  
                    x = 27890.98438,  
                    y = -1861.46875,  
                    z = -14.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1230]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1231]],  
                    x = 27883.375,  
                    y = -1869.015625,  
                    z = -13.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1233]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1234]],  
                    x = 27887.78125,  
                    y = -1903.015625,  
                    z = -10.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1236]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1237]],  
                    x = 27898.15625,  
                    y = -1901.34375,  
                    z = -7.859375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1215]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_1136]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1260]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1240]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1241]],  
                x = 27908.625,  
                y = -1876.734375,  
                z = -11.28125
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1238]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ceremony Master]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1256]],  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1257]],  
                x = 27907.17188,  
                y = -1874.671875,  
                z = -11.9375
              },  
              Angle = -0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1254]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 7,  
              HairType = 5623854,  
              HairColor = 4,  
              Tattoo = 10,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 7,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6736686,  
              TrouserModel = 6735918,  
              FeetModel = 6734638,  
              HandsModel = 6735150,  
              ArmModel = 6736174,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 0,  
              WeaponRightHand = 6936622,  
              WeaponLeftHand = 0,  
              Name = [[Ceremony Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_acid_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1252]],  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1253]],  
                x = 27907.75,  
                y = -1878.078125,  
                z = -10.984375
              },  
              Angle = -0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1250]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 5,  
              HairType = 8238,  
              HairColor = 5,  
              Tattoo = 9,  
              EyesColor = 1,  
              MorphTarget1 = 4,  
              MorphTarget2 = 7,  
              MorphTarget3 = 6,  
              MorphTarget4 = 7,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 6736942,  
              TrouserModel = 6735662,  
              FeetModel = 6734638,  
              HandsModel = 6735406,  
              ArmModel = 6736430,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 2,  
              WeaponRightHand = 6936622,  
              WeaponLeftHand = 0,  
              Name = [[Ceremony Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_acid_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1259]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1258]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 1,  
      InstanceId = [[Client1_1135]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_1133]],  
    Texts = {
    }
  }
}