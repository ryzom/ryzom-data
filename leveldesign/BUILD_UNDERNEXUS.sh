#!/bin/bash

source ../scripts/utils.sh
#../scripts/pipeline_setup.sh

cp -r D:/repos/ryzom-data/leveldesign/world R:/leveldesign/world
cp /d/repos/ryzom-data/leveldesign/landscape/primes_racines/undernexus.land /r/graphics/landscape/ligo/primes_racines


cd /r/code/nel/tools/build_gamedata
"$SCRIPT_DIR"/call_python.sh 2_build.py -ipj continents/undernexus -ipc ai_wmap

read