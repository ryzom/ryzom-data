#!/bin/bash

source /r/scripts/utils.sh
#/r/scripts/pipeline_setup.sh

cd /r/pipeline/build_gamedata
"$SCRIPT_DIR"/call_python.sh 1_export.py -ipj ecosystems/primes_racines
"$SCRIPT_DIR"/call_python.sh 2_build.py -ipj ecosystems/primes_racines
read