#!/usr/bin/env python
import os, sys, shutil, glob
import xml.etree.ElementTree as ET
import socket
import humanize

tree = ET.parse("live.hist")
root = tree.getroot()
total_size = 0
total_patchssize = 0
for item in root.findall("_Files"):
	last_version = 0
	filesize = 0
	patchssize = 0
	all_versions = {}
	delete_versions = []
	name = item.find("_FileName").attrib["value"]
	filevers = item.findall("_Versions")
	filevers.reverse()
	print("Managing %s" % (name[:-4]))
	for vers in filevers:
		for v in vers:
			if v.tag == "_VersionNumber":
				version = int(v.attrib["value"])
				if version > last_version :
					last_version = version
			if filesize == 0 and v.tag == "_FileSize":
				filesize  = int(v.attrib["value"])
			if v.tag == "_PatchSize":
				patchsize  = int(v.attrib["value"])
		#print(version, patchsize, filesize)
		all_versions[version] = vers
		patchssize += patchsize
		if patchssize > filesize and version != last_version :
			print("-", version)
			delete_versions.append(version)
		item.remove(vers)
	for delete_version in delete_versions:
		del(all_versions[delete_version])
	total_size += filesize
	total_patchssize += patchssize

	for version, infos in all_versions.items():
		item.append(infos)
		version_path = "patch_live/%05d" % (version)
		if not os.path.exists("new_"+version_path):
			os.mkdir("new_"+version_path)
			if os.path.exists(version_path+"/ryzom_%05d.idx" % (version)):
				shutil.copy(version_path+"/ryzom_%05d.idx" % (version), "new_"+version_path+"/")
			if os.path.exists(version_path+"/ryzom_%05d_debug.xml" % (version)):
				shutil.copy(version_path+"/ryzom_%05d_debug.xml" % (version), "new_"+version_path+"/")
		for filename in glob.glob("patch_live/%05d/%s_%05d.*" % (version, name[:-4],  version)):
			shutil.copy(filename, "new_"+version_path+"/")
			print(".", end="")
			sys.stdout.flush()
		for filename in glob.glob("patch_live/%05d/%s.*" % (version, name[:-4])):
			shutil.copy(filename, "new_"+version_path+"/")
			print(".", end="")
			sys.stdout.flush()
	print("")
	
print(last_version, all_versions)
print(humanize.naturalsize(total_size), humanize.naturalsize(total_patchssize))
tree.write("live.xml")



def useless():
	print("un7z %05d/%s" % (last_version, name))
	os.system("7z x patch_live/%05d/%s.lzma -oref_live/" % (last_version, name))
	shutil.move("ref_live/"+name, "ref_live/%s_%05i%s" % (name[:-4], last_version, name[-4:]))
	if name == "leveldesign.bnp":
		os.system("bnp_make -u ref_live/leveldesign_%05i.bnp" % last_version)
		shutil.copy("leveldesign_%05i/sheet_id.bin" % last_version, "/home/nevrax/repos/ryzom-private-data/game_element/sheet_id.bin")


