#!/usr/bin/env python3

import os, sys

patch_live_path = sys.argv[1]

files = os.listdir(patch_live_path)
versions = {}

for bnp in files:
		filename, ext = bnp.split(".")
		name = filename[:-5]+"."+ext

		version = int(filename[-5:])
		if not name in versions:
			versions[name] = (version, filename[-5:], filename[:-5], ext)
		else:
			if version > versions[name][0]:
				versions[name] = (version, filename[-5:], filename[:-5], ext)

for bnp, version in versions.items():
		print(patch_live_path+"/"+version[2]+version[1]+"."+version[3])
