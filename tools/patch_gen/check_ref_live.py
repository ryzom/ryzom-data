#!/usr/bin/env python3

################################################################################
#
# Check the ref_live using live.hist
# usage : xxx path_to_patchs path_to_packed
#
################################################################################

import os, sys, json, shutil
import subprocess
import xml.etree.ElementTree as ET

ref_live = sys.argv[1]+"/ref_live"
packed = sys.argv[2]
stats_file = "ref_live.stats"

tree = ET.parse("live.hist")
root = tree.getroot()

if os.path.isfile(stats_file):
	with open(stats_file, "r") as f:
		stats = json.load(f)
else:
	stats = {}

items = root.findall("_Files")
nbr_files = len(items)
i = 0
diff_out = ""
for item in items:
	last_version = 0
	name = item.find("_FileName").attrib["value"]
	basename =  name[:-4]
	extenstion = name[-4:]
	print("[%3d%%] Checking <span style='color: orange'>%s</span>..." % (((100*i)/nbr_files), name), end=" ")
	i += 1
	for vers in item.findall("_Versions"):
		for v in vers:
			if v.tag == "_VersionNumber":
				version = int(v.attrib["value"])
				if version > last_version :
					last_version = version

	filename = "%s_%s" % (basename, last_version)
	fullname = "%s/%s_%05d%s" % (ref_live, basename, last_version, extenstion)
	if filename in stats:
		(md5A, sizeA) = stats[filename]
	else:
		md5A = ""

	if md5A == "" or sizeA != os.path.getmtime(fullname):
		print("Generating %s" % filename)
		md5A = subprocess.check_output("md5sum %s/%s_%05d%s" % (ref_live, basename, last_version, extenstion), shell=True).decode("utf-8").split(" ")[0]
		stats[filename] = (md5A, os.path.getmtime(fullname))

	bnpname = "%s/%s" % (packed, name)
	if os.path.isfile(bnpname):
		md5B = subprocess.check_output("md5sum %s" % bnpname, shell=True).decode("utf-8").split(" ")[0]
		if md5A != md5B:
			print("<span style='color: red'>DIFF</span>")
			diff_out += "<span style='color:orange'>%s</span> are different from REF and NEW PACKED. Listing diffs:\n" % basename
			sys.stdout.flush()
			os.chdir(sys.argv[1]+"/tmp")
			shutil.rmtree("A", ignore_errors=True)
			shutil.rmtree("B", ignore_errors=True)
			shutil.copyfile(fullname, "A.bnp")
			os.system("bnp_make -u A.bnp")
			shutil.copyfile(bnpname, "B.bnp")
			os.system("bnp_make -u B.bnp")
			os.system("diff A B > bnp_diff")
			with open("bnp_diff", "r") as f:
				for line in f.read().split("\n"):
					diff_out += "<span style='color: white'>"+line.replace("A/", "").replace("B/", "")+"</span>\n"
		else:
			print("<span style='color: green'>OK</span>")
	else:
		print("Missing in %s" % packed)
	sys.stdout.flush()
print()
print(diff_out)
sys.stdout.flush()

with open(stats_file, "w") as f:
	json.dump(stats, f)








