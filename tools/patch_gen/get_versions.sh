cd ~/repos/ryzom-core/ryzom/client/src
git rev-list HEAD --count .
cd ~/repos/ryzom-core/ryzom/server/src
git rev-list HEAD --count .
cd ~/repos/ryzom-data/
git rev-list HEAD --count .
cd ~/repos/ryzom-private-data/
git rev-list HEAD --count .
cat ~/patchs/patch_live/ryzom_staging.version | cut -d" " -f1

