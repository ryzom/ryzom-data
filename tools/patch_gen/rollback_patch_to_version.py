#!/usr/bin/env python
import os, sys, shutil
import xml.etree.ElementTree as ET
import socket

if len(sys.argv) < 2:
	with open("patch_live/ryzom_live.version") as f:
		version = int(f.read().split(" ")[0])
else:
	version = int(sys.argv[1])

history_file = ""

if os.path.isfile("history/live.hist.%i" % version):
	history_file = "history/live.hist.%i" % version

if os.path.isfile("history/live/%i" % version):
	history_file = "history/live/%i" % version

if history_file:
	print("Using live.hist from version %i" % version)
	#Backup current files
	print("Backup live.hist and ref_live")

	if os.path.isdir("ref_live.bk"):
		shutil.rmtree("ref_live.bk")

	shutil.move("ref_live", "ref_live.bk")
	os.makedirs("ref_live")

	print("Get wanted live.hist")
	shutil.copy(history_file, "live.hist")

	if not os.path.isdir("patch_live/olds"):
		os.makedirs("patch_live/olds")

	next_version = version + 1
	while os.path.isdir("patch_live/%05i" % next_version):
		print("Move patch_live version %05i into olds" % next_version)
		if os.path.isdir("patch_live/olds/%05i" % next_version):
			shutil.rmtree("patch_live/olds/%05i" % next_version)
		shutil.move("patch_live/%05i" % next_version, "patch_live/olds/")
		next_version = next_version + 1

	if socket.gethostname() == "Yubo":
		open("patch_live/ryzom_dev.version", "w").write("%i %i" % (version, version))
	elif socket.gethostname() == "Gingo":
		open("patch_live/ryzom_test.version", "w").write("%i %i" % (version, version))

	tree = ET.parse("live.hist")
	root = tree.getroot()
	for item in root.findall("_Files"):
		last_version = 0
		name = item.find("_FileName").attrib["value"]
		for vers in item.findall("_Versions"):
			for v in vers:
				if v.tag == "_VersionNumber":
					version = int(v.attrib["value"])
					if version > last_version :
						last_version = version

		print("un7z %05d/%s" % (last_version, name))
		os.system("7z x patch_live/%05d/%s.lzma -oref_live/" % (last_version, name))
		shutil.move("ref_live/"+name, "ref_live/%s_%05i%s" % (name[:-4], last_version, name[-4:]))
		if name == "leveldesign.bnp":
			os.system("bnp_make -u ref_live/leveldesign_%05i.bnp" % last_version)
			shutil.copy("leveldesign_%05i/sheet_id.bin" % last_version, "/home/nevrax/repos/ryzom-private-data/game_element/sheet_id.bin")


