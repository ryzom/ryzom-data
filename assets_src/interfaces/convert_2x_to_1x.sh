#!/bin/bash

function convert_image {
	cd ${1}_2x
	for a in  *.png
	do
		if [ ! -f ../$1/$a ]
		then
			echo $a
			magick $a -resize 50% ../$1/$a
		fi
	done
	cd ..
}

#convert_image texture_interfaces_dxtc_2x/tags texture_interfaces_dxtc/tags
convert_image texture_interfaces_dxtc
convert_image texture_interfaces_v3


