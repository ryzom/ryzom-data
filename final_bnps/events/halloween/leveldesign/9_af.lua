-- GROUP: maro_helloween_stable
-- 980d672fe8baab45f89629a1ff733be47a1d4820
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 951.006, -1359.182, 49.566, 0, 0, 0, '{"scale z":1,"col size z":8.223,"scale y":1,"scale x":1,"col size y":3.033,"col pos y":-1372.528,"col pos x":51.796,"col size x":3.014,"col orientation":0}')

-- ac4f11b0045a1db0f98a329a5433781f87819b1d
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 928.854, -1389.183, 50.026, 0, 0, 0, '{"scale z":1,"col size z":8.223,"scale y":1,"scale x":1,"col size y":3.033,"col pos y":-1389.183,"col pos x":28.886,"col size x":3.014,"col orientation":0}')

-- 9f224985a2a79bb4d5333bffd71557d5e0eb833e
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 884.461, -1300.673, 51.092, 0, 0, 0, '{"scale z":1,"col size z":8.223,"scale y":1,"scale x":1,"col size y":3.033,"col pos y":-1300.673,"col pos x":-19.269,"col size x":3.014,"col orientation":0}')

-- fc4d187111653b6b3348f94573b375ef939fc4ed
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 909.227, -1316.33, 52.272, 0, 0, 0, '{"scale z":1,"col size z":8.223,"scale y":1,"scale x":1,"col size y":3.033,"col pos y":-1316.33,"col pos x":5.582,"col size x":3.014,"col orientation":-0}')

-- fbca11f55d252c60321e13c94b862d1db0fce314
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 894.557, -1361.98, 50.293, 0, 0, -1.85, '{"scale z":1,"col size z":8.223,"scale y":1,"scale x":1,"col size y":3.033,"col pos y":-1361.98,"col pos x":894.557,"col size x":3.014,"col orientation":0}')

-- 285429ad3cba45abb1588e65ef864dcf07ab4693
SceneEditor:spawnShape("Gen_Bt_Ecurie_villefyros_est.shape", 892.677, -1350.231, 50.909, -0, 0, 1.59, '{"scale z":1,"col size z":7.51,"scale y":1,"scale x":1,"col size y":7.85,"col pos y":-1350.231,"col pos x":892.677,"col size x":7.29,"col orientation":10.98}')

-- 24af64bf16f596c13926623c12cab8c910879e98
SceneEditor:spawnShape("fy_burnedtree_a.ps", 893.227, -1356.407, 55.261, 1.39, -0.34, -0, '{"scale z":0.735,"col size z":14.231,"scale y":0.735,"scale x":0.735,"col size y":10.994,"col pos y":-1361.841,"col pos x":-6.517,"col size x":9.978,"col orientation":0}')

-- ffcd796cf55a769e91e17b77b84961bfb266da5f
SceneEditor:spawnShape("fy_burnedtree_a.ps", 892.51, -1354.111, 54.527, 1.24, -0.2, 0, '{"scale z":0.735,"col size z":14.231,"scale y":0.735,"scale x":0.735,"col size y":10.994,"col pos y":-1367.457,"col pos x":-6.7,"col size x":9.978,"col orientation":0}')

-- 875b198dcfaac851b5a3a3bac17f468054453e6c
SceneEditor:spawnShape("fy_burnedtree_a.ps", 892.168, -1351.611, 54.002, 1.17, -0.16, 0, '{"scale z":0.802,"col size z":14.231,"scale y":0.802,"scale x":0.802,"col size y":10.994,"col pos y":-1351.611,"col pos x":-7.8,"col size x":9.978,"col orientation":0}')

-- 8e5d1dac76239e6afbbc1048fcd83fb12c04d328
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 891.89, -1351.29, 56.406, 0, 0, 4.8, '{"scale z":2.07,"col size z":8.223,"scale y":2.07,"scale x":2.07,"col size y":3.033,"col pos y":-1351.29,"col pos x":-11.84,"col size x":3.014,"col orientation":0}')

-- 7c48cf140728e4debe514c47a5c4e6188fa5792e
SceneEditor:spawnShape("ge_mission_pumpkin.ps", 892.084, -1342.499, 57.778, 0, 0, 0, '{"scale z":0.821,"col size z":8.223,"scale y":0.821,"scale x":0.821,"col size y":3.033,"col pos y":-1342.499,"col pos x":-11.561,"col size x":3.014,"col orientation":0}')