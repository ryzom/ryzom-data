-- GROUP: stable_fh
-- 7d83c5d3c18465945f386bafc3305787bd9e2964
SceneEditor:spawnShape("xmas_stable2022.ps", 17269.717, -32941.965, -2.1, 0, 0, 3.14, '{"scale y":1,"col pos y":-20621.965,"col size y":9.577,"col pos x":-15795.292,"scale z":1,"scale x":1,"col orientation":0,"col size z":9.389,"col size x":26.321}')


-- GROUP: fh_deco
-- 8474960931467816e34c2cf7bdacaafab9899228
SceneEditor:spawnShape("Ge_mission_Gift.shape", 17218.037, -32902.801, -2.755, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7101.963,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20582.801,"scale x":1}')

-- bbf5390f2aeac5581b448a9eb15062812c989b73
SceneEditor:spawnShape("Ge_mission_Gift.shape", 17218.293, -32901.203, -2.751, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7101.707,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20581.203,"scale x":1}')

-- 697412b396579abc7e2574dab9646549433a5e0a
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 17220.957, -32902.109, -2.546, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7099.043,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20582.109,"scale x":1}')

-- fb8811964acca886ee2c810a6fe2a848e7a50303
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 17220.281, -32900.988, -2.591, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7099.719,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20580.988,"scale x":1}')

-- c0606c7c6df8ce0665699f0fd277d7e40fb0ec01
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 17220.166, -32903.008, -2.585, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7099.834,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20583.008,"scale x":1}')

-- f202c8e3efebc026b28dfc4b9177eb665ef41215
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 17218.957, -32900.383, -2.703, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7101.043,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20580.383,"scale x":1}')

-- 01c096e7c3f0fefbd13722e601cc0cddc57ee3a3
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 17219.074, -32903.594, -2.653, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7100.926,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20583.594,"scale x":1}')

-- 1452ad6b4327c717f8424f6edafbe1ba39c35103
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 17236.945, -32857.77, 0.094, 0, 0, 0, '{"col orientation":0,"col size z":11.357,"col pos x":-24320,"scale z":1,"scale y":1,"col size y":8.525,"col size x":8.581,"col pos y":-20537.77,"scale x":1}')

-- 6f47dde424196e71f9f09545dd53c4adb7efc8dd
SceneEditor:spawnShape("ge_mission_snowman.ps", 17222.766, -32929.422, -1.556, 0, 0, 0, '{"col orientation":0,"col size z":3.66,"col pos x":-24320,"scale z":1,"scale y":1,"col size y":1.601,"col size x":1.527,"col pos y":-20609.422,"scale x":1}')

-- GROUP: fh_main
-- be02ef884ef8882a613d33ac9176f91740261588
SceneEditor:spawnShape("xmas_snowman.shape", 17166.082, -32928.574, 0.003, 0, 0, -2, '{"col size x":2.832,"col pos x":-5,"scale x":1,"scale y":1,"col pos y":-32928.574,"col orientation":0,"col size y":1.651,"scale z":1,"col size z":3.66,"context":null,"url":null}')

-- fb1446075b959e8a294db4eb8762d62ee0de6ed6
SceneEditor:spawnShape("snowhill.shape", 17165.953, -32928.582, -0.897, 0, 0, 0, '{"col size x":21.946,"col pos x":0,"scale x":0.45,"scale y":0.45,"col pos y":-32928.582,"col orientation":0,"col size y":21.572,"scale z":0.45,"col size z":5.142,"context":null,"url":null}')

-- 1afb5810180a17334bb3f4489b4ce3d08028c0df
SceneEditor:spawnShape("xmas_acc_candycane_star.shape", 17230.244, -32911.324, 10.787, 0, 0, 1.77, '{"col size x":0.455,"col pos x":-0.01,"scale x":4.02,"scale y":4.02,"col pos y":-32911.324,"col orientation":0,"col size y":0.12,"scale z":4.02,"col size z":1.947,"context":null,"url":null}')

-- 60786b63624cf03252d3e580bbe98a4db3a0480c
SceneEditor:spawnShape("xmas_lights_02.shape", 17166.43, -32928.477, -0.897, 0, 0, 0, '{"col size x":5.591,"col pos x":0,"scale x":1.711,"scale y":1.711,"col pos y":-32928.477,"col orientation":0,"col size y":5.521,"scale z":1.711,"col size z":2.333,"context":null,"url":null}')

-- c6260d86f888542642fcc388936da1e201d6511e
SceneEditor:spawnShape("xmas_gingeryubo.shape", 17172.01, -32930.371, -0.087, 1.5, 0, 5, '{"col size x":0.345,"col pos x":31.732,"scale x":1,"scale y":1,"col pos y":-32930.371,"col orientation":0,"col size y":0.028,"scale z":1,"col size z":0.252,"context":null,"url":null}')

-- 5a9718d9cbf9c2cbd1e70935fe389568d7ca5469
SceneEditor:spawnShape("xmas_gingeryubo.shape", 17171.512, -32929.559, -0.087, 1.54, 0, -0.89, '{"col size x":0.345,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-32929.559,"col orientation":0,"col size y":0.028,"scale z":1,"col size z":0.252,"context":null,"url":null}')

-- 8d600e18a5bf792a619b700cacaf29b52a65b740
SceneEditor:spawnShape("xmas_gingeryubo.shape", 17171.859, -32929.66, -0.087, 1.47, 0, 0, '{"col size x":0.345,"col pos x":39.113,"scale x":1,"scale y":1,"col pos y":-32929.66,"col orientation":-0.213,"col size y":0.028,"scale z":1,"col size z":0.252,"context":null,"url":null}')

-- fa9ac1c5c33d88dadb948105cec981fc4e95cdd3
SceneEditor:spawnShape("xmas_gingeryubo.shape", 17172.01, -32930.023, -0.077, 1.5, -0.8, -1.9, '{"col size x":0.345,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-32930.023,"col orientation":0,"col size y":0.028,"scale z":1,"col size z":0.252,"context":null,"url":null}')

-- ffbec092941c53cc4cbefdc465bc7c7d922078c7
SceneEditor:spawnShape("xmas_gingeryubo.shape", 17172.01, -32929.863, -0.087, 1.57, 0, -1, '{"col size x":0.345,"col pos x":18.248,"scale x":1,"scale y":1,"col pos y":-32929.863,"col orientation":0,"col size y":0.028,"scale z":1,"col size z":0.252,"context":null,"url":null}')

-- 19befe809c78dac42b533385dfc39f81e9194ffa
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17185.938, -32941.973, -1.889, 0, 0, -1.52, '{"col size x":3.161,"col pos x":-2.488,"scale x":1,"scale y":1,"col pos y":-32941.973,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- f6c55d9bd279732590c08a95fbb469e954a69613
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17179.701, -32948.188, -1.889, 0, 0, 2.73, '{"col size x":3.161,"col pos x":1.305,"scale x":1,"scale y":1,"col pos y":-32948.188,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- b5444913db73fc4d28b76604258c31566ad7d9ef
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17145.113, -32938.949, -1.889, 0, 0, 3.14, '{"col size x":3.161,"col pos x":-1.018,"scale x":1,"scale y":1,"col pos y":-32938.949,"col orientation":6.283,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- ea9e4bc5358b1b346ffafbd7a5b0dbf8ee94a3fa
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17142.836, -32930.543, -1.889, 0, 0, 0.36, '{"col size x":3.161,"col pos x":-0.836,"scale x":1,"scale y":1,"col pos y":-32930.543,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 83904666aad0bd5fdb60501aa4a053fa496c9724
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17168.086, -32905.492, -1.89, 0, 0, 0.91, '{"col size x":3.161,"col pos x":-0.889,"scale x":1,"scale y":1,"col pos y":-32905.492,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 88bd1e70d83def5c6e8c1f14b79a25fe6a260bb0
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17176.674, -32907.793, -1.89, 0, 0, -1.74, '{"col size x":3.161,"col pos x":0.609,"scale x":1,"scale y":1,"col pos y":-32907.793,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 373e0557375ffc2e71fdc8abecff343a6fcf8f64
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17186.027, -32918.617, -1.889, 0, 0, -0.1, '{"col size x":3.161,"col pos x":-0.484,"scale x":1,"scale y":1,"col pos y":-32918.617,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- b301f43553ca11ae18efd646cf8ca858b7f1f840
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17188.441, -32927.605, -1.889, 0, 0, -2.24, '{"col size x":3.161,"col pos x":0.24,"scale x":1,"scale y":1,"col pos y":-32927.605,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 2d581242ffa123397763d605e6beb83603b481a6
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17217.959, -32918.543, -1.871, 0, 0, 3.43, '{"col size x":3.161,"col pos x":0.381,"scale x":1,"scale y":1,"col pos y":-32918.543,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- e6e300d32d39a6bdc4d9c641ed9da25db2ded314
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17214.402, -32919.477, -1.835, 0, 0, 3.44, '{"col size x":3.161,"col pos x":0.855,"scale x":1,"scale y":1,"col pos y":-32919.477,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 4027aa6fda5684c02ba949d82934e2dd683111b5
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17210.76, -32920.457, -1.8, 0, 0, -2.88, '{"col size x":3.161,"col pos x":0.605,"scale x":1,"scale y":1,"col pos y":-32920.457,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 06abbaae333b48d69549c0adebe7fd2309a3e61d
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17205.266, -32921.941, -1.889, 0, 0, 3.41, '{"col size x":3.161,"col pos x":0.809,"scale x":1,"scale y":1,"col pos y":-32921.941,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 626c3c8c91f3a8650c37fd52fa8848454f7b026d
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17201.289, -32922.988, -1.889, 0, 0, 3.4, '{"col size x":3.161,"col pos x":0.662,"scale x":1,"scale y":1,"col pos y":-32922.992,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 4956fc2ed3d594739d0a2a349281589f4a71ef24
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17197.379, -32924.055, -1.889, 0, 0, 3.43, '{"col size x":3.161,"col pos x":0.107,"scale x":1,"scale y":1,"col pos y":-32924.055,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 23b438c38c830248ed8dd67eba2a247306332986
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17195.535, -32917.324, -1.889, 0, 0, 0.25, '{"col size x":3.161,"col pos x":0.783,"scale x":1,"scale y":1,"col pos y":-32917.324,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- ebddc2ef3678773a9b2a003a7ba67fcded866bdd
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17199.543, -32916.242, -1.889, 0, 0, 0.22, '{"col size x":3.161,"col pos x":-0.035,"scale x":1,"scale y":1,"col pos y":-32916.242,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 9689a8d26ce96532a0059525bff961185e301718
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17203.418, -32915.199, -1.889, 0, 0, 0.23, '{"col size x":3.161,"col pos x":0.092,"scale x":1,"scale y":1,"col pos y":-32915.199,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- d173bb933e6bf25aecc3c674f9744a554e2134b2
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17208.93, -32913.738, -1.819, 0, 0, 0.22, '{"col size x":3.161,"col pos x":0.314,"scale x":1,"scale y":1,"col pos y":-32913.738,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- c540f8e81a1828ba7205f8e9399e21e3ebfd2866
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17212.518, -32912.773, -1.835, 0, 0, 0.26, '{"col size x":3.161,"col pos x":0.014,"scale x":1,"scale y":1,"col pos y":-32912.773,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- ee9c8aa380ee9760ee1c2186a414ba4b154f3e90
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17216.16, -32911.789, -1.871, 0, 0, 6.56, '{"col size x":3.161,"col pos x":-0.004,"scale x":1,"scale y":1,"col pos y":-32915.191,"col orientation":0,"col size y":1.453,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 51f5cea76c4736e410b7e8f9b170205504606a25
SceneEditor:spawnShape("xmas_snowglobe.shape", 17169.637, -32921.078, 0.003, 0, 0, 3.2, '{"col size x":0.412,"col pos x":0,"scale x":0.5,"scale y":0.5,"col pos y":-32921.078,"col orientation":0,"col size y":0.412,"scale z":0.5,"col size z":0.519,"context":null,"url":null}')

-- afa461e41376d765ef43b31ce7564e9c81223d74
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 17167.807, -32920.594, 0.003, 0, 0, 0, '{"col size x":8.581,"col pos x":0,"scale x":0.081,"scale y":0.081,"col pos y":-32923.996,"col orientation":0,"col size y":8.525,"scale z":0.081,"col size z":10.379,"context":null,"url":null}')

-- be0ae2321a1ef18401c42c7fe8e7217895f2f964
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 17149.457, -32917.168, -1.89, 0, 0, 0, '{"col size x":8.581,"col pos x":0,"scale x":0.5,"scale y":0.5,"col pos y":-32917.168,"col orientation":0,"col size y":8.525,"scale z":0.5,"col size z":10.379,"context":null,"url":null}')

-- a0c600990694ce333fb83a2e5d39d11600b6d5f0
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 17157.203, -32949.32, -1.889, 0, 0, 0, '{"col size x":8.581,"col pos x":0,"scale x":0.5,"scale y":0.5,"col pos y":-32949.32,"col orientation":2.579,"col size y":8.525,"scale z":0.5,"col size z":10.379,"context":null,"url":null}')

-- 0edad39d5a1d270872e7edf471014b53b4b15289
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 17191.086, -32925.902, -1.889, 0, 0, 0, '{"col size x":8.581,"col pos x":-6.463,"scale x":0.5,"scale y":0.5,"col pos y":-32925.902,"col orientation":0,"col size y":8.525,"scale z":0.5,"col size z":10.379,"context":null,"url":null}')

-- c7766337eb853eacc626fc0e9c59f1896cc9a050
SceneEditor:spawnShape("xmas_lights_01.shape", 17177.076, -32925.707, -1.115, 0, 0, -1.32, '{"col size x":7.757,"col pos x":0,"scale x":0.72,"scale y":0.72,"col pos y":-32925.707,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 87b04e198769c2e4710079c5910d8013ba81a683
SceneEditor:spawnShape("xmas_lights_01.shape", 17174.301, -32920.91, -1.115, 0, 0, -0.79, '{"col size x":7.757,"col pos x":-4.541,"scale x":0.72,"scale y":0.72,"col pos y":-32920.91,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 6136674c2dd33e2cce1b79feb5494a8bd25bede8
SceneEditor:spawnShape("xmas_lights_01.shape", 17169.34, -32918.164, -1.115, 0, 0, -0.25, '{"col size x":7.757,"col pos x":-5.266,"scale x":0.72,"scale y":0.72,"col pos y":-32918.164,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- d45171d1032c0c472984a0bf3bdf3e50fffa27e8
SceneEditor:spawnShape("xmas_lights_01.shape", 17163.795, -32918.113, -1.115, 0, 0, 0.27, '{"col size x":7.757,"col pos x":-0.932,"scale x":0.72,"scale y":0.72,"col pos y":-32918.316,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- f32f8e7780283f9301a662f1db564c80f1612d2a
SceneEditor:spawnShape("xmas_lights_01.shape", 17158.932, -32920.953, -1.115, 0, 0, 0.79, '{"col size x":7.757,"col pos x":0.186,"scale x":0.72,"scale y":0.72,"col pos y":-32923.617,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 12ca42e3176a161cbb252ef8d4ffb2e75770d3c7
SceneEditor:spawnShape("xmas_lights_01.shape", 17156.102, -32925.824, -1.115, 0, 0, 1.31, '{"col size x":7.757,"col pos x":0,"scale x":0.72,"scale y":0.72,"col pos y":-32925.824,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 3e140422f1ec35e41a91808f3c359988473ec0a3
SceneEditor:spawnShape("xmas_lights_01.shape", 17156.098, -32931.449, -1.115, 0, 0, -1.3, '{"col size x":7.757,"col pos x":0,"scale x":0.72,"scale y":0.72,"col pos y":-32931.652,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- d97da1473599f9ecf5aabc924a70bc45d72a46d4
SceneEditor:spawnShape("xmas_lights_01.shape", 17158.922, -32936.316, -1.115, 0, 0, -0.8, '{"col size x":7.757,"col pos x":0,"scale x":0.72,"scale y":0.72,"col pos y":-32938.98,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 6794905e728f2133dad9136eca1666a651489aa5
SceneEditor:spawnShape("xmas_lights_01.shape", 17163.766, -32939.105, -1.115, 0, 0, -0.27, '{"col size x":7.757,"col pos x":7.66,"scale x":0.72,"scale y":0.72,"col pos y":-32939.105,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 304b1f83a0c4f5c5a9c297ce8efb6c9052b8a4b1
SceneEditor:spawnShape("xmas_lights_01.shape", 17169.352, -32939.117, -1.115, 0, 0, 0.3, '{"col size x":7.757,"col pos x":-2.109,"scale x":0.72,"scale y":0.72,"col pos y":-32939.32,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 2ea1d0eb7b79a19f3199037c82d71f4fe1b1b8ae
SceneEditor:spawnShape("xmas_lights_01.shape", 17174.266, -32936.207, -1.115, 0, 0, 0.8, '{"col size x":7.757,"col pos x":0,"scale x":0.72,"scale y":0.72,"col pos y":-32937.988,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 2e7a003d8aa72e0e9a293edf66e26531c43d627b
SceneEditor:spawnShape("xmas_lights_01.shape", 17177.109, -32931.426, -1.115, 0, 0, 1.32, '{"col size x":7.757,"col pos x":0,"scale x":0.72,"scale y":0.72,"col pos y":-32931.426,"col orientation":0,"col size y":0.606,"scale z":0.72,"col size z":2.323,"context":null,"url":null}')

-- 1c95708d298e55ea80dc2b4471e89f8fee1d4469
SceneEditor:spawnShape("ge_mission_gift-red.shape", 17162.801, -32931.578, -0.897, 0, 0, 0, '{"col size x":0.704,"col pos x":0,"scale x":0.59,"scale y":0.59,"col pos y":-32931.613,"col orientation":0,"col size y":0.704,"scale z":0.59,"col size z":0.943,"context":null,"url":null}')

-- 2931e8d3d015ceb5fececd3ec080baa48c21f1a4
SceneEditor:spawnShape("ge_mission_gift-green.shape", 17160.82, -32930.305, -0.897, 0, 0, 0, '{"col size x":0.704,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-32930.305,"col orientation":-1.402,"col size y":0.704,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- 024759c14fa551fcb6fd86f024fe4df538db0ae3
SceneEditor:spawnShape("ge_mission_gift-purple.shape", 17161.77, -32932.273, -0.897, 0, 0, 0, '{"col size x":0.704,"col pos x":0,"scale x":0.66,"scale y":0.66,"col pos y":-32932.273,"col orientation":0,"col size y":0.704,"scale z":0.66,"col size z":0.943,"context":null,"url":null}')

-- 037e181c2fe170f1e6da2682859020e7ff210a69
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 17161.904, -32931.059, -0.787, 0, 0, 0, '{"col size x":5.887,"col pos x":0,"scale x":0.3,"scale y":0.3,"col pos y":-32929.516,"col orientation":0,"col size y":6.025,"scale z":0.3,"col size z":10.225,"context":null,"url":null}')


