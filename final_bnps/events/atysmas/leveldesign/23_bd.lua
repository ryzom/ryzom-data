-- GROUP: yrk_deco
-- 929cb14012c79bf91f169f9565182d1d162e5fc1
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4795.213, -3530.566, -8.929, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":4.79,"col size y":8.525,"scale y":1,"col pos y":8789.434,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- e1e773e572201b6d2e7d86bc7bbe0471e5eadb8f
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4792.485, -3643.227, -9.512, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":3.363,"col size y":8.525,"scale y":1,"col pos y":8676.773,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- 58dfe14543cfac969202000dc7dde5b8fd344dd3
SceneEditor:spawnShape("ge_mission_snowman.ps", 4644.061, -3573.866, -9.484, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":8746.134,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 7ef05eb63f70ad8e79b631bc107f775692fac3c6
SceneEditor:spawnShape("ge_mission_snowman.ps", 4647.112, -3533.085, -9.516, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":8786.915,"col size z":3.66,"col size x":1.527,"scale x":1}')