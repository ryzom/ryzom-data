-- GROUP: stable_pyr
-- 0b2847bcf857572cf75ef160a648d34c3f06d918
SceneEditor:spawnShape("xmas_stable2022.ps", 18594.492, -24295.711, -1, 0, 0, -2.2, '{"col pos y":-11975.711,"col pos x":-24319.715,"col orientation":0,"col size z":9.389,"scale z":1,"scale x":1,"col size x":26.321,"scale y":1,"col size y":9.577}')

-- GROUP: pyr_deco
-- 9bb6a6d4368015e876c8702265e4c0ae424eb959
SceneEditor:spawnShape("Ge_mission_Gift.shape", 18590.15, -24302.473, -1.019, 0, 0, 0, '{"col size z":0.943,"scale y":0.64,"scale z":0.64,"col orientation":0,"col pos y":-11982.473,"col pos x":-24320,"col size y":0.704,"col size x":0.704,"scale x":0.64}')

-- bafcc2ebaa5a32eb2f7dad49c9b542d9b12f333c
SceneEditor:spawnShape("Ge_mission_Gift.shape", 18586.746, -24306.758, -1.013, 0, 0, -0.49, '{"col size z":0.943,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-11986.758,"col pos x":-24321.41,"col size y":0.704,"col size x":0.704,"scale x":1}')

-- ea43128b20122c43c05562dc8101939f1ca24118
SceneEditor:spawnShape("Ge_mission_Gift.shape", 18588.137, -24308.148, -1.009, 0, 0, 0, '{"col size z":0.943,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-11988.148,"col pos x":-24320.734,"col size y":0.704,"col size x":0.704,"scale x":1}')

-- 12a733ddc4681dce6831dfbd8c21244352d17cf3
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 18589.219, -24304.102, -0.999, 0, 0, 0, '{"col size z":0.943,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-11984.102,"col pos x":-24320,"col size y":0.704,"col size x":0.704,"scale x":1}')

-- 86078f6db426f2b13ca8755d284e418cb4248a44
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 18587.746, -24305.127, -1.008, 0, 0, 0, '{"col size z":0.943,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-11985.127,"col pos x":-24321.48,"col size y":0.704,"col size x":0.704,"scale x":1}')

-- bb1534f565aeb89602ad411d79b2b63bddf90ba2
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 18589.625, -24306.564, -0.989, 0, 0, 0, '{"col size z":0.943,"scale y":1.86,"scale z":1.86,"col orientation":0,"col pos y":-11986.564,"col pos x":-24320,"col size y":0.704,"col size x":0.704,"scale x":1.86}')

-- 0a6ee9c2889459b7c20c815a34e9ab726bf2dde0
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 18590.057, -24305.246, -0.987, 0, 0, 0, '{"col size z":0.943,"scale y":1.3,"scale z":1.18,"col orientation":0,"col pos y":-11985.246,"col pos x":-24320.297,"col size y":0.704,"col size x":0.704,"scale x":1.3}')

-- a11785127c50dfe3fdd530a36cbc2462260768a1
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 18588.359, -24302.785, -1.007, 0, 0, 0, '{"col size z":0.943,"scale y":1.19,"scale z":1.19,"col orientation":0,"col pos y":-11982.785,"col pos x":-24322.012,"col size y":0.704,"col size x":0.704,"scale x":1.19}')

-- 875366a4a7b058b8498a45d307b2f35ee6804983
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 18591.506, -24304.402, -1.008, 0, 0, 0, '{"col size z":0.943,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-11984.402,"col pos x":-24320,"col size y":0.704,"col size x":0.704,"scale x":1}')

-- ed727c18f72d3e34d2550bf8f054597630be5bfc
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 18600.781, -24283.828, -0.984, 0, 0, 0, '{"col size z":11.357,"scale y":1,"scale z":1,"col orientation":1.081,"col pos y":-11963.828,"col pos x":-24320,"col size y":8.525,"col size x":8.581,"scale x":1}')