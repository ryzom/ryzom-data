-- GROUP: island_main
-- 495fda79af29bf3d2cb0347bf729d2e8472d5435
SceneEditor:spawnShape("env-snow.ps", 29866.107, -11098.481, -20.701, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11098.481,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 5697e965c405d8dd9903c94bdc3c8e3eb9d11ac0
SceneEditor:spawnShape("env-snow.ps", 29866.027, -11068.849, -20.021, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11068.849,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 3326931d7538c1a45099022e325e448f664ea3ac
SceneEditor:spawnShape("env-snow.ps", 29895.791, -11128.232, -22.3, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11128.232,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- d99f7e0011bdbaf85b988196870e30d96622462b
SceneEditor:spawnShape("env-snow.ps", 29895.975, -11098.316, -22.23, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11098.316,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- f3773f39df20a06a4f575dfd18c280e862c1becc
SceneEditor:spawnShape("env-snow.ps", 29895.959, -11068.21, -21.436, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11068.21,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- a7337537fd060728b838f5c77c6c919815d2b9a9
SceneEditor:spawnShape("snowhill.shape", 29870.068, -11071.97, -20.394, 0, 0, 0, '{"col size x":21.946,"col pos x":-81.746,"scale x":0.43,"scale y":0.43,"col pos y":-11074.633,"col orientation":0,"col size y":21.572,"scale z":0.43,"col size z":5.142,"context":null,"url":null}')

-- fdd30ca36cde5348878eca8ab4ef70c7590ab90b
SceneEditor:spawnShape("snowhill.shape", 29871.328, -11057.845, -20.644, 0, 0, 0, '{"col size x":21.946,"col pos x":-1.342,"scale x":0.52,"scale y":0.52,"col pos y":-11057.845,"col orientation":0,"col size y":21.572,"scale z":0.52,"col size z":5.142,"context":null,"url":null}')

-- 2513bf638940652e5553cc70fb954cc5f5e18ed8
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29872.711, -11055.638, -20.674, 0, 0, 1.18, '{"col size x":4.201,"col pos x":29872.711,"scale x":1.39,"scale y":1.39,"col pos y":-11055.638,"col orientation":0,"col size y":4.145,"scale z":1.39,"col size z":11.357,"context":null,"url":null}')

-- 90f7e9e81bbcf9afe61dcf84051757edd904a5fb
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29870.967, -11072.276, -20.492, 0, 0, 1.71, '{"col size x":2.961,"col pos x":29871.131,"scale x":1,"scale y":1,"col pos y":-11072.155,"col orientation":0,"col size y":2.905,"scale z":1,"col size z":11.357,"context":null,"url":null}')

-- a7276d71f77a7b0239542f1c1c8ce36945d92035
SceneEditor:spawnShape("snowhill.shape", 29873.307, -11066.662, -20.764, 0, 0, 0, '{"col size x":8.926,"col pos x":29874.746,"scale x":0.88,"scale y":0.88,"col pos y":-11066.662,"col orientation":0,"col size y":10.582,"scale z":0.88,"col size z":5.142,"context":null,"url":null}')

-- ed379a6355478fa6b94a1cde3bdfde08bdfce65b
SceneEditor:spawnShape("xmas_snowglobe.shape", 29875.004, -11066.697, -21.118, 0, 0, 1.47, '{"col size x":6.852,"col pos x":29875.004,"scale x":17.91,"scale y":17.91,"col pos y":-11066.697,"col orientation":-0.1,"col size y":6.852,"scale z":17.91,"col size z":4.969,"context":null,"url":null}')

-- 67d157595e596a676af5b22205c50e6cac1e3c1e
SceneEditor:spawnShape("ge_mission_gift-red.shape", 29919.49, -11072.494, -21.009, 0, 0, 0, '{"col size x":0.704,"col pos x":29919.49,"scale x":1,"scale y":1,"col pos y":-11072.494,"col orientation":0,"col size y":0.704,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- aeb12651ade56744ede4c393996107d00525dd13
SceneEditor:spawnShape("ge_mission_gift-green.shape", 29919.014, -11069.886, -20.989, 0, 0, 0, '{"col size x":0.704,"col pos x":29919.014,"scale x":1,"scale y":1,"col pos y":-11069.886,"col orientation":0,"col size y":0.704,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- 44686c370976483be32aedf1620d0783b891bc26
SceneEditor:spawnShape("ge_mission_gift-purple.shape", 29918.861, -11071.286, -20.975, 0, 0, 0.84, '{"col size x":1.404,"col pos x":29918.861,"scale x":1.99,"scale y":1.99,"col pos y":-11071.286,"col orientation":0.84,"col size y":1.404,"scale z":1.99,"col size z":0.943,"context":null,"url":null}')


-- GROUP: island_cooking
-- 01c3c9246e4617ada92bebfcafc757873e8746b2
SceneEditor:spawnShape("snowhill.shape", 29881.809, -11050.365, -21.073, 0, 0, 0, '{"scale x":0.27,"col size y":21.572,"col orientation":0,"col pos x":0,"scale z":0.27,"col pos y":-11050.365,"scale y":0.27,"col size x":21.946,"col size z":5.142,"context":null,"url":null}')

-- 89d0692d16ab62877883e8023694c87c2f96bdd7
SceneEditor:spawnShape("xmas_snowman.shape", 29882.711, -11050.429, -21.005, 0, 0, -1.83, '{"scale x":1,"col size y":1.051,"col orientation":0,"col pos x":29882.711,"scale z":1,"col pos y":-11050.429,"scale y":1,"col size x":1.092,"col size z":3.66,"context":null,"url":null}')


-- GROUP: island_snow_pile
-- 64bee767bf645da896fc4bda056d49f60cbc1af3
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29878.973, -11142.238, -21.335, 0, 0, 0, '{"col size y":8.525,"col size x":8.581,"scale z":1.78,"scale x":1.78,"col pos x":29878.973,"col pos y":-11142.238,"col orientation":0,"col size z":11.357,"scale y":1.78,"context":null,"url":null}')

-- ef17afb8ee47b5096e60b06e2a0001c0ea6ef413
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29856.318, -11122.205, -17.78, 0, 0, 0, '{"col size y":5.645,"col size x":5.701,"scale z":1,"scale x":1,"col pos x":29856.537,"col pos y":-11122.407,"col orientation":0,"col size z":11.357,"scale y":1,"context":null,"url":null}')

-- 3d2456dca5ace1879b9cefac9493d8eb71aebdee
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29908.406, -11123.111, -21.464, 0, 0, 0, '{"col size y":5.955,"col size x":6.011,"scale z":1,"scale x":1,"col pos x":29906.705,"col pos y":-11124.895,"col orientation":0,"col size z":11.357,"scale y":1,"context":null,"url":null}')

-- 16f2f2c29679fef7faf70a6d3a569f83597b29d5
SceneEditor:spawnShape("xmas_snowman.shape", 29870.316, -11113.504, -20.945, 0, 0, 5.54, '{"col size y":1.651,"col size x":2.832,"scale z":2.17,"scale x":2.17,"col pos x":29870.316,"col pos y":-11113.504,"col orientation":0,"col size z":3.66,"scale y":2.17,"context":null,"url":null}')

-- 94d60d43b217d089212da5b1c6e0f97eaabc95ef
SceneEditor:spawnShape("xmas_snowman.shape", 29901.1, -11122.305, -21.83, 0, 0, -0.32, '{"col size y":1.651,"col size x":2.832,"scale z":1.98,"scale x":1.98,"col pos x":29901.293,"col pos y":-11122.342,"col orientation":0,"col size z":3.66,"scale y":1.98,"context":null,"url":null}')

-- 47f17d6ea507b716f5f6452be20fa3148426b078
SceneEditor:spawnShape("xmas_snowman.shape", 29881.465, -11118.92, -22.234, 0, 0, -0.97, '{"col size y":1.651,"col size x":2.832,"scale z":1.69,"scale x":1.69,"col pos x":29881.465,"col pos y":-11118.92,"col orientation":0,"col size z":3.66,"scale y":1.69,"context":null,"url":null}')

-- 0ec7cb36d9398d611ef821ae15b35d9eaa6ef370
SceneEditor:spawnShape("xmas_snowman.shape", 29888.721, -11127.589, -22.494, 0, 0, -0.68, '{"col size y":1.651,"col size x":2.832,"scale z":5.45,"scale x":5.36,"col pos x":29888.721,"col pos y":-11127.589,"col orientation":0,"col size z":3.66,"scale y":5.36,"context":null,"url":null}')

-- 47f15f8ad267fd6cea7360951b4977032fc98971
SceneEditor:spawnShape("snowhill.shape", 29897.977, -11143.459, -22.129, 0, 0, 0, '{"col size y":24.372,"col size x":4.556,"scale z":2.87,"scale x":2.87,"col pos x":29909.816,"col pos y":-11141.914,"col orientation":0,"col size z":5.142,"scale y":2.87,"context":null,"url":null}')

-- 8d7094a63a575b987cee1cd931d04c1a9acf9d9e
SceneEditor:spawnShape("snowhill.shape", 29891.277, -11119.021, -21.929, 0, 0, -0.02, '{"col size y":6.472,"col size x":14.906,"scale z":1.34,"scale x":1.34,"col pos x":29892.785,"col pos y":-11114.6,"col orientation":-0.37,"col size z":5.142,"scale y":1.34,"context":null,"url":null}')

-- e61841d7e694cab8bafa6b85a0da59a4657b8f71
SceneEditor:spawnShape("snowhill.shape", 29904.457, -11118.858, -22.42, 0, 0, 0, '{"col size y":17.002,"col size x":11.276,"scale z":1,"scale x":1,"col pos x":29903.203,"col pos y":-11124.13,"col orientation":0.42,"col size z":5.142,"scale y":1,"context":null,"url":null}')

-- 5eb4d9386ae07a514f236fdb07f88275e3920db1
SceneEditor:spawnShape("snowhill.shape", 29855.279, -11115.158, -18.464, 0, 0, 1.35, '{"col size y":16.562,"col size x":3.406,"scale z":1.43,"scale x":1.43,"col pos x":29862.975,"col pos y":-11105.438,"col orientation":-2.03,"col size z":5.142,"scale y":1.43,"context":null,"url":null}')

-- 62982537930514d18eaeb3db4dbdf3c9a040b456
SceneEditor:spawnShape("snowhill.shape", 29874.504, -11113.532, -21.529, 0, 0, 1.11, '{"col size y":3.462,"col size x":17.816,"scale z":1,"scale x":1,"col pos x":29879.174,"col pos y":-11109.869,"col orientation":-0.07,"col size z":6.832,"scale y":1,"context":null,"url":null}')

-- a8c44c3305ffd5fff19c1cce3a952d6d4cc898cf
SceneEditor:spawnShape("snowhill.shape", 29863.559, -11128.892, -21.493, 0, 0, -0.09, '{"col size y":21.572,"col size x":21.946,"scale z":2.53,"scale x":2.53,"col pos x":29863.559,"col pos y":-11128.892,"col orientation":0,"col size z":5.142,"scale y":2.53,"context":null,"url":null}')

-- a1d3f2c23d327d2e92993097e845a0d17d9e37b1
SceneEditor:spawnShape("snowhill.shape", 29879.467, -11132.344, -22.527, 0, 0, 0, '{"col size y":12.222,"col size x":2.556,"scale z":1.86,"scale x":1.86,"col pos x":29909.717,"col pos y":-11131.649,"col orientation":0.13,"col size z":5.142,"scale y":1.86,"context":null,"url":null}')


