-- GROUP: island_main
-- 87a119a92e3c2e374886bfcc671c606ff90775bc
SceneEditor:spawnShape("ge_mission_gift-green.shape", 29995.658, -11077.206, -16.108, 0, 0, 0, '{"col size x":0.704,"col pos x":29995.658,"scale x":1.19,"scale y":1.19,"col pos y":-11077.206,"col orientation":0,"col size y":0.704,"scale z":1.19,"col size z":0.943,"context":null,"url":null}')

-- 5f018eb4ae3dc747b3b6fbc983e7c60bbf8abace
SceneEditor:spawnShape("ge_mission_gift-brown.shape", 29995.428, -11078.067, -16.372, 0, 0, 0, '{"col size x":0.474,"col pos x":29995.428,"scale x":1,"scale y":1,"col pos y":-11078.067,"col orientation":0,"col size y":0.474,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- c6b70f2399e3573637b3d814db1ec4793e324d53
SceneEditor:spawnShape("ge_mission_gift-red.shape", 29989.707, -11064.604, -17.933, 0, 0, 0.62, '{"col size x":0.704,"col pos x":29989.707,"scale x":1.27,"scale y":1.27,"col pos y":-11064.604,"col orientation":0.62,"col size y":0.704,"scale z":1.27,"col size z":0.943,"context":null,"url":null}')

-- a29dcaf50bb407a9f479409b11724a31a1678ca4
SceneEditor:spawnShape("ge_mission_gift.shape", 29988.656, -11064.68, -18.222, 0, 0, 0, '{"col size x":0.584,"col pos x":29988.656,"scale x":1,"scale y":1,"col pos y":-11064.68,"col orientation":0,"col size y":0.584,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- eb43e7c562ababab8d44e4f32c0affb6224834b2
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29989.203, -11064.069, -18.154, 0, 0, -0.32, '{"col size x":0.581,"col pos x":29989.203,"scale x":1,"scale y":1,"col pos y":-11064.069,"col orientation":0,"col size y":0.403,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 5210605eaf1c3c29c792c5cf1287ad86d626b77d
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29993.871, -11063.54, -16.825, 0, 0, -0.26, '{"col size x":0.681,"col pos x":29993.871,"scale x":1,"scale y":1,"col pos y":-11063.54,"col orientation":0,"col size y":0.493,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 34aa8819a244c74030e080945bc5089933bdb163
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29998.008, -11063.231, -15.401, 0, 0, -0.32, '{"col size x":0.591,"col pos x":29998.008,"scale x":1,"scale y":1,"col pos y":-11063.231,"col orientation":0,"col size y":0.503,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- a3b6bc84ac51e85448a7e2114e986e565e628c98
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29991.904, -11078.905, -17.512, 0, 0, 3.72, '{"col size x":0.621,"col pos x":29991.904,"scale x":1,"scale y":1,"col pos y":-11078.905,"col orientation":0,"col size y":0.613,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 1d5f97426be282c3cf5dc9ae7fcaae7ca17faf5d
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29996.045, -11077.995, -16.168, 0, 0, -2.61, '{"col size x":0.411,"col pos x":29996.045,"scale x":1,"scale y":1,"col pos y":-11077.995,"col orientation":0,"col size y":0.423,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- a580654bf6b161ae6d3ae9178a8666b00507249a
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30000.047, -11076.832, -14.507, 0, 0, -2.44, '{"col size x":0.571,"col pos x":30000.047,"scale x":1,"scale y":1,"col pos y":-11076.832,"col orientation":0,"col size y":0.513,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 58d2e6b8b3462419b2b74181b889be6d469dbe46
SceneEditor:spawnShape("ge_christmas_dag.shape", 29923.914, -11068.129, -21.054, 0, 0, 2.31, '{"col size x":0.732,"col pos x":29923.914,"scale x":3.47,"scale y":3.47,"col pos y":-11068.129,"col orientation":-0.83,"col size y":2.323,"scale z":3.47,"col size z":0.638,"context":null,"url":null}')

-- b5bf6f1ec3882360e52cd322ab3d6b919ca1b810
SceneEditor:spawnShape("snowhill.shape", 30003.066, -11071.034, -12.982, 0, 0, 0, '{"col size x":21.946,"col pos x":-0.939,"scale x":0.28,"scale y":0.28,"col pos y":-11071.034,"col orientation":0,"col size y":21.572,"scale z":0.28,"col size z":5.142,"context":null,"url":null}')

-- 87b2d8187986483bdd48fc3f8e250c778aa2d5ef
SceneEditor:spawnShape("snowhill.shape", 30000.922, -11071.302, -13.839, 0, 0, 0, '{"col size x":21.946,"col pos x":-0.596,"scale x":0.28,"scale y":0.28,"col pos y":-11071.302,"col orientation":0,"col size y":21.572,"scale z":0.28,"col size z":5.142,"context":null,"url":null}')

-- a07f73c556a194448599bf04475b542496b235f5
SceneEditor:spawnShape("snowhill.shape", 30001.562, -11069.561, -13.87, 0, 0, 0, '{"col size x":19.906,"col pos x":-0.104,"scale x":0.41,"scale y":0.41,"col pos y":-11069.561,"col orientation":0,"col size y":19.532,"scale z":0.41,"col size z":5.142,"context":null,"url":null}')

-- f322a608e1379e1917f9c5fbbc4f89d4ba6b59a1
SceneEditor:spawnShape("env-snow.ps", 30004.525, -11119.739, -13.508, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11119.739,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 188da585f04f10686819bfb27524e9abc6eb607d
SceneEditor:spawnShape("env-snow.ps", 29985.52, -11098.389, -20.035, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11098.389,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 258bc93faa9e33d4cf143b5eafa60076d63ac869
SceneEditor:spawnShape("env-snow.ps", 29984.938, -11128.386, -16.043, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11128.386,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 971d47332caafb161776e5ea8521cf571cfb6c08
SceneEditor:spawnShape("env-snow.ps", 29982.564, -11066.977, -19.068, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11066.977,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 9b094efdc7f6915a58640fee6640e3406c26f035
SceneEditor:spawnShape("env-snow.ps", 29956.5, -11157.916, -20.238, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11157.916,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 44c8e20bfc5dc679448fb4217c26a21e1fc992cb
SceneEditor:spawnShape("env-snow.ps", 29955.539, -11129.078, -22.24, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11129.078,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 17c874a304ae8119b3985ea020a8a2f0aa0d2c3c
SceneEditor:spawnShape("env-snow.ps", 29955.516, -11099.07, -22.415, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11099.07,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 07c702fe162683fea9a76731fcde86ac8dd7c1c2
SceneEditor:spawnShape("env-snow.ps", 29955.602, -11068.305, -21.431, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11068.305,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- ed52a4642a8a05f2c70b7b95cd89832b54e13bd9
SceneEditor:spawnShape("env-snow.ps", 29925.924, -11127.91, -21.165, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11127.91,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 8e9867e37b11bb59d62e8dbba2c487c8c8973d7b
SceneEditor:spawnShape("env-snow.ps", 29926.008, -11098.11, -21.179, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11098.11,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- d83001ceef5059712c757ab36e9501e9c11eefde
SceneEditor:spawnShape("env-snow.ps", 29925.887, -11068.24, -21.036, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11068.24,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 90dd31334ff5672692a48754b48be125a02e073a
SceneEditor:spawnShape("snowhill.shape", 29994.525, -11096.36, -19.025, 0, 0, 0.95, '{"col size x":21.946,"col pos x":35.762,"scale x":0.49,"scale y":0.49,"col pos y":-11096.36,"col orientation":0,"col size y":21.572,"scale z":0.49,"col size z":5.142,"context":null,"url":null}')

-- 7ee5f021fc04fd249d973a53e726d248f2ad54dc
SceneEditor:spawnShape("snowhill.shape", 29972.002, -11124.301, -19.363, 0, 0, 0, '{"col size x":21.946,"col pos x":14.453,"scale x":0.42,"scale y":0.42,"col pos y":-11124.503,"col orientation":0,"col size y":21.572,"scale z":0.42,"col size z":5.142,"context":null,"url":null}')

-- 5bec66f9ea827629e32796f567dbf6a4a7c4a57e
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29971.311, -11121.659, -19.927, 0, 0, 0.2, '{"col size x":1.711,"col pos x":29971.311,"scale x":1.01,"scale y":1.01,"col pos y":-11121.659,"col orientation":-1.16,"col size y":1.655,"scale z":1.01,"col size z":11.357,"context":null,"url":null}')

-- ae4fb8b157ce7fdcb0c3c892a123901b88f598e4
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29991.684, -11095.613, -19.65, 0, 0, 1.29, '{"col size x":2.291,"col pos x":29991.684,"scale x":1.05,"scale y":1.05,"col pos y":-11095.613,"col orientation":0,"col size y":2.235,"scale z":1.05,"col size z":11.357,"context":null,"url":null}')

-- 1d647531708645aab6751a0e4849bc90325d9c7c
SceneEditor:spawnShape("snowhill.shape", 29920.965, -11071.057, -20.982, 0, 0, 0, '{"col size x":21.946,"col pos x":0,"scale x":0.37,"scale y":0.37,"col pos y":-11071.057,"col orientation":0,"col size y":21.572,"scale z":0.37,"col size z":5.142,"context":null,"url":null}')

-- 56923c8e35b4ab948f79be5bb64d5c43bc0f3705
SceneEditor:spawnShape("snowhill.shape", 29968.854, -11127.309, -19.342, 0, 0, -2.64, '{"col size x":21.946,"col pos x":-0.945,"scale x":0.21,"scale y":0.21,"col pos y":-11127.309,"col orientation":0,"col size y":21.572,"scale z":0.21,"col size z":5.142,"context":null,"url":null}')

-- c1cad67a77358480ba7de8ffe5cb9cc54630b88a
SceneEditor:spawnShape("snowhill.shape", 29995.572, -11089.655, -19.168, 0, 0, 0, '{"col size x":21.946,"col pos x":-0.354,"scale x":0.29,"scale y":0.29,"col pos y":-11089.655,"col orientation":0,"col size y":21.572,"scale z":0.29,"col size z":5.142,"context":null,"url":null}')

-- 79fd85c16664eaca8b11e7e14bfac60aa39b6814
SceneEditor:spawnShape("xmas_snowman.shape", 29967.973, -11126.912, -19.581, 0, 0, 0.64, '{"col size x":2.412,"col pos x":29967.973,"scale x":1.57,"scale y":1.57,"col pos y":-11126.912,"col orientation":0.83,"col size y":2.121,"scale z":1.57,"col size z":3.66,"context":null,"url":null}')

-- f2b4dd51e44d923e2513a60e384d41e148b85f63
SceneEditor:spawnShape("xmas_snowman.shape", 29994.234, -11090.121, -19.541, 0, 0, 1.31, '{"col size x":2.292,"col pos x":29994.234,"scale x":1.63,"scale y":1.63,"col pos y":-11090.121,"col orientation":0,"col size y":2.191,"scale z":1.63,"col size z":3.66,"context":null,"url":null}')

-- cad6daedc6466d85047a0f50cc354a51e6d94ab0
SceneEditor:spawnShape("ge_mission_gift-red.shape", 30003.209, -11119.802, -12.947, 0, 0, 0.21, '{"col size x":10.764,"col pos x":30001.875,"scale x":1,"scale y":1,"col pos y":-11118.599,"col orientation":-0.61,"col size y":5.384,"scale z":1,"col size z":5.443,"context":null,"url":null}')

-- 939969cd8f93436e66f7264755c336d8148240a3
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30008.711, -11137.079, -7.301, 0, 0, 3.09, '{"col size x":0.671,"col pos x":30008.711,"scale x":1,"scale y":1,"col pos y":-11137.079,"col orientation":0,"col size y":0.763,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 349d92c74c0f851a58f36827f726f083f8b7491f
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30002.344, -11134.763, -9.686, 0, 0, 3.11, '{"col size x":0.731,"col pos x":30002.344,"scale x":1,"scale y":1,"col pos y":-11134.763,"col orientation":0,"col size y":0.613,"scale z":1,"col size z":5.404,"context":null,"url":null}')

-- 8b327c62bbad275efde00172c14d10ff03c82673
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29997.215, -11132.731, -11.939, 0, 0, 3.12, '{"col size x":0.731,"col pos x":29997.215,"scale x":1,"scale y":1,"col pos y":-11132.731,"col orientation":0,"col size y":0.863,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 1513046478822cd1944377bab788d8a069037ddb
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29992.564, -11130.556, -13.805, 0, 0, 3.07, '{"col size x":0.921,"col pos x":29992.564,"scale x":1,"scale y":1,"col pos y":-11130.556,"col orientation":3.07,"col size y":0.693,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 3eeff175988fec5e3d06e2bb8424970c9e01f6a2
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29987.049, -11127.64, -15.847, 0, 0, 3.08, '{"col size x":0.901,"col pos x":29987.049,"scale x":1,"scale y":1,"col pos y":-11127.64,"col orientation":0,"col size y":0.563,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- a49c2e1c853c271e188359bc94605e0dc93db8c9
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29981.348, -11124.467, -17.679, 0, 0, -3.15, '{"col size x":1.081,"col pos x":29981.348,"scale x":1,"scale y":1,"col pos y":-11124.467,"col orientation":0,"col size y":0.843,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- d1d8b02069585d44a5038d59f0b772995742d22e
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29976.533, -11121.79, -19.197, 0, 0, 3.07, '{"col size x":0.911,"col pos x":29976.533,"scale x":1,"scale y":1,"col pos y":-11121.79,"col orientation":3.07,"col size y":0.803,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 235e6bbb7d3de4911d51c0014d2d69651e1d12f9
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29993.045, -11101.223, -18.891, 0, 0, -0.81, '{"col size x":0.571,"col pos x":29993.045,"scale x":1,"scale y":1,"col pos y":-11101.223,"col orientation":0,"col size y":0.653,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 879057c71eec0b19d1365a5a5cd18b0e298f1191
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 29998.621, -11104.938, -17.336, 0, 0, -0.99, '{"col size x":0.581,"col pos x":29998.621,"scale x":1,"scale y":1,"col pos y":-11104.938,"col orientation":0,"col size y":0.663,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 96f1f1842138f37d01313b8212129f02a324c4fb
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30004.854, -11108.924, -14.795, 0, 0, -1.13, '{"col size x":0.591,"col pos x":30004.854,"scale x":1,"scale y":1,"col pos y":-11108.924,"col orientation":0,"col size y":0.603,"scale z":0.96,"col size z":5.674,"context":null,"url":null}')

-- 90bc163467b074d9d437f93384534c41d7e4f6ae
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30009.322, -11111.958, -13.123, 0, 0, -1.07, '{"col size x":0.631,"col pos x":30009.322,"scale x":1,"scale y":1,"col pos y":-11111.958,"col orientation":0,"col size y":0.363,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 0db98c2be1192fbdf49bfb86ce81f79b56063a6a
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30014.703, -11115.506, -11.104, 0, 0, -1.12, '{"col size x":0.511,"col pos x":30014.715,"scale x":1,"scale y":1,"col pos y":-11115.48,"col orientation":-0.09,"col size y":0.413,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- c046e9d9d4d2c756344cbdbfb7e1e2c482fdee01
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30017.982, -11118.138, -9.692, 0, 0, -1.04, '{"col size x":0.531,"col pos x":30017.982,"scale x":1,"scale y":1,"col pos y":-11118.138,"col orientation":0,"col size y":0.653,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- a6df07478bae0cef2a40e9f4d415eaefc59a1420
SceneEditor:spawnShape("ge_mission_reverbere_green.ps", 30022.629, -11121.691, -7.818, 0, 0, -0.83, '{"col size x":0.661,"col pos x":30022.629,"scale x":1,"scale y":1,"col pos y":-11121.691,"col orientation":0,"col size y":0.443,"scale z":1,"col size z":5.674,"context":null,"url":null}')

-- 3bdd9b9388d303965cc3e0bebda95e536b8e8e62
SceneEditor:spawnShape("xmas_stable2022_deco.shape", 30007.242, -11124.739, -26.923, 0, 0, -0.74, '{"col size x":22.601,"col pos x":30013.354,"scale x":2.59,"scale y":2.59,"col pos y":-11128.141,"col orientation":-0.71,"col size y":2.714,"scale z":2.59,"col size z":10.652,"context":null,"url":null}')

-- 8b3ee645f26d490434ee6bffe5f399ad11f34c5d
SceneEditor:spawnShape("ge_mission_gift-purple.shape", 29920.617, -11068.526, -20.954, 0, 0, -0.3, '{"col size x":0.704,"col pos x":29920.617,"scale x":0.9,"scale y":0.9,"col pos y":-11068.526,"col orientation":-0.31,"col size y":0.704,"scale z":0.9,"col size z":0.943,"context":null,"url":null}')

-- 2ff6e2f0ce1c3db8d0ede0801169050ef4bf3a44
SceneEditor:spawnShape("ge_mission_gift-green.shape", 29921.564, -11068.415, -20.964, 0, 0, 0, '{"col size x":0.924,"col pos x":29921.564,"scale x":1.29,"scale y":1.29,"col pos y":-11068.415,"col orientation":0,"col size y":0.924,"scale z":1.29,"col size z":0.943,"context":null,"url":null}')

-- 7899078f5f2edf4f3b5d1be23d56510c574b458a
SceneEditor:spawnShape("ge_mission_gift-turquoise.shape", 29923.359, -11071.544, -20.979, 0, 0, -0.73, '{"col size x":0.554,"col pos x":29923.359,"scale x":0.77,"scale y":0.77,"col pos y":-11071.544,"col orientation":-0.83,"col size y":0.554,"scale z":0.77,"col size z":0.943,"context":null,"url":null}')

-- 6efd24322f38932907e5ababaf33d9516518be01
SceneEditor:spawnShape("ge_mission_gift-brown.shape", 29922.842, -11072.275, -20.985, 0, 0, 0, '{"col size x":0.704,"col pos x":29922.842,"scale x":1,"scale y":1,"col pos y":-11072.275,"col orientation":0,"col size y":0.704,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- cc136245c8087351b2b64fca2624f8063b751cb8
SceneEditor:spawnShape("ge_mission_gift-turquoise.shape", 29922.562, -11068.517, -21.001, 0, 0, -0.51, '{"col size x":0.704,"col pos x":29922.562,"scale x":1,"scale y":1,"col pos y":-11068.517,"col orientation":-0.53,"col size y":0.704,"scale z":1,"col size z":0.943,"context":null,"url":null}')

-- 8b1fed297c5df90e21d098463c6de2c15a6d7c28
SceneEditor:spawnShape("ge_mission_gift-red.shape", 29923.188, -11070.23, -20.987, 0, 0, 0, '{"col size x":1.864,"col pos x":29923.188,"scale x":2.6,"scale y":2.6,"col pos y":-11070.23,"col orientation":0,"col size y":1.864,"scale z":2.6,"col size z":1.863,"context":null,"url":null}')

-- a2e081da10e5fcd5c91788c75ccbc9858a1897c9
SceneEditor:spawnShape("ge_mission_gift-purple.shape", 29922.367, -11072.933, -20.987, 0, 0, 0.54, '{"col size x":0.464,"col pos x":29922.367,"scale x":0.68,"scale y":0.68,"col pos y":-11072.933,"col orientation":0.53,"col size y":0.464,"scale z":0.68,"col size z":0.943,"context":null,"url":null}')

-- d9a792fac6b2666441df019dccdb9370f82cd665
SceneEditor:spawnShape("ge_mission_gift-green.shape", 29921.475, -11072.91, -21.002, 0, 0, 0, '{"col size x":0.964,"col pos x":29921.475,"scale x":1.33,"scale y":1.33,"col pos y":-11072.91,"col orientation":0,"col size y":0.964,"scale z":1.33,"col size z":0.943,"context":null,"url":null}')

-- e26e86061a7cfc10fa26c3c9d0cb096248d14b03
SceneEditor:spawnShape("ge_mission_gift-brown.shape", 29919.816, -11069.202, -21.002, 0, 0, 0.44, '{"col size x":0.884,"col pos x":29919.816,"scale x":1.24,"scale y":1.24,"col pos y":-11069.202,"col orientation":0.45,"col size y":0.884,"scale z":1.24,"col size z":0.943,"context":null,"url":null}')

-- db344c35bf5b84fac9d1db87736850f843d11f9c
SceneEditor:spawnShape("ge_mission_gift-turquoise.shape", 29920.359, -11073.123, -21.001, 0, 0, -0.26, '{"col size x":0.794,"col pos x":29920.359,"scale x":1.14,"scale y":1.14,"col pos y":-11073.123,"col orientation":-0.26,"col size y":0.794,"scale z":1.14,"col size z":0.943,"context":null,"url":null}')

-- 1bff3ca8631f034c98342d3994e6327dde1def87
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 29921.109, -11070.447, -20.978, 0, 0, 0, '{"col size x":1.907,"col pos x":29921.109,"scale x":2.87,"scale y":2.87,"col pos y":-11070.447,"col orientation":0,"col size y":2.045,"scale z":2.87,"col size z":10.225,"context":null,"url":null}')


-- GROUP: island_stable
-- f0115a658f7a1f76db695b2f8a3599af23dff793
SceneEditor:spawnShape("snowhill.shape", 29986.811, -11050.119, -19.676, 0, 0, 0, '{"col size y":21.572,"col pos x":0,"scale y":0.43,"scale z":0.43,"scale x":0.43,"col pos y":-11053.521,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 981cbecacc01d343163caf53f08e44c1f4404656
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29986.549, -11048.499, -20.472, 0, 0, -1.07, '{"col size y":3.085,"col pos x":29986.549,"scale y":1,"scale z":1,"scale x":1,"col pos y":-11048.499,"col size x":3.141,"col orientation":0,"col size z":11.357,"context":null,"url":null}')

-- 49a0040bb99a7233489238895be4a83b8a59c172
SceneEditor:spawnShape("xmas_lights_01.shape", 29965.229, -11044.15, -19.681, 0, 0, -0.75, '{"col size y":0.606,"col pos x":2.018,"scale y":0.5,"scale z":0.5,"scale x":0.5,"col pos y":-11044.15,"col size x":7.757,"col orientation":0,"col size z":2.323,"context":null,"url":null}')

-- 69eb5e52aabc004349476c9b5610477a28de14f3
SceneEditor:spawnShape("xmas_acc_candycane_star.shape", 29967.629, -11041.162, -16.842, 0, 0, -0.7, '{"col size y":0.12,"col pos x":29967.629,"scale y":7.44,"scale z":7.44,"scale x":7.44,"col pos y":-11041.162,"col size x":0.455,"col orientation":0,"col size z":1.947,"context":null,"url":null}')

-- 2ee7d46037983fee191256eb9db4add3c78dbbd5
SceneEditor:spawnShape("xmas_fy_lantern.shape", 29980.373, -11056.581, -20.323, 0, 0, 4.09, '{"col size y":0.596,"col pos x":29980.373,"scale y":1,"scale z":1,"scale x":1,"col pos y":-11056.581,"col size x":1.675,"col orientation":4.09,"col size z":4.34,"context":null,"url":null}')

-- 5db370b3b5d03e6b615c081e7a2517850385d56f
SceneEditor:spawnShape("xmas_fy_lantern.shape", 29971.42, -11049.439, -20.739, 0, 0, 4.01, '{"col size y":0.596,"col pos x":29971.42,"scale y":1,"scale z":1,"scale x":1,"col pos y":-11049.439,"col size x":1.675,"col orientation":4.01,"col size z":4.34,"context":null,"url":null}')

-- bb9ceebf43ac3634bab4d46f70a93f1635df32d1
SceneEditor:spawnShape("snowhill.shape", 29957.549, -11042.447, -21.018, 0, 0, 0, '{"col size y":21.572,"col pos x":0,"scale y":0.71,"scale z":0.71,"scale x":0.71,"col pos y":-11042.649,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 1875618ff4a7f07ba1852c725698564c6f1a07b5
SceneEditor:spawnShape("xmas_sleigh.shape", 29978.016, -11048.779, -20.593, 0, 0, -0.75, '{"col size y":7.355,"col pos x":29978.016,"scale y":1,"scale z":1,"scale x":1,"col pos y":-11048.779,"col size x":15.002,"col orientation":-0.75,"col size z":3.004,"context":null,"url":null}')

-- 6bef99a7be6244ff2ff914fbe4c4595f487a94ef
SceneEditor:spawnShape("xmas_stable2022.shape", 29968.484, -11041.854, -20.973, 0, 0, -0.73, '{"col size y":8.781,"col pos x":29968.484,"scale y":1.15,"scale z":1.15,"scale x":1.15,"col pos y":-11041.854,"col size x":26.605,"col orientation":-0.73,"col size z":10.394,"context":null,"url":null}')


-- GROUP: island_trees
-- 90b63c9d5e38601bbfa99081725e21c711e5a2eb
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29924.088, -11151.858, -18.609, 0, 0, 0, '{"scale y":0.73,"scale x":0.73,"col size z":10.379,"col size x":1.371,"col orientation":0,"col pos y":-11151.858,"scale z":0.73,"col size y":1.315,"col pos x":29924.088,"context":null,"url":null}')

-- deb21c299d4859253af6d3d24b07947346ff265d
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29952.984, -11159.36, -20.142, 0, 0, 0, '{"scale y":1,"scale x":1,"col size z":10.379,"col size x":1.731,"col orientation":0,"col pos y":-11159.36,"scale z":1,"col size y":1.675,"col pos x":29952.984,"context":null,"url":null}')

-- adba6a056282299d12fc283916cc91943d162cea
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29940.564, -11156.274, -20.956, 0, 0, 0, '{"scale y":1.31,"scale x":1.31,"col size z":10.379,"col size x":1.541,"col orientation":0,"col pos y":-11156.274,"scale z":1.31,"col size y":1.485,"col pos x":29940.564,"context":null,"url":null}')

-- 368a5e303f0895ab2dc54c17916dce1c831d266d
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29959.268, -11149.686, -20.821, 0, 0, 0, '{"scale y":1,"scale x":1,"col size z":10.379,"col size x":1.521,"col orientation":0,"col pos y":-11149.686,"scale z":1,"col size y":1.465,"col pos x":29959.268,"context":null,"url":null}')

-- 360a13b4391094c2ffb4fff2e561b470977fabbc
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29939.648, -11138.784, -21.966, 0, 0, 0, '{"scale y":1.23,"scale x":1.23,"col size z":10.379,"col size x":1.231,"col orientation":0,"col pos y":-11138.784,"scale z":1.23,"col size y":1.175,"col pos x":29939.648,"context":null,"url":null}')

-- d6dfb244964b42ed3cecf4c1cd24190592910289
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29924, -11139, -20.855, 0, 0, 0, '{"scale y":0.91,"scale x":0.91,"col size z":10.379,"col size x":1.441,"col orientation":0,"col pos y":-11139,"scale z":0.91,"col size y":1.385,"col pos x":29924,"context":null,"url":null}')

-- 49e67ba92affda95a9b1c0696abb49ce047b4aa9
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29934.59, -11145.358, -21.439, 0, 0, 0, '{"scale y":0.89,"scale x":0.89,"col size z":10.379,"col size x":1.451,"col orientation":0,"col pos y":-11145.358,"scale z":0.89,"col size y":1.395,"col pos x":29934.59,"context":null,"url":null}')

-- e2833fa4160c281da60092faed790ee75e485935
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29947.002, -11144.968, -21.952, 0, 0, 0, '{"scale y":0.8,"scale x":0.8,"col size z":10.379,"col size x":1.111,"col orientation":0,"col pos y":-11144.968,"scale z":0.8,"col size y":1.055,"col pos x":29947.002,"context":null,"url":null}')

-- f174b09ed846f787b376da9416f7320eeb3c1c70
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29953, -11134, -22.454, 0, 0, 0, '{"scale y":1.16,"scale x":1.16,"col size z":10.379,"col size x":1.881,"col orientation":0,"col pos y":-11134,"scale z":1.16,"col size y":1.825,"col pos x":29953,"context":null,"url":null}')

-- dc2a7e59910b93b6f4c3763e36ba0440560a07aa
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29930, -11131, -21.335, 0, 0, 0, '{"scale y":1,"scale x":1,"col size z":10.379,"col size x":1.401,"col orientation":0,"col pos y":-11131,"scale z":1,"col size y":1.345,"col pos x":29930,"context":null,"url":null}')

-- 77519b1f04bf98b200a3b442fd917ee330f487cf
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 29941, -11130, -22.149, 0, 0, 0, '{"scale y":1,"scale x":1,"col size z":10.379,"col size x":1.621,"col orientation":0,"col pos y":-11130,"scale z":1,"col size y":1.565,"col pos x":29941,"context":null,"url":null}')


