-- GROUP: island_main
-- e1b8ee72183d25c2e5366edd6821b9d3d7c1ef42
SceneEditor:spawnShape("env-snow.ps", 29866.023, -11039.033, -21.074, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11039.033,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 7bdd60d1e0fea2c134388597a83be7cf348d7ac5
SceneEditor:spawnShape("env-snow.ps", 29895.83, -11008.276, -21.716, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11008.276,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 7cdaed59bc20df2d546a04199ad41f5f8ae64b0b
SceneEditor:spawnShape("env-snow.ps", 29895.9, -11038.276, -21.083, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11038.276,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')


-- GROUP: island_cooking
-- 954e1830b3056cd064caf3b4cf4ae1c9bd689a0c
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29885.922, -11020.214, -21.553, 0, 0, 1.09, '{"scale x":1,"col size y":2.845,"col orientation":0,"col pos x":29885.922,"scale z":1,"col pos y":-11020.214,"scale y":1,"col size x":2.901,"col size z":11.357,"context":null,"url":null}')

-- 755b121e13fe5ad4adf2ea7a4962a125553539e8
SceneEditor:spawnShape("snowhill.shape", 29909.205, -11021.483, -21.025, 0, 0, 0, '{"scale x":0.21,"col size y":21.572,"col orientation":0,"col pos x":0,"scale z":0.21,"col pos y":-11021.483,"scale y":0.21,"col size x":21.946,"col size z":5.142,"context":null,"url":null}')

-- 312a44f259a49f3c82f88ffddf7e7f048bf66b14
SceneEditor:spawnShape("xmas_snowman.shape", 29909.428, -11021.752, -21.022, 0, 0, -3.04, '{"scale x":1,"col size y":1.331,"col orientation":0,"col pos x":29909.428,"scale z":1,"col pos y":-11021.752,"scale y":1,"col size x":1.242,"col size z":3.66,"context":null,"url":null}')

-- c3d138de05ea21dd476c3db5768104a158ccb51a
SceneEditor:spawnShape("xmas_oven.shape", 29890.072, -11033.776, -21.088, 0, 0, 0.9, '{"scale x":1.39,"col size y":1.561,"col orientation":0.9,"col pos x":29890.072,"scale z":1.39,"col pos y":-11033.776,"scale y":1.39,"col size x":1.456,"col size z":2.217,"context":null,"url":null}')

-- 9b536128d71c14bf894287988dac33908de4174a
SceneEditor:spawnShape("xmas_acc_candycane_star.shape", 29887.402, -11032.984, -21.096, 0, 0, 1.07, '{"scale x":5.67,"col size y":0.12,"col orientation":0,"col pos x":29887.402,"scale z":5.67,"col pos y":-11032.984,"scale y":5.67,"col size x":0.455,"col size z":1.947,"context":null,"url":null}')

-- 4408b97ac049f4f6b21f5a280c1f151635a6ec99
SceneEditor:spawnShape("xmas_gingerbread.shape", 29898.088, -11030.338, -20.355, -1.61, -0.05, -1.98, '{"scale x":1,"col size y":0.015,"col orientation":0,"col pos x":29898.088,"scale z":1,"col pos y":-11030.338,"scale y":1,"col size x":0.197,"col size z":0.336,"context":null,"url":null}')

-- ef6026c83ea95ba3c5b5a4429a34902bb09ab80b
SceneEditor:spawnShape("xmas_gingerbread.shape", 29898.078, -11030.543, -20.362, 1.6, -0.28, 1.26, '{"scale x":1,"col size y":0.015,"col orientation":0,"col pos x":29898.078,"scale z":1,"col pos y":-11030.543,"scale y":1,"col size x":0.197,"col size z":0.336,"context":null,"url":null}')

-- 00cc7ba7ed3e6ee83582323816a3d0350d0528e0
SceneEditor:spawnShape("xmas_gingerbread.shape", 29898.1, -11030.763, -20.37, 1.52, 0, 0, '{"scale x":1,"col size y":0.015,"col orientation":0,"col pos x":29898.1,"scale z":1,"col pos y":-11030.763,"scale y":1,"col size x":0.197,"col size z":0.336,"context":null,"url":null}')

-- b422185f6533af836c194feea7d261b26ae86811
SceneEditor:spawnShape("xmas_gingerbread.shape", 29897.957, -11030.684, -20.349, 1.51, 0, 1.36, '{"scale x":1,"col size y":0.015,"col orientation":0,"col pos x":29897.957,"scale z":0.9,"col pos y":-11030.684,"scale y":1,"col size x":0.197,"col size z":0.336,"context":null,"url":null}')

-- e4e0e8c98d4a3b30bccdf629511890486ca3622f
SceneEditor:spawnShape("xmas_gingeryubo.shape", 29897.732, -11030.663, -20.35, 1.44, -0.09, 2.13, '{"scale x":1,"col size y":0.028,"col orientation":0,"col pos x":29897.732,"scale z":1,"col pos y":-11030.663,"scale y":1,"col size x":0.345,"col size z":0.252,"context":null,"url":null}')

-- 29a4fb26e4ced529a322bc79fe3562ed4eb9d839
SceneEditor:spawnShape("xmas_gingeryubo.shape", 29897.918, -11030.396, -20.354, 1.45, 0, 1.16, '{"scale x":1,"col size y":0.028,"col orientation":0,"col pos x":29897.918,"scale z":1,"col pos y":-11030.396,"scale y":1,"col size x":0.345,"col size z":0.252,"context":null,"url":null}')

-- ba55404e4a5fb7a3d857f7ea40a9b9e215bef357
SceneEditor:spawnShape("xmas_gingeryubo.shape", 29897.736, -11030.491, -20.355, -1.48, 0, -1.57, '{"scale x":1,"col size y":0.028,"col orientation":0,"col pos x":29897.736,"scale z":1,"col pos y":-11030.491,"scale y":1,"col size x":0.345,"col size z":0.252,"context":null,"url":null}')

-- 1dd2876ae13258c3d745153906b91e6c1c385c56
SceneEditor:spawnShape("ge_mission_sac_a.shape", 29898.449, -11030.812, -20.637, 1.23, 0, 1.38, '{"scale x":1,"col size y":0.875,"col orientation":-0.21,"col pos x":29898.59,"scale z":1,"col pos y":-11030.815,"scale y":1,"col size x":0.637,"col size z":0.404,"context":null,"url":null}')

-- 9060a4f6369d11672ef11e5d21642cf69efb0df8
SceneEditor:spawnShape("ge_mission_sac_a_mr_snow_spices.shape", 29891.684, -11023.604, -21.009, 0.79, -0.09, 1.8, '{"scale x":1,"col size y":1.165,"col orientation":0,"col pos x":29891.684,"scale z":1,"col pos y":-11023.604,"scale y":1,"col size x":0.967,"col size z":0.404,"context":null,"url":null}')

-- f955551f3fb50adee0da5c53dd1f76729b8202bf
SceneEditor:spawnShape("ge_mission_sac_a_mr_snow_spices.shape", 29891.688, -11024.234, -20.961, 0.9, 0, 1.03, '{"scale x":1,"col size y":1.165,"col orientation":0,"col pos x":29891.688,"scale z":1,"col pos y":-11024.234,"scale y":1,"col size x":0.967,"col size z":0.404,"context":null,"url":null}')

-- 9d1447ae9e72f122be05d909fa59144005593677
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 29897.943, -11030.629, -21.883, 0, 0, 0, '{"scale x":1.22,"col size y":1.184,"col orientation":-0.32,"col pos x":29897.943,"scale z":1.22,"col pos y":-11030.629,"scale y":1.22,"col size x":1.184,"col size z":1.241,"context":null,"url":null}')

-- b2526b30ea83d6f7cd814c41fdafdb4b1c1e7075
SceneEditor:spawnShape("ge_mission_yubo_stick.shape", 29891.164, -11022.632, -20.268, 0, -0.35, 0, '{"scale x":1,"col size y":0.473,"col orientation":0,"col pos x":29891.164,"scale z":1,"col pos y":-11022.632,"scale y":1,"col size x":0.12,"col size z":0.324,"context":null,"url":null}')

-- 2840f45019830f08a0075130e42d47192c8e84dd
SceneEditor:spawnShape("ge_mission_feu_off.shape", 29899.217, -11028.912, -21.063, 0, 0, 0, '{"scale x":1,"col size y":2.215,"col orientation":-0.18,"col pos x":29899.217,"scale z":1,"col pos y":-11028.912,"scale y":1,"col size x":2.031,"col size z":0.642,"context":null,"url":null}')

-- 7c61af552e838bd4ba7da1329d9da44605251964
SceneEditor:spawnShape("ge_mission_objet_pack_4.shape", 29890.562, -11022.568, -21.378, 0, 0, 3.18, '{"scale x":1,"col size y":4.345,"col orientation":0,"col pos x":29890.562,"scale z":1,"col pos y":-11022.568,"scale y":1,"col size x":3.046,"col size z":2.048,"context":null,"url":null}')

-- 8581b9c1b875668d48ffc152d965b02208d841e4
SceneEditor:spawnShape("GE_Mission_tente.shape", 29894.551, -11019.584, -21.292, 0, 0, 3.48, '{"scale x":1.2,"col size y":6.485,"col orientation":0.27,"col pos x":29894.551,"scale z":1.2,"col pos y":-11019.584,"scale y":1.2,"col size x":7.148,"col size z":5.034,"context":null,"url":null}')

-- a53d3927932ae7a90ae4a83f91d62723a1c63300
SceneEditor:spawnShape("GE_Mission_tente.shape", 29888.773, -11027.894, -21.23, 0, 0, 4.76, '{"scale x":1.4,"col size y":8.305,"col orientation":0.12,"col pos x":29888.773,"scale z":1.4,"col pos y":-11027.894,"scale y":1.4,"col size x":7.358,"col size z":5.034,"context":null,"url":null}')


-- GROUP: island_stable
-- 6dfa8af3ff3c8d112bc834e0437029d23f69ace4
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 29883.383, -11031.711, -21.172, 0, 0, -2.45, '{"col size y":3.854,"col pos x":29883.383,"scale y":0.769,"scale z":0.769,"scale x":0.769,"col pos y":-11031.711,"col size x":1.355,"col orientation":0.67,"col size z":2.147,"context":null,"url":null}')

-- ea4639012fec294f42d20941b40370529e58642d
SceneEditor:spawnShape("ge_mission_enclos.shape", 29880.24, -11038.397, -21.091, 0, 0, -2.14, '{"col size y":5.763,"col pos x":29880.24,"scale y":1,"scale z":1,"scale x":1,"col pos y":-11038.397,"col size x":7.121,"col orientation":-2.14,"col size z":6.629,"context":null,"url":null}')


