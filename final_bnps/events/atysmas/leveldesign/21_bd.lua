-- GROUP: stable_1
-- 97d6812ab11bd1ac15f061db231321a082935bcd
SceneEditor:spawnShape("xmas_stable2022.ps", 4712.073, -3211.028, -9.489, 0, 0, -0.83, '{"scale y":1,"col pos y":9108.972,"scale x":1,"scale z":1,"col size z":9.389,"col pos x":-36178.329,"col size y":9.577,"col orientation":0,"col size x":26.321}')

-- GROUP: yrk_deco
-- eef4cc65d98e369121641f774d6d4ee662a4428f
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4724.864, -3263.736, -7.397, 0, 0, 0, '{"col pos x":-24320,"scale z":0.8,"col orientation":0,"col size y":8.525,"scale y":0.8,"col pos y":9056.264,"col size z":11.357,"col size x":8.581,"scale x":0.8}')

-- 8e97bc24187b3aebb5d48905d34a5ca687cb4b94
SceneEditor:spawnShape("ge_mission_snowman.ps", 4704.283, -3208.374, -9.485, 0, 0, -3.64, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":9111.626,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 01c44a5d9774c7358b437a192daa85ac24ed27b7
SceneEditor:spawnShape("ge_mission_snowman.ps", 4781.434, -3234.561, -9.45, 0, 0, -2.69, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":9085.439,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- aeef85562950281a83911414529e9b0039f545c0
SceneEditor:spawnShape("ge_mission_snowman.ps", 4785.906, -3244.528, -9.497, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":9075.472,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 0b2d3a752065f20f0d30b7af65c2c97b49858ad2
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4646.815, -3226.276, -9.472, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":9093.724,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- 42a161db07a159b9ef47b1c4d9ce4cd20fd576b8
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4659.424, -3203.465, -9.483, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":9116.535,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- GROUP: yrk_main
-- 210dd0174ec82f23061d80e1a0156b3da3e584fb
SceneEditor:spawnShape("xmas_snowglobe.shape", 4709.801, -3213.345, -8.198, 0, 0, -1.41, '{"col size y":0.412,"col pos x":-12422.945,"scale y":0.36,"scale z":0.36,"scale x":0.36,"col pos y":-3213.345,"col size x":0.412,"col orientation":0,"col size z":0.519,"context":null,"url":null}')

-- cd4ce785c28b14a2c3529336448ae68135696a2e
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 4724.285, -3211.843, -9.506, 0, 0, 0, '{"col size y":8.525,"col pos x":-12447.725,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3211.843,"col size x":8.581,"col orientation":1.404,"col size z":10.379,"context":null,"url":null}')

-- 2383346c805f6c85c672d6aa25a71f83edd7919b
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 4716.946, -3242.721, -9.433, 0, 0, 0, '{"col size y":8.525,"col pos x":-12436.816,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3242.721,"col size x":8.581,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- 5d3d7f2bbe15872d07aeaf5934715eb27c381322
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 4703.606, -3253.491, -9.525, 0, 0, 0, '{"col size y":8.525,"col pos x":-12484.82,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3253.491,"col size x":8.581,"col orientation":2.7,"col size z":10.379,"context":null,"url":null}')

-- 9bec3d5dd9c20a3c199d8f178cb77e24272ed488
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 4671.467, -3231.24, -9.483, 0, 0, 0, '{"col size y":8.525,"col pos x":-12506.928,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3231.24,"col size x":8.581,"col orientation":0.461,"col size z":10.379,"context":null,"url":null}')

-- a4b5315fb328d589e97a269ee3c870176cd5806c
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 4684.99, -3210.904, -9.427, 0, 0, 0, '{"col size y":8.525,"col pos x":-12461.141,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3210.904,"col size x":8.581,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- d22672828abf349bd0aa258d004f0391ccd1fccc
SceneEditor:spawnShape("xmas_lights_01.shape", 4709.649, -3234.29, -6.315, 0, 0, 1.02, '{"col size y":0.606,"col pos x":-12434.023,"scale y":0.66,"scale z":0.66,"scale x":0.66,"col pos y":-3234.29,"col size x":7.757,"col orientation":0,"col size z":2.323,"context":null,"url":null}')

-- 367405138326aee806083e2a3fae82f46060974c
SceneEditor:spawnShape("xmas_lights_01.shape", 4688.157, -3226.659, -6.784, 0, 0, 0.89, '{"col size y":0.606,"col pos x":-12480.819,"scale y":0.827,"scale z":0.827,"scale x":0.827,"col pos y":-3226.659,"col size x":7.757,"col orientation":0.236,"col size z":2.323,"context":null,"url":null}')

-- 4670c21932362007e1024790e6777d3ded20510d
SceneEditor:spawnShape("xmas_lights_01.shape", 4676.127, -3222.251, -8.491, 0, 0, 1.41, '{"col size y":0.606,"col pos x":-12499.939,"scale y":1.163,"scale z":1.163,"scale x":1.163,"col pos y":-3222.251,"col size x":7.757,"col orientation":0,"col size z":2.323,"context":null,"url":null}')

-- f99581ff5924cf54aeaf22a65d7c36f6b5e95fda
SceneEditor:spawnShape("xmas_lights_01.shape", 4665.748, -3221.684, -7.791, 0, 0, 1.07, '{"col size y":0.606,"col pos x":0.421,"scale y":1.02,"scale z":1.02,"scale x":1.02,"col pos y":-3221.684,"col size x":7.757,"col orientation":0,"col size z":2.323,"context":null,"url":null}')

-- 61406e4f66689a3d5e9bbbb5c05793c173a0f532
SceneEditor:spawnShape("ge_mission_gift-turquoise.shape", 4706.482, -3210.045, -9.489, 0, 0, 0, '{"col size y":0.894,"col pos x":4706.482,"scale y":1.29,"scale z":1.29,"scale x":1.29,"col pos y":-3210.045,"col size x":0.894,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- d88bd0864494e8227fcb9c57ee45405f6e48f4ed
SceneEditor:spawnShape("ge_mission_gift-red.shape", 4706.746, -3211.224, -9.489, 0, 0, -0.48, '{"col size y":0.654,"col pos x":4706.746,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3211.224,"col size x":0.654,"col orientation":-0.48,"col size z":0.943,"context":null,"url":null}')

-- e844117a673a6fc5b18c26c0f3c33829b64fbe3f
SceneEditor:spawnShape("ge_mission_gift-purple.shape", 4715, -3218.672, -9.49, 0, 0, 0.73, '{"col size y":1.024,"col pos x":4715,"scale y":1.57,"scale z":1.57,"scale x":1.57,"col pos y":-3218.672,"col size x":1.024,"col orientation":0.73,"col size z":0.943,"context":null,"url":null}')

-- 32ed2ad9c7329c48c784b9331369407b6bb8f447
SceneEditor:spawnShape("ge_mission_gift-green.shape", 4713.779, -3218.074, -9.485, 0, 0, -1.3, '{"col size y":-0.806,"col pos x":4713.779,"scale y":1.31,"scale z":1.31,"scale x":1.31,"col pos y":-3218.074,"col size x":-0.806,"col orientation":-1.3,"col size z":0.943,"context":null,"url":null}')

-- 3f0d3065bd7549641d3e45b4d424ac09ac6410dc
SceneEditor:spawnShape("ge_mission_gift-brown.shape", 4714.021, -3219.313, -9.48, 0, 0, 0, '{"col size y":0.644,"col pos x":4714.021,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3219.313,"col size x":0.644,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- 9b7f94a430e82f0336d8ce7f63e267f3f39db12d
SceneEditor:spawnShape("snowhill.shape", 4663.644, -3214.79, -9.561, 0, 0, 0, '{"col size y":21.572,"col pos x":-12536.983,"scale y":0.37,"scale z":0.37,"scale x":0.37,"col pos y":-3214.793,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 560572419ba47e114fb5ae073f58c07152be6f32
SceneEditor:spawnShape("snowhill.shape", 4660.442, -3212.895, -9.482, 0, 0, 0, '{"col size y":21.572,"col pos x":-12536.83,"scale y":0.43,"scale z":0.43,"scale x":0.43,"col pos y":-3212.895,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 1d1a4a172a3b67e7b9fdd5d3f74a0187e2b06cd8
SceneEditor:spawnShape("snowhill.shape", 4659.068, -3223.367, -9.499, 0, 0, 0, '{"col size y":21.572,"col pos x":-12535.684,"scale y":0.42,"scale z":0.42,"scale x":0.42,"col pos y":-3223.367,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 226e5b0e98c96afd875999cb3ee6ab77a4ad77f2
SceneEditor:spawnShape("snowhill.shape", 4655.271, -3223.485, -9.473, 0, 0, 0, '{"col size y":21.572,"col pos x":-12544.307,"scale y":0.48,"scale z":0.48,"scale x":0.48,"col pos y":-3223.485,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 6d096037f2a2bfe38a9edd0f7900cba443c6bdf9
SceneEditor:spawnShape("snowhill.shape", 4652.037, -3228.57, -9.432, 0, 0, 0, '{"col size y":21.572,"col pos x":-12551.291,"scale y":0.55,"scale z":0.55,"scale x":0.55,"col pos y":-3228.57,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 38ce82719e2ae08cf7e0531a51e6cceb4c392bac
SceneEditor:spawnShape("snowhill.shape", 4662.208, -3207.568, -9.51, 0, 0, 0, '{"col size y":21.572,"col pos x":-12546.407,"scale y":0.6,"scale z":0.6,"scale x":0.6,"col pos y":-3207.568,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 45020682eca3a34ea880436c0586a34170249118
SceneEditor:spawnShape("xmas_ma_lantern.shape", 4690.346, -3223.839, -9.501, 0, 0, -0.57, '{"col size y":0.401,"col pos x":4690.346,"scale y":1.02,"scale z":1.02,"scale x":1.02,"col pos y":-3223.839,"col size x":0.442,"col orientation":0,"col size z":6.624,"context":null,"url":null}')

-- 2384dac82e1cf69e2932562683c39cd1b257b4b1
SceneEditor:spawnShape("xmas_sleigh.shape", 4729.705, -3215.186, -9.52, 0, 0, -1.46, '{"col size y":1.815,"col pos x":4729.705,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3215.186,"col size x":4.582,"col orientation":-1.46,"col size z":3.004,"context":null,"url":null}')

-- 3e691746e9d130cf4eb475ac17baae89af7b02ef
SceneEditor:spawnShape("xmas_ma_lantern.shape", 4711.346, -3232.038, -9.572, 0, 0, -1.25, '{"col size y":0.381,"col pos x":4711.346,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3232.038,"col size x":0.422,"col orientation":-1.25,"col size z":6.624,"context":null,"url":null}')

-- 42de8dd2ac79a9cad2c6590a44d9187b4330b9d2
SceneEditor:spawnShape("xmas_ma_lantern.shape", 4667.735, -3217.994, -9.46, 0, 0, -0.45, '{"col size y":0.341,"col pos x":4667.735,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3217.994,"col size x":0.412,"col orientation":-0.19,"col size z":6.624,"context":null,"url":null}')

-- 9a72869493c46dbff232db2d23a4f694f6b2ad00
SceneEditor:spawnShape("xmas_ma_lantern.shape", 4663.77, -3225.32, -9.454, 0, 0, 2.7, '{"col size y":0.271,"col pos x":4663.77,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3225.32,"col size x":0.332,"col orientation":2.84,"col size z":6.624,"context":null,"url":null}')

-- 4e9ebb3b93a79dd224748d20c66a12ee75784050
SceneEditor:spawnShape("xmas_ma_lantern.shape", 4675.307, -3226.848, -9.516, 0, 0, 2.89, '{"col size y":0.421,"col pos x":4675.307,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3226.848,"col size x":0.322,"col orientation":2.89,"col size z":6.624,"context":null,"url":null}')

-- d79856cd679c65268a810cfca33cbae6845f943b
SceneEditor:spawnShape("xmas_ma_lantern.shape", 4676.886, -3217.648, -9.467, 0, 0, -0.12, '{"col size y":0.401,"col pos x":4676.886,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3217.648,"col size x":0.362,"col orientation":-0.12,"col size z":6.624,"context":null,"url":null}')

-- 9b9780ae4bfbd1e5637af8d6e3c14754bc058eb8
SceneEditor:spawnShape("snowhill.shape", 4700.852, -3219.755, -9.484, 0, 0, 0, '{"col size y":21.572,"col pos x":0,"scale y":0.41,"scale z":0.41,"scale x":0.41,"col pos y":-3219.755,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 2c9b418fbc22e4cd14e537f641afa080df147d1f
SceneEditor:spawnShape("xmas_lights_01.shape", 4709.118, -3212.672, -8.588, 0, 0, -0.83, '{"col size y":0.606,"col pos x":0,"scale y":0.45,"scale z":0.46,"scale x":0.45,"col pos y":-3212.672,"col size x":7.757,"col orientation":0,"col size z":2.323,"context":null,"url":null}')

-- b7fe29d50d35ede4c198cecba5fef20541013b4c
SceneEditor:spawnShape("snowhill.shape", 4725.077, -3264.026, -9.487, 0, 0, 0, '{"col size y":21.572,"col pos x":-12453.765,"scale y":1,"scale z":1,"scale x":1,"col pos y":-3264.026,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')


