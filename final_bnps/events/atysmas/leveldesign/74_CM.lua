-- GROUP: stable
-- d12ba75b71df150b872cfb6c15c24530f55df560
SceneEditor:spawnShape("xmas_stable2022.ps", 10348.164, -11734.939, -1.761, 0, 0, 1.77, '{"col pos y":-11734.939,"col size x":26.605,"col size y":10.641,"scale x":1,"col pos x":0,"scale y":1,"col orientation":0,"scale z":1,"col size z":9.534,"context":null,"url":null}')


-- GROUP: dead_santa_deco
-- cf672dad4a9b0fc2bb4b9280e28adf3847575f53
SceneEditor:spawnShape("goo_mo_charogneinsect.ps", 10305.126, -11795.284, -3.207, 0, 0, 0, '{"scale z":1,"scale x":1,"col size z":9.009,"col size x":5.731,"scale y":1,"col orientation":-2.856,"col pos x":10305.126,"col size y":8.971,"col pos y":-11795.284,"context":null,"url":null}')

-- 3f9f742545acb27a61f263ddee4e9ecb8f9824bc
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 10304.639, -11795.981, -1.771, 0, 0, 0, '{"scale z":0.59,"scale x":0.59,"col size z":0.943,"col size x":0.704,"scale y":0.59,"col orientation":0,"col pos x":10304.639,"col size y":0.704,"col pos y":-11795.981,"context":null,"url":null}')

-- 24a290aef3427f26678d1fc51ea4d9c3a5aaecff
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 10305.136, -11794.411, -1.785, 0, 0, 0, '{"scale z":0.25,"scale x":0.25,"col size z":0.943,"col size x":0.704,"scale y":0.25,"col orientation":0,"col pos x":10305.136,"col size y":0.704,"col pos y":-11794.411,"context":null,"url":null}')

-- 09d0dea72836adc4fe12cd15b586fe62ff869e91
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 10304.146, -11794.278, -1.786, 0, 0, 0, '{"scale z":0.38,"scale x":0.38,"col size z":0.943,"col size x":0.704,"scale y":0.38,"col orientation":0,"col pos x":10304.146,"col size y":0.704,"col pos y":-11794.278,"context":null,"url":null}')

-- 4e54312a6a8f3b08b17b73156bdd8308fafeaeae
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 10305.38, -11796.5, -1.767, 0, 0, -0.41, '{"scale z":0.43,"scale x":0.43,"col size z":0.943,"col size x":0.704,"scale y":0.43,"col orientation":0,"col pos x":10305.38,"col size y":0.704,"col pos y":-11796.5,"context":null,"url":null}')

-- 63887f92a089c8c9e8ccf68d331f16933b28a5ce
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 10305.257, -11796.054, -1.771, 0, 0, 0, '{"scale z":0.36,"scale x":0.36,"col size z":0.943,"col size x":0.704,"scale y":0.36,"col orientation":0,"col pos x":10305.257,"col size y":0.704,"col pos y":-11796.054,"context":null,"url":null}')
