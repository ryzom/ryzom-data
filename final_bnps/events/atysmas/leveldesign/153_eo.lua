-- GROUP: stable_pyr
-- c18038c3af20958dc44389a7107814b81e13f96f
SceneEditor:spawnShape("xmas_stable2022.ps", 18963.371, -24408.867, -1.956, 0, 0, -1.45, '{"col pos y":-12088.867,"col pos x":-24319.701,"col orientation":0,"col size z":9.389,"scale z":1,"scale x":1,"col size x":26.321,"scale y":1,"col size y":9.577}')

-- GROUP: pyr_deco
-- d7010e9e237b9a8d45d2a8b273db459fc7fbd146
SceneEditor:spawnShape("ge_mission_snowman.ps", 18919.635, -24371.617, -1.326, 0, 0, -2.91, '{"col size z":3.66,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-12051.617,"col pos x":-24320,"col size y":1.601,"col size x":1.527,"scale x":1}')

-- d157ef4a83d4745046a9066250775486b26c32aa
SceneEditor:spawnShape("ge_mission_snowman.ps", 18939.143, -24379.072, -0.933, 0, 0, -3.08, '{"col size z":3.66,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-12059.072,"col pos x":-24319.479,"col size y":1.601,"col size x":1.527,"scale x":1}')

-- 7e13cf00feb0d0a7ee3779dccffe1719798eb0a2
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 18960.266, -24395.252, -1.91, 0, 0, 0, '{"col size z":11.357,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-12075.252,"col pos x":-24320,"col size y":8.525,"col size x":8.581,"scale x":1}')

-- GROUP: fyros_main
-- d747abc54388604a5edb6700adf2249804ac4fae
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18909.701, -24352.172, -4.033, 0, 0, 0.53, '{"col size y":0.156,"col pos x":18909.701,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24352.172,"col size x":0.255,"col orientation":0.917,"col size z":4.34,"context":null,"url":null}')

-- 3a69c5eb953e882e6ba4fbf636667749bf40b6af
SceneEditor:spawnShape("xmas_snowman.shape", 18903.879, -24368.154, -6.354, 0, 0, 1.77, '{"col size y":1.651,"col pos x":1709.127,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24368.154,"col size x":2.832,"col orientation":-2.881,"col size z":3.66,"context":null,"url":null}')

-- 4e80614b1aacef923657de2528cf48cea2c7285a
SceneEditor:spawnShape("snowhill.shape", 18907.924, -24390.031, -6.519, 0, 0, 0, '{"col size y":21.572,"col pos x":-3.5433918004359685e+28,"scale y":0.5,"scale z":0.5,"scale x":0.5,"col pos y":-24390.031,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- d5be549fca19669b6bc38befaac732013554ac11
SceneEditor:spawnShape("snowhill.shape", 18892.75, -24351.34, -6.269, 0, 0, 0, '{"col size y":21.572,"col pos x":0,"scale y":0.25,"scale z":0.25,"scale x":0.25,"col pos y":-24351.34,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 06e710c89d28dc363c9301bf5c86a0bf152e8889
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 18923.129, -24328.246, -6.277, 0, 0, 0, '{"col size y":0.815,"col pos x":18923.129,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24328.246,"col size x":0.871,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- 54b5d535ba4d0746bd1d4f07bfa4f1a1bd645587
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 18879.602, -24347.43, -6.237, 0, 0, 0, '{"col size y":0.465,"col pos x":18879.602,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24347.43,"col size x":0.521,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- 840b407e06d910a4a4438ca9d5483f8b6f8fac29
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 18883.295, -24380.723, -6.354, 0, 0, 0, '{"col size y":0.715,"col pos x":18883.295,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24380.723,"col size x":0.771,"col orientation":0,"col size z":10.399,"context":null,"url":null}')

-- 280a13da42b492a215db18dff3bddc6d8e5b8035
SceneEditor:spawnShape("xmas_snowglobe.shape", 18893.379, -24350.982, -6.262, 0, 0, -1.13, '{"col size y":2.172,"col pos x":18893.379,"scale y":6.57,"scale z":6.57,"scale x":6.57,"col pos y":-24350.982,"col size x":2.172,"col orientation":0.303,"col size z":0.519,"context":null,"url":null}')

-- affc86e57234cc624736a26ada8e51bb707b3a71
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18928.109, -24334.998, -5.739, 0, 0, -2.94, '{"col size y":0.166,"col pos x":18928.109,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24334.998,"col size x":0.185,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- f68880189f8e4000dde286454cedbf160a3cbc1a
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18880.727, -24343.895, -6.164, 0, 0, -0.34, '{"col size y":0.206,"col pos x":3.5433918004359685e+28,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24342.352,"col size x":1.285,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 5914dff5d564876944140c1f274f9f88d4302deb
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18883.008, -24336.961, -6.354, 0, 0, -0.21, '{"col size y":0.196,"col pos x":18883.008,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24336.961,"col size x":0.245,"col orientation":1.394,"col size z":4.34,"context":null,"url":null}')

-- 799eaa05eacfca4adbd11cfd2c17fb8aa2f1b954
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18884.824, -24330.043, -6.354, 0, 0, -0.24, '{"col size y":0.216,"col pos x":18884.824,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24330.043,"col size x":0.165,"col orientation":0.21,"col size z":4.34,"context":null,"url":null}')

-- 04208cd509f48d3bfc06fb6865eba89cc0303e27
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18887.717, -24384.426, -6.354, 0, 0, 1.46, '{"col size y":0.146,"col pos x":18887.717,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24384.426,"col size x":0.145,"col orientation":2.541,"col size z":4.34,"context":null,"url":null}')

-- fd9f77710e9d9d8711a362a5672e627887cef100
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18892.643, -24385.52, -6.391, 0, 0, 1.24, '{"col size y":0.206,"col pos x":18892.643,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24385.52,"col size x":0.385,"col orientation":-1.541,"col size z":4.34,"context":null,"url":null}')

-- d968a47480a50a7135156cb046025739713a4cf4
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18896.748, -24387.879, -6.527, 0, 0, 0.72, '{"col size y":0.176,"col pos x":3.5433918004359685e+28,"scale y":1,"scale z":1,"scale x":1,"col pos y":0,"col size x":0.215,"col orientation":-1.217,"col size z":4.34,"context":null,"url":null}')

-- 12484f1084cd7032baacf44a4badb0ddab7f2e77
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18907.992, -24387.152, -6.389, 0, 0, -3.18, '{"col size y":0.116,"col pos x":18907.992,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24387.152,"col size x":0.155,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 90c8ab5844dcedc3f82e357077c38fe92bb0b6c4
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18907.836, -24382.426, -6.366, 0, 0, 3.21, '{"col size y":0.106,"col pos x":18907.836,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24382.426,"col size x":0.155,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 972a02019f6578739c5cd0b37ec90e2f54c68e25
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18907.133, -24377.738, -6.354, 0, 0, -3.02, '{"col size y":0.106,"col pos x":18907.133,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24377.738,"col size x":0.175,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 486687fe83f79307835aa7c6ec5d30fe2b748913
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18899.582, -24360.111, -6.354, 0, 0, -2.45, '{"col size y":0.096,"col pos x":18899.582,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24360.111,"col size x":0.115,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- d8453e12011b3343d39a470678ec67d0503a7198
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18906.205, -24372.523, -6.354, 0, 0, -2.9, '{"col size y":0.156,"col pos x":18906.205,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24372.523,"col size x":0.115,"col orientation":0.203,"col size z":4.34,"context":null,"url":null}')

-- 63180a1bf6c5a40da39011fdcd786ab738899db1
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18929.277, -24345.852, -4.271, 0, 0, -2.95, '{"col size y":0.086,"col pos x":18929.277,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24345.852,"col size x":0.105,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 8cd19c73a42c482f821ba314b31c417b0c891546
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18902.326, -24363.682, -6.354, 0, 0, 3.7, '{"col size y":0.116,"col pos x":18902.326,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24363.682,"col size x":0.115,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- f8123e5d8e7abf55ed9511c40e7cd1f58446ce8f
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18896.92, -24356.879, -6.354, 0, 0, -2.4, '{"col size y":0.146,"col pos x":18896.92,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24356.879,"col size x":0.205,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- b7a6a0e5864020324b0fddb99f59a85f3658dc9a
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18894.441, -24353.084, -6.354, 0, 0, -2.95, '{"col size y":-0.184,"col pos x":18894.441,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24353.084,"col size x":0.135,"col orientation":1.476,"col size z":4.34,"context":null,"url":null}')

-- b679283466316310801018c8a01fc1eae7b8e2f3
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18893.059, -24348.42, -6.165, 0, 0, 2.81, '{"col size y":-0.134,"col pos x":18893.059,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24348.42,"col size x":0.105,"col orientation":-2.435,"col size z":4.34,"context":null,"url":null}')

-- 8ab9d3e705ddeea36e85ba194df890d8d618bcd2
SceneEditor:spawnShape("ge_mission_gift-turquoise.shape", 18900.82, -24340.283, -6.356, 0, 0, 0, '{"col size y":0.704,"col pos x":1697.492,"scale y":1.15,"scale z":1.15,"scale x":1.15,"col pos y":-24340.283,"col size x":0.704,"col orientation":1.524,"col size z":0.943,"context":null,"url":null}')

-- 42662e20603d1a2f7ae8990e9ebb890b56dd345c
SceneEditor:spawnShape("ge_mission_gift-green.shape", 18903.539, -24344.191, -6.127, 0, 0, 0, '{"col size y":0.704,"col pos x":1703.961,"scale y":1.45,"scale z":1.45,"scale x":1.45,"col pos y":-24344.191,"col size x":0.704,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- f109247bc6e0cbd56ae4dccf43cc42bccab306d1
SceneEditor:spawnShape("ge_mission_gift-brown.shape", 18899.645, -24341.848, -6.354, 0, 0, 0, '{"col size y":0.704,"col pos x":1704.893,"scale y":1.68,"scale z":1.68,"scale x":1.68,"col pos y":-24341.848,"col size x":0.704,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- e4e59f728f6f087ae0f915737b7bc8a4685fccae
SceneEditor:spawnShape("ge_mission_gift-red.shape", 18903.48, -24342.102, -6.325, 0, 0, 0, '{"col size y":0.704,"col pos x":1706.209,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24342.102,"col size x":0.704,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- b6cd67d25967f753e9ba47d97a7392f6cf2994ba
SceneEditor:spawnShape("ge_mission_gift.shape", 18902.852, -24340.684, -6.357, 0, 0, 0, '{"col size y":0.704,"col pos x":18902.852,"scale y":1.17,"scale z":1.17,"scale x":1.17,"col pos y":-24340.684,"col size x":0.704,"col orientation":0,"col size z":0.943,"context":null,"url":null}')


