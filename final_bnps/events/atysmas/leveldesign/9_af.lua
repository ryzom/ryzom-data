-- GROUP: stable_maro
-- 80f180663bb0c4d81a6f4efbae58be184f1f1780
SceneEditor:spawnShape("xmas_stable2022.ps", 893.073, -1349.862, 50.894, 0, 0, 1.58, '{"col size y":-8.13,"col size x":-5.95,"col size z":6.98,"col orientation":1.54,"col pos x":-23427.207,"scale x":1,"scale z":1,"col pos y":10969.426,"scale y":1}')

-- GROUP: maro_xmas_deco
-- 48c34f74d6b26167b2fbc061a1a68dc2d8d86c6b
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 889.961, -1339.397, 50.526, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":0.704,"scale y":1,"col pos y":10980.603,"col size z":0.943,"col size x":0.704,"scale x":1}')

-- 6d63e3dbd183dacd817bc6c1787b712ee8bd8cd0
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 890.081, -1341.589, 50.726, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":0.704,"scale y":1,"col pos y":10978.411,"col size z":0.943,"col size x":0.704,"scale x":1}')

-- 633f1683d81295e70d9265f6c7681a9e0256c2c7
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 891.008, -1339.756, 50.63, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":0.704,"scale y":1,"col pos y":10980.244,"col size z":0.943,"col size x":0.704,"scale x":1}')

-- 9f5d385f9bce53996be6fceaf3f74a1d375cbae4
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 890.103, -1340.54, 50.634, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":0.704,"scale y":1,"col pos y":10979.46,"col size z":0.943,"col size x":0.704,"scale x":1}')

-- f6bab400a2d83a361f7b7838957380406fce0bf3
SceneEditor:spawnShape("ge_mission_snowman.ps", 891.307, -1341.729, 50.746, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":10978.271,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- cffb3c3ebcc48f30308c9e5d44d47632c0af0d30
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 919.583, -1309.765, 53.558, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":11010.235,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- f9afccba0e3946366eb4893fcf244d83b5b8dd84
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 938.04, -1338.33, 46.906, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":10981.67,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- bfb1e8db58823f4ca3352ed5f45c68a77a4889b2
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 919.041, -1387.16, 50.325, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":10932.84,"col size z":11.357,"col size x":8.581,"scale x":1}')