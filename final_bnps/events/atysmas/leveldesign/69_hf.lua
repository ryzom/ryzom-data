-- GROUP: island_main
-- 01dbd60a8c88c33449676b503e4c5edd465868c0
SceneEditor:spawnShape("env-snow.ps", 29982.125, -11037.4, -20.988, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11037.4,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 91d06ac17a35e4d380854a7905d1d9224431e0ad
SceneEditor:spawnShape("env-snow.ps", 29956.035, -11009.129, -22.28, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11009.129,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 6628a1c60d47369fb3ed5dd091139fa4124a8b20
SceneEditor:spawnShape("env-snow.ps", 29955.914, -11038.466, -20.99, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11038.466,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 522c730cdd05fed61a3e99c25f4c43ae576f0177
SceneEditor:spawnShape("env-snow.ps", 29925.998, -10979.219, -20.14, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-10979.219,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 9149cbcd511179070a90a03c32de7d280a1445f7
SceneEditor:spawnShape("env-snow.ps", 29925.93, -11008.843, -21.091, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11008.843,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 481df1c407fdc4640c015151b94a2868559505f9
SceneEditor:spawnShape("env-snow.ps", 29925.93, -11038.253, -20.965, 0, 0, 0, '{"col size x":30,"col pos x":0,"scale x":1,"scale y":1,"col pos y":-11038.253,"col orientation":0,"col size y":30,"scale z":1,"col size z":100,"context":null,"url":null}')

-- 73395674298d99b450152c74178d67b416781571
SceneEditor:spawnShape("snowhill.shape", 29920.885, -10998.777, -21.055, 0, 0, 0, '{"col size x":21.946,"col pos x":-0.848,"scale x":0.51,"scale y":0.51,"col pos y":-10998.777,"col orientation":0,"col size y":21.572,"scale z":0.51,"col size z":5.142,"context":null,"url":null}')

-- 36d8e386751942fcf7189b27e41ac5128383df0c
SceneEditor:spawnShape("snowhill.shape", 29943.852, -11020.817, -21.161, 0, 0, 0, '{"col size x":21.946,"col pos x":0,"scale x":0.52,"scale y":0.52,"col pos y":-11023.48,"col orientation":0,"col size y":21.572,"scale z":0.52,"col size z":5.142,"context":null,"url":null}')

-- 09ed0ea4665e63b85c4b5d35a7dbfd45128cdd0d
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29921.064, -10998.43, -21.053, 0, 0, 0, '{"col size x":2.831,"col pos x":29921.064,"scale x":1,"scale y":1,"col pos y":-10998.43,"col orientation":0,"col size y":2.775,"scale z":1,"col size z":11.357,"context":null,"url":null}')

-- 7218ee67e64d46160763f2574202d89709274ed4
SceneEditor:spawnShape("ge_mission_xmass_tree.shape", 29943.957, -11021.001, -21.183, 0, 0, -0.76, '{"col size x":4.331,"col pos x":29943.957,"scale x":1.5,"scale y":1.5,"col pos y":-11021.001,"col orientation":0,"col size y":4.275,"scale z":1.5,"col size z":11.357,"context":null,"url":null}')

-- 4462b492c0fdbdff1c5930528c0f105c0f77d753
SceneEditor:spawnShape("snowhill.shape", 29929.914, -11010.812, -21.2, 0, 0, 6.08, '{"col size x":8.386,"col pos x":29929.914,"scale x":0.87,"scale y":0.87,"col pos y":-11010.812,"col orientation":0,"col size y":8.012,"scale z":0.87,"col size z":5.142,"context":null,"url":null}')

-- 66d45d3432e391891c7ff8a5ac2477baae8a9070
SceneEditor:spawnShape("xmas_statue.shape", 29931.129, -11010.166, -21.153, 0, 0, -0.21, '{"col size x":8.755,"col pos x":29931.129,"scale x":2.04,"scale y":2.04,"col pos y":-11010.166,"col orientation":0,"col size y":8.824,"scale z":2.04,"col size z":7.774,"context":null,"url":null}')


-- GROUP: island_stable
-- b84f04977b1e7ff4cafcae2c4a059c6d920c124a
SceneEditor:spawnShape("xmas_fy_lantern.shape", 29960.408, -11039.465, -21.048, 0, 0, -2.26, '{"col size y":0.596,"col pos x":29960.408,"scale y":1,"scale z":1,"scale x":1,"col pos y":-11039.465,"col size x":1.675,"col orientation":-2.26,"col size z":4.34,"context":null,"url":null}')

-- f7505f18a256c6911c48960a0bdfc52898d4c4f5
SceneEditor:spawnShape("snowhill.shape", 29958.764, -11034.065, -21.105, 0, 0, 0, '{"col size y":21.572,"col pos x":0,"scale y":0.85,"scale z":0.85,"scale x":0.85,"col pos y":-11034.065,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 5e4a11c1959df15b9e6cf36ef8e44c33005a2599
SceneEditor:spawnShape("snowhill.shape", 29951.814, -11038.091, -20.972, 0, 0, 0, '{"col size y":21.572,"col pos x":0,"scale y":0.74,"scale z":0.74,"scale x":0.74,"col pos y":-11040.754,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 192d5380210383cecae5f5b832d92bc6b6f287a0
SceneEditor:spawnShape("xmas_snowman.shape", 29956.775, -11038.685, -20.969, 0, 0, 2.27, '{"col size y":8.141,"col pos x":29956.775,"scale y":3.21,"scale z":3.21,"scale x":3.21,"col pos y":-11038.685,"col size x":9.322,"col orientation":2.27,"col size z":3.66,"context":null,"url":null}')


