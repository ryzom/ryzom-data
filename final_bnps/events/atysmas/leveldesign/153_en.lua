-- GROUP: fyros_main
-- 79a3fba8e9eaa193b3a6145e31bb20cb2f28046a
SceneEditor:spawnShape("xmas_sleigh.shape", 18855, -24367.949, -6.354, 0, 0, 0, '{"col size y":1.815,"col pos x":18855,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24367.949,"col size x":4.582,"col orientation":0,"col size z":3.004,"context":null,"url":null}')

-- 3f8971918fa96c030e411e35bf21eb6910da90c0
SceneEditor:spawnShape("snowhill.shape", 18872.773, -24356.16, -6.346, 0, 0, 0, '{"col size y":21.572,"col pos x":1673.195,"scale y":0.33,"scale z":0.33,"scale x":0.33,"col pos y":-24356.16,"col size x":21.946,"col orientation":1.708,"col size z":5.142,"context":null,"url":null}')

-- 2186c6be63101b455bc500d257e3093cd9e8f678
SceneEditor:spawnShape("snowhill.shape", 18865.334, -24385.637, -6.354, 0, 0, 0, '{"col size y":21.572,"col pos x":-3.5433918004359685e+28,"scale y":0.69,"scale z":0.69,"scale x":0.69,"col pos y":-24385.637,"col size x":21.946,"col orientation":1.063,"col size z":5.142,"context":null,"url":null}')

-- d446a341a62d21d180c82e1d6ac98bde3e76a40b
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 18798.006, -24394.047, -6.383, 0, 0, 0, '{"col size y":0.425,"col pos x":18798.006,"scale y":2.75,"scale z":2.75,"scale x":2.75,"col pos y":-24394.047,"col size x":0.481,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- 046ca4b4c0ff91cc9d3edbdc6e7985f287b2f8bd
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18877.75, -24384.062, -6.354, 0, 0, 1.63, '{"col size y":0.136,"col pos x":18877.75,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24384.062,"col size x":0.195,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 192dd3ec5d47d3ed2065c1b3e53ca3c7dff8f858
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18862.002, -24365.512, -6.354, 0, 0, -1.21, '{"col size y":0.106,"col pos x":18862.002,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24365.512,"col size x":0.195,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- d7d9b5f9c02280003dca40d564fa75ac94c3c4c4
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18865.168, -24363.816, -6.354, 0, 0, -0.99, '{"col size y":0.156,"col pos x":18865.168,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24363.816,"col size x":0.175,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 15f81193c50947072e68221732eb5f7b4b76ec6a
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18869.658, -24359.961, -6.354, 0, 0, -0.76, '{"col size y":0.136,"col pos x":18869.658,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24359.961,"col size x":0.215,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- 07b3d7f1a9cf60aa6d096d413467111245dc5216
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18872.434, -24356.797, -6.354, 0, 0, -0.56, '{"col size y":0.206,"col pos x":18872.434,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24356.797,"col size x":0.215,"col orientation":0,"col size z":4.34,"context":null,"url":null}')

-- f4af0d6c97626274b34c514f9063149ea7975085
SceneEditor:spawnShape("xmas_fy_lantern.shape", 18858.531, -24380.855, -6.354, 0, 0, 1.01, '{"col size y":0.136,"col pos x":18858.531,"scale y":1,"scale z":1,"scale x":1,"col pos y":-24380.855,"col size x":0.195,"col orientation":0,"col size z":4.34,"context":null,"url":null}')


