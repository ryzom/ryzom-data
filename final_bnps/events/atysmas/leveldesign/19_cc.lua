-- GROUP: stable_zora
-- 0327f2da28d32f3157f6deee8ab6413e53e52133
SceneEditor:spawnShape("xmas_stable2022.ps", 8746.503, -2986.903, -77.011, 0, 0, -2.78, '{"col pos y":9333.097,"col size x":26.321,"scale z":1,"col size z":9.389,"col orientation":0,"scale x":1,"col size y":9.577,"scale y":1,"col pos x":-24318.506}')

-- GROUP: zora_deco
-- 747fdbd25029dd02e01a24adbc24bc1649b1af53
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 8717.598, -3007.558, -77.177, 0, 0, 0, '{"col orientation":-0.923,"col size z":11.357,"scale z":1,"col pos y":9312.442,"col size y":8.525,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":8.581}')

-- 0d4c29de8f4d16e6598fb5a280b5e5f055dd784e
SceneEditor:spawnShape("ge_mission_snowman.ps", 8757.685, -2952.515, -76.831, 0, 0, -1.96, '{"col orientation":0,"col size z":3.66,"scale z":1,"col pos y":9367.485,"col size y":1.601,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":1.527}')

-- 08d7c138b5bcce83f9d1f864b7e910dbcd01ef46
SceneEditor:spawnShape("Ge_mission_Gift.shape", 8762.02, -2951.092, -76.934, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9368.908,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- d7ebbeb5d62eca8b64281c8e5ba3845942dc5b9f
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 8762.413, -2949.56, -76.908, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9370.44,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 925c42ef79059bc2637e8599a9f8028c8011a8b1
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 8761.034, -2948.412, -76.889, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9371.588,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 473a979d5aa5055e2081576703ce5e1117de46e9
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 8773.384, -2949.419, -77.03, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9370.581,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- e2f1a83789cf58da5feb5ce058667f2d042c65ad
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 8771.141, -2948.134, -77.007, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9371.866,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 2d443e130de09c7d22d1f0a09b80e4af901187c1
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 8771.9, -2950.171, -77.017, 0, 0, 0, '{"col orientation":-0.192,"col size z":0.943,"scale z":1,"col pos y":9369.829,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 9dbae1930635d8a019f3f4fd4ab7c7941420ad87
SceneEditor:spawnShape("ge_mission_snowman.ps", 8730.728, -3019.03, -77.362, 0, 0, -3.37, '{"col orientation":-1.828,"col size z":3.66,"scale z":1,"col pos y":9300.97,"col size y":1.601,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":1.527}')

-- a421635f3bd892b1e0de4b1d257edfc192f72685
SceneEditor:spawnShape("ge_mission_snowman.ps", 8713.349, -3019.602, -77.335, 0, 0, -3.25, '{"col orientation":0,"col size z":3.66,"scale z":0.99,"col pos y":9300.398,"col size y":1.601,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":1.527}')

-- GROUP: zorai_main
-- 93be32bdd8deb9e0602cf8dc0a00b4adf58563f9
SceneEditor:spawnShape("xmas_lights_02.shape", 8756.111, -2961.916, -77.289, 0, 0, 0.31, '{"col size y":5.521,"col pos x":-8376.635,"scale y":1.6,"scale z":1.6,"scale x":1.6,"col pos y":-2961.916,"col size x":5.591,"col orientation":0,"col size z":2.333,"context":null,"url":null}')

-- 113e8fc48b6124dac544c9e5eedd63c8359da05b
SceneEditor:spawnShape("xmas_lights_01.shape", 8746.134, -2983.531, -76.428, 0, 0, 0.32, '{"col size y":0.606,"col pos x":-8425.876,"scale y":0.51,"scale z":0.51,"scale x":0.51,"col pos y":-2983.531,"col size x":7.757,"col orientation":0,"col size z":2.323,"context":null,"url":null}')

-- 41a73fb36a48f9f28ed678b68b958b4e6ff1a790
SceneEditor:spawnShape("snowhill.shape", 8707.812, -2968.065, -79.981, 0, 0, 0, '{"col size y":21.572,"col pos x":-8445.95,"scale y":0.69,"scale z":0.69,"scale x":0.69,"col pos y":-2968.065,"col size x":21.946,"col orientation":0.817,"col size z":5.142,"context":null,"url":null}')

-- aaf7ef25168a6e996274593a85ea22db70f14a92
SceneEditor:spawnShape("snowhill.shape", 8759.94, -2990.477, -77.098, 0, 0, 0, '{"col size y":21.572,"col pos x":-8428.485,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2990.477,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- cc3e99d0ea460d1b3f98c031b8b816bfa5b96e44
SceneEditor:spawnShape("snowhill.shape", 8759.176, -2970.433, -76.996, 0, 0, 0, '{"col size y":21.572,"col pos x":-8419.219,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2970.433,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 5ee75ca3a7fd0d464d23ac95e271810738fe93c9
SceneEditor:spawnShape("snowhill.shape", 8751.053, -2956.585, -76.762, 0, 0, 0, '{"col size y":21.572,"col pos x":-8395.078,"scale y":0.36,"scale z":0.36,"scale x":0.36,"col pos y":-2956.585,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 7a1cf94498a1eaeaa28f58e23376e76362d19d92
SceneEditor:spawnShape("snowhill.shape", 8726.509, -2979.546, -77.249, 0, 0, 0, '{"col size y":21.572,"col pos x":-8417.163,"scale y":0.37,"scale z":0.37,"scale x":0.37,"col pos y":-2979.546,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- a109561f3dd880512b460c28dd3077fce15d647e
SceneEditor:spawnShape("ge_mission_gift-brown.shape", 8754.146, -2979.65, -76.994, 0, 0, 0, '{"col size y":0.594,"col pos x":8754.146,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2979.65,"col size x":0.594,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- e43b436521b15398a5727323fe9be5f6517abbc5
SceneEditor:spawnShape("ge_mission_gift-green.shape", 8755.354, -2980.291, -76.982, 0, 0, 0.41, '{"col size y":1.024,"col pos x":8755.354,"scale y":1.65,"scale z":1.65,"scale x":1.65,"col pos y":-2980.291,"col size x":1.024,"col orientation":0.41,"col size z":0.943,"context":null,"url":null}')

-- bf9286189de76446a49207e9ab46bdd1f66828cc
SceneEditor:spawnShape("ge_mission_gift-turquoise.shape", 8753.919, -2981.195, -77.015, 0, 0, -0.71, '{"col size y":1.194,"col pos x":8753.919,"scale y":1.91,"scale z":1.91,"scale x":1.91,"col pos y":-2981.195,"col size x":1.194,"col orientation":-0.71,"col size z":0.943,"context":null,"url":null}')

-- c66d99f29df5ed8eced6e08648c5f66b685839a3
SceneEditor:spawnShape("ge_mission_gift-red.shape", 8734.321, -2952.12, -76.422, 0, 0, 0.24, '{"col size y":0.634,"col pos x":8734.321,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2952.12,"col size x":0.634,"col orientation":0.26,"col size z":0.943,"context":null,"url":null}')

-- 2eff8da92880fb8ff58807128d0003b2c9b3597f
SceneEditor:spawnShape("ge_mission_gift-purple.shape", 8733.325, -2951.738, -76.434, 0, 0, -0.19, '{"col size y":0.704,"col pos x":8733.325,"scale y":1.35,"scale z":1.35,"scale x":1.35,"col pos y":-2951.738,"col size x":0.704,"col orientation":0,"col size z":0.943,"context":null,"url":null}')

-- 927d709f5e6e22bf1e82fdd515d30d2492c092a0
SceneEditor:spawnShape("snowhill.shape", 8736.89, -2948.971, -76.385, 0, 0, 0, '{"col size y":21.572,"col pos x":-10.113,"scale y":0.19,"scale z":0.19,"scale x":0.19,"col pos y":-2948.971,"col size x":21.946,"col orientation":0.728,"col size z":5.142,"context":null,"url":null}')

-- 65785a79864e3de6772f1befbc85c876c2216fde
SceneEditor:spawnShape("snowhill.shape", 8746.576, -2951.56, -76.566, 0, 0, 0, '{"col size y":21.572,"col pos x":5.24,"scale y":0.38,"scale z":0.38,"scale x":0.38,"col pos y":-2951.56,"col size x":21.946,"col orientation":0,"col size z":5.142,"context":null,"url":null}')

-- 707ebf9b321bfe5f567f67f813aacb226f4654a0
SceneEditor:spawnShape("snowhill.shape", 8734.539, -2950.077, -76.375, 0, 0, 0, '{"col size y":21.572,"col pos x":0.269,"scale y":0.27,"scale z":0.27,"scale x":0.27,"col pos y":-2950.077,"col size x":21.946,"col orientation":-1.231,"col size z":5.142,"context":null,"url":null}')

-- 5f5a7a4621f3b3324527144bdd827dcd60182d43
SceneEditor:spawnShape("xmas_statue.shape", 8734.409, -2948.676, -76.382, 0, 0, -0.47, '{"col size y":3.114,"col pos x":8734.409,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2948.676,"col size x":3.045,"col orientation":-0.47,"col size z":7.774,"context":null,"url":null}')

-- 5fd050d4034e45503243f4ffdeb14da9f7b3ec96
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8717.133, -2929.453, -84.669, 0, 0, 3.12, '{"col size y":1.34,"col pos x":-8480.139,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2929.453,"col size x":0.907,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- eafd6730e38e9ac0afa7d82130d1ef5de5b9b02c
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8712.688, -2939.983, -82.265, 0, 0, -1, '{"col size y":1.34,"col pos x":-8482.063,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2939.983,"col size x":0.907,"col orientation":0.66,"col size z":4.146,"context":null,"url":null}')

-- 799f2a8cc272995299bbdb84abb62af95e15a9fb
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8722.384, -2946.267, -81.711, 0, 0, 1.87, '{"col size y":1.34,"col pos x":-8477.194,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2946.267,"col size x":0.907,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- f792eb60867e4a0e12c910ccca09a09ce1bf61e7
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8725.089, -2957.625, -79.672, 0, 0, 1.86, '{"col size y":1.34,"col pos x":-8478.239,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2957.625,"col size x":0.907,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- c3ddf8b022794daa31d0829380a6fbacf760ad48
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8715.75, -2985.661, -76.867, 0, 0, 4.82, '{"col size y":1.34,"col pos x":-8492.865,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2985.661,"col size x":0.907,"col orientation":3.132,"col size z":4.146,"context":null,"url":null}')

-- 8cf7e4e62e532e666bdd80ad5a33a2da62b1a1b1
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8724.93, -2982.371, -77.177, 0, 0, -4.54, '{"col size y":0.19,"col pos x":8724.93,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2982.371,"col size x":-0.243,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- 69703995a1a80f92e5c60518888fddcff841497a
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8726.74, -2989.944, -77.023, 0, 0, -4.42, '{"col size y":0.18,"col pos x":8726.74,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2989.944,"col size x":-0.253,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- 4631b0740ee0c0f058bf3717b5f80008645d7070
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8717.377, -2992.726, -77.422, 0, 0, -1.37, '{"col size y":0.27,"col pos x":8717.377,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2992.726,"col size x":-0.163,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- c64896f38c9d9b9993920767a6383830b86a149e
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8734.746, -2964.85, -76.814, 0, 0, -3.05, '{"col size y":0.23,"col pos x":8734.746,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2964.85,"col size x":-0.203,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- 306e1fad1fd024a9f5123032b3099d4ebd7a0d55
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8753.987, -2967.831, -76.969, 0, 0, -5.51, '{"col size y":0.19,"col pos x":8753.987,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2967.831,"col size x":0.167,"col orientation":-2.282,"col size z":4.146,"context":null,"url":null}')

-- 0f2014b980846f42fba667c9b312f854437fa80d
SceneEditor:spawnShape("xmas_zo_lantern.shape", 8750.632, -2958.465, -76.734, 0, 0, -3.45, '{"col size y":0.23,"col pos x":8750.632,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2958.465,"col size x":0.137,"col orientation":0,"col size z":4.146,"context":null,"url":null}')

-- 17423e6578371feda2d933e0344617b9cbd56e7a
SceneEditor:spawnShape("xmas_sleigh.shape", 8759.242, -2978.86, -76.949, 0, 0, 2.09, '{"col size y":1.625,"col pos x":8759.242,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2978.86,"col size x":4.392,"col orientation":2.07,"col size z":3.004,"context":null,"url":null}')

-- 0bcd7b871b0c4f61083611cbf9aca8cfbe0a6b8d
SceneEditor:spawnShape("xmas_snowman.shape", 8746.609, -2949.958, -76.574, 0, 0, 2.09, '{"col size y":1.651,"col pos x":4045.757,"scale y":1.73,"scale z":1.73,"scale x":1.73,"col pos y":-2949.958,"col size x":2.832,"col orientation":0,"col size z":3.66,"context":null,"url":null}')

-- aca783703969975294c7bef46fcec5b777d97d21
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 8741.727, -2919.455, -84.251, 0, 0, 0, '{"col size y":8.525,"col pos x":4032.608,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2919.455,"col size x":8.581,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- 0329f919ac79152cd1f96b4471f0374f360d44b8
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 8705.39, -2964.287, -80.524, 0, 0, 0, '{"col size y":8.525,"col pos x":-8473.452,"scale y":1,"scale z":1,"scale x":1,"col pos y":-2964.287,"col size x":8.581,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- f8f69347eecc63300c8c21c7441ad2233e7e6aaf
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 8757.466, -2985.157, -77.045, 0, 0, 0, '{"col size y":8.525,"col pos x":-8404.438,"scale y":1.1,"scale z":1.1,"scale x":1.1,"col pos y":-2983.614,"col size x":8.581,"col orientation":0,"col size z":10.379,"context":null,"url":null}')

-- a4bb9dae01b3be2e7c9442482271173b80bcf270
SceneEditor:spawnShape("ge_mission_xmass_tree_without_star.shape", 8717.012, -2927.559, -84.779, 0, 0, 0, '{"col size y":8.525,"col pos x":-8444.758,"scale y":1.31,"scale z":1.31,"scale x":1.31,"col pos y":-2927.559,"col size x":8.581,"col orientation":0,"col size z":10.379,"context":null,"url":null}')


