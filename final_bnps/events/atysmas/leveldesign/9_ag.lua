-- GROUP: maro_xmas_deco
-- 199e8d62520cd1ea5304f02a683a8dedcc127f02
SceneEditor:spawnShape("Ge_mission_Gift.shape", 1035.266, -1325.167, 49.268, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":0.704,"scale y":1,"col pos y":10994.833,"col size z":0.943,"col size x":0.704,"scale x":1}')

-- 70a021eb3e683007386631e08b72165b300ca488
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 1034.073, -1324.59, 49.17, 0, 0, 0, '{"col pos x":-24320,"scale z":0.87,"col orientation":0,"col size y":0.704,"scale y":0.87,"col pos y":10995.41,"col size z":0.943,"col size x":0.704,"scale x":0.87}')

-- 78d8c83376994f071d5cf887bd12dd023f9f70aa
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 1033.904, -1325.56, 49.281, 0, 0, 0, '{"col pos x":-24320,"scale z":1.41,"col orientation":0,"col size y":0.704,"scale y":1.41,"col pos y":10994.44,"col size z":0.943,"col size x":0.704,"scale x":1.41}')

-- 7d67b48ddd3efbeace4c392b8e7fce3d4f6189f6
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 1036.379, -1325.712, 49.372, 0, 0, 0, '{"col pos x":-24320,"scale z":0.6,"col orientation":0,"col size y":0.704,"scale y":0.6,"col pos y":10994.288,"col size z":0.943,"col size x":0.704,"scale x":0.6}')

-- 97e97f3a92747b1477d27de364b2260dbe0a1d66
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 1035.58, -1323.883, 49.142, 0, 0, 0, '{"col pos x":-24320,"scale z":1.7,"col orientation":0,"col size y":0.704,"scale y":1.7,"col pos y":10996.117,"col size z":0.943,"col size x":0.704,"scale x":1.7}')

-- f1fc26d31688bcadf75353c5def815e0c91513b3
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 1056.98, -1306.31, 49.555, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":11013.69,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- 4e3f6ec1855906f49e70693aceabca8b20677981
SceneEditor:spawnShape("ge_mission_snowman.ps", 969.159, -1296.469, 52.723, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":11023.531,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 4643b950722803089294e7cdd55b6abd508a0664
SceneEditor:spawnShape("ge_mission_snowman.ps", 985.568, -1300.87, 51.04, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":11019.13,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 0096a418a057c69b06e47b1d328d301e0fd94682
SceneEditor:spawnShape("ge_mission_snowman.ps", 996.249, -1306.437, 50.652, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":11013.563,"col size z":3.66,"col size x":1.527,"scale x":1}')