-- GROUP: black__lams
-- d8eae75998b708760346751e3910c1ad00159a3b
SceneEditor:spawnShape("ge_feudecamp.ps", 11432.802, -4859.791, -28.101, 0, 0, 0, '{"col pos x":0,"col pos y":-4859.784,"scale y":1,"col size y":2.608,"col size x":2.491,"scale z":1,"col orientation":-0.294,"col size z":5.716,"scale x":1}')

-- a7c3097bb1026ff0fdcbd2079197affd89a88b73
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 11424.672, -4855.441, -27.362, 0, 0, 0, '{"col pos x":11424.64,"col pos y":-4855.487,"scale y":1,"col size y":-0.807,"col size x":0.901,"scale z":1,"col orientation":-3.031,"col size z":5.674,"scale x":1}')

-- 3e7a51a4aa4cefd5712cd78fe0ebd66275877fba
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 11412.129, -4858.244, -19.237, 0, 0, 0, '{"col pos x":11412.129,"col pos y":-4858.244,"scale y":1,"col size y":-0.917,"col size x":0.791,"scale z":1,"col orientation":-1.907,"col size z":5.674,"scale x":1}')

-- 171f9e7fe2cb7b90e84a59ed6f1310a1250dc889
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 11421.118, -4858.283, -20.287, 0, 0, 0, '{"col pos x":11421.118,"col pos y":-4858.283,"scale y":1,"col size y":0.89,"col size x":0.89,"scale z":1,"col orientation":0,"col size z":1,"scale x":1}')

-- 466c58eb49e220bb90be42caefd5309ac5282e2b
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 11416.798, -4840.292, -16.706, 0, 0, 0, '{"col pos x":11416.794,"col pos y":-4840.296,"scale y":1,"col size y":-0.757,"col size x":0.951,"scale z":1,"col orientation":0,"col size z":5.674,"scale x":1}')

-- 4583b7b510a20e4dea0daaec37a33539d480a662
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 11482.495, -4874.558, -26.738, 0, 0, 0, '{"col pos x":11482.236,"col pos y":-4873.285,"scale y":1,"col size y":2.837,"col size x":2.339,"scale z":1,"col orientation":0,"col size z":2.556,"scale x":1}')

-- deb004fa922f2bd99f24a7041b3bf69ccb475076
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 11479.925, -4875.931, -27.474, 0, 0, 0, '{"col pos x":11479.938,"col pos y":-4875.952,"scale y":1,"col size y":4.365,"col size x":3.835,"scale z":1,"col orientation":0,"col size z":3.046,"scale x":1}')

-- fd9d22566c38e1c118a058c7ed2ee50a6bf3f81a
SceneEditor:spawnShape("ge_mission_tente.shape", 11413.194, -4842.878, -16.793, 0, 0, -1.79, '{"col pos x":11413.062,"col pos y":-4842.957,"scale y":1,"col size y":5.775,"col size x":6.438,"scale z":1,"col orientation":-0.04,"col size z":5.034,"scale x":1}')

-- 56d31e667e135af36dd11a40777bdd6d89d7a3c9
SceneEditor:spawnShape("ge_mission_tente.shape", 11418.634, -4836.27, -16.184, 0, 0, -2.65, '{"col pos x":11418.634,"col pos y":-4836.27,"scale y":1,"col size y":5.775,"col size x":6.438,"scale z":1,"col orientation":-1.107,"col size z":5.034,"scale x":1}')

-- 1951f7b1e1703354fa9c7c929f613db049285704
SceneEditor:spawnShape("ge_mission_totem_oiseau.shape", 11462.495, -4852.235, -27.192, 0, 0, -2.81, '{"col pos x":11462.495,"col pos y":-4852.235,"scale y":1,"col size y":4.811,"col size x":6.934,"scale z":1,"col orientation":-1.627,"col size z":13.199,"scale x":1}')

-- 7271a14ad6444076810288db8fd798d6dcc27dc3
SceneEditor:spawnShape("ge_mission_tente.shape", 11427.241, -4851.998, -26.847, 0, 0, -2.73, '{"col pos x":11427.241,"col pos y":-4851.998,"scale y":1,"col size y":5.775,"col size x":6.438,"scale z":1,"col orientation":0.292,"col size z":5.034,"scale x":1}')

-- 182c009b369f2b97af4112f72cb52390d4a63cd6
SceneEditor:spawnShape("ge_mission_barriere.shape", 11419.667, -4858.733, -20.375, 0, -0.13, 0.89, '{"col pos x":11419.646,"col pos y":-4858.773,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0.87,"col size z":2.303,"scale x":1}')

-- 9020d866d9efd4674ef8fc680b1d36aeb862b79f
SceneEditor:spawnShape("ge_mission_barriere.shape", 11422.441, -4857.58, -20.096, 0, 0, 0, '{"col pos x":11422.423,"col pos y":-4857.594,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0,"col size z":2.303,"scale x":1}')

-- 63f2ee85518786e7a005c5b72a8034c3c10497f8
SceneEditor:spawnShape("ge_mission_barriere.shape", 11413.278, -4858.585, -19.659, 0, 0.34, -1.1, '{"col pos x":11413.278,"col pos y":-4858.585,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":2.02,"col size z":2.303,"scale x":1}')

-- 2ac1484e5ae1e2720b591f03b7e3206f7bfa41fb
SceneEditor:spawnShape("ge_mission_barriere.shape", 11410.68, -4857.436, -18.477, 0, 0.35, 0, '{"col pos x":11410.635,"col pos y":-4857.45,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-3.139,"col size z":2.303,"scale x":1}')

-- 93d9e0e2d02b93f0282fce566c88e347fa6ba9d1
SceneEditor:spawnShape("ge_mission_borne.shape", 11428.588, -4846.402, -17.791, 0, 0, 0.46, '{"col pos x":11428.588,"col pos y":-4846.402,"scale y":1,"col size y":0.3,"col size x":0.3,"scale z":1,"col orientation":-3.017,"col size z":0.821,"scale x":1}')

-- a967bdde6cc386aaa5f6653e9d36edc309942a98
SceneEditor:spawnShape("ge_mission_borne.shape", 11431.243, -4845.673, -17.97, 0, 0, 0.26, '{"col pos x":11431.243,"col pos y":-4845.673,"scale y":1,"col size y":0.3,"col size x":0.3,"scale z":1,"col orientation":0,"col size z":0.821,"scale x":1}')

-- b1a7bb8ba016274a8318229e37c87e17ba77cd55
SceneEditor:spawnShape("ge_mission_borne.shape", 11434.254, -4845.6, -18.406, 0, 0, 0, '{"col pos x":11434.254,"col pos y":-4845.6,"scale y":1,"col size y":0.3,"col size x":0.3,"scale z":1,"col orientation":0,"col size z":0.821,"scale x":1}')

-- 27c0c90c6dc7a9d283eac283e559204939d5255d
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 11411.457, -4860.216, -19.423, -0.37, 0, -2.34, '{"col pos x":11411.457,"col pos y":-4860.216,"scale y":0.769,"col size y":5.184,"col size x":2.685,"scale z":0.769,"col orientation":0.78,"col size z":2.147,"scale x":0.769}')

-- 3cc36fa231c30cda660ac44f6dcaaa31bcfe11b4
SceneEditor:spawnShape("ge_mission_objet_pack_4.shape", 11409.532, -4868.506, -19.954, 0, 0.29, 0, '{"col pos x":11409.532,"col pos y":-4868.506,"scale y":1,"col size y":4.345,"col size x":3.046,"scale z":1,"col orientation":-3.129,"col size z":2.048,"scale x":1}')

-- 3616b8437c888747c9e815712d3a5dd5ed219ef7
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 11412.013, -4873.982, -19.959, -0.31, 0.16, 0, '{"col pos x":11412.013,"col pos y":-4873.982,"scale y":1,"col size y":4.365,"col size x":3.835,"scale z":1,"col orientation":0.882,"col size z":3.046,"scale x":1}')


