-- GROUP: skelter_punks
-- 004aece8051884c27f130c72c7c94f5dc03aed24
SceneEditor:spawnShape("ge_feudecamp.ps", 9395.357, -2323.89, -78.834, 0, 0, 0, '{"scale x":1,"col pos x":0,"col size z":5.716,"col size x":2.491,"col pos y":-2323.89,"scale y":1,"col orientation":2.479,"col size y":2.608,"scale z":1}')

-- f8c8aa55637d4c44f4d37d3eb2b7e645d880f841
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9384.136, -2325.011, -79.044, 0, 0, 0, '{"scale x":0.833,"col pos x":9384.136,"col size z":1.192,"col size x":1.173,"col pos y":-2325.011,"scale y":0.833,"col orientation":-2.337,"col size y":1.078,"scale z":0.833}')

-- 1166a3549339e9bb37b835c4e52fd2af547ddb0a
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 9382.794, -2324.493, -78.991, 0, 0, 1.15, '{"scale x":1,"col pos x":9385.47,"col size z":2,"col size x":2.532,"col pos y":-2323.776,"scale y":1,"col orientation":-2.381,"col size y":1.299,"scale z":1}')

-- b1e467f6084e66ce6d84130332e2feba5c931b65
SceneEditor:spawnShape("ju_s3_bush_tree.shape", 9382.562, -2311.693, -79.503, 0, 0, 0, '{"scale x":1,"col pos x":9382.562,"col size z":4.612,"col size x":5.048,"col pos y":-2311.693,"scale y":1,"col orientation":0,"col size y":4.998,"scale z":1}')

-- de688b8ac7df7cd6fc6846b48e53757556319f03
SceneEditor:spawnShape("ju_s3_bamboo.shape", 9411.505, -2323.283, -78.111, 0, 0, 0, '{"scale x":1,"col pos x":9411.505,"col size z":6.611,"col size x":4.404,"col pos y":-2323.283,"scale y":1,"col orientation":-3.91,"col size y":4.612,"scale z":1}')

-- 7b1e4bdcb2fb09e1b480e8ea6fbc31674e91482a
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 9398.253, -2333.079, -77.872, 0, 0, 0, '{"scale x":1,"col pos x":9398.253,"col size z":3.102,"col size x":4.551,"col pos y":-2333.079,"scale y":1,"col orientation":2.035,"col size y":4.448,"scale z":1}')

-- e613f2c1da9a75dabf80798e7e49f6d5a1a64e56
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9380.763, -2335.363, -78.992, 0, 0, 0, '{"scale x":1,"col pos x":9380.763,"col size z":5.674,"col size x":0.871,"col pos y":-2335.363,"scale y":1,"col orientation":-1.058,"col size y":-0.837,"scale z":1}')

-- 2c628a2e6f0ad5f954e762217949acbd7592df2c
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9385.747, -2340.705, -79.11, 0, 0, 0, '{"scale x":1,"col pos x":9385.747,"col size z":5.674,"col size x":0.851,"col pos y":-2340.705,"scale y":1,"col orientation":-3.073,"col size y":-0.857,"scale z":1}')

-- 1732c2e8abbd754c848de3eaaf090830314bf737
SceneEditor:spawnShape("ge_mission_barriere.shape", 9381.924, -2333.804, -79.005, 0, 0, -0.78, '{"scale x":1,"col pos x":9381.924,"col size z":2.303,"col size x":3.424,"col pos y":-2333.804,"scale y":1,"col orientation":2.143,"col size y":0.188,"scale z":1}')

-- de488b328f625b85fbb3ca01228c10ef216c86aa
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 9386.994, -2339.715, -79.074, 0, 0, 0.61, '{"scale x":1,"col pos x":9387.631,"col size z":2.376,"col size x":5.481,"col pos y":-2340.734,"scale y":1,"col orientation":-5.672,"col size y":0.952,"scale z":1}')

-- 703a6d0f7e5cea37bdf1e81fae920a144f05af49
SceneEditor:spawnShape("ge_mission_barriere.shape", 9401.801, -2328.547, -77.973, 0, 0, 0.14, '{"scale x":1,"col pos x":9401.801,"col size z":2.303,"col size x":3.424,"col pos y":-2328.547,"scale y":1,"col orientation":3.27,"col size y":0.188,"scale z":1}')

-- 56bed7ae35cecb507a7717d3d7996c255dad05f4
SceneEditor:spawnShape("ge_mission_barriere.shape", 9397.938, -2329.774, -78.236, 0, 0, 0.34, '{"scale x":1,"col pos x":9397.938,"col size z":2.303,"col size x":3.424,"col pos y":-2329.774,"scale y":1,"col orientation":-2.808,"col size y":0.188,"scale z":1}')

-- de2e936bb2b7f717d9230133e4a5f894dbf40143
SceneEditor:spawnShape("ge_mission_barriere.shape", 9394.92, -2332.221, -78.395, 0, 0, 1.1, '{"scale x":1,"col pos x":9394.92,"col size z":2.303,"col size x":3.424,"col pos y":-2332.221,"scale y":1,"col orientation":1.05,"col size y":0.188,"scale z":1}')

-- 733c592dcb36a9299956f43e411e18066a79122d
SceneEditor:spawnShape("ge_mission_barriere.shape", 9409.807, -2322.828, -78.071, 0, 0, -1.81, '{"scale x":1,"col pos x":9409.807,"col size z":2.303,"col size x":3.424,"col pos y":-2322.828,"scale y":1,"col orientation":-5,"col size y":0.188,"scale z":1}')

-- c2937399ec471fb636d167bd24ff5af2cf94554f
SceneEditor:spawnShape("ge_mission_enclos.shape", 9407.86, -2328.4, -77.501, 0, 0, 0.71, '{"scale x":1,"col pos x":9407.86,"col size z":6.629,"col size x":8.401,"col pos y":-2328.4,"scale y":1,"col orientation":2.48,"col size y":7.043,"scale z":1}')

-- 78dc33032a1b6de8481f63c6234d2312f484b1b1
SceneEditor:spawnShape("ge_mission_charette.shape", 9386.317, -2315.688, -79.62, 0, 0, -1.56, '{"scale x":0.769,"col pos x":9386.317,"col size z":2.592,"col size x":1.565,"col pos y":-2315.688,"scale y":0.769,"col orientation":-1.57,"col size y":4.244,"scale z":0.769}')

-- 4e4ed1e813cab34e4bdb766a7f48cdb9e4cd0a72
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 9384.931, -2320.079, -79.331, 0, 0, 0, '{"scale x":1,"col pos x":9384.931,"col size z":3.046,"col size x":3.835,"col pos y":-2320.079,"scale y":1,"col orientation":-1.574,"col size y":3.495,"scale z":1}')

-- 243aa02feb139ab3e5a6290a80c33e29ff9e6c77
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9394.035, -2309.896, -79.308, 0, 0, 0, '{"scale x":1,"col pos x":9394.035,"col size z":5.674,"col size x":0.881,"col pos y":-2309.896,"scale y":1,"col orientation":-0.01,"col size y":-0.827,"scale z":1}')

-- 72cdf182e00c72a8366240e7c788dc00611503f3
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9386.139, -2311.092, -79.751, 0, 0, 0, '{"scale x":1,"col pos x":9386.139,"col size z":5.674,"col size x":0.831,"col pos y":-2311.092,"scale y":1,"col orientation":0,"col size y":-0.877,"scale z":1}')

-- 76e35bc8f66d256c5d709fb79fa94a66684ffe70
SceneEditor:spawnShape("ge_mission_barriere.shape", 9395.581, -2311.058, -79.507, 0, 0, 0, '{"scale x":1,"col pos x":9395.581,"col size z":2.303,"col size x":3.424,"col pos y":-2311.058,"scale y":1,"col orientation":0,"col size y":0.188,"scale z":1}')

-- 8aa48c2ef1e9dc02044dd9d702414fe0213e5ad3
SceneEditor:spawnShape("ge_mission_barriere.shape", 9382.51, -2313.312, -79.541, 0, 0, 0, '{"scale x":1,"col pos x":9382.51,"col size z":2.303,"col size x":3.424,"col pos y":-2313.312,"scale y":1,"col orientation":3.07,"col size y":0.188,"scale z":1}')

-- bf10aa797f9e985ff881d3eeca1dceb224fae4b1
SceneEditor:spawnShape("ge_mission_barriere.shape", 9386.191, -2312.843, -79.747, 0, 0, 0.31, '{"scale x":1,"col pos x":9386.191,"col size z":2.303,"col size x":3.424,"col pos y":-2312.843,"scale y":1,"col orientation":-2.82,"col size y":0.188,"scale z":1}')

-- 67e30b4b75c870f7fc01b8dff0269bba251f17dd
SceneEditor:spawnShape("ge_mission_tente.shape", 9403.561, -2313.156, -78.767, 0, 0, -3.81, '{"scale x":1,"col pos x":9403.561,"col size z":5.034,"col size x":6.438,"col pos y":-2313.156,"scale y":1,"col orientation":0,"col size y":5.775,"scale z":1}')


