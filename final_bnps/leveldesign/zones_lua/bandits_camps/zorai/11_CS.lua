-- GROUP: goo_grapplers
-- 4cb31cb1961c271b2997e31b1cd67ed57c927e22
SceneEditor:spawnShape("ge_mission_spot_goo.shape", 11217.388, -1749.13, -10.014, 0, 0, 0, '{"col size y":4.323,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":2167.004,"col size z":2.473,"col size x":4.251,"col pos y":-1749.13}')

-- 4a734fc9affcf42c9c643e42015c1d6104b20427
SceneEditor:spawnShape("ge_mission_barriere.shape", 11202.111, -1748.78, -10.036, 0, 0, 0.11, '{"col size y":0.188,"col orientation":-3.044,"scale x":1,"scale y":1,"scale z":1,"col pos x":11202.111,"col size z":2.303,"col size x":3.424,"col pos y":-1748.78}')

-- e16bec05d2fa56926e3a9e27d1ff103ceb0b1699
SceneEditor:spawnShape("ge_mission_barriere.shape", 11205.572, -1748.31, -9.967, 0, 0, 0.17, '{"col size y":0.188,"col orientation":0.172,"scale x":1,"scale y":1,"scale z":1,"col pos x":11205.572,"col size z":2.303,"col size x":3.424,"col pos y":-1748.31}')

-- 39334c9504ecf5f745352c80868fca3fe34469e1
SceneEditor:spawnShape("ge_mission_barriere.shape", 11209.021, -1747.617, -9.977, 0, 0, 0.23, '{"col size y":0.188,"col orientation":3.377,"scale x":1,"scale y":1,"scale z":1,"col pos x":11209.021,"col size z":2.303,"col size x":3.424,"col pos y":-1747.617}')

-- 084303601dfaf672a9b10201ef47b3328a812a48
SceneEditor:spawnShape("ge_mission_barriere.shape", 11212.484, -1746.759, -9.994, 0, 0, 0.26, '{"col size y":0.188,"col orientation":0.28,"scale x":1,"scale y":1,"scale z":1,"col pos x":11212.484,"col size z":2.303,"col size x":3.424,"col pos y":-1746.759}')

-- 0764e4ca54156175c2c936cea7d200c3d48a041f
SceneEditor:spawnShape("ge_mission_barriere.shape", 11216.017, -1746.022, -10.003, 0, 0, 0.13, '{"col size y":0.188,"col orientation":0.112,"scale x":1,"scale y":1,"scale z":1,"col pos x":11216.017,"col size z":2.303,"col size x":3.424,"col pos y":-1746.022}')

-- 4a62689f996649c594663141ec4a2d9a481bd1bc
SceneEditor:spawnShape("ge_mission_barriere.shape", 11219.658, -1745.718, -9.983, 0, 0, 0.02, '{"col size y":0.188,"col orientation":3.153,"scale x":1,"scale y":1,"scale z":1,"col pos x":11219.658,"col size z":2.303,"col size x":3.424,"col pos y":-1745.718}')

-- b8d61a6f8346da6a4eae1a28f32cbdcd3e29ada9
SceneEditor:spawnShape("ge_mission_barriere.shape", 11223.434, -1745.672, -9.999, 0, 0, 0, '{"col size y":0.188,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":11223.434,"col size z":2.303,"col size x":3.424,"col pos y":-1745.672}')

-- 46084eacb15ffe6fd11b5428f958a58150f9000c
SceneEditor:spawnShape("ge_mission_barriere.shape", 11227.108, -1745.666, -10.008, 0, 0, 0, '{"col size y":0.188,"col orientation":-3.177,"scale x":1,"scale y":1,"scale z":1,"col pos x":11227.108,"col size z":2.303,"col size x":3.424,"col pos y":-1745.666}')

-- 0e2cf45411905b1bf2d955535162fad9c322c1fd
SceneEditor:spawnShape("ge_mission_barriere.shape", 11230.655, -1746.019, -9.966, 0, 0, -0.19, '{"col size y":0.188,"col orientation":-0.198,"scale x":1,"scale y":1,"scale z":1,"col pos x":11230.655,"col size z":2.303,"col size x":3.424,"col pos y":-1746.019}')


