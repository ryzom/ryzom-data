-- GROUP: homin_hounders
-- 3ee9e5e76888f2266db48a9099387bd96c439d12
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9193.992, -2817.247, -77.23, 0, 0, 0, '{"col pos x":0,"col pos y":-2817.247,"scale y":1,"col size y":1.453,"col size x":3.161,"scale z":1,"col orientation":0,"col size z":5.674,"scale x":1}')

-- 828091fee1aeb82b3159b4b3c598d5af841d2b2b
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9213.353, -2810.685, -76.994, 0, 0, 0, '{"col pos x":9213.353,"col pos y":-2810.685,"scale y":1,"col size y":-0.927,"col size x":0.781,"scale z":1,"col orientation":0,"col size z":5.674,"scale x":1}')

-- fe386f011c552d26e6ea363a45db4972cde6b915
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9214.666, -2828.389, -76.965, 0, 0, 0, '{"col pos x":9214.666,"col pos y":-2828.389,"scale y":1,"col size y":-0.927,"col size x":0.781,"scale z":1,"col orientation":0,"col size z":5.674,"scale x":1}')

-- e472246efc450fac63360e7eb3342a5fefdd55c5
SceneEditor:spawnShape("ge_feudecamp.ps", 9222.705, -2822.873, -76.986, 0, 0, 0, '{"col pos x":0,"col pos y":-2822.873,"scale y":1,"col size y":2.608,"col size x":2.491,"scale z":1,"col orientation":0,"col size z":5.716,"scale x":1}')

-- 934a79ef051ec76f311c648a7828a23b2128c7c3
SceneEditor:spawnShape("ju_s3_bamboo.shape", 9234.613, -2824.482, -77.013, 0, 0, 0, '{"col pos x":0,"col pos y":-2824.482,"scale y":1,"col size y":4.612,"col size x":4.404,"scale z":1,"col orientation":-1.12,"col size z":6.611,"scale x":1}')

-- 1515655a46ab02dfb70461105f91ac25e8a4223f
SceneEditor:spawnShape("ju_s3_bamboo.shape", 9211.764, -2809.258, -76.982, 0, 0, 0, '{"col pos x":0,"col pos y":-2809.258,"scale y":1,"col size y":4.612,"col size x":4.404,"scale z":1,"col orientation":0,"col size z":6.611,"scale x":1}')

-- 4bc70644d5674666b11fc0b498ec9c1896e3bedc
SceneEditor:spawnShape("ge_mission_charogneinsect.shape", 9188.412, -2818.962, -77.37, 0, 0, 0, '{"col pos x":9188.412,"col pos y":-2818.962,"scale y":1,"col size y":3.571,"col size x":6.304,"scale z":1,"col orientation":2.338,"col size z":2.189,"scale x":1}')

-- 24f0e260dc017419998646d54dae53388ddab846
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 9201.422, -2810.924, -77.08, 0, 0, 0, '{"col pos x":9201.422,"col pos y":-2810.924,"scale y":0.769,"col size y":5.184,"col size x":2.685,"scale z":0.769,"col orientation":0.2,"col size z":2.147,"scale x":0.769}')

-- 2b52704ca8d146a28156a4d97bb5517ea3e731ca
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9231.749, -2827.64, -76.973, 0, 0, 0, '{"col pos x":9231.749,"col pos y":-2827.64,"scale y":0.833,"col size y":1.078,"col size x":1.173,"scale z":0.833,"col orientation":0.235,"col size z":1.192,"scale x":0.833}')

-- c7f039d2ab674da083a0d8e189eabf2a3fa378a3
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9232.391, -2827.295, -76.97, 0, 0, 0, '{"col pos x":9232.391,"col pos y":-2827.295,"scale y":0.833,"col size y":1.078,"col size x":1.173,"scale z":0.833,"col orientation":0.214,"col size z":1.192,"scale x":0.833}')

-- 7e3ae7a7a08e968feb3de48b3738eeeed0b5e656
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 9229.4, -2828.057, -76.983, 0, 0, 0, '{"col pos x":-2177.55,"col pos y":-2828.057,"scale y":1,"col size y":1.299,"col size x":2.532,"scale z":1,"col orientation":0,"col size z":2,"scale x":1}')

-- 8ac34241719b64ee3f99fa57af5b87417184422d
SceneEditor:spawnShape("ge_mission_barriere.shape", 9229.877, -2819.69, -76.992, 0, 0, -1.02, '{"col pos x":9229.877,"col pos y":-2819.69,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.062,"col size z":2.303,"scale x":1}')

-- 9b022c02adc7344fc76c6ac7aec049fafef0bd30
SceneEditor:spawnShape("ge_mission_barriere.shape", 9195.144, -2820.689, -77.202, 0, 0, -0.89, '{"col pos x":9195.144,"col pos y":-2820.689,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1,"col size z":2.303,"scale x":1}')

-- 278e6e0e3380e600e612b423dc5d95f4dd955efb
SceneEditor:spawnShape("ge_mission_barriere.shape", 9203.076, -2826.243, -76.998, 0, 0, -0.25, '{"col pos x":9203.076,"col pos y":-2826.243,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0,"col size z":2.303,"scale x":1}')

-- 6e8def957a0fd6ba70fba3dc9f699acf44a99fa2
SceneEditor:spawnShape("ge_mission_barriere.shape", 9192.834, -2818.033, -77.266, 0, 0, -0.81, '{"col pos x":9192.834,"col pos y":-2818.033,"scale y":1,"col size y":0.24,"col size x":3.38,"scale z":1,"col orientation":-0.81,"col size z":1,"scale x":1}')

-- 174a1fa156bb045bf2f3b99582ab84e2d217f4b7
SceneEditor:spawnShape("ge_mission_barriere.shape", 9190.643, -2815.31, -77.442, 0, 0, -0.96, '{"col pos x":9190.643,"col pos y":-2815.31,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.99,"col size z":2.303,"scale x":1}')

-- e459c6050b6b346b126186486e9a0f128ce17777
SceneEditor:spawnShape("ge_mission_barriere.shape", 9188.907, -2812.065, -77.604, 0, 0, -1.16, '{"col pos x":9188.907,"col pos y":-2812.065,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.17,"col size z":2.303,"scale x":1}')

-- 9e6e860182d14ea7eb7b3e26c63c18a094ae6723
SceneEditor:spawnShape("ge_mission_barriere.shape", 9206.545, -2827.072, -76.993, 0, 0, -0.25, '{"col pos x":9206.545,"col pos y":-2827.072,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0,"col size z":2.303,"scale x":1}')

-- fa20263273a167d1edc40ca5ebdd920d26c34816
SceneEditor:spawnShape("ge_mission_barriere.shape", 9209.929, -2827.93, -77.009, 0, 0, -3.39, '{"col pos x":9209.929,"col pos y":-2827.93,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0,"col size z":2.303,"scale x":1}')

-- 98f43269db84841ba592bf717bf45e8262bebb60
SceneEditor:spawnShape("ge_mission_barriere.shape", 9231.618, -2822.777, -76.973, 0, 0, -1.08, '{"col pos x":9231.618,"col pos y":-2822.777,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.08,"col size z":2.303,"scale x":1}')

-- c40424d641ddfdc3d1ea0ea7d262b2d799c1e6d1
SceneEditor:spawnShape("ge_mission_barriere.shape", 9232.959, -2826.016, -76.954, 0, 0, -1.26, '{"col pos x":9232.959,"col pos y":-2826.016,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.29,"col size z":2.303,"scale x":1}')

-- 3fc61571f1e9e46f851688ad6293c42dc7c823c4
SceneEditor:spawnShape("ge_mission_barriere.shape", 9222.771, -2810.466, -76.989, 0, 0, -0.51, '{"col pos x":9222.771,"col pos y":-2810.466,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.52,"col size z":2.303,"scale x":1}')

-- 9b0521a756c43f59759e9e84efff30b13fe398a4
SceneEditor:spawnShape("ge_mission_barriere.shape", 9219.912, -2808.5, -76.939, 0, 0, -0.77, '{"col pos x":9219.912,"col pos y":-2808.5,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.76,"col size z":2.303,"scale x":1}')

-- 700637bb40a8c0a07e36300d1e12eae7095d4347
SceneEditor:spawnShape("ge_mission_barriere.shape", 9217.374, -2805.965, -77.014, 0, 0, -0.79, '{"col pos x":9217.374,"col pos y":-2805.965,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.79,"col size z":2.303,"scale x":1}')

-- dd791c2f9482e32c1f6c8e6d912baf94b07aafad
SceneEditor:spawnShape("ge_mission_hut.shape", 9195.163, -2812.486, -77.239, 0, 0, -2.4, '{"col pos x":9195.163,"col pos y":-2812.486,"scale y":1,"col size y":9.64,"col size x":8.037,"scale z":1,"col orientation":-2.559,"col size z":4.669,"scale x":1}')

-- 9f7af577fe5f3c87fd4b9a95cb9434a8d2706580
SceneEditor:spawnShape("ge_mission_totem_kitin.shape", 9220.178, -2795.372, -78.177, 0, 0, -1.72, '{"col pos x":9220.178,"col pos y":-2795.372,"scale y":1,"col size y":4.304,"col size x":4.882,"scale z":1,"col orientation":0,"col size z":13.169,"scale x":1}')

-- 68cdced9b10d61c982ce8785d72ef6c887958ca4
SceneEditor:spawnShape("ge_mission_stand.shape", 9215.783, -2808.354, -77.003, 0, 0, 0.61, '{"col pos x":9215.783,"col pos y":-2808.354,"scale y":0.938,"col size y":4.775,"col size x":4.258,"scale z":0.644,"col orientation":3.811,"col size z":6.313,"scale x":1.088}')

-- 2b9816d047ff1970b8ad8d6dc3601eadf4900558
SceneEditor:spawnShape("ge_mission_tente.shape", 9225.161, -2827.83, -76.963, 0, 0, 0.39, '{"col pos x":9225.161,"col pos y":-2827.83,"scale y":1,"col size y":5.775,"col size x":6.438,"scale z":1,"col orientation":0,"col size z":5.034,"scale x":1}')

-- 94c43e8a026dcb853815556b655e71a136605f77
SceneEditor:spawnShape("ge_mission_tente.shape", 9218.621, -2828.338, -77.004, 0, 0, -0.3, '{"col pos x":9218.621,"col pos y":-2828.338,"scale y":1,"col size y":5.775,"col size x":6.438,"scale z":1,"col orientation":0,"col size z":5.034,"scale x":1}')


