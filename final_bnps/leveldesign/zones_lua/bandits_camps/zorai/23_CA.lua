-- GROUP: browbeat_brothers
-- 9a7ee50b79f3dddcd4862828500fa1b64adfae48
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 8466.002, -3673.507, -77.017, 0, 0, 0, '{"scale y":1,"col size x":1,"col pos y":-3673.507,"col orientation":0,"col size z":1,"scale x":1,"col size y":1,"col pos x":8466.002,"scale z":1}')

-- 96956e079212cf63c58a43e587832d08185e0a29
SceneEditor:spawnShape("ju_s3_plantegrasse.shape", 8468.18, -3671.329, -77.008, 0, 0, 0, '{"scale y":1,"col size x":4.765,"col pos y":-3671.329,"col orientation":0,"col size z":3.99,"scale x":1,"col size y":4.346,"col pos x":0,"scale z":1}')

-- 8f7f2df770ae9424daf96106f7b3aa4d84331312
SceneEditor:spawnShape("ge_mission_sac_a.shape", 8478.813, -3669.978, -76.997, 0, 0, 0, '{"scale y":1,"col size x":0.967,"col pos y":-3669.978,"col orientation":0,"col size z":0.404,"scale x":1,"col size y":1.165,"col pos x":8478.813,"scale z":1}')

-- 1cece43b87be54a3c783924665366b2743df4049
SceneEditor:spawnShape("ge_mission_sac_b.shape", 8478.751, -3668.913, -76.98, 0, 0, 0, '{"scale y":1,"col size x":1.7,"col pos y":-3668.913,"col orientation":-1.718,"col size z":0.968,"scale x":1,"col size y":1.62,"col pos x":8478.751,"scale z":1}')

-- 279dc7fa180a84f34a22dec9c62573a69fc0eae7
SceneEditor:spawnShape("ge_mission_barriere.shape", 8474.612, -3667.239, -77.001, 0, 0, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3667.239,"col orientation":0,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":8474.612,"scale z":1}')

-- cff821e2144d6836bbc0b2405f24e8c4fc2b5777
SceneEditor:spawnShape("ge_mission_barriere.shape", 8471.521, -3668.408, -76.981, 0, 0, -2.36, '{"scale y":1,"col size x":3.424,"col pos y":-3668.408,"col orientation":-2.33,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":8471.521,"scale z":1}')

-- e3e47d978505faa28683a68891b3cd9e2975697c
SceneEditor:spawnShape("ge_mission_barriere.shape", 8469.962, -3671.38, -76.962, 0, 0, -1.76, '{"scale y":1,"col size x":3.424,"col pos y":-3671.38,"col orientation":-1.74,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":8469.962,"scale z":1}')

-- faf1e141a32428312ee5e6d47b04cf6177c92755
SceneEditor:spawnShape("ge_mission_barriere.shape", 8468.062, -3673.366, -77.012, 0, 0, 0.15, '{"scale y":1,"col size x":3.424,"col pos y":-3673.366,"col orientation":0.2,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":8468.062,"scale z":1}')

-- cc405961f3bd551de9563538f81577084ed02d53
SceneEditor:spawnShape("ge_mission_barriere.shape", 8478.153, -3667.253, -76.965, 0, 0, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3667.253,"col orientation":0,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":8478.153,"scale z":1}')


