-- GROUP: zoras_thorn
-- dcdc2c3c08fe23aec4d703e8adf79ba013953d0f
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 8161.739, -3136.81, -77.043, 0, 0, 0, '{"col pos x":8161.739,"col pos y":-3136.81,"scale y":1,"col size y":2.837,"col size x":2.339,"scale z":1,"col orientation":0,"col size z":2.556,"scale x":1}')

-- f47b59e3f75583e40826954a8bc9d7b7bc73ec97
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 8160.148, -3140.717, -77.005, 0, 0, 2.24, '{"col pos x":8160.469,"col pos y":-3139.99,"scale y":0.769,"col size y":3.504,"col size x":2.685,"scale z":0.769,"col orientation":0,"col size z":2.147,"scale x":0.769}')

-- b033364a60674bc2149ee1443609132dfda8c909
SceneEditor:spawnShape("ge_mission_tente.shape", 8160.304, -3129.894, -77.021, 0, 0, 1.45, '{"col pos x":8160.304,"col pos y":-3129.894,"scale y":1,"col size y":4.62,"col size x":4.62,"scale z":1,"col orientation":3.14,"col size z":1,"scale x":1}')

-- 7d97f1403b1863f33f527e08423c3b52efd9b80e
SceneEditor:spawnShape("ge_mission_barriere.shape", 8163.553, -3133.763, -77.059, 0, 0, -1.52, '{"col pos x":8163.553,"col pos y":-3133.763,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":1.67,"col size z":2.303,"scale x":1}')

-- cdc184e505099c44b1d814761b4f24e137d3eaec
SceneEditor:spawnShape("ge_mission_barriere.shape", 8163.336, -3130.25, -77.049, 0, 0, -1.47, '{"col pos x":8163.336,"col pos y":-3130.25,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.47,"col size z":2.303,"scale x":1}')

-- 60a8e0aadf2b89365d1cbe7d80b2f9079f4ff25d
SceneEditor:spawnShape("ge_mission_barriere.shape", 8163.557, -3137.202, -77.112, 0, 0, -1.58, '{"col pos x":8163.557,"col pos y":-3137.202,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":1.59,"col size z":2.303,"scale x":1}')

-- ef6a9dd79f13d39e168e3485b0ee63990b265814
SceneEditor:spawnShape("ge_mission_barriere.shape", 8163.47, -3140.738, -77.138, 0, 0, -1.64, '{"col pos x":8163.47,"col pos y":-3140.738,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.65,"col size z":2.303,"scale x":1}')

-- 8c63e5c09ef7099dc073da56f54dc2fea2bb442c
SceneEditor:spawnShape("ge_mission_barriere.shape", 8162.943, -3144.198, -77.124, 0, 0, -1.8, '{"col pos x":8162.943,"col pos y":-3144.198,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.76,"col size z":2.303,"scale x":1}')

-- 4ec0540a205f5bde414a9afafe190875076abd54
SceneEditor:spawnShape("ge_mission_barriere.shape", 8162.184, -3147.718, -77.034, 0, 0, -1.78, '{"col pos x":8162.184,"col pos y":-3147.718,"scale y":1,"col size y":0.27,"col size x":3.37,"scale z":1,"col orientation":-1.78,"col size z":1,"scale x":1}')

-- ec77cf6016785991e4f41e659d47f9a300fa5684
SceneEditor:spawnShape("ge_mission_barriere.shape", 8161.729, -3127.533, -77.057, 0, 0, -0.52, '{"col pos x":8161.729,"col pos y":-3127.533,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":2.71,"col size z":2.303,"scale x":1}')


