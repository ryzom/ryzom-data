-- GROUP: plain_scourers
-- 25f502e9143dab2ececce82b0faa41bccfcff6bc
SceneEditor:spawnShape("ge_mission_tourgarde.shape", 11696.639, -3202.053, -77.027, 0, 0, 3.46, '{"col size y":12.671,"col orientation":-0.28,"scale x":1,"scale y":1,"scale z":1,"col pos x":11696.639,"col size z":15.808,"col size x":10.033,"col pos y":-3202.053}')

-- 6ec930de7246d8e623ee89bc268abc33f0052322
SceneEditor:spawnShape("ge_mission_sac_a.shape", 11690.622, -3207.764, -77.078, 0, 0, 0, '{"col size y":1.165,"col orientation":2.36,"scale x":1,"scale y":1,"scale z":1,"col pos x":11690.622,"col size z":0.404,"col size x":0.967,"col pos y":-3207.764}')

-- 1358eaa33a673b1e18f34163123667f52a2ca33b
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 11691.705, -3206.961, -77.006, 0, 0, 0, '{"col size y":1.078,"col orientation":0,"scale x":0.833,"scale y":0.833,"scale z":0.833,"col pos x":11691.705,"col size z":1.192,"col size x":1.173,"col pos y":-3206.961}')

-- 82da1abbbdfa200f975c4c082f12c68d77c3005b
SceneEditor:spawnShape("ge_mission_barriere.shape", 11693.697, -3208.254, -77.059, 0, 0, -2.56, '{"col size y":0.188,"col orientation":0.57,"scale x":1,"scale y":1,"scale z":1,"col pos x":11693.697,"col size z":2.303,"col size x":3.424,"col pos y":-3208.254}')

-- b71ceb9a10d44c8c22810635b82eeca2860b8a79
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 11686.037, -3211.846, -77.046, 0, 0, -1.26, '{"col size y":0.965,"col orientation":-1.25,"scale x":1,"scale y":1,"scale z":1,"col pos x":11686.037,"col size z":1.76,"col size x":1.494,"col pos y":-3211.846}')

-- f22626fe95348c1f185b6ba118a93424c99220bb
SceneEditor:spawnShape("ge_mission_barriere.shape", 11691.098, -3210.474, -77.138, 0, 0, -2.27, '{"col size y":0.188,"col orientation":0.88,"scale x":1,"scale y":1,"scale z":1,"col pos x":11691.098,"col size z":2.303,"col size x":3.424,"col pos y":-3210.474}')

-- 46962244d15577f23c28a878411bdb2738d6951d
SceneEditor:spawnShape("ge_mission_barriere.shape", 11680.979, -3209.848, -77.042, 0, 0, -0.32, '{"col size y":0.188,"col orientation":-3.472,"scale x":1,"scale y":1,"scale z":1,"col pos x":11680.979,"col size z":2.303,"col size x":3.424,"col pos y":-3209.848}')

-- 963fd2e7562c03fd4fe74722c73ee9a5d0a7dd50
SceneEditor:spawnShape("ge_mission_barriere.shape", 11684.537, -3210.325, -77.024, 0, 0, 0, '{"col size y":0.188,"col orientation":0.014,"scale x":1,"scale y":1,"scale z":1,"col pos x":11684.537,"col size z":2.303,"col size x":3.424,"col pos y":-3210.325}')

-- 14aee072e2f40cf354bcf47b98b402f35b7796e8
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 11686.88, -3211.789, -77.07, 0, 0, 0.37, '{"col size y":2.832,"col orientation":0.41,"scale x":1,"scale y":1,"scale z":1,"col pos x":11687.235,"col size z":2.376,"col size x":5.411,"col pos y":-3211.92}')


