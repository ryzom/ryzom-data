-- GROUP: birlds_of_omen
-- a2a8d575acfa7a6c0daf5d4de7f01d47a142cba9
SceneEditor:spawnShape("ge_mission_objet_pack_4.shape", 10048.383, -3667.622, -76.55, 0, 0, 2.51, '{"col orientation":2.51,"col size y":4.345,"col size x":3.046,"col size z":2.048,"col pos y":-3667.622,"scale y":1,"scale x":1,"col pos x":10048.383,"scale z":1,"context":null,"url":null}')

-- 3236052cc1cfdc71b57d467342080f5867d961ae
SceneEditor:spawnShape("gen_bt_drapeau_revendication.shape", 10052.003, -3665.474, -75.666, 0, 0, 0, '{"col orientation":0.22,"col size y":-2.391,"col size x":-1.222,"col size z":2.36,"col pos y":-3665.249,"scale y":1,"scale x":1,"col pos x":10051.885,"scale z":1,"context":null,"url":null}')

-- e2e66bd9581a321f2963b7182480a97555f88c08
SceneEditor:spawnShape("zo_wea_masse2m.shape", 10051.752, -3665.738, -75.745, 1.37, 0, 0, '{"col orientation":0,"col size y":1.286,"col size x":0.079,"col size z":0.083,"col pos y":-3665.859,"scale y":1.4,"scale x":1.4,"col pos x":10051.812,"scale z":1.4,"context":null,"url":null}')

-- d7f4d8ba62fea405b12dd0751692d53119b20314
SceneEditor:spawnShape("zo_wea_masse2m.shape", 10052.315, -3665.733, -75.65, -4.9, -2.55, 0.51, '{"col orientation":0,"col size y":1.286,"col size x":0.079,"col size z":0.083,"col pos y":-3665.686,"scale y":1.4,"scale x":1.4,"col pos x":10052.163,"scale z":1.4,"context":null,"url":null}')

-- b6b2160c577121f173a4e618870b1010a2eae35a
SceneEditor:spawnShape("ge_mission_reverbere.shape", 10060.315, -3671.045, -76.519, 0, 0, 0, '{"col orientation":0,"col size y":0.545,"col size x":0.577,"col size z":3.679,"col pos y":-3670.946,"scale y":1,"scale x":1,"col pos x":10060.37,"scale z":1,"context":null,"url":null}')

-- d696fc5a1d10795a08fd562499e9b099acd01f10
SceneEditor:spawnShape("ge_mission_reverbere.shape", 10041.842, -3666.639, -76.261, 0, 0, 0, '{"col orientation":0,"col size y":0.545,"col size x":0.577,"col size z":3.679,"col pos y":-3666.549,"scale y":1,"scale x":1,"col pos x":10041.812,"scale z":1,"context":null,"url":null}')

-- f78befcd119f6481ed01d6454d24a80322ce81c1
SceneEditor:spawnShape("ge_mission_reverbere.shape", 10037.484, -3668.317, -75.92, 0, 0, 0, '{"col orientation":1.445,"col size y":0.545,"col size x":0.577,"col size z":3.679,"col pos y":-3668.31,"scale y":1,"scale x":1,"col pos x":10037.467,"scale z":1,"context":null,"url":null}')

-- 23cba5fecc869bbb5acf3ee45b73803319edd90e
SceneEditor:spawnShape("ge_mission_reverbere.shape", 10058.131, -3667.041, -76.03, 0, 0, 0, '{"col orientation":0,"col size y":0.545,"col size x":0.577,"col size z":3.679,"col pos y":-3667.087,"scale y":1,"scale x":1,"col pos x":10058.099,"scale z":1,"context":null,"url":null}')

-- 171a65b198e24e1664275dd93842b95b658986de
SceneEditor:spawnShape("ge_mission_barriere.shape", 10054.204, -3664.923, -75.994, 0, 0, -0.61, '{"col orientation":-0.58,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3664.923,"scale y":1,"scale x":1,"col pos x":10054.204,"scale z":1,"context":null,"url":null}')

-- e77a263b9cb95be68e60ed854af4125e201a2441
SceneEditor:spawnShape("ge_mission_barriere.shape", 10057.03, -3666.89, -76.059, 0, 0, -0.64, '{"col orientation":-0.64,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3666.89,"scale y":1,"scale x":1,"col pos x":10057.03,"scale z":1,"context":null,"url":null}')

-- 1b0dbb11677aa48d7c0440380fabbe0744143974
SceneEditor:spawnShape("ge_mission_barriere.shape", 10059.898, -3672.111, -76.549, 0, 0, -1.53, '{"col orientation":-1.52,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3672.116,"scale y":1,"scale x":1,"col pos x":10059.895,"scale z":1,"context":null,"url":null}')

-- 6675fd33eca1387b2c783bc8e41a771b740a9ce5
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 10050.23, -3665.875, -76.079, 0, 0, -2.93, '{"col orientation":0.22,"col size y":0.342,"col size x":5.331,"col size z":2.376,"col pos y":-3664.602,"scale y":1,"scale x":1,"col pos x":10049.972,"scale z":1,"context":null,"url":null}')

-- 27870626f49d86904d293f2c818d1adcd9482058
SceneEditor:spawnShape("ge_mission_barriere.shape", 10046.032, -3666.042, -76.334, 0, 0, -2.49, '{"col orientation":0.72,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3666.063,"scale y":1,"scale x":1,"col pos x":10046.046,"scale z":1,"context":null,"url":null}')

-- 67b9d390aa5ba51dd2cc2990bb8b6d0fc606bcd1
SceneEditor:spawnShape("ge_mission_barriere.shape", 10042.937, -3667.085, -76.341, 0, 0, 0, '{"col orientation":0,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3667.164,"scale y":1,"scale x":1,"col pos x":10042.804,"scale z":1,"context":null,"url":null}')

-- c897156d8b3b5495d4af9721f53f71502d77c791
SceneEditor:spawnShape("ge_mission_barriere.shape", 10037.043, -3672.725, -76.043, 0, 0, -1.27, '{"col orientation":-1.27,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3672.725,"scale y":1,"scale x":1,"col pos x":10037.043,"scale z":1,"context":null,"url":null}')

-- c9afa031786797936863cf8c75d0de6d477989a6
SceneEditor:spawnShape("ge_mission_barriere.shape", 10037.447, -3669.525, -76.045, 0, 0, -2.08, '{"col orientation":-2.06,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3669.525,"scale y":1,"scale x":1,"col pos x":10037.447,"scale z":1,"context":null,"url":null}')

-- ec2d722f9c348d3d373d946f254f66d0b1c706e8
SceneEditor:spawnShape("ge_mission_barriere.shape", 10037.148, -3676.119, -76.024, 0, 0, -1.81, '{"col orientation":-1.805,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3676.119,"scale y":1,"scale x":1,"col pos x":10037.148,"scale z":1,"context":null,"url":null}')

-- 28b66aa7e88cb783b23f0cd06487e94c06ef6c6f
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10045.9, -3676.972, -76.941, 0, 0, 0, '{"col orientation":-1,"col size y":1.054,"col size x":1.054,"col size z":1.241,"col pos y":-3677.012,"scale y":0.7,"scale x":0.7,"col pos x":10045.879,"scale z":0.7,"context":null,"url":null}')

-- bbb29616f9e2f40f17dca20c17bdf128768a15ca
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10049.766, -3675.448, -76.844, 0, 0, 0, '{"col orientation":0,"col size y":1.054,"col size x":1.054,"col size z":1.241,"col pos y":-3675.462,"scale y":0.7,"scale x":0.7,"col pos x":10049.747,"scale z":0.7,"context":null,"url":null}')

-- 4e113b847fcf6e46d3c63730468209473e235b75
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10045.442, -3674.75, -76.832, 0, 0, 0, '{"col orientation":-0.44,"col size y":1.054,"col size x":1.054,"col size z":1.241,"col pos y":-3674.75,"scale y":0.7,"scale x":0.7,"col pos x":10045.442,"scale z":0.7,"context":null,"url":null}')

-- c4e107c6640d7b246a97e6122ec5a366ee690383
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10048.344, -3677.613, -76.952, 0, 0, 0, '{"col orientation":-1.302,"col size y":1.054,"col size x":1.054,"col size z":1.241,"col pos y":-3677.628,"scale y":0.7,"scale x":0.7,"col pos x":10048.299,"scale z":0.7,"context":null,"url":null}')

-- d5e04cde6fe2111afddf5638d305a9461d888467
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10047.873, -3673.513, -76.89, 0, 0, 0, '{"col orientation":-1.71,"col size y":1.054,"col size x":1.054,"col size z":1.241,"col pos y":-3673.513,"scale y":0.7,"scale x":0.7,"col pos x":10047.873,"scale z":0.7,"context":null,"url":null}')

-- 6540ec7b59465c755afdee4c4eb7bb25509e3b45
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10047.587, -3675.663, -78.383, 0, 0, 0, '{"col orientation":0,"col size y":2.144,"col size x":2.144,"col size z":1.241,"col pos y":-3675.642,"scale y":2.32,"scale x":2.12,"col pos x":10047.437,"scale z":2.121,"context":null,"url":null}')

-- 18eaa0f1aa252ce4c8fa48d41f809c540660b2f9
SceneEditor:spawnShape("ge_mission_comptoir.shape", 10047.566, -3676.297, -76.915, 0, 0, -3.14, '{"col orientation":0,"col size y":1.454,"col size x":3.059,"col size z":1.412,"col pos y":-3676.297,"scale y":1,"scale x":1,"col pos x":0,"scale z":1,"context":null,"url":null}')

-- 0621797094b3eed01a0ae982baada104aec54702
SceneEditor:spawnShape("ge_mission_comptoir.shape", 10047.575, -3674.982, -76.918, 0, 0, 0, '{"col orientation":0.135,"col size y":1.454,"col size x":3.059,"col size z":1.412,"col pos y":-3674.982,"scale y":1,"scale x":1,"col pos x":0,"scale z":1,"context":null,"url":null}')


