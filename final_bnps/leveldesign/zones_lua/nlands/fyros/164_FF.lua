-- GROUP: fyros_npc
-- 7298bd6132d4f6f091585fea7d221ea37d3ae71a
SceneEditor:spawnShape("ma_land_telepod.ps", 21635.98, -26173.449, 76.086, 0, 0, 1.61, '{"scale x":0.51,"scale z":0.51,"col pos x":-10.299,"col size y":24,"col size x":24,"scale y":0.51,"col orientation":0,"col size z":24,"col pos y":-26173.449,"context":null,"url":null}')

-- f3bd9b973712b96f03400f229c6a60fe680938ca
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 21637.115, -26169.852, 75.676, 0, 0, 0, '{"scale x":1,"scale z":1,"col pos x":21637.115,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":0,"col size z":3.89,"col pos y":-26169.852,"context":null,"url":null}')

-- b83788c6c14f95046e506d9bcdc425602f78070d
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 21634.576, -26173.408, 76.015, 0, 0, 1.55, '{"scale x":1,"scale z":1,"col pos x":21634.576,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":1.55,"col size z":3.89,"col pos y":-26173.408,"context":null,"url":null}')

-- 899a5ef0fe1e8cef8b6f62447baf8c839991b955
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 21636.904, -26176.982, 76.232, 0, 0, 0, '{"scale x":1,"scale z":1,"col pos x":21636.904,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":0,"col size z":3.89,"col pos y":-26176.982,"context":null,"url":null}')
