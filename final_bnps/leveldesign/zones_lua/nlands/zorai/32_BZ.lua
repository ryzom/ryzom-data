-- GROUP: zorai
-- ed3671eb1e4db477b9611400e6f1659e3769ff06
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 8225.188, -5017.873, -13.407, 0, 0, 1.66, '{"scale x":1,"scale z":1,"col pos x":8225.188,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":1.66,"col size z":3.89,"col pos y":-5017.873,"context":null,"url":null}')

-- 39fd9e00222b9df8a83ad9d9819e4a5e9312ba1d
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 8222.752, -5022.202, -13.916, 0, 0, 0.13, '{"scale x":1,"scale z":1,"col pos x":8222.752,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":0.13,"col size z":3.89,"col pos y":-5022.202,"context":null,"url":null}')

-- e6c02c3a2e9f938198b8c265d7a0a4ac06ceb170
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 8217.234, -5019.548, -13.665, 0, 0, -0.82, '{"scale x":1,"scale z":1,"col pos x":8217.234,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":-0.82,"col size z":3.89,"col pos y":-5019.548,"context":null,"url":null}')

-- 8546cf5584277bcf16093f5692b5fd87fb19afd3
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 8212.668, -5014.651, -13.616, 0, 0, -0.79, '{"scale x":1,"scale z":1,"col pos x":8212.668,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":-0.79,"col size z":3.89,"col pos y":-5014.651,"context":null,"url":null}')

-- 66ec0418e497d0d91d4ba87cd2e584ba2bf8d6f2
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 8224.726, -5011.428, -13.279, 0, 0, -1.47, '{"scale x":1,"scale z":1,"col pos x":8224.726,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":-1.47,"col size z":3.89,"col pos y":-5011.428,"context":null,"url":null}')

-- 3604572e91370c51e1b8b5c8761325cfdb9aa11e
SceneEditor:spawnShape("ma_land_telepod.ps", 8221.802, -5017.981, -14.45, 0, 0, 3.77, '{"scale x":0.64,"scale z":0.64,"col pos x":-1.504,"col size y":24,"col size x":24,"scale y":0.64,"col orientation":0,"col size z":24,"col pos y":-5017.981,"context":null,"url":null}')