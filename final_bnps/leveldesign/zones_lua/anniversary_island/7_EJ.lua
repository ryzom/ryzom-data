--############### SE_DECO 12333#################
-- GROUP: buildings_zorai
-- 227feb4210e0684a581925e11fa7968d3be8f99d
SceneEditor:spawnShape("zo_autel_base_zo_autel_village_a.shape", 18082, -1092, 2.071, 0, 0, -0.93, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18082,"col pos y":-1092,"col orientation":0.63,"col size x":22.066,"col size y":22.066,"col size z":5.611,"context":null,"url":null}')

-- 75408b349470ec3262f0cfffc900bb558d6db19a
SceneEditor:spawnShape("zo_autel_ambre_zo_autel_village_a.shape", 18086.865, -1088.365, 4.75, 0, 0, -0.93, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-1088.365,"col orientation":0,"col size x":12.423,"col size y":9.503,"col size z":0.6,"context":null,"url":null}')

-- 20eebda5f6cf58d1087ad4d6d72c4c4f6e7f72e1
SceneEditor:spawnShape("zo_bar_ambre_centre_zo_bar_village_b.shape", 18088.143, -1092.34, 4.7, 0.4, 0, -0.65, '{"scale x":1.2,"scale y":1.2,"scale z":1.1,"col pos x":17.686,"col pos y":-1092.34,"col orientation":0,"col size x":2.825,"col size y":2.97,"col size z":1.709,"context":null,"url":null}')


-- GROUP: plants_zorai
-- f6c039a266d34d5c4bd256007823cf39397180c0
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18096.504, -976.992, 0.075, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18096.504,"col pos y":-976.992,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 32d798d0d456473177cfb09efabcf8fb6132b2f0
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18087.494, -985.284, -0.002, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18087.494,"col pos y":-985.284,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- a3c05256dc7dd30b35ae3702f7d1f2e71ba8d078
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18119.848, -996.359, 1.127, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18119.848,"col pos y":-996.359,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 9209978de18ea7cf6d0a63cf1bdd3a530c91591d
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18118.104, -1010.139, 2.939, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18118.104,"col pos y":-1010.139,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 56c335a30dc1c4434e1072b70abaa6bac7c65119
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18167.467, -1024.042, 0.731, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18167.467,"col pos y":-1024.042,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- c47076e13dda4745437c6abbfc2e9316bfcc6806
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18153.275, -1042.631, 0.403, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18153.275,"col pos y":-1042.631,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- d3ba5a3915c03d3021821bbe65543aa4d9eab18b
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18130.82, -1008.137, 2.635, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18130.82,"col pos y":-1008.137,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 21227d63c41d9641acbad0102380db023152d600
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18137.238, -1049.859, -0.343, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18137.238,"col pos y":-1049.859,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 131eed1194461167d8c3a23d601d0e05be1f7c69
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18116.021, -1039.226, 1.118, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18116.021,"col pos y":-1039.226,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 15494a7f0f79217b92a85171fa8036f7960fb811
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18092.961, -1026.582, 0.029, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18092.961,"col pos y":-1026.582,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- fcbaa1006a4355f372c000700bb32d9b0b359997
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18095.646, -1013.303, -0.012, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18095.646,"col pos y":-1013.303,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- dc744456789c295dd59de23f6e0ba2af3e187f2a
SceneEditor:spawnShape("ju_s2_big_tree.shape", 18105.113, -993.001, 0.025, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18105.113,"col pos y":-993.001,"col orientation":0,"col size x":4.85,"col size y":4.065,"col size z":25.035,"context":null,"url":null}')

-- 511865534df62871f0781f0ff8da1f94ca4e4701
SceneEditor:spawnShape("ju_s2_big_tree.shape", 18084.246, -963.973, -0.034, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18084.246,"col pos y":-963.973,"col orientation":0,"col size x":8.65,"col size y":5.165,"col size z":25.035,"context":null,"url":null}')

-- 711570a4ab2cb5888c3e0a7baf1d3414b30c0591
SceneEditor:spawnShape("ju_s3_tree.shape", 18106.117, -1029.952, 0.091, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18106.117,"col pos y":-1030.504,"col orientation":0,"col size x":2.115,"col size y":2.845,"col size z":19.556,"context":null,"url":null}')

-- a42eb551069b916f3dc4e881aa514c9fe0581d21
SceneEditor:spawnShape("ju_s3_tree.shape", 18146.336, -987.745, 1.6, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18146.336,"col pos y":-988.271,"col orientation":0,"col size x":1.725,"col size y":3.035,"col size z":19.556,"context":null,"url":null}')

-- 10f808dd836c6bbf65a544dd304b6f23d77a034d
SceneEditor:spawnShape("ju_s3_fantree.shape", 18083.666, -1066.558, 0.023, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18083.666,"col pos y":-1066.558,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 3b651dc7bac5da731aba295f6b65af10869b21a3
SceneEditor:spawnShape("ju_s3_fantree.shape", 18110.152, -1061.49, -0.138, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110.152,"col pos y":-1061.49,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 5c6fb2e0c53efac8142b4592c8f9d59af0e4af0d
SceneEditor:spawnShape("ju_s3_fantree.shape", 18124.008, -1093.419, -1.197, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18124.008,"col pos y":-1093.419,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 71510684bacbed786ad00706d0fe2ef7f1e8e320
SceneEditor:spawnShape("ju_s3_fantree.shape", 18101.754, -1107.195, -0.417, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18101.754,"col pos y":-1107.195,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 4a8ee192eb12e72d56c3ae40e1ae81152c350374
SceneEditor:spawnShape("ju_s3_fantree.shape", 18095.447, -1105.166, -0.271, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18095.447,"col pos y":-1105.166,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 762aa816a09862f3efd96885c9acd4cf2f50a42d
SceneEditor:spawnShape("ju_s1_bamboo.shape", 18182.312, -992.089, 0.331, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18182.312,"col pos y":-992.089,"col orientation":0,"col size x":6.997,"col size y":10.731,"col size z":52.171,"context":null,"url":null}')

-- 48acd1cc9b73a66de0f4f3a7428ff11806a545ca
SceneEditor:spawnShape("ju_s3_fantree.shape", 18127.279, -982.779, 0.916, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18127.279,"col pos y":-982.779,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- d17d73f92e47d59caca8365711de2d53bafa47cb
SceneEditor:spawnShape("ju_s3_fantree.shape", 18105.734, -966.874, -0.008, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18105.734,"col pos y":-966.874,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')
--############### SE_DECO 12333#################

