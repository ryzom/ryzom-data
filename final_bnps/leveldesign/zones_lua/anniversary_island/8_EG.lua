--############### SE_DECO 12333#################
-- GROUP: buildings_kami
-- de7d4ae6c65ef3cce60a60a8be5dfebc259b2037
SceneEditor:spawnShape("ge_mission_etendard_kami.ps", 17738.459, -1148.604, -1.105, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17738.459,"col pos y":-1148.604,"col orientation":0,"col size x":6,"col size y":6,"col size z":10.2,"context":null,"url":null}')

-- 0f3f63049a4879b1609a60cf1bf340ba41c55448
SceneEditor:spawnShape("ge_mission_etendard_kami.ps", 17721.539, -1148.604, -1.377, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":74.953,"col pos y":-1148.604,"col orientation":0,"col size x":6,"col size y":6,"col size z":10.2,"context":null,"url":null}')

-- 83e3efb1d1f5cc813b850b78ae3c44042bcf86a7
SceneEditor:spawnShape("ge_mission_portail_kami.ps", 17734.002, -1126.781, 0, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":-15.322,"col pos y":-1126.781,"col orientation":0,"col size x":8,"col size y":8,"col size z":16.031,"context":null,"url":null}')

-- 48b4be908231edd9ad7116a93073342a719b921d
SceneEditor:spawnShape("ge_mission_portail_kami.ps", 17720.383, -1126.781, 0, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":-25.033,"col pos y":-1126.781,"col orientation":0,"col size x":8,"col size y":8,"col size z":16.031,"context":null,"url":null}')

-- 6cfb707e5bba45f46244ada853b6da7b2dd5c5d0
SceneEditor:spawnShape("ge_mission_portail_kami.ps", 17727.141, -1126.781, 0, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":-0.449,"col pos y":-1126.781,"col orientation":0,"col size x":8,"col size y":8,"col size z":16.031,"context":null,"url":null}')

-- 66bacfa6db953a68771877d4901e82f13ee622b2
SceneEditor:spawnShape("ge_mission_temple_of_maduk.ps", 17731.633, -1174.417, -0.938, 0, 0, 1.12, '{"scale x":2,"scale y":2,"scale z":2,"col pos x":17731.633,"col pos y":-1174.417,"col orientation":0,"col size x":30,"col size y":36,"col size z":68.6,"context":null,"url":null}')

-- 2a42f05cf79bcf675d4042fc98f4ef3773ae8349
SceneEditor:spawnShape("ge_mission_cercle_runique.shape", 17665.305, -1128.416, -0.018, 0, 0, 0, '{"scale x":1.3,"scale y":1.3,"scale z":1.3,"col pos x":17665.305,"col pos y":-1128.416,"col orientation":0,"col size x":19.857,"col size y":18.947,"col size z":10.413,"context":null,"url":null}')

-- 8fbde3744dab86d7084d2f79207e10b1a2bddd64
SceneEditor:spawnShape("ge_mission_cercle_runique.ps", 17665.305, -1128.416, -1.026, 0, 0, 0, '{"scale x":1.5,"scale y":1.5,"scale z":1.5,"col pos x":17665.305,"col pos y":-1128.416,"col orientation":0,"col size x":16.956,"col size y":16.998,"col size z":16.989,"context":null,"url":null}')

-- 005181fe79c10867ee06fefc546f20452589f417
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17695.85, -1191.595, 0.519, 0, 0, 2.13, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17695.85,"col pos y":-1191.595,"col orientation":-1.06,"col size x":4.401,"col size y":1.58,"col size z":7.182,"context":null,"url":null}')

-- ad390782327ff63c9ab82be7e2d9e9ceb156691e
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17713.734, -1127.464, -0.141, 0, 0, 0.34, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17713.734,"col pos y":-1127.464,"col orientation":0.28,"col size x":4.171,"col size y":1.35,"col size z":7.182,"context":null,"url":null}')

-- e18c1fe92fa1452e6970f73fc177e4b7e5c5f87a
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17740.885, -1128.781, -0.198, 0, 0, -3.52, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17740.885,"col pos y":-1128.781,"col orientation":-0.37,"col size x":4.191,"col size y":1.37,"col size z":7.182,"context":null,"url":null}')

-- 81efb86e4cbdc9ead10be22f19ab255b4b2f9a0e
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17758.719, -1137.455, 0.01, 0, 0, 2.55, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17758.719,"col pos y":-1137.455,"col orientation":0,"col size x":7.821,"col size y":5,"col size z":7.182,"context":null,"url":null}')

-- ed2f28893ac64450eeaacdd2880e3b2c572abe46
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17696.443, -1145.233, -0.048, 0, 0, 1.09, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17696.443,"col pos y":-1145.233,"col orientation":1.07,"col size x":4.161,"col size y":1.34,"col size z":7.182,"context":null,"url":null}')

-- 4e417fad33d2772dcb7fff70bc9b15e9b5fcc904
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17690.756, -1166.994, -0.274, 0, 0, 1.58, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17690.756,"col pos y":-1166.994,"col orientation":1.57,"col size x":4.311,"col size y":1.49,"col size z":7.182,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_deco_Memorial 12437#################
-- GROUP: memorial
-- 02723b0c1abf0486d725e62d7b0439cadc0f353c
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17599.854, -1235.681, 0.022, 0, 0, -3.36, '{"col pos x":17599.854,"col size x":9.377,"scale z":1,"col size y":13.111,"col orientation":-0.12,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1235.681,"context":null,"url":null}')

-- e842be44f50883a0767b64c94b96dce9e5f38776
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17601.254, -1225.763, -0.012, 0, 0, -3.29, '{"col pos x":17601.254,"col size x":9.627,"scale z":1,"col size y":13.361,"col orientation":-0.15,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1225.763,"context":null,"url":null}')

-- 73aae70f8d26c7c1ada32b80f927b11833c8bf3f
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17601.775, -1214.13, 0.048, 0, 0, 0, '{"col pos x":17601.775,"col size x":8.807,"scale z":1,"col size y":12.541,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1214.13,"context":null,"url":null}')

-- 16bc0ea6326e7d03448a9fb8715805ad155b78e3
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17600.793, -1203.768, 0.025, 0, 0, 0.06, '{"col pos x":17600.793,"col size x":7.617,"scale z":1,"col size y":11.351,"col orientation":0.06,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1203.768,"context":null,"url":null}')

-- 2fd023959508ac6ab8fcb6025ee8f0c80b12fc09
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17599.689, -1160.719, -0.066, 0, 0, -6.17, '{"col pos x":17599.689,"col size x":6.887,"scale z":1,"col size y":10.621,"col orientation":-3.22,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1160.719,"context":null,"url":null}')

-- 180d55153438aa1d1dda96ee066e96fd167aef90
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17600.373, -1192.427, -0.04, 0, 0, -0.25, '{"col pos x":17600.373,"col size x":8.827,"scale z":1,"col size y":12.561,"col orientation":0.08,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1192.427,"context":null,"url":null}')

-- cb18740fc83631f26559aeceec38c89b12b5b7e8
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17599.811, -1181.449, -0.014, 0, 0, 0, '{"col pos x":17599.811,"col size x":7.807,"scale z":1,"col size y":11.541,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1181.449,"context":null,"url":null}')

-- 9c92768252b3e15d29cc8c5ebb3ee6d50fd9bb24
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17600.207, -1170.637, 0.012, 0, 0, 0, '{"col pos x":17600.207,"col size x":8.757,"scale z":1,"col size y":12.491,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1170.637,"context":null,"url":null}')

-- 21e26f890fea11a4f0e4941ee7b2501e4edf825b
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17602.291, -1151.098, 0.044, 0, 0, -6.79, '{"col pos x":17602.291,"col size x":8.827,"scale z":1,"col size y":12.561,"col orientation":-0.24,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1151.098,"context":null,"url":null}')

-- 510f05a2088f95eb7b6476ee41b72d11cac4ceee
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17609.355, -1130.684, 0.008, 0, 0, -0.6, '{"col pos x":17609.355,"col size x":8.397,"scale z":1,"col size y":12.131,"col orientation":-0.39,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1130.684,"context":null,"url":null}')

-- e2019d5f502e898fbb0f297b3b0f4450be685d19
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17604.928, -1141.598, 0.002, 0, 0, -3.53, '{"col pos x":17604.928,"col size x":8.707,"scale z":1,"col size y":12.441,"col orientation":-0.34,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1141.598,"context":null,"url":null}')

-- 34fb763566ed8524fada440e7350a2a54c7590dd
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17613.225, -1120.276, 0.015, 0, 0, -0.4, '{"col pos x":17613.225,"col size x":9.517,"scale z":1,"col size y":13.251,"col orientation":-0.36,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1120.276,"context":null,"url":null}')
--############### SE_deco_Memorial 12437#################

