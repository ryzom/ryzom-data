--############### SE_DECO 12333#################
-- GROUP: buildings_tryker
-- 451f7408d4c999da6d85d2d8fc68e3317e9f4473
SceneEditor:spawnShape("tr_bar_01_village_a.shape", 17373.123, -1026.327, -0.902, 0, 0, -0.63, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-1026.327,"col orientation":0,"col size x":53.187,"col size y":56.863,"col size z":21.473,"context":null,"url":null}')

-- 2eaeaa1af1812a04cc2b9752c7c0a50dd2902174
SceneEditor:spawnShape("tr_mairie_helicemairie_village_a.shape", 17375.721, -1028.222, 16.652, 0, 1.57, -0.6, '{"scale x":1.26,"scale y":1.26,"scale z":1.26,"col pos x":110.91,"col pos y":-1028.222,"col orientation":0,"col size x":7.19,"col size y":6.705,"col size z":0.494,"context":null,"url":null}')

-- 53ad839763afebc4df2ee227ae6a1b04324fb138
SceneEditor:spawnShape("ge_mission_tente_tr.shape", 17303.863, -1117.539, -0.04, 0, 0, 0.63, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17303.863,"col pos y":-1117.539,"col orientation":0,"col size x":6.627,"col size y":7.862,"col size z":9.323,"context":null,"url":null}')

-- 129d579d1a91018930613a84812d79313bb4c7c3
SceneEditor:spawnShape("gen_bt_silo.shape", 17332.258, -1115.307, 0.034, 0, 0, -3.35, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17332.258,"col pos y":-1115.307,"col orientation":0,"col size x":4.181,"col size y":5.563,"col size z":10.403,"context":null,"url":null}')

-- 7ec6a612e3799427bc4f4324124495fd16d568f8
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 17305.059, -1114.442, 0.007, 0, 0, 0, '{"scale x":0.833,"scale y":0.833,"scale z":0.833,"col pos x":17305.059,"col pos y":-1114.442,"col orientation":0,"col size x":1.173,"col size y":1.078,"col size z":1.192,"context":null,"url":null}')

-- b3d2de4736901e48b5cf8b2cda180fae208bcf46
SceneEditor:spawnShape("tr_arche_01_village_a.shape", 17424.994, -977.498, 0.297, 0, 0, -1.09, '{"scale x":2.72,"scale y":1.65,"scale z":1,"col pos x":28.486,"col pos y":-977.498,"col orientation":-0.51,"col size x":10.668,"col size y":7.848,"col size z":14.364,"context":null,"url":null}')


-- GROUP: plants_tryker
-- 117223b52e1210e56de9cc68f8a44896168dc535
SceneEditor:spawnShape("tr_s2_bamboo_a.shape", 17311.342, -1106.989, 0.256, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17311.342,"col pos y":-1106.989,"col orientation":0,"col size x":2.238,"col size y":1.705,"col size z":11.739,"context":null,"url":null}')

-- 931c67531d6cfdd8cb3fc2662191f3b2d0378573
SceneEditor:spawnShape("tr_s2_bamboo_a.shape", 17347.574, -1086.704, -1.389, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17347.574,"col pos y":-1086.704,"col orientation":0,"col size x":2.728,"col size y":2.195,"col size z":11.739,"context":null,"url":null}')

-- f5078f2af5440391ca07a523607ea5126b2bc55a
SceneEditor:spawnShape("tr_s2_bamboo_a.shape", 17305.398, -1029.483, -0.327, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17305.398,"col pos y":-1029.483,"col orientation":0,"col size x":3.098,"col size y":2.565,"col size z":11.739,"context":null,"url":null}')

-- 3f420ce0659afd8b3f2ce9e0ff55f55787bb575d
SceneEditor:spawnShape("tr_s2_bamboo_a.shape", 17281.926, -999.933, -0.114, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17281.926,"col pos y":-999.933,"col orientation":0,"col size x":2.108,"col size y":1.575,"col size z":11.739,"context":null,"url":null}')

-- 2e058433f74bc903d2c10691df9d193d3eea5f80
SceneEditor:spawnShape("tr_s2_bamboo_a.shape", 17337.88, -978.33, -0.74, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17337.88,"col pos y":-978.33,"col orientation":0,"col size x":2.898,"col size y":2.365,"col size z":11.739,"context":null,"url":null}')

-- 2c9c59e756f5215339ec8cc1f0dd4b795737ad49
SceneEditor:spawnShape("tr_s2_bamboo_a.shape", 17361.799, -962.288, -0.134, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17361.799,"col pos y":-962.288,"col orientation":0,"col size x":3.518,"col size y":2.985,"col size z":11.739,"context":null,"url":null}')

-- 1c438ede5de25673cc7a40289533164ad9a0f500
SceneEditor:spawnShape("tr_s2_lokness_c.shape", 17279.873, -1078.937, -0.077, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17279.873,"col pos y":-1072.262,"col orientation":0,"col size x":5.953,"col size y":4.921,"col size z":13.808,"context":null,"url":null}')

-- 5f55cb22d17bc5769ac529a6d2394a11b5c236ba
SceneEditor:spawnShape("tr_s2_nenufly_a.shape", 17371.375, -1099.508, 0.908, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17371.375,"col pos y":-1099.508,"col orientation":0,"col size x":3.253,"col size y":3.253,"col size z":11.213,"context":null,"url":null}')

-- 47e3f0da65c6c096723e086b9272705ddd9e3501
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17414.141, -1090.497, -1.407, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17414.141,"col pos y":-1090.497,"col orientation":0,"col size x":0.473,"col size y":1.153,"col size z":16.228,"context":null,"url":null}')

-- 5030a32644f861ff979a7f75917a8a359e498569
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17331.949, -1105.129, 0.031, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17331.949,"col pos y":-1105.129,"col orientation":0,"col size x":0.793,"col size y":1.473,"col size z":16.228,"context":null,"url":null}')

-- 0b547f9a0db213c6e6995d15bad3a99dc7bb4ebb
SceneEditor:spawnShape("tr_s3_nenufly_b.shape", 17391.055, -1110.98, 1.079, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17391.055,"col pos y":-1110.98,"col orientation":0,"col size x":6.256,"col size y":6.256,"col size z":5.446,"context":null,"url":null}')

-- afcbbe5ffefbf9eb86cd4de2c7334e05c3d9c586
SceneEditor:spawnShape("tr_s3_nenufly_b.shape", 17289.203, -1029.921, -0.103, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17289.203,"col pos y":-1029.921,"col orientation":0,"col size x":1.956,"col size y":1.956,"col size z":5.446,"context":null,"url":null}')

-- c4f125a93cae2e9d858aa61a2aba3e8971a32a1f
SceneEditor:spawnShape("tr_s3_nenufly_b.shape", 17284.021, -969.081, -0.037, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17284.021,"col pos y":-969.081,"col orientation":0,"col size x":1.696,"col size y":1.696,"col size z":5.446,"context":null,"url":null}')

-- 1c853587cdfc492fd882ac612f232157e417809f
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17315.508, -981.673, -0.213, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17315.508,"col pos y":-981.673,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 860ccf23475dffcdf419dcc43cb72129f477be57
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17287.98, -1007.519, -0.394, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17287.98,"col pos y":-1007.519,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- f4a54058eb8106f54beadfcffe8f7975b87c08bd
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17279.533, -1035.379, 0.013, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17279.533,"col pos y":-1035.379,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 704c92a0a2b470a519c4c508ee2a4bb230a8f624
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17281.328, -1064.062, -0.114, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17281.328,"col pos y":-1064.062,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- c7a54a1c6284bf20bfd58192768955c08446c447
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17303.846, -1093.335, 0.298, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17303.846,"col pos y":-1093.335,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- cfc8f24052b3571233e9c92e8fff73c1e92df42b
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17290.115, -1116.039, -0.004, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17290.115,"col pos y":-1116.039,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 7215c9e4d9e92842d3bc46a48b5b4177c1595211
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17413.068, -981.882, 0.358, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17413.068,"col pos y":-981.882,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 469d93127cfb6689ceeff8a16c946d6596bc1a27
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17381.555, -964.075, -0.291, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17381.555,"col pos y":-964.075,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- bc24cc17fd150655fe909c53c5c008212742f688
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17334.16, -964.368, -0.065, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17334.16,"col pos y":-964.368,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 7a19ac9e77e74a943dca473ac387a4d411721bd8
SceneEditor:spawnShape("Water_bassin-3.shape", 17347.156, -1030.192, -2.5, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17347.156,"col pos y":-1030.192,"col orientation":0,"col size x":152.754,"col size y":152.533,"col size z":0,"context":null,"url":null}')

-- dd7e292493c27f766d7f7023ef08e9b022523f41
SceneEditor:spawnShape("tr_s2_lokness_c.shape", 17292.707, -1078.99, -20, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17279.963,"col pos y":-1084.282,"col orientation":0,"col size x":5.713,"col size y":4.991,"col size z":13.808,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_deco_Entrance 12462#################
-- GROUP: entrance
-- 315b9f4bddce8cb20ca5d382e70bdfb1b05e8367
SceneEditor:spawnShape("gen_mission_1_caisse.shape", 17306.895, -968.524, 0.011, 0, 0, 0, '{"col pos x":17306.895,"col size x":1.61,"scale z":1.47,"col size y":1.61,"col orientation":0,"scale y":1.47,"col size z":1,"scale x":1.47,"col pos y":-968.524,"context":null,"url":null}')

-- f48421585363f00edc421fd14e86716a0cf2795c
SceneEditor:spawnShape("fo_s3_fougere.shape", 17285.762, -987.617, -0.116, 0, 0, 0, '{"col pos x":17285.762,"col size x":5.341,"scale z":1.279,"col size y":4.678,"col orientation":-1.92,"scale y":1.385,"col size z":3.538,"scale x":1.385,"col pos y":-987.617,"context":null,"url":null}')

-- d144b4c122342fffdd962d0b1b9919b34b91a798
SceneEditor:spawnShape("fo_s3_fougere.shape", 17305.771, -990.387, -1.314, 0, 0, 0, '{"col pos x":17305.771,"col size x":5.341,"scale z":1.369,"col size y":4.678,"col orientation":-1.19,"scale y":1.475,"col size z":3.538,"scale x":1.475,"col pos y":-990.387,"context":null,"url":null}')

-- 6de02caecdd7e23124a63fdeef28bc66369db86e
SceneEditor:spawnShape("fo_s3_fougere.shape", 17311.9, -971.708, 0.068, 0, 0, 0, '{"col pos x":17311.9,"col size x":5.341,"scale z":0.979,"col size y":4.678,"col orientation":0,"scale y":1.085,"col size z":3.538,"scale x":1.085,"col pos y":-971.708,"context":null,"url":null}')

-- c9eeb15c4a27a3effb5f2847a86820a5a74f12c3
SceneEditor:spawnShape("fo_s3_fougere.shape", 17320.652, -964.543, 0.015, 0, 0, 0, '{"col pos x":17320.652,"col size x":5.341,"scale z":1.019,"col size y":4.678,"col orientation":0,"scale y":1.125,"col size z":3.538,"scale x":1.125,"col pos y":-964.543,"context":null,"url":null}')

-- a19b2a44d43b7589a19cda6a05066e1c6da22021
SceneEditor:spawnShape("ge_mission_sac_b.shape", 17312.172, -963.657, -0.007, 0, 0, 0, '{"col pos x":17312.172,"col size x":1.7,"scale z":1,"col size y":1.62,"col orientation":0,"scale y":1,"col size z":0.968,"scale x":1,"col pos y":-963.657,"context":null,"url":null}')

-- a32264e7adbd65a29ecac456e40cad357ac24660
SceneEditor:spawnShape("ge_partylamp_02.shape", 17289.549, -1005.619, -0.592, 0, 0, 0, '{"col pos x":17296.66,"col size x":5.906,"scale z":1.52,"col size y":0.743,"col orientation":0.23,"scale y":1.52,"col size z":5.18,"scale x":1.52,"col pos y":-1005.226,"context":null,"url":null}')

-- d0bdf284fce9126d29708921144dbfa7929e32d0
SceneEditor:spawnShape("ge_partylamp_02.shape", 17303.531, -1003.21, -1.828, 0, 0, 0.32, '{"col pos x":17309.848,"col size x":5.706,"scale z":1.52,"col size y":1.603,"col orientation":0.64,"scale y":1.52,"col size z":5.18,"scale x":1.52,"col pos y":-1000.349,"context":null,"url":null}')

-- 2ee396cba93743b43e5e66bd049fdb725973e82b
SceneEditor:spawnShape("ge_partylamp_02.shape", 17313.945, -994.344, -1.87, 0, 0, 1.05, '{"col pos x":17317.744,"col size x":5.936,"scale z":1.55,"col size y":0.743,"col orientation":1.05,"scale y":1.55,"col size z":5.18,"scale x":1.55,"col pos y":-988.05,"context":null,"url":null}')

-- fd44c77ecbc2b96159cf7bfc84469bf7d1794362
SceneEditor:spawnShape("ge_partylamp_02.shape", 17321.094, -981.603, -0.224, 0, 0, 1.06, '{"col pos x":17324.49,"col size x":5.516,"scale z":1.51,"col size y":0.743,"col orientation":1.07,"scale y":1.51,"col size z":5.18,"scale x":1.51,"col pos y":-975.394,"context":null,"url":null}')

-- cbd22bf39de9b204f527899a5ca23362542c6117
SceneEditor:spawnShape("ge_partylamp_02.shape", 17327.342, -968.854, -0.088, 0, 0, 1.19, '{"col pos x":17329.934,"col size x":5.406,"scale z":1.48,"col size y":0.743,"col orientation":1.19,"scale y":1.48,"col size z":5.18,"scale x":1.48,"col pos y":-962.347,"context":null,"url":null}')

-- c5805840859e247f853d2609c400e98926356a86
SceneEditor:spawnShape("ge_mission_tente_ma.shape", 17305.561, -974.411, -0.044, 0, 0, 0.82, '{"col pos x":17305.561,"col size x":7.677,"scale z":1.54,"col size y":7.196,"col orientation":0.87,"scale y":1.54,"col size z":9.26,"scale x":1.54,"col pos y":-974.411,"context":null,"url":null}')

-- 13fd303bc143450c887bc4c00b3817c7faae9df7
SceneEditor:spawnShape("ge_mission_tente_karavan.shape", 17295.773, -997.859, -1.277, 0, 0, 0, '{"col pos x":17295.773,"col size x":15.879,"scale z":1.92,"col size y":8.24,"col orientation":0,"scale y":1.92,"col size z":5.598,"scale x":1.92,"col pos y":-997.859,"context":null,"url":null}')

-- 31268be1d394aa927656162a4ba57d0eb06dfc97
SceneEditor:spawnShape("ge_feudecamp.ps", 17286.34, -959.617, -0.042, 0, 0, 0, '{"col pos x":17286.34,"col size x":2.491,"scale z":2.07,"col size y":2.608,"col orientation":0,"scale y":2.07,"col size z":5.716,"scale x":2.07,"col pos y":-959.617,"context":null,"url":null}')

-- aaea93edc51e7094afb27665aad6e6382b98694e
SceneEditor:spawnShape("Gen_Bt_Silo.shape", 17323.775, -961.454, -0.006, 0, 0, 4.32, '{"col pos x":17323.775,"col size x":3.321,"scale z":1,"col size y":3.433,"col orientation":1.21,"scale y":1,"col size z":10.403,"scale x":1,"col pos y":-961.454,"context":null,"url":null}')

-- 2ca216ce32aa8d305d12ff95f34928b0a92b7c9e
SceneEditor:spawnShape("GE_Mission_tente.shape", 17282.373, -980.338, -0.019, 0, 0, -0.56, '{"col pos x":17282.373,"col size x":5.578,"scale z":1.28,"col size y":6.365,"col orientation":1.08,"scale y":1.28,"col size z":5.034,"scale x":1.28,"col pos y":-980.338,"context":null,"url":null}')

-- f17401f76f040b70f90a368195e17c4169d582ca
SceneEditor:spawnShape("GE_Mission_tente.shape", 17315.66, -965.016, 0.004, 0, 0, 0.32, '{"col pos x":17315.66,"col size x":5.238,"scale z":1,"col size y":4.575,"col orientation":0.38,"scale y":1,"col size z":5.034,"scale x":1,"col pos y":-965.016,"context":null,"url":null}')
--############### SE_deco_Entrance 12462#################

