--############### SE_DECO 12333#################
-- GROUP: buildings_matis
-- 82d0e5c9527a0e521f42d3283e021fde5a616a9d
SceneEditor:spawnShape("ma_annexe_ext_4_village_a.shape", 18085.266, -665.448, 6, 0, 0, -1.98, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18085.266,"col pos y":-665.448,"col orientation":1.18,"col size x":15.42,"col size y":29.245,"col size z":20.841,"context":null,"url":null}')


-- GROUP: buildings_marauder
-- 6224f046c271a126dfa53d2965ce9305442ebd17
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18153.236, -798.197, 0.014, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18153.236,"col pos y":-798.197,"col orientation":0,"col size x":0.751,"col size y":0.663,"col size z":5.674,"context":null,"url":null}')

-- 630fc55aeb3ff5b754fdd56bb21093ae3eb02b48
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18127.217, -789.473, 0.255, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18127.217,"col pos y":-789.473,"col orientation":0,"col size x":0.731,"col size y":0.823,"col size z":5.674,"context":null,"url":null}')

-- 51dbbef9332c98d17f50a5028447b18f2ac69f69
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18105.996, -792.47, -0.042, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18105.996,"col pos y":-792.47,"col orientation":0,"col size x":0.681,"col size y":0.733,"col size z":5.674,"context":null,"url":null}')


-- GROUP: plants_matis
-- 02ac7cc1d80887a25bb9b6ba9d14cae06ee2d763
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 18115.309, -701.827, -0.007, 0, 0, 0, '{"col size x":5.054,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.602,"col size z":12.685,"scale y":1,"col pos y":-701.827,"col pos x":18115.309,"context":null,"url":null}')

-- 757443f079a0729bb0d64d418ceb9b9399e2f61e
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 18095.309, -706.724, -0.012, 0, 0, 0, '{"col size x":3.141,"scale x":1,"col orientation":0,"scale z":0.74,"col size y":3.742,"col size z":66.446,"scale y":1,"col pos y":-706.724,"col pos x":18095.309,"context":null,"url":null}')

-- 85896f1dfad22784ce6664eb86ce620a2be036d4
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18085.719, -694.401, -0.022, 0, 0, 0, '{"col size x":1.577,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.188,"col size z":13.303,"scale y":1,"col pos y":-694.401,"col pos x":18085.719,"context":null,"url":null}')

-- 78721855c84b92942c24c9d1ab13b2d9e08a5e78
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18108.27, -696.905, -0.158, 0, 0, 0, '{"col size x":1.397,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.008,"col size z":13.303,"scale y":1,"col pos y":-696.905,"col pos x":18108.27,"context":null,"url":null}')

-- b083013aa52a2186398b970d776f05d67c3bd883
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18122.359, -700.34, -0.055, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-700.34,"col pos x":18122.359,"context":null,"url":null}')

-- b3b46a31ea4ca6711999778d852c8916a3512868
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18131.184, -670.996, -1.123, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-670.996,"col pos x":18131.184,"context":null,"url":null}')

-- dfac79884ddab2504c72ab450f78c463b7e370f5
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18156.525, -667.611, -0.181, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-667.611,"col pos x":18156.525,"context":null,"url":null}')

-- e901494b3aa75e3b058d37e5a0b188267276397c
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18160.062, -697, 0.359, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-697,"col pos x":18160.062,"context":null,"url":null}')

-- bd6844b99a1648fb272816875b5cc2af08181fb7
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18136.82, -711.015, -0.486, 0, 0, 0, '{"col size x":1.647,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.258,"col size z":13.303,"scale y":1,"col pos y":-711.015,"col pos x":18136.82,"context":null,"url":null}')

-- 8d78bb29cc260daa1eb095cf04f6704e428b9945
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18125.439, -729.434, -0.72, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":1.53,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-729.434,"col pos x":18125.439,"context":null,"url":null}')

-- 947dbde48fd480664532dda2c3c459dcf93a39bc
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18147.496, -733.151, -1.279, 0, 0, 0, '{"col size x":2.287,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.898,"col size z":13.303,"scale y":1,"col pos y":-733.151,"col pos x":18147.496,"context":null,"url":null}')

-- 417fd26da72258ed960187652b916cb183b87eb1
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18179.125, -729.429, 2.067, 0, 0, 0, '{"col size x":1.457,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.068,"col size z":13.303,"scale y":1,"col pos y":-729.429,"col pos x":18179.125,"context":null,"url":null}')

-- b506e51d98945800b05ed6a69cbf751ecd5516c4
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18167.641, -757.195, 0.358, 0, 0, 0, '{"col size x":1.537,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.148,"col size z":13.303,"scale y":1,"col pos y":-757.195,"col pos x":18167.641,"context":null,"url":null}')

-- abb05f7cc5118f14fa38a05e0f5d20c6262d4722
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18153.736, -789.641, 0.434, 0, 0, 0, '{"col size x":1.667,"scale x":1,"col orientation":0,"scale z":0.85,"col size y":1.278,"col size z":13.303,"scale y":1,"col pos y":-789.641,"col pos x":18153.736,"context":null,"url":null}')

-- efbd7d9b22fefedadb54abac8462b528babe0ae1
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18156.225, -770.063, 1.183, 0, 0, 0, '{"col size x":1.317,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.928,"col size z":13.303,"scale y":1,"col pos y":-770.063,"col pos x":18156.225,"context":null,"url":null}')

-- 78baca8e9ed33d2ff4902eaf4050b1f6c25f5b59
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18149.422, -755.666, 0.558, 0, 0, 0, '{"col size x":1.657,"scale x":1,"col orientation":0,"scale z":1.29,"col size y":1.268,"col size z":13.303,"scale y":1,"col pos y":-755.666,"col pos x":18149.422,"context":null,"url":null}')

-- 3fc518199f0c96ddd8095fef98463a78bff0d681
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18116.715, -747.852, 2.959, 0, 0, 0, '{"col size x":1.457,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.068,"col size z":13.303,"scale y":1,"col pos y":-747.852,"col pos x":18116.715,"context":null,"url":null}')

-- 230f1be120ffbbf3a0e12c73e9baffa5e21b457e
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18083.895, -731.543, 0.049, 0, 0, 0, '{"col size x":1.347,"scale x":1,"col orientation":0,"scale z":1.36,"col size y":0.958,"col size z":13.303,"scale y":1,"col pos y":-731.543,"col pos x":18083.895,"context":null,"url":null}')

-- 9197b701c2a2c5c0110239c4cb87de4084bbb6a2
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18094.256, -715.613, 0.017, 0, 0, 0, '{"col size x":1.077,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.688,"col size z":13.303,"scale y":1,"col pos y":-715.613,"col pos x":18094.256,"context":null,"url":null}')

-- ef19e39ff73694a345b58dedddcb366aacb917c8
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18094.732, -764.398, 0.036, 0, 0, 0, '{"col size x":1.667,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.278,"col size z":13.303,"scale y":1,"col pos y":-764.398,"col pos x":18094.732,"context":null,"url":null}')

-- efb354bf2425faa87f0f9024c5b96bf0890119ac
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18086.68, -774.72, 0.019, 0, 0, 0, '{"col size x":1.687,"scale x":1,"col orientation":0,"scale z":0.83,"col size y":1.298,"col size z":13.303,"scale y":1,"col pos y":-774.72,"col pos x":18086.68,"context":null,"url":null}')

-- 695bb59744e813f16b1a913672f6a9c5b8ca45fd
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18115.49, -768.932, 0.013, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-768.932,"col pos x":18115.49,"context":null,"url":null}')

-- 860e15be3534ec96713aab4788bf34769cfc44bb
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18133.998, -774.264, 1.485, 0, 0, 0, '{"col size x":1.437,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.048,"col size z":13.303,"scale y":1,"col pos y":-774.264,"col pos x":18133.998,"context":null,"url":null}')

-- d71d5dc1366c3059a2d25ca562c4c7c863456d93
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18136.418, -788.337, 0.56, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":0.85,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-788.337,"col pos x":18136.418,"context":null,"url":null}')
--############### SE_DECO 12333#################

