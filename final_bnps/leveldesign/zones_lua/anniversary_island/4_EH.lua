--############### SE_DECO 12333#################
-- GROUP: plants_matis
-- a2e0872756d4334364560f4d2810a0052dce598d
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 17906.391, -583.734, 0.616, 0, 0, 0, '{"col size x":3.441,"scale x":1,"col orientation":0,"scale z":1,"col size y":3.562,"col size z":66.446,"scale y":1,"col pos y":-583.734,"col pos x":17906.391,"context":null,"url":null}')

-- 6564cd7e94eb026777be647c70c2e11d28b1e0bd
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17906.516, -636.389, -0.016, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-636.389,"col pos x":17906.516,"context":null,"url":null}')

-- ffcba77f3467f536bd4ae7159d8365c0de686837
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17873.346, -616.858, 0.485, 0, 0, 0, '{"col size x":1.337,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.948,"col size z":13.303,"scale y":1,"col pos y":-616.858,"col pos x":17873.346,"context":null,"url":null}')

-- 4aa7ad86529454717b93d853f7262f535cd6aed2
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17884.557, -601.139, 0.812, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-601.139,"col pos x":17884.557,"context":null,"url":null}')

-- 0cb9e6be85c13ff3723e4af794c6d531748578f9
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17899.924, -619.175, 0.052, 0, 0, 0, '{"col size x":1.757,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.368,"col size z":13.303,"scale y":1,"col pos y":-619.175,"col pos x":17899.924,"context":null,"url":null}')

-- e6ecdd7428ffd1bcfcadc56284c0f5bd25dd2ebf
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17908.867, -603.4, 0.005, 0, 0, 0, '{"col size x":1.757,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.368,"col size z":13.303,"scale y":1,"col pos y":-603.4,"col pos x":17908.867,"context":null,"url":null}')

-- 04eb99a1d8e0d8d82fa2954189682036e8e7b0d3
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17893.404, -595.46, 1.314, 0, 0, 0, '{"col size x":1.367,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.978,"col size z":13.303,"scale y":1,"col pos y":-595.46,"col pos x":17893.404,"context":null,"url":null}')

-- 91c023ecc16c82741e627e31445c9e240dfd5bcd
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17914.963, -580.274, 0.158, 0, 0, 0, '{"col size x":1.437,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.048,"col size z":13.303,"scale y":1,"col pos y":-580.274,"col pos x":17914.963,"context":null,"url":null}')

-- 7d08cc1f208cc5a8e9a075793ec073067dc2a390
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17902.611, -575.535, 0.844, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-575.535,"col pos x":17902.611,"context":null,"url":null}')

-- c47ca55114fdaf62f4f725ddf2d1a97e37191e5c
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17891.393, -570.559, 1.5, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-570.559,"col pos x":17891.393,"context":null,"url":null}')

-- 340050c206202ece0dd157f8942537a072f3ab9b
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17863.676, -634.592, 0.006, 0, 0, 0, '{"col size x":1.887,"scale x":1,"col orientation":0,"scale z":1.39,"col size y":1.498,"col size z":13.303,"scale y":1,"col pos y":-634.592,"col pos x":17863.676,"context":null,"url":null}')

-- 24fa74c0cc9bbb8c1507b1518b79759e2b54a6e7
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17858.439, -607.632, 3.319, 0, 0, 0, '{"col size x":1.487,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.098,"col size z":13.303,"scale y":1,"col pos y":-607.632,"col pos x":17858.439,"context":null,"url":null}')

-- 0c4128afdf95eb4b2711b634cbf9ea525ff3d7c8
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17859.887, -589.403, -0.159, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-589.403,"col pos x":17859.887,"context":null,"url":null}')

-- e67e658c6f035ab3525bae5f12c28ca416420965
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17865.748, -565.248, -0.094, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-565.248,"col pos x":17865.748,"context":null,"url":null}')

-- b023a730fafc291fe9acdf9c4ab3efc9e3549961
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17871.604, -545.809, 0.614, 0, 0, 0, '{"col size x":1.797,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.408,"col size z":13.303,"scale y":1,"col pos y":-545.809,"col pos x":17871.604,"context":null,"url":null}')

-- e5bbbfc53e1b706062410dd294b4a3575af39d8e
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17881.762, -534.491, 0.993, 0, 0, 0, '{"col size x":1.357,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.968,"col size z":13.303,"scale y":1,"col pos y":-534.491,"col pos x":17881.762,"context":null,"url":null}')

-- ddac77f5adc612551dd0b3864e94ba05ffaf7e07
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17905.785, -548.039, 0.015, 0, 0, 0, '{"col size x":1.237,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.848,"col size z":13.303,"scale y":1,"col pos y":-548.039,"col pos x":17905.785,"context":null,"url":null}')
--############### SE_DECO 12333#################

