--############### SE_DECO 12333#################
-- GROUP: plants_fyros
-- 814b2f6bd26fd51060b51eeeca308a4316973247
SceneEditor:spawnShape("fy_s2_lovejail_c.shape", 17378.836, -804.176, -0.237, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17378.836,"col pos y":-804.176,"col orientation":0,"col size x":3.167,"col size y":2.255,"col size z":5.141,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_deco_Entrance 12462#################
-- GROUP: entrance
-- 0e729cf635ff3d29a5862d872fe1f62c4e373eb4
SceneEditor:spawnShape("ge_mission_objet_pack_2.shape", 17303.92, -897.283, -3.943, 0, 0, 0, '{"col pos x":17303.92,"col size x":2.321,"scale z":1,"col size y":3.263,"col orientation":0,"scale y":1,"col size z":2.243,"scale x":1,"col pos y":-897.283,"context":null,"url":null}')

-- 272ad11aefb6c6106489761ad2b9ad46300f2da1
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 17320.896, -895.253, -4.788, 0, 0, 3.15, '{"col pos x":17320.896,"col size x":2.685,"scale z":0.769,"col size y":5.184,"col orientation":0,"scale y":0.769,"col size z":2.147,"scale x":0.769,"col pos y":-895.253,"context":null,"url":null}')

-- 85b8fb5bab7561445a17ff51a224d2457c0d6d72
SceneEditor:spawnShape("ge_mission_socle_flame_courage.ps", 17308.199, -904.892, -3.797, 0, 0, 0, '{"col pos x":17308.199,"col size x":1.647,"scale z":1.32,"col size y":1.608,"col orientation":0,"scale y":1.32,"col size z":2.071,"scale x":1.32,"col pos y":-904.892,"context":null,"url":null}')

-- cac4f10ee08ff06c8f93da740798f51a0bc78c35
SceneEditor:spawnShape("gen_mission_1_caisse.shape", 17304.309, -900.286, -3.794, 0, 0, 0, '{"col pos x":17304.309,"col size x":1.11,"scale z":1,"col size y":1.11,"col orientation":0,"scale y":1,"col size z":1,"scale x":1,"col pos y":-900.286,"context":null,"url":null}')

-- 2e713621bc7a15084b3f6c67de71be7c3d55c6c6
SceneEditor:spawnShape("gen_mission_3_caisses.shape", 17305.402, -899.253, -4.073, 0, 0, 0.49, '{"col pos x":17305.402,"col size x":1.362,"scale z":1,"col size y":2.542,"col orientation":0.45,"scale y":1,"col size z":2,"scale x":1,"col pos y":-899.253,"context":null,"url":null}')

-- bffc29c39f40eca69d5eb64297053bd7983f6c77
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 17311.664, -894.462, -4.754, 0, 0, 0, '{"col pos x":17311.664,"col size x":12.323,"scale z":2.07,"col size y":11.569,"col orientation":0,"scale y":2.07,"col size z":5.553,"scale x":2.07,"col pos y":-894.462,"context":null,"url":null}')

-- ab2cbc025e301e5a02de2039ec46ad8c9a47c8a3
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17404.717, -880.031, -0.886, 0, 0, 0, '{"col pos x":17404.717,"col size x":0.531,"scale z":1,"col size y":0.533,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-880.031,"context":null,"url":null}')

-- 4b1e830207b273e3828b3410cbf2a0314f3fcef2
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17418.4, -879.26, -0.251, 0, 0, 0, '{"col pos x":17418.4,"col size x":0.581,"scale z":1,"col size y":0.463,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-879.26,"context":null,"url":null}')

-- 037e8032c155bae5bea43ed6abd701e99f4beaf1
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17424.006, -893.935, -0.508, 0, 0, 0, '{"col pos x":17424.006,"col size x":0.631,"scale z":1,"col size y":0.553,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-893.935,"context":null,"url":null}')

-- e2c26c464b620c78d8944c3f2c5f576a57e65cc8
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 17410.033, -895.253, -1.248, 0, 0, 0, '{"col pos x":17410.033,"col size x":0.461,"scale z":1,"col size y":0.433,"col orientation":0.135,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-895.253,"context":null,"url":null}')

-- b154b507ab63b70a5e82dc03fce13ced00491521
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 17417.242, -896.304, -0.876, 0, 0, 0, '{"col pos x":17417.242,"col size x":1.173,"scale z":0.833,"col size y":1.078,"col orientation":0,"scale y":0.833,"col size z":1.192,"scale x":0.833,"col pos y":-896.304,"context":null,"url":null}')

-- e334c6ad8e3eb15b25ef29d739bbb5990cc160e5
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17412.654, -873.796, 0.1, 0, 0, 0, '{"col pos x":17412.654,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":-1.92,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-873.796,"context":null,"url":null}')

-- 68284fe40f1189a3addc9553267c8b430e566502
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17414.238, -879.384, -0.431, 0, 0, 0, '{"col pos x":17414.238,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-879.384,"context":null,"url":null}')

-- a31c8271e890e6856f7a22e65a1e0d31e88dbcdd
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17413.828, -901.319, -0.816, 0, 0, 0, '{"col pos x":17413.828,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-901.319,"context":null,"url":null}')

-- 599c9a822ef0ab5bd40c9d0b41920e0ae187cec3
SceneEditor:spawnShape("gen_mission_3_tonneaux.shape", 17414.035, -902.318, -0.81, 0, 0, 0, '{"col pos x":17414.035,"col size x":2.141,"scale z":1,"col size y":1.168,"col orientation":-1.92,"scale y":1,"col size z":2.481,"scale x":1,"col pos y":-902.318,"context":null,"url":null}')

-- 12e659761367a80baef823693d2183356d1ae59b
SceneEditor:spawnShape("gen_mission_3_tonneaux.shape", 17412.545, -872.796, 0.208, 0, 0, 0, '{"col pos x":17412.545,"col size x":2.141,"scale z":1,"col size y":1.168,"col orientation":0,"scale y":1,"col size z":2.481,"scale x":1,"col pos y":-872.796,"context":null,"url":null}')

-- 30c958db02bd68f2b4252987ecbfb1525f661c09
SceneEditor:spawnShape("tr_hall_reunion_table.shape", 17410.594, -877.753, 0.011, 0, 0, 0, '{"col pos x":17410.076,"col size x":4.168,"scale z":1,"col size y":2.045,"col orientation":0.16,"scale y":1,"col size z":0.965,"scale x":1,"col pos y":-877.637,"context":null,"url":null}')

-- 6a86b6aaa2210721e340be57810018822e7a0b7e
SceneEditor:spawnShape("tr_hall_reunion_table.shape", 17418.395, -897.206, -0.524, 0, 0, 0, '{"col pos x":17417.822,"col size x":4.218,"scale z":1,"col size y":2.245,"col orientation":0.22,"scale y":1,"col size z":0.965,"scale x":1,"col pos y":-897.382,"context":null,"url":null}')

-- 505e86489aaea6508b72dafdb7bcbc880bee7806
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17407.307, -863.625, 1.251, 0, 0, 0, '{"col pos x":17407.307,"col size x":0.541,"scale z":1,"col size y":0.553,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-863.625,"context":null,"url":null}')

-- fce42509fb62a7ba1d13aa010dea7ab3c330f7b2
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17435.791, -866.236, 0.283, 0, 0, 0, '{"col pos x":17435.791,"col size x":0.551,"scale z":1,"col size y":0.513,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-866.236,"context":null,"url":null}')

-- 782236a24461898000fc2373297f0953a38e0f67
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17409.816, -910.436, -0.906, 0, 0, 0, '{"col pos x":17409.775,"col size x":0.501,"scale z":1,"col size y":0.513,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-910.448,"context":null,"url":null}')

-- 4069ebbf315d14dd31bc45c123cdc6dedbc2c77d
SceneEditor:spawnShape("ge_mission_jarre.shape", 17282.689, -953.945, -0.032, 0, 0, 0, '{"col pos x":17282.689,"col size x":0.598,"scale z":0.833,"col size y":0.598,"col orientation":0,"scale y":0.833,"col size z":1.151,"scale x":0.833,"col pos y":-953.945,"context":null,"url":null}')

-- 2a31847570eea0d6eaa2599f32b36867265858cd
SceneEditor:spawnShape("ge_mission_jarre.shape", 17282.18, -954.319, -0.024, 0, 0, 0, '{"col pos x":17282.18,"col size x":0.598,"scale z":0.833,"col size y":0.598,"col orientation":0,"scale y":0.833,"col size z":1.151,"scale x":0.833,"col pos y":-954.319,"context":null,"url":null}')

-- cd5b17d6d964f0ac0f87e24488b668542474703f
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 17281.691, -954.315, 0.133, 0, 0, 0, '{"col pos x":17281.691,"col size x":0.648,"scale z":1,"col size y":0.652,"col orientation":0,"scale y":1,"col size z":0.935,"scale x":1,"col pos y":-954.315,"context":null,"url":null}')

-- 632bea2d0833e4ab5eb97a2e64a6f699f97962c6
SceneEditor:spawnShape("gen_mission_tonneau_broke.shape", 17282.615, -952.324, -0.015, 0, 0, 0, '{"col pos x":17282.615,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-952.324,"context":null,"url":null}')

-- 411671e1a378efc26c8cecd521ed9d2a3d2e290b
SceneEditor:spawnShape("gen_bt_totem.shape", 17305.869, -951.7, -0.187, 0, 0, -0.86, '{"col pos x":17305.869,"col size x":0.575,"scale z":2.38,"col size y":0.348,"col orientation":0,"scale y":2.38,"col size z":4.62,"scale x":2.38,"col pos y":-951.7,"context":null,"url":null}')

-- a7c538f14c30c063456d41891844ba3fe471abba
SceneEditor:spawnShape("gen_bt_totem.shape", 17295.824, -937.042, -0.101, 0, 0, -0.96, '{"col pos x":17295.824,"col size x":0.505,"scale z":2.3,"col size y":0.268,"col orientation":0,"scale y":2.3,"col size z":4.62,"scale x":2.3,"col pos y":-937.042,"context":null,"url":null}')

-- d2d67ba0eb354eaaf719b0a0803c28b43d0fb915
SceneEditor:spawnShape("gen_bt_atelier_sac.shape", 17326.537, -930.887, -2.164, 0, 0, 2.11, '{"col pos x":17326.537,"col size x":2.577,"scale z":1.66,"col size y":2.813,"col orientation":0,"scale y":1.66,"col size z":0.671,"scale x":1.66,"col pos y":-930.887,"context":null,"url":null}')

-- 49cb211cc037a0e1b3539205739e40460e60d70e
SceneEditor:spawnShape("gen_aub_int_tonneaux_b.shape", 17296.16, -945.359, -0.041, 0, 0, 0, '{"col pos x":17296.16,"col size x":1.861,"scale z":1,"col size y":1.656,"col orientation":0,"scale y":1,"col size z":1.154,"scale x":1,"col pos y":-945.359,"context":null,"url":null}')

-- 2a0ec5f9add6629527ae72d364e50c8770ecabc2
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17281.52, -952.303, -0.03, 0, 0, 0, '{"col pos x":17281.52,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-952.303,"context":null,"url":null}')

-- 60aca5c6c42b11a94857398de64ee7988cbe18fe
SceneEditor:spawnShape("gen_mission_3_tonneaux.shape", 17281.602, -953.413, -0.029, 0, 0, 0, '{"col pos x":17281.602,"col size x":2.141,"scale z":1,"col size y":1.168,"col orientation":0,"scale y":1,"col size z":2.481,"scale x":1,"col pos y":-953.413,"context":null,"url":null}')

-- 5deaa9bfc785eb8723fe5beac7545e8c50040232
SceneEditor:spawnShape("ge_partylamp_02.shape", 17300.568, -888.06, -4.146, 0, 0, 0.31, '{"col pos x":17293.408,"col size x":5.756,"scale z":1.55,"col size y":0.743,"col orientation":0.32,"scale y":1.55,"col size z":5.18,"scale x":1.55,"col pos y":-890.207,"context":null,"url":null}')

-- 3dd13739e371b6f394ef9f37b1be09229e138c4e
SceneEditor:spawnShape("ge_partylamp_02.shape", 17287.115, -893.691, -0.854, 0, 0, 0.46, '{"col pos x":17280.455,"col size x":5.696,"scale z":1.56,"col size y":0.743,"col orientation":0.47,"scale y":1.56,"col size z":5.18,"scale x":1.56,"col pos y":-897.024,"context":null,"url":null}')

-- 3b11e0f807f05162010d16cfd7479c23cfe90c05
SceneEditor:spawnShape("ge_partylamp_02.shape", 17332.283, -955.882, -0.999, 0, 0, 1.23, '{"col pos x":17334.412,"col size x":5.335,"scale z":1.43,"col size y":0.743,"col orientation":1.26,"scale y":1.43,"col size z":5.18,"scale x":1.43,"col pos y":-949.24,"context":null,"url":null}')

-- 891fdc2f62b4ee7a217e0675c4a0000a658cc493
SceneEditor:spawnShape("ge_partylamp_02.shape", 17336.902, -942.738, -2.251, 0, 0, 4.33, '{"col pos x":17339.086,"col size x":2.856,"scale z":1.48,"col size y":0.743,"col orientation":1.18,"scale y":1.48,"col size z":5.18,"scale x":1.48,"col pos y":-937.418,"context":null,"url":null}')

-- 2f8fbd5c4add32555b5fff19c5d9e456d608a931
SceneEditor:spawnShape("ge_mission_tente_zo.shape", 17279.551, -926.647, -0.024, 0, 0, -2.74, '{"col pos x":17279.551,"col size x":10.612,"scale z":1.6,"col size y":10.612,"col orientation":0.45,"scale y":1.6,"col size z":5.994,"scale x":1.6,"col pos y":-926.647,"context":null,"url":null}')

-- 19632aef7d83fc785b7da9e23cea3034e9549515
SceneEditor:spawnShape("ge_mission_puit.shape", 17325.031, -943.366, -1.829, 0, 0, 4.88, '{"col pos x":17325.031,"col size x":3.331,"scale z":1,"col size y":8.42,"col orientation":1.71,"scale y":1,"col size z":4.876,"scale x":1,"col pos y":-943.366,"context":null,"url":null}')

-- eb24a5d7efdd21986c24eb10acc2997f9deba158
SceneEditor:spawnShape("ge_mission_sac_b.shape", 17296.402, -947.493, -0.02, 0, 0, 0, '{"col pos x":17296.402,"col size x":1.7,"scale z":1,"col size y":1.62,"col orientation":0,"scale y":1,"col size z":0.968,"scale x":1,"col pos y":-947.493,"context":null,"url":null}')

-- 7c244c1023acc40ae0eb3fa89eee4a3dc8130e30
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 17301.756, -947.206, -0.153, 0, 0, 0, '{"col pos x":17301.756,"col size x":2.339,"scale z":1,"col size y":2.837,"col orientation":0,"scale y":1,"col size z":2.556,"scale x":1,"col pos y":-947.206,"context":null,"url":null}')

-- f3650e7f7da5bec7921240a959dcee916922ada9
SceneEditor:spawnShape("ge_mission_enclos.shape", 17327.887, -931.119, -2.147, 0, 0, -4.99, '{"col pos x":17327.887,"col size x":7.201,"scale z":1,"col size y":7.073,"col orientation":1.32,"scale y":1,"col size z":6.629,"scale x":1,"col pos y":-931.119,"context":null,"url":null}')

-- 6325c6da9eb4514ae5a5a1e350cc07d2ac83d311
SceneEditor:spawnShape("ge_mission_charette.shape", 17298.99, -946.488, -0.115, 0, 0, -2.92, '{"col pos x":17298.99,"col size x":2.505,"scale z":1.029,"col size y":5.184,"col orientation":0,"scale y":1.029,"col size z":2.592,"scale x":1.029,"col pos y":-946.488,"context":null,"url":null}')

-- b1bc54a7679bcfa5d934e9f8a73a3ac92ce15548
SceneEditor:spawnShape("Ge_Mission_Stand.shape", 17297.43, -944.646, -0.09, 0, 0, -0.74, '{"col pos x":17297.43,"col size x":4.258,"scale z":0.864,"col size y":4.775,"col orientation":0,"scale y":1.158,"col size z":6.313,"scale x":1.308,"col pos y":-944.646,"context":null,"url":null}')

-- 5ec6a04541328f3296ab0eec8df5fbfd26637959
SceneEditor:spawnShape("gen_bt_totem.shape", 17358.572, -880.689, -4.683, 0, 0, 0, '{"col pos x":17358.516,"col size x":0.775,"scale z":1,"col size y":1.068,"col orientation":0,"scale y":1,"col size z":4.62,"scale x":1,"col pos y":-881.163,"context":null,"url":null}')

-- 6875313414a4c592b4906698de2ceacae9482644
SceneEditor:spawnShape("gen_bt_totem.shape", 17366.438, -880.511, -4.817, 0, 0, 0, '{"col pos x":17366.311,"col size x":0.855,"scale z":1,"col size y":1.318,"col orientation":0,"scale y":1,"col size z":4.62,"scale x":1,"col pos y":-880.996,"context":null,"url":null}')

-- ab371ff9bdfce8e9129f09a2e652ba8d23803761
SceneEditor:spawnShape("gen_bt_totem.shape", 17366.66, -895.652, -4.851, 0, 0, 0, '{"col pos x":17366.629,"col size x":1.005,"scale z":1,"col size y":1.038,"col orientation":0,"scale y":1,"col size z":4.62,"scale x":1,"col pos y":-895.226,"context":null,"url":null}')

-- 92d6384acf57d6699b34458339e8cb91e6a0baa6
SceneEditor:spawnShape("gen_bt_totem.shape", 17358.818, -895.768, -4.77, 0, 0, 0, '{"col pos x":17358.762,"col size x":0.915,"scale z":1,"col size y":1.218,"col orientation":0,"scale y":1,"col size z":4.62,"scale x":1,"col pos y":-895.259,"context":null,"url":null}')

-- ddc6e05da7444dd63f06b258ced30d8733beb66b
SceneEditor:spawnShape("tr_arche_village_a.shape", 17362.578, -888.216, -4.759, 0, 0, 1.59, '{"col pos x":0,"col size x":10.418,"scale z":1.45,"col size y":7.598,"col orientation":0,"scale y":1.45,"col size z":14.364,"scale x":1.45,"col pos y":-888.216,"context":null,"url":null}')

-- d215d76001050ebef736b8e93f21ee3d132e9ea4
SceneEditor:spawnShape("gigabossrendorsilan1.shape", 17376.387, -897.514, -3.776, -0.35, -0.01, -0.94, '{"col pos x":-378.035,"col size x":2.026,"scale z":0.59,"col size y":0.219,"col orientation":0,"scale y":0.59,"col size z":3.045,"scale x":0.59,"col pos y":-903.514,"context":null,"url":null}')

-- 439bbfd0cf9f1cab11ca32bbd93848a5c1953112
SceneEditor:spawnShape("ge_gift_blue.shape", 17378.258, -896.972, -3.434, 0, 0, -0.74, '{"col pos x":-376.164,"col size x":0.704,"scale z":1,"col size y":0.704,"col orientation":0,"scale y":1,"col size z":0.943,"scale x":1,"col pos y":-896.972,"context":null,"url":null}')

-- 67d2992c372ff531afc6ec2a4d5779bee69341d4
SceneEditor:spawnShape("ge_gift_red.shape", 17378.664, -895.522, -4.604, 0, 0, 0.28, '{"col pos x":-375.758,"col size x":0.704,"scale z":1,"col size y":0.704,"col orientation":0,"scale y":1,"col size z":0.943,"scale x":1,"col pos y":-889.522,"context":null,"url":null}')

-- a563dbba81e87c3582a516ccedf08d2aa9081332
SceneEditor:spawnShape("ge_gift_yellow.shape", 17378.295, -897.978, -4.514, 0, 0, -0.01, '{"col pos x":-370.127,"col size x":0.704,"scale z":1,"col size y":0.704,"col orientation":0,"scale y":1,"col size z":0.943,"scale x":1,"col pos y":-933.978,"context":null,"url":null}')

-- 79eb4949645226b0d2be7b9a1529eb1743218de5
SceneEditor:spawnShape("ge_picture_bodoc.shape", 17377.402, -897.718, -3.858, -0.25, -0.03, 0.51, '{"col pos x":-371.02,"col size x":0.711,"scale z":1.89,"col size y":0.026,"col orientation":0,"scale y":1.89,"col size z":0.513,"scale x":1.89,"col pos y":-927.718,"context":null,"url":null}')

-- b42b0ac96542c29df787e660513414cc3b759c36
SceneEditor:spawnShape("ge_mission_fortuna_wheel_ring.shape", 17375.133, -895.692, -4.114, -0.3, -0.93, 0, '{"col pos x":0,"col size x":1.447,"scale z":1,"col size y":1.439,"col orientation":0,"scale y":1,"col size z":0.1,"scale x":1,"col pos y":-895.692,"context":null,"url":null}')

-- 11f2fe77040fd24028a5a3628587cc82ed10d90b
SceneEditor:spawnShape("ge_mission_fortuna_wheel_base.shape", 17375.723, -895.855, -4.736, 0, 0, 0.99, '{"col pos x":-372.699,"col size x":1.702,"scale z":1,"col size y":0.96,"col orientation":0,"scale y":1,"col size z":2,"scale x":1,"col pos y":-907.855,"context":null,"url":null}')

-- 261b76f18ca8f03df3d3ec9d0af78d9a4f47ca63
SceneEditor:spawnShape("fy_s2_savantree_c.shape", 17379.916, -894.336, -4.461, 0, 0, 0, '{"col pos x":17380.008,"col size x":2.077,"scale z":1,"col size y":1.81,"col orientation":0,"scale y":1,"col size z":10.591,"scale x":1,"col pos y":-894.927,"context":null,"url":null}')

-- e11fbeb0394eaca7a44b262948dbd4cb082d278b
SceneEditor:spawnShape("ge_mission_objet_pack_3.shape", 17376.959, -896.583, -4.686, 0, 0, -1.3, '{"col pos x":17376.959,"col size x":4.1,"scale z":1,"col size y":3.431,"col orientation":0,"scale y":1,"col size z":2.313,"scale x":1,"col pos y":-896.583,"context":null,"url":null}')

-- ceae0578d3e47008df5e500ac519f4d5943215d5
SceneEditor:spawnShape("fireworkk.ps", 17378.348, -892.713, -4.613, 0, 0, 0, '{"col pos x":-370.074,"col size x":2,"scale z":1,"col size y":2,"col orientation":0,"scale y":1,"col size z":2,"scale x":1,"col pos y":-886.713,"context":null,"url":null}')

-- 510e4eb0699a0394e30e8144807aa2bbb8d25807
SceneEditor:spawnShape("fireworkk.ps", 17376.225, -894.831, -4.725, 0, 0, 0, '{"col pos x":0,"col size x":2.32,"scale z":1,"col size y":2.32,"col orientation":2.894,"scale y":1,"col size z":2,"scale x":1,"col pos y":-894.831,"context":null,"url":null}')

-- 21acba7224fbfa6ca5b0065f062c9fee66cc5b3f
SceneEditor:spawnShape("ge_mission_comptoir.shape", 17376.727, -893.186, -4.708, 0, 0, 0.81, '{"col pos x":17376.727,"col size x":3.059,"scale z":1,"col size y":1.334,"col orientation":0.81,"scale y":1,"col size z":1.412,"scale x":1,"col pos y":-893.186,"context":null,"url":null}')

-- 15aa37d273759212d1f19b2917096acdff458bd2
SceneEditor:spawnShape("fireworkq.ps", 17355.451, -880.668, -4.737, 0, 0, 0, '{"col pos x":0,"col size x":2,"scale z":1,"col size y":2,"col orientation":0,"scale y":1,"col size z":2,"scale x":1,"col pos y":-880.668,"context":null,"url":null}')

-- 456f84b34766f060ef8919f1da3aa712452a9190
SceneEditor:spawnShape("ge_partylamp_01.shape", 17393.469, -913.907, -0.878, 0, 0, 3.2, '{"col pos x":17395.004,"col size x":7.031,"scale z":1.67,"col size y":0.673,"col orientation":0.08,"scale y":1.67,"col size z":5.18,"scale x":1.67,"col pos y":-913.51,"context":null,"url":null}')

-- 58f1a0bf9085617bb64f7df9e5f10ebf9b1e3974
SceneEditor:spawnShape("ge_partylamp_01.shape", 17389.514, -914.028, -1.298, 0, 0, 0.07, '{"col pos x":17387.936,"col size x":5.751,"scale z":1.44,"col size y":0.743,"col orientation":0.06,"scale y":1.44,"col size z":5.18,"scale x":1.44,"col pos y":-914.065,"context":null,"url":null}')

-- a18090e531ab50e8931ea4d9549da9db536fb234
SceneEditor:spawnShape("ge_partylamp_01.shape", 17391.898, -860.068, -0.02, 0, 0, 3.04, '{"col pos x":17393.387,"col size x":4.761,"scale z":1.33,"col size y":0.743,"col orientation":3.11,"scale y":1.33,"col size z":5.18,"scale x":1.33,"col pos y":-860.167,"context":null,"url":null}')

-- 32e4229997cc555e7243b5e0929fea3e2315f11a
SceneEditor:spawnShape("ge_partylamp_01.shape", 17389.291, -859.925, -0.831, 0, 0, -0.13, '{"col pos x":17389.291,"col size x":1.861,"scale z":1.4,"col size y":0.743,"col orientation":0,"scale y":1.4,"col size z":5.18,"scale x":1.4,"col pos y":-859.925,"context":null,"url":null}')

-- 4e3b8545eb9f7a98cd9b6b807ac5e1ed59316a5e
SceneEditor:spawnShape("ge_partylamp_01.shape", 17387.002, -860.193, -2.094, 0, 0, 3.03, '{"col pos x":17387.002,"col size x":1.861,"scale z":1.45,"col size y":0.743,"col orientation":0,"scale y":1.45,"col size z":5.18,"scale x":1.45,"col pos y":-860.193,"context":null,"url":null}')

-- 76826df02257bba61ef0414dd99fa8a6d728a27a
SceneEditor:spawnShape("ge_partylamp_01.shape", 17384.207, -859.975, -3.371, 0, 0, -0.1, '{"col pos x":17382.906,"col size x":4.561,"scale z":1.43,"col size y":0.743,"col orientation":-0.06,"scale y":1.43,"col size z":5.18,"scale x":1.43,"col pos y":-859.809,"context":null,"url":null}')

-- f076ba5d888c0e01846113129bb08fc9e5657439
SceneEditor:spawnShape("ge_partylamp_02.shape", 17417.814, -912.355, -0.6, 0, 0, 0.06, '{"col pos x":17425.088,"col size x":5.376,"scale z":1.54,"col size y":0.743,"col orientation":0.06,"scale y":1.54,"col size z":5.18,"scale x":1.54,"col pos y":-911.867,"context":null,"url":null}')

-- 6acc8e575498878bd2b27761a707660ace4d3596
SceneEditor:spawnShape("ge_partylamp_02.shape", 17432.41, -911.385, -0.196, 0, 0, 0.07, '{"col pos x":17439.5,"col size x":5.496,"scale z":1.52,"col size y":0.743,"col orientation":0.09,"scale y":1.52,"col size z":5.18,"scale x":1.52,"col pos y":-910.817,"context":null,"url":null}')

-- 24fa82c6957d6bf2623f8c6433261897939a5765
SceneEditor:spawnShape("ge_partylamp_02.shape", 17329.256, -902.791, -4.105, 0, 0, -1.04, '{"col pos x":17333.09,"col size x":5.346,"scale z":1.54,"col size y":0.743,"col orientation":2.181,"scale y":1.54,"col size z":5.18,"scale x":1.54,"col pos y":-908.822,"context":null,"url":null}')

-- 4df7dc1fbbe694a68b5cf7c349b5f0aeabf56135
SceneEditor:spawnShape("ge_partylamp_02.shape", 17337.809, -914.23, -2.344, 0, 0, -0.85, '{"col pos x":17341.551,"col size x":2.236,"scale z":1.54,"col size y":0.633,"col orientation":2.31,"scale y":1.54,"col size z":5.18,"scale x":1.54,"col pos y":-918.581,"context":null,"url":null}')

-- 254fe943aab2a071752c476aec6db834cbba8396
SceneEditor:spawnShape("ge_partylamp_02.shape", 17428.836, -863.193, 1.119, 0, 0, -0.08, '{"col pos x":17436.262,"col size x":6.016,"scale z":1.5,"col size y":0.743,"col orientation":-0.08,"scale y":1.5,"col size z":5.18,"scale x":1.5,"col pos y":-863.623,"context":null,"url":null}')

-- e41bb56c84bac2e6faa5fcc99c2d31553f30edc2
SceneEditor:spawnShape("ge_partylamp_02.shape", 17414.662, -862.03, 1.162, 0, 0, -0.09, '{"col pos x":17421.76,"col size x":5.546,"scale z":1.49,"col size y":0.743,"col orientation":-0.08,"scale y":1.49,"col size z":5.18,"scale x":1.49,"col pos y":-862.634,"context":null,"url":null}')

-- 328df88ba74d45f8e5788f62da500e2c13861b81
SceneEditor:spawnShape("ge_partylamp_02.shape", 17399.969, -860.705, 1.015, 0, 0, -0.09, '{"col pos x":17407.348,"col size x":6.006,"scale z":1.49,"col size y":0.743,"col orientation":-0.07,"scale y":1.49,"col size z":5.18,"scale x":1.49,"col pos y":-861.308,"context":null,"url":null}')

-- 75f35f3dfff57c5b61c339b18f5d4d7d7e059d71
SceneEditor:spawnShape("ge_partylamp_02.shape", 17348.746, -848.539, -4.734, 0, 0, -2.78, '{"col pos x":17343.301,"col size x":2.976,"scale z":1.56,"col size y":0.603,"col orientation":3.491,"scale y":1.56,"col size z":5.18,"scale x":1.56,"col pos y":-850.512,"context":null,"url":null}')

-- f5946b9941274c27f09cb7272243e95b91a30fd5
SceneEditor:spawnShape("ge_partylamp_02.shape", 17376.027, -859.197, -4.751, 0, 0, -0.09, '{"col pos x":17370.314,"col size x":2.456,"scale z":1.51,"col size y":0.533,"col orientation":-0.09,"scale y":1.51,"col size z":5.18,"scale x":1.51,"col pos y":-858.663,"context":null,"url":null}')

-- fc0e44e8d0dbddc23e69390027b8835a52a7a850
SceneEditor:spawnShape("ge_partylamp_02.shape", 17366.059, -915.212, -2.54, 0, 0, 0.05, '{"col pos x":17360.277,"col size x":2.556,"scale z":1.55,"col size y":0.743,"col orientation":0.06,"scale y":1.55,"col size z":5.18,"scale x":1.55,"col pos y":-915.598,"context":null,"url":null}')

-- 0adfe5222a29593d9ce160cd2430949cc875dd4b
SceneEditor:spawnShape("ge_partylamp_02.shape", 17380.582, -914.476, -2.612, 0, 0, 0.05, '{"col pos x":17373.174,"col size x":5.766,"scale z":1.53,"col size y":0.743,"col orientation":0.06,"scale y":1.53,"col size z":5.18,"scale x":1.53,"col pos y":-914.821,"context":null,"url":null}')

-- 77d09a4dd345eefd3e7a5c914880ba22d20d23d7
SceneEditor:spawnShape("ge_partylamp_02.shape", 17403.199, -913.267, -0.891, 0, 0, 0.06, '{"col pos x":17410.438,"col size x":5.366,"scale z":1.54,"col size y":0.743,"col orientation":0.06,"scale y":1.54,"col size z":5.18,"scale x":1.54,"col pos y":-912.749,"context":null,"url":null}')

-- 8f2320863121dc9a3db527b5a9152ca40472296d
SceneEditor:spawnShape("ge_partylamp_02.shape", 17300.959, -944.135, -0.252, 0, 0, 2.17, '{"col pos x":17300.959,"col size x":14.726,"scale z":1.53,"col size y":0.743,"col orientation":2.17,"scale y":1.53,"col size z":5.18,"scale x":1.53,"col pos y":-944.135,"context":null,"url":null}')
--############### SE_deco_Entrance 12462#################

