--############### SE_DECO 12333#################
-- GROUP: plants_fyros
-- 085b0fa8075f8fa0d586193254c074095a4af95d
SceneEditor:spawnShape("fy_s1_baobab_a.shape", 17214.961, -693.74, -0.257, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17214.961,"col pos y":-693.291,"col orientation":0,"col size x":6.186,"col size y":6.384,"col size z":20.842,"context":null,"url":null}')

-- b1714d7310f193042167c59beec5e4937eebd8da
SceneEditor:spawnShape("fy_s1_burnedtree_b.shape", 17257.471, -680.623, 0.186, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17257.471,"col pos y":-680.623,"col orientation":0,"col size x":4.725,"col size y":4.964,"col size z":15.87,"context":null,"url":null}')

-- 62182867adb65347549be449ce2894df6f8e23e4
SceneEditor:spawnShape("fy_s2_lovejail_c.shape", 17238.875, -743.707, -0.216, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17238.875,"col pos y":-743.707,"col orientation":0,"col size x":3.167,"col size y":2.255,"col size z":5.141,"context":null,"url":null}')

-- 3d8166f19a14bcd5921a2a171684120f78a5d1ac
SceneEditor:spawnShape("fy_s2_savantree_c.shape", 17278.773, -702.387, -0.024, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17278.773,"col pos y":-703.025,"col orientation":0,"col size x":1.017,"col size y":1.33,"col size z":10.591,"context":null,"url":null}')

-- b047af5c06c779c7e79f7bdf7f4f31beeb54ca98
SceneEditor:spawnShape("fy_s2_savantree_c.shape", 17237.408, -655.519, 0.095, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17237.408,"col pos y":-656.119,"col orientation":0,"col size x":1.227,"col size y":1.31,"col size z":10.591,"context":null,"url":null}')

-- 2800cb5eacffc420016daf7db23f1e7f8c23da80
SceneEditor:spawnShape("fy_s2_lovejail_b.shape", 17276.949, -651.021, -0.03, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17276.949,"col pos y":-651.021,"col orientation":0,"col size x":2.304,"col size y":2.444,"col size z":3.891,"context":null,"url":null}')

-- 8addebb42b23cedeed41c0fc567d84576b933af6
SceneEditor:spawnShape("fy_s2_lovejail_b.shape", 17242.006, -674.333, 0.47, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17242.006,"col pos y":-674.333,"col orientation":0,"col size x":2.304,"col size y":2.444,"col size z":3.891,"context":null,"url":null}')

-- 41c3763d5f81dcee18278be876e908b87111287a
SceneEditor:spawnShape("fy_s2_lovejail_b.shape", 17186.334, -692.155, 0.814, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17186.334,"col pos y":-692.155,"col orientation":0,"col size x":2.304,"col size y":2.444,"col size z":3.891,"context":null,"url":null}')
--############### SE_DECO 12333#################

