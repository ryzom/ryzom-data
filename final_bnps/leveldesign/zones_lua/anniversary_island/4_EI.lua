--############### SE_DECO 12333#################
-- GROUP: buildings_matis
-- 60c636ff27357a07ccfcafccd769dfe366f3b3c9
SceneEditor:spawnShape("ma_annexe_ext_4_village_a.shape", 18047.252, -586.984, 6.7, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18047.252,"col pos y":-586.984,"col orientation":0.05,"col size x":17.91,"col size y":27.365,"col size z":20.841,"context":null,"url":null}')

-- 3fceaf4d569fdee9bf9d609ab3ff5d4daeb85073
SceneEditor:spawnShape("ma_annexe_ext_4_village_a.shape", 18014.873, -604.154, 6.9, 0, 0, 0.97, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18014.873,"col pos y":-604.154,"col orientation":0.95,"col size x":17.889,"col size y":27.365,"col size z":20.841,"context":null,"url":null}')

-- 529e2a198dee9a983b943c3948ee4cc6a9f149f7
SceneEditor:spawnShape("ma_imm_ext-nb_01.shape", 18002.623, -632.397, 0, 0, 0, 4.69, '{"scale x":0.74,"scale y":0.74,"scale z":0.74,"col pos x":18002.623,"col pos y":-632.397,"col orientation":3.431,"col size x":15.552,"col size y":18.215,"col size z":69.87,"context":null,"url":null}')

-- f90329fa4ca14d2b359599dfba8be4076dbcb57f
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18074.043, -616.673, 0.041, 0, 0, 0.48, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18073.637,"col pos y":-616.673,"col orientation":0.31,"col size x":0.762,"col size y":1.426,"col size z":7.134,"context":null,"url":null}')

-- bcf36a9d6494c4acff8f271bb268ac6b15b87f68
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18065.215, -618.16, 0.417, 0, 0, 0.22, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18065.215,"col pos y":-618.16,"col orientation":0.11,"col size x":1.232,"col size y":1.086,"col size z":7.134,"context":null,"url":null}')

-- d7f0ecb861aeef69888afb219e06b877e2b4b85f
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18013.545, -624.803, 0.443, 0, 0, 0.39, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18013.545,"col pos y":-624.803,"col orientation":0,"col size x":1.232,"col size y":1.136,"col size z":7.134,"context":null,"url":null}')

-- b5ae6a2c92dd67f5f99fadd8939422f1d91c0b40
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18051.281, -606.307, 1.266, 0, 0, -1.55, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18051.281,"col pos y":-606.307,"col orientation":0,"col size x":1.232,"col size y":1.456,"col size z":7.134,"context":null,"url":null}')

-- 8a0cd14368cb65a28b9eb67a8be9a1c3871aeec4
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18076.361, -625.555, 0.02, 0, 0, -3.04, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18076.361,"col pos y":-625.555,"col orientation":0,"col size x":1.232,"col size y":0.956,"col size z":7.134,"context":null,"url":null}')

-- b928b0deca1fbc19c824f59a326e0fc179a689bf
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18067.551, -628.862, 0.091, 0, 0, -2.73, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18067.551,"col pos y":-628.862,"col orientation":0,"col size x":1.232,"col size y":1.006,"col size z":7.134,"context":null,"url":null}')

-- 119c9db6aee775f29aa9d432ec502ea460bf1c62
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18060.604, -637.226, 0.033, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18060.604,"col pos y":-637.226,"col orientation":0,"col size x":1.232,"col size y":3.556,"col size z":7.134,"context":null,"url":null}')

-- 2dfeabbeefa213a577c457730d5f6bc135681dba
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18043.299, -606.036, 1.306, 0, 0, 1.59, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18043.299,"col pos y":-606.036,"col orientation":0,"col size x":1.232,"col size y":1.296,"col size z":7.134,"context":null,"url":null}')

-- db8e2bba21d987f33c2dfe611c4bb45e4e3b4632
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18037.559, -609.443, 1.197, 0, 0, 0.46, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18037.559,"col pos y":-609.443,"col orientation":0,"col size x":1.232,"col size y":1.586,"col size z":7.134,"context":null,"url":null}')

-- dee2e64f434193c45191788c58c89520a562f663
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18034.285, -611.906, 1.118, 0, 0, -0.62, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18034.285,"col pos y":-611.906,"col orientation":0,"col size x":1.232,"col size y":1.166,"col size z":7.134,"context":null,"url":null}')

-- b7a7bad8d938cc506fdcb3a46517b7b65042beea
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18027.488, -619.029, 0.738, 0, 0, 2.42, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18027.488,"col pos y":-619.029,"col orientation":0,"col size x":1.232,"col size y":1.316,"col size z":7.134,"context":null,"url":null}')

-- a19586787cf5f238bb772e00c9ad94720ba32af4
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18015.818, -633.593, 0.05, 0, 0, -2.6, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18015.818,"col pos y":-633.593,"col orientation":0,"col size x":1.232,"col size y":1.286,"col size z":7.134,"context":null,"url":null}')

-- 8ad468856e53f284a46212677c835285948915d7
SceneEditor:spawnShape("ge_mission_stand.shape", 18042.557, -615.709, 1.068, 0, 0, 0, '{"scale x":1.088,"scale y":0.938,"scale z":0.644,"col pos x":18042.557,"col pos y":-615.709,"col orientation":0,"col size x":4.258,"col size y":4.775,"col size z":6.313,"context":null,"url":null}')

-- 3d7395a053b3b277dd6b4cf8bac3655cda3baf78
SceneEditor:spawnShape("ge_mission_stand.shape", 18054.111, -637.239, 0.052, 0, 0, -2.44, '{"scale x":1.088,"scale y":0.938,"scale z":0.644,"col pos x":18054.111,"col pos y":-637.239,"col orientation":0,"col size x":4.258,"col size y":4.775,"col size z":6.313,"context":null,"url":null}')

-- 7307bf4acd5c7270a21327470ad322e5d78686d6
SceneEditor:spawnShape("ge_mission_stand.shape", 18056.729, -614.638, 0.935, 0, 0, -0.72, '{"scale x":1.088,"scale y":0.938,"scale z":0.644,"col pos x":18056.729,"col pos y":-614.638,"col orientation":-0.75,"col size x":4.258,"col size y":4.775,"col size z":6.313,"context":null,"url":null}')


-- GROUP: plants_matis
-- d294a1da83dc9f180f2651e7a785a48915dbb814
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 17969.881, -620.968, -0.765, 0, 0, 0, '{"col size x":5.054,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.602,"col size z":12.685,"scale y":1,"col pos y":-620.968,"col pos x":17969.881,"context":null,"url":null}')

-- f582e418ce699b56b2d535c0d7d7a27a1fef1b47
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 17984.951, -581.89, -0.758, 0, 0, 0, '{"col size x":21.481,"scale x":1,"col orientation":0,"scale z":0.74,"col size y":16.352,"col size z":66.446,"scale y":1,"col pos y":-581.89,"col pos x":17984.951,"context":null,"url":null}')

-- 002ae5b1dca0107bbef3b53f73366365e16f199b
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 17934.211, -628.995, -0.183, 0, 0, 0, '{"col size x":5.131,"scale x":1,"col orientation":0,"scale z":0.84,"col size y":5.902,"col size z":66.446,"scale y":1,"col pos y":-628.995,"col pos x":17934.211,"context":null,"url":null}')

-- ef3eb0ca140f3ad1909e1ba4243cf41d21e46643
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17922.314, -605.057, -0.017, 0, 0, 0, '{"col size x":1.647,"scale x":1,"col orientation":0,"scale z":1.65,"col size y":1.258,"col size z":13.303,"scale y":1,"col pos y":-605.057,"col pos x":17922.314,"context":null,"url":null}')

-- 170a649cb32ba12de097de7612aca85ca09b83dc
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17935.607, -604.149, -0.777, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-604.149,"col pos x":17935.607,"context":null,"url":null}')

-- 9433c1fd01f23fbc3e7356625bd246269a62fac2
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17953.529, -579.416, -0.888, 0, 0, 0, '{"col size x":1.447,"scale x":1,"col orientation":0,"scale z":2.24,"col size y":1.058,"col size z":13.303,"scale y":1,"col pos y":-579.416,"col pos x":17953.529,"context":null,"url":null}')

-- 266c269085d74b6910aac36acc52651bad585455
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17929.971, -583.41, -0.273, 0, 0, 0, '{"col size x":1.587,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.198,"col size z":13.303,"scale y":1,"col pos y":-583.41,"col pos x":17929.971,"context":null,"url":null}')

-- f699d6bfed32e3fa0bf4c55334a5e3b01843ce7b
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17923.848, -558.443, 0.024, 0, 0, 0, '{"col size x":1.727,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.338,"col size z":13.303,"scale y":1,"col pos y":-558.443,"col pos x":17923.848,"context":null,"url":null}')

-- aec6ecf1756557b81eb8bae1ca32eaa72def56e4
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17943.352, -565.266, -0.205, 0, 0, 0, '{"col size x":1.607,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.218,"col size z":13.303,"scale y":1,"col pos y":-565.266,"col pos x":17943.352,"context":null,"url":null}')

-- 548c3cdb15595ce47b9464d45d5e37ffcc7edd52
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17947.314, -552.356, 0.969, 0, 0, 0, '{"col size x":1.537,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.148,"col size z":13.303,"scale y":1,"col pos y":-552.356,"col pos x":17947.314,"context":null,"url":null}')

-- 9737223e848d788ef013d3a81c9396fa0db5b10c
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17964.186, -562.245, -0.142, 0, 0, 0, '{"col size x":2.027,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.638,"col size z":13.303,"scale y":1,"col pos y":-562.245,"col pos x":17964.186,"context":null,"url":null}')

-- fabd6bcd9bf39b2c8ca87aa7a2d3a2390391b117
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17962.1, -580.498, -0.863, 0, 0, 0, '{"col size x":1.717,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.328,"col size z":13.303,"scale y":1,"col pos y":-580.498,"col pos x":17962.1,"context":null,"url":null}')

-- cf59f3cf88416daec6030cd0ef8e26dceb941207
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17968.066, -591.479, -0.937, 0, 0, 0, '{"col size x":2.127,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.738,"col size z":13.303,"scale y":1,"col pos y":-591.479,"col pos x":17968.066,"context":null,"url":null}')

-- 72aeec1e7ed637616e4e6d12ab86ae4c679c561c
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17961.797, -605.866, -1.5, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-605.866,"col pos x":17961.797,"context":null,"url":null}')

-- 831ce9124b039696b3d94b4635fe7cf6f2c4434a
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17952.617, -617.598, -1.388, 0, 0, 0, '{"col size x":2.647,"scale x":1,"col orientation":0,"scale z":1,"col size y":2.258,"col size z":11.403,"scale y":1,"col pos y":-617.598,"col pos x":17952.617,"context":null,"url":null}')

-- bbc8304935dc44c45ba0c8a6491b88405f47ae86
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17943.188, -608.115, -1.398, 0, 0, 0, '{"col size x":1.617,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.228,"col size z":13.303,"scale y":1,"col pos y":-608.115,"col pos x":17943.188,"context":null,"url":null}')

-- a0024df8d5ce9662cd29f2ff4aac5efda062b619
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17930.969, -619.024, -0.379, 0, 0, 0, '{"col size x":1.877,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.488,"col size z":13.303,"scale y":1,"col pos y":-619.024,"col pos x":17930.969,"context":null,"url":null}')

-- 5db32ea8f8027832a3a581e34c2100f82f9f8517
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17925.09, -636.601, 0.044, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-636.601,"col pos x":17925.09,"context":null,"url":null}')

-- c0679f6217489242abba166b6355a74c991f1414
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17939.594, -639.163, 0.005, 0, 0, 0, '{"col size x":1.617,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.228,"col size z":13.303,"scale y":1,"col pos y":-639.163,"col pos x":17939.594,"context":null,"url":null}')
--############### SE_DECO 12333#################

