--############### SE_DECO 12333#################
-- GROUP: buildings_kami
-- 5371fbfd28d6389271032c9bff51b0937e3276fb
SceneEditor:spawnShape("ge_mission_cercle_runique.shape", 17788.576, -1128.416, -0.239, 0, 0, 0, '{"scale x":1.3,"scale y":1.3,"scale z":1.3,"col pos x":17788.576,"col pos y":-1128.416,"col orientation":0,"col size x":19.857,"col size y":18.947,"col size z":10.413,"context":null,"url":null}')

-- 952212e4fd28cf3cb470b2ac0f24962bf01d9b2d
SceneEditor:spawnShape("ge_mission_cercle_runique.ps", 17788.576, -1128.416, -0.152, 0, 0, 0, '{"scale x":1.5,"scale y":1.5,"scale z":1.5,"col pos x":17788.576,"col pos y":-1128.416,"col orientation":0,"col size x":16.956,"col size y":16.998,"col size z":16.989,"context":null,"url":null}')

-- 6ff5ffb9842db90c2e3f14c7f4dae21f7dcf0c90
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17770.473, -1154.883, -0.209, 0, 0, 2.05, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17770.473,"col pos y":-1154.883,"col orientation":0,"col size x":7.821,"col size y":5,"col size z":7.182,"context":null,"url":null}')

-- 5d9f0397882391976cc83286f9ab91ff3c36bde8
SceneEditor:spawnShape("ge_mission_barriere_kami.ps", 17775.061, -1180.193, 0.005, 0, 0, 1.58, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17775.061,"col pos y":-1180.193,"col orientation":3.141,"col size x":7.821,"col size y":5,"col size z":7.182,"context":null,"url":null}')

-- 1cb86c67145ade68eb0422bfec9a2aa57a57b08c
SceneEditor:spawnShape("ge_mission_totem_kamik.shape", 17803.391, -1120.314, 0.015, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17803.391,"col pos y":-1120.314,"col orientation":0,"col size x":4.132,"col size y":2.62,"col size z":13.184,"context":null,"url":null}')


-- GROUP: plants_zorai
-- 3dbe363a84c32f6ad9d8678c2e72fc16074a870c
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17919.273, -1147.443, 0.007, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17919.273,"col pos y":-1147.443,"col orientation":0,"col size x":10.921,"col size y":13.482,"col size z":16.447,"context":null,"url":null}')

-- 9a7ed9b28461dc57a0262cc5bdcd8332862dd756
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17898.707, -1124.962, -0.125, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17898.707,"col pos y":-1124.962,"col orientation":0,"col size x":0.801,"col size y":0.692,"col size z":16.447,"context":null,"url":null}')
--############### SE_DECO 12333#################

