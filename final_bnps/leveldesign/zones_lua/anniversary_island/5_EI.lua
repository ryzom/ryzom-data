--############### SE_DECO 12333#################
-- GROUP: buildings_matis
-- 08d2f629bb8c23617d46ffadb7d6ee482bbd51f0
SceneEditor:spawnShape("ma_portes_ma_portes_village_a.shape", 18004.324, -675.633, 7, 0, 0, -0.72, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18009.973,"col pos y":-680.625,"col orientation":-0.85,"col size x":6.129,"col size y":6.332,"col size z":26.012,"context":null,"url":null}')

-- 9d0a010466950a3068fe0d5db80deffd96a95896
SceneEditor:spawnShape("ma_portes_ma_portes_village_a.shape", 18014.689, -661.567, -20, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17998.697,"col pos y":-670.714,"col orientation":-0.64,"col size x":6.309,"col size y":4.342,"col size z":26.012,"context":null,"url":null}')

-- 52eeca2ec5cfaabb750347e4ecac7bd71a5f1b64
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18068.129, -653.005, -0.219, 0, 0, -0.36, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18068.129,"col pos y":-653.005,"col orientation":0,"col size x":1.232,"col size y":1.166,"col size z":7.134,"context":null,"url":null}')

-- f2346ab51dad371e36715708a295374540b81cc8
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18064.594, -662.357, -0.544, 0, 0, -3.46, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18064.594,"col pos y":-662.357,"col orientation":0,"col size x":1.232,"col size y":1.056,"col size z":7.134,"context":null,"url":null}')

-- f76d5a97689763e82cf5f1db1c1533221433fca8
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18049.316, -669.736, -0.998, 0, 0, -3.1, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18049.316,"col pos y":-669.736,"col orientation":0,"col size x":1.232,"col size y":1.086,"col size z":7.134,"context":null,"url":null}')

-- afc4710499185f668cfdbbada1a2d2a0b959c15a
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18011.439, -658.344, -2.106, 0, 0, 0.74, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18011.439,"col pos y":-658.344,"col orientation":0,"col size x":1.232,"col size y":1.056,"col size z":7.134,"context":null,"url":null}')

-- eabde9242a485b81b8a68536b372c70efe618a14
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18022.584, -673.243, -2.436, 0, 0, -2.2, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18022.584,"col pos y":-673.243,"col orientation":0,"col size x":1.232,"col size y":1.266,"col size z":7.134,"context":null,"url":null}')

-- e0651d31ca2d171a05ba16312de3881544800563
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18020.352, -643.343, -0.091, 0, 0, 1.03, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18020.352,"col pos y":-643.343,"col orientation":0,"col size x":1.232,"col size y":0.966,"col size z":7.134,"context":null,"url":null}')

-- 77f2b62b1bf3b3065a8bf254ae72637255cbc54a
SceneEditor:spawnShape("ma_lampadaire_white.shape", 18034.898, -669.961, -1.989, 0, 0, -2.83, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18034.898,"col pos y":-669.961,"col orientation":3.141,"col size x":1.232,"col size y":1.256,"col size z":7.134,"context":null,"url":null}')


-- GROUP: plants_matis
-- fc40991c2a4c3a297f69690ad0efc70617d88a40
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 18028.787, -749.213, -1.401, 0, 0, 0, '{"col size x":5.054,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.602,"col size z":12.685,"scale y":1,"col pos y":-749.213,"col pos x":18028.787,"context":null,"url":null}')

-- c8161d7d8f79ca2d947207d9517dce59d7c886a2
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 17953.885, -774.228, -1.861, 0, 0, 0, '{"col size x":5.054,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.602,"col size z":12.685,"scale y":1,"col pos y":-774.228,"col pos x":17953.885,"context":null,"url":null}')

-- 830d09cd208af06feb877aac45da3046681a2516
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 17921.055, -692.608, 0.012, 0, 0, 0, '{"col size x":5.054,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.602,"col size z":12.685,"scale y":1,"col pos y":-692.608,"col pos x":17921.055,"context":null,"url":null}')

-- ee00dee0b19ed46efaf1236af4b2ec9f6970165d
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 17976.178, -652.436, -0.384, 0, 0, 0, '{"col size x":3.371,"scale x":1,"col orientation":0,"scale z":1,"col size y":3.422,"col size z":66.446,"scale y":1,"col pos y":-652.436,"col pos x":17976.178,"context":null,"url":null}')

-- 2d518c4d01ac8b256264a7c9dae7318a014dfbec
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 17968.881, -687.686, -2.802, 0, 0, 0, '{"col size x":4.391,"scale x":1,"col orientation":0,"scale z":0.93,"col size y":4.882,"col size z":66.446,"scale y":1,"col pos y":-687.686,"col pos x":17968.881,"context":null,"url":null}')

-- 95c2616f06613db806d4300312f0983d36a46a3d
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 18046.525, -757.074, 0.472, 0, 0, 0, '{"col size x":3.741,"scale x":1,"col orientation":0,"scale z":1,"col size y":4.912,"col size z":66.446,"scale y":1,"col pos y":-757.074,"col pos x":18046.525,"context":null,"url":null}')

-- 54e254c1aa6f287830666efcb7dce048bee4cdf0
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 18037.078, -693.075, 0.723, 0, 0, 0, '{"col size x":3.771,"scale x":1,"col orientation":0,"scale z":1,"col size y":4.542,"col size z":66.446,"scale y":1,"col pos y":-693.075,"col pos x":18037.078,"context":null,"url":null}')

-- ff11a79249ea9fee8d8bb9a28ebe9a076df59888
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 18009.635, -718.251, -3.04, 0, 0, 0, '{"col size x":4.241,"scale x":1,"col orientation":0,"scale z":1,"col size y":4.652,"col size z":66.446,"scale y":1,"col pos y":-718.251,"col pos x":18009.635,"context":null,"url":null}')

-- f38092dc863c40f66e746959df8d67b8a19521cc
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17944.697, -659.086, 0.462, 0, 0, 0, '{"col size x":1.717,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.328,"col size z":13.303,"scale y":1,"col pos y":-659.086,"col pos x":17944.697,"context":null,"url":null}')

-- 29d79f6ce0be61cb47134d393f5a12ed655af617
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17927.965, -662.945, 0.054, 0, 0, 0, '{"col size x":1.677,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.288,"col size z":13.303,"scale y":1,"col pos y":-662.945,"col pos x":17927.965,"context":null,"url":null}')

-- eb573003e7f4bd943f5260711e2c1c6a02215828
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17935.256, -684.095, -0.805, 0, 0, 0, '{"col size x":1.807,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.418,"col size z":13.303,"scale y":1,"col pos y":-684.095,"col pos x":17935.256,"context":null,"url":null}')

-- dfa0c43349b6d913a149a16b5bb0c3e895ac0a86
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17960.723, -680.669, -1.322, 0, 0, 0, '{"col size x":1.547,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.158,"col size z":13.303,"scale y":1,"col pos y":-680.669,"col pos x":17960.723,"context":null,"url":null}')

-- 8a9f09493040ffc9dc42d326e5f76ed18abedaba
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17975.434, -684.394, -3.069, 0, 0, 0, '{"col size x":1.947,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.558,"col size z":13.303,"scale y":1,"col pos y":-684.394,"col pos x":17975.434,"context":null,"url":null}')

-- 57525e9b2bf2f6aa70d334663883997f0f35610f
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17991.334, -674.046, -3.602, 0, 0, 0, '{"col size x":1.757,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.368,"col size z":13.303,"scale y":1,"col pos y":-674.046,"col pos x":17991.334,"context":null,"url":null}')

-- aef1519c55d8050375fb49ba1146b85666d5c518
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17994.785, -686.347, -3.327, 0, 0, 0, '{"col size x":1.697,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.308,"col size z":13.303,"scale y":1,"col pos y":-686.347,"col pos x":17994.785,"context":null,"url":null}')

-- 096ea2e63501615c77b3d6b5b210d711109611ae
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17984.947, -697.879, -3.316, 0, 0, 0, '{"col size x":1.707,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.318,"col size z":13.303,"scale y":1,"col pos y":-697.879,"col pos x":17984.947,"context":null,"url":null}')

-- ea7fe117e8e824a5a2121a9661fccdfc1e28d591
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17964.863, -699.546, -4.091, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-699.546,"col pos x":17964.863,"context":null,"url":null}')

-- fb5a0091d2c5ee43e176099cbe9262a334fe11d4
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17940.283, -699.268, -2.87, 0, 0, 0, '{"col size x":1.367,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.978,"col size z":13.303,"scale y":1,"col pos y":-699.268,"col pos x":17940.283,"context":null,"url":null}')

-- 23edf5093d48eea9eeabebf5180022a6fb252647
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17945.33, -714.035, -3.769, 0, 0, 0, '{"col size x":1.517,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.128,"col size z":13.303,"scale y":1,"col pos y":-714.035,"col pos x":17945.33,"context":null,"url":null}')

-- 30e49d5934bab15c57de1c5629cec59cf6c4f3e8
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17930.012, -737.05, -0.288, 0, 0, 0, '{"col size x":1.667,"scale x":1,"col orientation":0,"scale z":1.35,"col size y":1.278,"col size z":13.303,"scale y":1,"col pos y":-737.05,"col pos x":17930.012,"context":null,"url":null}')

-- c9d1e2f9f51d9a3bccc0d5b00e0d83ce90c0caae
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17954.648, -742.869, -2.064, 0, 0, 0, '{"col size x":1.787,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.398,"col size z":13.303,"scale y":1,"col pos y":-742.869,"col pos x":17954.648,"context":null,"url":null}')

-- 6921a536008d96913713254216def074affcf0af
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17974.291, -743.522, -3.994, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-743.522,"col pos x":17974.291,"context":null,"url":null}')

-- 7798b4f9a0bf70ccd86f42934b386a028ff77d75
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17996.221, -764.165, -1.421, 0, 0, 0, '{"col size x":1.667,"scale x":1,"col orientation":0,"scale z":1.75,"col size y":1.278,"col size z":13.303,"scale y":1,"col pos y":-764.165,"col pos x":17996.221,"context":null,"url":null}')

-- 8f11f55dec79201ca510d313dc9fc6aa6803cf1c
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18014.188, -738.89, -2.563, 0, 0, 0, '{"col size x":1.587,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.198,"col size z":13.303,"scale y":1,"col pos y":-738.89,"col pos x":18014.188,"context":null,"url":null}')

-- a940117730a45b185ba3ecdd433e161a9731b02d
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18028.088, -715.921, -1.039, 0, 0, 0, '{"col size x":1.787,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.398,"col size z":13.303,"scale y":1,"col pos y":-715.921,"col pos x":18028.088,"context":null,"url":null}')

-- 054e87e2663bc3fdb9052f15e7a3419c96a47091
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18048.441, -718.195, -0.384, 0, 0, 0, '{"col size x":1.577,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.188,"col size z":13.303,"scale y":1,"col pos y":-718.195,"col pos x":18048.441,"context":null,"url":null}')

-- be886541ca83df6409e30f9d7672b18836b90d77
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18068.301, -695.201, 1.382, 0, 0, 0, '{"col size x":1.497,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.108,"col size z":13.303,"scale y":1,"col pos y":-695.201,"col pos x":18068.301,"context":null,"url":null}')

-- 13bd8ee5e0f03a492f5fb3407b5cb48b0c14d187
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18075.617, -713.432, 0.161, 0, 0, 0, '{"col size x":1.577,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.188,"col size z":13.303,"scale y":1,"col pos y":-713.432,"col pos x":18075.617,"context":null,"url":null}')

-- 218cebf7ac95531c43ee8d62ed5eabf0bd129663
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18061.918, -729.601, -0.535, 0, 0, 0, '{"col size x":1.187,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.798,"col size z":13.303,"scale y":1,"col pos y":-729.601,"col pos x":18061.918,"context":null,"url":null}')

-- 48bbf023bd6425f2ef9684265fe225c7282cd1bc
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18049.898, -744.461, -0.54, 0, 0, 0, '{"col size x":2.127,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.738,"col size z":13.303,"scale y":1,"col pos y":-744.461,"col pos x":18049.898,"context":null,"url":null}')

-- 4a75dda36e65e4f6894dc96f7a6d39bfe5e1796f
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18061.969, -753.634, 0.342, 0, 0, 0, '{"col size x":1.617,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.228,"col size z":13.303,"scale y":1,"col pos y":-753.634,"col pos x":18061.969,"context":null,"url":null}')

-- 035e97db83ee58e1216156b77d6c60b3e8a766b9
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18072.848, -766.096, 0.085, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-766.096,"col pos x":18072.848,"context":null,"url":null}')

-- 4ea60956afc161537e716765e153f3f60aa9395b
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18037.711, -785.98, -0.083, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-785.98,"col pos x":18037.711,"context":null,"url":null}')

-- 2cac72e6898ec7e44952d5b4e86156e20fc8db1e
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18013.91, -783.731, -2.186, 0, 0, 0, '{"col size x":1.417,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.028,"col size z":13.303,"scale y":1,"col pos y":-783.731,"col pos x":18013.91,"context":null,"url":null}')

-- 1080e1617b84fabfd272d6446e7e7375e850d9e7
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18027.586, -739.235, -1.59, 0, 0, 0, '{"col size x":1.597,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.208,"col size z":13.303,"scale y":1,"col pos y":-739.235,"col pos x":18027.586,"context":null,"url":null}')

-- babe1f6dc9ee626320e7f15617fae60c33a1cd4f
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18020.686, -761.769, -2.822, 0, 0, 0, '{"col size x":1.617,"scale x":1,"col orientation":0,"scale z":1.23,"col size y":1.228,"col size z":13.303,"scale y":1,"col pos y":-761.769,"col pos x":18020.686,"context":null,"url":null}')

-- 2859c1f1d0fb118529ee7061f8e16ccc6742e024
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17994.523, -729.421, -4.975, 0, 0, 0, '{"col size x":1.847,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.458,"col size z":13.303,"scale y":1,"col pos y":-729.421,"col pos x":17994.523,"context":null,"url":null}')

-- 0815e84c2656a8a5a70962b9d2af3e3fbc7a0796
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17963.637, -718.235, -4.867, 0, 0, 0, '{"col size x":1.697,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.308,"col size z":13.303,"scale y":1,"col pos y":-718.235,"col pos x":17963.637,"context":null,"url":null}')

-- 56b4903ce1530005e1284b7c8d3fb68fe58f757a
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17936.67, -752.859, 0.043, 0, 0, 0, '{"col size x":1.357,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.968,"col size z":13.303,"scale y":1,"col pos y":-752.859,"col pos x":17936.67,"context":null,"url":null}')

-- a577c1a8a1bbcdffdf801ad54b8e1f11e9219547
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17970.613, -762.162, -1.94, 0, 0, 0, '{"col size x":1.457,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.068,"col size z":13.303,"scale y":1,"col pos y":-762.162,"col pos x":17970.613,"context":null,"url":null}')

-- 2e749eddb856ddcaaa2cef7785e130d031d48a42
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17942.203, -770.067, -0.354, 0, 0, 0, '{"col size x":1.507,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.118,"col size z":13.303,"scale y":1,"col pos y":-770.067,"col pos x":17942.203,"context":null,"url":null}')

-- 32a75fac10fabc673753e3b83df2d33acb365883
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17922.156, -772.223, -0.011, 0, 0, 0, '{"col size x":1.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.478,"col size z":13.303,"scale y":1,"col pos y":-772.223,"col pos x":17922.156,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### Old_island_ryzom_story - SE_Decoration 12345#################
-- GROUP: entry_from_anniversary
-- 8b23da983af46c3c3162ba93e8c5f9cd3890eee5
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 18050.654, -651.041, -0.777, 0, 0, -1.52, '{"scale x":1,"scale z":1,"col pos x":18050.654,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":-1.52,"col size z":3.89,"col pos y":-651.041,"context":null,"url":null}')

-- 5d2ddedeb5d0a7c63bd7e55d9d962935ee0a60c2
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 18048.098, -647.33, -0.48, 0, 0, 0.02, '{"scale x":1,"scale z":1,"col pos x":18048.098,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":0.02,"col size z":3.89,"col pos y":-647.33,"context":null,"url":null}')

-- aa448e137ff62eb1f3a093485d3976309c3cbf29
SceneEditor:spawnShape("ge_mission_barriere_karavan.ps", 18048.652, -654.855, -1.226, 0, 0, 0, '{"scale x":1,"scale z":1,"col pos x":18048.652,"col size y":0.78,"col size x":8.577,"scale y":1,"col orientation":0,"col size z":3.89,"col pos y":-654.855,"context":null,"url":null}')

-- 63e0fc9fdc395bc2b4b901bf58acc11e1e690a38
SceneEditor:spawnShape("ma_land_telepod.ps", 18049.041, -650.808, -0.798, 0, 0, -1.6, '{"scale x":0.43,"scale z":0.43,"col pos x":0,"col size y":24,"col size x":24,"scale y":0.43,"col orientation":0,"col size z":24,"col pos y":-650.808,"context":null,"url":null}')
--############### Old_island_ryzom_story - SE_Decoration 12345#################

