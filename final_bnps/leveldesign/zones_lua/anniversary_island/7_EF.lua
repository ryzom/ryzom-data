--############### SE_DECO 12333#################
-- GROUP: plants_tryker
-- 0fdc004b6a856a3f6daed644eaa8f518b0c5a6fd
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17513.264, -1117.875, -0.018, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17513.264,"col pos y":-1117.875,"col orientation":0,"col size x":0.923,"col size y":1.603,"col size z":16.228,"context":null,"url":null}')

-- 107e9a7ffb07b9e4b84f2d66e794ab0302ff8467
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17463.631, -1054.577, -2.958, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17463.631,"col pos y":-1054.577,"col orientation":0,"col size x":0.843,"col size y":1.523,"col size z":16.228,"context":null,"url":null}')

-- 81d5e057f4b51c17de487bd876488d260cd2c071
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17499.875, -1110.269, -0.22, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17499.875,"col pos y":-1110.269,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 657397d33f0a195778e1aba1309d1752e53ce02e
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17508.377, -1070.196, -1.183, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17508.377,"col pos y":-1070.196,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- bab67fb546d1127f703e611199a60868228c6641
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17480.627, -1062.307, -2.509, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17480.627,"col pos y":-1062.307,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- e02e430043efe19e144364ef06cc282e6a362e3f
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17458.021, -1037.218, -1.027, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17458.021,"col pos y":-1037.218,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- eec2a9666131e9bdc32bd450e3195eb4463f6f15
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17441.98, -1026.902, 0.016, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17441.98,"col pos y":-1026.902,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 12ff0529716109b5101e3b3e223adf7d5315283c
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17444.51, -1012.062, -0.083, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17444.51,"col pos y":-1012.062,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')


-- GROUP: center
-- 5c105449388f227429d394e490b005e6cb4ad9d3
SceneEditor:spawnShape("ge_partylamp_03.shape", 17565.43, -979.861, 9.982, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17565.43,"col pos y":-979.861,"col orientation":0,"col size x":1.435,"col size y":1.435,"col size z":5.267,"context":null,"url":null}')

-- c7b8ff10cc85174efc3401661d4dabc63f22f247
SceneEditor:spawnShape("ge_partylamp_03.shape", 17594.66, -996.456, 10.106, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17594.66,"col pos y":-996.456,"col orientation":0,"col size x":1.405,"col size y":1.405,"col size z":5.267,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- 7529629910fd9f459850c6286e91efc529010e82
SceneEditor:spawnShape("GE_Mission_borne.shape", 17594.55, -1006.81, 10.275, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17594.55,"col pos y":-1006.81,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3f4f2fe10cbe41d1e43a37a179d14242322e1837
SceneEditor:spawnShape("GE_Mission_borne.shape", 17588.5, -1005.89, 10.377, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17588.5,"col pos y":-1005.89,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 420c55d911cf7651b0128374b70b2c757dfeab35
SceneEditor:spawnShape("GE_Mission_borne.shape", 17582.12, -1006.08, 10.367, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17582.12,"col pos y":-1006.08,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e039ca73b48dcc3e9dcc61d519c6a588fe3b8a2a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17575.71, -1005.92, 10.163, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17575.71,"col pos y":-1005.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b43fd3f942f242e2f1998845d78cc6b5de2ad040
SceneEditor:spawnShape("GE_Mission_borne.shape", 17570.25, -1004.7, 9.934, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17570.25,"col pos y":-1004.7,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0755a1424a08d862940fb0699e7f3dc81b17892c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17564.88, -1002.22, 9.348, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17564.88,"col pos y":-1002.22,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 73ccbc729ea215a1d8850ead31d76dd4587c42c6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17560.28, -998.25, 9.019, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17560.28,"col pos y":-998.25,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0a610b8dca9cfda8fdd599389a81ed4561f0ef2f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17556.39, -992.85, 9.097, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17556.39,"col pos y":-992.85,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 78805438ec44cdbd2434526a48bfa72534a7d76f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17551.95, -987.85, 9.243, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17551.95,"col pos y":-987.85,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0ee800bd6708c25f5e21fbe41bfd4f2c779a4617
SceneEditor:spawnShape("GE_Mission_borne.shape", 17547.58, -982.43, 9.517, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17547.58,"col pos y":-982.43,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b5ae5e458fa762af76e1100187fd32b6b2ce3c83
SceneEditor:spawnShape("GE_Mission_borne.shape", 17544.08, -978.07, 9.617, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17544.08,"col pos y":-978.07,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c48f8dc461aefb2eab73794f93dcee5cc893541e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17540.5, -972.91, 9.514, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17540.5,"col pos y":-972.91,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a8c311147b535e2c84c2922214511a6b1aab618b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17540.58, -967.02, 9.786, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17540.58,"col pos y":-967.02,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 073d96f0b7af0e057c4ba548a0ef5505509a69c4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17544.63, -962.86, 10.17, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17544.63,"col pos y":-962.86,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 44f6697b6e6a5e03ac80610f3cc4df781a66e032
SceneEditor:spawnShape("GE_Mission_borne.shape", 17529.24, -959.5, 8.92, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17529.24,"col pos y":-959.5,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a9da8fe926254bae8f144998bcb9e964deafb9ff
SceneEditor:spawnShape("GE_Mission_borne.shape", 17526.31, -963.92, 8.313, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17526.31,"col pos y":-963.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 03cf91ad42e51a082c287e3c6768bf199db15a74
SceneEditor:spawnShape("GE_Mission_borne.shape", 17525.64, -969.64, 7.921, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17525.64,"col pos y":-969.64,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 686f4df2b611ad46b7d0450158b8c0b0f8e61c85
SceneEditor:spawnShape("GE_Mission_borne.shape", 17527.1, -975.79, 7.91, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17527.1,"col pos y":-975.79,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f07f21e3c2a94f473cb830e1a96f1e5fcd3278b7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17528.9, -982.73, 7.854, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17528.9,"col pos y":-982.73,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 19234ef69b733b7c8ac13c0343d85b25849d789d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17531.92, -989.01, 7.735, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17531.92,"col pos y":-989.01,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6b7cf3e9131f2f3ff0fb6b4a12067758bdc7514d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17535.8, -993.84, 7.647, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17535.8,"col pos y":-993.84,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- fa7c515fba0dda88a44b64c05018332e4034727b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17540.14, -998.63, 7.148, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17540.14,"col pos y":-998.63,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 78a1bffa44203e7d2a3849edaf3f15093f57b261
SceneEditor:spawnShape("GE_Mission_borne.shape", 17545.05, -1003.75, 6.484, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17545.05,"col pos y":-1003.75,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 90ce20ee7106b7fecb865f943e2ea2aa61b74195
SceneEditor:spawnShape("GE_Mission_borne.shape", 17549.28, -1008.36, 6.183, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17549.28,"col pos y":-1008.36,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f676f15e96162aae36a8411509add2767511c0ab
SceneEditor:spawnShape("GE_Mission_borne.shape", 17553.63, -1012.73, 6.338, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17553.63,"col pos y":-1012.73,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ca53e89c2ebf5142be5161eb7f4d214e30ea65c2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17559.28, -1016.76, 7.302, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17559.28,"col pos y":-1016.76,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f195c7efa306c31d9e1b95cbc19e681ddb188ee8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17564.97, -1019.28, 8.378, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17564.97,"col pos y":-1019.28,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 1c09152c253ff993588a700d832d02e236971ecd
SceneEditor:spawnShape("GE_Mission_borne.shape", 17564.97, -1019.28, 8.378, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17564.97,"col pos y":-1019.28,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 081baea5c13501e40786136c59477e0467158517
SceneEditor:spawnShape("GE_Mission_borne.shape", 17570.67, -1019.95, 9.207, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17570.67,"col pos y":-1019.95,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2184c1c78c5c16910ddb160439ca487f8f9b858a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17576.9, -1020.58, 9.621, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17576.9,"col pos y":-1020.58,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c50e73cf6465bc18b9236f62deae2c32e673e6b4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17582.99, -1020.74, 9.987, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17582.99,"col pos y":-1020.74,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 7ca6da0a0b668c30c67970a340ca9fca5abaf8a9
SceneEditor:spawnShape("GE_Mission_borne.shape", 17589.88, -1021.35, 10.122, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17589.88,"col pos y":-1021.35,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 73a804932be2fa320fc61599b7cadd7dba3b6e42
SceneEditor:spawnShape("GE_Mission_borne.shape", 17595.96, -1021.12, 10.178, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17595.96,"col pos y":-1021.12,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################

--############### SE_deco_Memorial 12437#################
-- GROUP: memorial
-- a0d796a2b176a0cf172cf70fbc132a806de40315
SceneEditor:spawnShape("ju_s3_bamboo.shape", 17585.566, -1108.152, -0.227, 0, 0, 0, '{"col pos x":17585.604,"col size x":8.634,"scale z":-2.68,"col size y":6.982,"col orientation":-0.22,"scale y":-2.68,"col size z":6.611,"scale x":-2.68,"col pos y":-1107.691,"context":null,"url":null}')

-- 7c515ebfcd1264b7ab066c92319910252fc47291
SceneEditor:spawnShape("ge_mission_defense_wall.shape", 17572.344, -1104.889, -0.376, 0, 0, -0.2, '{"col pos x":17572.381,"col size x":35.477,"scale z":2.03,"col size y":7.822,"col orientation":-0.18,"scale y":2.03,"col size z":10.601,"scale x":2.03,"col pos y":-1105.019,"context":null,"url":null}')

-- 1a64718afbd49408a62bcab84a4fa87f28e38b8f
SceneEditor:spawnShape("ge_mission_panneau.shape", 17586.395, -1080.394, 0.202, 0, 0, 0, '{"col pos x":17586.395,"col size x":1.61,"scale z":1.77,"col size y":0.172,"col orientation":0,"scale y":1.77,"col size z":2.305,"scale x":1.77,"col pos y":-1080.394,"context":"Memorial Grove","url":"https://app.ryzom.com/app_arcc/index.php?action=mScript_Run&script=12443&_hideWindow=1&command=reset_all&idText=1"}')

-- ba0e2861d7e9e6009cb5e4fa8d43933b0a562e88
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17598.012, -1092.272, -0.27, 0, 0, 1.33, '{"col pos x":17598.012,"col size x":9.727,"scale z":1,"col size y":13.461,"col orientation":-1.97,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1092.272,"context":null,"url":null}')

-- 3f64c91521e1a58b85596b7176d61ee2366f5223
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17588.693, -1088.534, -0.341, 0, 0, -4.48, '{"col pos x":17588.637,"col size x":5.937,"scale z":0.43,"col size y":16.061,"col orientation":-5.02,"scale y":0.43,"col size z":52.171,"scale x":0.43,"col pos y":-1088.846,"context":null,"url":null}')

-- b5672a4434a07161e1c5b745d9cffdf044660e1d
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17559.152, -1080.071, 0.873, 0, 0, -1.74, '{"col pos x":17559.152,"col size x":7.417,"scale z":0.76,"col size y":11.152,"col orientation":-1.85,"scale y":0.76,"col size z":52.171,"scale x":0.76,"col pos y":-1080.071,"context":null,"url":null}')

-- 981feb54f626544f748096c0486bfb110ae6147b
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17527.721, -1106.335, 0.74, 0, 0, -0.42, '{"col pos x":17527.721,"col size x":8.757,"scale z":1,"col size y":12.491,"col orientation":-0.23,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1106.335,"context":null,"url":null}')

-- 4baaa7f6e283cb0898500657b5f26fc3fd26bdc8
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17566.574, -1082.529, -0.177, 0, 0, 0, '{"col pos x":17566.574,"col size x":6.327,"scale z":0.38,"col size y":13.961,"col orientation":-1.88,"scale y":0.38,"col size z":52.171,"scale x":0.38,"col pos y":-1082.529,"context":null,"url":null}')

-- bbb8b3342de237f54d19a789fae69b5488bf68e4
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17537.316, -1082.669, 1.039, 0, 0, -0.58, '{"col pos x":17537.316,"col size x":10.247,"scale z":1,"col size y":13.981,"col orientation":-0.61,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1082.669,"context":null,"url":null}')

-- 6bbade631d8db18a5bd2319770ecbf7839ff7f14
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17530.984, -1094.222, 0.786, 0, 0, 0.03, '{"col pos x":17530.984,"col size x":9.807,"scale z":1,"col size y":13.541,"col orientation":-3.51,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1094.222,"context":null,"url":null}')

-- b4856748094da308b464eba9632589f4bb7115a7
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17525.654, -1119.405, -0.016, 0, 0, 0, '{"col pos x":17525.654,"col size x":9.127,"scale z":1,"col size y":12.861,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1119.405,"context":null,"url":null}')

-- c319058c916d02b9fba3c36efe248a7471e569ed
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17547.74, -1078.364, 1.12, 0, 0, 1.47, '{"col pos x":17547.74,"col size x":10.077,"scale z":1,"col size y":13.812,"col orientation":1.6,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1078.364,"context":null,"url":null}')

-- 3bd95b4bcd4e26a6005ba8265ec7056dac8e3ebf
SceneEditor:spawnShape("ge_mission_gate.shape", 17577.178, -1085.556, -0.575, 0, 0, 2.86, '{"col pos x":16.08,"col size x":22.452,"scale z":1,"col size y":17.616,"col orientation":0,"scale y":1,"col size z":19.674,"scale x":1,"col pos y":-1085.556,"context":null,"url":null}')

-- 1e3daa434942b9406bf90362bc74a4fb28da5eb4
SceneEditor:spawnShape("ju_s1_bamboo.shape", 17579.055, -1106.723, -0.356, 0, 0, 1.31, '{"col pos x":17579.055,"col size x":6.907,"scale z":1,"col size y":23.791,"col orientation":1.36,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1106.723,"context":null,"url":null}')

-- adfd9bd8a51cf25e246d7ba6e215323e3b2eb074
SceneEditor:spawnShape("ju_s1_bamboo.shape", 17566.779, -1104.009, -0.058, 0, 0, 1.66, '{"col pos x":17566.779,"col size x":7.827,"scale z":1,"col size y":19.611,"col orientation":-1.75,"scale y":1,"col size z":52.621,"scale x":1,"col pos y":-1104.009,"context":null,"url":null}')
--############### SE_deco_Memorial 12437#################


