--############### SE_DECO 12333#################
-- GROUP: buildings_fyros
-- 904bccbb5f5c43b5dc2e8f91f4f8bd51f19c618e
SceneEditor:spawnShape("fy_cn_trykerinn_a_ext.shape", 17301.77, -651.891, -1.245, 0, 0, 2.37, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17301.77,"col pos y":-651.891,"col orientation":2.37,"col size x":38.788,"col size y":46.539,"col size z":62.528,"context":null,"url":null}')

-- 0b0938024a17495271849d324207739f98f905f3
SceneEditor:spawnShape("fy_cn_trykerinn_a_int.shape", 17301.795, -651.91, -1.248, 0, 0, -3.909, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0.254,"col pos y":-651.91,"col orientation":0,"col size x":16.749,"col size y":18.374,"col size z":10.47,"context":null,"url":null}')


-- GROUP: plants_fyros
-- 86e016bf6e567693c2e3d0129fcc1ac5605d4764
SceneEditor:spawnShape("fy_s1_baobab_a.shape", 17332.463, -777.255, -2.509, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17332.463,"col pos y":-777.255,"col orientation":0,"col size x":5.626,"col size y":5.824,"col size z":20.842,"context":null,"url":null}')

-- c9f05187b0c139d09c6117b3c2e4167e9e26986e
SceneEditor:spawnShape("fy_s1_baobab_a.shape", 17423.861, -719.2, -0.217, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17423.861,"col pos y":-719.2,"col orientation":0,"col size x":8.206,"col size y":8.404,"col size z":20.842,"context":null,"url":null}')

-- a9c1620b3c97960b3d27df3360b5493891f26783
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17368.232, -758.629, -3.181, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17368.232,"col pos y":-758.629,"col orientation":0,"col size x":4.695,"col size y":5.808,"col size z":15.054,"context":null,"url":null}')

-- 7194ed7f7eb88091b46a32241e850d0a6653234c
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17310.684, -729.374, -1.943, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17310.684,"col pos y":-729.374,"col orientation":0,"col size x":4.415,"col size y":5.528,"col size z":15.054,"context":null,"url":null}')

-- e61b6785dc881ad5ca0843b9a0cbdf32c057e354
SceneEditor:spawnShape("fy_s1_burnedtree_b.shape", 17381.668, -665.613, -2.546, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17381.668,"col pos y":-665.613,"col orientation":0,"col size x":4.085,"col size y":4.324,"col size z":15.87,"context":null,"url":null}')

-- c460bcc6dccbbfe43a069d6366a42fca171696e9
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17359.834, -717.117, -3.856, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17359.834,"col pos y":-717.117,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- 6d51bebf395e332b0540c7769f7c0c5d405dd702
SceneEditor:spawnShape("fy_s2_lovejail_c.shape", 17410.881, -753.993, 0.417, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17410.881,"col pos y":-753.993,"col orientation":0,"col size x":3.167,"col size y":2.255,"col size z":5.141,"context":null,"url":null}')

-- 7eb7cefa25a14e90c21533ae9cb901d1958c5351
SceneEditor:spawnShape("fy_s2_savantree_a.shape", 17383.555, -692.667, -3.228, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17383.555,"col pos y":-693.255,"col orientation":0,"col size x":1.487,"col size y":1.84,"col size z":11.066,"context":null,"url":null}')

-- 420ed77b19504c305ed60170bdc15633d4c07bc2
SceneEditor:spawnShape("fy_s2_savantree_b.shape", 17356.861, -655.833, -1.41, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17356.861,"col pos y":-656.188,"col orientation":0,"col size x":2.067,"col size y":1.67,"col size z":10.293,"context":null,"url":null}')

-- 6ff1fe078dc1291202e9cfe70a5425985a29ac62
SceneEditor:spawnShape("fy_s2_savantree_b.shape", 17340.051, -710.025, -4.856, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17340.051,"col pos y":-710.334,"col orientation":0,"col size x":1.187,"col size y":1.41,"col size z":10.293,"context":null,"url":null}')

-- c378855a29f595dda0900bc97a63956f77a65e7f
SceneEditor:spawnShape("fy_s2_savantree_c.shape", 17319.766, -672.773, -0.552, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17319.766,"col pos y":-673.337,"col orientation":0,"col size x":1.077,"col size y":1.32,"col size z":10.591,"context":null,"url":null}')
--############### SE_DECO 12333#################

