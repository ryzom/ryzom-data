--############### SE_DECO 12333#################
-- GROUP: plants_matis
-- 7b1d5e0bbfa0044a3e6fc990f77554e2f1a81d58
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 17908.865, -658.635, 0.151, 0, 0, 0, '{"col size x":3.751,"scale x":1,"col orientation":0,"scale z":0.96,"col size y":3.822,"col size z":66.446,"scale y":1,"col pos y":-658.635,"col pos x":17908.865,"context":null,"url":null}')

-- d5bdca5cf91494c8d9e60b900061b55c16008a78
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17916.377, -686.845, -0.007, 0, 0, 0, '{"col size x":1.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.478,"col size z":13.303,"scale y":1,"col pos y":-686.845,"col pos x":17916.377,"context":null,"url":null}')

-- 22b9579cf68618394195051afa8ce7adefa3e899
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17901.314, -654.176, 0.192, 0, 0, 0, '{"col size x":1.447,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.058,"col size z":13.303,"scale y":1,"col pos y":-654.176,"col pos x":17901.314,"context":null,"url":null}')

-- 1b0d2934782fd53d1355e46b1bf18fe412c900a4
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17890.574, -640.327, -0.004, 0, 0, 0, '{"col size x":1.527,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.138,"col size z":13.303,"scale y":1,"col pos y":-640.327,"col pos x":17890.574,"context":null,"url":null}')

-- 684cb449e4d945fda90612b23049496ef8ce07be
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17889.564, -652.469, 0.295, 0, 0, 0, '{"col size x":1.317,"scale x":1,"col orientation":0,"scale z":2.98,"col size y":0.928,"col size z":13.303,"scale y":1,"col pos y":-652.469,"col pos x":17889.564,"context":null,"url":null}')

-- 7c387e9f6332792b2f6f4b888d231f8a4571b632
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17881.352, -662.23, 0.65, 0, 0, 0, '{"col size x":1.557,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.168,"col size z":13.303,"scale y":1,"col pos y":-662.23,"col pos x":17881.352,"context":null,"url":null}')

-- f19417342b46c23abd5238610028a22c34a4c483
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17875.494, -677.724, 0.055, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":0.91,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-677.724,"col pos x":17875.494,"context":null,"url":null}')

-- a900c899f4ddf920490abe9adfb878c532a7d6ce
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17865.391, -668.309, -0.383, 0, 0, 0, '{"col size x":1.477,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.088,"col size z":13.303,"scale y":1,"col pos y":-668.309,"col pos x":17865.391,"context":null,"url":null}')

-- f4f582c32cdf499ff3daee6871631f84ebed3dc1
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17864.297, -648.738, 0, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-648.738,"col pos x":17864.297,"context":null,"url":null}')

-- 6bc5df1f8d78e9fbfc33d0fb113d73e0c86b8963
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17914.326, -714.596, -0.298, 0, 0, 0, '{"col size x":1.637,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.248,"col size z":13.303,"scale y":1,"col pos y":-714.596,"col pos x":17914.326,"context":null,"url":null}')

-- dcac806338c897f8cfecde33d263afd8a6411757
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17893.193, -695.971, -2.721, 0, 0, 0, '{"col size x":1.587,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.198,"col size z":13.303,"scale y":1,"col pos y":-695.971,"col pos x":17893.193,"context":null,"url":null}')

-- b9990ce0db86c3f2ed32079abd015f46fa002ef7
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17902.66, -704.417, -1.948, 0, 0, 0, '{"col size x":1.567,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.178,"col size z":13.303,"scale y":1,"col pos y":-704.417,"col pos x":17902.66,"context":null,"url":null}')

-- 11be5f2c9ea2f9b8273af0ae7cd1809941b47152
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17896.562, -714.385, -2.059, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-714.385,"col pos x":17896.562,"context":null,"url":null}')

-- 6b41015ffae358be7315fd7e02fbbadee0e87e6d
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17863.471, -707.784, -2.842, 0, 0, 0, '{"col size x":1.627,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.238,"col size z":13.303,"scale y":1,"col pos y":-707.784,"col pos x":17863.471,"context":null,"url":null}')

-- c77cdc455eab8659ae1f94cbdad1aed60e31de9c
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17857.092, -727.262, -3.251, 0, 0, 0, '{"col size x":1.667,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.278,"col size z":13.303,"scale y":1,"col pos y":-727.262,"col pos x":17857.092,"context":null,"url":null}')

-- 9d2e660b53055c1a6afb2d46297c510859a35c8f
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17873.99, -724.844, -3.004, 0, 0, 0, '{"col size x":1.447,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.058,"col size z":13.303,"scale y":1,"col pos y":-724.844,"col pos x":17873.99,"context":null,"url":null}')

-- 9d56f8646e88dfff3c16e0be1a0098978a1c9919
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17882.975, -736.702, -1.071, 0, 0, 0, '{"col size x":1.557,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.168,"col size z":13.303,"scale y":1,"col pos y":-736.702,"col pos x":17882.975,"context":null,"url":null}')

-- 347dec67110c9688ea0576ffbf5550405a6ddef4
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17868.098, -739.069, -3.048, 0, 0, 0, '{"col size x":1.397,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.008,"col size z":13.303,"scale y":1,"col pos y":-739.069,"col pos x":17868.098,"context":null,"url":null}')

-- af6224c9096287c0c68f39ccec06a703d9d02bab
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17891.441, -776.46, -1.148, 0, 0, 0, '{"col size x":1.407,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.018,"col size z":13.303,"scale y":1,"col pos y":-776.46,"col pos x":17891.441,"context":null,"url":null}')

-- 45d12cf68ea1946b1a751c63e7ed442eae89d5cd
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17882.686, -766.279, -1.332, 0, 0, 0, '{"col size x":1.177,"scale x":1,"col orientation":0,"scale z":1,"col size y":0.788,"col size z":13.303,"scale y":1,"col pos y":-766.279,"col pos x":17882.686,"context":null,"url":null}')

-- c95b4fdf557287db0c0cf18ec257a37cfa75fd03
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17889.508, -742.674, -0.843, 0, 0, 0, '{"col size x":1.607,"scale x":1,"col orientation":0,"scale z":1.46,"col size y":1.218,"col size z":13.303,"scale y":1,"col pos y":-742.674,"col pos x":17889.508,"context":null,"url":null}')

-- 8517da099e2105f0105bbc5bd9da71302476b80d
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17906.75, -727.539, -0.356, 0, 0, 0, '{"col size x":1.427,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.038,"col size z":13.303,"scale y":1,"col pos y":-727.539,"col pos x":17906.75,"context":null,"url":null}')

-- dd7451034f8503b98919aae0e5ea37488a6ceecc
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17907.539, -743.6, -0.336, 0, 0, 0, '{"col size x":1.977,"scale x":1,"col orientation":0,"scale z":1.88,"col size y":1.588,"col size z":13.303,"scale y":1,"col pos y":-743.6,"col pos x":17907.539,"context":null,"url":null}')

-- 14ab1d367e530a316f79c2c2dc397940d34d9946
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17915.758, -754.541, -0.122, 0, 0, 0, '{"col size x":1.677,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.288,"col size z":13.303,"scale y":1,"col pos y":-754.541,"col pos x":17915.758,"context":null,"url":null}')

-- 20961058ba1366f3dd6e26013c7a12d64c452d9b
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17898.422, -752.978, -1.307, 0, 0, 0, '{"col size x":1.897,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.508,"col size z":13.303,"scale y":1,"col pos y":-752.978,"col pos x":17898.422,"context":null,"url":null}')

-- c3f52b890d810a2c2761fcdc03589023ad9a47cc
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17897.844, -761.822, -1.597, 0, 0, 0, '{"col size x":2.067,"scale x":1,"col orientation":6.14,"scale z":1,"col size y":1.678,"col size z":13.303,"scale y":1,"col pos y":-761.822,"col pos x":17897.844,"context":null,"url":null}')

-- 49a5dbd18920febfc22c2da880a4be432fc28c03
SceneEditor:spawnShape("fo_s2_spiketree.shape", 17898.936, -770.73, -1.181, 0, 0, 0, '{"col size x":6.867,"scale x":1,"col orientation":0,"scale z":1,"col size y":6.478,"col size z":13.303,"scale y":1,"col pos y":-770.73,"col pos x":17898.936}')


-- GROUP: center
-- dbcc0ccc907c898cc59629ac9e5c4e25e04120c1
SceneEditor:spawnShape("ge_partylamp_03.shape", 17787.766, -793.226, 10.081, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17787.766,"col pos y":-793.226,"col orientation":0,"col size x":1.475,"col size y":1.475,"col size z":5.267,"context":null,"url":null}')


-- GROUP: center_bar
-- 6a3cdd4a408b8fdda553542911d7395b8b7eafd7
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17779.832, -791.768, 10.078, 0, 0, 0, '{"col pos x":17779.832,"col size x":1.013,"scale z":1,"col size y":0.883,"col orientation":0,"scale y":1,"col size z":16.228,"scale x":1,"col pos y":-791.768,"context":null,"url":null}')

-- a8afcb8844def05e2e71638cfb5c7c45c1e13e83
SceneEditor:spawnShape("tr_s2_palmtree_f.shape", 17777.25, -792.7, 10.092, 0, 0, 0, '{"col pos x":17777.484,"col size x":1.396,"scale z":1,"col size y":3.48,"col orientation":0,"scale y":1,"col size z":19.267,"scale x":1,"col pos y":-793.274,"context":null,"url":null}')

-- da79488080b14418034d827a18109ef1110f8fea
SceneEditor:spawnShape("tr_s2_palmtree_a.shape", 17789.24, -786.331, 10.236, 0, 0, 2.75, '{"col pos x":17789.24,"col size x":0.856,"scale z":1,"col size y":1.516,"col orientation":0,"scale y":1,"col size z":6.288,"scale x":1,"col pos y":-786.331,"context":null,"url":null}')

-- 2978e513b7f6b7a6dc948b106fa93ef5dddf7d02
SceneEditor:spawnShape("coussin.shape", 17791.854, -798.208, 9.745, 0, 0, 0.43, '{"col pos x":-1.457,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-798.208,"context":null,"url":null}')

-- 6f313ed21d32a10388ecf826a30f71c9ca61dfc2
SceneEditor:spawnShape("coussin.shape", 17788.146, -797.285, 9.832, 0, 0, -0.79, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-797.285,"context":null,"url":null}')

-- b5b622ceb2ccf26c1a7edfdb8647464eb49caad1
SceneEditor:spawnShape("coussin.shape", 17788.775, -788.926, 9.797, 0, 0, -0.51, '{"col pos x":-0.652,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-788.926,"context":null,"url":null}')

-- 54bfd4889bf40d58954b485e3cb35ecb95820aaf
SceneEditor:spawnShape("coussin.shape", 17787.031, -784.95, 9.87, 0, 0, -0.43, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-784.95,"context":null,"url":null}')

-- 0781f8b356d057ddaccce35d7825f542d846d5e5
SceneEditor:spawnShape("coussin.shape", 17783.588, -788.769, 9.856, 0, 0, 0.44, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-788.769,"context":null,"url":null}')

-- 0f2599ac3760ef66b9e42bc351414d012f19c3c1
SceneEditor:spawnShape("coussin.shape", 17772.582, -792.017, 9.707, 0, 0, 0.65, '{"col pos x":-1.873,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-792.017,"context":null,"url":null}')

-- d02ee4deadbd3c85498d92bd2a471d32b0d86ab3
SceneEditor:spawnShape("coussin.shape", 17772.43, -795.76, 9.737, 0, 0, 0.75, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-795.76,"context":null,"url":null}')

-- cad07cc755e9e1ca612857a1c75d4a12f8873b1e
SceneEditor:spawnShape("coussin.shape", 17769.205, -792.978, 9.696, 0, 0, -0.39, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-792.978,"context":null,"url":null}')

-- f90091dc5f597f731946e3db40d6a13051cbd735
SceneEditor:spawnShape("tr_acc_table.shape", 17770.965, -793.552, 10.013, 0, 0, 0, '{"col pos x":17770.965,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-793.552,"context":null,"url":null}')

-- 77df4503c8f9838ca22628186c2732d307dfe4b9
SceneEditor:spawnShape("tr_acc_table.shape", 17786.115, -787.424, 10.162, 0, 0, 0, '{"col pos x":17786.115,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":3.141,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-787.424,"context":null,"url":null}')

-- 1e4e75c0603af7639ed2d4ca319dd94a71823bca
SceneEditor:spawnShape("tr_acc_table.shape", 17789.73, -799.15, 10.054, 0, 0, 0, '{"col pos x":17789.73,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":3.141,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-799.15,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- e610b331a3b3632920eeb2573b179283ce989f7e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17829.28, -798.19, 9.38, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17829.28,"col pos y":-798.19,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 68d44c5221e8d38e6be4735b6a2eccb119b0c0c6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17823.63, -789.94, 9.68, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17823.63,"col pos y":-789.94,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 82954f63c0ff53a982e79dfb50482c638c1c474a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17818, -781.68, 9.72, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17818,"col pos y":-781.68,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2817c38dbdbd2187475cea93bfa0f05db8f7f80d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17809.1, -777.1, 9.74, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17809.1,"col pos y":-777.1,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 427d13c47cb5835f3048fb4353887e318a1f311a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17800.21, -772.54, 9.89, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17800.21,"col pos y":-772.54,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2921be1f2dfb57445b5cade1a750700e805cebb7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17791.33, -767.95, 10.05, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17791.33,"col pos y":-767.95,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f6016f636e20b63d26136e0371cba8b9b2f9dd64
SceneEditor:spawnShape("GE_Mission_borne.shape", 17782.43, -763.38, 10.01, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17782.43,"col pos y":-763.38,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 91aea630f8a9bc97f5f20f8f2e2217f97b052316
SceneEditor:spawnShape("GE_Mission_borne.shape", 17773.54, -758.8, 10, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17773.54,"col pos y":-758.8,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- fabae2cdc577721a59f584bfe3c28be790ef3f4c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17764.64, -754.22, 10.27, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17764.64,"col pos y":-754.22,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0c64312229b5cc5d343c374b030d39092612ee06
SceneEditor:spawnShape("GE_Mission_borne.shape", 17762.34, -768.91, 10.06, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17762.34,"col pos y":-768.91,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 22f039465d11195af4aa49900e78c64616977e72
SceneEditor:spawnShape("GE_Mission_borne.shape", 17771.31, -773.31, 10.06, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17771.31,"col pos y":-773.31,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 07cd71e9d71590067448d612eee2c8d643faf382
SceneEditor:spawnShape("GE_Mission_borne.shape", 17780.29, -777.71, 10.23, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17780.29,"col pos y":-777.71,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f0b0f81c19f1824bf04786f0369112d4f458bf7e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17789.26, -782.13, 10.24, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17789.26,"col pos y":-782.13,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 76d92c2e899f349031cae44d066d4f87e7061d70
SceneEditor:spawnShape("GE_Mission_borne.shape", 17798.24, -786.53, 10.03, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17798.24,"col pos y":-786.53,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 5f5336517e0bd0165bf47ed2fca3809244436ed2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17805.97, -793.63, 10.12, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17805.97,"col pos y":-793.63,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################
