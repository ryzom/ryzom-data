--############### SE_DECO 12333#################
-- GROUP: plants_tryker
-- 0c0e8fd0d04abe9aa042a779038cddfd658357c0
SceneEditor:spawnShape("tr_s2_nenufly_a.shape", 17449.637, -1181.534, 0.034, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17449.637,"col pos y":-1181.534,"col orientation":0,"col size x":2.543,"col size y":2.543,"col size z":11.213,"context":null,"url":null}')

-- 13e6f7172f04d25b2f309b8df7932b9dbce35e07
SceneEditor:spawnShape("tr_s2_nenufly_a.shape", 17470.236, -1122.868, 0.048, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17470.236,"col pos y":-1122.868,"col orientation":0,"col size x":3.293,"col size y":3.293,"col size z":11.213,"context":null,"url":null}')

-- e6a89439825a19da84339b02c47b3919b964c77e
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17483.008, -1170.557, 1.06, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17483.008,"col pos y":-1170.557,"col orientation":0,"col size x":12.203,"col size y":12.883,"col size z":16.228,"context":null,"url":null}')

-- 26694cb792322ed0f1cca864f5fefccd89a0b964
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17444.918, -1157.22, 0.038, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17444.918,"col pos y":-1157.22,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- d0b96f607972164cc31ba623259b9f4c2a07d8ff
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17479.705, -1143.659, 1.054, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17479.705,"col pos y":-1143.659,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- ebed85fa39276c99c3f25485546369d918816d3d
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17464.967, -1186.449, 0.222, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17464.967,"col pos y":-1186.449,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 4bfe55004f15340e686a654c4fd4ce5f3f550a0c
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17487.191, -1217.126, 2.283, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17487.191,"col pos y":-1217.126,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- ab378fad391d1cda1605095c63457db10c53dc42
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17506.906, -1202.749, -0.806, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17506.906,"col pos y":-1202.749,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- f6ad81295ce2bea7ab41fb7069a6f0bf7ef7a76d
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17508.363, -1174.506, 2.071, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17508.363,"col pos y":-1174.506,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- eb9df615e7cd12f15c89ff468609f05c296a1839
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17502.197, -1155.787, 1.016, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17502.197,"col pos y":-1155.787,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_deco_Memorial 12437#################
-- GROUP: memorial
-- 066859d934b2dbecf060c195e1dcbacc51192c3b
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 17540.336, -1145.813, -0.75, 0, 0, 1.45, '{"col pos x":17540.336,"col size x":1.334,"scale z":1,"col size y":0.539,"col orientation":1.45,"scale y":1,"col size z":1.192,"scale x":1,"col pos y":-1145.813,"context":null,"url":null}')

-- 0a19f6276e060a074e862d06443fa38fcdceb44f
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 17588.934, -1158.544, -0.456, 0, 0, 1.33, '{"col pos x":17588.934,"col size x":1.494,"scale z":1,"col size y":0.965,"col orientation":4.47,"scale y":1,"col size z":1.76,"scale x":1,"col pos y":-1158.544,"context":null,"url":null}')

-- 817d1a6e2ec073d730c41dd28bb10afbfd565aa8
SceneEditor:spawnShape("ge_livre.shape", 17560.092, -1196.133, 0.963, 1.84, 1.56, 10.84, '{"col pos x":-0.066,"col size x":0.45,"scale z":2.41,"col size y":0.216,"col orientation":0,"scale y":2.41,"col size z":0.561,"scale x":2.41,"col pos y":-1193.533,"context":"Farewell","url":"https://app.ryzom.com/app_arcc/index.php?action=mScript_Run&script=12443&_hideWindow=1&command=reset_all&idText=0"}')

-- a6b0af1354902286c6308a7044048a800060ec2e
SceneEditor:spawnShape("fo_s2_bigroot_b.shape", 17534.371, -1158.21, -0.853, 0, 0, 1.59, '{"col pos x":17535.041,"col size x":17.379,"scale z":1.55,"col size y":7.458,"col orientation":-1.77,"scale y":1.55,"col size z":4.065,"scale x":1.55,"col pos y":-1154.52,"context":null,"url":null}')

-- 35a34c6f4143a3c14558a3b9864a261c1304692a
SceneEditor:spawnShape("fo_s2_bigroot_a.shape", 17530.748, -1168.325, -0.561, 0, 0, -1.39, '{"col pos x":17529.406,"col size x":15.346,"scale z":1,"col size y":4.659,"col orientation":1.23,"scale y":1,"col size z":8.57,"scale x":1,"col pos y":-1174.628,"context":null,"url":null}')

-- 2373931a4a1701a8503cf2555ac2dcc228eba016
SceneEditor:spawnShape("fo_s2_bigroot_c.shape", 17535.266, -1142.513, -0.706, 0, 0, 1.41, '{"col pos x":17536.5,"col size x":11.836,"scale z":1,"col size y":6.447,"col orientation":1.81,"scale y":1,"col size z":5.622,"scale x":1,"col pos y":-1137.856,"context":null,"url":null}')

-- 9bba7296922ff5a78be3ed8836344bd44f3eca75
SceneEditor:spawnShape("ju_s3_plante.shape", 17587.676, -1178.908, -0.281, 0, 0, 0, '{"col pos x":17587.676,"col size x":5.976,"scale z":1.5,"col size y":5.849,"col orientation":0,"scale y":1.5,"col size z":3.21,"scale x":1.5,"col pos y":-1178.908,"context":null,"url":null}')

-- a1cea64335305f7e128f1a87fb6eba5c8c68463e
SceneEditor:spawnShape("ju_s3_plante.shape", 17590.27, -1151.527, -0.482, 0, 0, 1.2, '{"col pos x":17590.27,"col size x":5.976,"scale z":1.28,"col size y":5.849,"col orientation":0,"scale y":1.28,"col size z":3.21,"scale x":1.28,"col pos y":-1151.527,"context":null,"url":null}')

-- 2ce5c69707c88bcdf8094e451b69230eac6e30db
SceneEditor:spawnShape("ju_s3_plante.shape", 17588.129, -1166.636, -0.421, 0, 0, -1.54, '{"col pos x":17588.129,"col size x":5.976,"scale z":2.48,"col size y":5.849,"col orientation":0,"scale y":2.48,"col size z":3.21,"scale x":2.48,"col pos y":-1166.636,"context":null,"url":null}')

-- 3ff8631080fa0b716c15dc94e7d17fe7720d046d
SceneEditor:spawnShape("ju_s3_plantegrasse.shape", 17539.467, -1187.573, -0.822, 0, 0, 0, '{"col pos x":17539.467,"col size x":3.345,"scale z":1,"col size y":2.926,"col orientation":0,"scale y":1,"col size z":3.99,"scale x":1,"col pos y":-1187.573,"context":null,"url":null}')

-- fac7f2f87039759234c454ccc924a41847df332b
SceneEditor:spawnShape("ju_s3_plantegrasse.shape", 17543.582, -1194.368, -0.752, 0, 0, 0, '{"col pos x":17543.582,"col size x":3.265,"scale z":1,"col size y":2.846,"col orientation":0,"scale y":1,"col size z":3.99,"scale x":1,"col pos y":-1194.368,"context":null,"url":null}')

-- a8c2b64f8304299d3c199ccf33cc3f2cf98b502a
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 17593.805, -1141.91, -0.171, 0, 0, 0, '{"col pos x":17593.805,"col size x":5.319,"scale z":1.54,"col size y":5.665,"col orientation":0,"scale y":1.54,"col size z":4.997,"scale x":1.54,"col pos y":-1141.91,"context":null,"url":null}')

-- f7a65bf10ef1ffc6b17bbabfd5483abb5a4792b3
SceneEditor:spawnShape("pr_s1_rotaflore_b.shape", 17561.66, -1197.12, -0.119, 0, 0, 0, '{"col pos x":17561.66,"col size x":4.113,"scale z":1,"col size y":3.937,"col orientation":0,"scale y":1,"col size z":9.724,"scale x":1,"col pos y":-1197.12,"context":null,"url":null}')

-- aaba8ce18b4e3f4e16f07d5dde6de0aff5b90c09
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 17557.461, -1196.706, -0.333, 0, 0, 1.44, '{"col pos x":17557.461,"col size x":3.334,"scale z":0.71,"col size y":3.882,"col orientation":0,"scale y":0.71,"col size z":12.685,"scale x":0.71,"col pos y":-1196.706,"context":null,"url":null}')

-- faa2ed5c6a4a3f8d111fb3611daf1ea591d13108
SceneEditor:spawnShape("ge_mission_justice_flame.ps", 17579.363, -1197.478, -0.002, 0, 0, 3.04, '{"col pos x":17579.363,"col size x":4.544,"scale z":1,"col size y":4.563,"col orientation":0,"scale y":1,"col size z":8.223,"scale x":1,"col pos y":-1197.478,"context":null,"url":null}')

-- c61a58b1056c99d96419c6674057ced894911c97
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17580.496, -1128.133, 3.218, 0, 0, 0, '{"col pos x":17580.496,"col size x":0.58,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1128.133,"context":null,"url":null}')

-- 221065392c0805404ae6402b9968bc5c8cb4511b
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17579.158, -1134.956, 2.895, 0, 0, 0, '{"col pos x":17579.158,"col size x":0.59,"scale z":1,"col size y":0.549,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1134.956,"context":null,"url":null}')

-- 9a981e7bae9cf180ad8ddcfb280b58bd53a790bc
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17577.705, -1144.3, 2.359, 0, 0, 0, '{"col pos x":17577.705,"col size x":0.63,"scale z":1,"col size y":0.589,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1144.3,"context":null,"url":null}')

-- fcc666c9cac41de735812daa701f5323e7642003
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17576.688, -1152.298, 2.111, 0, 0, 0, '{"col pos x":17576.688,"col size x":0.54,"scale z":1,"col size y":0.499,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1152.298,"context":null,"url":null}')

-- 797cf554f0a09752c523af8970e81a51b33cbbcb
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17575.412, -1161.845, 2.134, 0, 0, 0, '{"col pos x":17575.412,"col size x":0.58,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1161.845,"context":null,"url":null}')

-- f59ff491234abf8ecd9cb6c1b5aabb1eb2534916
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17574.594, -1170.359, 2.422, 0, 0, 0, '{"col pos x":17574.594,"col size x":0.55,"scale z":1,"col size y":0.509,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1170.359,"context":null,"url":null}')

-- 53f5b8db3af159525a899b77a628ef019635ae94
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17573.785, -1178.991, 2.683, 0, 0, 0, '{"col pos x":17573.785,"col size x":0.58,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1178.991,"context":null,"url":null}')

-- 59fb09ec3fea5c93c670f86cd6e776c88f6f140e
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17550.27, -1174.436, 2.612, 0, 0, 0, '{"col pos x":17550.27,"col size x":0.55,"scale z":1,"col size y":0.509,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1174.436,"context":null,"url":null}')

-- 0821329351d1fd3c790836a20d2b1aadb9a95bc6
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17550.943, -1164.59, 2.423, 0, 0, 0, '{"col pos x":17550.943,"col size x":0.51,"scale z":1,"col size y":0.469,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1164.59,"context":null,"url":null}')

-- fba0134b502c62dae3b4a2d44f814d767f9fa82a
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17552.1, -1155.955, 2.278, 0, 0, 0, '{"col pos x":17552.1,"col size x":0.61,"scale z":1,"col size y":0.569,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1155.955,"context":null,"url":null}')

-- 63ce45be5c046d442501a23f35ac3c4b2d38c566
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17553.318, -1147.598, 2.266, 0, 0, 0, '{"col pos x":17553.318,"col size x":0.57,"scale z":1,"col size y":0.529,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1147.598,"context":null,"url":null}')

-- c2c12e0e71a6ed5ea38f529b582ab0acd5901a14
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17554.52, -1140.365, 2.561, 0, 0, 0, '{"col pos x":17554.52,"col size x":0.55,"scale z":1,"col size y":0.509,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1140.365,"context":null,"url":null}')

-- f576ab11e036162c28441e890f666ae0d9cfcfa4
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17555.947, -1131.678, 3.091, 0, 0, 0, '{"col pos x":17555.947,"col size x":0.55,"scale z":1,"col size y":0.509,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1131.678,"context":null,"url":null}')

-- 3bc60b0c41069282c1e61a481d1ffc0bd2ec82f2
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17573.773, -1178.997, -0.835, 0, 0, 0, '{"col pos x":17573.773,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1178.997,"context":null,"url":null}')

-- 89f064d71784c80e12532ba3deda5a025bb39bc2
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17550.26, -1174.436, -0.931, 0, 0, 0, '{"col pos x":17550.26,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1174.436,"context":null,"url":null}')

-- 8d724488cca38bdf3193315d037f2ade35ea4722
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17550.941, -1164.584, -1.104, 0, 0, 0, '{"col pos x":17550.941,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1164.584,"context":null,"url":null}')

-- a3a4a034c840976b4adb1be620bec84f6e07aae4
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17552.088, -1155.952, -1.233, 0, 0, 0, '{"col pos x":17552.088,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1155.952,"context":null,"url":null}')

-- 22cae071510b7cbad2be6b45c8bc058f3221a7e0
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17553.312, -1147.595, -1.253, 0, 0, 0, '{"col pos x":17553.312,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1147.595,"context":null,"url":null}')

-- 7fa9580d8abbbd4a7a2b9eebf862b14421dfe753
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17554.516, -1140.414, -0.973, 0, 0, 0, '{"col pos x":17554.516,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1140.414,"context":null,"url":null}')

-- 68906e5aa8a3846ccc8f6a3b634783a3b7afe11c
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17555.951, -1131.664, -0.424, 0, 0, 0, '{"col pos x":17555.951,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1131.664,"context":null,"url":null}')

-- 58a899897d1842171e99d89b0c994600436d8c45
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17575.418, -1161.849, -1.353, 0, 0, 0, '{"col pos x":17575.418,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1161.849,"context":null,"url":null}')

-- f48532be240bd74ac64a3ef3730a65dc6f848934
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17574.592, -1170.361, -1.062, 0, 0, 0, '{"col pos x":17574.592,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1170.361,"context":null,"url":null}')

-- cee812db6f7e1ad99c0151698f465368d9afe7c3
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17576.684, -1152.298, -1.396, 0, 0, 0, '{"col pos x":17576.684,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":3.141,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1152.298,"context":null,"url":null}')

-- 6297a53a6489fa2731082c249cf75f20783871b1
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17577.727, -1144.253, -1.118, 0, 0, 0, '{"col pos x":17577.727,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1144.253,"context":null,"url":null}')

-- fc74e8934e2eba255561a40ec47e91f676996fa5
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17579.168, -1134.947, -0.549, 0, 0, 0, '{"col pos x":17579.168,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1134.947,"context":null,"url":null}')

-- 30d850e1ec4184c3aee95397da227128c9ae46e0
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17580.512, -1128.144, -0.224, 0, 0, 0, '{"col pos x":17580.512,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1128.144,"context":null,"url":null}')

-- 73d1ffef41ba9236e9147c5d7657e2c47d059b79
SceneEditor:spawnShape("ge_mission_reverbere.shape", 17557.029, -1124.363, -0.099, 0, 0, 0, '{"col pos x":17557.029,"col size x":0.577,"scale z":1,"col size y":0.545,"col orientation":0,"scale y":1,"col size z":3.679,"scale x":1,"col pos y":-1124.363,"context":null,"url":null}')

-- 167c24e1f20b9657e33b739252b5797de182947f
SceneEditor:spawnShape("ge_mission_table.shape", 17559.873, -1195.845, -0.274, 0, 0, -3.31, '{"col pos x":17559.873,"col size x":1.952,"scale z":1.29,"col size y":1.738,"col orientation":-0.18,"scale y":0.969,"col size z":1.065,"scale x":1.115,"col pos y":-1195.845,"context":null,"url":null}')

-- 4f5d29addbde2bdd08d005d05c8059160ab5c43e
SceneEditor:spawnShape("zo_bt_mon_flare_couloir.shape", 17557.047, -1124.373, 3.377, 0, 0, 0, '{"col pos x":17557.047,"col size x":0.58,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":2.895,"scale x":1,"col pos y":-1124.373,"context":null,"url":null}')

-- e8ed43e96fa4baca98d11ec39c43683da8d43741
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17596.988, -1244.061, 0.193, 0, 0, -1.49, '{"col pos x":17596.988,"col size x":9.117,"scale z":1,"col size y":12.851,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1244.061,"context":null,"url":null}')

-- b584110bb8e668a6e0b23bc214e156d21cbdc865
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17597.609, -1133.371, -0.038, 0, 0, 1.05, '{"col pos x":17597.609,"col size x":8.617,"scale z":1,"col size y":12.351,"col orientation":1.2,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1133.371,"context":null,"url":null}')

-- 01f3e3af3f76398b4dd0ddf30464f043b21ed32c
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17588.586, -1129.221, -0.057, 0, 0, -0.44, '{"col pos x":17588.586,"col size x":10.047,"scale z":1,"col size y":13.781,"col orientation":-0.37,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1129.221,"context":null,"url":null}')

-- 8f4fcfd618efde3f783d14b2bc75062248585e44
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17521.832, -1219.63, 2.742, 0, 0, -2.4, '{"col pos x":17521.832,"col size x":8.747,"scale z":1,"col size y":12.482,"col orientation":0.97,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1219.63,"context":null,"url":null}')

-- d2f2921a334f32e6748d497fd3f69ce2440e5727
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17513.295, -1212.054, 0.952, 0, 0, -3.1, '{"col pos x":17513.295,"col size x":8.797,"scale z":1,"col size y":12.531,"col orientation":0.28,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1212.054,"context":null,"url":null}')

-- 4497bc58a1670b718c1ea7479120328b25bbba9a
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17519.176, -1176.694, 0.933, 0, 0, -3.41, '{"col pos x":17519.176,"col size x":7.597,"scale z":1,"col size y":11.331,"col orientation":-0.42,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1176.694,"context":null,"url":null}')

-- 581013dfc2d345aca7cc8f6637b72f061b478d01
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17513.84, -1186.661, 1.721, 0, 0, -0.47, '{"col pos x":17513.84,"col size x":8.787,"scale z":1,"col size y":12.521,"col orientation":-0.49,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1186.661,"context":null,"url":null}')

-- fdb3f69ff8382597a609667fccb6e6157dff1345
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17510.438, -1199.481, -1.057, 0, 0, 0, '{"col pos x":17510.438,"col size x":11.147,"scale z":1,"col size y":14.881,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1199.481,"context":null,"url":null}')

-- 71599ce8d87b025895efd1e9ca05d88101b3ab47
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17522.025, -1166.083, 0.213, 0, 0, -3.13, '{"col pos x":17522.025,"col size x":8.357,"scale z":1,"col size y":12.091,"col orientation":-0.32,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1166.083,"context":null,"url":null}')

-- eb1d16df80d843a49eb78e50240fe82d2ce946f7
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17523.57, -1156.003, -0.239, 0, 0, -0.06, '{"col pos x":17523.57,"col size x":8.507,"scale z":1,"col size y":12.241,"col orientation":-0.1,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1156.003,"context":null,"url":null}')

-- 0697361cf82574a42c84a60009db9a04d29da008
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17524.234, -1143.267, -0.2, 0, 0, 0.01, '{"col pos x":17524.234,"col size x":11.567,"scale z":1,"col size y":15.301,"col orientation":-0.21,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1143.267,"context":null,"url":null}')

-- c860b9668b6d25f25e42dfdf3da237a6d0877a65
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17526.213, -1131.417, -0.131, 0, 0, 0, '{"col pos x":17526.213,"col size x":10.037,"scale z":1,"col size y":13.771,"col orientation":0,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1131.417,"context":null,"url":null}')

-- 28f9162475fc81c667ee26371cbc954228d54782
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17536.895, -1122.453, 0.036, 0, 0, 1.63, '{"col pos x":17536.895,"col size x":9.767,"scale z":1,"col size y":13.501,"col orientation":-1.57,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1122.453,"context":null,"url":null}')

-- 6c9169a6b4bc6ecf90b64ec2f47c0dc9a48e1b9b
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17547.479, -1122.329, 0.019, 0, 0, 0, '{"col pos x":17547.479,"col size x":10.807,"scale z":1,"col size y":14.541,"col orientation":-3.23,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1122.329,"context":null,"url":null}')

-- 84d125023b418c724f9e5a8c853de8c8d83777a0
SceneEditor:spawnShape("fo_s1_arbragrelot.shape", 17558.279, -1210.642, 1.384, 0, 0, -0.19, '{"col pos x":17557.828,"col size x":7.497,"scale z":1,"col size y":11.703,"col orientation":-1.43,"scale y":1,"col size z":47.418,"scale x":1,"col pos y":-1210.582,"context":null,"url":null}')

-- fb0cb83eaac1a0792baf4c3f969598aeacf65ed2
SceneEditor:spawnShape("waterbassinb08.shape", 17515.1, -1200.532, -0.92, 0, 0, 0, '{"col pos x":0,"col size x":103.912,"scale z":-0.35,"col size y":101.472,"col orientation":0,"scale y":-0.35,"col size z":0,"scale x":-0.35,"col pos y":-1200.532,"context":null,"url":null}')

-- 3df36545dc1ca087444dbd32ecd1d733f17f2e68
SceneEditor:spawnShape("fo_s2_bigroot_c.shape", 17532.701, -1197.433, -0.332, 0, 0, 0, '{"col pos x":17537.957,"col size x":12.116,"scale z":1,"col size y":6.727,"col orientation":0.18,"scale y":1,"col size z":5.622,"scale x":1,"col pos y":-1197.933,"context":null,"url":null}')

-- f08506c443d6ac4965e5444ef3b0c5bda2e982dc
SceneEditor:spawnShape("fo_s2_bigroot_b.shape", 17531.264, -1186.694, -0.692, 0, 0, -0.18, '{"col pos x":17533.838,"col size x":12.089,"scale z":1,"col size y":3.528,"col orientation":-0.04,"scale y":1,"col size z":4.065,"scale x":1,"col pos y":-1186.41,"context":null,"url":null}')

-- 69f266199c91235c5f90b5ca4a19e2685e411532
SceneEditor:spawnShape("fo_s2_bigroot_a.shape", 17529.273, -1199.5, 0.414, 0, 0, 2.36, '{"col pos x":17525.318,"col size x":15.256,"scale z":1,"col size y":9.019,"col orientation":1.96,"scale y":1,"col size z":8.57,"scale x":1,"col pos y":-1193.664,"context":null,"url":null}')

-- 511af950b766d0b6d505608dd6ff5d5421b0b428
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 17578.15, -1175.327, -0.776, 0, 0, 1.07, '{"col pos x":17578.15,"col size x":1.334,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":1.192,"scale x":1,"col pos y":-1175.327,"context":null,"url":null}')

-- 2667909387aa3d009c0639180659d79c3619106f
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 17585.812, -1154.154, -0.659, 0, 0, 1.57, '{"col pos x":17585.812,"col size x":1.334,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":1.192,"scale x":1,"col pos y":-1154.154,"context":null,"url":null}')

-- c1e1f27000f6efc9867da704551ea70224fb42b0
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 17543.416, -1169.153, -0.875, 0, 0, -1.46, '{"col pos x":17543.416,"col size x":1.334,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":1.192,"scale x":1,"col pos y":-1169.153,"context":null,"url":null}')

-- 89ee18898dde85d8e4eb2bdc929e8cb918bb2a65
SceneEditor:spawnShape("ge_mission_tombe_d.shape", 17583.453, -1172.451, -0.569, 0, 0, 1.19, '{"col pos x":17583.453,"col size x":0.793,"scale z":1,"col size y":0.822,"col orientation":0,"scale y":1,"col size z":2.124,"scale x":1,"col pos y":-1172.451,"context":null,"url":null}')

-- 72e68cac34d364eac0cdaa7437880c67952866a7
SceneEditor:spawnShape("ge_mission_tombe_d.shape", 17587.432, -1145.321, -0.477, 0, 0, 1.21, '{"col pos x":17587.432,"col size x":0.793,"scale z":1,"col size y":0.822,"col orientation":0,"scale y":1,"col size z":2.124,"scale x":1,"col pos y":-1145.321,"context":null,"url":null}')

-- ee690ee7a96d3b6ff1d46397ddd815181c2b9b85
SceneEditor:spawnShape("ge_mission_tombe_d.shape", 17546.105, -1158.715, -1.036, 0, 0, -1.62, '{"col pos x":17546.105,"col size x":0.793,"scale z":1,"col size y":0.822,"col orientation":0,"scale y":1,"col size z":2.124,"scale x":1,"col pos y":-1158.715,"context":null,"url":null}')

-- 70c88eb91245798de2f0084ab21b171ec46495da
SceneEditor:spawnShape("ge_mission_tombe_c.shape", 17582.074, -1184.143, -0.514, 0, 0, 0.98, '{"col pos x":17582.074,"col size x":0.902,"scale z":1,"col size y":0.57,"col orientation":0,"scale y":1,"col size z":1.564,"scale x":1,"col pos y":-1184.143,"context":null,"url":null}')

-- 9cb668c2b403ae5892594aa64e5c5c95d7497929
SceneEditor:spawnShape("ge_mission_tombe_c.shape", 17581.41, -1148.994, -1.063, 0, 0, 1.29, '{"col pos x":17581.41,"col size x":0.902,"scale z":1,"col size y":0.57,"col orientation":0,"scale y":1,"col size z":1.564,"scale x":1,"col pos y":-1148.994,"context":null,"url":null}')

-- 50fadda40205372e6062e4b25685160f575f4c3c
SceneEditor:spawnShape("ge_mission_tombe_c.shape", 17537.043, -1173.978, -0.85, 0, 0, -1.51, '{"col pos x":17537.043,"col size x":0.902,"scale z":1,"col size y":0.57,"col orientation":0,"scale y":1,"col size z":1.564,"scale x":1,"col pos y":-1173.978,"context":null,"url":null}')

-- a53fe216f9ff95af05c5a05d2c7daa0bcc8090df
SceneEditor:spawnShape("ge_mission_tombe_b.shape", 17578.957, -1158.484, -1.221, 0, 0, 1.28, '{"col pos x":17578.957,"col size x":1.029,"scale z":1,"col size y":0.515,"col orientation":0,"scale y":1,"col size z":1.93,"scale x":1,"col pos y":-1158.484,"context":null,"url":null}')

-- 1f23bc03cc985c06cefda70aa34824d780c8f013
SceneEditor:spawnShape("ge_mission_tombe_b.shape", 17584.629, -1162.692, -0.725, 0, 0, 1.1, '{"col pos x":17584.629,"col size x":1.029,"scale z":1,"col size y":0.515,"col orientation":0,"scale y":1,"col size z":1.93,"scale x":1,"col pos y":-1162.692,"context":null,"url":null}')

-- 3b895f80b463851c7abfcd7987801d2a42d6e7cb
SceneEditor:spawnShape("ge_mission_tombe_b.shape", 17549.273, -1142.623, -0.953, 0, 0, -1.41, '{"col pos x":17549.273,"col size x":1.029,"scale z":1,"col size y":0.515,"col orientation":0,"scale y":1,"col size z":1.93,"scale x":1,"col pos y":-1142.623,"context":null,"url":null}')

-- 0ff1407a5d85b5d8838b9ee5f24d8ae27995b0ef
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 17544.594, -1180.297, -0.793, 0, 0, -1.42, '{"col pos x":17544.594,"col size x":1.494,"scale z":1,"col size y":0.965,"col orientation":0,"scale y":1,"col size z":1.76,"scale x":1,"col pos y":-1180.297,"context":null,"url":null}')

-- 31e7208c0d198138ee594ad48cf8e86c941c4aba
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 17582.004, -1141.398, -0.777, 0, 0, 0.93, '{"col pos x":17582.004,"col size x":1.494,"scale z":1,"col size y":0.965,"col orientation":0,"scale y":1,"col size z":1.76,"scale x":1,"col pos y":-1141.398,"context":null,"url":null}')

-- d985b490239551022b19d51de96428c47781d492
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 17579.068, -1167.196, -0.947, 0, 0, 1.08, '{"col pos x":17579.068,"col size x":1.494,"scale z":1,"col size y":0.965,"col orientation":0,"scale y":1,"col size z":1.76,"scale x":1,"col pos y":-1167.196,"context":null,"url":null}')

-- 85e0b45b19924f6e39d93ac60daa8be998301455
SceneEditor:spawnShape("ge_mission_stele.shape", 17541.959, -1190.888, -0.896, 0, 0, 2.05, '{"col pos x":17541.959,"col size x":5.207,"scale z":1,"col size y":3.005,"col orientation":-1.17,"scale y":1,"col size z":4.882,"scale x":1,"col pos y":-1190.888,"context":null,"url":null}')

-- 3e3d37f75f9ece0c7e8a16dc44092884dc8e7d91
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 17550.252, -1135.555, -0.58, 0, 0, -1.26, '{"col pos x":17550.252,"col size x":1.334,"scale z":1,"col size y":0.539,"col orientation":0,"scale y":1,"col size z":1.192,"scale x":1,"col pos y":-1135.555,"context":null,"url":null}')

-- 9e9639277c3010420f6b5612aa1b2d0d6e75c24f
SceneEditor:spawnShape("ge_mission_tombe_d.shape", 17541.895, -1152.953, -0.977, 0, 0, 1.5, '{"col pos x":17541.895,"col size x":0.793,"scale z":1,"col size y":0.822,"col orientation":0,"scale y":1,"col size z":2.124,"scale x":1,"col pos y":-1152.953,"context":null,"url":null}')

-- e487729427485e79bcbe5e6c7826fc559eb95ab8
SceneEditor:spawnShape("ge_mission_tombe_c.shape", 17548.703, -1150.787, -1.14, 0, 0, -1.6, '{"col pos x":17548.703,"col size x":0.902,"scale z":1,"col size y":0.57,"col orientation":0,"scale y":1,"col size z":1.564,"scale x":1,"col pos y":-1150.787,"context":null,"url":null}')

-- a8db3e2705458285149d80468e2cdb938a958a87
SceneEditor:spawnShape("ge_mission_tombe_b.shape", 17543.203, -1139.221, -0.628, 0, 0, -1.8, '{"col pos x":17543.203,"col size x":1.029,"scale z":1,"col size y":0.515,"col orientation":0,"scale y":1,"col size z":1.93,"scale x":1,"col pos y":-1139.221,"context":null,"url":null}')

-- 5436e9b76a977171079731bca2dca2f733de5d3a
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 17539.387, -1164.442, -0.832, 0, 0, -1.8, '{"col pos x":17539.387,"col size x":1.494,"scale z":1,"col size y":0.965,"col orientation":0,"scale y":1,"col size z":1.76,"scale x":1,"col pos y":-1164.442,"context":null,"url":null}')
--############### SE_deco_Memorial 12437#################


