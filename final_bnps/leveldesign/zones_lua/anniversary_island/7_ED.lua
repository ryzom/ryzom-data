--############### SE_DECO 12333#################
-- GROUP: buildings_tryker
-- e2d3e975499cf154c5779a5518102d0c7328a59b
SceneEditor:spawnShape("tr_agora_village_a.shape", 17190.195, -1111.461, 12, 0, 0, 3.1, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17190.195,"col pos y":-1111.461,"col orientation":0.45,"col size x":54.397,"col size y":58.073,"col size z":27.292,"context":null,"url":null}')

-- bf1feb7513305a4ed8783002d761bec9aaa8785d
SceneEditor:spawnShape("tr_saucisse_01_village_a.shape", 17273.09, -1035.82, 0.28, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17273.09,"col pos y":-1035.82,"col orientation":0,"col size x":21.687,"col size y":25.363,"col size z":70.401,"context":null,"url":null}')

-- 26410c432f9cf79bd02e2adbd3b3038db0242051
SceneEditor:spawnShape("ge_mission_tente_tr.shape", 17204.359, -1054.626, -0.524, 0, 0, 4.21, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17204.359,"col pos y":-1054.626,"col orientation":0,"col size x":6.627,"col size y":7.862,"col size z":9.323,"context":null,"url":null}')

-- e36eb0812a88b5f0f096e8218f586d61f051fcd8
SceneEditor:spawnShape("ge_mission_tente_tr.shape", 17254.07, -1097.674, -0.01, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17254.07,"col pos y":-1097.674,"col orientation":0,"col size x":6.627,"col size y":7.862,"col size z":9.323,"context":null,"url":null}')

-- 3584df7dcd782adb72d8116017e99712f09ad7ee
SceneEditor:spawnShape("ge_mission_tente_tr.shape", 17232.9, -1032.048, -0.691, 0, 0, -2.87, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17232.9,"col pos y":-1032.048,"col orientation":0,"col size x":6.627,"col size y":7.862,"col size z":9.323,"context":null,"url":null}')

-- 70a49b4e842f0d4a8adabee3391adeb7af7226cd
SceneEditor:spawnShape("gen_bt_silo.shape", 17188.535, -1058.377, 1.018, 0, 0, 0.93, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17188.535,"col pos y":-1058.377,"col orientation":0.89,"col size x":4.181,"col size y":5.563,"col size z":10.403,"context":null,"url":null}')

-- 0394f3795b0dc8492c7a7c5a46050ed0fd58422a
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 17250.889, -1094.803, -0.003, 0, 0, -1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17250.889,"col pos y":-1094.803,"col orientation":-1.6,"col size x":2.145,"col size y":1.103,"col size z":2.481,"context":null,"url":null}')

-- c4f7f3e5a9ba45a82a54911d940b1266499393f1
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 17207.15, -1059.102, -0.716, 0, 0, 0, '{"scale x":0.833,"scale y":0.833,"scale z":0.833,"col pos x":17207.15,"col pos y":-1059.102,"col orientation":0,"col size x":1.173,"col size y":1.078,"col size z":1.192,"context":null,"url":null}')

-- 42cd23c1b281a7eef97dd23fc97286ad3b573cb6
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 17229.639, -1035.278, -1.163, 0, 0, 0, '{"scale x":0.833,"scale y":0.833,"scale z":0.833,"col pos x":17229.639,"col pos y":-1035.278,"col orientation":0,"col size x":1.173,"col size y":1.078,"col size z":1.192,"context":null,"url":null}')


-- GROUP: plants_tryker
-- c51154f95a0f6d487ec1ea18b76dccfdbecf5782
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17274.824, -990.788, -0.072, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17274.824,"col pos y":-990.788,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 3ec98ec34c654c79cac4c52a2dd39f233c32387f
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17259.625, -1026.324, -0.023, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17259.625,"col pos y":-1026.324,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 3ef7b66ec2b036e4ad7fc50f7b5260b0c245ac90
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17272.857, -1117.256, -0.012, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17272.857,"col pos y":-1117.256,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- a21a81d12bb6026c1c46420b7b493d3c975b5e45
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17233.62, -1008.02, -0.48, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17233.62,"col pos y":-1008.02,"col orientation":0,"col size x":10.737,"col size y":14.471,"col size z":52.171,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_deco_Entrance 12462#################
-- GROUP: entrance
-- 18b655025f46ea92b29d12fe5560a0fe4d08e193
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17272.762, -968.669, -0.033, 0, 0, 0, '{"col pos x":17272.762,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-968.669,"context":null,"url":null}')

-- dd4b883c2f25b5fe51863ac97492296585927ac4
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17271.523, -968.636, -0.037, 0, 0, 0, '{"col pos x":17271.523,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-968.636,"context":null,"url":null}')

-- 884171300c3b6cdd4acf24af012677039d5642ce
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 17263.938, -971.752, -0.215, 0, 0, 0, '{"col pos x":17263.938,"col size x":3.835,"scale z":1,"col size y":4.365,"col orientation":0,"scale y":1,"col size z":3.046,"scale x":1,"col pos y":-971.752,"context":null,"url":null}')

-- 67acba6a60d9d41b2571016ff83aea05e1838029
SceneEditor:spawnShape("ge_partylamp_02.shape", 17248.078, -966.464, -0.226, 0, 0, -1.19, '{"col pos x":17250.867,"col size x":5.636,"scale z":1.55,"col size y":0.743,"col orientation":-1.19,"scale y":1.55,"col size z":5.18,"scale x":1.55,"col pos y":-973.264,"context":null,"url":null}')

-- 374e6029e52c917f27850660486d891a650d4110
SceneEditor:spawnShape("ge_partylamp_02.shape", 17255.012, -979.579, -0.929, 0, 0, -0.98, '{"col pos x":17259.363,"col size x":6.236,"scale z":1.62,"col size y":0.663,"col orientation":-0.98,"scale y":1.62,"col size z":5.18,"scale x":1.62,"col pos y":-985.968,"context":null,"url":null}')

-- 20ed6c75980c808659180dac62edac58484e8402
SceneEditor:spawnShape("ge_partylamp_02.shape", 17264.352, -991.987, -0.661, 0, 0, -0.85, '{"col pos x":17269.697,"col size x":6.036,"scale z":1.62,"col size y":0.743,"col orientation":-0.74,"scale y":1.62,"col size z":5.18,"scale x":1.62,"col pos y":-997.578,"context":null,"url":null}')

-- 2eecaf813ab303de6a73cf5fb3b8d2131b2d14a0
SceneEditor:spawnShape("ge_partylamp_02.shape", 17275.906, -1001.72, -0.031, 0, 0, -0.54, '{"col pos x":17282.449,"col size x":5.696,"scale z":1.58,"col size y":0.943,"col orientation":-0.45,"scale y":1.58,"col size z":5.18,"scale x":1.58,"col pos y":-1005.099,"context":null,"url":null}')

-- e3f0f8bfd62ca07f5ebaa70eb1db39db545cc8bc
SceneEditor:spawnShape("ge_mission_tente_tr.shape", 17271.469, -975.266, -0.108, 0, 0, -0.83, '{"col pos x":17271.979,"col size x":10.137,"scale z":1.88,"col size y":10.482,"col orientation":0,"scale y":1.88,"col size z":9.323,"scale x":1.88,"col pos y":-975.266,"context":null,"url":null}')

-- 6bf603aaa3327bf06bd1d9518ecba0406a85e221
SceneEditor:spawnShape("tr_acc_lit.shape", 17276.734, -968.957, 0.811, 0, 0, 0, '{"col pos x":17276.734,"col size x":4.041,"scale z":1,"col size y":1.997,"col orientation":0,"scale y":1,"col size z":1.765,"scale x":1,"col pos y":-968.957,"context":null,"url":null}')

-- 8895a6e48f9ddd3de3b95d7c318486029d11a043
SceneEditor:spawnShape("GE_Mission_tente.shape", 17262.439, -964.485, -0.031, 0, 0, -1.27, '{"col pos x":17262.439,"col size x":8.378,"scale z":1.65,"col size y":9.665,"col orientation":0.28,"scale y":1.65,"col size z":5.034,"scale x":1.65,"col pos y":-964.485,"context":null,"url":null}')
--############### SE_deco_Entrance 12462#################
