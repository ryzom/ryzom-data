--############### SE_DECO 12333#################
-- GROUP: buildings_fyros
-- e17608803ece60c7327ed581feeb2a68f69b4c23
SceneEditor:spawnShape("fy_con_defensetower.shape", 17324.518, -584.447, -0.866, 0, 0, -0.7, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17324.518,"col pos y":-584.447,"col orientation":-0.73,"col size x":2.569,"col size y":5.293,"col size z":15.32,"context":null,"url":null}')

-- 09de25176885e4977427e6d04e764bc5f7f62caf
SceneEditor:spawnShape("fy_cn_caravanserail_a_nb05.shape", 17369.373, -613.715, -0.05, 0, 0, -1.62, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17369.373,"col pos y":-613.715,"col orientation":-1.62,"col size x":43.688,"col size y":15.045,"col size z":8.336,"context":null,"url":null}')

-- 489c79710d9ce68567499a207c31dc77b967de8b
SceneEditor:spawnShape("fy_con_defensetower.shape", 17298.156, -612.311, -0.98, 0, 0, -0.7, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17298.156,"col pos y":-612.311,"col orientation":-0.66,"col size x":2.809,"col size y":5.793,"col size z":15.32,"context":null,"url":null}')

-- d48246e4ee757551a69cd1ae95fd5d854457e471
SceneEditor:spawnShape("fy_feu_foret.ps", 17320.344, -589.425, -1.144, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-12.578,"col pos y":-589.425,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')

-- fd35b48f90519ce20ce80def16994f40a2dda108
SceneEditor:spawnShape("fy_feu_foret.ps", 17317.318, -592.483, -1.27, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-13.365,"col pos y":-592.483,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')

-- 1d053510900987bff64e9b03c90a8c47ddba8ff2
SceneEditor:spawnShape("fy_feu_foret.ps", 17314.52, -595.149, -1.42, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-12.863,"col pos y":-595.149,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')

-- 74702ff0fe39997129a7f507b6385766f6e64cdf
SceneEditor:spawnShape("fy_feu_foret.ps", 17311.346, -598.008, -1.543, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-12.332,"col pos y":-598.008,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')

-- 7b47fcd92bdc5f12bcc34444f2b37e0d6d88aa1e
SceneEditor:spawnShape("fy_feu_foret.ps", 17308.148, -600.922, -1.507, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-11.928,"col pos y":-600.922,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')

-- 35a9816fc758db691ed4f6b312f1e43389bfc56a
SceneEditor:spawnShape("fy_feu_foret.ps", 17305.473, -603.913, -1.505, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-10.529,"col pos y":-603.913,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')

-- 13db1df6e6a0852196a608889607804b729a114e
SceneEditor:spawnShape("fy_feu_foret.ps", 17302.854, -606.936, -1.342, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":3,"col pos x":-9.449,"col pos y":-606.936,"col orientation":0,"col size x":2.06,"col size y":1.936,"col size z":2.854,"context":null,"url":null}')


-- GROUP: plants_fyros
-- aaeb7a50a9a17e2eccea7e1a3c1c448b1bc9f2f2
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17429.535, -551.477, 0.013, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17429.535,"col pos y":-551.477,"col orientation":0,"col size x":3.925,"col size y":5.038,"col size z":15.054,"context":null,"url":null}')

-- 3701ca7b895107fea21d6e8a89bf0c896c027135
SceneEditor:spawnShape("fy_s1_burnedtree_b.shape", 17435.131, -620.543, 0.033, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17435.131,"col pos y":-620.543,"col orientation":0,"col size x":7.405,"col size y":7.644,"col size z":15.87,"context":null,"url":null}')

-- eb1ee0cf72749fc04c0456bc5fbbee776b32e6cb
SceneEditor:spawnShape("fy_s2_savantree_a.shape", 17412.818, -626.718, 0.425, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17412.818,"col pos y":-627.238,"col orientation":0,"col size x":1.187,"col size y":1.72,"col size z":11.066,"context":null,"url":null}')

-- 8bf7d2ac11808d0edbeebba5d7b1068ccec8b634
SceneEditor:spawnShape("fy_s1_baobab_c.shape", 17341.383, -621.345, -0.507, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17341.383,"col pos y":-621.345,"col orientation":0,"col size x":5.489,"col size y":5.832,"col size z":26.163,"context":null,"url":null}')

-- 53d67500f6bf9f4661aa28a1b3f30d687aaba72e
SceneEditor:spawnShape("fy_s1_burnedtree_c.shape", 17372.389, -577.213, 2.446, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17372.389,"col pos y":-577.213,"col orientation":0,"col size x":5.492,"col size y":5.274,"col size z":15.87,"context":null,"url":null}')
--############### SE_DECO 12333#################
