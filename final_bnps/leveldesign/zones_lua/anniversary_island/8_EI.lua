--############### SE_DECO 12333#################
-- GROUP: buildings_zorai
-- 7a3b2ce73c51d7bcc6705a72c9df5fcc8ac88ece
SceneEditor:spawnShape("zo_mairie_ext_zo_mairie_village_a.shape", 18070, -1170, 0, 0, 0, 0.5, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18070,"col pos y":-1170,"col orientation":0.49,"col size x":26.395,"col size y":33.173,"col size z":18.631,"context":null,"url":null}')

-- b8d114ecdd7390d94edd0f278b770d62c50c7fee
SceneEditor:spawnShape("zo_mairie_couloir.shape", 18070, -1170, 8.82, 0, 0, 0.5, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-1170,"col orientation":0,"col size x":6.946,"col size y":12.463,"col size z":2.913,"context":null,"url":null}')

-- 2b0780f6b92c5a0701d10ec854dbd7a37503346b
SceneEditor:spawnShape("zo_agora_bras_zo_agora_village_a.shape", 18070, -1170, 23, 0, 0, 0.54, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-1170,"col orientation":0,"col size x":27.263,"col size y":21.649,"col size z":19.816,"context":null,"url":null}')

-- 915b5af20800081d8e76112cc8f12a04663c2fe2
SceneEditor:spawnShape("zo_agora_ambre_zo_agora_village_a.shape", 18070, -1170, 12, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-1170,"col orientation":0,"col size x":2.791,"col size y":2.791,"col size z":17.333,"context":null,"url":null}')

-- d5911270cf5d0f99e8da0710b168c528a634ca4d
SceneEditor:spawnShape("zo_module_commerce_zo_comm_2_village_a.shape", 18036.469, -1168.553, 2.611, 0, 0, 2.8, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18036.469,"col pos y":-1168.553,"col orientation":-0.35,"col size x":7.123,"col size y":9.497,"col size z":6.979,"context":null,"url":null}')

-- 1b873a6d82a9b5600060fbfa203f6173ad10e030
SceneEditor:spawnShape("zo_module_atelier_zo_atelier_6_village_a.shape", 18031.121, -1131.483, 0.033, 0, 0, 1.8, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18031.121,"col pos y":-1131.483,"col orientation":1.77,"col size x":7.123,"col size y":9.497,"col size z":5.7,"context":null,"url":null}')

-- 5ee9f8847f7a9c9daac9884bd3a9d1b79a6d7b77
SceneEditor:spawnShape("zo_imm_boule_nb_01.shape", 18001, -1180, 11.6, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":-4.848,"col pos y":-1180,"col orientation":0,"col size x":8,"col size y":8,"col size z":8,"context":null,"url":null}')

-- 50ad838e7a7dbc0b0a603a338daedf3f97ff492b
SceneEditor:spawnShape("zo_imm_arbre_zo_imm_1_village_c.shape", 18001, -1180, 15, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":-1.852,"col pos y":-1180,"col orientation":0,"col size x":12.467,"col size y":14.416,"col size z":17.81,"context":null,"url":null}')

-- 3de7e7df65501e42109903b08909458bf34d2028
SceneEditor:spawnShape("zo_exterieur_immeuble_zo_imm_4_village_a.shape", 18001, -1180, -1, 0, 0, -0.4, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18001,"col pos y":-1180,"col orientation":-0.87,"col size x":37.901,"col size y":36.444,"col size z":47.918,"context":null,"url":null}')

-- a575c6484d4a231474e3bbb843be55aa3bfe0b61
SceneEditor:spawnShape("zo_acc_ascenseur_ext1_imm_4_village_a.shape", 18005.172, -1175.931, 6, 0, 0, 1.69, '{"scale x":1.22,"scale y":1,"scale z":0.971,"col pos x":0,"col pos y":-1175.931,"col orientation":0,"col size x":4,"col size y":5.437,"col size z":8.237,"context":null,"url":null}')

-- 1e56a574df09c077b730eac6a8203fda68310959
SceneEditor:spawnShape("zo_acc_ascenseur_ext1_imm_4_village_a.shape", 18002.379, -1185.676, 6, 0, 0, -0.4, '{"scale x":1.22,"scale y":1,"scale z":0.971,"col pos x":0,"col pos y":-1185.676,"col orientation":0,"col size x":4,"col size y":5.437,"col size z":8.237,"context":null,"url":null}')

-- 16b1af23e1c521a211d04281574338577d6660bd
SceneEditor:spawnShape("zo_acc_ascenseur_ext1_imm_4_village_a.shape", 17995.412, -1178.341, 6, 0, 0, -2.49, '{"scale x":1.22,"scale y":1,"scale z":0.971,"col pos x":-1.104,"col pos y":-1178.341,"col orientation":0,"col size x":4,"col size y":5.437,"col size z":8.237,"context":null,"url":null}')


-- GROUP: plants_zorai
-- 7cbfde2d3c1f6e9e06031f511adc0b0e5629d2bb
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17951.027, -1162.081, -1.391, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17951.027,"col pos y":-1162.081,"col orientation":0,"col size x":0.461,"col size y":0.482,"col size z":16.447,"context":null,"url":null}')

-- fe9d3337d838b99b768fcf6273544c538d492671
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17975.531, -1170.254, -0.117, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17975.531,"col pos y":-1170.254,"col orientation":0,"col size x":0.681,"col size y":0.722,"col size z":16.447,"context":null,"url":null}')

-- 2f43506afa6b563207f037f812c981bb50060516
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17994.605, -1138.126, -0.042, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17994.605,"col pos y":-1138.126,"col orientation":0,"col size x":0.471,"col size y":0.572,"col size z":16.447,"context":null,"url":null}')

-- bbc172549d8ffd07a91b802e26c05f64b6b8cf1b
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17980.561, -1168.039, -0.01, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17980.561,"col pos y":-1168.039,"col orientation":0,"col size x":4.52,"col size y":5.345,"col size z":25.035,"context":null,"url":null}')

-- 03e9410503f2907e72b7754e6a90a6c5a3e5dd8c
SceneEditor:spawnShape("ju_s3_fantree.shape", 18030.777, -1120.65, 0.061, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18030.777,"col pos y":-1120.65,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 7205904053c60fd95e8f9073b286db35aae9362b
SceneEditor:spawnShape("ju_s3_fantree.shape", 18032.906, -1145.931, 1.042, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18032.906,"col pos y":-1145.931,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 47ee4c7dc80f66c6cc75c3bee2bdc5f48ac41c0e
SceneEditor:spawnShape("ju_s3_fantree.shape", 18000.699, -1148.136, 0.176, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18000.699,"col pos y":-1148.136,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 8dae16282b66cc8124b117b00f41ea1f148c69e8
SceneEditor:spawnShape("ju_s3_fantree.shape", 17995.045, -1210.311, 3.781, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17995.045,"col pos y":-1210.311,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 07457591061baf9e8279c796032d11316d20bf00
SceneEditor:spawnShape("ju_s3_fantree.shape", 17985.68, -1243.733, 14.807, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17985.68,"col pos y":-1243.733,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- fa74c8e7c274b3decc3a48820fb689f08f393dd8
SceneEditor:spawnShape("ju_s3_fantree.shape", 18026.955, -1212.921, 0.846, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18026.955,"col pos y":-1212.921,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 54609d98266eb5f40e14852435da8a76f2cc4ef9
SceneEditor:spawnShape("ju_s3_fantree.shape", 18028.295, -1181.548, 0.271, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18028.295,"col pos y":-1181.548,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- c68f03d58b62d7759f04b5cc1604a2e7765518c0
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 17991.701, -1154.675, 0.341, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17991.701,"col pos y":-1154.675,"col orientation":0,"col size x":5.319,"col size y":5.665,"col size z":4.997,"context":null,"url":null}')

-- 8647dea7091179eb05cad2b20fa57df9c78d9898
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 17949.113, -1189.995, -0.742, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17949.113,"col pos y":-1189.995,"col orientation":0,"col size x":5.319,"col size y":5.665,"col size z":4.997,"context":null,"url":null}')
--############### SE_DECO 12333#################

