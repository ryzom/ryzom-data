--############### SE_DECO 12333#################
-- GROUP: buildings_karavan
-- c73ff696e26fa77774093ceed3dc9e824b6cf5dc
SceneEditor:spawnShape("ge_mission_temple_of_jena.ps", 17640.705, -606.583, -0.669, 0, 0, 1, '{"scale x":2,"scale y":2,"scale z":2,"col pos x":17640.705,"col pos y":-606.583,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 124382041df3c90275cb8cd1c775bf4ca709864e
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.672, -630.931, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.672,"col pos y":-630.931,"col orientation":1.57,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- f239842e51a38c56fc6155816fa8e9576981fd74
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.672, -621.592, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.672,"col pos y":-621.592,"col orientation":1.57,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 5fbd97a1b7934cbee50f659177a469ea3e5334d4
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.74, -612.368, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.74,"col pos y":-612.368,"col orientation":4.716,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 9fc6a60a9c2c0a57f3c5f85b17b317adb824d69d
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.74, -603.083, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.74,"col pos y":-603.083,"col orientation":1.59,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- d54c63582600d71eda1a8efa00c105a7c2fd887d
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.74, -593.854, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.74,"col pos y":-593.854,"col orientation":1.55,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 91d213263f013a08c9ea22b7ed2c71ea7c76e222
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.74, -584.63, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.74,"col pos y":-584.63,"col orientation":1.54,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- be76d082ecb40bfd9c7225cff98b54a573fa9768
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.74, -575.486, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.74,"col pos y":-575.486,"col orientation":1.56,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- fffe6811fd61e59e1806c24b6f5921eb057a0201
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -631.166, 0.22, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-631.166,"col orientation":1.58,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- b2b16f90b5aa9143dfd975ab7cb63a69327132d6
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -621.975, -0.351, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-621.975,"col orientation":1.56,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 51e62cf03ae170c9d7b410d3c40f91306643dd48
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -603.37, -0.475, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-603.37,"col orientation":1.57,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- b01b3de4261b07329c4ff9000ee81f2ef1340dd8
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -594.078, 0.658, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-594.078,"col orientation":1.59,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 52d18c191bd98fe20c06da0e1a76087633b07e60
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -612.751, -0.523, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-612.751,"col orientation":1.57,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 6e3a29f1e7bd25e3ba2585dfcf77d5c0584a34ea
SceneEditor:spawnShape("ge_mission_etendard_karavan.ps", 17646.586, -635.743, 3, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17646.586,"col pos y":-635.743,"col orientation":0,"col size x":2.426,"col size y":0.638,"col size z":9.347,"context":null,"url":null}')

-- de1e9656dd0ea05b0674d072ca570160b25cc7b3
SceneEditor:spawnShape("ge_mission_etendard_karavan.ps", 17633.984, -635.743, 3, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17633.984,"col pos y":-635.743,"col orientation":0,"col size x":2.426,"col size y":0.638,"col size z":9.347,"context":null,"url":null}')
--############### SE_DECO 12333#################
