--############### SE_DECO 12333#################
-- GROUP: plants_center
-- a26dd6d7827acd19b454fdded1f3e53bd892a8cd
SceneEditor:spawnShape("fy_s2_savantree_a.shape", 17555.402, -933.283, 10.014, 0, 0, 0, '{"col size x":1.467,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.48,"col size z":11.066,"scale y":1,"col pos y":-933.838,"col pos x":17555.402,"context":null,"url":null}')

-- e8bc847e57c8d17bc3fb124724a5ef6a13c1a331
SceneEditor:spawnShape("fy_s2_savantree_a.shape", 17598.771, -948.753, 10.009, 0, 0, 0, '{"col size x":1.817,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.52,"col size z":11.066,"scale y":1,"col pos y":-949.177,"col pos x":17598.771,"context":null,"url":null}')

-- 276bc5971d0e70598e71706e9bba2e602ac45ad2
SceneEditor:spawnShape("fo_s2_arbragrelot.shape", 17574.447, -946.069, 10.087, 0, 0, 0, '{"col size x":3.179,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.033,"col size z":28.184,"scale y":1,"col pos y":-946.069,"col pos x":17574.447,"context":null,"url":null}')


-- GROUP: center
-- 31ea731e39dfd22869999a7ffca36106c621fbf8
SceneEditor:spawnShape("ge_partylamp_03.shape", 17536.58, -935.418, 9.429, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17536.58,"col pos y":-935.418,"col orientation":0,"col size x":1.525,"col size y":1.525,"col size z":5.267,"context":null,"url":null}')

-- 158c68ab37e200aa6b51748ccecdf2ea405b0e64
SceneEditor:spawnShape("ge_partylamp_03.shape", 17545.84, -957.306, 10.267, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17545.84,"col pos y":-957.306,"col orientation":0,"col size x":2.075,"col size y":2.075,"col size z":5.267,"context":null,"url":null}')

-- 3bfa53cff60a87f914dc018637f42a5a6298e912
SceneEditor:spawnShape("ge_partylamp_03.shape", 17538.699, -807.069, 10.156, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17538.699,"col pos y":-807.069,"col orientation":0,"col size x":1.605,"col size y":1.605,"col size z":5.267,"context":null,"url":null}')

-- 5a16fe5bc2687b313e84e73095b067a07ebbc8a7
SceneEditor:spawnShape("ge_partylamp_03.shape", 17535.436, -838.609, 9.576, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17535.436,"col pos y":-838.609,"col orientation":0,"col size x":1.165,"col size y":1.165,"col size z":5.267,"context":null,"url":null}')

-- 3c92250fcc19e0d3b7c0df408e15d2050ab6f926
SceneEditor:spawnShape("ge_partylamp_03.shape", 17535.113, -872.767, 8.257, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17535.113,"col pos y":-872.767,"col orientation":0,"col size x":1.575,"col size y":1.575,"col size z":5.267,"context":null,"url":null}')

-- 66c867a1e8aa9ecde303e27f7a417ab25cc4f7cc
SceneEditor:spawnShape("ge_partylamp_03.shape", 17529.834, -904.928, 8.253, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17529.834,"col pos y":-904.928,"col orientation":0,"col size x":1.355,"col size y":1.355,"col size z":5.267,"context":null,"url":null}')


-- GROUP: center_20
-- 51705ae3d242f1385a9d85cb91d1a23187aa34b3
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17589.61, -864.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 01257ec8e4732fb648fd5b5bccc1f597b8f3cb2c
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17585.11, -866.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- ca9006293bc54ec14b2710ae5f6ad53a0ccd4474
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17582.11, -867.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 36f90eb23047d3f2d88902f6fdef4b19a259ad26
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17577.61, -867.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a34978950f589f4c319a739dd56709cdca6dd6b0
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17574.61, -866.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 7ae2463fbff01a313398829f5dea71b90a5ecc1c
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17571.61, -861.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 73adf9bc95726acdf8608abe3707914ef57c6189
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17571.61, -855.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 545ebd3f80ac2d955c7492760f5a1487aaf85f1c
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17574.61, -851.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- ee0c1c4d7c30165117b0bb4b47051930a5c8c6a1
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17577.61, -848.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- f1d084cb4fec2222a3a0abcc19087d8405f356f5
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17580.61, -845.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a38e5205ff9a68ef8536e7f8bd32241ac883c35d
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17583.61, -842.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 11a65d2abadec521cbdee15461605355cc8519b2
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17588.11, -839.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- f61dcc514ee12b93c2ea7532b1f7f0c0595f2dc3
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17592.61, -837.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- c4bf853d0b7ac4cf5dfe0306c5a1fe9137f1ff88
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17597.11, -836.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 649a15cda9aca32a6b9662010b5ae53d7c5f4b8d
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17597.11, -888.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 152639dddbc23113615ba74ef827149598c9b00e
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17592.61, -891.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 66e8088063956a083b3f07c7e239db3f5d01331b
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17589.61, -896.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 689752cb409a568a2d32bd9b2cb4d3fc26e97dec
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17585.11, -900.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 40cac4dd2b2edc498357d8f6630497f9cd32d26e
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17580.61, -905.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a0c7a8026192887e95b03b03a7d03c20364aaa37
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17576.11, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- ec08d71b2a101b2c6c894a1db18c4734c9a28341
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17580.61, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 8be9cd7de58826c33185bf6158fbde763e2402dd
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17585.11, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 2a0a422f2f6c2895f8ae9d76649013b95a00f5ed
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17589.61, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- cc1d159ebd9a3953f782966e0b6cbbe343f1cf1f
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17594.11, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 95fff1d3e9202198d29bd98f333f9526ab27abfa
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17598.61, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_deco_Entrance 12462#################
-- GROUP: entrance
-- e76865be3d084a51bdb1d384b1cdf401aedcec72
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17465.479, -868.677, -0.951, 0, 0, 0, '{"col pos x":17465.479,"col size x":0.571,"scale z":1,"col size y":0.583,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-868.677,"context":null,"url":null}')

-- 75abdcade16b1a956b6dea421f271311704b64f2
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17466.564, -906.574, 1.845, 0, 0, 0, '{"col pos x":17466.564,"col size x":0.501,"scale z":1,"col size y":0.523,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-906.574,"context":null,"url":null}')

-- ab6a9c2961fcc46764fae97c29b6dadca4c5906f
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17439.512, -908.716, 0.027, 0, 0, 0, '{"col pos x":17439.512,"col size x":0.421,"scale z":1,"col size y":0.493,"col orientation":2.922,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-908.716,"context":null,"url":null}')

-- 82604393de92c97399be04c92c3d636d0d832a4c
SceneEditor:spawnShape("ge_partylamp_03.shape", 17482.145, -907.935, 0.63, 0, 0, 0.89, '{"col pos x":17482.145,"col size x":3.315,"scale z":2.22,"col size y":3.315,"col orientation":0.08,"scale y":2.22,"col size z":5.267,"scale x":2.22,"col pos y":-907.935,"context":null,"url":null}')

-- fccd1bfca8c49f2046526073e2069944455c1111
SceneEditor:spawnShape("ge_partylamp_03.shape", 17481.439, -867.027, -1.431, 0, 0, 0.68, '{"col pos x":17481.439,"col size x":3.515,"scale z":2.45,"col size y":3.515,"col orientation":0,"scale y":2.45,"col size z":5.267,"scale x":2.45,"col pos y":-867.027,"context":null,"url":null}')

-- 900267e15b0fd48f0cec690ffdf3ccac4e6f817b
SceneEditor:spawnShape("ge_partylamp_02.shape", 17472.332, -866.477, -1.19, 0, 0, -0.06, '{"col pos x":17477.758,"col size x":2.656,"scale z":1.44,"col size y":0.743,"col orientation":-0.07,"scale y":1.44,"col size z":5.18,"scale x":1.44,"col pos y":-866.869,"context":null,"url":null}')

-- 9ee4e3356e7ad9ebe25206d4a3a0152bbb9c141c
SceneEditor:spawnShape("ge_partylamp_02.shape", 17458.25, -865.534, -0.72, 0, 0, -0.08, '{"col pos x":17465.299,"col size x":5.716,"scale z":1.47,"col size y":0.743,"col orientation":-0.09,"scale y":1.47,"col size z":5.18,"scale x":1.47,"col pos y":-866.134,"context":null,"url":null}')

-- fc9b2ae9a0a5550223b4ed6744e265fc35150d58
SceneEditor:spawnShape("ge_partylamp_02.shape", 17443.691, -864.355, -0.037, 0, 0, -0.09, '{"col pos x":17451.098,"col size x":5.896,"scale z":1.56,"col size y":0.743,"col orientation":-0.09,"scale y":1.56,"col size z":5.18,"scale x":1.56,"col pos y":-864.956,"context":null,"url":null}')

-- 07d078b5d703fcffcf1ebd31b88aba3dd766eb3a
SceneEditor:spawnShape("ge_partylamp_02.shape", 17473.654, -908.577, 1.784, 0, 0, 0.07, '{"col pos x":17478.943,"col size x":2.316,"scale z":1.39,"col size y":0.743,"col orientation":0.08,"scale y":1.39,"col size z":5.18,"scale x":1.39,"col pos y":-908.168,"context":null,"url":null}')

-- 78a5654fd566632b4231d1a3d12a0cacb095ad53
SceneEditor:spawnShape("ge_partylamp_02.shape", 17460.361, -909.444, 1.643, 0, 0, 0.06, '{"col pos x":17466.986,"col size x":5.046,"scale z":1.42,"col size y":0.743,"col orientation":6.356,"scale y":1.42,"col size z":5.18,"scale x":1.42,"col pos y":-909.127,"context":null,"url":null}')

-- b91a21e4531bed9832d5f008389f6179d9256998
SceneEditor:spawnShape("ge_partylamp_02.shape", 17446.457, -910.36, 0.634, 0, 0, 0.07, '{"col pos x":17453.346,"col size x":5.626,"scale z":1.41,"col size y":0.743,"col orientation":0.087,"scale y":1.41,"col size z":5.18,"scale x":1.41,"col pos y":-909.924,"context":null,"url":null}')
--############### SE_deco_Entrance 12462#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- db82f9be950271acb2825b8a046fea43f8d5bf62
SceneEditor:spawnShape("GE_Mission_borne.shape", 17549.39, -959.12, 10.328, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17549.39,"col pos y":-959.12,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 12eb213ebf86e8712d5186fc216c74fc837c4bba
SceneEditor:spawnShape("GE_Mission_borne.shape", 17553.51, -956.41, 10.361, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17553.51,"col pos y":-956.41,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6c132a9f184a2307fb395dc2d8300f47a59fef23
SceneEditor:spawnShape("GE_Mission_borne.shape", 17559.3, -954.21, 10.199, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17559.3,"col pos y":-954.21,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 7f9a79ca64d3c3dc86d0eda14560b977f80eb898
SceneEditor:spawnShape("GE_Mission_borne.shape", 17564.74, -955.08, 10.035, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17564.74,"col pos y":-955.08,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 545ff6330c1142416751ad15e537c27af70390a6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17566.96, -949.85, 10.025, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17566.96,"col pos y":-949.85,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4ba2d77b35458aca2d50fb4909d1f8b3db491857
SceneEditor:spawnShape("GE_Mission_borne.shape", 17563.92, -944.58, 10.067, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17563.92,"col pos y":-944.58,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 7249595f40f9ed532a9796b1fdd41bb9fd230915
SceneEditor:spawnShape("GE_Mission_borne.shape", 17559.29, -941.2, 10.096, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17559.29,"col pos y":-941.2,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 246c561916c5f34fd6f2d9e22ae6db35341c5f9e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17552.94, -939.84, 10.15, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17552.94,"col pos y":-939.84,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e9d4ae09272e6a705503e62c44049b63be198e9f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17547.56, -943.41, 10.139, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17547.56,"col pos y":-943.41,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- baec08d031190a37b67dfc0ee6f62c9f0e4f35ac
SceneEditor:spawnShape("GE_Mission_borne.shape", 17544.21, -947.66, 10.182, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17544.21,"col pos y":-947.66,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 70024b1ac067fbeca96a330bedc157fcbb66b5d7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17539.34, -951.43, 9.99, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17539.34,"col pos y":-951.43,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f5507f2054256afbc4c4795eaa07265abb4836d5
SceneEditor:spawnShape("GE_Mission_borne.shape", 17533.56, -954.97, 9.455, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17533.56,"col pos y":-954.97,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################