--############### SE_DECO 12333#################
-- GROUP: plants_center
-- 309e833961e31b108e0205e949673ffb49ccf36d
SceneEditor:spawnShape("fy_s2_savantree_a.shape", 17630.926, -930.355, 9.935, 0, 0, 0, '{"col size x":1.437,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.43,"col size z":11.066,"scale y":1,"col pos y":-930.886,"col pos x":17630.65,"context":null,"url":null}')

-- 2367c0d064a9d576a855286676b72779c3d55812
SceneEditor:spawnShape("tr_s2_palmtree_d.shape", 17718.02, -922.589, 10.274, 0, 0, 0, '{"col size x":1.38,"scale x":1,"col orientation":0.03,"scale z":1,"col size y":1.155,"col size z":17.871,"scale y":1,"col pos y":-922.589,"col pos x":17718.02,"context":null,"url":null}')


-- GROUP: center_bar
-- a59d559d95a660870c8d52e8028716dfadbb82e4
SceneEditor:spawnShape("fy_s2_coconuts_b.shape", 17728.334, -810.755, 10.037, 0, 0, 0, '{"col pos x":17728.23,"col size x":2.362,"scale z":1,"col size y":2.368,"col orientation":0,"scale y":1,"col size z":13.468,"scale x":1,"col pos y":-811.513,"context":null,"url":null}')

-- 36001e197e1a1f428ed24ec3d3ee0b205ce5a104
SceneEditor:spawnShape("fy_s2_coconuts_a.shape", 17741.33, -809.33, 10.054, 0, 0, 0, '{"col pos x":17741.33,"col size x":1.868,"scale z":1,"col size y":1.964,"col orientation":0,"scale y":1,"col size z":13.485,"scale x":1,"col pos y":-809.33,"context":null,"url":null}')

-- f089ac6e5dff5cdc76107a9183280b9fabb0b2c7
SceneEditor:spawnShape("coussin.shape", 17716.469, -806.424, 9.679, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-806.424,"context":null,"url":null}')

-- 98e74c92cbd1ac05d55408790066a12eb5e81aaa
SceneEditor:spawnShape("coussin.shape", 17716.584, -803.049, 9.68, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-803.049,"context":null,"url":null}')

-- ba3dcdbac6f1d1e98f80c1fa4adda1306862b54b
SceneEditor:spawnShape("coussin.shape", 17721.148, -800.938, 9.661, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-800.938,"context":null,"url":null}')

-- 1f4a1fbc009ed0a84e0f28785a5fa9e6fc9e986e
SceneEditor:spawnShape("coussin.shape", 17723.477, -811.92, 9.691, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-811.92,"context":null,"url":null}')

-- ab1cb2fc098424cc3ab63f8f9c4fe043bbee3110
SceneEditor:spawnShape("coussin.shape", 17723.467, -808.621, 9.666, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-808.621,"context":null,"url":null}')

-- c3eb83e8ff486e6cafe70ff06786025eae2c80eb
SceneEditor:spawnShape("coussin.shape", 17728.723, -806.448, 9.724, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-806.448,"context":null,"url":null}')

-- 5af44cff000a9b1adb9d30e83ad5b9c35279207e
SceneEditor:spawnShape("coussin.shape", 17728.768, -803.511, 9.716, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-803.511,"context":null,"url":null}')

-- 5020c84e181dedf99c32c63e447ec391bfdd766c
SceneEditor:spawnShape("coussin.shape", 17729.455, -815.655, 9.786, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-815.655,"context":null,"url":null}')

-- 9cf482bf34e732e5a24a787a554c8d4cc72d0b50
SceneEditor:spawnShape("coussin.shape", 17729.541, -818.763, 9.783, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-818.763,"context":null,"url":null}')

-- 2d35a6e26706b66f43900e4f7f311534cbfc6197
SceneEditor:spawnShape("coussin.shape", 17735.619, -808.138, 9.692, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-808.138,"context":null,"url":null}')

-- e30d833fe9543b6c64109c1fda7e5ca1cf7ea26e
SceneEditor:spawnShape("coussin.shape", 17735.377, -811.649, 9.768, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-811.649,"context":null,"url":null}')

-- d9dd3a55536e0686b732e6b80e9252be1be579e6
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17729.291, -817.111, 10.092, 0, 0, 0, '{"col pos x":17729.291,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-817.111,"context":null,"url":null}')

-- 4e277e78608111c057a479d1eabe4c3d29da1bf7
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17723.422, -810.201, 9.937, 0, 0, 0, '{"col pos x":17723.422,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-810.201,"context":null,"url":null}')

-- b00a9c63116d91ac36e2d3162ab766f6bbfed46e
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17716.445, -804.592, 10.002, 0, 0, 0, '{"col pos x":17716.445,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-804.592,"context":null,"url":null}')

-- 415e34de9bd2792495dc68ef9794ae9f43c860e9
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17728.652, -804.96, 10.02, 0, 0, 0, '{"col pos x":17728.652,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":3.141,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-804.96,"context":null,"url":null}')

-- 644b323bfe3a0b54b1525d048d53189af04f6c0f
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17735.396, -809.862, 10.065, 0, 0, 0, '{"col pos x":17735.396,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-809.862,"context":null,"url":null}')

-- 8226184d1efe3bdff0135da1b200a98667b92a37
SceneEditor:spawnShape("coussin.shape", 17741.162, -802.205, 9.784, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-802.205,"context":null,"url":null}')

-- 4c0a6da8d9739df62a006e1ab54ae5fa7483fd0c
SceneEditor:spawnShape("coussin.shape", 17741.699, -805.266, 9.744, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-805.266,"context":null,"url":null}')

-- 9295ce38e6ea0b23c0b753507ed9855ab3f4fed4
SceneEditor:spawnShape("coussin.shape", 17734.332, -800.127, 9.68, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-800.127,"context":null,"url":null}')

-- 715c30a9987e8d898703ae9ecdd2914c1a8589af
SceneEditor:spawnShape("coussin.shape", 17745.785, -799.953, 9.774, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-799.953,"context":null,"url":null}')

-- 83ba83427a1e9877bc53d855ba91670b71e0e8d0
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17741.387, -803.935, 10.04, 0, 0, 0, '{"col pos x":17741.387,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-803.935,"context":null,"url":null}')


-- GROUP: center_dancefloor
-- 8b60a3fb24b1a9ee8599ccca10c80f388efce492
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a8bda848fa412cd996299af6d0e67946c21c36c0
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 692321104b4f1fd56aa8437015006ad145809f5b
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5920f03b38b3e78c11687bafaeb5783d33dac8da
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5aadddfe8576cc137ec76f5fe20ffaf485194c55
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0a26a6b4cf6baf133aa4418555b63acafb4e3d3d
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d6b963fa4c1d9fe0be96f17ff97f3f07965a9a81
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 1fb870e2ccbefbcd9abb2dab704138aae3e762ba
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c4e26c4e95a239736f9dbb918da08a1a2645a6c5
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 6c8bfbcfc57f78a9bbe0206f608f4c43f4afeb24
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d4b01a534841a746559a67ac8cca3ba7c99071e1
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 22eda978559aa087e9f3c07cfa89c28bcf982a1d
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b61dde60ed30e99bb9bae1274069b8ea571235e8
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5625e5baa6a19d24509098b6b3a14a4ae0b5f3ac
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- bdad04a35c2137e8d99e96d419a5b62d2781c205
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 26acfb59d3626a0d43030d8089bc0ba1faccfc2f
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2cb139b643aff42d1a329c0e7b01833b4ab7aa19
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 8436dec184d2e946dd3e0c8d057f63ea9683d170
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 846258a0ebaee3861e386e7f8d80218545815dd6
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- db0512081c71cccc07484c3f873f92784be08bb1
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 33f9d7b8248f2479e14e2f967302f8cd65ac7500
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 7eeb046d821970a514158b7b76afa3e1c606cda4
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- df6813d217221439f6a4d43a6b46557bc43c19e1
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- cb0ba782e8ee11ff88bdd0cdc2799c4f98a9995a
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 044addb25e19947bba31a4c098b2fe15c53603de
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2844fe26820a53fb6702ac6707de63c2fa3a1917
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 50bda0fe14f78ba873b70be111f193f32e60c32d
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5042622e8a820512e30bdc9da3bf1b0cb57ae477
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b1f839dd283ca5030d9a3acbee2ed1a87300aa5c
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 30813fec4fd4c05d2f83571142bc12cf20759c0e
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5dc5a8f7aa28ff14138f3da0fc36e67e630d9cc4
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9afd2597ad2c8a1b5de20d85d839b23a6f7974c7
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 740c8e06cac81d538d4ed18ad35e8a570aa68209
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 35a5f924399cc6fc1c20b99360869b84cf02c49b
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 8d1f95fb090d477ed62598c637fe0459993f2d49
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ec2094b66e532c548c83709725f2e2229bfc9939
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 7b99ce8c5d7f597a3dd3ef456b06ba6ad2446156
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 248c437ca8ee775bc6d73d00d81010ea8a858d3d
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0c513156f95c68027a8a675c6033f08650ace0e2
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 32100ab27ebb74b024c40dd38b94f4c745960db3
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 08f0af56e2d22bfa9b3593d1b9c3b2e8f2c12530
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 644870b9bda876c67608d7f35af1baf7fe0e7840
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- af577ce09832c4256e7709d4ec5b9041d728e441
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 69adae401cc3faccacbc18d999178457c2d76ae0
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 26a9780315e1884daf6a78f982942829f92d724d
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 4f4163dde4aed5857d543c4b5f4f63194f7505d2
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 8c0edd7066e52066630d1ed1cf088e2911386d49
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ef5ff3f9d6e1cadffdb513ac50531457abd955a2
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 05c63c0f9fb4aa2b2667fbd36223f5a6a838f7c2
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f15f39a5538dabe62c6b167062004d70e4096240
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b63da1f9f6d2d992299bc9b7bab756e2eb10077f
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e2494755ce93ca37a0e69a14eaa2756addf5683d
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- db5f6b82346b3142ebd337266ba219f9ff00aa0e
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e6275edbd1373483617fdb22ea9b83475070d38d
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ff3254704208f4d75db85d697a0493a38403ea27
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 83e07bbe19b39a2dc3fcdce81e44916d3f51b50a
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 485bfb49091ea01a11b49dc67a7427443423d018
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5835f460151124357401ad355ecc3ad8d7ef2998
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 387c42ec9e7c900ecbd5e9c14f8a190c1a09ff3e
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5e614c1637df55cafa9069adff63d25ff08d0c55
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a727fd5eb8095dc3d0ebb82e0caf8ef8447128c5
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9e189971a48b14c3deb029a4a90b1a788cbf2d6e
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 4f190f8eca1140da291a5e28aec08bd2a64182a2
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 80ff5a604773f3451342322cbba08e099cb91900
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 441aad75b5682e04f7bc9b042ceadc0c8abd9e06
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- cf8903883a94198ef2ea79d80109c60e5ecccc17
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 51b8c6e9c82c4baab1322f38aa846b0e653cb42d
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 211926813d2474e886a543490915ec5897f4d270
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 25f65692ec9a954b1be5e8ebf9ba7836aee9b458
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 073797ecfd68db09f186ac3194f13bd0e43a4466
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c390daf9a634f337d1661283b43ce743c9d9bece
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e4ef83dc90e6d878c3a989dcea09b84a2b9c4f7a
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ac40661e65a2355adb62bf409095eb7e1720ff83
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 30d580467414ff7ee5e579de74e3a1d0ba6c9861
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0e19122766ea45ad39f05f0af63456c3363a0676
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 68c0dbe6309bc45ce21ec18a54f9e2b0e69c7e40
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 3b85934432f84a7ab89ab08b7ec8d6b010da5632
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 4b6254f8d38b1bc71b46760f50076aa412102574
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 74d2453f07426daaac5253d079ae84355e8727e0
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c74dd22741bae7c2fe6bfc652ae03af9ca8e6f59
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f152a1684922358cffbde3f733c32646dfce167d
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 857c26dc4c2846e91739dd3e0321c8d57ba8f941
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2a7d4b8edfd462d101c91b4ec0d25e5d83e65e0a
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 188651ab12214e4c46ecf9a79bc64277d99ee164
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- dd58c98e589f864ff17000ff6513f1a962647e53
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -959, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 36e27869ddee7a4e585fd35c51fbee41e46efa51
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -953, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0a9f1df5ba271dcf0551674660f4df0f707da3b4
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -947, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 32410a77aa8dc9f00939ca6baa6a67b72ad98285
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -941, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 64260bb83aeee02436e2642be4b260d1ce38f1c3
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 3d7bfdeef20a4f08f1296c6ef0bfa3244ea09ea9
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- fb3cb478acd458bd64e221c99a8c898216df299c
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 84f56857aa4afdc41770c5463b82b4615c18679c
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 131b0e67062ed617d2ba1f5cf5b9dae099925d7e
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- acac32aca63ecda363302aa9bdef9626980a2f34
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- daf480a532c21b938a5861d0c5359e4f4d7df640
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 471639dade5020ba9fe6298f4ba8a327be5c3c01
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9b3b278156275d050c1b9732233caf25ec2dff7b
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 55038be9798b62c2d748cc28f97f82e222e398af
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 4e32681f62dc34545fc653dba12da67bdf40f3da
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d2b4d0ed1a6c9ab505496b116bb46d4ca14c3a8f
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5c20e0880e6e3b72fa14da7966e9a1f85d6740bc
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 86f96e375a4f21e96af2cce89baad79b36812d99
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 663b1fa867eda6a807be4215c4c16f6a4c9f5937
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 80affe0d1c47b8fd1b6460c4814af96bd906c164
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 4f8e0ff58f9f23df106c619bd527d9bbdf03982f
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 29f5ae85f56e977fc7fc3c552ff92ca5acd06817
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 3768197456e096d4416ee3ec4fd423f8c4c1de0f
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2a4c41cd6b1ae5314ac4ff7ee33a7b1b5775510f
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- dff62e44a6193fd9c05953798484da89bea6703e
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b43c9c13ca94686657da471ceb1b25e0ab9d2888
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -956, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 85201ed84c3a3b10f05ec99f33ea9792a4b292be
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -950, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e6f23c25ac1daee3b4995db38b5431cbb1a121da
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -944, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 3d9a4f95f32174a13f2c5300a9d4aef99ec8132c
SceneEditor:spawnShape("ge_partylamp_02.shape", 17700.418, -937.702, 10.372, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17705.584,"col pos y":-938.557,"col orientation":0,"col size x":3.196,"col size y":2.943,"col size z":5.18,"context":null,"url":null}')

-- 3d9bae38da2503aaec3ed660eea4b89df5bb012b
SceneEditor:spawnShape("ge_partylamp_02.shape", 17690.814, -937.62, 10.261, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17695.666,"col pos y":-937.62,"col orientation":0,"col size x":3.686,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 26b3252ee8e9733e8ca29642718d8fb5a286d09a
SceneEditor:spawnShape("ge_partylamp_02.shape", 17680.805, -937.301, 10.053, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17685.877,"col pos y":-937.301,"col orientation":0,"col size x":4.026,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- f33d0cb2de7ffedf75478b161f68d5ae9b2ae0ea
SceneEditor:spawnShape("ge_partylamp_02.shape", 17670.902, -937.071, 9.853, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.994,"col pos y":-937.071,"col orientation":0,"col size x":3.856,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- f60bcba252af04798f964c61ddfeb56e961abf52
SceneEditor:spawnShape("ge_partylamp_02.shape", 17661.211, -937.329, 9.741, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17666.125,"col pos y":-937.329,"col orientation":0,"col size x":3.896,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- a28b5ae6a4631bc11e8a15f90f7c0cb8538cdb60
SceneEditor:spawnShape("ge_partylamp_02.shape", 17706.84, -953.517, 10.075, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17706.84,"col pos y":-958.37,"col orientation":1.54,"col size x":3.676,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- ff6d83784738d7ab33500752d2c3ba5c31ea2e85
SceneEditor:spawnShape("ge_partylamp_02.shape", 17706.957, -943.87, 10.283, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17706.957,"col pos y":-948.746,"col orientation":1.53,"col size x":3.766,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- a7bce300cf30dec1d1183abcdcf188a086e7044f
SceneEditor:spawnShape("ge_partylamp_02.shape", 17655.23, -944.448, 9.862, 0, 0, -1.55, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17656.277,"col pos y":-939.114,"col orientation":0.83,"col size x":4.486,"col size y":3.153,"col size z":5.18,"context":null,"url":null}')

-- 502cf34e7b77bc2a8ae2a2c57b77f0a7ad597777
SceneEditor:spawnShape("ge_partylamp_02.shape", 17655.059, -953.926, 9.956, 0, 0, -1.62, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17655.059,"col pos y":-949.207,"col orientation":4.661,"col size x":3.566,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 8979e0f465ac817e1dae5a982b8c49cffb086ca9
SceneEditor:spawnShape("fy_acc_tabouret_a_appart.shape", 17696.066, -933.841, 10.359, 0, 0, -1.72, '{"scale x":1,"scale y":3.17,"scale z":1,"col pos x":17696.066,"col pos y":-933.841,"col orientation":-0.15,"col size x":1.504,"col size y":0.448,"col size z":0.567,"context":null,"url":null}')

-- e86b5f61895b155e8def0139aea921ea4d716290
SceneEditor:spawnShape("fy_acc_tabouret_a_appart.shape", 17691.582, -933.618, 10.273, 0, 0, -1.63, '{"scale x":1,"scale y":3.52,"scale z":1,"col pos x":17691.582,"col pos y":-933.618,"col orientation":-0.06,"col size x":1.714,"col size y":0.498,"col size z":0.567,"context":null,"url":null}')

-- 6a5c6775b9d18b5ec73170ec665d23ea910db5ae
SceneEditor:spawnShape("fy_acc_tabouret_a_appart.shape", 17685.516, -933.366, 10.158, 0, 0, -1.73, '{"scale x":1,"scale y":3.66,"scale z":1,"col pos x":17685.516,"col pos y":-933.366,"col orientation":-0.184,"col size x":1.874,"col size y":0.528,"col size z":0.567,"context":null,"url":null}')

-- 1cf8065d479f3052175082b631c7d1efead57015
SceneEditor:spawnShape("ma_acc_chaise01_hall_reunion.shape", 17651.477, -954.638, 9.956, 0, 0, 3.14, '{"scale x":2,"scale y":4,"scale z":1.5,"col pos x":17651.477,"col pos y":-954.638,"col orientation":0,"col size x":1.491,"col size y":2.79,"col size z":1.363,"context":null,"url":null}')

-- deeaa6512eb576028737b926f7830802f2ee01a8
SceneEditor:spawnShape("xmas_lights_02.shape", 17691.303, -953.089, 10.091, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-953.089,"col orientation":0,"col size x":5.591,"col size y":5.521,"col size z":2.333,"context":null,"url":null}')

-- 2f2d4b98248f9f3d91463e12a7851e81e4c18795
SceneEditor:spawnShape("xmas_lights_02.shape", 17670.535, -953.218, 9.994, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-953.218,"col orientation":0,"col size x":5.591,"col size y":5.521,"col size z":2.333,"context":null,"url":null}')

-- 9ccdf764f61e6d7ae6b62035c3d82ddcfe474d69
SceneEditor:spawnShape("ge_mission_stand.shape", 17644.369, -958.926, 10.049, 0, 0, 1.59, '{"scale x":1.088,"scale y":0.938,"scale z":0.644,"col pos x":17644.369,"col pos y":-958.926,"col orientation":1.55,"col size x":4.258,"col size y":4.775,"col size z":6.313,"context":null,"url":null}')

-- 05a6d98753a3e5355095d4530843f1cc767c2783
SceneEditor:spawnShape("ge_mission_stand.shape", 17678.021, -930.708, 9.964, 0, 0, 0, '{"scale x":1.088,"scale y":0.938,"scale z":0.644,"col pos x":17678.021,"col pos y":-930.708,"col orientation":0,"col size x":4.258,"col size y":4.775,"col size z":6.313,"context":null,"url":null}')


-- GROUP: center_arena
-- 91b3e3d33275492dde514b7cad52178acffe354b
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 17731.852, -934.796, 9.852, 0, 0, 0, '{"col size x":2.685,"scale x":0.769,"col orientation":0,"scale z":0.769,"col size y":5.184,"col size z":2.147,"scale y":0.769,"col pos y":-934.796,"col pos x":17731.852,"context":null,"url":null}')

-- b7817c3d6aa161d70ffb1a6dac57523bb1ba820a
SceneEditor:spawnShape("ge_feudecamp.ps", 17746.447, -949.388, 9.993, 0, 0, 0, '{"col size x":1.031,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.148,"col size z":5.716,"scale y":1,"col pos y":-949.388,"col pos x":17746.447,"context":null,"url":null}')

-- d5c4e161e255f5ee8fb5272852f41c8ec6324f30
SceneEditor:spawnShape("skull01.shape", 17742.996, -929.685, 15.58, -1.4, -0.43, 0, '{"col size x":1.199,"scale x":3.299,"col orientation":0,"scale z":3.299,"col size y":0.522,"col size z":0.174,"scale y":3.299,"col pos y":-929.685,"col pos x":17742.996,"context":null,"url":null}')

-- 4896cf2c613ab51754e76e1e89f4cd073e9f480e
SceneEditor:spawnShape("skull01.shape", 17746.725, -928.228, 15.68, -1.4, -0.43, 0, '{"col size x":1.199,"scale x":3.299,"col orientation":0,"scale z":3.299,"col size y":0.522,"col size z":0.174,"scale y":3.299,"col pos y":-928.228,"col pos x":17746.725,"context":null,"url":null}')

-- 3cea6ee875d0bcf95627e02fef8a8fc7aa0bbd91
SceneEditor:spawnShape("ge_mission_panneau.shape", 17745.283, -929.527, 9.908, 0, 0, 0.37, '{"col size x":1.61,"scale x":10.42,"col orientation":0,"scale z":3.86,"col size y":0.172,"col size z":2.305,"scale y":2.19,"col pos y":-929.527,"col pos x":1.594,"context":null,"url":null}')

-- e950494f4ab590f9808e6a2d0deba3c6a23b2c66
SceneEditor:spawnShape("ju_fo_brumes.ps", 17755.309, -952.015, 10.02, 0, 0, -0.29, '{"col size x":30.111,"scale x":1,"col orientation":-1.33,"scale z":1,"col size y":18.025,"col size z":11.766,"scale y":1,"col pos y":-952.015,"col pos x":11.475,"context":null,"url":null}')

-- 3de454d46cebde19972c11a749fa0e6b7bc2f491
SceneEditor:spawnShape("ju_fo_brumes.ps", 17757.803, -948.553, 9.977, 0, 0, 0, '{"col size x":30.111,"scale x":1,"col orientation":0,"scale z":1,"col size y":18.025,"col size z":11.766,"scale y":1,"col pos y":-948.553,"col pos x":0,"context":null,"url":null}')

-- ffb462054e9c8c063f49f43227facde87cebe3d3
SceneEditor:spawnShape("ju_fo_brumes.ps", 17756.805, -957.91, 9.967, 0, 0, 0, '{"col size x":30.111,"scale x":1,"col orientation":0,"scale z":1,"col size y":18.025,"col size z":11.766,"scale y":1,"col pos y":-957.91,"col pos x":0,"context":null,"url":null}')

-- 774d2f81ae91ac44faa2a9eb4bf3fdb995bc14c5
SceneEditor:spawnShape("mp_bones.ps", 17755.469, -943.202, 9.913, 0, 0, 0, '{"col size x":1.111,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.114,"col size z":1.016,"scale y":1,"col pos y":-943.202,"col pos x":17755.469,"context":null,"url":null}')

-- 8fb356a7708031befce852d645a27ac43f723044
SceneEditor:spawnShape("goo_mo_charognemamal.ps", 17736.857, -952.654, 10.001, 0, 0, 0, '{"col size x":5.731,"scale x":1,"col orientation":0,"scale z":1,"col size y":8.971,"col size z":9.009,"scale y":1,"col pos y":-952.654,"col pos x":0,"context":null,"url":null}')

-- fafa2d7c83622c469c9d04b6df5425edaa4c9821
SceneEditor:spawnShape("ge_mission_barriere.shape", 17726.42, -957.88, 10.092, 0, 0, 4.71, '{"col size x":3.424,"scale x":1,"col orientation":4.71,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-957.88,"col pos x":17726.42,"context":null,"url":null}')

-- de63fa5e90a73ec4669c88c73f716a6302635136
SceneEditor:spawnShape("ge_mission_barriere.shape", 17726.58, -954.74, 10.092, 0, 0, 4.61, '{"col size x":3.424,"scale x":1,"col orientation":4.61,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-954.74,"col pos x":17726.58,"context":null,"url":null}')

-- 127c67981f964a4ea518d7bfc84a0bfcce65153f
SceneEditor:spawnShape("ge_mission_barriere.shape", 17727.08, -951.64, 10.092, 0, 0, 4.5, '{"col size x":3.424,"scale x":1,"col orientation":4.5,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-951.64,"col pos x":17727.08,"context":null,"url":null}')

-- c0d9f73d1c177dbdafd463dd8be7f9d3f5bf0228
SceneEditor:spawnShape("ge_mission_barriere.shape", 17727.891, -948.61, 10.092, 0, 0, 4.4, '{"col size x":3.424,"scale x":1,"col orientation":4.4,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-948.61,"col pos x":17727.891,"context":null,"url":null}')

-- 905effc924bd833c09c24f05560bf19df5408aff
SceneEditor:spawnShape("ge_mission_barriere.shape", 17729.01, -945.68, 10.092, 0, 0, 4.29, '{"col size x":3.424,"scale x":1,"col orientation":4.29,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-945.68,"col pos x":17729.01,"context":null,"url":null}')

-- 01e527099565c36e5e5d45ccae680ed3b423d730
SceneEditor:spawnShape("ge_mission_barriere.shape", 17730.439, -942.88, 10.092, 0, 0, 4.19, '{"col size x":3.424,"scale x":1,"col orientation":4.19,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-942.88,"col pos x":17730.439,"context":null,"url":null}')

-- 0d33aeec7238d1b3186a26264c7d4110805a5f35
SceneEditor:spawnShape("ge_mission_barriere.shape", 17732.15, -940.25, 10.092, 0, 0, 4.08, '{"col size x":3.424,"scale x":1,"col orientation":4.08,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-940.25,"col pos x":17732.15,"context":null,"url":null}')

-- 2c0428aa479a8369e56cd50e609c89c4be5c20b5
SceneEditor:spawnShape("ge_mission_barriere.shape", 17734.131, -937.81, 10.092, 0, 0, 3.98, '{"col size x":3.424,"scale x":1,"col orientation":3.98,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-937.81,"col pos x":17734.131,"context":null,"url":null}')

-- 4df8f6ece394b0852442364de320fa6bc12d33d6
SceneEditor:spawnShape("ge_mission_barriere.shape", 17736.35, -935.59, 10.092, 0, 0, 3.87, '{"col size x":3.424,"scale x":1,"col orientation":3.87,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-935.59,"col pos x":17736.35,"context":null,"url":null}')

-- a4f0baed0f17bdf1ffa29e82661a331ad7e042ba
SceneEditor:spawnShape("ge_mission_barriere.shape", 17738.789, -933.61, 10.092, 0, 0, 3.77, '{"col size x":3.424,"scale x":1,"col orientation":3.77,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-933.61,"col pos x":17738.789,"context":null,"url":null}')

-- 41e3c553a534a41715a83cf9c824d10008021fdc
SceneEditor:spawnShape("ge_mission_barriere.shape", 17741.42, -931.9, 10.092, 0, 0, 3.66, '{"col size x":3.424,"scale x":1,"col orientation":3.66,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-931.9,"col pos x":17741.42,"context":null,"url":null}')

-- ac6925c1fae462197aee3215d2f3c1a0f8301b61
SceneEditor:spawnShape("ge_mission_barriere.shape", 17741.242, -930.665, 9.851, 0, 0, 2.84, '{"col size x":3.424,"scale x":1,"col orientation":2.79,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-930.665,"col pos x":17741.242,"context":null,"url":null}')

-- ddd3a074c86d9ee4784bd2207e62558d05083f98
SceneEditor:spawnShape("ge_mission_barriere.shape", 17748.82, -927.326, 9.87, 0, 0, 1.5, '{"col size x":3.424,"scale x":1,"col orientation":1.5,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-927.326,"col pos x":17748.82,"context":null,"url":null}')

-- 6e16a927d0b1247a76eecb88f89c7d1f1b14cb82
SceneEditor:spawnShape("ge_mission_barriere.shape", 17750.18, -928.54, 10.092, 0, 0, 3.35, '{"col size x":3.424,"scale x":1,"col orientation":3.35,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-928.54,"col pos x":17750.18,"context":null,"url":null}')

-- c2637e26f74841f1e6f286f49942219f74a0a4de
SceneEditor:spawnShape("ge_mission_barriere.shape", 17753.279, -928.04, 10.092, 0, 0, 3.25, '{"col size x":3.424,"scale x":1,"col orientation":3.25,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-928.04,"col pos x":17753.279,"context":null,"url":null}')

-- f8e03aee30841a7c0a2b4a6cb52c34e2c6675599
SceneEditor:spawnShape("ge_mission_barriere.shape", 17756.42, -927.88, 10.092, 0, 0, 3.14, '{"col size x":3.424,"scale x":1,"col orientation":3.14,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-927.88,"col pos x":17756.42,"context":null,"url":null}')


-- GROUP: center_20
-- 6ef020ded801ceab889174329968f73fb8a88458
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17601.61, -836.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- d7080d5822b12f8c7a157849dc7d275d738ecf58
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17606.11, -836.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 544d35a7baccd7a1874c712e328b023d7bd29962
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17610.61, -837.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 5a62acb4db4f41c5a1d32e94df82e1518ab89daf
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17615.11, -839.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 3c448c0450fb34a1640a95a619e89536553b21f1
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17618.11, -842.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 4eebebd17cc482c3c11297e90a07b8e7c7019659
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17619.61, -846.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- dbc450a763c2c5c17a62f85aeb60b61b108a00ce
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17619.61, -851.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 8d43012dfc77fa7ed0c6674d032d3a18c41a4cd9
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17618.11, -855.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- f72ce34572042af2ef378947d828730278bf0094
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17616.61, -860.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 73ff5acd0a93c4f17cd35464f86746ecb0e692cc
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17613.61, -864.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- eb346e51ac69f778c2c2a825aa52f0efb3f11850
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17610.61, -869.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- efcba7ab12bad194fa5e5ddbfc0787ff22b79b4f
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17607.61, -873.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 504bc4aa28b6748981891c5975620ed00d8db9a9
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17604.61, -878.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- cbc2b503807dbe25ab23650f41fcc6e2cf45dccd
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17601.61, -882.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 4b8052d097ad9d4f13611142ec41c4978f5035ec
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17603.11, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a10c88522acb891e7589062533c4086e2a26292d
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17607.61, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 80244de1c5f010ea3d7786b97df6c49db127b86c
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17612.11, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 09d92f411e5b28325acebfdf3dca11266bb8c6bd
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17616.61, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 78785a5f0144c8cc05621c6e0173357a3f4361a3
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17621.11, -909.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 80b2ed35460a1ceeda159076cedaf23e8d2cab92
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17664.61, -842.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a6cbfa2011eef924c9604001f5ce4b99e37a6c67
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17658.61, -843.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- b090395b234fa933c43912bfe86c66fc6f43e373
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17654.11, -846.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 60a05ff56e20b645049c8924157979e13d9af6dd
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17649.61, -851.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- d601c74aad6984ac311da363c04ecc24cd9df61e
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17646.61, -857.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 3206621853bdb6bc75b3b538b6abdc988fde5122
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17645.11, -863.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- edcf9c965ba8017b21d699ba7f4d666e780206cc
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17643.61, -869.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- d930252f20e4213325ac883512768b54fe12d836
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17643.61, -875.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 114a4df5d79a58ff980fc3773398acc0ce84a21e
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17643.61, -881.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 7ef57c35a8b019611f06d5a7db368cc1542c83d8
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17645.11, -887.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 3631336bde2d2617eea17b542bdf313a96c31d97
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17646.61, -893.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- adc256299be5793c43a1b8856934367ac735efe2
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17649.61, -897.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a1484d76680db2ad7ffdbab1f7220fd3ff97c76c
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17652.61, -902.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- c0e0cf865ddd6399ec66306c95723cafc138271f
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17655.61, -905.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 949227286038402f8c56ec756cc836eefbf5a1cf
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17658.61, -908.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 4a13bfb17c738d2fbfb5f9e424ac8fb23efec057
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17663.11, -911.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- c27a9b5974e145e305f8d0c348967d820a6504d8
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17667.61, -912.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 9d69aedaf7117a450fd9b4a438ebe6b3efdd6a84
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17672.11, -912.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 463c531fef2a811300688bab0f18ce85ecedcf4a
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17676.61, -911.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 4ce62564c6814746d213ecbc165646ba9346fe5d
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17681.11, -908.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 228b62d04c6931c2ba90897b8b93e76d103c06ae
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17684.11, -905.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 27208698d3221aa918221051f198cfb73de4fc40
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17687.11, -900.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- bdd6fca4c2c75161686148377b7791984b1cbe41
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17688.61, -896.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 50078f33799798de19f73c147d8b0adb5bae318b
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17690.11, -890.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 5169244d3d04244e80796b951cae533f1634f957
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17691.61, -885.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- b4fa336ca6a5ff4a7603f3f5eaa2b3f888833309
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17691.61, -879.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 28c2f38c2c87029e5b31c4d44aaf17f1159d00ce
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17691.61, -873.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- bf127c59211b09868f21557db7fe0b9257686cd0
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17691.61, -869.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- a53a7815a6807f6d96e612dd13faf76883d63f55
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17691.61, -863.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- df6bbc3646ece8b984e9899d51de360c2d7dfe16
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17690.11, -858.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- f719dc309d3c3cd6d10f41cc3c3f7daf656938d1
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17688.61, -854.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 0ca785f247de5364335a1effc6422a75c7acbabb
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17685.61, -849.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 2cb525ecc116a0e4c60ba685081f04c142a595ac
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17681.11, -846.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 006557cb8138c939ece6db23cb75d2d35bb04b1c
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17676.61, -843.79, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 74ed82a5478e04364faf424e462540206ee15078
SceneEditor:spawnShape("ge_mission_kite_kamique.ps", 17672.11, -842.29, 9.966, 0, 0, 0, '{"scale x":0.22,"scale y":0.22,"scale z":0.22,"col pos x":0,"col pos y":-865.645,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')
--############### SE_DECO 12333#################

