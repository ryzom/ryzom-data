--############### SE_deco_Entrance 12462#################
-- GROUP: entrance
-- 17852367b69aceb4ed17a0859c1b866a2e45df16
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 17266.645, -940.204, -0.732, 0, 0, -1.24, '{"col pos x":17266.645,"col size x":2.685,"scale z":0.769,"col size y":5.184,"col orientation":-1.24,"scale y":0.769,"col size z":2.147,"scale x":0.769,"col pos y":-940.204,"context":null,"url":null}')

-- 71c436c4f99f58647e4d37ea22c79b55b908dfa0
SceneEditor:spawnShape("gen_mission_3_caisses.shape", 17268.783, -942.165, -0.477, 0, 0, 0, '{"col pos x":17268.783,"col size x":1.362,"scale z":1,"col size y":2.542,"col orientation":0,"scale y":1,"col size z":2,"scale x":1,"col pos y":-942.165,"context":null,"url":null}')

-- f6821bb28ee210ff6345969cf7f99a7e1df69a6f
SceneEditor:spawnShape("gen_mission_1_tonneau.shape", 17269.359, -954.808, -0.051, 0, 0, 0, '{"col pos x":17269.359,"col size x":1.044,"scale z":1,"col size y":1.044,"col orientation":0,"scale y":1,"col size z":1.241,"scale x":1,"col pos y":-954.808,"context":null,"url":null}')

-- f6bdd008c20aae469dd8e324f029b5b231a6bc7a
SceneEditor:spawnShape("fo_s3_fougere.shape", 17265.795, -957.168, -0.012, 0, 0, 0, '{"col pos x":17265.795,"col size x":5.341,"scale z":1.199,"col size y":4.678,"col orientation":0,"scale y":1.305,"col size z":3.538,"scale x":1.305,"col pos y":-957.168,"context":null,"url":null}')

-- 506aa8b62a2e7ee91a00ac5cef7649b7847bee09
SceneEditor:spawnShape("fo_s3_fougere.shape", 17272.533, -930.922, -0.428, 0, 0, 0, '{"col pos x":17272.533,"col size x":5.341,"scale z":1.289,"col size y":4.678,"col orientation":0,"scale y":1.395,"col size z":3.538,"scale x":1.395,"col pos y":-930.922,"context":null,"url":null}')

-- 0d0958ff0a117e7474130305b8ce43037b567a9b
SceneEditor:spawnShape("ge_partylamp_02.shape", 17273.732, -900.435, -1.098, 0, 0, 0.47, '{"col pos x":17266.838,"col size x":6.136,"scale z":1.6,"col size y":0.743,"col orientation":0.45,"scale y":1.6,"col size z":5.18,"scale x":1.6,"col pos y":-903.984,"context":null,"url":null}')

-- 8cd1f68613fe0a27f081d6ba063bd456f641db7b
SceneEditor:spawnShape("ge_partylamp_02.shape", 17259.924, -906.909, -3.438, 0, 0, 0.38, '{"col pos x":17253.225,"col size x":5.836,"scale z":1.54,"col size y":0.743,"col orientation":0.41,"scale y":1.54,"col size z":5.18,"scale x":1.54,"col pos y":-909.666,"context":null,"url":null}')

-- 2b08edccfcd3ee560ce6553a17d748054577efeb
SceneEditor:spawnShape("ge_partylamp_02.shape", 17246.432, -912.786, -3.738, 0, 0, 0.45, '{"col pos x":17240.219,"col size x":5.006,"scale z":1.57,"col size y":1.773,"col orientation":0.92,"scale y":1.57,"col size z":5.18,"scale x":1.57,"col pos y":-916.511,"context":null,"url":null}')

-- 36e54c698e1773bbf69831631a2306c1f4a6f00e
SceneEditor:spawnShape("ge_partylamp_02.shape", 17238.922, -923.566, -3.697, 0, 0, -1.61, '{"col pos x":17238.789,"col size x":5.596,"scale z":1.57,"col size y":0.743,"col orientation":1.66,"scale y":1.57,"col size z":5.18,"scale x":1.57,"col pos y":-931.005,"context":null,"url":null}')

-- 4c6d2ef638c80ac87ae92e082021c8d99742d4e1
SceneEditor:spawnShape("ge_partylamp_02.shape", 17240.219, -937.962, -2.083, 0, 0, -1.3, '{"col pos x":17242.207,"col size x":5.716,"scale z":1.56,"col size y":0.743,"col orientation":1.86,"scale y":1.56,"col size z":5.18,"scale x":1.56,"col pos y":-945.206,"context":null,"url":null}')

-- 8765278613d475d4ea8ca2eaa8324ac53ce1d090
SceneEditor:spawnShape("ge_partylamp_02.shape", 17243.74, -952.246, -0.336, 0, 0, 1.79, '{"col pos x":17245.465,"col size x":5.956,"scale z":1.53,"col size y":0.743,"col orientation":1.84,"scale y":1.53,"col size z":5.18,"scale x":1.53,"col pos y":-959.533,"context":null,"url":null}')

-- 77e84a59cfa0f4a7ee0dca8cb1b13595384ad24c
SceneEditor:spawnShape("ge_mission_tente_kamique.shape", 17249.484, -925.67, -2.547, 0, 0, 1.05, '{"col pos x":17249.484,"col size x":12.645,"scale z":1.68,"col size y":13.01,"col orientation":1.1,"scale y":1.68,"col size z":14.935,"scale x":1.68,"col pos y":-925.67,"context":null,"url":null}')

-- f40640b748935496d5147e897525fee65d2df5e7
SceneEditor:spawnShape("ge_mission_tente_fy.shape", 17262.467, -952.467, -0.262, 0, 0, 4.75, '{"col pos x":17262.467,"col size x":5.325,"scale z":1.25,"col size y":9.552,"col orientation":-1.56,"scale y":1.25,"col size z":5.925,"scale x":1.25,"col pos y":-952.467,"context":null,"url":null}')

-- c8458c565625228adacbedf3528ce50fae647026
SceneEditor:spawnShape("GE_Mission_tente.shape", 17265.586, -944.809, -0.454, 0, 0, -2.13, '{"col pos x":17265.586,"col size x":5.148,"scale z":1,"col size y":4.485,"col orientation":-0.56,"scale y":1,"col size z":5.034,"scale x":1,"col pos y":-944.809,"context":null,"url":null}')
--############### SE_deco_Entrance 12462#################

