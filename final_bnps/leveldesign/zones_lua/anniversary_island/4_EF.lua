--############### SE_DECO 12333#################
-- GROUP: plants_fyros
-- 59e2eb4996da1aa481b34c49659d029dd84df449
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17485.521, -548.935, 0.417, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17485.521,"col pos y":-548.935,"col orientation":0,"col size x":4.615,"col size y":5.728,"col size z":15.054,"context":null,"url":null}')

-- e1cd7421bac42401a22afbce5ed3051359929e33
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17493.992, -627.274, -0.022, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17493.992,"col pos y":-627.274,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- 806f4cbbc432e91e6722f23293eda5a523db4b8d
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17519.43, -562.051, 1.011, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17519.43,"col pos y":-562.051,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- 220578a33b5efeda073cae21e06a56d72dfd43fe
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17564.039, -589.291, -1.09, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17564.039,"col pos y":-589.291,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- 5b914afd8eb2c634dd806805f025f4816551ac8c
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17550.078, -639.431, 0.022, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17550.078,"col pos y":-639.431,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- d5b9037ce7511dc5316e0b1ff3d36e75c5ad58e3
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17552.895, -587.453, -0.289, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17552.895,"col pos y":-587.453,"col orientation":0,"col size x":0.827,"col size y":0.504,"col size z":11.847,"context":null,"url":null}')

-- be70f301e90925a5bded4559839288b40bd613db
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17537.846, -580.269, 0.048, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17537.846,"col pos y":-580.269,"col orientation":0,"col size x":0.827,"col size y":0.534,"col size z":11.847,"context":null,"url":null}')

-- 38d8e185c5af4b59293348cc7563bbdccdee741a
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17535.209, -606.023, 0.013, 0, 0, -0.69, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17535.209,"col pos y":-606.023,"col orientation":-0.92,"col size x":0.827,"col size y":0.684,"col size z":11.847,"context":null,"url":null}')

-- 607c00befc7117d980a3ddc09188b539f9d8981e
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17509.398, -575.193, -1.433, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17509.398,"col pos y":-575.193,"col orientation":0,"col size x":0.827,"col size y":0.644,"col size z":11.847,"context":null,"url":null}')

-- 4cf6b37de403cf02f9b6863c793224439d29cce7
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17549.41, -600.959, -0.569, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17549.41,"col pos y":-600.959,"col orientation":0,"col size x":0.827,"col size y":0.614,"col size z":11.847,"context":null,"url":null}')

-- cb1da7b84374b96ab6b4286f7f1101c61453e306
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17505.055, -603.56, 2.681, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17505.055,"col pos y":-603.56,"col orientation":0,"col size x":0.827,"col size y":0.864,"col size z":11.847,"context":null,"url":null}')

-- 6e5f2c7faffe100135cf2c8f3219cbdbeb76e4ac
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17496.199, -579.169, -0.444, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17496.199,"col pos y":-579.169,"col orientation":0,"col size x":0.827,"col size y":0.654,"col size z":11.847,"context":null,"url":null}')

-- d1f5964e11474379d5b095ebd05763a1b062cca5
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17477.809, -535.97, 0.731, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17477.809,"col pos y":-535.97,"col orientation":0,"col size x":0.827,"col size y":0.824,"col size z":11.847,"context":null,"url":null}')

-- e0a9f5638c7fd29b729a025a01cca0e382c29b3b
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17468.938, -561.285, 0.956, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17468.938,"col pos y":-561.285,"col orientation":0,"col size x":0.827,"col size y":1.064,"col size z":11.847,"context":null,"url":null}')

-- d1ea9df642bd455d9e7276ce90cd4130fe1fc365
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17482.438, -577.695, 1.061, 0, 0, 0.59, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17482.438,"col pos y":-577.695,"col orientation":0.56,"col size x":0.827,"col size y":1.184,"col size z":11.847,"context":null,"url":null}')

-- 0d02bf86800761b168c42c79cac657aee38be923
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17462.91, -581.306, 1.132, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17462.91,"col pos y":-581.306,"col orientation":0,"col size x":0.827,"col size y":1.294,"col size z":11.847,"context":null,"url":null}')

-- ecd48b049b46975a2f0dd7a21917c2b84707c3ee
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17485.125, -596.151, 2.723, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17485.125,"col pos y":-596.151,"col orientation":0,"col size x":0.827,"col size y":8.464,"col size z":11.847,"context":null,"url":null}')

-- 215d6ab51d2a8d85e15e18148efd75f9bcdf0b58
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17483.525, -619.527, 0.133, 0, 0, -1.01, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17483.525,"col pos y":-619.527,"col orientation":-1.08,"col size x":0.827,"col size y":1.054,"col size z":11.847,"context":null,"url":null}')

-- 86ed5ec860d62b7835ff627026633775d49a4be7
SceneEditor:spawnShape("fy_s2_papaleaf_a.shape", 17505.109, -624.66, -0.003, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17505.109,"col pos y":-624.66,"col orientation":0,"col size x":0.827,"col size y":1.044,"col size z":11.847,"context":null,"url":null}')
--############### SE_DECO 12333#################
