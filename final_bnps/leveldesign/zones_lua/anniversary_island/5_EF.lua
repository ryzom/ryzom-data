--############### SE_DECO 12333#################
-- GROUP: cake
-- 9094ce139b78801b30ca96203f2b4e8c773ac750
--SceneEditor:spawnShape("event_20years_cake_p0.shape", 17585.404, -794.663, 10.04, 0, 0, 1.12, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17585.404,"col pos y":-794.663,"col orientation":0,"col size x":24.031,"col size y":25.866,"col size z":14.265,"context":null,"url":null}')
SceneEditor:spawnShape("event_20years_cake.ps", 17585.404, -794.663, 10.04, 0, 0, 1.12, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17585.404,"col pos y":-794.663,"col orientation":0,"col size x":24.031,"col size y":25.866,"col size z":14.265,"context":null,"url":null}')


-- GROUP: plants_fyros
-- efc78921433923560aaba8390a9f5ddd82b5d0a9
SceneEditor:spawnShape("fy_s1_baobab_a.shape", 17491.58, -753.85, -0.415, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17491.58,"col pos y":-753.85,"col orientation":0,"col size x":4.386,"col size y":7.184,"col size z":20.842,"context":null,"url":null}')

-- 8e73c3298b7856f43fd96eb69fe2ff31575d9266
SceneEditor:spawnShape("fy_s1_baobab_b.shape", 17516.135, -642.627, -0.003, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17516.135,"col pos y":-642.627,"col orientation":0,"col size x":5.426,"col size y":6.454,"col size z":19.63,"context":null,"url":null}')

-- 81d2badac60638350307b3aad4984fc3c9d244d4
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17467.303, -659.009, -0.213, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17467.303,"col pos y":-659.009,"col orientation":0,"col size x":4.525,"col size y":5.638,"col size z":15.054,"context":null,"url":null}')

-- 5199771406cdbf89c3181e1655c1cdd3f9a19020
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17521.094, -723.489, -1.91, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17521.094,"col pos y":-723.489,"col orientation":0,"col size x":3.455,"col size y":4.568,"col size z":15.054,"context":null,"url":null}')

-- 059453330b49d64d9c99401dea1d5359c2ca96e9
SceneEditor:spawnShape("fy_s1_burnedtree_a.shape", 17451.564, -766.881, -0.233, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17451.564,"col pos y":-766.881,"col orientation":0,"col size x":4.095,"col size y":5.208,"col size z":15.054,"context":null,"url":null}')

-- 9761eb9c4b75bd99b137028332bf075c5b4b2b65
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17494.438, -680.192, -2.685, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17494.438,"col pos y":-680.192,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- a6caeb6f167eae3b028c89880de68b350e2dcea5
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17513.436, -737.158, -0.713, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17513.436,"col pos y":-737.158,"col orientation":0,"col size x":2.543,"col size y":2.444,"col size z":5.947,"context":null,"url":null}')

-- b84305666bfa55556268bbec89ab4897f046ff42
SceneEditor:spawnShape("fy_s2_lovejail_c.shape", 17460.102, -725.628, -1.943, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17460.102,"col pos y":-725.628,"col orientation":0,"col size x":3.167,"col size y":2.255,"col size z":5.141,"context":null,"url":null}')

-- c7676ba470391cb5b00c7f0fc2c754072b1d4212
SceneEditor:spawnShape("fy_s2_lovejail_c.shape", 17544.973, -689.067, -3.043, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17544.973,"col pos y":-689.067,"col orientation":0,"col size x":3.167,"col size y":2.255,"col size z":5.141,"context":null,"url":null}')
--############### SE_DECO 12333#################

