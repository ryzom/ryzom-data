--############### SE_DECO 12333#################
-- GROUP: buildings_marauder
-- 35c8f7cb606f5d1f0457d70f705f0652c66a00a0
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18124.07, -823.988, 0.818, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18124.07,"col pos y":-823.988,"col orientation":0,"col size x":8.533,"col size y":7.539,"col size z":5.553,"context":null,"url":null}')

-- 0bb273543b72d7759e3a7b395ab26741cbcae863
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18133.934, -845.229, 1.114, 0, 0, -2.66, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18133.934,"col pos y":-845.229,"col orientation":0,"col size x":8.533,"col size y":7.539,"col size z":5.553,"context":null,"url":null}')

-- dfb1156759f1ed2e78903bef9dfcd600c25f4eb9
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18124.943, -860.766, 1.511, 0, 0, -3.16, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18124.943,"col pos y":-860.766,"col orientation":0,"col size x":8.533,"col size y":7.539,"col size z":5.553,"context":null,"url":null}')

-- 66b8a244b5fe37f92f94017ca8928183136ddb2c
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18144.264, -802.802, 0.013, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18144.264,"col pos y":-802.802,"col orientation":0,"col size x":8.533,"col size y":7.539,"col size z":5.553,"context":null,"url":null}')

-- 2ed90462823e81f0e0a61b3745a785ac5738965f
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18159.455, -827.882, 0.169, 0, 0, -1.57, '{"scale x":2.31,"scale y":2.31,"scale z":2.31,"col pos x":18159.455,"col pos y":-827.882,"col orientation":0,"col size x":15.513,"col size y":14.519,"col size z":5.553,"context":null,"url":null}')

-- 61c8b7068c84a149bbf4c0356bd07a892985992e
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18102.123, -809.993, 0.184, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18102.123,"col pos y":-809.993,"col orientation":0,"col size x":8.533,"col size y":7.539,"col size z":5.553,"context":null,"url":null}')

-- 90532a47dc376510d45e4116fd9efedb5e6aefb4
SceneEditor:spawnShape("marauder_cn_yurt_small.shape", 18099.119, -854.061, 0.682, 0, 0, -3.05, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18099.119,"col pos y":-854.061,"col orientation":0,"col size x":8.533,"col size y":7.539,"col size z":5.553,"context":null,"url":null}')

-- ea220951b24e21b5cc95bfa187640590930454e9
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 18113.098, -854.81, 1.063, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18113.098,"col pos y":-854.81,"col orientation":0,"col size x":2.532,"col size y":1.299,"col size z":2,"context":null,"url":null}')

-- 4ec06140137a2d46a14951a0f396b41510e660a9
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 18119.49, -842.483, 1.138, 0, 0, 1, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18119.49,"col pos y":-842.483,"col orientation":1,"col size x":2.532,"col size y":1.299,"col size z":2,"context":null,"url":null}')

-- d7fc091801439692ea8753eb1bd48f17a9590bab
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 18151.367, -839.502, 0.055, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18151.367,"col pos y":-839.502,"col orientation":0,"col size x":2.532,"col size y":1.299,"col size z":2,"context":null,"url":null}')

-- 868d74ce40b520a42efeb29cb87607406879717f
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 18139.516, -800.017, -0.008, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18139.516,"col pos y":-800.017,"col orientation":0,"col size x":2.532,"col size y":1.299,"col size z":2,"context":null,"url":null}')

-- 1b0e236b454bb6ad34b09d6113ef6da8dedb3b26
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 18108.922, -805.155, 0.054, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18108.922,"col pos y":-805.155,"col orientation":0,"col size x":2.532,"col size y":1.299,"col size z":2,"context":null,"url":null}')

-- 80bbe704c1c771b757afcc5064a0d380a9887fb7
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 18091.727, -851.14, 0.304, 0, 0, 1.58, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18091.727,"col pos y":-851.14,"col orientation":1.65,"col size x":2.532,"col size y":1.299,"col size z":2,"context":null,"url":null}')

-- 178b27674488689d5d536a4b58ba343681273b44
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18088.836, -869.122, 0.146, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18088.836,"col pos y":-869.122,"col orientation":0,"col size x":0.811,"col size y":0.703,"col size z":5.674,"context":null,"url":null}')

-- e96bbe1567ed511732ca85cc4c07de1a597ba455
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18108.879, -874.478, 0.47, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18108.879,"col pos y":-874.478,"col orientation":3.726,"col size x":0.781,"col size y":0.653,"col size z":5.674,"context":null,"url":null}')

-- 47eb75299e4fe10029bc0b61fba179d6c3f170b6
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18128.732, -872.288, 1.401, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18128.732,"col pos y":-872.288,"col orientation":98.86,"col size x":0.761,"col size y":0.733,"col size z":5.674,"context":null,"url":null}')

-- a32526f296a34f3395b32791896f1c1bfae4a681
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18142.93, -861.271, 2.53, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18142.93,"col pos y":-861.271,"col orientation":3.726,"col size x":0.811,"col size y":0.653,"col size z":5.674,"context":null,"url":null}')

-- bf1c4cf17fc02150cdc6f169a0eaddc9721c6ba2
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18166.725, -846.412, 0.676, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18166.725,"col pos y":-846.412,"col orientation":0,"col size x":0.891,"col size y":0.803,"col size z":5.674,"context":null,"url":null}')

-- 2605a38bdfb34cb47aaa0c13af254660a8f0caea
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18149.588, -833.931, 0.21, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18149.588,"col pos y":-833.931,"col orientation":0,"col size x":0.871,"col size y":0.823,"col size z":5.674,"context":null,"url":null}')

-- 9f761d1e3f9f3669b199b2af330571dc5977c8b1
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18150.443, -822.359, 0.159, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18150.443,"col pos y":-822.359,"col orientation":0,"col size x":0.661,"col size y":0.733,"col size z":5.674,"context":null,"url":null}')

-- 724d604760d11b48f9c8f47b94f6129bf6c52ccd
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18092.805, -802.229, -0.007, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18092.805,"col pos y":-802.229,"col orientation":0,"col size x":0.761,"col size y":0.703,"col size z":5.674,"context":null,"url":null}')

-- 32821bb25f6cd125e7a53bc3e558013bbba93a35
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18086.838, -819.619, 0.073, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18086.838,"col pos y":-819.619,"col orientation":0,"col size x":1.041,"col size y":1.023,"col size z":5.674,"context":null,"url":null}')

-- 2f811722453bd68e5b62701b0036e025c1ab61e0
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18110.523, -845.523, 1.181, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110.523,"col pos y":-845.523,"col orientation":0,"col size x":0.801,"col size y":0.823,"col size z":5.674,"context":null,"url":null}')

-- 3f808b95434cd33cf0987087abd997cc087f2e7e
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18114.088, -819.917, 0.886, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18114.088,"col pos y":-819.917,"col orientation":0,"col size x":0.781,"col size y":0.623,"col size z":5.674,"context":null,"url":null}')

-- f15e3fc59ba467d6ec22e0a33d9d317be05b58aa
SceneEditor:spawnShape("ge_mission_barriere.shape", 18091, -843, 0.362, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18091,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- de8e169dce81b41834b27b6f32072d62a9765144
SceneEditor:spawnShape("ge_mission_barriere.shape", 18094, -843, 0.548, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18094,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- a084fe5bc3bf847da0c426c243caa08a74040926
SceneEditor:spawnShape("ge_mission_barriere.shape", 18097, -843, 0.736, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18097,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 03dff35f8ff7a5053313c8aabfed94e9de257aef
SceneEditor:spawnShape("ge_mission_barriere.shape", 18100, -843, 0.843, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18100,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 80884dd2252dc161baf509eb49fc78e8b703555e
SceneEditor:spawnShape("ge_mission_barriere.shape", 18103, -843, 1.01, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18103,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 7a94a2b95dcfcc7fe8df17c1cab9cfd2bd99c05e
SceneEditor:spawnShape("ge_mission_barriere.shape", 18106, -843, 1.13, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18106,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 55d53b03f5aa3811713b57b5bc3ba8b331aa8e9b
SceneEditor:spawnShape("ge_mission_barriere.shape", 18109, -843, 1.188, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18109,"col pos y":-843,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 2998235eb701f19f555ed9d4f769ef33f5457b75
SceneEditor:spawnShape("ge_mission_barriere.shape", 18110, -842, 1.121, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110,"col pos y":-842,"col orientation":-1.55,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 205b1af5653f6c9078bbe418b37cc80066338b9e
SceneEditor:spawnShape("ge_mission_barriere.shape", 18110, -839.5, 1.258, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110,"col pos y":-839.5,"col orientation":-1.55,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- ff77b5f4d69ebd22e1eaf0a114778628ab0bb243
SceneEditor:spawnShape("ge_mission_barriere.shape", 18110, -837, 1.239, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110,"col pos y":-837,"col orientation":-1.56,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 6159ab3df68a36a20033fa9b9f9783636fe778f1
SceneEditor:spawnShape("ge_mission_barriere.shape", 18110, -830.5, 1.29, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110,"col pos y":-830.5,"col orientation":1.58,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- ff9ae1155af755368dfee28c48821415c60d81d3
SceneEditor:spawnShape("ge_mission_barriere.shape", 18110, -827.5, 1.253, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110,"col pos y":-827.5,"col orientation":1.59,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- a5a758fb7767fe0a76939844f97563faa3132145
SceneEditor:spawnShape("ge_mission_barriere.shape", 18110, -825, 1.117, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18110,"col pos y":-825,"col orientation":1.57,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 2863f0713a79b08eb63b4e44235f203411f5a788
SceneEditor:spawnShape("ge_mission_barriere.shape", 18109, -824, 1.068, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18109,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 42c33985b549a90845df44b84670eef4d68ec425
SceneEditor:spawnShape("ge_mission_barriere.shape", 18106, -824, 1.009, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18106,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 432c4a714d01e2415f112bccb4183925fcad9c9c
SceneEditor:spawnShape("ge_mission_barriere.shape", 18103, -824, 0.945, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18103,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 4cf13fb7db296ccfbfcacfd3560fc6632c86c01e
SceneEditor:spawnShape("ge_mission_barriere.shape", 18100, -824, 0.728, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18100,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- b6fa87cefa20d25c286c5cae42c387f68a8e51e6
SceneEditor:spawnShape("ge_mission_barriere.shape", 18097, -824, 0.594, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18097,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- d29cb6be5dee43655e208deb4940a7da92e313df
SceneEditor:spawnShape("ge_mission_barriere.shape", 18094, -824, 0.403, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18094,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- abc803777c70d9e57a29c118e5512a8a6d2838ac
SceneEditor:spawnShape("ge_mission_barriere.shape", 18091, -824, 0.259, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18091,"col pos y":-824,"col orientation":0,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 0dcdc45f1a93f87448163da47b484463717f188d
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -825, 0.225, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-825,"col orientation":1.59,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 2d6084203305e9200bd588b80eab9ca44a251b0c
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -828, 0.202, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-828,"col orientation":1.57,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 8bd6a8280c3914837455e2964c90163e818271d2
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -831, 0.282, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-831,"col orientation":1.57,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 0bc6baf8adc7f25429926521dd7a30b1a02ab0be
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -834, 0.265, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-834,"col orientation":1.54,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 59ea52e3310f637852dd5c459808e63190d17c09
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -837, 0.248, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-837,"col orientation":1.6,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- b715c13ae647e6606ade9045841ae6c129258041
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -840, 0.279, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-840,"col orientation":1.57,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 93219c5fee1abf13ad3238706753b04467b695ef
SceneEditor:spawnShape("ge_mission_barriere.shape", 18090, -842, 0.263, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18090,"col pos y":-842,"col orientation":1.56,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 36c45f11ac28ff02001b3bd87cf1d27dfb3bcf74
SceneEditor:spawnShape("ge_mission_barriere.shape", 18111.359, -836.157, 1.303, 0, 0, -0.46, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18111.359,"col pos y":-836.157,"col orientation":-0.44,"col size x":3.424,"col size y":0.188,"col size z":2.303,"context":null,"url":null}')

-- 22f671ea40936ba3b43ac23108fb1f047af0db59
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 18085.297, -846.56, 0.066, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18085.297,"col pos y":-846.56,"col orientation":0,"col size x":0.961,"col size y":0.863,"col size z":5.674,"context":null,"url":null}')


-- GROUP: plants_matis
-- d3bfd747caa5eda9543b9c3644a667951da56bcd
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18080.309, -801.78, -0.005, 0, 0, 0, '{"col size x":1.697,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.308,"col size z":13.303,"scale y":1,"col pos y":-801.78,"col pos x":18080.309,"context":null,"url":null}')


-- GROUP: plants_zorai
-- 0e7c6084f7c54d09605927d064cc94ac5040cd2b
SceneEditor:spawnShape("ju_s1_bamboo.shape", 18144.932, -918.42, -0.796, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18144.932,"col pos y":-918.42,"col orientation":0,"col size x":7.507,"col size y":11.241,"col size z":52.171,"context":null,"url":null}')

-- ff0216a3d4b713e0ec4a18e754b890ef3d1fd8cf
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18115.607, -916.222, -1.359, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18115.607,"col pos y":-916.222,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 19ee0c96912f02e5644030612a29850f5301d295
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18093.945, -923.282, -0.703, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18093.945,"col pos y":-923.282,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 83f9edc151d0d0a5ed094d19155456eacc3df136
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18081.176, -933.906, -0.013, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18081.176,"col pos y":-933.906,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- eaa415ee38c069cc11836d83691321d0af7e19f9
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18100.297, -895.737, -0.559, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18100.297,"col pos y":-895.737,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 9537f09189fd8f584c18609b563ee78f64be5e2b
SceneEditor:spawnShape("ju_s3_fantree.shape", 18138.559, -876.009, 1.494, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18138.559,"col pos y":-876.009,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 1b88f245b2e6255a33c14d446597fb08dc7114bf
SceneEditor:spawnShape("ju_s3_fantree.shape", 18114.936, -871.061, 0.776, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18114.936,"col pos y":-871.061,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 9ee11bee5b764f9b0335c1f4ee36a43c6d07b93d
SceneEditor:spawnShape("ju_s3_fantree.shape", 18084.578, -885.586, -0.024, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18084.578,"col pos y":-885.586,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- ec275ed722c2dfd2d6c073d2f6474f037c7811f5
SceneEditor:spawnShape("ju_s3_fantree.shape", 18097.865, -927.527, -0.96, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18097.865,"col pos y":-927.527,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- 2a8a9c4334a3602b50b02ebc97b018b2538f6268
SceneEditor:spawnShape("ju_s3_fantree.shape", 18130.621, -942.008, -0.651, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18130.621,"col pos y":-942.008,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')
--############### SE_DECO 12333#################

