--############### SE_DECO 12333#################
-- GROUP: buildings_zorai
-- 55c1a84813fc6cd4e63ffd088f2d958ae0f53c0f
SceneEditor:spawnShape("zo_autel_bloc_zo_autel_village_a.shape", 18079, -1095, 3, 0, 0, 2.2, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-1095,"col orientation":0,"col size x":3.643,"col size y":14.298,"col size z":10.78,"context":null,"url":null}')


-- GROUP: plants_zorai
-- 9282cb9cf6d35fd36eef74fdb464c912f8187f51
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17941.555, -1075.165, 0.012, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17941.555,"col pos y":-1075.165,"col orientation":0,"col size x":0.611,"col size y":0.582,"col size z":16.447,"context":null,"url":null}')

-- 4a1ab1024f8433fa6abe144e6b3a2720a3fa3bcc
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17948.271, -1118.831, 0.009, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17948.271,"col pos y":-1118.831,"col orientation":0,"col size x":0.601,"col size y":0.572,"col size z":16.447,"context":null,"url":null}')

-- 91dd0cd4aee517783b6ce123551db43cfafd3534
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17984.26, -1079.813, -3.558, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17984.26,"col pos y":-1079.813,"col orientation":0,"col size x":0.521,"col size y":0.312,"col size z":16.447,"context":null,"url":null}')

-- a1ae8d1c81ad9f436b387c19c2a7249598c032ea
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17955.268, -1018.298, -0.614, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17955.268,"col pos y":-1018.298,"col orientation":0,"col size x":0.701,"col size y":0.652,"col size z":16.447,"context":null,"url":null}')

-- 69c8524f67d653989721edf682ae72f0e6570af8
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 17997.666, -994.621, -2.822, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17997.666,"col pos y":-994.621,"col orientation":2.356,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- c07ecc281867cc0248afeb2ef70a001c89ddc491
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 17998.656, -983.682, -2.827, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17998.656,"col pos y":-983.682,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- c25a9e244bd2f56354151d6f295aa8947fc77c1b
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18004.789, -974.34, -1.507, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18004.789,"col pos y":-974.34,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 174872c4ce150b6daccad05eeabb7f8f4f4bf32d
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18011.141, -984.046, -2.142, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18011.141,"col pos y":-984.046,"col orientation":2.984,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 41c834872a76bf2aac5ed1eec5a527b89caffb1b
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18009.584, -997.889, -2.185, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18009.584,"col pos y":-997.889,"col orientation":3.141,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- b9f022b965be030750e06cd56b14fac272a0a73b
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18051.533, -989.805, -1.014, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18051.533,"col pos y":-989.805,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 474a7f3b498adcdb0b9c02398f0356ad719391a4
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18034.77, -969.582, -0.72, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18034.77,"col pos y":-969.582,"col orientation":0.785,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- fc04954370cd60f7e35f1fe6d67f197dc0d1b447
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18035.471, -981.61, -2.327, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18035.471,"col pos y":-981.61,"col orientation":-2.343,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- ff563a2378f127f31022a01377e07909bd386d75
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18036.891, -997.163, -2.471, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18036.891,"col pos y":-997.163,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 1304858fd2954367a66b6cd249c74f822388c2be
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18053.156, -972.922, -0.423, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18053.156,"col pos y":-972.922,"col orientation":2.383,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 148614d6212e8582b2354036d43a1220539432e4
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18071.57, -964.949, -0.025, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18071.57,"col pos y":-964.949,"col orientation":3.141,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- d54c4c68b26de111ef1386d2527a38c6af854aa4
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18079.379, -989.047, 0.011, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18079.379,"col pos y":-989.047,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 96f5014398916b53cd0403e61301ba07c2ef0e7c
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18077.326, -1013.014, -0.014, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18077.326,"col pos y":-1013.014,"col orientation":0,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 319794779768903f3448539bc5dc4067f7156c52
SceneEditor:spawnShape("ju_s2_big_tree.shape", 18039.047, -1008.705, -1.984, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18039.047,"col pos y":-1008.705,"col orientation":0,"col size x":4.16,"col size y":3.575,"col size z":25.035,"context":null,"url":null}')

-- e336e43fe802b2f68d65eded49619e7dff7c6322
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17983.902, -974.044, -1.137, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17983.633,"col pos y":-974.441,"col orientation":0,"col size x":5.74,"col size y":5.375,"col size z":25.035,"context":null,"url":null}')

-- 2e8f5745cc86042a9635a051b8a59bed8ff55489
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17940.777, -1005.278, 0.101, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17940.777,"col pos y":-1005.278,"col orientation":0,"col size x":4.43,"col size y":3.625,"col size z":25.035,"context":null,"url":null}')

-- fc4454edafe488d3695a74e8b1495aca15c39104
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17940.826, -1061.954, -0.091, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17940.826,"col pos y":-1061.954,"col orientation":0,"col size x":3.42,"col size y":4.535,"col size z":25.035,"context":null,"url":null}')

-- 37c62029b2540bc719dc7efa1d533262fd2169ec
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17926.896, -1109.756, -0.015, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17926.896,"col pos y":-1109.756,"col orientation":0,"col size x":4.24,"col size y":4.725,"col size z":25.035,"context":null,"url":null}')

-- c0c575cab8a83bed6fd97028ff60e9a15c2587d2
SceneEditor:spawnShape("ju_s3_tree.shape", 18005.422, -1110.538, -0.556, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18005.098,"col pos y":-1111.543,"col orientation":2.188,"col size x":2.215,"col size y":2.745,"col size z":19.556,"context":null,"url":null}')

-- 364901d8524317174a8343f1936ee7efd4f08495
SceneEditor:spawnShape("ju_s3_tree.shape", 18013.012, -1071.919, -2.97, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18013.012,"col pos y":-1072.579,"col orientation":0,"col size x":2.285,"col size y":2.985,"col size z":19.556,"context":null,"url":null}')

-- 567f0016ca6ca331e4c57c36805fbbc3a209035d
SceneEditor:spawnShape("ju_s3_tree.shape", 18044.322, -1099.581, 0.338, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18044.322,"col pos y":-1100.228,"col orientation":0,"col size x":1.975,"col size y":2.795,"col size z":19.556,"context":null,"url":null}')

-- 183f814f22e4de0314360521238016bb99678f9c
SceneEditor:spawnShape("ju_s3_tree.shape", 18063.326, -1013.569, 0.146, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18063.326,"col pos y":-1014.127,"col orientation":0,"col size x":1.815,"col size y":2.315,"col size z":19.556,"context":null,"url":null}')

-- 5863514ee9983b1da698ca72bc3ee0a2ada5cf97
SceneEditor:spawnShape("ju_s3_tree.shape", 18018.273, -1022.061, -4.282, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18018.273,"col pos y":-1022.061,"col orientation":0,"col size x":4.115,"col size y":7.005,"col size z":19.556,"context":null,"url":null}')

-- 3e6e48621c258a2136c518a54c95d14ce1498a55
SceneEditor:spawnShape("ju_s3_tree.shape", 18023.338, -976.301, -1.732, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18023.338,"col pos y":-977.051,"col orientation":0,"col size x":1.245,"col size y":1.775,"col size z":19.556,"context":null,"url":null}')

-- 67de3ca8e580cf99265a3595a145f2aad9b76c77
SceneEditor:spawnShape("ju_s3_tree.shape", 18060.236, -979.725, -0.269, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18060.236,"col pos y":-980.344,"col orientation":0,"col size x":1.515,"col size y":2.415,"col size z":19.556,"context":null,"url":null}')

-- 2e41b3be58f345077d72f63aff465248d0041132
SceneEditor:spawnShape("ju_s3_fantree.shape", 18067.098, -1028.897, -0.365, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18067.098,"col pos y":-1028.897,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- d95f88fb6097bdcf0b74c0301d5ae491380fa94e
SceneEditor:spawnShape("ju_s3_fantree.shape", 18040.164, -1066.482, -3.222, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18040.164,"col pos y":-1066.482,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- e59ec118800e71449d6dd62a88dcb8debc1f0bd3
SceneEditor:spawnShape("ju_s3_fantree.shape", 18066.75, -1115.966, 0.017, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18066.75,"col pos y":-1115.966,"col orientation":0,"col size x":2.466,"col size y":2.199,"col size z":8.502,"context":null,"url":null}')

-- d1867963ff7733998a7e085886c1f536a6e2a0e4
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 18021.438, -1111.641, -0.104, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18021.438,"col pos y":-1111.641,"col orientation":0,"col size x":5.319,"col size y":5.665,"col size z":4.997,"context":null,"url":null}')

-- 532b43bc675b7ace5e0313e24dbc92fcd6ae9616
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 18065.324, -1054.087, -2.258, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18065.324,"col pos y":-1054.087,"col orientation":0,"col size x":5.319,"col size y":5.665,"col size z":4.997,"context":null,"url":null}')

-- cce94a582ddf5472e9b49c874ad556a91cf7a934
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 18008.254, -1038.34, -4.45, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18008.254,"col pos y":-1038.34,"col orientation":0,"col size x":5.319,"col size y":5.665,"col size z":4.997,"context":null,"url":null}')

-- f878c3e645739d240e81120c52965ad3389d14ab
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 17933.986, -1042.176, -0.237, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17933.986,"col pos y":-1042.176,"col orientation":0,"col size x":5.319,"col size y":5.665,"col size z":4.997,"context":null,"url":null}')

-- 8f98cb9b338c817b5e19f6f0c74306663c2ead8d
SceneEditor:spawnShape("ju_s1_bamboo.shape", 17963.561, -1111.396, -0.405, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17963.561,"col pos y":-1111.396,"col orientation":3.141,"col size x":9.207,"col size y":12.941,"col size z":52.171,"context":null,"url":null}')
--############### SE_DECO 12333#################

