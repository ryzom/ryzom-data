--############### SE_DECO 12333#################
-- GROUP: buildings_kami
-- 5c706cae0d93c2e273a027e3fab425e5fb0b96dc
SceneEditor:spawnShape("spire_fx_ka_11.ps", 17753.072, -1112.13, -0.158, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17753.072,"col pos y":-1112.13,"col orientation":0,"col size x":5.82,"col size y":5.82,"col size z":2,"context":null,"url":null}')

-- 36cb639e5264e13b3c6e7a3cb73db6f356d93dfc
SceneEditor:spawnShape("spire_fx_ka_11.ps", 17700.035, -1112.13, -0.319, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17700.035,"col pos y":-1112.13,"col orientation":0,"col size x":6.73,"col size y":6.73,"col size z":2,"context":null,"url":null}')

-- 405e947f69405738e227a27b952cad81a0a07d7a
SceneEditor:spawnShape("ge_mission_totem_kamik.shape", 17654.531, -1117.746, -0.079, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17654.531,"col pos y":-1117.746,"col orientation":3.141,"col size x":4.132,"col size y":2.62,"col size z":13.184,"context":null,"url":null}')


-- GROUP: plants_center
-- 43ba88b60df24de14f3dae9d3de71b4797c1173d
SceneEditor:spawnShape("fo_s2_arbragrelot.shape", 17618.15, -978.207, 9.987, 0, 0, 0, '{"col size x":3.389,"scale x":1,"col orientation":0,"scale z":1,"col size y":5.243,"col size z":28.184,"scale y":1,"col pos y":-978.207,"col pos x":17618.15,"context":null,"url":null}')

-- 3b6fbad03e9d00d3d187034f114399e016e1d286
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17714.652, -981.529, 9.777, 0, 0, 0, '{"col size x":1.173,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.853,"col size z":16.228,"scale y":1,"col pos y":-981.529,"col pos x":17714.652,"context":null,"url":null}')


-- GROUP: center
-- da64f12d5bdc974f6114de5ed5eeb4a9e7e621e4
SceneEditor:spawnShape("ge_partylamp_03.shape", 17619.6, -1003.724, 9.98, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17619.6,"col pos y":-1003.724,"col orientation":0,"col size x":1.325,"col size y":1.325,"col size z":5.267,"context":null,"url":null}')

-- 6d71aacbd8574e781634f2a548aff61123567060
SceneEditor:spawnShape("ge_partylamp_03.shape", 17650.297, -1011.786, 9.62, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17650.297,"col pos y":-1011.786,"col orientation":0,"col size x":1.405,"col size y":1.405,"col size z":5.267,"context":null,"url":null}')

-- acea54090bab36111618d4d8d89fb37ddd14d04f
SceneEditor:spawnShape("ge_partylamp_03.shape", 17679.537, -1017.464, 9.694, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17679.537,"col pos y":-1017.464,"col orientation":0,"col size x":1.605,"col size y":1.605,"col size z":5.267,"context":null,"url":null}')

-- 04ae000227da133ac1e9ec2d925fd99eb5d571c4
SceneEditor:spawnShape("ge_partylamp_03.shape", 17705.551, -1022.289, 9.729, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17705.551,"col pos y":-1022.289,"col orientation":0,"col size x":1.365,"col size y":1.365,"col size z":5.267,"context":null,"url":null}')

-- 76615536a1427371d6039760e39f4cfbb32a627e
SceneEditor:spawnShape("ge_partylamp_03.shape", 17743.301, -1033.218, 8.057, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17743.301,"col pos y":-1033.218,"col orientation":0,"col size x":1.445,"col size y":1.445,"col size z":5.267,"context":null,"url":null}')


-- GROUP: center_dancefloor
-- 16fc6464e7e70160f4c44692a06c9f9b21d8fd4d
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 276f6991ffc279e880aed538ed7bcfa448776e75
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 00054925283a719e30c9317369a8eb9bbf7b3ada
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 91f656fd43cd4b14de1a6324b9a17d57d5f0ce65
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 32b8aa78f4cc69f0bac1d890a3f2a9a04df29ae4
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c31fe6bd8d5f16b2177fa40ffe2e3152976280ce
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ad40aeb4baf2349ab5fc0cd4819240a81041f3af
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17667, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2af042a8fa3e257b45dc1d2c9179d4c58eca376b
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2068da96a78cc04d930809c9c03a6f2b088a0baa
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 8c2662f10ec5ed0e25d58b778a378fe0892a83bc
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 051b0d78464904bda0b0af2fb1d9971cb9e3983f
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17673, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a1082d033fb0b61472988ba4eec22b16390077c5
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 42e4cfda3518d48599f3d198e811d9bc16625cab
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d0273d0e4f2e1b2cec40e3056ed29162d1502c08
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9c412f918fa87a190b226948f5e26a93585262e6
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17679, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- bf900a670617c674bf9c39d0ee3f0c7cd839158d
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0d0e72dbe043c2821407fcec5a95c9f52d2d3122
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a541d2c53bfc862d63b62e7e9375374b7fdd8eb6
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 864fce4f2d2d9c72bd9417e38bfc171fe78f01b4
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17685, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f92ca8773472423c6bbdf118a4f3024eb8432c52
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 779634f02389102e08a112dc2304789a1fda1780
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f2defe96569c52fa4a6c72cf72910f8a9c76a5f7
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 3d5aa568c6c61b8231394ca48b0982a07fd91471
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17691, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0c24806cdd588697846cfd5f72f5af76bb6b9eef
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d6995792fef324efb1b697b2bbd98d3ed30919a4
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 166d0c0064ae6961bddd2a29de926bb29fabc109
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 886ff217bc542cea388575681c00bdec6b36efe9
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17697, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 404bae56c3e0fad598838c140b48b567096ceeba
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 511a5d97f8c2f1906ba29732133008b3256cd203
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e527fe1c5fd7ce2967270d9fa94a14293e8482b5
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 6edba68e6dc6553f0f311edfc60bcdbaa91bbf55
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17703, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9fa8d98a281c2bbb8a872e330d36142138b8574c
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 82a6002d1f62328c177b35989b4cd61cf67c0d5d
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f974397e8b7f7ee4039f960c0ebf5c7b31314225
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- dfe679e4813ef98f1f27dab034f1ef496ca095f8
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0c755a283e90a78e59c1a9b300913f1022710482
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17661, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c6b9104689ae6bc0d4a543311fc7ee5acf471118
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e2b37d7d6654938bd8a010595ff4cd7c2a080937
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d0303fe5eb4aa1454db639517ba6d87960176f9c
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 68f5a1d1d53b3f6632878b709c578869ecee8407
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 7afbf956fea18449fb674b2d616dea2c575a0b42
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17667, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 588f42bea78f686e2f41f4a876a0dbce55c6c0b6
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 446cdef947a3b3f7b8a78e5c8c44340c5913cc3b
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5d2a59d53af2c821d67743d7f89586e20e1881da
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 11015638ec2e3bdeebea1ac8eb92d5bf8d120144
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 94578920249a9fbd7782903ff7d32e144f45e5a9
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17673, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5270a0546a430314b5407dc44615159f37540d5c
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 63e8d7ebc187e67f013270727e7453a952806b07
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5f144e9f43f09cfdbef0761f2e1abafbafcd581d
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2a905e28b03d8f1322b633047ad0f3de19ac6d67
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 6d5a828817f0b2fc74d70388218c23344b6df284
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17679, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- cc322e37da7e2e5caf6b0531961d51fffa939265
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 55afb8a1d36cd439a3daad13397cdea94190f04c
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f3120c884ada08a9cf2271cd1ec22bba0dc2fe01
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 54880b45539a6cf616964d67b097dbd807ab0fde
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9ae1122aa741f8495cc3eac29edec32a1cd1e350
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17685, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d51c35dc2a41b085f6d11dd95570aea3fceffbf9
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d1746228d7dd36e567312ec3f189a4089f39a348
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e229205359cbe7768635852bf96bfdbd11c6813e
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 7724c8f6543741db7a73fa4b83636dcbc73100b4
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d8fd681554e7f1b780d30da091a48726dcec3017
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17691, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 909060d3e63e5b5c3e61b7ce44c0530cfd4c4f81
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e25ddf7cab4dbcbdfed336f8eb358f56be939e3b
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a988ca2bac7285c4879172903ba7adec62ba5921
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e9a63669bc10758ec1ffdb94e428d2fa8da894bc
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 20258a618ebfe87c16f5047f71d34893f12f1491
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17697, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 63ffbf83a3176e6f5ba997cb3facc1072e303598
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 21b05cea12cb246085171c1a929a7ff001ed863a
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 04651a3992decfe6d172fe7703095d30ec1e6186
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 8f3f585718d7001f7367094c36a442e2518ea4a9
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- bd8a23fa5af339772faab33955eca392b563ac83
SceneEditor:spawnShape("knowlege_topbox_tr.shape", 17703, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-82.434,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c4b141c6321a8118e89db769ca8f19c26dacd840
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0e116ac48ea5ec2dd28202bf92ab4b816f4cfc63
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5f5832fd1b0771657aaf7ee47bbd9e6096895909
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b496f12549b12efc6d0acd67b06d94d200740fc6
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17658, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ef6cfb4abfe9b8e3fc0cd79f710d9615035443d0
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 78c5e0e739cdb56ef74d3e7ae7a68dc6dc4d62d5
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f7a7df39151ca893cc2c6090cd3c3b0ca53f8b03
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e3184f3dd13b1132d5c41d32bf9ed4c2b8f9a837
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17664, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- dc9acddbe6c716f03c66f23db4f2dc14a65c255b
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 29a1b5b7ab82c3d9891856817edeb6ca456d08a2
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e889b8db1e24d7359d6c2c0692d1ccbe0b74af3e
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 1aac5ebd922df9af41c721400d115c15fb79ffd5
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17670, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a2c0a9ab3322d23611a2d11fa58098c54ae04798
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2377f1c819b6d9671341d878469901b50511a481
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e611d377361187d496616835c62368955355597e
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ae1ee9229eaa8785e61bf0c471e3e52be112a38d
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17676, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e944a747d9c0fd53e5dec27a72acf61dddd80535
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 42c26a2141ec4673728b02a39096ac94d77082ca
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 92c3744916b37e6388b9b215c166e64f88b344b0
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e3e63c16f5cf92099f6b283304d245e2720ef3a2
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17682, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- fef534877c31150b5a904be94f8237a33c910f13
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ce57aa6f43cbeff129e14b7b5aac15e99bab6800
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9f7481410d329f82868909cfaa261431fe1825d8
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5a2e3837268881b8070746a9a19d5764fa8e3dc1
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17688, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9e47e26d134be1eae4e95739052290869942d46b
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 7723224db9d211cd81627e1de27b603c3c1bc93b
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d1d15637f2a77fc9af5e16dd017f1e511c3e06c2
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 57c3a6d568a8ae16dd773ef08d6666e92f9bdeb3
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17694, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d0412cbfe5d882065e02b59f790ec48ff1d51520
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5ec2cc743c57a163e99f8e0ae15d93afdf5cc941
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -977, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a3d7f55bb0046a39c29e4defc53e6ad3dee98287
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -971, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 688854405402b2eee8bed8639eb4cef2642bc7e8
SceneEditor:spawnShape("knowlege_topbox_ma.shape", 17700, -965, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-84.342,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f1f8cd61a278c043c07478403c8cb99dcf232d96
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 278e456ddbe4ad7dfe90b8435f32b7a00f6a01b7
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 3f4d4fbac6067fe0898493ca5a8823c56343e2aa
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b2a19cb0945bdf2394f1fe81a486ade819a42a48
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d5c4adcd69f266b7e09a3da56e2aa316e3a9503b
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17658, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5e90eaff569880e44b433b041c1e287d3361aba9
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 251b4d39f8c9b4fa70161e2dfe4e633d220c45fd
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 353a034f9462a47d1ee40d312936a1ab0471976c
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 7c8025cfb9cc1ebfe6cd7f783ac81780b781a12a
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 18be6814df9c08e891ac6b282a09fbbb9a6ad3ef
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17664, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0651d1304158f27a59a4a53d24564903a2d41aa3
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- fac342c6260d367165e91350d4469d7acd806697
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 5fcf9651645643de6932fa2ff83b42e55bf01efc
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b7c21f39134e48ad9fb2a0bfa9633a94b6404425
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 4df7861da22ca493568d1ffb0d3e91123fe07f39
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17670, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 209d6a648a9ae66725cf079b0b8356d0477edb4f
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 0120d35d40451fdb65b6325a2e9b4bd4820e7e2d
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a69fd4054e5fd0bc2454b5ed32e649af37e2bebf
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- a62e5e03518958aacab558da3e6b8f23bde615d0
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 626666862715ca7ac74d1d564524b241443b6d34
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17676, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ce97eb91e45aa965310c292091274d72ce07f91c
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- cff1b7b43cb24a482a6ec4e951cd1e74fe004f84
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- e576ed6ce21221bf491fd5aaedbca7efa8fbc66b
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- fb64fdf1987dc4e8897d47eaca4adaa73d92cc2f
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 299ce680cf5192424b23ee914a614a7f38a13389
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17682, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- f60fd5f628d9786401c683b29d4a004c3264bfa2
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- c87d4893fac6041d72a4c40ef0652e3f75126ed1
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 85268397b3b81de759c8ca6f4229a9b90c5872c9
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b4f8346f9103dded0587272b7aac247bacc6bb79
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- b61715e61d555a5da1daffa6566c541d973e13c8
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17688, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- ec64fad4cf81de52851ee348ac9e982fd95a8791
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 6c86ed61d7bb181dba415ea63a621a50430070c7
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 9bf9a96b33eb3945ec963c3ffe152be324351f8e
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2f0e60e2a32ef20ac18af28ca66a024088d41ffd
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 2a8a792171cf50a20c9a69646cb2ef4cadd31dd7
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17694, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 128657245d771487bcc8e44080bafda303ea58e9
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -986, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 02c2805bf175d272bf79ab38e8ae42df667c8948
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -980, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- d74e9823b7fc456f526f0c500412f19b5526a050
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -974, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 6ec3170dc6ae22a5af102f2f9484c38be540226a
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -968, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 579e6780602c856860903b4f2d61cd2662f63e08
SceneEditor:spawnShape("knowlege_topbox_zo.shape", 17700, -962, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-85.428,"col pos y":-980,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 1788747d4267ce3076be47613d875a8c5de61d62
SceneEditor:spawnShape("ge_partylamp_02.shape", 17661.588, -988.606, 10.445, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17656.105,"col pos y":-987.646,"col orientation":2.561,"col size x":5.346,"col size y":1.883,"col size z":5.18,"context":null,"url":null}')

-- b2d328e1fe8199a176b21a152741d96a11a5ee79
SceneEditor:spawnShape("ge_partylamp_02.shape", 17671.461, -988.66, 10.192, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17666.49,"col pos y":-988.66,"col orientation":0,"col size x":4.056,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 8ff7f84f07c251fc6d451e0ee58f162cccd6e739
SceneEditor:spawnShape("ge_partylamp_02.shape", 17681.59, -988.82, 9.925, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17676.576,"col pos y":-988.82,"col orientation":0,"col size x":4.236,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 9cd278fc5dd5256bad81d79ee5afd16a55f1791d
SceneEditor:spawnShape("ge_partylamp_02.shape", 17691.248, -988.846, 9.74, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17686.391,"col pos y":-988.846,"col orientation":0,"col size x":4.016,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 94a1f6fb1571a0446c91a8d27d0de8d5481ff34e
SceneEditor:spawnShape("ge_partylamp_02.shape", 17701.223, -988.961, 9.614, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17696.203,"col pos y":-988.961,"col orientation":0,"col size x":4.006,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- a1452b3d9560f6bf512d5ccdbc07095439584bab
SceneEditor:spawnShape("ge_partylamp_02.shape", 17706.131, -982.412, 9.693, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17705.35,"col pos y":-987.575,"col orientation":1.16,"col size x":3.966,"col size y":1.813,"col size z":5.18,"context":null,"url":null}')

-- 71e9f3a81a4cbfd17fb6ba88b81d42983afcd879
SceneEditor:spawnShape("ge_partylamp_02.shape", 17706.326, -972.677, 9.837, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17706.326,"col pos y":-977.585,"col orientation":1.55,"col size x":3.606,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- ed71b7c8a10d744b44fcbc233fa33c801c3a00b2
SceneEditor:spawnShape("ge_partylamp_02.shape", 17654.693, -963.51, 10.106, 0, 0, -1.58, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17654.693,"col pos y":-958.74,"col orientation":1.56,"col size x":3.596,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 6edb2df8b8d6d38799e1b54f8cbfcdf93a525883
SceneEditor:spawnShape("ge_partylamp_02.shape", 17654.348, -973.031, 10.265, 0, 0, -1.56, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17654.348,"col pos y":-968.217,"col orientation":1.54,"col size x":3.436,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- 52b18036e92ed347b65642b982a23a2fabeb773e
SceneEditor:spawnShape("ge_partylamp_02.shape", 17654.471, -982.552, 10.448, 0, 0, -1.6, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17654.471,"col pos y":-977.81,"col orientation":1.55,"col size x":3.506,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- f8b84afede4f7288fab83f5dcf88829695f65b8e
SceneEditor:spawnShape("knowlege_topbox_fy.shape", 17661, -983, 10.4, 0, 0, 0, '{"scale x":3,"scale y":3,"scale z":1,"col pos x":-81.422,"col pos y":-977,"col orientation":0,"col size x":1,"col size y":1,"col size z":0.08,"context":null,"url":null}')

-- 54fdc3f14e2f32ed69324c7d71a5d07c33b732a5
SceneEditor:spawnShape("ge_partylamp_02.shape", 17706.613, -963.103, 9.926, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17706.613,"col pos y":-967.848,"col orientation":1.56,"col size x":3.675,"col size y":0.743,"col size z":5.18,"context":null,"url":null}')

-- ed64b654a63093e8ff439fa061f20765613be36b
SceneEditor:spawnShape("ma_acc_chaise01_hall_reunion.shape", 17651.541, -965.937, 10.136, 0, 0, 3.14, '{"scale x":2,"scale y":4,"scale z":1.5,"col pos x":17651.541,"col pos y":-965.937,"col orientation":0,"col size x":1.421,"col size y":2.81,"col size z":1.363,"context":null,"url":null}')

-- 5ae4fadd7f93344709634cfc4e096da09f3ec66a
SceneEditor:spawnShape("ma_acc_chaise01_hall_reunion.shape", 17651.688, -960.383, 10.053, 0, 0, 3.14, '{"scale x":2,"scale y":4,"scale z":1.5,"col pos x":17651.688,"col pos y":-960.383,"col orientation":0,"col size x":1.551,"col size y":2.92,"col size z":1.363,"context":null,"url":null}')

-- 4981db824ab00980671919f7bcf02a7cb2f11ba7
SceneEditor:spawnShape("xmas_lights_02.shape", 17668.477, -973.42, 10.252, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-973.42,"col orientation":0,"col size x":5.591,"col size y":5.521,"col size z":2.333,"context":null,"url":null}')

-- 72fc5b5a8506acd3e0f00e5800e7c9355d7e4744
SceneEditor:spawnShape("xmas_lights_02.shape", 17692.566, -973.689, 9.906, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-973.689,"col orientation":0,"col size x":5.591,"col size y":5.521,"col size z":2.333,"context":null,"url":null}')


-- GROUP: center_arena
-- 96fe4c44cf42b682e51f03496b180088fa976822
SceneEditor:spawnShape("ge_mission_crane.shape", 17756.867, -998.89, 10.008, 0, 0, 0.07, '{"col size x":13.502,"scale x":2.14,"col orientation":-0.13,"scale z":2.14,"col size y":14.092,"col size z":6.826,"scale y":2.14,"col pos y":-998.89,"col pos x":17756.867,"context":null,"url":null}')

-- 888b158c1f8a7faa6b3d529f15222fce2c2b1c56
SceneEditor:spawnShape("ge_feudecamp.ps", 17741.492, -967.586, 10.003, 0, 0, 0, '{"col size x":1.001,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.118,"col size z":5.716,"scale y":1,"col pos y":-967.586,"col pos x":17741.492,"context":null,"url":null}')

-- 375763592ee9125e974b705d2cfa474108fc6066
SceneEditor:spawnShape("ju_fo_brumes.ps", 17757.938, -965.099, 9.992, 0, 0, 0, '{"col size x":30.111,"scale x":1,"col orientation":0,"scale z":1,"col size y":18.025,"col size z":11.766,"scale y":1,"col pos y":-965.099,"col pos x":0,"context":null,"url":null}')

-- 84969513bfc85ebfa7f91a885836f8459a8100a5
SceneEditor:spawnShape("goo_mo_charognemamal.ps", 17747.344, -974.951, 10.032, 0, 0, 0, '{"col size x":5.731,"scale x":1,"col orientation":0,"scale z":1,"col size y":8.971,"col size z":9.009,"scale y":1,"col pos y":-974.951,"col pos x":0,"context":null,"url":null}')

-- ccc55abcf29f7dbf731896dc149956158dc1e204
SceneEditor:spawnShape("ge_mission_barriere.shape", 17756.42, -987.88, 10.092, 0, 0, 6.28, '{"col size x":3.424,"scale x":1,"col orientation":6.28,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-987.88,"col pos x":17756.42,"context":null,"url":null}')

-- f2a29a320a43f9756cd76d3f004e28d81997c3f7
SceneEditor:spawnShape("ge_mission_barriere.shape", 17753.279, -987.72, 10.092, 0, 0, 6.18, '{"col size x":3.424,"scale x":1,"col orientation":6.18,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-987.72,"col pos x":17753.279,"context":null,"url":null}')

-- 8ada3f3e5a7c72557a170ddfe4a2c6e7188d1897
SceneEditor:spawnShape("ge_mission_barriere.shape", 17750.18, -987.22, 10.092, 0, 0, 6.07, '{"col size x":3.424,"scale x":1,"col orientation":6.07,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-987.22,"col pos x":17750.18,"context":null,"url":null}')

-- cbe8121053dbc6fc01baaeba31a7767ad1c680f0
SceneEditor:spawnShape("ge_mission_barriere.shape", 17747.15, -986.41, 10.092, 0, 0, 5.97, '{"col size x":3.424,"scale x":1,"col orientation":5.97,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-986.41,"col pos x":17747.15,"context":null,"url":null}')

-- 1a8470c63243c0178ed3c431fc7b14714c58c949
SceneEditor:spawnShape("ge_mission_barriere.shape", 17744.221, -985.29, 10.092, 0, 0, 5.86, '{"col size x":3.424,"scale x":1,"col orientation":5.86,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-985.29,"col pos x":17744.221,"context":null,"url":null}')

-- c31f1620c065e186f8a0b13c792c92dd5d0c30ae
SceneEditor:spawnShape("ge_mission_barriere.shape", 17741.42, -983.86, 10.092, 0, 0, 5.76, '{"col size x":3.424,"scale x":1,"col orientation":5.76,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-983.86,"col pos x":17741.42,"context":null,"url":null}')

-- e20866089669510b99fef50d0966669635ed58cd
SceneEditor:spawnShape("ge_mission_barriere.shape", 17738.789, -982.15, 10.092, 0, 0, 5.65, '{"col size x":3.424,"scale x":1,"col orientation":5.65,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-982.15,"col pos x":17738.789,"context":null,"url":null}')

-- 346b9d3f8eafb33916a005ee920e35ec58318d4f
SceneEditor:spawnShape("ge_mission_barriere.shape", 17736.35, -980.17, 10.092, 0, 0, 5.55, '{"col size x":3.424,"scale x":1,"col orientation":5.55,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-980.17,"col pos x":17736.35,"context":null,"url":null}')

-- 976892ca15136b9612fd1737c3b73898bf83bc28
SceneEditor:spawnShape("ge_mission_barriere.shape", 17734.131, -977.95, 10.092, 0, 0, 5.44, '{"col size x":3.424,"scale x":1,"col orientation":5.44,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-977.95,"col pos x":17734.131,"context":null,"url":null}')

-- d038e27b414dd70c839ae38b1ab5254cb909d790
SceneEditor:spawnShape("ge_mission_barriere.shape", 17732.15, -975.51, 10.092, 0, 0, 5.34, '{"col size x":3.424,"scale x":1,"col orientation":5.34,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-975.51,"col pos x":17732.15,"context":null,"url":null}')

-- e79cb92252e81d600b1fd10f87b11e13b10c5570
SceneEditor:spawnShape("ge_mission_barriere.shape", 17730.439, -972.88, 10.092, 0, 0, 5.24, '{"col size x":3.424,"scale x":1,"col orientation":5.24,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-972.88,"col pos x":17730.439,"context":null,"url":null}')

-- 1ccf3538756cedcecf35183631d6cd3bc35f9c71
SceneEditor:spawnShape("ge_mission_barriere.shape", 17729.01, -970.08, 10.092, 0, 0, 5.13, '{"col size x":3.424,"scale x":1,"col orientation":5.13,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-970.08,"col pos x":17729.01,"context":null,"url":null}')

-- b43e84ca64eb38c3929668fa292719873f8e0ed3
SceneEditor:spawnShape("ge_mission_barriere.shape", 17727.891, -967.15, 10.092, 0, 0, 5.03, '{"col size x":3.424,"scale x":1,"col orientation":5.03,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-967.15,"col pos x":17727.891,"context":null,"url":null}')

-- 33ecd23d2fd8b8fe7a7d3477755693d2dba36922
SceneEditor:spawnShape("ge_mission_barriere.shape", 17727.08, -964.12, 10.092, 0, 0, 4.92, '{"col size x":3.424,"scale x":1,"col orientation":4.92,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-964.12,"col pos x":17727.08,"context":null,"url":null}')

-- cfe3859b85db8f6ee540d285a68ca4e162be13fb
SceneEditor:spawnShape("ge_mission_barriere.shape", 17726.58, -961.02, 10.092, 0, 0, 4.82, '{"col size x":3.424,"scale x":1,"col orientation":4.82,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-961.02,"col pos x":17726.58,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- 285a3d7863548271b4b2eb702dac450441f95630
SceneEditor:spawnShape("GE_Mission_borne.shape", 17753.61, -1010.34, 10.217, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17753.61,"col pos y":-1010.34,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2f45dcd13d3b03dcb86833e003bf1ac1301445f4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17748.33, -1007.94, 10.081, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17748.33,"col pos y":-1007.94,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3a8c3b9f9e2736d8c185c7c97a6020dd7f83b6f5
SceneEditor:spawnShape("GE_Mission_borne.shape", 17736.39, -1004.93, 9.934, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17736.39,"col pos y":-1004.93,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 1127c13b56c5e61448d42201a452b9d1adac9dff
SceneEditor:spawnShape("GE_Mission_borne.shape", 17731.16, -1005.76, 9.893, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17731.16,"col pos y":-1005.76,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 89c75224ca8f1b50266199fe4efe3fb3b6b1feec
SceneEditor:spawnShape("GE_Mission_borne.shape", 17724.97, -1007.04, 9.821, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17724.97,"col pos y":-1007.04,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d0870be7e99a7117d2cf2e59657af5b9489e8df9
SceneEditor:spawnShape("GE_Mission_borne.shape", 17719.48, -1007.59, 9.753, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17719.48,"col pos y":-1007.59,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 5bd2a4221477a14fc4653edf7a41ea610ee25484
SceneEditor:spawnShape("GE_Mission_borne.shape", 17713.33, -1008.22, 9.716, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17713.33,"col pos y":-1008.22,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 69a9dd624a9fd9c1581fae35e9d395ac4f88832d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17707.02, -1009.48, 9.678, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17707.02,"col pos y":-1009.48,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ee0df6580ef91276a09790f75c40687828810e04
SceneEditor:spawnShape("GE_Mission_borne.shape", 17701.09, -1010.92, 9.709, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17701.09,"col pos y":-1010.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2334a224fa29b4780293520efa67cf5309bb83f0
SceneEditor:spawnShape("GE_Mission_borne.shape", 17694.82, -1011.51, 9.715, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17694.82,"col pos y":-1011.51,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e2bf60834999a2890089029f419f5aa93ac09ed2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17688.34, -1011.85, 9.712, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17688.34,"col pos y":-1011.85,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 61e189e38398c85abd3850b9730e01c26576ee49
SceneEditor:spawnShape("GE_Mission_borne.shape", 17681.82, -1012.53, 9.758, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17681.82,"col pos y":-1012.53,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 78e9183a78053a34da2d195db0aead31d47f8d5b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17676.13, -1012.98, 9.823, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17676.13,"col pos y":-1012.98,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 69562eebcb12a95fb98f143e055cab3909fd908b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17669.8, -1012.74, 9.733, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17669.8,"col pos y":-1012.74,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 61b9d584e930c030fc5ddce2db4e1de62731fe63
SceneEditor:spawnShape("GE_Mission_borne.shape", 17664.35, -1012.99, 9.705, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17664.35,"col pos y":-1012.99,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 506f2bb51f43518fef98124372c734691dfa0a79
SceneEditor:spawnShape("GE_Mission_borne.shape", 17656.6, -1012.42, 9.626, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17656.6,"col pos y":-1012.42,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e4870d3634920960b4bead3646df5cfb73c2b0bf
SceneEditor:spawnShape("GE_Mission_borne.shape", 17644.02, -1010.5, 9.844, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17644.02,"col pos y":-1010.5,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c374978747e260551ec5dd84429c53e6d8e0e2b4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17650.07, -1010.43, 9.72, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17650.07,"col pos y":-1010.43,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 22370110130939581b23d66d083f331f9797d554
SceneEditor:spawnShape("GE_Mission_borne.shape", 17637.69, -1009.72, 10, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17637.69,"col pos y":-1009.72,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0d3e40fd8b914c961c0118b6875ebc39a1d40048
SceneEditor:spawnShape("GE_Mission_borne.shape", 17631.89, -1010.27, 9.977, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17631.89,"col pos y":-1010.27,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 42d3042883d3a2e8c47b8c89c904d53fb7bafee2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17625.64, -1010.35, 9.948, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17625.64,"col pos y":-1010.35,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f24c6733ff5ff1bebc226bdac57225f094a22cd2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17619.69, -1009.55, 10.095, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17619.69,"col pos y":-1009.55,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 91b28c3409afcbc00f13e291953ab7dfa001d098
SceneEditor:spawnShape("GE_Mission_borne.shape", 17614.23, -1008.89, 10.252, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17614.23,"col pos y":-1008.89,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2a6f6bd2d38321cea0231202f64d97aff8e7ea50
SceneEditor:spawnShape("GE_Mission_borne.shape", 17606.86, -1007.56, 10.305, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17606.86,"col pos y":-1007.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e8856410d643ce282e68e252ac39b3c4fbbe61ae
SceneEditor:spawnShape("GE_Mission_borne.shape", 17601.49, -1007.17, 10.354, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17601.49,"col pos y":-1007.17,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- cace9f9081032ee6b94ad0d292435bde4653badc
SceneEditor:spawnShape("GE_Mission_borne.shape", 17602.67, -1021.55, 10.194, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17602.67,"col pos y":-1021.55,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f5aef83b8a354d70381db00b7c506aa29235b1fe
SceneEditor:spawnShape("GE_Mission_borne.shape", 17610.06, -1022.01, 10.102, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17610.06,"col pos y":-1022.01,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3977552bf488e202869b9745fc8d8cf269b5a480
SceneEditor:spawnShape("GE_Mission_borne.shape", 17617.81, -1023.04, 10.031, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17617.81,"col pos y":-1023.04,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 81189907ac306bd2c45c2de1cc23043330f1fb29
SceneEditor:spawnShape("GE_Mission_borne.shape", 17625.17, -1024.46, 9.789, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17625.17,"col pos y":-1024.46,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2e875e5bf5501da7d2daf8ad9f550209748aa80c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17631.52, -1024.85, 9.644, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17631.52,"col pos y":-1024.85,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 882432ede0270eb1bdb9500f13f5e9e096e3ba32
SceneEditor:spawnShape("GE_Mission_borne.shape", 17637.82, -1027.22, 9.214, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17637.82,"col pos y":-1027.22,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 8d9e672c435ea27d6472333d2744d6fa77a22f2f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17644.35, -1028.58, 8.602, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17644.35,"col pos y":-1028.58,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 808b4ab754db822f7396f9acee61a6a6bdc1e22a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17650.99, -1030.86, 7.945, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17650.99,"col pos y":-1030.86,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f0f2ed8fd8ec499ba580a55d6905d9a75ddba2f8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17656.84, -1033.09, 7.331, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17656.84,"col pos y":-1033.09,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- bc925ac3b25a7886d8b7cccac3fd3235784a856b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17661.91, -1034.73, 7.024, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17661.91,"col pos y":-1034.73,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 968c961465db325caa7a24f6c863a8ba20e526d9
SceneEditor:spawnShape("GE_Mission_borne.shape", 17667.77, -1035.99, 6.834, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17667.77,"col pos y":-1035.99,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ad7c9267e48dfb74a007d12d47c58ffe88f2be27
SceneEditor:spawnShape("GE_Mission_borne.shape", 17674.12, -1036.4, 7.36, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17674.12,"col pos y":-1036.4,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 593c6520c35d11e79cce3ae1ab9b3dcee510ab37
SceneEditor:spawnShape("GE_Mission_borne.shape", 17679.6, -1034.69, 8.142, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17679.6,"col pos y":-1034.69,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3ecf727c1898ce932ee1d2a44ea24fc661c5a750
SceneEditor:spawnShape("GE_Mission_borne.shape", 17685.6, -1033.52, 8.748, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17685.6,"col pos y":-1033.52,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e20f9cca7a71d827fae9aa1fc57e9fa979094b17
SceneEditor:spawnShape("GE_Mission_borne.shape", 17690.83, -1032.43, 9.073, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17690.83,"col pos y":-1032.43,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 83ef0b90446d38862b283581d34d023b6ec95cf2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17696.5, -1031.05, 9.333, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17696.5,"col pos y":-1031.05,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a3894b983513382e4b6467c4f439e24889ede425
SceneEditor:spawnShape("GE_Mission_borne.shape", 17701.89, -1029.78, 9.233, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17701.89,"col pos y":-1029.78,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2058cdfac73e022b7dd2c860ec6317ecdcea274d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17708.24, -1028.89, 8.664, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17708.24,"col pos y":-1028.89,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0aec8fb7fe6f3e35414dceba9e2a2251a9f40e54
SceneEditor:spawnShape("GE_Mission_borne.shape", 17714.02, -1028.21, 8.227, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17714.02,"col pos y":-1028.21,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b45fab7a5fac5642eaaf98a2c045ed306a0808af
SceneEditor:spawnShape("GE_Mission_borne.shape", 17719.66, -1027.56, 8.533, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17719.66,"col pos y":-1027.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 184a7e9532eedab32ca5bac80efce5e6b901529d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17725.95, -1027.02, 8.584, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17725.95,"col pos y":-1027.02,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 573a49af2f87d85f54b7c501f17f643b55445ec8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17732.22, -1025.84, 8.834, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17732.22,"col pos y":-1025.84,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- fb8510f8ba81babfb09caa7c01b5babdf8ae0602
SceneEditor:spawnShape("GE_Mission_borne.shape", 17738.02, -1026.71, 8.854, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17738.02,"col pos y":-1026.71,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 780fd4fb4231ca47c31083dc1e4c64d33b5a58c1
SceneEditor:spawnShape("GE_Mission_borne.shape", 17743.72, -1027.55, 8.922, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17743.72,"col pos y":-1027.55,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 22dee053d153e427e8db93fe7f02f69b71237f09
SceneEditor:spawnShape("GE_Mission_borne.shape", 17750.32, -1029.42, 8.946, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17750.32,"col pos y":-1029.42,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- fa2eac15e2324c784b5cc443929118dadab76a05
SceneEditor:spawnShape("GE_Mission_borne.shape", 17756.27, -1032.37, 8.959, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17756.27,"col pos y":-1032.37,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################

--############### SE_deco_Memorial 12437#################
-- GROUP: memorial
-- 31698c00782de686e0ffa5ee820241032de0bc49
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17613.299, -1109.106, -0.076, 0, 0, -2.74, '{"col pos x":17613.299,"col size x":10.427,"scale z":1,"col size y":14.161,"col orientation":-2.91,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1109.106,"context":null,"url":null}')

-- beec9a6ee6c71b690313b479e017bec8930b461b
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17608.162, -1098.839, -0.186, 0, 0, -2.56, '{"col pos x":17608.162,"col size x":9.627,"scale z":1,"col size y":13.361,"col orientation":0.771,"scale y":1,"col size z":52.171,"scale x":1,"col pos y":-1098.839,"context":null,"url":null}')
--############### SE_deco_Memorial 12437#################


