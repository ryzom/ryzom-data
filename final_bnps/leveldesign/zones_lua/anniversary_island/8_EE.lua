--############### SE_DECO 12333#################
-- GROUP: buildings_tryker
-- c72562aa310c6a6ac2e742733e4d8cea8b6325b9
SceneEditor:spawnShape("gen_bt_silo.shape", 17280.305, -1158.071, -0.079, 0, 0, -2.65, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17280.305,"col pos y":-1158.071,"col orientation":-2.62,"col size x":4.181,"col size y":5.563,"col size z":10.403,"context":null,"url":null}')


-- GROUP: plants_tryker
-- eb959daa19bea636dcb14fc118eb6b06370b6560
SceneEditor:spawnShape("tr_s1_bamboo_a.shape", 17282.021, -1133.409, -0.038, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17282.021,"col pos y":-1133.409,"col orientation":0,"col size x":11.297,"col size y":15.031,"col size z":52.171,"context":null,"url":null}')

-- 79d42d1501cc17589754acc8320431a83217a78a
SceneEditor:spawnShape("tr_s2_nenufly_a.shape", 17386.738, -1161.073, 2.525, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17386.738,"col pos y":-1161.073,"col orientation":0,"col size x":2.653,"col size y":2.653,"col size z":11.213,"context":null,"url":null}')

-- 71ae8353e7042acb539b207c23483a37296ed19b
SceneEditor:spawnShape("tr_s2_nenufly_a.shape", 17338.201, -1181.359, -0.048, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17338.201,"col pos y":-1181.359,"col orientation":0.38,"col size x":3.523,"col size y":3.523,"col size z":11.213,"context":null,"url":null}')

-- 3a57732a9320fb00847ec0760a9695d73af37927
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17381.004, -1128.825, -0.001, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17381.004,"col pos y":-1128.825,"col orientation":0,"col size x":1.123,"col size y":1.803,"col size z":16.228,"context":null,"url":null}')

-- 109c86d29f2d7af605b46f94323912ed543bad54
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17310.945, -1154.201, -1.521, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17310.945,"col pos y":-1154.201,"col orientation":0,"col size x":1.233,"col size y":1.913,"col size z":16.228,"context":null,"url":null}')

-- f4460da7cd3e8b780fcb41e9d6fe7c134d50efa9
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17372.348, -1187.96, -1.305, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17372.348,"col pos y":-1187.96,"col orientation":0,"col size x":0.433,"col size y":1.113,"col size z":16.228,"context":null,"url":null}')

-- 3263fc1ab38bf57d09323464149f053b049fd7e0
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17346.953, -1152.244, 0.019, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17346.953,"col pos y":-1152.244,"col orientation":0,"col size x":12.203,"col size y":12.883,"col size z":16.228,"context":null,"url":null}')

-- 15d567fe624c827659578476b31111d8da60a990
SceneEditor:spawnShape("tr_s3_nenufly_b.shape", 17401.162, -1158.518, 1.431, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17401.162,"col pos y":-1158.518,"col orientation":0,"col size x":1.156,"col size y":1.156,"col size z":5.446,"context":null,"url":null}')

-- 4e7786dddefb7246f8535f6fd688bc82675eaaeb
SceneEditor:spawnShape("tr_s3_nenufly_b.shape", 17338.385, -1121.465, -0.039, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17338.385,"col pos y":-1121.465,"col orientation":0,"col size x":6.256,"col size y":6.256,"col size z":5.446,"context":null,"url":null}')

-- 447a9afb3d0581ccd1c3682a786d78193458841b
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17301.889, -1166.573, -0.85, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17301.889,"col pos y":-1166.573,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 132151744906275ffbfd00652adba5f36d23dfbf
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17315.684, -1132.692, -0.56, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17315.684,"col pos y":-1132.692,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 9f974297ae5fa2d8e61344f654bf449db6f25e46
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17281.867, -1163.68, -0.034, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17281.867,"col pos y":-1163.68,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 52ceafe44a731bf6b005bb0731c15677fe1befb5
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17321.457, -1193.404, -0.791, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17321.457,"col pos y":-1193.404,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 2ce0c4161c63b1f236ef55ab722b05ea36691105
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17346.201, -1193.42, 0.805, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17346.201,"col pos y":-1193.42,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 3bb8c669f3a94354fd6e01ccc3d364d4cbd370ab
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17369.574, -1208.685, 1.264, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17369.574,"col pos y":-1208.685,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 003e05c1111106965efdbdbae14313c407bc8551
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17381.57, -1189.114, -0.766, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17381.57,"col pos y":-1189.114,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- 08cf65c24d599e66919c4b8f248a06b75761dfa5
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17393.9, -1179.572, 1.053, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17393.9,"col pos y":-1179.572,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- a964f0d71a45cc799030f5149b62704de729318a
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17405.797, -1183.657, 1.837, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17405.797,"col pos y":-1183.657,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')

-- c0cdc4c143867e8a4551fca90952dead1a24f10f
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17427.211, -1189.159, 0.642, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17427.211,"col pos y":-1189.159,"col orientation":0,"col size x":0.737,"col size y":0.728,"col size z":1.29,"context":null,"url":null}')
--############### SE_DECO 12333#################

