--############### SE_DECO 12333#################
-- GROUP: plants_center
-- ffe59e21486f6e06404b36ab2c99659cf2c68d20
SceneEditor:spawnShape("fy_s2_palmtree_a.shape", 17766.74, -871.701, 9.92, 0, 0, 0, '{"col size x":1.534,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.715,"col size z":27.074,"scale y":1,"col pos y":-871.701,"col pos x":17766.74,"context":null,"url":null}')

-- afc2c2fda81973e818309cf84ddc0207675c3edc
SceneEditor:spawnShape("tr_s2_palmtree_c.shape", 17779.379, -907.37, 9.731, 0, 0, 0, '{"col size x":1.213,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.893,"col size z":16.228,"scale y":1,"col pos y":-907.37,"col pos x":17779.379,"context":null,"url":null}')

-- 6cda8678cc79ae45951f1e4af7026edc256771c2
SceneEditor:spawnShape("tr_s2_palmtree_d.shape", 17796.369, -936.11, 9.684, 0, 0, 0, '{"col size x":1.41,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.185,"col size z":17.871,"scale y":1,"col pos y":-936.11,"col pos x":17796.369,"context":null,"url":null}')

-- 9e1407ff0512f017ba0e2b7119241bc642ca1601
SceneEditor:spawnShape("tr_s2_palmtree_a.shape", 17794.57, -877.989, 8.837, 0, 0, 0, '{"col size x":1.796,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.236,"col size z":6.288,"scale y":1,"col pos y":-877.989,"col pos x":17794.57,"context":null,"url":null}')


-- GROUP: center
-- 2c4989cef843ce8b239c787b4300bae74a16633f
SceneEditor:spawnShape("ge_partylamp_03.shape", 17811.043, -956.146, 10.306, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17811.043,"col pos y":-956.146,"col orientation":0,"col size x":1.195,"col size y":1.195,"col size z":5.267,"context":null,"url":null}')

-- 0f2b3cb99b669f01a71368e9a0b31667ac9fa20a
SceneEditor:spawnShape("ge_partylamp_03.shape", 17829.9, -925.946, 9.915, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17829.9,"col pos y":-925.946,"col orientation":0,"col size x":1.025,"col size y":1.025,"col size z":5.267,"context":null,"url":null}')

-- 0d38e25750d8b2ed1852add1b82baca8c0f021a3
SceneEditor:spawnShape("ge_partylamp_03.shape", 17827.418, -870.801, 8.602, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17827.418,"col pos y":-870.801,"col orientation":0,"col size x":2.105,"col size y":2.105,"col size z":5.267,"context":null,"url":null}')

-- e2da65f469805c5f9867c2164ba582d4a341daf4
SceneEditor:spawnShape("ge_partylamp_03.shape", 17817.684, -841.514, 9.194, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17817.684,"col pos y":-841.514,"col orientation":0,"col size x":1.975,"col size y":1.975,"col size z":5.267,"context":null,"url":null}')

-- aedc4ed97059849dd365f8437ee8150a85fdb2ad
SceneEditor:spawnShape("ge_partylamp_03.shape", 17818.762, -817.401, 10.224, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17818.762,"col pos y":-817.401,"col orientation":0,"col size x":1.785,"col size y":1.785,"col size z":5.267,"context":null,"url":null}')


-- GROUP: center_bar
-- 28a5c8b36e3d1ceb6109fd2a843dd1e7b4826994
SceneEditor:spawnShape("tr_s2_palmtree_d.shape", 17767.504, -813.393, 10.036, 0, 0, 0.74, '{"col pos x":17767.4,"col size x":1.47,"scale z":1,"col size y":1.245,"col orientation":0,"scale y":1,"col size z":17.871,"scale x":1,"col pos y":-813.174,"context":null,"url":null}')

-- 94fbe0016fc921db2b4c1a58c2b8889cae847673
SceneEditor:spawnShape("tr_s2_palmtree_b.shape", 17793, -834.602, 9.958, 0, 0, 1.95, '{"col pos x":17792.422,"col size x":3.633,"scale z":1,"col size y":0.904,"col orientation":0,"scale y":1,"col size z":12.402,"scale x":1,"col pos y":-835.236,"context":null,"url":null}')

-- 0ab491f12b506bdf1e439297df72ab6e2d906805
SceneEditor:spawnShape("tr_s2_palmtree_f.shape", 17794.053, -834.287, 9.942, 0, 0, 0, '{"col pos x":17794.26,"col size x":1.426,"scale z":1,"col size y":3.18,"col orientation":0,"scale y":1,"col size z":19.267,"scale x":1,"col pos y":-834.602,"context":null,"url":null}')

-- 464bbde15489ad09635f3f744ebf7b05b6009c5c
SceneEditor:spawnShape("tr_s2_palmtree_f.shape", 17768.664, -815.988, 10.037, 0, 0, 0, '{"col pos x":17768.514,"col size x":1.436,"scale z":1,"col size y":3.3,"col orientation":0,"scale y":1,"col size z":19.267,"scale x":1,"col pos y":-816.624,"context":null,"url":null}')

-- 8cfe10da01147b166f34826587f85beebf0d078e
SceneEditor:spawnShape("tr_s2_palmtree_a.shape", 17760.719, -804.648, 10.012, 0, 0, 5.04, '{"col pos x":17760.719,"col size x":0.926,"scale z":1,"col size y":1.586,"col orientation":0,"scale y":1,"col size z":6.288,"scale x":1,"col pos y":-804.648,"context":null,"url":null}')

-- 53d01cfb31211fba0651ebfbc023cbdb7d24e81d
SceneEditor:spawnShape("tr_s2_palmtree_a.shape", 17811.125, -846.793, 9.08, 0, 0, 3.2, '{"col pos x":17811.125,"col size x":0.696,"scale z":1,"col size y":1.356,"col orientation":0,"scale y":1,"col size z":6.288,"scale x":1,"col pos y":-846.793,"context":null,"url":null}')

-- 09610e4b8005bf639f907663e236a8424a9c5ac4
SceneEditor:spawnShape("tr_s2_palmtree_a.shape", 17783.945, -839.114, 9.818, 0, 0, 3.72, '{"col pos x":17791.557,"col size x":0.566,"scale z":1,"col size y":1.226,"col orientation":0,"scale y":1,"col size z":6.288,"scale x":1,"col pos y":-835.045,"context":null,"url":null}')

-- 494bb2a924a099b6009d1f9f6e25aedf320decf6
SceneEditor:spawnShape("coussin.shape", 17773.648, -818.908, 9.82, 0, 0, 0.06, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-818.908,"context":null,"url":null}')

-- e13f1707db08832fcd70b6907c6b7f7d2fe0b81e
SceneEditor:spawnShape("gen_bt_silo.shape", 17802.43, -811.481, 10.137, 0, 0, -1, '{"col pos x":17802.43,"col size x":3.151,"scale z":1,"col size y":4.533,"col orientation":-0.99,"scale y":1,"col size z":10.403,"scale x":1,"col pos y":-811.481,"context":null,"url":null}')

-- 9451b112b8e3417e265acd2b8786a007c9ce011a
SceneEditor:spawnShape("gen_bt_silo.shape", 17805.867, -818.393, 10.206, 0, 0, -1.55, '{"col pos x":17805.867,"col size x":2.981,"scale z":1,"col size y":4.363,"col orientation":-1.56,"scale y":1,"col size z":10.403,"scale x":1,"col pos y":-818.393,"context":null,"url":null}')

-- fe54dc61325a0615f609eeeb8f3be022b83ea799
SceneEditor:spawnShape("coussin.shape", 17793.162, -842.095, 9.277, 0, 0, 0.3, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-842.095,"context":null,"url":null}')

-- 391edf0e28e1786d8c50a2998e32c56423640090
SceneEditor:spawnShape("coussin.shape", 17794.592, -839.106, 9.439, 0, 0, 0.27, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-839.106,"context":null,"url":null}')

-- 521521adf0763f78dfbc787d3a0fe2c5351f3063
SceneEditor:spawnShape("coussin.shape", 17797.098, -841.843, 9.231, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-841.843,"context":null,"url":null}')

-- ed3bf40c4b0f575d5cbf243b3492039f9fa4fb59
SceneEditor:spawnShape("coussin.shape", 17810.059, -804.51, 10.107, 0, 0, 0.4, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-804.51,"context":null,"url":null}')

-- 316203bf45af3f1984c3139b59f6e6ca2fd90395
SceneEditor:spawnShape("coussin.shape", 17808.689, -807.541, 10.014, 0, 0, 0.33, '{"col pos x":-0.043,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":3.141,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-807.541,"context":null,"url":null}')

-- 258f0106db6cb75ff31bd2b56f54ce0d57b049c9
SceneEditor:spawnShape("coussin.shape", 17811.621, -808.8, 10.104, 0, 0, 0.25, '{"col pos x":-2.234,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-808.8,"context":null,"url":null}')

-- dfba3f8446af980ffeaf377489add3f53c3d5140
SceneEditor:spawnShape("coussin.shape", 17803.328, -806.119, 9.839, 0, 0, -0.47, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-806.119,"context":null,"url":null}')

-- 5cfe7bea276cfb16cbea73ffadf20f5b9f94c6a7
SceneEditor:spawnShape("coussin.shape", 17799.748, -802.736, 9.721, 0, 0, 0.31, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-802.736,"context":null,"url":null}')

-- 0246407d0cc2ffe46d3ae8c64a861f1e99e6cc30
SceneEditor:spawnShape("coussin.shape", 17798.184, -806.196, 9.818, 0, 0, 0.48, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-806.196,"context":null,"url":null}')

-- 416c30b0a8d5c283e46a1cebd17664b2b3687217
SceneEditor:spawnShape("coussin.shape", 17789.807, -801.975, 9.761, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-801.975,"context":null,"url":null}')

-- 660977ad0020b475e8669578880fdedcbd607d4d
SceneEditor:spawnShape("coussin.shape", 17774.361, -803.704, 9.762, 0, 0, 0.68, '{"col pos x":-8.713,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-803.704,"context":null,"url":null}')

-- 447f275de7eb0fe89952277416135cd921819261
SceneEditor:spawnShape("coussin.shape", 17775.148, -807.731, 9.847, 0, 0, -0.27, '{"col pos x":-8.527,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":3.141,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-807.731,"context":null,"url":null}')

-- 63b8a9dc7165919a58d64ccb483d421cce0070ef
SceneEditor:spawnShape("coussin.shape", 17777.797, -805.207, 9.756, 0, 0, 0.32, '{"col pos x":-9.734,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-805.207,"context":null,"url":null}')

-- 585bca98a64f147e60a5fdf3ca5c472dbda219c2
SceneEditor:spawnShape("coussin.shape", 17764.338, -805.607, 9.692, 0, 0, -0.7, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-805.607,"context":null,"url":null}')

-- 29ea8f493dbe9451ef64112f13e7a5fbe3203101
SceneEditor:spawnShape("coussin.shape", 17759.744, -808.018, 9.782, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-808.018,"context":null,"url":null}')

-- 2e655fdcc53e76dd1b1b7fa2c577a1618974bddc
SceneEditor:spawnShape("coussin.shape", 17763.033, -810.541, 9.712, 0, 0, 0.24, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-810.541,"context":null,"url":null}')

-- 77c178a395e135e06705679b63f301ca53105d41
SceneEditor:spawnShape("coussin.shape", 17761.828, -823.799, 9.707, 0, 0, 0.36, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-823.799,"context":null,"url":null}')

-- 08e6b69389d0362f88454d6c7d1ee74d95c2d534
SceneEditor:spawnShape("coussin.shape", 17763.959, -819.604, 9.744, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-819.604,"context":null,"url":null}')

-- 323b3b66760455c8f526ad2e9d1de71fb7eca156
SceneEditor:spawnShape("coussin.shape", 17765.994, -824.643, 9.701, 0, 0, 0.77, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-824.643,"context":null,"url":null}')

-- 65efa63a055c881dd5cb6b0a23de8bd4f84ae348
SceneEditor:spawnShape("coussin.shape", 17775.537, -832.164, 9.803, 0, 0, 0, '{"col pos x":-9.711,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-832.164,"context":null,"url":null}')

-- a1c13deceade0ecde9148c9d797238d7c0520451
SceneEditor:spawnShape("coussin.shape", 17772.436, -829.743, 9.811, 0, 0, 0.33, '{"col pos x":-11.875,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-829.743,"context":null,"url":null}')

-- 5f736f9e92a5da6bcf85b32c8f9f518c7eb30b7e
SceneEditor:spawnShape("coussin.shape", 17771.562, -833.776, 9.76, 0, 0, -0.82, '{"col pos x":-9.484,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-833.776,"context":null,"url":null}')

-- 6a5cb2d4e2b5af39bb123af717aa812b234ed7b6
SceneEditor:spawnShape("coussin.shape", 17771.531, -822.241, 9.764, 0, 0, 0.49, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":3.141,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-822.241,"context":null,"url":null}')

-- 2d87a06747a847554ca19efdde8d5ab0541d892f
SceneEditor:spawnShape("coussin.shape", 17776.031, -822.321, 9.755, 0, 0, -0.5, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-822.321,"context":null,"url":null}')

-- 20086d89248ac316be0ff636e6f288fadd32bbbe
SceneEditor:spawnShape("coussin.shape", 17768.475, -843.548, 9.586, 0, 0, 0.42, '{"col pos x":0.131,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-843.548,"context":null,"url":null}')

-- 7f060b79f3649a10809c48d8c02eacd43f0fde8c
SceneEditor:spawnShape("coussin.shape", 17771.369, -844.759, 9.557, 0, 0, 0.59, '{"col pos x":0.174,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-844.759,"context":null,"url":null}')

-- 24f0e583058782b953b06decd4f8b7f3d2220383
SceneEditor:spawnShape("coussin.shape", 17770.303, -841.051, 9.594, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":3.141,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-841.051,"context":null,"url":null}')

-- eb75c1966e1df30443e5fafdd9d90fae9f16f9d7
SceneEditor:spawnShape("coussin.shape", 17781.533, -838.793, 9.516, 0, 0, 0.37, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-838.793,"context":null,"url":null}')

-- 6dce3147aa2b5e003818237bde4abd65952ee264
SceneEditor:spawnShape("coussin.shape", 17784.809, -841.584, 9.439, 0, 0, -0.32, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":3.141,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-841.584,"context":null,"url":null}')

-- 31231d81a979502de482ecb559ff3aab981cb6d4
SceneEditor:spawnShape("coussin.shape", 17780.998, -843.394, 9.371, 0, 0, -0.39, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-843.394,"context":null,"url":null}')

-- ce9806847ba5a31dcafefb89e51b8c6de959cb30
SceneEditor:spawnShape("coussin.shape", 17783.309, -848.431, 9.154, 0, 0, 0.99, '{"col pos x":-0.641,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-848.431,"context":null,"url":null}')

-- bf8e3a9439464e6c0e94b21ea4c0035229064e17
SceneEditor:spawnShape("coussin.shape", 17780.441, -851.242, 9.243, 0, 0, 0.27, '{"col pos x":0.795,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-851.242,"context":null,"url":null}')

-- 5bb9e2a4acf8d9c778e3c563b03b7d8edd72507d
SceneEditor:spawnShape("coussin.shape", 17784.807, -851.81, 9.149, 0, 0, -0.38, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-851.81,"context":null,"url":null}')

-- 31f9c1baf275409f7067fe1d768df5f3aa3c74ad
SceneEditor:spawnShape("coussin.shape", 17793.719, -850.352, 8.929, 0, 0, -0.55, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-850.352,"context":null,"url":null}')

-- 6ed45b4f9ec8437fdda5086b06a8b2da648a3fbd
SceneEditor:spawnShape("coussin.shape", 17795.072, -854.847, 8.757, 0, 0, -0.39, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-854.847,"context":null,"url":null}')

-- 080968122b58adf748e1cfed71b6686443f55b46
SceneEditor:spawnShape("coussin.shape", 17799.404, -850.889, 8.904, 0, 0, 0.38, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-850.889,"context":null,"url":null}')

-- eb209db73cae4baf3adff749119f2241e1a2ec77
SceneEditor:spawnShape("coussin.shape", 17806.248, -848.241, 8.957, 0, 0, 0.33, '{"col pos x":2.039,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-848.241,"context":null,"url":null}')

-- ab0a025684136496121d70129833420d1543c5e1
SceneEditor:spawnShape("coussin.shape", 17811.016, -849.624, 8.822, 0, 0, 0.86, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-849.624,"context":null,"url":null}')

-- aa75e4bdf89e9197c0d53007a41a4977566589c5
SceneEditor:spawnShape("coussin.shape", 17808.512, -845.281, 8.84, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-845.281,"context":null,"url":null}')

-- 7d0d6eadf4034b23279cd528b26810a20a290250
SceneEditor:spawnShape("coussin.shape", 17809.109, -840.185, 9.238, 0, 0, 0.81, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-840.185,"context":null,"url":null}')

-- a82e8f5da428648d623699bfe5e498d2db196c4a
SceneEditor:spawnShape("coussin.shape", 17812.516, -837.601, 9.318, 0, 0, 0.33, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-837.601,"context":null,"url":null}')

-- 31c918fcf9dfa58898f022d84f793e5070226065
SceneEditor:spawnShape("coussin.shape", 17808.666, -836.323, 9.535, 0, 0, 0.57, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-836.323,"context":null,"url":null}')

-- 9ed8737ad811c2c4b32b56efaaad9da83171b310
SceneEditor:spawnShape("coussin.shape", 17799.547, -833.565, 9.609, 0, 0, -0.65, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-833.565,"context":null,"url":null}')

-- e0b2f9a847362a0b361684961c45e0b60b504c0c
SceneEditor:spawnShape("coussin.shape", 17803.418, -835.382, 9.538, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-835.382,"context":null,"url":null}')

-- b8d4993e809fed35fcd7e59eef2be2e1072c1f00
SceneEditor:spawnShape("coussin.shape", 17801.17, -837.538, 9.436, 0, 0, -0.34, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-837.538,"context":null,"url":null}')

-- 7c2f7154cc0c1bc88fcac1dfcf851478535a7f68
SceneEditor:spawnShape("tr_acc_table.shape", 17808.326, -847.547, 9.195, 0, 0, 0, '{"col pos x":17808.326,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-847.547,"context":null,"url":null}')

-- 1202edc83edb51a39602d1259e9401ba3b31152b
SceneEditor:spawnShape("tr_acc_table.shape", 17796.549, -852.151, 9.116, 0, 0, 0, '{"col pos x":17796.549,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-852.151,"context":null,"url":null}')

-- e82aecd4b2ceb921996d2ea969684bb0bd45ac54
SceneEditor:spawnShape("tr_acc_table.shape", 17782.221, -850.36, 9.479, 0, 0, 0, '{"col pos x":17782.221,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-850.36,"context":null,"url":null}')

-- 8978f1f85a5862022cea660874e3359153be8118
SceneEditor:spawnShape("tr_acc_table.shape", 17770.051, -842.893, 9.9, 0, 0, 0, '{"col pos x":17770.051,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-842.893,"context":null,"url":null}')

-- 3d339667e91f61a4fbe6c2820150cece0f0ebb7e
SceneEditor:spawnShape("tr_acc_table.shape", 17763.951, -822.925, 10.03, 0, 0, 0, '{"col pos x":17763.951,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-822.925,"context":null,"url":null}')

-- e3b5ea02cd0b71200b67e4b08031cf04798c47da
SceneEditor:spawnShape("tr_acc_table.shape", 17762.5, -807.996, 10.034, 0, 0, 0, '{"col pos x":17762.5,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-807.996,"context":null,"url":null}')

-- 73b17590c5b1c6795ea7918fe1dd13a1ea398b01
SceneEditor:spawnShape("tr_acc_table.shape", 17810.842, -806.811, 10.35, 0, 0, 0, '{"col pos x":17810.842,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":3.141,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-806.811,"context":null,"url":null}')

-- a7044fda322a86f8bc450067a1463d01d1bab5d4
SceneEditor:spawnShape("tr_acc_table.shape", 17800.506, -804.887, 10.148, 0, 0, 0, '{"col pos x":17800.506,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":3.141,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-804.887,"context":null,"url":null}')

-- 6d28ccdf66fcde69e669cd3cad5523809181d104
SceneEditor:spawnShape("tr_acc_table.shape", 17775.438, -805.984, 10.051, 0, 0, 0, '{"col pos x":17775.438,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-805.984,"context":null,"url":null}')

-- c1ccac468524bda2b0adbf570626248a241e0a9b
SceneEditor:spawnShape("tr_acc_table.shape", 17773.744, -820.969, 10.046, 0, 0, 0, '{"col pos x":17773.744,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-820.969,"context":null,"url":null}')

-- 738feced6b1076b1c4c03b548b9e25f36ced1181
SceneEditor:spawnShape("tr_acc_table.shape", 17773.533, -832.203, 10.031, 0, 0, 0, '{"col pos x":17773.533,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":3.141,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-832.203,"context":null,"url":null}')

-- 66475c44fd88683d0c1bfac35a21d445ba9ab8ac
SceneEditor:spawnShape("tr_acc_table.shape", 17782.404, -840.938, 9.753, 0, 0, 0, '{"col pos x":17782.404,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-840.938,"context":null,"url":null}')

-- ea362dbabe7868ad48ccf9428c1e9a7d07cd01ca
SceneEditor:spawnShape("tr_acc_table.shape", 17794.828, -840.925, 9.59, 0, 0, 0, '{"col pos x":17794.828,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-840.925,"context":null,"url":null}')

-- cfc87bc08295143c42baccc969ad18323fcbcf04
SceneEditor:spawnShape("tr_acc_table.shape", 17801.705, -835.537, 9.851, 0, 0, 0, '{"col pos x":17801.705,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-835.537,"context":null,"url":null}')

-- a90beb44b6bd2809b37460cc6f6b3ddf44189f78
SceneEditor:spawnShape("tr_acc_table.shape", 17810.684, -838.224, 9.553, 0, 0, 0, '{"col pos x":17810.684,"col size x":1.507,"scale z":1,"col size y":1.433,"col orientation":0,"scale y":1,"col size z":0.959,"scale x":1,"col pos y":-838.224,"context":null,"url":null}')

-- 89b6af319e1f8ae1bb1ed1abe2889f342961461e
SceneEditor:spawnShape("tr_bar_02_village_a.shape", 17788.893, -819.514, 8.786, 0, 0, 1.91, '{"col pos x":17783.443,"col size x":8.997,"scale z":1,"col size y":4.683,"col orientation":1.95,"scale y":1,"col size z":21.473,"scale x":1,"col pos y":-821.307,"context":null,"url":null}')

-- f9887c90aa8416288f56cab7161818b995bd9689
SceneEditor:spawnShape("gen_bt_silo.shape", 17802.764, -826.951, 10.042, 0, 0, -2.3, '{"col pos x":17802.764,"col size x":2.631,"scale z":1,"col size y":4.013,"col orientation":0.81,"scale y":1,"col size z":10.403,"scale x":1,"col pos y":-826.951,"context":null,"url":null}')


-- GROUP: center_arena
-- f51a6ee59f7fd3ef5dab93656128f00daf5f881f
SceneEditor:spawnShape("ge_mission_barriere.shape", 17785.76, -951.64, 10.092, 0, 0, 1.78, '{"col size x":3.424,"scale x":1,"col orientation":1.78,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-951.64,"col pos x":17785.76,"context":null,"url":null}')

-- 91c23e0a608c8631edc06c3c74e4c7525ad7b9c0
SceneEditor:spawnShape("ge_feudecamp.ps", 17767.336, -943.755, 10.031, 0, 0, 0, '{"col size x":1.071,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.188,"col size z":5.716,"scale y":1,"col pos y":-943.755,"col pos x":17767.336,"context":null,"url":null}')

-- 4c9c9a4d9c9ffce5faecb5f04c5f24fa9b6ec045
SceneEditor:spawnShape("ge_feudecamp.ps", 17764.154, -957.918, 10.005, 0, 0, 0, '{"col size x":1.161,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.278,"col size z":5.716,"scale y":1,"col pos y":-957.918,"col pos x":17764.154,"context":null,"url":null}')

-- 71f529719ec6ee08d7f36363ad842c64881adea9
SceneEditor:spawnShape("ju_fo_brumes.ps", 17767.365, -956.102, 10.015, 0, 0, 0, '{"col size x":30.111,"scale x":1,"col orientation":0,"scale z":1,"col size y":18.025,"col size z":11.766,"scale y":1,"col pos y":-956.102,"col pos x":0,"context":null,"url":null}')

-- 49c9f2d30ca2801aba37aed7b403f17e8042c2a7
SceneEditor:spawnShape("ge_mission_charogneinsect.shape", 17775.188, -956.657, 10.047, 0, 0, 0, '{"col size x":6.304,"scale x":1,"col orientation":0,"scale z":1,"col size y":3.571,"col size z":2.189,"scale y":1,"col pos y":-956.657,"col pos x":0,"context":null,"url":null}')

-- 4a29594fe2e700c41bd55d3e1229693aa5839814
SceneEditor:spawnShape("ge_mission_barriere.shape", 17786.26, -954.74, 10.092, 0, 0, 1.67, '{"col size x":3.424,"scale x":1,"col orientation":1.67,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-954.74,"col pos x":17786.26,"context":null,"url":null}')

-- 33e342c67b0bc5a448f78070c199d5fc13ff798e
SceneEditor:spawnShape("ge_mission_barriere.shape", 17786.42, -957.88, 10.092, 0, 0, 7.85, '{"col size x":3.424,"scale x":1,"col orientation":7.85,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-957.88,"col pos x":17786.42,"context":null,"url":null}')

-- f1a9cee08e6dd20b924b7833ec7052a0acb9f653
SceneEditor:spawnShape("ge_mission_barriere.shape", 17759.561, -928.04, 10.092, 0, 0, 3.04, '{"col size x":3.424,"scale x":1,"col orientation":3.04,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-928.04,"col pos x":17759.561,"context":null,"url":null}')

-- 42ce8ac3afb6bebf8c66ea35c4f9ca5272343242
SceneEditor:spawnShape("ge_mission_barriere.shape", 17762.66, -928.54, 10.092, 0, 0, 2.93, '{"col size x":3.424,"scale x":1,"col orientation":2.93,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-928.54,"col pos x":17762.66,"context":null,"url":null}')

-- b5bd7eae3be36b79855635a8691999271ffd9aad
SceneEditor:spawnShape("ge_mission_barriere.shape", 17765.689, -929.35, 10.092, 0, 0, 2.83, '{"col size x":3.424,"scale x":1,"col orientation":2.83,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-929.35,"col pos x":17765.689,"context":null,"url":null}')

-- a7c9af1098388a937eb122359ce211706c058965
SceneEditor:spawnShape("ge_mission_barriere.shape", 17768.619, -930.47, 10.092, 0, 0, 2.72, '{"col size x":3.424,"scale x":1,"col orientation":2.72,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-930.47,"col pos x":17768.619,"context":null,"url":null}')

-- decd0fbf069b95f4d5f868f895db577266cf482a
SceneEditor:spawnShape("ge_mission_barriere.shape", 17771.42, -931.9, 10.092, 0, 0, 2.62, '{"col size x":3.424,"scale x":1,"col orientation":2.62,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-931.9,"col pos x":17771.42,"context":null,"url":null}')

-- ae52986af02c6c052e00286acb735a2ac466d4b2
SceneEditor:spawnShape("ge_mission_barriere.shape", 17774.051, -933.61, 10.092, 0, 0, 2.51, '{"col size x":3.424,"scale x":1,"col orientation":2.51,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-933.61,"col pos x":17774.051,"context":null,"url":null}')

-- e7a8df6c81aa3d662fec68bf734ff00ff70e7d8b
SceneEditor:spawnShape("ge_mission_barriere.shape", 17776.49, -935.59, 10.092, 0, 0, 2.41, '{"col size x":3.424,"scale x":1,"col orientation":2.41,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-935.59,"col pos x":17776.49,"context":null,"url":null}')

-- 7e3cd2c5aaa6db421fd9f4aeb153a7480cfd644b
SceneEditor:spawnShape("ge_mission_barriere.shape", 17778.711, -937.81, 10.092, 0, 0, 2.3, '{"col size x":3.424,"scale x":1,"col orientation":2.3,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-937.81,"col pos x":17778.711,"context":null,"url":null}')

-- 22ae633386b267cc3f689f6c43cd6f36f1c9f05f
SceneEditor:spawnShape("ge_mission_barriere.shape", 17780.689, -940.25, 10.092, 0, 0, 2.2, '{"col size x":3.424,"scale x":1,"col orientation":2.2,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-940.25,"col pos x":17780.689,"context":null,"url":null}')

-- 77c94b9fa520700481055ecab2e0152b345458a2
SceneEditor:spawnShape("ge_mission_barriere.shape", 17782.4, -942.88, 10.092, 0, 0, 2.09, '{"col size x":3.424,"scale x":1,"col orientation":2.09,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-942.88,"col pos x":17782.4,"context":null,"url":null}')

-- 0d621dd2d08a9877e6f5fbd2e94e15d63eff3911
SceneEditor:spawnShape("ge_mission_barriere.shape", 17783.83, -945.68, 10.092, 0, 0, 1.99, '{"col size x":3.424,"scale x":1,"col orientation":1.99,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-945.68,"col pos x":17783.83,"context":null,"url":null}')

-- 5f65a25d2e0362f9d3b4f88fd473ffcaf75dccc6
SceneEditor:spawnShape("ge_mission_barriere.shape", 17784.949, -948.61, 10.092, 0, 0, 1.88, '{"col size x":3.424,"scale x":1,"col orientation":1.88,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-948.61,"col pos x":17784.949,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- 1f546c6aa2c06c18af194fe125b190deea44a6b2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17850.18, -957.5, 6.88, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17850.18,"col pos y":-957.5,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c308ed046be71af09049efa855654e065746b0e9
SceneEditor:spawnShape("GE_Mission_borne.shape", 17855.33, -948.93, 5.91, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17855.33,"col pos y":-948.93,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4930c9a14de42e02a500d1ae53f8d0b6f1d2a0ac
SceneEditor:spawnShape("GE_Mission_borne.shape", 17860.5, -940.35, 4.56, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17860.5,"col pos y":-940.35,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ff783bcd9ebc18800642d539cef2bffcb87063b6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17865.65, -931.79, 3.12, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17865.65,"col pos y":-931.79,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 52c77b70fd7dc2a18c0473d9d7c688bd517b62a8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17870.8, -923.23, 2.38, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17870.8,"col pos y":-923.23,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 828b6ffd832a67fd8da6f68ec5a855caf8f80005
SceneEditor:spawnShape("GE_Mission_borne.shape", 17875.97, -914.66, 2.39, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17875.97,"col pos y":-914.66,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f55a5c3489a911b33a765814f969647403a44ccf
SceneEditor:spawnShape("GE_Mission_borne.shape", 17881.1, -906.08, 3.13, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17881.1,"col pos y":-906.08,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e40f15689260ea8a578048c7147705e042cc92d3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17886.27, -897.52, 3.41, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17886.27,"col pos y":-897.52,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 8b8f79dbfb52eb340ed5e05aae116ea9ad317d5b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17891.42, -888.96, 2.73, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17891.42,"col pos y":-888.96,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d2bb80bf3a66d23317edcbf7cc538e4455934f1f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17885.76, -880.72, 2.89, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17885.76,"col pos y":-880.72,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 05a3c39a3cfd9ab20b05b4600163e65bece8cee8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17880.11, -872.46, 2.75, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17880.11,"col pos y":-872.46,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e574476e66d3c786d55e94a3bbc0de960c6b7d64
SceneEditor:spawnShape("GE_Mission_borne.shape", 17874.46, -864.21, 2.48, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17874.46,"col pos y":-864.21,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4138178c6b1c7106f9b6af001fe652c11f7c9512
SceneEditor:spawnShape("GE_Mission_borne.shape", 17868.82, -855.95, 2.04, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17868.82,"col pos y":-855.95,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 0f4c975c28636126dba05213764897bd6cca24b9
SceneEditor:spawnShape("GE_Mission_borne.shape", 17863.17, -847.71, 2.03, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17863.17,"col pos y":-847.71,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b0a2a3a28f6e90f39c7d415b6c6ad5cfa8d97809
SceneEditor:spawnShape("GE_Mission_borne.shape", 17857.52, -839.46, 2.78, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17857.52,"col pos y":-839.46,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3d339a6ba18434f1a3d7585b354a8a81b0b2ad86
SceneEditor:spawnShape("GE_Mission_borne.shape", 17851.87, -831.2, 3.7, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17851.87,"col pos y":-831.2,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 1aa6f193a02c9269130dc11b69713ad814d4d04a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17846.24, -822.94, 5.37, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17846.24,"col pos y":-822.94,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3ae23811c0d4bccc168bc2de5c79befdf705efaa
SceneEditor:spawnShape("GE_Mission_borne.shape", 17840.58, -814.7, 7.35, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17840.58,"col pos y":-814.7,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 7eff7130513f97bc94f477a3037d8a0cd9149c15
SceneEditor:spawnShape("GE_Mission_borne.shape", 17834.93, -806.45, 8.69, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17834.93,"col pos y":-806.45,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 9756a5eb2a86d36a2e04b07a8b9aa85225c1506b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17812.38, -801.69, 10.38, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17812.38,"col pos y":-801.69,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3af5834083b153e2d478f7790386470700c12c9b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17815.57, -811.17, 10.31, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17815.57,"col pos y":-811.17,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- bdff8b083e122662729a2d5689892c1a4fe85b75
SceneEditor:spawnShape("GE_Mission_borne.shape", 17818.77, -820.64, 9.91, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17818.77,"col pos y":-820.64,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4f31442951d9a8358125e502b52b098a45361b1c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17821.97, -830.11, 9.39, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17821.97,"col pos y":-830.11,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 452692da9f8c66338455f2c41c6bc84f98258960
SceneEditor:spawnShape("GE_Mission_borne.shape", 17825.17, -839.6, 8.91, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17825.17,"col pos y":-839.6,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 44a04f5ce9984347bb7a47f289a943eebfff14c3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17828.37, -849.07, 8.46, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17828.37,"col pos y":-849.07,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6345cd9ca86256db74b3d0c836af4e6cae4ff8a8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17831.57, -858.54, 8.16, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17831.57,"col pos y":-858.54,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- da86f9f16cf49aba13f3414a3e785f520bb9ab57
SceneEditor:spawnShape("GE_Mission_borne.shape", 17834.77, -868.02, 7.97, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17834.77,"col pos y":-868.02,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2f767784f4b5652f54e32d4f2143e7dc9f24bd9d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17837.97, -877.49, 7.79, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17837.97,"col pos y":-877.49,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 53ae916a9fcc641aec4b1e652d0656ce38240046
SceneEditor:spawnShape("GE_Mission_borne.shape", 17841.17, -886.96, 8.11, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17841.17,"col pos y":-886.96,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f3d2eac41789675c7f65c7d18938199b279d4432
SceneEditor:spawnShape("GE_Mission_borne.shape", 17839.9, -896.88, 8.67, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17839.9,"col pos y":-896.88,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c81a2fc44befd060bf3845c15fe01ed26fe1098e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17838.67, -906.8, 8.35, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17838.67,"col pos y":-906.8,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d0d38decc85a5d38a99c530b62681f42acc88913
SceneEditor:spawnShape("GE_Mission_borne.shape", 17837.42, -916.74, 9.4, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17837.42,"col pos y":-916.74,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- aad1f0043d17d6da2e542892a87b2a33e0398d09
SceneEditor:spawnShape("GE_Mission_borne.shape", 17836.18, -926.66, 9.92, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17836.18,"col pos y":-926.66,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 05d09b3bb83e95ddd6e1c89f328c1274d7a7b494
SceneEditor:spawnShape("GE_Mission_borne.shape", 17834.93, -936.58, 9.49, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17834.93,"col pos y":-936.58,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 540d07dda9f022fc8e1c447553652d6cde69107d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17833.68, -946.5, 9.07, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17833.68,"col pos y":-946.5,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 88c9104bb2f6779fe68788ae4815737207a3c9d5
SceneEditor:spawnShape("GE_Mission_borne.shape", 17825.79, -952.29, 9.86, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17825.79,"col pos y":-952.29,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 72797ec996d64cc4ed9c4434ccbd86d64abf0203
SceneEditor:spawnShape("GE_Mission_borne.shape", 17817.86, -958.38, 10.25, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17817.86,"col pos y":-958.38,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################
