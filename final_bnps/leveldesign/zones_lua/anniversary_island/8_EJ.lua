--############### SE_DECO 12333#################
-- GROUP: buildings_zorai
-- 6905e9847e4c56594def3ae03b9a6bb513175d7d
SceneEditor:spawnShape("zo_module_commerce_zo_comm_2_village_a.shape", 18100.463, -1165.05, -0.222, 0, 0, -2.13, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18100.463,"col pos y":-1165.05,"col orientation":-2.1,"col size x":7.123,"col size y":9.497,"col size z":6.979,"context":null,"url":null}')

-- ffd9b0c88bb3439a59d1404bdd0be3ff37b33781
SceneEditor:spawnShape("zo_imm_boule_nb_01.shape", 18125.299, -1134.4, 11.8, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":7.232,"col pos y":-1134.4,"col orientation":0,"col size x":8,"col size y":8,"col size z":8,"context":null,"url":null}')

-- fe295f979f02f28f1670608de424d910cfc08737
SceneEditor:spawnShape("zo_imm_arbre_zo_imm_1_village_c.shape", 18125, -1134, 15, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":6.68,"col pos y":-1134,"col orientation":0,"col size x":12.467,"col size y":14.416,"col size z":17.81,"context":null,"url":null}')

-- 8524e89d47903422463a35a03cdde437dc1a8a9f
SceneEditor:spawnShape("zo_exterieur_immeublezo_imm_village_nb_04.shape", 18125, -1134, 0.147, 0, 0, 0.19, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18125,"col pos y":-1134,"col orientation":0,"col size x":45.461,"col size y":44.004,"col size z":47.918,"context":null,"url":null}')

-- f6a46c35e2b1642dab27849a0b26bd63446b4011
SceneEditor:spawnShape("zo_acc_ascenseur_ext1_imm_2_village_d.shape", 18119.43, -1135.813, 7.4, 0, 0, -1.93, '{"scale x":1.22,"scale y":1,"scale z":0.971,"col pos x":0,"col pos y":-1135.813,"col orientation":0,"col size x":4,"col size y":5.437,"col size z":8.237,"context":null,"url":null}')

-- 4ea0bcdfe4be85863046f26c0c6ab283cb70c6a0
SceneEditor:spawnShape("zo_acc_ascenseur_ext1_imm_2_village_d.shape", 18126.314, -1128.317, 7.4, 0, 0, 2.28, '{"scale x":1.22,"scale y":1,"scale z":0.971,"col pos x":-1.93,"col pos y":-1128.317,"col orientation":0,"col size x":4,"col size y":5.437,"col size z":8.237,"context":null,"url":null}')

-- d3129e7b1f2112537be0b3deea0d85f3eab8351f
SceneEditor:spawnShape("zo_acc_ascenseur_ext1_imm_2_village_d.shape", 18129.299, -1138.019, 7.4, 0, 0, 0.18, '{"scale x":1.22,"scale y":1,"scale z":0.971,"col pos x":2.191,"col pos y":-1138.019,"col orientation":0,"col size x":4,"col size y":5.437,"col size z":8.237,"context":null,"url":null}')
--############### SE_DECO 12333#################

