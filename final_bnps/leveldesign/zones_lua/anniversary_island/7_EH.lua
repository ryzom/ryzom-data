--############### SE_DECO 12333#################
-- GROUP: plants_zorai
-- 8cef439e05d050b56273c84414796674d9e65bb5
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17893.779, -1093.558, 0.391, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17893.779,"col pos y":-1093.558,"col orientation":0,"col size x":0.711,"col size y":0.702,"col size z":16.447,"context":null,"url":null}')

-- 4b004bbae51d4d84f26813c47ae9b3fbd04f77bb
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17864.379, -1076.472, -3.039, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17864.379,"col pos y":-1076.472,"col orientation":0,"col size x":0.571,"col size y":0.532,"col size z":16.447,"context":null,"url":null}')

-- 18d02f862e7508f2430b0c5004701e0be0afdd0b
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17912.416, -1106.726, 0.102, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17912.416,"col pos y":-1106.726,"col orientation":0,"col size x":0.601,"col size y":0.592,"col size z":16.447,"context":null,"url":null}')

-- 9c91841dc30dea2bef295e5f73cc002429b81747
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17900.523, -1054.296, -2.787, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17900.523,"col pos y":-1054.296,"col orientation":0,"col size x":0.801,"col size y":1.052,"col size z":16.447,"context":null,"url":null}')

-- 2c8dd3e25ec2f9f8db445622e650e19064806c01
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17883.605, -1018.39, -2.076, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17883.605,"col pos y":-1018.39,"col orientation":0,"col size x":1.031,"col size y":0.802,"col size z":16.447,"context":null,"url":null}')

-- a1f7911c41b93fa7f336fc02b9164051f0888dac
SceneEditor:spawnShape("ju_s2_young_tree.shape", 17911.914, -973.205, -0.195, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17911.914,"col pos y":-973.205,"col orientation":0,"col size x":1.001,"col size y":1.362,"col size z":16.447,"context":null,"url":null}')

-- f1fac62d3e9186e2de2f63e079717a63e24c34d0
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17899.984, -1077.811, -0.993, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17899.984,"col pos y":-1077.811,"col orientation":3.141,"col size x":4.17,"col size y":4.235,"col size z":25.035,"context":null,"url":null}')

-- 4b7d2702874674b2fb7f0f46323b7c3de74d7919
SceneEditor:spawnShape("ju_s2_big_tree.shape", 17887.842, -1119.323, 0.013, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17887.842,"col pos y":-1119.323,"col orientation":0,"col size x":4.58,"col size y":5.445,"col size z":25.035,"context":null,"url":null}')


-- GROUP: plants_center
-- e7b00f939849d988bfdf4f7f2aa68a399d2da81e
SceneEditor:spawnShape("fy_s2_palmtree_a.shape", 17793.684, -965.566, 9.974, 0, 0, 0, '{"col size x":1.284,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.195,"col size z":27.074,"scale y":1,"col pos y":-965.566,"col pos x":17793.684,"context":null,"url":null}')


-- GROUP: center
-- 41af86af8aab8802706fa8170acfa05d452e438b
SceneEditor:spawnShape("ge_partylamp_03.shape", 17771.152, -1031.924, 9.332, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17771.152,"col pos y":-1031.924,"col orientation":0,"col size x":1.625,"col size y":1.625,"col size z":5.267,"context":null,"url":null}')

-- f3b4bbd82bb7c43f2b01586bd657fd5c1c3a8844
SceneEditor:spawnShape("ge_partylamp_03.shape", 17793.826, -982.653, 9.889, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17793.826,"col pos y":-982.653,"col orientation":0,"col size x":1.585,"col size y":1.585,"col size z":5.267,"context":null,"url":null}')

-- e0556b890ac89252c6f4ab27d4e341f80adae9f6
SceneEditor:spawnShape("ge_partylamp_03.shape", 17787.238, -1011.636, 8.47, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17787.238,"col pos y":-1011.636,"col orientation":0,"col size x":2.075,"col size y":2.075,"col size z":5.267,"context":null,"url":null}')


-- GROUP: center_arena
-- bdd3260e20258d59bc1adf86531b3ccea702175e
SceneEditor:spawnShape("ge_feudecamp.ps", 17764.438, -973.233, 10.057, 0, 0, 0, '{"col size x":1.221,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.338,"col size z":5.716,"scale y":1,"col pos y":-973.233,"col pos x":17764.438,"context":null,"url":null}')

-- 21b6daf8163da3977c26fe484bf269035cad5446
SceneEditor:spawnShape("mp_bones.ps", 17760.994, -973.326, 10.054, 0, 0, 0, '{"col size x":1.111,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.114,"col size z":1.016,"scale y":1,"col pos y":-973.326,"col pos x":17760.994,"context":null,"url":null}')

-- ead48649eac30f1a125e474bbce3380adc51d1c2
SceneEditor:spawnShape("ge_mission_barriere.shape", 17786.26, -961.02, 10.092, 0, 0, 7.75, '{"col size x":3.424,"scale x":1,"col orientation":7.75,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-961.02,"col pos x":17786.26,"context":null,"url":null}')

-- 0c430b4780becfb145c1bcba371d859a8815f215
SceneEditor:spawnShape("ge_mission_barriere.shape", 17785.76, -964.12, 10.092, 0, 0, 7.64, '{"col size x":3.424,"scale x":1,"col orientation":7.64,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-964.12,"col pos x":17785.76,"context":null,"url":null}')

-- 6ed68e820811813867dd4073a788fbb4e3dd76be
SceneEditor:spawnShape("ge_mission_barriere.shape", 17784.949, -967.15, 10.092, 0, 0, 7.54, '{"col size x":3.424,"scale x":1,"col orientation":7.54,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-967.15,"col pos x":17784.949,"context":null,"url":null}')

-- 433fda11cc53f8fe9988790d57e215acc6bc9582
SceneEditor:spawnShape("ge_mission_barriere.shape", 17783.83, -970.08, 10.092, 0, 0, 7.43, '{"col size x":3.424,"scale x":1,"col orientation":7.43,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-970.08,"col pos x":17783.83,"context":null,"url":null}')

-- 668fd1fd4d71c1065458c796d0b653e28396e7f2
SceneEditor:spawnShape("ge_mission_barriere.shape", 17782.4, -972.88, 10.092, 0, 0, 7.33, '{"col size x":3.424,"scale x":1,"col orientation":7.33,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-972.88,"col pos x":17782.4,"context":null,"url":null}')

-- b6cc8d4cced37886cc84c8891a7092836f426bdb
SceneEditor:spawnShape("ge_mission_barriere.shape", 17780.689, -975.51, 10.092, 0, 0, 7.22, '{"col size x":3.424,"scale x":1,"col orientation":7.22,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-975.51,"col pos x":17780.689,"context":null,"url":null}')

-- bda76fa0417d472add6d855ff8cb77da40073082
SceneEditor:spawnShape("ge_mission_barriere.shape", 17778.711, -977.95, 10.092, 0, 0, 7.12, '{"col size x":3.424,"scale x":1,"col orientation":7.12,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-977.95,"col pos x":17778.711,"context":null,"url":null}')

-- ae287fc0ef3442212ab57d0939ba5ac69bce3279
SceneEditor:spawnShape("ge_mission_barriere.shape", 17776.49, -980.17, 10.092, 0, 0, 7.02, '{"col size x":3.424,"scale x":1,"col orientation":7.02,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-980.17,"col pos x":17776.49,"context":null,"url":null}')

-- 35c09f93c29edd314560155a0576650b0b845e5e
SceneEditor:spawnShape("ge_mission_barriere.shape", 17774.051, -982.15, 10.092, 0, 0, 6.91, '{"col size x":3.424,"scale x":1,"col orientation":6.91,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-982.15,"col pos x":17774.051,"context":null,"url":null}')

-- f249e37001746c46187208fd86630ba566bf0815
SceneEditor:spawnShape("ge_mission_barriere.shape", 17771.42, -983.86, 10.092, 0, 0, 6.81, '{"col size x":3.424,"scale x":1,"col orientation":6.81,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-983.86,"col pos x":17771.42,"context":null,"url":null}')

-- 194487162a7306f56b9096d24f61ce8402a6cdf9
SceneEditor:spawnShape("ge_mission_barriere.shape", 17768.619, -985.29, 10.092, 0, 0, 6.7, '{"col size x":3.424,"scale x":1,"col orientation":6.7,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-985.29,"col pos x":17768.619,"context":null,"url":null}')

-- a25ec0b9a7e22fb4f76adc9942c02da9ddf53fde
SceneEditor:spawnShape("ge_mission_barriere.shape", 17765.689, -986.41, 10.092, 0, 0, 6.6, '{"col size x":3.424,"scale x":1,"col orientation":6.6,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-986.41,"col pos x":17765.689,"context":null,"url":null}')

-- 2e6ae6facc09194a9734ff246eaa9a7ac9de96b2
SceneEditor:spawnShape("ge_mission_barriere.shape", 17762.66, -987.22, 10.092, 0, 0, 6.49, '{"col size x":3.424,"scale x":1,"col orientation":6.49,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-987.22,"col pos x":17762.66,"context":null,"url":null}')

-- 8ec8854326772ca30f223f7d5e52b472faa45837
SceneEditor:spawnShape("ge_mission_barriere.shape", 17759.561, -987.72, 10.092, 0, 0, 6.39, '{"col size x":3.424,"scale x":1,"col orientation":6.39,"scale z":1,"col size y":0.188,"col size z":2.303,"scale y":1,"col pos y":-987.72,"col pos x":17759.561,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- 3aea4d87a9e388c895ffed0f6ab3f86788c4f960
SceneEditor:spawnShape("GE_Mission_borne.shape", 17804.41, -1040.8, 3.546, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17804.41,"col pos y":-1040.8,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 22e611f431ea98d4e6e54929888f25a4ac6477f2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17798.98, -1041.34, 5.174, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17798.98,"col pos y":-1041.34,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 34db1101aa0ef033f677fba52b52bbb103021168
SceneEditor:spawnShape("GE_Mission_borne.shape", 17795.07, -1037.43, 6.653, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17795.07,"col pos y":-1037.43,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6803003ca896309f7a964e5d61dfc7dd855cc1a7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17790.38, -1034.09, 7.537, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17790.38,"col pos y":-1034.09,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6671aa2a0eabce376ef2160ec7d2ca6e26ae63e6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17784.99, -1031.29, 8.802, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17784.99,"col pos y":-1031.29,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a151e4d31f5c4e713489bab3fd40da120a4b169c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17779.94, -1028.05, 8.983, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17779.94,"col pos y":-1028.05,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b22ebf7579bd080c51ba9e81b97b3c8c60bcd810
SceneEditor:spawnShape("GE_Mission_borne.shape", 17774.94, -1024.14, 9.325, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17774.94,"col pos y":-1024.14,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 106eae2e07250550ad56ed8e2de364cb86f216a1
SceneEditor:spawnShape("GE_Mission_borne.shape", 17770.25, -1019.73, 9.778, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17770.25,"col pos y":-1019.73,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 91337d7050fa816f490a9863fc9caa51c747d30d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17765.26, -1016.33, 10.259, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17765.26,"col pos y":-1016.33,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 223f05232717065674fc4ac9ade94b3a2cae8d33
SceneEditor:spawnShape("GE_Mission_borne.shape", 17759.56, -1012.98, 10.295, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17759.56,"col pos y":-1012.98,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2fec97c8eaa536b92a0c5c6f760db98a72ba1747
SceneEditor:spawnShape("GE_Mission_borne.shape", 17762.26, -1035.36, 8.968, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17762.26,"col pos y":-1035.36,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3d08def9d326e1655d8d7b14e2b9bf7e6d40b9b0
SceneEditor:spawnShape("GE_Mission_borne.shape", 17767.53, -1038.19, 8.781, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17767.53,"col pos y":-1038.19,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- dab8b74b7ea5a50b14f4c909511c498d820166a3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17773.19, -1041.21, 8.637, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17773.19,"col pos y":-1041.21,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 78eb74b23e83b4aa859e86d61a12af8a520a0859
SceneEditor:spawnShape("GE_Mission_borne.shape", 17778.93, -1042.69, 8.088, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17778.93,"col pos y":-1042.69,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- facd55d8aa4623c1f4ef2a092def48d86fd2abff
SceneEditor:spawnShape("GE_Mission_borne.shape", 17784.66, -1044.71, 6.535, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17784.66,"col pos y":-1044.71,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 08efb603050c8b86446e4e4936892db8ac9190f4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17790.05, -1048.04, 5.396, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17790.05,"col pos y":-1048.04,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6fb57a9b3b9f25d2ec8003588b4cc6be5b666ac3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17795.49, -1051.68, 4.362, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17795.49,"col pos y":-1051.68,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3fcdd8fb73b9f30c2cc342698b7571b6df337512
SceneEditor:spawnShape("GE_Mission_borne.shape", 17800.52, -1054.83, 2.93, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17800.52,"col pos y":-1054.83,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 9c307c1690aff01ddc05eefd8af35a5304389ffb
SceneEditor:spawnShape("GE_Mission_borne.shape", 17806.49, -1057.25, 1.432, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17806.49,"col pos y":-1057.25,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a9d8a5bc56fb9f67f64bea90c078f0595b55c384
SceneEditor:spawnShape("GE_Mission_borne.shape", 17811.98, -1058.68, -0.316, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17811.98,"col pos y":-1058.68,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 26f5cfc02a785e81ae7ec751a4d80da50acf1439
SceneEditor:spawnShape("GE_Mission_borne.shape", 17818.47, -1060.06, -2.161, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17818.47,"col pos y":-1060.06,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4514c411bec7d326a633cfcbc3eb528531f750a7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17823.58, -1060.67, -2.75, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17823.58,"col pos y":-1060.67,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a1d2171c48f8ffa01af383beeaec5763aa3f5cec
SceneEditor:spawnShape("GE_Mission_borne.shape", 17829.3, -1053.77, -3.04, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17829.3,"col pos y":-1053.77,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e78476bf1550931e2dcf2840322fa9e5d21e7755
SceneEditor:spawnShape("GE_Mission_borne.shape", 17830.17, -1046.5, -2.53, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17830.17,"col pos y":-1046.5,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e1dc1b1709e6cef8a5f1936f24f926553cc999c6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17830.83, -1039.87, -1.81, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17830.83,"col pos y":-1039.87,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 74638a18cf492c69534ac954096a59a5db761394
SceneEditor:spawnShape("GE_Mission_borne.shape", 17825.78, -1034.83, -1.13, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17825.78,"col pos y":-1034.83,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f0beeab150df3ba9826bab42bd52696c54d2da5b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17824.04, -1030.03, -0.38, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17824.04,"col pos y":-1030.03,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b1eace4ef171679e95cb042543165cf056a4084b
SceneEditor:spawnShape("GE_Mission_borne.shape", 17824.5, -1022.78, 1.02, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17824.5,"col pos y":-1022.78,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ae8445402a99879733e7dc5f81c5820399e7fdb1
SceneEditor:spawnShape("GE_Mission_borne.shape", 17823.01, -1016.46, 2.1, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17823.01,"col pos y":-1016.46,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- a35e86c7209e88066fdf1f2ae138c1d660b80d08
SceneEditor:spawnShape("GE_Mission_borne.shape", 17821.48, -1012.42, 3.79, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17821.48,"col pos y":-1012.42,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- afe667983a2e2c85a7acae916c39106e0865529e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17823.9, -1005.54, 6.37, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17823.9,"col pos y":-1005.54,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d5d971d01f8add355fe3da436484c3cd51f37545
SceneEditor:spawnShape("GE_Mission_borne.shape", 17824.4, -1000.33, 8.35, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17824.4,"col pos y":-1000.33,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c61eba064e5aea8c050190410efac11ceec394ba
SceneEditor:spawnShape("GE_Mission_borne.shape", 17829.55, -991.76, 8.29, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17829.55,"col pos y":-991.76,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 22ffa745229aa1cf773f2ce3d35b4939e6508b44
SceneEditor:spawnShape("GE_Mission_borne.shape", 17834.7, -983.2, 8.03, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17834.7,"col pos y":-983.2,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 7c953c8446d316020e8cdaa0628b5d2690a71eb4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17839.86, -974.64, 7.49, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17839.86,"col pos y":-974.64,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 28f2548485b603eb2115284efc80bd81e32ccb3e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17845.02, -966.06, 7.35, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17845.02,"col pos y":-966.06,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4be07593d0ef5eb88b6379a281b541365a1f9941
SceneEditor:spawnShape("GE_Mission_borne.shape", 17809.92, -964.46, 10.22, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17809.92,"col pos y":-964.46,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d1a3be0ea22e9dee9aa20f07a78eeded7f3c707f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17802, -970.56, 10.06, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17802,"col pos y":-970.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 40f8514c8f53a659a20f3e4cd29275b17914efa5
SceneEditor:spawnShape("GE_Mission_borne.shape", 17801.71, -980.56, 9.98, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17801.71,"col pos y":-980.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 98e58b3e909a13bd0467774c0aa8cfd790e83b37
SceneEditor:spawnShape("GE_Mission_borne.shape", 17801.42, -990.56, 9.88, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17801.42,"col pos y":-990.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 5056603700af69aed70b922af3b9b2b81359f242
SceneEditor:spawnShape("GE_Mission_borne.shape", 17801.14, -1000.56, 9.78, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17801.14,"col pos y":-1000.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- fad259a58934b84e2153b40759f2e442897df191
SceneEditor:spawnShape("GE_Mission_borne.shape", 17800.86, -1010.54, 8.87, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17800.86,"col pos y":-1010.54,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 1ef26e0b7f28ad2d672f1160d32229fd7652246c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17800.58, -1020.54, 7.66, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17800.58,"col pos y":-1020.54,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 70273011478bf8aad72d71faa3783762ba8e4495
SceneEditor:spawnShape("GE_Mission_borne.shape", 17807.98, -1021.38, 4.96, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17807.98,"col pos y":-1021.38,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 58e09c2e31ca630d91c1db8fd49d81fc3bc9483c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17813.87, -1020.92, 3.51, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17813.87,"col pos y":-1020.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 92f9f56be431f4bbef7a98346bf4cfefe14383c4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17818.7, -1023.39, 2.64, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17818.7,"col pos y":-1023.39,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 92ef5d65b62d3736f4c32328b82f3b0fad890e2c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17819.75, -1026.32, 2.44, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17819.75,"col pos y":-1026.32,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d096cb98ea1e9ba5470b5638c2372367aa66e5f3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17818.8, -1031.98, 1.64, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17818.8,"col pos y":-1031.98,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 452cb2e9c38807c8e6d4d5aded64f601d4e57408
SceneEditor:spawnShape("GE_Mission_borne.shape", 17815.47, -1037.16, 0.73, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17815.47,"col pos y":-1037.16,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 74e9d13c988bb1ec307d43f3bdfb402f5308ac3f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17810.4, -1039.91, 2.01, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17810.4,"col pos y":-1039.91,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################

