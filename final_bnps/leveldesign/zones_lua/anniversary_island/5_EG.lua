--############### SE_DECO 12333#################
-- GROUP: buildings_karavan
-- 49298406833d4111cf4acc818b3e9b92c2ffff27
SceneEditor:spawnShape("ge_mission_statue_jena_karavan.shape", 17619.975, -665.53, 20, 0, 0, 0, '{"scale x":2,"scale y":2,"scale z":2,"col pos x":-40.172,"col pos y":-665.53,"col orientation":0,"col size x":2.577,"col size y":1.21,"col size z":9.42,"context":null,"url":null}')

-- 6d3af62669bb1f540f1fec40ef25137b7e7e8b21
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17653.271, -654.48, -0.255, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17653.271,"col pos y":-654.48,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 24519928103ae4137c921648472ee30b47f99a4d
SceneEditor:spawnShape("ge_mission_portail_karavan.ps", 17640.406, -654.48, 0.586, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-654.48,"col orientation":0,"col size x":10,"col size y":10,"col size z":11.513,"context":null,"url":null}')

-- 338e23882e0a0898aaefc4f287214c64dba990de
SceneEditor:spawnShape("spire_fx_ca_04.ps", 17619.975, -665.53, -0.05, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17619.975,"col pos y":-665.53,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- cb0bf445d936e9dc7433878ab6df9764ca8ab2d6
SceneEditor:spawnShape("spire_fx_ca_04.ps", 17662.699, -665.53, -2.711, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17662.699,"col pos y":-665.53,"col orientation":0,"col size x":2,"col size y":2,"col size z":2,"context":null,"url":null}')

-- 5157e8c363b23e65a5309cca0e8087536fe37287
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17628.279, -654.48, 0.903, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17628.279,"col pos y":-654.48,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 67f727b56f7bee22f3184a6446501d9fe1c6ab1b
SceneEditor:spawnShape("ge_mission_portail_karavan.ps", 17645.627, -654.48, 0.586, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-654.48,"col orientation":0,"col size x":10,"col size y":10,"col size z":11.513,"context":null,"url":null}')

-- acd0c7b45c0b1a6d83dec9852a2d57d0462bf04f
SceneEditor:spawnShape("ge_mission_portail_karavan.ps", 17635.227, -654.48, 0.586, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":0,"col pos y":-654.48,"col orientation":0,"col size x":10,"col size y":10,"col size z":11.513,"context":null,"url":null}')

-- a99857a268906ad478ffec68be87025651e8aa42
SceneEditor:spawnShape("ge_mission_statue_jena_karavan.shape", 17662.699, -665.53, 20, 0, 0, 0, '{"scale x":2,"scale y":2,"scale z":2,"col pos x":54.105,"col pos y":-665.53,"col orientation":0,"col size x":2.577,"col size y":1.21,"col size z":9.42,"context":null,"url":null}')

-- 22e65d999c76cf583ad765c7b067bf1996d315d9
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17619.076, -654.48, 0.913, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17619.076,"col pos y":-654.48,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 4b03e5bcf605761f953a4ff78af02c9f1a2e9fcf
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17662.512, -654.48, -0.604, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17662.512,"col pos y":-654.48,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 944da6716813ba619b015ff4fa5f14604ae15bc5
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17671.783, -654.48, -1.106, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17671.783,"col pos y":-654.48,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 752b7a3c5d25d5d6e06d3f6ee8550f253b6c7b7c
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17609.877, -654.48, 0.869, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17609.877,"col pos y":-654.48,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- 312f7b5bd21352aba92a64dd98be2ddab93bb9d5
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.74, -649.249, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.74,"col pos y":-649.249,"col orientation":0,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- a7df413b88aff57405f851716c3da44ac28b6cc5
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17605.672, -640.182, 0.8, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17605.672,"col pos y":-640.182,"col orientation":1.6,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- d1c1d4de8b635c034170491dbbeb597905964da2
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -649.269, -0.797, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-649.269,"col orientation":4.861,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- f1c8f6c2c82ad62e32a5276f5140c5fe53174b35
SceneEditor:spawnShape("ge_mission_grande_barriere_karavan.ps", 17675.865, -640.387, -0.133, 0, 0, 1.57, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.865,"col pos y":-640.387,"col orientation":1.58,"col size x":8.577,"col size y":0.78,"col size z":3.89,"context":null,"url":null}')

-- bf66e64c0942122ba4c877de5b1d233f763cb3d0
SceneEditor:spawnShape("ge_mission_tourgarde_karavan.shape", 17666.811, -642.024, 0.012, 0, 0, 3.14, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17666.811,"col pos y":-642.024,"col orientation":0,"col size x":9.731,"col size y":9.93,"col size z":21.255,"context":null,"url":null}')

-- 9f02a6b33df7aa29ad96982d979695613acdd085
SceneEditor:spawnShape("ge_mission_tourgarde_karavan.shape", 17614.52, -641.635, 0.091, 0, 0, 3.14, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17614.52,"col pos y":-641.635,"col orientation":1.57,"col size x":9.731,"col size y":9.93,"col size z":21.255,"context":null,"url":null}')


-- GROUP: center
-- 18ed350afb931e028667a163272a44840b628963
SceneEditor:spawnShape("ge_partylamp_03.shape", 17758.15, -780.027, 10.115, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17758.15,"col pos y":-780.027,"col orientation":0,"col size x":1.425,"col size y":1.425,"col size z":5.267,"context":null,"url":null}')

-- c0c29e3a2fa80301949b46d639445b8e874fd9a0
SceneEditor:spawnShape("ge_partylamp_03.shape", 17732.67, -765.651, 9.92, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17732.67,"col pos y":-765.651,"col orientation":0,"col size x":1.055,"col size y":1.055,"col size z":5.267,"context":null,"url":null}')

-- d117483138638e167faa86ea6fdfcedaa108c4cb
SceneEditor:spawnShape("ge_partylamp_03.shape", 17710.912, -793.46, 9.895, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17710.912,"col pos y":-793.46,"col orientation":0,"col size x":1.205,"col size y":1.205,"col size z":5.267,"context":null,"url":null}')

-- b418a74fb55ec1d1a1d203cb4b5e25454ef55219
SceneEditor:spawnShape("ge_partylamp_03.shape", 17675.645, -797.728, 10.027, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.645,"col pos y":-797.728,"col orientation":0,"col size x":1.745,"col size y":1.745,"col size z":5.267,"context":null,"url":null}')

-- db63ac568fb03c7485a002caeaa0c6b4073c781c
SceneEditor:spawnShape("ge_partylamp_03.shape", 17630.641, -786.384, 10.713, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17630.641,"col pos y":-786.384,"col orientation":0,"col size x":1.405,"col size y":1.405,"col size z":5.267,"context":null,"url":null}')


-- GROUP: center_bar
-- b983e47ededa42114ec2df4a1c1e48ab02b158a9
SceneEditor:spawnShape("fy_s2_lovejail_a.shape", 17737.453, -787.497, 10.048, 0, 0, 0, '{"col pos x":17737.453,"col size x":2.543,"scale z":1,"col size y":2.444,"col orientation":0,"scale y":1,"col size z":5.947,"scale x":1,"col pos y":-787.497,"context":null,"url":null}')

-- 8a92fcefc585399cce3b4957e99fd7e9d5ebb109
SceneEditor:spawnShape("fy_s2_lovejail_c.shape", 17729.641, -798.977, 10.019, 0, 0, 0, '{"col pos x":17729.641,"col size x":3.167,"scale z":1,"col size y":2.255,"col orientation":0,"scale y":1,"col size z":5.141,"scale x":1,"col pos y":-798.977,"context":null,"url":null}')

-- 26e45c299a1ebe088d0c06db6fd336bbc5555b37
SceneEditor:spawnShape("fy_s2_coconuts_b.shape", 17744.949, -792.098, 10.049, 0, 0, 0, '{"col pos x":17744.99,"col size x":1.962,"scale z":1,"col size y":2.508,"col orientation":0,"scale y":1,"col size z":13.468,"scale x":1,"col pos y":-792.823,"context":null,"url":null}')

-- f864e97c3dd124a2d5182e1d54b4e8d9bf03d100
SceneEditor:spawnShape("fy_s2_coconuts_a.shape", 17753.543, -781.983, 10.029, 0, 0, 0, '{"col pos x":17753.543,"col size x":2.028,"scale z":1,"col size y":1.844,"col orientation":0,"scale y":1,"col size z":13.485,"scale x":1,"col pos y":-781.983,"context":null,"url":null}')

-- 1477e1a82f33f1b832aee87fd33d848c21499035
SceneEditor:spawnShape("fy_s2_coconuts_a.shape", 17716.389, -798.438, 9.956, 0, 0, 0, '{"col pos x":17716.389,"col size x":1.978,"scale z":1,"col size y":1.664,"col orientation":0,"scale y":1,"col size z":13.485,"scale x":1,"col pos y":-798.438,"context":null,"url":null}')

-- 52a8f5dd50c9ece1983b0924a318d3e2f7978b81
SceneEditor:spawnShape("skull01.shape", 17724.416, -783.056, 13.649, -1.6, -0.03, -2.32, '{"col pos x":-1,"col size x":1.199,"scale z":2.109,"col size y":0.522,"col orientation":0,"scale y":2.109,"col size z":0.174,"scale x":2.109,"col pos y":-783.056,"context":null,"url":null}')

-- 5b86743ec6ea61875771524bf7b56209c060a6e6
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17726.248, -779.367, 9.946, 0, 0, 0, '{"col pos x":0,"col size x":3.161,"scale z":1,"col size y":1.453,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-779.367,"context":null,"url":null}')

-- 202e4ab4a1fdf477bd3db1a14d898d20ca194a45
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 17721.436, -785.483, 9.831, 0, 0, 0, '{"col pos x":0,"col size x":3.161,"scale z":1,"col size y":1.453,"col orientation":0,"scale y":1,"col size z":5.674,"scale x":1,"col pos y":-785.483,"context":null,"url":null}')

-- e752208249b2e8ad9eb1a136cf24aa5c9cf32d64
SceneEditor:spawnShape("coffre.shape", 17724.783, -783.354, 10.819, 0, 0, -0.76, '{"col pos x":17724.783,"col size x":1,"scale z":2,"col size y":1,"col orientation":0.785,"scale y":5,"col size z":0.615,"scale x":1,"col pos y":-783.354,"context":null,"url":null}')

-- 4b213e8dc783754ec0c4d4948c8d51c8f957ffe8
SceneEditor:spawnShape("fy_bt_sheriff_ok_porte01_armurie.shape", 17723.576, -782.349, 9.961, 0, 0, 0.86, '{"col pos x":101.17,"col size x":1.5,"scale z":1.342,"col size y":0.2,"col orientation":0,"scale y":1.063,"col size z":2.9,"scale x":1.003,"col pos y":-782.349,"context":null,"url":null}')

-- 27d2da848a9bb969b940b0ec354fb650fb595e78
SceneEditor:spawnShape("fy_cn_module_nb03.shape", 17718.801, -778.184, 9.718, 0, 0, -0.74, '{"col pos x":17718.801,"col size x":16.011,"scale z":0.5,"col size y":9.698,"col orientation":-0.73,"scale y":0.5,"col size z":27.891,"scale x":0.6,"col pos y":-778.184,"context":null,"url":null}')

-- 20642a3aa2ec292c25fd611570cc419b06115fae
SceneEditor:spawnShape("coussin.shape", 17721.16, -797.614, 9.669, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-797.614,"context":null,"url":null}')

-- 80f996ed4b7b09aae2f6a0637c0c35424d6ff93b
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17721.066, -799.123, 9.962, 0, 0, 0, '{"col pos x":17721.066,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-799.123,"context":null,"url":null}')

-- d4d4131486c6f38190949305687d57fa212b9a10
SceneEditor:spawnShape("coussin.shape", 17734.354, -796.769, 9.704, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-796.769,"context":null,"url":null}')

-- e6d1f317138820fc80170cfe13d9a6a648d7a0dc
SceneEditor:spawnShape("coussin.shape", 17739.064, -793.771, 9.75, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-793.771,"context":null,"url":null}')

-- 7befa9f3558b366ebfacd2706c449b3cbd393fc0
SceneEditor:spawnShape("coussin.shape", 17738.697, -790.541, 9.686, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-790.541,"context":null,"url":null}')

-- ea0b2572d50a94554eec39fc6e927eb9e36164f9
SceneEditor:spawnShape("coussin.shape", 17743.887, -787.909, 9.786, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-787.909,"context":null,"url":null}')

-- 1a3bf29c77650da3fd1a12b719653a9198a39988
SceneEditor:spawnShape("coussin.shape", 17743.986, -784.888, 9.774, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-784.888,"context":null,"url":null}')

-- 1fe188bc66582e8e3edb5525be8d7ac7143ad3e8
SceneEditor:spawnShape("coussin.shape", 17748.299, -783.475, 9.766, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-783.475,"context":null,"url":null}')

-- 2e710785b2a9d01429e1e232b086341d323ed43d
SceneEditor:spawnShape("coussin.shape", 17748.074, -779.988, 9.804, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-779.988,"context":null,"url":null}')

-- c1003fc66cbe98ec093809789e44c439a17d7f9d
SceneEditor:spawnShape("coussin.shape", 17742.91, -775.019, 9.696, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-775.019,"context":null,"url":null}')

-- ab9518d04c90199f697accd80488d546086f62f3
SceneEditor:spawnShape("coussin.shape", 17742.785, -779.019, 9.762, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-779.019,"context":null,"url":null}')

-- 181318961547fdf9b60d129e6361b732412a7058
SceneEditor:spawnShape("coussin.shape", 17738.498, -779.466, 9.724, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-779.466,"context":null,"url":null}')

-- d727018e26db966344ff3809310dcfa62746c3b3
SceneEditor:spawnShape("coussin.shape", 17738.209, -783.242, 9.756, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":-2.415,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-783.242,"context":null,"url":null}')

-- 39479829360193bccd896944a325fa628cfd6d6b
SceneEditor:spawnShape("coussin.shape", 17731.984, -784.303, 9.707, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-784.303,"context":null,"url":null}')

-- dff02d191de3b93912d9b2bf654217270d9dea9d
SceneEditor:spawnShape("coussin.shape", 17732.033, -787.965, 9.715, 0, 0, 0, '{"col pos x":0.021,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-787.965,"context":null,"url":null}')

-- 58d71ccf1803321d6d2065a2f2c625c0d736eb35
SceneEditor:spawnShape("coussin.shape", 17727.232, -790.777, 9.721, 0, 0, 0, '{"col pos x":-0.9,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-790.777,"context":null,"url":null}')

-- 47b22f8b029700e98109e1d878dcbe56a9cfa1af
SceneEditor:spawnShape("coussin.shape", 17727.166, -794.228, 9.648, 0, 0, 0, '{"col pos x":-0.66,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-794.228,"context":null,"url":null}')

-- e11218cfffbf6ed36259dfef459f87eb69157fe1
SceneEditor:spawnShape("coussin.shape", 17754.229, -786.459, 9.764, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-786.459,"context":null,"url":null}')

-- 12b4669d9a3794501543414bbdfd957b81b8aacd
SceneEditor:spawnShape("coussin.shape", 17754.299, -790.635, 9.757, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-790.635,"context":null,"url":null}')

-- ea788a6fd7590de3b08c7049b2fab204b2f6bca3
SceneEditor:spawnShape("coussin.shape", 17749.898, -791.078, 9.768, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-791.078,"context":null,"url":null}')

-- 82c6c3d2508787903f27dc64ac9534ad284fe90a
SceneEditor:spawnShape("coussin.shape", 17750.02, -795.06, 9.717, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":0,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-795.06,"context":null,"url":null}')

-- 8298cd4f4b4425ed53c7a7b02df55aaddde85bfe
SceneEditor:spawnShape("coussin.shape", 17745.803, -796.509, 9.755, 0, 0, 0, '{"col pos x":0,"col size x":4.054,"scale z":1,"col size y":4.081,"col orientation":3.141,"scale y":0.504,"col size z":0.615,"scale x":0.504,"col pos y":-796.509,"context":null,"url":null}')

-- 96ecb0ef870b141b7f30c26d8aa46e1a7e15381e
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17754.219, -788.122, 10.045, 0, 0, 0, '{"col pos x":17754.219,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-788.122,"context":null,"url":null}')

-- c5b8bb5f56205a2c1607e656c791170bc2d110e0
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17748.531, -781.375, 10.049, 0, 0, 0, '{"col pos x":17748.531,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":3.141,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-781.375,"context":null,"url":null}')

-- cfcedfda778586d0daee46514c41ff8e1ddf6d14
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17743.082, -776.513, 10.022, 0, 0, 0, '{"col pos x":17743.082,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-776.513,"context":null,"url":null}')

-- 7772312860bab6830ad7f2e3e8c08da0b717e489
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17750.088, -792.843, 10.003, 0, 0, 0, '{"col pos x":17750.088,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-792.843,"context":null,"url":null}')

-- 95c453d0efc5fdb8404910082afbf2b3aba1ca88
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17745.777, -798.314, 10.058, 0, 0, 0, '{"col pos x":17745.777,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-798.314,"context":null,"url":null}')

-- 9c9e8ea33f117755aa08b4f83af01af1d9a2ddd1
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17744.137, -786.382, 10.106, 0, 0, 0, '{"col pos x":17744.137,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-786.382,"context":null,"url":null}')

-- 9a2094e4af5ce98c52194e25fdec6c0470b93361
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17738.828, -792.417, 10.016, 0, 0, 0, '{"col pos x":17738.828,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-792.417,"context":null,"url":null}')

-- d9627f96cde9e59e9a5d807d93fcc4d53fed1a77
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17731.922, -786.213, 10.006, 0, 0, 0, '{"col pos x":17731.922,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-786.213,"context":null,"url":null}')

-- b0d03d84e5b917874a0b11d6cea1fb5a1b2ba74a
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17734.287, -798.448, 10.04, 0, 0, 0, '{"col pos x":17734.287,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-798.448,"context":null,"url":null}')

-- f0cf876cc6163ab8bcb3eb706899a4fd12f793c2
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17726.836, -792.629, 9.985, 0, 0, 0, '{"col pos x":17726.836,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-792.629,"context":null,"url":null}')

-- 232109ff4fff3169b1b15a479e77599967d1a8a8
SceneEditor:spawnShape("fy_acc_table_taverne_02.shape", 17738.588, -781.069, 10.007, 0, 0, 0, '{"col pos x":17738.588,"col size x":0.999,"scale z":1.1,"col size y":1,"col orientation":0,"scale y":0.855,"col size z":1.056,"scale x":0.855,"col pos y":-781.069,"context":null,"url":null}')
--############### SE_DECO 12333#################

--############### SE_yubo_golf 12469#################
-- GROUP: yubo_golf_marker
-- 2b05031e9f5e75621914d9716869a1d549795839
SceneEditor:spawnShape("GE_Mission_borne.shape", 17755.74, -749.65, 10.44, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17755.74,"col pos y":-749.65,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 8822a73ec36c39404c26334b99797bf5c007067f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17746.86, -745.07, 10.44, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17746.86,"col pos y":-745.07,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 9ec057a925fa7c98c99b6dbd6e314f1739b5458a
SceneEditor:spawnShape("GE_Mission_borne.shape", 17737.97, -740.48, 10.17, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17737.97,"col pos y":-740.48,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b8aa9cdafee528da328cf213d3bdc25e0d4793e7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17729.09, -735.92, 9.63, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17729.09,"col pos y":-735.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 185988219b40d77b8151cc49216c8f4e80e9c219
SceneEditor:spawnShape("GE_Mission_borne.shape", 17720.19, -731.34, 9.04, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17720.19,"col pos y":-731.34,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 8f4651fd76f665b88f3796129f359ae5f1d6ddca
SceneEditor:spawnShape("GE_Mission_borne.shape", 17712.34, -737.54, 9.7, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17712.34,"col pos y":-737.54,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ede84373708783072e4a69a09a3e0848050396c5
SceneEditor:spawnShape("GE_Mission_borne.shape", 17704.46, -743.73, 9.64, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17704.46,"col pos y":-743.73,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c8f63b4b970bb7e6322da0282ba751a34ede3759
SceneEditor:spawnShape("GE_Mission_borne.shape", 17696.62, -749.92, 9.26, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17696.62,"col pos y":-749.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 16f1d3e9f65874243c658085ae63b1cb83c9a215
SceneEditor:spawnShape("GE_Mission_borne.shape", 17688.47, -753.21, 9.11, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17688.47,"col pos y":-753.21,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 66c8394016ce45ae4ee3f047aa4c6d3a9276a0e1
SceneEditor:spawnShape("GE_Mission_borne.shape", 17688.77, -756.11, 9, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17688.77,"col pos y":-756.11,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 508aa08f91fb94152d1fd16ec07d376a6dfd668e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17668.48, -752.34, 8.59, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17668.48,"col pos y":-752.34,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 2a260c8d2d963f19b06547977bd17a09a514a5a3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17658.5, -751.89, 8.78, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17658.5,"col pos y":-751.89,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- bb1213b46b8c779dd77266c34eefb2d5dc756ce7
SceneEditor:spawnShape("GE_Mission_borne.shape", 17648.5, -751.44, 9.91, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17648.5,"col pos y":-751.44,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ba2b38790f46bf7a367438b8cbb56f489ec5e5fe
SceneEditor:spawnShape("GE_Mission_borne.shape", 17638.51, -751.01, 10.72, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17638.51,"col pos y":-751.01,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f844c39d3a3136e70df9b674581048fc1b9a5eb4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17628.53, -750.56, 11.65, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17628.53,"col pos y":-750.56,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 093ab73c89e0e928aac04a48bd73b7d326041d9c
SceneEditor:spawnShape("GE_Mission_borne.shape", 17625.91, -746.87, 11.8, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17625.91,"col pos y":-746.87,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 8b7a972f5be215f14b33c2a808eb7ed94b29b076
SceneEditor:spawnShape("GE_Mission_borne.shape", 17626.04, -737.88, 11.34, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17626.04,"col pos y":-737.88,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 18e50cf625ee926938967fcda88f12a6491dd384
SceneEditor:spawnShape("GE_Mission_borne.shape", 17627.07, -727.68, 10.15, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17627.07,"col pos y":-727.68,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 53f027ef44712012d3e86f906e9446c47b8cb530
SceneEditor:spawnShape("GE_Mission_borne.shape", 17620.99, -719.74, 8.92, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17620.99,"col pos y":-719.74,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- e4556ce52680a787ca0e016945dd04c5186cb696
SceneEditor:spawnShape("GE_Mission_borne.shape", 17614.91, -711.79, 6.79, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17614.91,"col pos y":-711.79,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f80382d8f696483cf6791da79eede2db56ab47c8
SceneEditor:spawnShape("GE_Mission_borne.shape", 17608.85, -703.86, 5.39, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17608.85,"col pos y":-703.86,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3b4ec90f3264ab3ccd22755b6a85021a3f6a3abc
SceneEditor:spawnShape("GE_Mission_borne.shape", 17602.77, -695.92, 4.63, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17602.77,"col pos y":-695.92,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- bece79fede43b134bc16bbdc409409884fd5f4c2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17602.08, -705.9, 6.55, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17602.08,"col pos y":-705.9,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f7e662cd2c8abda91164c672d1659296cffe0192
SceneEditor:spawnShape("GE_Mission_borne.shape", 17601.41, -715.87, 8.47, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17601.41,"col pos y":-715.87,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 32c6fba20c0b673ee03f0ec46f98aebac9ef79c6
SceneEditor:spawnShape("GE_Mission_borne.shape", 17600.74, -725.86, 10.3, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17600.74,"col pos y":-725.86,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 3a6b28c7bf0fc0e76e131fe0f3b0dc1b77ba993f
SceneEditor:spawnShape("GE_Mission_borne.shape", 17600.05, -735.84, 11.73, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17600.05,"col pos y":-735.84,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ea84e6e584a539bc13e8492520d22b6390227cad
SceneEditor:spawnShape("GE_Mission_borne.shape", 17604.29, -744.76, 12.1, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17604.29,"col pos y":-744.76,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 4516810593d1051fe41d3ad85fbc7a82cc9ff2c4
SceneEditor:spawnShape("GE_Mission_borne.shape", 17608.13, -753.63, 11.89, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17608.13,"col pos y":-753.63,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 465641968be3f6440d2c119827cf4fe875f92490
SceneEditor:spawnShape("GE_Mission_borne.shape", 17614.62, -761.23, 11.72, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17614.62,"col pos y":-761.23,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 66db122628e227a54bcc2d1987570541abf4fbf2
SceneEditor:spawnShape("GE_Mission_borne.shape", 17621.12, -768.83, 11.55, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17621.12,"col pos y":-768.83,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6494ef5b652aa334a1dc08f77939e37fcb4a7abb
SceneEditor:spawnShape("GE_Mission_borne.shape", 17627.62, -776.43, 11.21, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17627.62,"col pos y":-776.43,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- acd0b0a586c2e3657770b4083900591dd0711563
SceneEditor:spawnShape("GE_Mission_borne.shape", 17637.23, -779.15, 10.58, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17637.23,"col pos y":-779.15,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 56cc0a0c25e81d234522f6f424c4e32df2928e10
SceneEditor:spawnShape("GE_Mission_borne.shape", 17646.86, -781.87, 9.98, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17646.86,"col pos y":-781.87,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- c749ecb3c0137d8f9d90e2a5a9ed344f244b8ea3
SceneEditor:spawnShape("GE_Mission_borne.shape", 17656.48, -784.58, 9.95, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17656.48,"col pos y":-784.58,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- f6e2ff929b4078ca285fb1e101a4c362547ce7f5
SceneEditor:spawnShape("GE_Mission_borne.shape", 17666.11, -787.3, 10, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17666.11,"col pos y":-787.3,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b4a224027d3e6dfae298f9d9c192217797875269
SceneEditor:spawnShape("GE_Mission_borne.shape", 17675.73, -790, 9.95, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17675.73,"col pos y":-790,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 7511d97d914e505f9aae8a7958b97ec7dd8ed1d0
SceneEditor:spawnShape("GE_Mission_borne.shape", 17685.36, -792.72, 9.89, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17685.36,"col pos y":-792.72,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- db1548b3699cda37232786f1335f5e3ded2ce742
SceneEditor:spawnShape("GE_Mission_borne.shape", 17694.98, -795.44, 9.87, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17694.98,"col pos y":-795.44,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ef3cc1e6225b04aa3cae39e1d1f6dcd7edb73b6e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17700.1, -786.86, 9.57, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17700.1,"col pos y":-786.86,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b2827025af104c194dfa39cfb1691b5366a95bba
SceneEditor:spawnShape("GE_Mission_borne.shape", 17705.2, -778.27, 9.34, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17705.2,"col pos y":-778.27,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 6302aebbc3a9635f9d2fcc89d41c3957acd994af
SceneEditor:spawnShape("GE_Mission_borne.shape", 17710.32, -769.66, 9.31, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17710.32,"col pos y":-769.66,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- b51f83aa17c6fd1b99c6d0f9b61da54edef6fe1d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17716.34, -759.57, 9.71, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17716.34,"col pos y":-759.57,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- 07fc1a867285259456422892a83909659afffe2e
SceneEditor:spawnShape("GE_Mission_borne.shape", 17725.1, -755.45, 10, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17725.1,"col pos y":-755.45,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- ae669315fd6a9965822caab8f429298d82a3ad26
SceneEditor:spawnShape("GE_Mission_borne.shape", 17735.41, -755.68, 10.14, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17735.41,"col pos y":-755.68,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- d327df3f26a504c256b54fae66b1b062e9d5a17d
SceneEditor:spawnShape("GE_Mission_borne.shape", 17744.38, -760.1, 10.09, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17744.38,"col pos y":-760.1,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')

-- aa81855450ef33783e0963fcb3e9bafc5e7c0b51
SceneEditor:spawnShape("GE_Mission_borne.shape", 17753.34, -764.5, 10.04, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":17753.34,"col pos y":-764.5,"col orientation":0,"col size x":0.3,"col size y":0.3,"col size z":0.821,"context":null,"url":null}')
--############### SE_yubo_golf 12469#################

--###############  Surprises_of_the_devs Deco 12475#################
-- GROUP: bar
-- 006c39560a3cd6e2e10b145d2561c4559defa053
SceneEditor:spawnShape("mag_aura_speedmove.ps", 17656.723, -711.186, 3.135, 0, 0, 0, '{"col size z":2,"col pos x":17656.723,"scale x":1.95,"scale z":1.95,"col size x":2,"col orientation":0,"scale y":1.95,"col size y":2,"col pos y":-711.186,"context":null,"url":null}')

-- 464ee1fda86eaaf3324a18277ffe2ad78c700454
SceneEditor:spawnShape("misc_preorder.ps", 17657.342, -711.271, 4.15, 0, 0, 0, '{"col size z":0.66,"col pos x":17657.342,"scale x":5.39,"scale z":5.39,"col size x":0.66,"col orientation":0,"scale y":5.39,"col size y":0.66,"col pos y":-711.271,"context":null,"url":null}')

-- ae64cac09163fe73cd28221554aa5c7329a2b77c
SceneEditor:spawnShape("coffre.shape", 17656.736, -711.281, 3.916, 0, 0, 0, '{"col size z":0.615,"col pos x":17656.736,"scale x":2.1,"scale z":2.1,"col size x":1,"col orientation":0,"scale y":2.1,"col size y":1,"col pos y":-711.281,"context":"INSERT COIN","url":null}')

-- 829a7daee8900a222258874fcae54f1da27337a3
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17664.178, -709.255, 2.277, 0, 0, 0, '{"col size z":1.29,"col pos x":17664.178,"scale x":1,"scale z":1,"col size x":0.737,"col orientation":0,"scale y":1,"col size y":0.728,"col pos y":-709.255,"context":null,"url":null}')

-- e307e4578f60f1103e9467eb86ca86c00839c86e
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17663.16, -709.226, 2.32, 0, 0, 0, '{"col size z":1.29,"col pos x":17663.16,"scale x":1,"scale z":1,"col size x":0.737,"col orientation":0,"scale y":1,"col size y":0.728,"col pos y":-709.226,"context":null,"url":null}')

-- ee00a6922f548ce16dd6bf4cd577037cdbae25f5
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17663.834, -709.522, 2.316, 0, 0, 0, '{"col size z":1.29,"col pos x":17663.834,"scale x":1,"scale z":1,"col size x":0.737,"col orientation":0,"scale y":1,"col size y":0.728,"col pos y":-709.522,"context":null,"url":null}')

-- c3570601e48f2eb68a12cc748b2ff84b2656ac2d
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17664.398, -709.947, 2.361, 0, 0, 0, '{"col size z":1.29,"col pos x":17664.398,"scale x":1,"scale z":1,"col size x":0.737,"col orientation":0,"scale y":1,"col size y":0.728,"col pos y":-709.947,"context":null,"url":null}')

-- 3336429267e27c928563d5d72bdd4d0780ecc71f
SceneEditor:spawnShape("tr_s3_flower_a.shape", 17665.023, -709.654, 2.307, 0, 0, 0, '{"col size z":1.29,"col pos x":17665.023,"scale x":1,"scale z":1,"col size x":0.737,"col orientation":0,"scale y":1,"col size y":0.728,"col pos y":-709.654,"context":null,"url":null}')

-- 77d5b172f6e325cae77ce5bd0510c5d47d5068ff
SceneEditor:spawnShape("pr_s3_ploomweed_a.shape", 17666.088, -709.702, 2.283, 0, 0, 0, '{"col size z":3.297,"col pos x":17666.088,"scale x":1,"scale z":1,"col size x":1.572,"col orientation":0,"scale y":1,"col size y":1.144,"col pos y":-709.702,"context":null,"url":null}')

-- 66a0ab380df3e447d0a9a2648fb2f4bbd7f3a299
SceneEditor:spawnShape("pr_s3_ploomweed_a.shape", 17666.264, -711.381, 2.574, 0, 0, 0, '{"col size z":3.297,"col pos x":17666.264,"scale x":1,"scale z":1,"col size x":1.572,"col orientation":0,"scale y":1,"col size y":1.144,"col pos y":-711.381,"context":null,"url":null}')

-- 13fea53ace18e867691d6e65b142da2e3cfcd0d0
SceneEditor:spawnShape("pr_s3_ploomweed_a.shape", 17662.604, -709.173, 2.33, 0, 0, 0, '{"col size z":3.297,"col pos x":17662.604,"scale x":1,"scale z":1,"col size x":1.572,"col orientation":0,"scale y":1,"col size y":1.144,"col pos y":-709.173,"context":null,"url":null}')

-- 7ee89709521c6b1a9ec058966a163ca58c225df4
SceneEditor:spawnShape("pr_s3_ploomweed_a.shape", 17661.898, -710.963, 2.662, 0, 0, 0, '{"col size z":3.297,"col pos x":17661.898,"scale x":1,"scale z":1,"col size x":1.572,"col orientation":0,"scale y":1,"col size y":1.144,"col pos y":-710.963,"context":null,"url":null}')

-- 1f4b32ca764adcfbe398a952abea3fbd587f00a5
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17675.117, -706.781, 1.691, 0, 0, 0, '{"col size z":2.616,"col pos x":17675.117,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-706.781,"context":null,"url":null}')

-- b2a5a38e08f8fbbc7f08adb714c80aaca51dbb5f
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17671.863, -705.122, 1.512, 0, 0, 0, '{"col size z":2.616,"col pos x":17671.863,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-705.122,"context":null,"url":null}')

-- 12194e2d8eb0b6a75c55a92831075e9c39ed6150
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17677.445, -708.22, 1.914, 0, 0, 0, '{"col size z":2.616,"col pos x":17677.445,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-708.22,"context":null,"url":null}')

-- dc36e9fb3f10ea4e2f2df1fe6b7af1d87a274ebd
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17671.113, -707.364, 1.937, 0, 0, 0, '{"col size z":2.616,"col pos x":17671.113,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-707.364,"context":null,"url":null}')

-- 107560ab05887cd5993eff076a1c469a1623978e
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17672.453, -706.672, 1.782, 0, 0, 0, '{"col size z":2.616,"col pos x":17672.453,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-706.672,"context":null,"url":null}')

-- c00e2f56d76c54bac2df3ef278bc78e86b25ae39
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17673.123, -709.439, 2.22, 0, 0, 0, '{"col size z":2.616,"col pos x":17673.123,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-709.439,"context":null,"url":null}')

-- 2ea7bf8088be05760152f12fd3bd6d824e92e6e7
SceneEditor:spawnShape("pr_s3_amoeba_c.shape", 17674.994, -708.856, 2.112, 0, 0, 0, '{"col size z":2.616,"col pos x":17674.994,"scale x":1,"scale z":1,"col size x":2.863,"col orientation":0,"scale y":1,"col size y":1.502,"col pos y":-708.856,"context":null,"url":null}')

-- 6b03af14e2aa8e25048588c7c2e6501849ae292a
SceneEditor:spawnShape("pr_s1_rotaflore_a.shape", 17672.746, -719.044, 4.682, 0, 0, 0, '{"col size z":12.685,"col pos x":17672.746,"scale x":1,"scale z":1,"col size x":5.054,"col orientation":0,"scale y":1,"col size y":5.602,"col pos y":-719.044,"context":null,"url":null}')

-- b62c65adbc03f00744fc40cefeedb40050de1af3
SceneEditor:spawnShape("fo_s3_fougere.shape", 17667.734, -717.863, 4.288, 0, 0, 0, '{"col size z":3.538,"col pos x":17667.734,"scale x":0.635,"scale z":0.529,"col size x":5.341,"col orientation":0,"scale y":0.635,"col size y":4.678,"col pos y":-717.863,"context":null,"url":null}')

-- c40717aae6e42c5940d1007fb151d86463f7e056
SceneEditor:spawnShape("fo_s3_fougere.shape", 17671.137, -714.875, 3.394, 0, 0, 0, '{"col size z":3.538,"col pos x":17671.137,"scale x":0.635,"scale z":0.529,"col size x":5.341,"col orientation":0,"scale y":0.635,"col size y":4.678,"col pos y":-714.875,"context":null,"url":null}')

-- daef91c63cb5dc72fb9d2c47dd0933847e7c87be
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17658.291, -716.702, 8.327, 0, 0, 0, '{"col size z":1,"col pos x":17658.291,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.702,"context":null,"url":null}')

-- b235af23d43bed8405138b5d6f637e233878ad68
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17657.219, -716.715, 8.257, 0, 0, 0, '{"col size z":1,"col pos x":17657.219,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.715,"context":null,"url":null}')

-- 7bee8168523b78d7e9ef2d021576fdec32571b57
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17658.324, -716.764, 7.285, 0, 0, 0, '{"col size z":1,"col pos x":17658.324,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.764,"context":null,"url":null}')

-- 879c27a51651ac58f06f9a8e65203ca67165d131
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17658.299, -716.753, 6.225, 0, 0, 0, '{"col size z":1,"col pos x":17658.299,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.753,"context":null,"url":null}')

-- e0817416524d6088c886bbc3aa9abfbb43edbc2a
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17663.566, -718.104, 10.44, 0, 0, 0, '{"col size z":1,"col pos x":17663.566,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-718.104,"context":null,"url":null}')

-- b82677a3d3deb6d70e732d33653eb445d8c87240
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17663.564, -718.068, 9.338, 0, 0, 0, '{"col size z":1,"col pos x":17663.564,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-718.068,"context":null,"url":null}')

-- 9d23c3ba5b3de5d58805ac5da6cb667996fb1bf7
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17664.705, -718.075, 9.339, 0, 0, 0, '{"col size z":1,"col pos x":17664.705,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-718.075,"context":null,"url":null}')

-- 8dfa67c2ad44ca7bf119954c92a44f8b1f163dc3
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17664.68, -718.077, 8.258, 0, 0, 0, '{"col size z":1,"col pos x":17664.68,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-718.077,"context":null,"url":null}')

-- 79d0c1f33c80317dd575fdf627e6fab2868ff57f
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17664.604, -716.664, 4.849, 0, 0, 0, '{"col size z":1,"col pos x":17664.604,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.664,"context":null,"url":null}')

-- 709b7c43612de72727cb9dca1dbd24a661474817
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17662.367, -716.651, 5.985, 0, 0, 0, '{"col size z":1,"col pos x":17662.367,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.651,"context":null,"url":null}')

-- 813bc9d9fbda16e0b95114f74dd411efc964e247
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17663.514, -716.646, 4.917, 0, 0, 0, '{"col size z":1,"col pos x":17663.514,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.646,"context":null,"url":null}')

-- 0f9129e6e2f685d959c04a743b793f30482e0ca2
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17660.271, -716.633, 5.951, 0, 0, 0, '{"col size z":1,"col pos x":17660.271,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.633,"context":null,"url":null}')

-- 937d94dc3ed1aa8e0a9d1a126693cb701cb4a2fc
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17660.287, -716.66, 4.898, 0, 0, 0, '{"col size z":1,"col pos x":17660.287,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.66,"context":null,"url":null}')

-- c26a9f4b368cdba7d5501829fcbed34e357d148e
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17662.406, -716.661, 4.931, 0, 0, 0, '{"col size z":1,"col pos x":17662.406,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.661,"context":null,"url":null}')

-- 7ecb25261e9d718eefd6a206e780510d07c744ff
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17660.311, -716.676, 3.845, 0, 0, 0, '{"col size z":1,"col pos x":17660.311,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.676,"context":null,"url":null}')

-- 3388c01a4fbbef80d09faf2314d7b74b41f1c0be
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17661.406, -716.703, 3.869, 0, 0, 0, '{"col size z":1,"col pos x":17661.406,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.703,"context":null,"url":null}')

-- 5a02f12fb99c4b48de5f42fdcc9049e5970a6ba4
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17664.547, -716.618, 3.724, 0, 0, 0, '{"col size z":1,"col pos x":17664.547,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.618,"context":null,"url":null}')

-- cb1707b8a6e0992a73154a01f17b37a2235ec06a
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 17662.469, -716.714, 3.864, 0, 0, 0, '{"col size z":1,"col pos x":17662.469,"scale x":1,"scale z":1,"col size x":1.044,"col orientation":0,"scale y":1,"col size y":1.044,"col pos y":-716.714,"context":null,"url":null}')

-- a702e3675319eca5879a3d8fcd6d84455fcc6ad5
SceneEditor:spawnShape("tr_acc_pouf.shape", 17663.367, -712.444, 3.172, 0, 0, 0, '{"col size z":0.522,"col pos x":0,"scale x":1,"scale z":1.13,"col size x":0.604,"col orientation":0,"scale y":1,"col size y":0.604,"col pos y":-712.444,"context":null,"url":null}')

-- d68ada95fcf2e973e4b2e6b0bc3b6d762c8a7945
SceneEditor:spawnShape("tr_acc_pouf.shape", 17664.754, -712.474, 3.184, 0, 0, 0, '{"col size z":0.522,"col pos x":0,"scale x":1,"scale z":1.15,"col size x":0.604,"col orientation":0,"scale y":1,"col size y":0.604,"col pos y":-712.474,"context":null,"url":null}')

-- fcaa18e4c22ddbce7a473d384a514cde6974d037
SceneEditor:spawnShape("tr_acc_lit.shape", 17668.436, -714.082, 4.083, 0.08, -0.22, -2.2, '{"col size z":1.765,"col pos x":17668.436,"scale x":1,"scale z":1,"col size x":4.041,"col orientation":0,"scale y":1,"col size y":1.997,"col pos y":-714.082,"context":null,"url":null}')

-- 8aa3752f5536845b55cb69ba975f4350d1087e15
SceneEditor:spawnShape("ge_mission_table.shape", 17663.926, -710.531, 2.441, 0, 0, 0, '{"col size z":1.065,"col pos x":17663.926,"scale x":1.445,"scale z":1.49,"col size x":1.952,"col orientation":0,"scale y":0.679,"col size y":1.738,"col pos y":-710.531,"context":null,"url":null}')

-- 57a75c1ad383694e8305ac30d812ec599830ccff
SceneEditor:spawnShape("ge_mission_talkie.shape", 17665.117, -710.423, 3.926, 0, 0, 0, '{"col size z":0.361,"col pos x":17665.117,"scale x":1,"scale z":1,"col size x":0.131,"col orientation":0,"scale y":1,"col size y":0.226,"col pos y":-710.423,"context":null,"url":null}')

-- 6fe48dcfd8ea657b94969071fca83def4450319a
SceneEditor:spawnShape("ge_mission_talkie.shape", 17662.721, -710.324, 3.947, 0, 0, 0, '{"col size z":0.361,"col pos x":17662.721,"scale x":1,"scale z":1,"col size x":0.131,"col orientation":0,"scale y":1,"col size y":0.226,"col pos y":-710.324,"context":null,"url":null}')

-- 8b676bdb18dd27faf43a6ee067d29122c82b4190
SceneEditor:spawnShape("ge_mission_laptop.shape", 17664.6, -710.724, 3.998, 0, -0.17, -1.54, '{"col size z":0.762,"col pos x":17664.6,"scale x":1,"scale z":1,"col size x":0.505,"col orientation":0,"scale y":1,"col size y":0.632,"col pos y":-710.724,"context":null,"url":null}')

-- ce765f0c472fcb915f883e61fb9f8fbbce7cbace
SceneEditor:spawnShape("ge_mission_laptop.shape", 17663.33, -710.724, 4.008, 0, -0.17, -1.54, '{"col size z":0.762,"col pos x":17663.33,"scale x":1,"scale z":1,"col size x":0.505,"col orientation":0,"scale y":1,"col size y":0.632,"col pos y":-710.724,"context":null,"url":null}')
--###############  Surprises_of_the_devs Deco 12475#################

