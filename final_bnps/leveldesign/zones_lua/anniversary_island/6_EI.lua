--############### SE_DECO 12333#################
-- GROUP: plants_matis
-- 9758299cc326475f2bc3774395ad90307a7fc21e
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18059.361, -812.113, -0.106, 0, 0, 0, '{"col size x":1.837,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.448,"col size z":13.303,"scale y":1,"col pos y":-812.113,"col pos x":18059.361,"context":null,"url":null}')

-- 71f36e4d4cb365e7a00cf100b5bdd269681f265e
SceneEditor:spawnShape("fo_s2_spiketree.shape", 18054.537, -829.854, -0.691, 0, 0, 0, '{"col size x":1.747,"scale x":1,"col orientation":0,"scale z":1,"col size y":1.358,"col size z":13.303,"scale y":1,"col pos y":-829.854,"col pos x":18054.537,"context":null,"url":null}')


-- GROUP: plants_zorai
-- a545e8b2ba58099bfa0ea243f9f594dda684fa4c
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 18048.195, -957.46, -0.053, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18048.195,"col pos y":-957.46,"col orientation":-0.813,"col size x":4.551,"col size y":4.448,"col size z":3.102,"context":null,"url":null}')

-- 4019819fdea969029b453fed6cb616b604f6b867
SceneEditor:spawnShape("ju_s2_big_tree.shape", 18028.48, -955.149, -0.115, 0, 0, 0, '{"scale x":1,"scale y":1,"scale z":1,"col pos x":18028.48,"col pos y":-955.149,"col orientation":0,"col size x":8.19,"col size y":4.705,"col size z":25.035,"context":null,"url":null}')
--############### SE_DECO 12333#################

