
---- ZONE : 74_CL
-- GROUP: deco
-- 930feedc41352206b2e0685386273100c0e068cf
SceneEditor:spawnShape("fo_s2_birch.shape", 10151.922, -11848.574, 8.199, 0, 0, 0, '{"col orientation":-0.7,"col pos y":-11846.853,"scale y":1,"scale z":1,"col size y":-1.059,"col size z":15.074,"col pos x":10151.812,"col size x":-0.665,"scale x":1}')

-- ef0e9d48598a9fb84ab504befebfcb79af5f6f1f
SceneEditor:spawnShape("ge_mission_barriere.shape", 10160.082, -11842.369, 8.895, 0, 0, -1.56, '{"col orientation":1.64,"col pos y":-11842.369,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10160.082,"col size x":3.424,"scale x":1}')

-- 9402c7d63358bc1bc44af4cf9fe43857970fa37a
SceneEditor:spawnShape("fy_acc_reverb_a_1.shape", 10160.056, -11840.662, 9.195, 0, 0, -2.69, '{"col orientation":0,"col pos y":-11840.662,"scale y":1,"scale z":1,"col size y":0.596,"col size z":3.446,"col pos x":10160.056,"col size x":1.436,"scale x":1}')

-- 934ce8273457d2dc3633a50db1576e36f8402959
SceneEditor:spawnShape("fy_acc_reverb_a_1.shape", 10160.072, -11843.955, 8.693, 0, 0, -3.42, '{"col orientation":0,"col pos y":-11843.955,"scale y":1,"scale z":1,"col size y":0.596,"col size z":3.446,"col pos x":10160.072,"col size x":1.436,"scale x":1}')

-- 7e4878d2fe12919ea790b0e8defffaf6ebbdc7c3
SceneEditor:spawnShape("fy_acc_reverb_a_1.shape", 10156.671, -11848.051, 8.586, 0, -0, -4.61, '{"col orientation":0,"col pos y":-11848.051,"scale y":1,"scale z":1,"col size y":0.616,"col size z":3.446,"col pos x":10156.671,"col size x":1.456,"scale x":1}')

-- f8b6fc8d099616276f3d5f5d4737a1659d08cd31
SceneEditor:spawnShape("fy_acc_reverb_a_1.shape", 10159.708, -11847.228, 7.911, 0, 0, -3.89, '{"col orientation":0,"col pos y":-11847.228,"scale y":1,"scale z":1,"col size y":0.596,"col size z":3.446,"col pos x":10159.708,"col size x":1.436,"scale x":1}')

-- 36a54fd557951d2806b89273419067ed2a57447e
SceneEditor:spawnShape("fy_acc_reverb_a_1.shape", 10153.389, -11847.157, 8.693, 0, 0, -5.72, '{"col orientation":-0.22,"col pos y":-11847.157,"scale y":1,"scale z":1,"col size y":0.596,"col size z":3.446,"col pos x":10153.389,"col size x":1.436,"scale x":1}')

-- ce9a97f12186c46a4e25444992c1299aca17adbd
SceneEditor:spawnShape("fo_s3_fougere.shape", 10160.284, -11848.312, 7.342, 0, 0, 0, '{"col orientation":0,"col pos y":-11848.312,"scale y":0.635,"scale z":0.529,"col size y":3.768,"col size z":3.538,"col pos x":10160.284,"col size x":2.231,"scale x":0.635}')

-- 4f9acbff6bd0d2cf526c3855dfaee1f022d6c080
SceneEditor:spawnShape("fo_s3_fougere.shape", 10162.649, -11851.56, 5.391, 0, 0, 0, '{"col orientation":0,"col pos y":-11851.56,"scale y":0.635,"scale z":0.529,"col size y":4.678,"col size z":3.538,"col pos x":10162.649,"col size x":2.321,"scale x":0.635}')

-- 919d912ef4d0666330fa88390d4741c269b7a33f
SceneEditor:spawnShape("fo_s3_fougere.shape", 10164.165, -11849.638, 5.19, 0, 0, 0, '{"col orientation":0,"col pos y":-11849.638,"scale y":0.635,"scale z":0.529,"col size y":4.678,"col size z":3.538,"col pos x":10164.165,"col size x":7.241,"scale x":0.635}')

-- 7eeb4199d33abdf355b06cabf189c6e5842640db
SceneEditor:spawnShape("fo_s3_buissonaepine.shape", 10158.304, -11850.572, 7.245, 0, 0, 0.71, '{"col orientation":0,"col pos y":-11850.572,"scale y":1,"scale z":1,"col size y":3.179,"col size z":2.906,"col pos x":10158.304,"col size x":3.785,"scale x":1}')

-- 5514a41084b438d3bb1541884ebc5c7f48ede2e5
SceneEditor:spawnShape("fo_s3_buissonaepine.shape", 10154.598, -11849.399, 8.356, 0, 0, 0, '{"col orientation":-0.28,"col pos y":-11848.956,"scale y":1,"scale z":1,"col size y":3.159,"col size z":2.906,"col pos x":10154.761,"col size x":3.765,"scale x":1}')

-- 8cca40798e5e169540bc161552b9d2280ea355bf
SceneEditor:spawnShape("ge_mission_barriere.shape", 10160.08, -11845.167, 8.383, 0.07, 0.39, -8, '{"col orientation":1.43,"col pos y":-11845.167,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10160.08,"col size x":3.424,"scale x":1}')

-- 3ac30f483ada58c049969079cbfac9eb8a7dea43
SceneEditor:spawnShape("ge_mission_barriere.shape", 10155.091, -11847.655, 8.899, 0, 0, -0.28, '{"col orientation":-5.01,"col pos y":-11847.347,"scale y":1,"scale z":1,"col size y":-3.452,"col size z":2.303,"col pos x":10154.861,"col size x":-0.216,"scale x":1}')

-- 4c2b466283fa4f296b06fda088430a0abd9b8e8b
SceneEditor:spawnShape("ge_mission_barriere.shape", 10157.804, -11847.77, 8.403, 0, -0.36, -2.86, '{"col orientation":0,"col pos y":-11847.77,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10157.804,"col size x":3.424,"scale x":1}')


-- GROUP: clickable_items
-- abbffbd46b8b2b20757c786e1b34d74844ae7876
SceneEditor:spawnShape("sp_medit.ps", 10156.205, -11841.211, 10.086, 0, 0, 0, '{"col pos x":10151.947,"col orientation":-3.53,"col size y":-0.564,"col size x":-0.581,"scale y":1,"col size z":5.208,"scale z":1,"col pos y":-11847.119,"scale x":1}')


