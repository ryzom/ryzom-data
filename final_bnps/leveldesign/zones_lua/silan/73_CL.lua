---- ZONE : 73_CL
-- GROUP: deco
-- b7d028cd0a9aeb4a92ef60817d6990403d714c52
SceneEditor:spawnShape("ge_acc_baniere_seniorguide.ps", 10154.962, -11818.943, 4.265, 0, 0, 0, '{"col orientation":0,"col pos y":-11818.943,"scale y":1,"scale z":1,"col size y":1.65,"col size z":2.596,"col pos x":10154.962,"col size x":1.65,"scale x":1}')

-- 7e1b21b459b00fcee531496ac853de3e0d050eef
SceneEditor:spawnShape("ge_acc_baniere_seniorguide.ps", 10154.354, -11822.3, 4.331, 0, 0, 0, '{"col orientation":0,"col pos y":-11822.3,"scale y":1,"scale z":1,"col size y":1.65,"col size z":2.596,"col pos x":10154.354,"col size x":1.65,"scale x":1}')

-- cd0b7a14b29676582b8458aa8749e8889b2fec66
SceneEditor:spawnShape("matis_alcove.ps", 10154.269, -11822.263, 5.467, 0, 0, 0, '{"col orientation":0,"col pos y":-11822.264,"scale y":1,"scale z":1,"col size y":0.649,"col size z":2.895,"col pos x":10152.758,"col size x":0.69,"scale x":1}')

-- 2abe657ccc28c7ccc542c3a47aa744e48d07ff4a
SceneEditor:spawnShape("matis_alcove.ps", 10154.943, -11819.002, 5.484, 0, 0, 0, '{"col orientation":0,"col pos y":-11819.002,"scale y":1,"scale z":1,"col size y":0.519,"col size z":2.895,"col pos x":10154.943,"col size x":0.56,"scale x":1}')

-- 71e393e221556dc7e6b5129c7bd171c9c5e38a07
SceneEditor:spawnShape("ge_mission_stand.shape", 10152.479, -11819.875, 4.773, 0, 0, 1.36, '{"col orientation":0,"col pos y":-11819.875,"scale y":0.938,"scale z":0.504,"col size y":4.775,"col size z":6.313,"col pos x":10152.479,"col size x":4.258,"scale x":1.088}')

-- 6002f80e38b627f7747c502423ab9ea77e7d6fc6
SceneEditor:spawnShape("ge_mission_reverbere.shape", 10154.938, -11818.901, 1.125, 0, 0, 1.29, '{"col orientation":0,"col pos y":-11818.901,"scale y":1.42,"scale z":1.42,"col size y":0.545,"col size z":3.679,"col pos x":10154.938,"col size x":0.577,"scale x":1.42}')

-- 0fb7f2c51d4bfe5309a13dcfb8870f6076ec4e91
SceneEditor:spawnShape("ge_mission_reverbere.shape", 10154.342, -11822.253, 0.96, 0, 0, 1.35, '{"col orientation":0,"col pos y":-11822.253,"scale y":1.48,"scale z":1.48,"col size y":0.545,"col size z":3.679,"col pos x":10154.342,"col size x":0.577,"scale x":1.48}')

-- 6b8eb6792dc15a594981b31a2957dd25febb9efb
SceneEditor:spawnShape("ge_mission_barriere.shape", 10154.778, -11812.08, 4.931, 0, -0.24, 2.32, '{"col orientation":2.43,"col pos y":-11812.08,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10154.778,"col size x":3.424,"scale x":1}')

-- a0aa81768b7ebbc0a3770d9611842818bf9f1173
SceneEditor:spawnShape("ge_mission_barriere.shape", 10156.274, -11815.146, 4.43, 0, 0, 1.71, '{"col orientation":4.82,"col pos y":-11815.146,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10156.274,"col size x":3.424,"scale x":1}')

-- 4431bcf9c3ca11ae54a372d147818e33cff26750
SceneEditor:spawnShape("ge_mission_barriere.shape", 10151.169, -11826.278, 6.765, 0, 0.44, 0.99, '{"col orientation":4.07,"col pos y":-11826.278,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10151.169,"col size x":3.424,"scale x":1}')

-- ab764cbe2f1b837cdb0787797d005b4f8765fded
SceneEditor:spawnShape("ge_mission_barriere.shape", 10152.704, -11823.534, 5.461, 0, 0.45, 7.42, '{"col orientation":4.31,"col pos y":-11823.534,"scale y":1,"scale z":0.88,"col size y":0.188,"col size z":2.303,"col pos x":10152.704,"col size x":3.424,"scale x":1}')

-- 460e66ead0f9de782103e9a141516ad778bbe39d
SceneEditor:spawnShape("ge_mission_barriere.shape", 10155.23, -11817.71, 4.463, 0, 0, 0.62, '{"col orientation":-2.52,"col pos y":-11817.71,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10155.23,"col size x":3.424,"scale x":1}')

-- 53e5f6dfde58c3b56b7b4f613139905ca592fe9b
SceneEditor:spawnShape("fo_s2_birch.shape", 10155.573, -11816.583, 4.491, 0, 0, 0, '{"col orientation":0,"col pos y":-11816.583,"scale y":1,"scale z":1,"col size y":1.851,"col size z":15.074,"col pos x":10155.573,"col size x":2.245,"scale x":1}')

-- 4e350e01de22f864d5b53f24991205ffcc5ae651
SceneEditor:spawnShape("fo_s3_champignou_a.shape", 10150.889, -11824.797, 6.071, 0, 0, 0, '{"col orientation":0,"col pos y":-11824.797,"scale y":1,"scale z":1,"col size y":1.782,"col size z":3.305,"col pos x":10150.889,"col size x":1.897,"scale x":1}')

-- d67096e54c05652d223bc26ec9d7da9407e83807
SceneEditor:spawnShape("fo_s3_fougere.shape", 10147.441, -11820.279, 5.546, 0, 0, 0, '{"col orientation":0,"col pos y":-11820.279,"scale y":0.635,"scale z":0.529,"col size y":1.518,"col size z":3.538,"col pos x":10147.441,"col size x":2.181,"scale x":0.635}')

-- 2de4efb18fd592679a3b7bf4e92e23f396bacfa9
SceneEditor:spawnShape("fo_s3_fougere.shape", 10153.099, -11816.788, 5.287, 0, 0, 0, '{"col orientation":0,"col pos y":-11816.788,"scale y":0.635,"scale z":0.529,"col size y":1.878,"col size z":3.538,"col pos x":10153.099,"col size x":2.541,"scale x":0.635}')

-- 7bd548a986ffa3d6442571f42989b6f12a5a51fb
SceneEditor:spawnShape("fo_s3_birch.shape", 10151.66, -11817.108, 5.653, 0, 0, 0, '{"col orientation":0,"col pos y":-11817.108,"scale y":1.91,"scale z":2.17,"col size y":0.502,"col size z":8.641,"col pos x":10151.66,"col size x":0.572,"scale x":1.91}')

-- 39614550682ff7da9bf31ba0d715dacf0517a9bb
SceneEditor:spawnShape("fo_s3_birch.shape", 10155.306, -11813.292, 4.876, 0, 0, 0, '{"col orientation":0,"col pos y":-11813.292,"scale y":1,"scale z":1,"col size y":0.312,"col size z":8.641,"col pos x":10155.306,"col size x":0.642,"scale x":1}')

-- 8237065cf241392c6ccfd9f961ae474603ebd016
SceneEditor:spawnShape("fo_s3_birch.shape", 10150.553, -11815.442, 6.218, 0, 0, 0, '{"col orientation":0,"col pos y":-11815.442,"scale y":1,"scale z":1,"col size y":4.382,"col size z":8.641,"col pos x":10150.553,"col size x":4.972,"scale x":1}')

-- 80a37c906de6cc735b710f8e3be81204acba1a7b
SceneEditor:spawnShape("fo_s3_buissonaepine.shape", 10153.381, -11814.613, 5.455, 0, 0, 0, '{"col orientation":0,"col pos y":-11814.613,"scale y":1,"scale z":1,"col size y":3.179,"col size z":2.906,"col pos x":10153.381,"col size x":3.785,"scale x":1}')

-- 31ef574b975122afe69a31c902aa8b01f19dcab8
SceneEditor:spawnShape("fo_s3_buissonaepine.shape", 10149.328, -11818.2, 5.97, 0, 0, 0, '{"col orientation":0,"col pos y":-11824.108,"scale y":1,"scale z":1,"col size y":3.179,"col size z":2.906,"col pos x":10145.07,"col size x":3.785,"scale x":1}')

-- 0c3f86ce171a9da47915dbc088a1fe86d7bf7e8e
SceneEditor:spawnShape("fo_s3_buissonaepine.shape", 10150.104, -11823.056, 5.458, 0, 0, 0, '{"col orientation":0,"col pos y":-11823.056,"scale y":1,"scale z":1,"col size y":3.179,"col size z":2.906,"col pos x":10150.104,"col size x":3.785,"scale x":1}')

-- b1e13c62bed7f05c47a2e1bc47ef04f4fb7ec639
SceneEditor:spawnShape("ge_feudecamp.ps", 10158.17, -11819.996, 3.707, 0, 0, 0, '{"col orientation":0,"col pos y":-11819.996,"scale y":1,"scale z":1,"col size y":1.588,"col size z":5.716,"col pos x":10158.17,"col size x":1.471,"scale x":1}')

-- 43ebadc20e6d64aa7f398795da265c8e89f0b7cb
SceneEditor:spawnShape("ge_mission_objet_pack_3.shape", 10163.48, -11828.516, 3.957, 0, 0, 0, '{"col orientation":1.1444875133696e+243,"col pos y":-11828.516,"scale y":1,"scale z":1,"col size y":3.431,"col size z":2.313,"col pos x":10163.48,"col size x":4.19,"scale x":1}')

-- 4883c8b5fc800d50eaf06cb58a30850f28167e10
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 10162.486, -11825.513, 3.921, 0, 0, 0, '{"col orientation":0,"col pos y":-11825.513,"scale y":1,"scale z":1,"col size y":2.837,"col size z":2.556,"col pos x":10162.486,"col size x":2.339,"scale x":1}')

-- b417f4e81568569852d5b4e52c8bd9ac771606c9
SceneEditor:spawnShape("ge_mission_objet_pack_4.shape", 10162.396, -11821.495, 2.918, 0, 0, 0, '{"col orientation":0,"col pos y":-11821.495,"scale y":1,"scale z":1,"col size y":4.345,"col size z":2.048,"col pos x":10162.396,"col size x":3.046,"scale x":1}')

-- da6813c25811db7cafcb090aec46e4146a024e7f
SceneEditor:spawnShape("fo_s3_champignou_b.shape", 10168.161, -11824.529, 1.656, 0, 0, 0, '{"col orientation":0,"col pos y":-11824.529,"scale y":1,"scale z":1,"col size y":1.782,"col size z":3.305,"col pos x":10168.161,"col size x":1.897,"scale x":1}')

-- 9b7dd8223cec4f72755c3b947f4d89ab2bd91cb2
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10166.533, -11814.672, 1.229, 0, 0, 0.9, '{"col orientation":0,"col pos y":-11814.672,"scale y":1,"scale z":1,"col size y":1.299,"col size z":2,"col pos x":10166.533,"col size x":2.532,"scale x":1}')

-- 8ff0debe82e1df058acd364fe2c521ee1e4319dc
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10164.197, -11812.654, 1.982, 0, 0, -0.74, '{"col orientation":0,"col pos y":-11812.654,"scale y":1,"scale z":1,"col size y":1.103,"col size z":2.481,"col pos x":10164.197,"col size x":2.145,"scale x":1}')

-- e2b117a216f02f694f3cd9e9365b6233113f985b
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10166.977, -11812.135, 1.202, 0, 0, 0.85, '{"col orientation":0,"col pos y":-11812.135,"scale y":1,"scale z":1,"col size y":1.103,"col size z":2.481,"col pos x":10166.977,"col size x":2.145,"scale x":1}')

-- 509f23b3f0e37b42f48615ff31e7385052d078ae
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10161.911, -11816.545, 2.531, 0, 0, 0, '{"col orientation":0,"col pos y":-11816.545,"scale y":1,"scale z":1,"col size y":1.103,"col size z":2.481,"col pos x":10161.911,"col size x":2.145,"scale x":1}')

-- 1dfa534cfdedaf19696226d64374bce9c18a1dda
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10164.392, -11818.618, 1.993, 0, 0, 0, '{"col orientation":0,"col pos y":-11818.618,"scale y":0.833,"scale z":0.833,"col size y":1.078,"col size z":1.192,"col pos x":10164.392,"col size x":1.173,"scale x":0.833}')

-- 705619b12544ec5f33c194ab95e63ced7e0296ac
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10162.501, -11817.823, 2.42, 0, 0, 0.78, '{"col orientation":0,"col pos y":-11817.823,"scale y":0.833,"scale z":0.833,"col size y":1.078,"col size z":1.192,"col pos x":10162.501,"col size x":1.173,"scale x":0.833}')

-- 8f36707bfbe1106cb5d31a8740fbfde19e595e06
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10161.42, -11818.663, 2.823, 0, 0.33, 0, '{"col orientation":-0.38,"col pos y":-11818.663,"scale y":0.833,"scale z":0.833,"col size y":1.078,"col size z":1.192,"col pos x":10161.42,"col size x":1.173,"scale x":0.833}')

-- d235feaddad82fb4872b467bb897ae739eb4f7aa
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10160.882, -11815.114, 2.926, 0, -0.28, -0.61, '{"col orientation":0,"col pos y":-11815.114,"scale y":0.833,"scale z":0.833,"col size y":1.078,"col size z":1.192,"col pos x":10160.882,"col size x":1.173,"scale x":0.833}')

-- 204ffde6b4dbca19c4e1077db88a390fff000bc2
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10165.793, -11816.769, 1.458, 0, 0, 0, '{"col orientation":0,"col pos y":-11816.769,"scale y":1,"scale z":1,"col size y":1.299,"col size z":2,"col pos x":10165.793,"col size x":2.532,"scale x":1}')

-- 86e14078ba0a8a30d451cacef3ff5a79df4d9604
SceneEditor:spawnShape("ge_mission_charette.shape", 10162.359, -11813.879, 2.489, 0, 0, -5.42, '{"col orientation":-2.41,"col pos y":-11813.879,"scale y":0.769,"scale z":0.769,"col size y":5.184,"col size z":2.592,"col pos x":10162.359,"col size x":2.505,"scale x":0.769}')

-- 72574b557a9eef2e5dd9c88722489e8399f7050b
SceneEditor:spawnShape("fo_s2_big_tree.shape", 10124.872, -11771.764, -2.143, 0, 0, 0, '{"col orientation":-0.27,"col pos y":-11773.544,"scale y":1,"scale z":1,"col size y":3.433,"col size z":37.853,"col pos x":10125.657,"col size x":4.822,"scale x":1}')

-- 6d1d1e703bcb38cab8896fbd657f9c7e00efe410
SceneEditor:spawnShape("ge_mission_gate.shape", 10120.407, -11786.178, 1.839, 0, 0, -5.01, '{"col orientation":-1.75,"col pos y":-11792.264,"scale y":1,"scale z":1,"col size y":7.386,"col size z":19.674,"col pos x":10118.793,"col size x":5.382,"scale x":1}')

-- 435be0405f8aae77d1c0296a37d1ac852b461141
SceneEditor:spawnShape("fo_s3_buissonaepine.shape", 10162.074, -11839.593, 8.077, 0, 0, 0, '{"col orientation":0,"col pos y":-11839.91,"scale y":1,"scale z":1,"col size y":3.179,"col size z":2.906,"col pos x":10161.424,"col size x":3.785,"scale x":1}')

-- c46cb353ce9d64b85985721d08932be5f19a9cef
SceneEditor:spawnShape("fo_s3_champignou_b.shape", 10160.517, -11837.593, 8.216, 0, 0, 0, '{"col orientation":-0.06,"col pos y":-11837.707,"scale y":1,"scale z":1,"col size y":2.452,"col size z":3.305,"col pos x":10160.942,"col size x":2.567,"scale x":1}')

-- 88f4fc40469389a033f6317b2787d63af88deb02
SceneEditor:spawnShape("ge_mission_barriere.shape", 10169.249, -11812.59, 0.682, 0, 0, 1.49, '{"col orientation":1.49,"col pos y":-11812.59,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10169.249,"col size x":3.424,"scale x":1}')

-- 46d8d2e386deeb46176ae9b99565780a2aca5bca
SceneEditor:spawnShape("ge_mission_barriere.shape", 10169.047, -11809.236, 0.819, 0, -0.04, 1.76, '{"col orientation":1.8,"col pos y":-11809.236,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10169.047,"col size x":3.424,"scale x":1}')

-- 8ec3c09b99197114c74de3ce20ea6b3ad19eaced
SceneEditor:spawnShape("ge_mission_barriere.shape", 10168.761, -11816.038, 0.783, 0, 0.08, 1.37, '{"col orientation":1.37,"col pos y":-11816.038,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10168.761,"col size x":3.424,"scale x":1}')

-- aa78cc0a5bd70f70e62a9f797524592b75ef7c80
SceneEditor:spawnShape("ge_mission_barriere.shape", 10168.135, -11805.938, 1.081, 0, 0.13, -1.23, '{"col orientation":-1.26,"col pos y":-11805.938,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10168.135,"col size x":3.424,"scale x":1}')

-- e03db5194e1d08f1ec576b204f2c86118312438b
SceneEditor:spawnShape("ge_mission_barriere.shape", 10167.216, -11802.663, 1.535, 0, 0.17, -1.35, '{"col orientation":11.22,"col pos y":-11802.663,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10167.216,"col size x":3.424,"scale x":1}')

-- ddce9f282dc147a700d09d180bb4d473da6b5aaa
SceneEditor:spawnShape("ge_mission_barriere.shape", 10166.624, -11799.665, 1.679, 0, -0.05, -1.4, '{"col orientation":1.75,"col pos y":-11799.665,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10166.624,"col size x":3.424,"scale x":1}')

-- a3ef021fc4faf55c4548a61d827aae4104d83c25
SceneEditor:spawnShape("ge_mission_barriere.shape", 10166.163, -11796.326, 1.493, 0, 0.05, 1.68, '{"col orientation":-1.49,"col pos y":-11796.326,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10166.163,"col size x":3.424,"scale x":1}')

-- 52b52dc127289374cdfd9b679cb220aee96a5b3d
SceneEditor:spawnShape("ge_mission_barriere.shape", 10165.734, -11792.951, 1.414, 0, 0, -1.43, '{"col orientation":-1.44,"col pos y":-11792.951,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10165.734,"col size x":3.424,"scale x":1}')

-- 3c87525759c2d41bbabd84f5d992b168d550aec8
SceneEditor:spawnShape("ge_mission_barriere.shape", 10165.222, -11789.631, 1.342, 0, 0, -1.41, '{"col orientation":1.75,"col pos y":-11789.631,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10165.222,"col size x":3.424,"scale x":1}')

-- 74989ee865817231da12bbd53ec3a5a2588d88e7
SceneEditor:spawnShape("ge_mission_barriere.shape", 10164.506, -11786.354, 1.338, 0, 0, 1.85, '{"col orientation":1.87,"col pos y":-11786.354,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10164.506,"col size x":3.424,"scale x":1}')

-- 621c1d01fe5d47903205f34a6a33ef8851797686
SceneEditor:spawnShape("ge_mission_barriere.shape", 10160.267, -11783.396, 1.793, 0, 0.14, -0.08, '{"col orientation":-0.09,"col pos y":-11783.396,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10160.267,"col size x":4.944,"scale x":1}')

-- 2506a30bc07e237b0a1e6f74b2f4ecfaf6edacba
SceneEditor:spawnShape("ge_mission_barriere.shape", 10156.607, -11783.25, 2.355, 0, 0.21, 0, '{"col orientation":0,"col pos y":-11783.25,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10156.607,"col size x":3.424,"scale x":1}')

-- f592af4cbdc727de366b8525dd13b86c9a40c00b
SceneEditor:spawnShape("ge_mission_barriere.shape", 10153.01, -11783.2, 2.812, 0, 0.11, -0.02, '{"col orientation":0,"col pos y":-11783.2,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10153.01,"col size x":3.424,"scale x":1}')

-- 1d7fc9f689b669f110034c2978f5c84579f6cd8a
SceneEditor:spawnShape("ge_mission_barriere.shape", 10149.24, -11782.93, 3.287, 0, 0.13, -0.12, '{"col orientation":-6.42,"col pos y":-11782.93,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10149.24,"col size x":3.424,"scale x":1}')

-- 8f066c7ea3d618cb74932e40f59400ef3dba6c59
SceneEditor:spawnShape("ge_mission_barriere.shape", 10145.559, -11782.459, 3.541, 0, 0, -0.14, '{"col orientation":-0.13,"col pos y":-11782.459,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10145.559,"col size x":3.424,"scale x":1}')

-- 3a6421fd32df1a6192b325ffcb4c9920c0e31d57
SceneEditor:spawnShape("fo_s2_bigroot_a.shape", 10151.739, -11827.77, 7.468, 0, 0, -10.02, '{"col orientation":-0.94,"col pos y":-11827.77,"scale y":1,"scale z":1,"col size y":2.039,"col size z":8.57,"col pos x":10151.739,"col size x":0.856,"scale x":1}')

-- a51a8c8dbefc5521ce44eab9151349a6266943a5
SceneEditor:spawnShape("fo_s1_giant_tree.shape", 10139.879, -11806.54, 7.202, 0, 0, 0, '{"col orientation":115.861,"col pos y":-11806.54,"scale y":1,"scale z":1,"col size y":5.332,"col size z":66.446,"col pos x":10139.879,"col size x":4.891,"scale x":1}')

-- 355e916e190d4ac6df7e382191156e18ebc0e25b
SceneEditor:spawnShape("fo_s2_big_tree.shape", 10127.655, -11799.933, 3.793, 0, 0, 0, '{"col orientation":1.25,"col pos y":-11799.933,"scale y":1,"scale z":1,"col size y":2.903,"col size z":37.853,"col pos x":10127.655,"col size x":4.292,"scale x":1}')

-- c133adb85b8ab55412acf273ec7f5d5514fc5ecd
SceneEditor:spawnShape("fo_s3_fougere.shape", 10125.245, -11795.672, 2.793, 0, 0, 0, '{"col orientation":-1.84,"col pos y":-11779.694,"scale y":0.635,"scale z":0.529,"col size y":6.748,"col size z":3.538,"col pos x":10122.745,"col size x":6.431,"scale x":0.635}')

-- e20e17ba5b112078e03a094ea49479281826cc72
SceneEditor:spawnShape("ge_mission_stand.shape", 10135.696, -11790.228, 3.682, 0, 0, 1.31, '{"col orientation":1.29,"col pos y":-11790.228,"scale y":0.938,"scale z":0.644,"col size y":4.775,"col size z":6.313,"col pos x":10135.696,"col size x":4.258,"scale x":1.088}')

-- 5f6eb56d0cb722c7d665d09b95799449fea9a0c5
SceneEditor:spawnShape("ge_mission_barriere.shape", 10166.29, -11826.844, 3.231, 0, 0, 1.27, '{"col orientation":-1.86,"col pos y":-11826.844,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10166.29,"col size x":3.424,"scale x":1}')

-- 1ba783ef4440ed01f1dfa5d1ec9335998e7cc444
SceneEditor:spawnShape("ge_mission_barriere.shape", 10164.974, -11830.24, 3.731, 0, 0.33, 1.14, '{"col orientation":-1.99,"col pos y":-11830.24,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10164.974,"col size x":3.424,"scale x":1}')

-- 9de37c4c308b455e5f2a35f18d2b5f9b2cee00b7
SceneEditor:spawnShape("ge_mission_barriere.shape", 10163.026, -11832.765, 5.028, 0, 0.51, 0.77, '{"col orientation":-5.53,"col pos y":-11832.765,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10163.026,"col size x":3.424,"scale x":1}')

-- f973f1c0ac69757afa37691fa3b3aafdacfbc754
SceneEditor:spawnShape("ge_mission_barriere.shape", 10161.026, -11834.906, 6.322, 0, 0.38, 0.84, '{"col orientation":-5.41,"col pos y":-11834.906,"scale y":1,"scale z":1,"col size y":0.188,"col size z":2.303,"col pos x":10161.026,"col size x":3.424,"scale x":1}')


-- GROUP: clickable_items
-- ae73d0f019dbc50fc0bef3b52d37ac71ee27fba3
-- SceneEditor:spawnShape("ge_treasure_box_ma.shape", 10154.014, -11786.984, 2.828, 0, 0, 0, '{"col pos x":10154.014,"col orientation":254,"col size y":1,"col size x":1,"scale y":1,"col size z":1,"scale z":1,"col pos y":-11786.984,"scale x":1}')

-- f48b9c73e0adfa8c5d850c4caa91665b6fa33d84
-- SceneEditor:spawnShape("ge_treasure_box_gen.shape", 10151.004, -11785.948, 3.144, 0, 0, 0, '{"col pos x":10151.004,"col orientation":0,"col size y":1,"col size x":1,"scale y":1,"col size z":1,"scale z":1,"col pos y":-11785.948,"scale x":1}')

-- f1d3efb8ccc1b50c884b66f598cdad73348559b7
-- SceneEditor:spawnShape("ge_treasure_box_tr.shape", 10148.002, -11785.012, 3.418, 0, 0, 0, '{"col pos x":10148.002,"col orientation":-8.689448929584826e+231,"col size y":1,"col size x":1,"scale y":1,"col size z":1,"scale z":1,"col pos y":-11785.012,"scale x":1}')

-- f0f845289e5c0b77d9caf0f91278c5fb5423efcd
-- SceneEditor:spawnShape("ge_treasure_box_zo.shape", 10144.996, -11784.01, 3.6, 0, 0, 0, '{"col pos x":10144.996,"col orientation":0,"col size y":1,"col size x":1,"scale y":1,"col size z":1,"scale z":1,"col pos y":-11784.01,"scale x":1}')

-- c59551c60feb88fef458b79961e306fbbc4934b6
-- SceneEditor:spawnShape("ge_treasure_box_fy.shape", 10157.024, -11788.034, 2.443, 0, 0, 0, '{"col pos x":10157.024,"col orientation":4.287421892732054e+246,"col size y":1,"col size x":1,"scale y":1,"col size z":1,"scale z":1,"col pos y":-11788.034,"scale x":1}')


-- GROUP: zones
-- 839b646ff9f90836b707bddee766b35509e0a58b
SceneEditor:spawnShape("ge_picture_yubo.shape", 10148.932, -11809.455, 9.222, 0, 0, 3.06, '{"col pos x":10148.932,"col orientation":0,"col size y":0.026,"col size x":0.711,"scale y":1,"col size z":0.513,"scale z":1,"col pos y":-11809.455,"scale x":1}')

-- 8b98a3872ce832d3e1452d2b06eeb65eceefec04
SceneEditor:spawnShape("ge_picture_bodoc.shape", 10147.375, -11809.58, 9.146, 0, 0, -2.77, '{"col pos x":10147.375,"col orientation":0,"col size y":0.026,"col size x":0.711,"scale y":1,"col size z":0.513,"scale z":1,"col pos y":-11809.58,"scale x":1}')

-- 9df05b145112c17ff867abe49179bd5d25375a44
SceneEditor:spawnShape("ge_mission_jarre.shape", 10147.767, -11809.722, 7.804, 0, -0.64, 0, '{"col pos x":10147.767,"col orientation":0,"col size y":0.598,"col size x":0.598,"scale y":0.833,"col size z":1.151,"scale z":0.833,"col pos y":-11809.722,"scale x":0.833}')

-- 16da218f31f82659233866524ddc9a182a98887e
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10144.639, -11811.631, 7.533, -0.18, -0.5, -2.98, '{"col pos x":10144.639,"col orientation":0,"col size y":1.078,"col size x":1.173,"scale y":0.833,"col size z":1.192,"scale z":0.833,"col pos y":-11811.631,"scale x":0.833}')

-- 774e4ea336076481a244c766b6e646fdec0c0f11
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10144.571, -11810.88, 7.537, 0, 0, -1.75, '{"col pos x":10144.571,"col orientation":0,"col size y":1.078,"col size x":1.173,"scale y":0.833,"col size z":1.192,"scale z":0.833,"col pos y":-11810.88,"scale x":0.833}')

-- 37a1e9340202b20d609335a61a700dd7ce1ebcf1
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10145.777, -11812.638, 7.322, -0.35, -0.12, 0, '{"col pos x":10145.777,"col orientation":0,"col size y":1.078,"col size x":1.173,"scale y":0.833,"col size z":1.192,"scale z":0.833,"col pos y":-11812.638,"scale x":0.833}')

-- 72783b52bc33429523dbf75041131e469f1a9434
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10145.621, -11811.725, 7.36, 0, 0, -1.6, '{"col pos x":10145.621,"col orientation":0,"col size y":1.078,"col size x":1.173,"scale y":0.833,"col size z":1.192,"scale z":0.833,"col pos y":-11811.725,"scale x":0.833}')

-- a590c4257adc108811d30aee655ce6eb57801de3
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10148.222, -11810.032, 8.674, 0, 0, 0, '{"col pos x":10148.222,"col orientation":0,"col size y":1.299,"col size x":2.532,"scale y":1,"col size z":2,"scale z":1,"col pos y":-11810.032,"scale x":1}')

-- 9c5de9850e0fcca072c935b3f93712611364b88e
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10146.007, -11809.992, 7.262, 0, 0.22, 0, '{"col pos x":10146.007,"col orientation":0,"col size y":1.299,"col size x":2.532,"scale y":1,"col size z":2,"scale z":1,"col pos y":-11809.992,"scale x":1}')

-- 0e21a4bdd06690ac517afba162b93152597f1522
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10148.59, -11809.649, 6.671, 0, 0, 0, '{"col pos x":10148.25,"col orientation":0,"col size y":1.299,"col size x":2.532,"scale y":1,"col size z":2,"scale z":1,"col pos y":-11809.795,"scale x":1}')

-- 9c32484902505c4697533fcd777955c997dc9748
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10151.145, -11809.223, 6.067, 0, 0.14, 0.18, '{"col pos x":10151.388,"col orientation":0,"col size y":1.103,"col size x":2.145,"scale y":1,"col size z":2.481,"scale z":1,"col pos y":-11809.157,"scale x":1}')

-- 9deb6ab152ae902bb3e9ae74b53d031612c76e63
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10153.562, -11810.2, 5.369, 0, 0, -0.62, '{"col pos x":10153.562,"col orientation":-0.73,"col size y":1.299,"col size x":2.532,"scale y":1,"col size z":2,"scale z":1,"col pos y":-11810.2,"scale x":1}')

-- a8f16f3b8b7bf1677c0ab1e0939db60fcbc7b75f
SceneEditor:spawnShape("ge_mission_barriere.shape", 10152.046, -11810.597, 5.794, 0, 0.31, -0.29, '{"col pos x":10152.046,"col orientation":-0.27,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"scale z":1,"col pos y":-11810.597,"scale x":1}')

-- 917aba88dad1a131fe225615b87a5170d19cc48a
SceneEditor:spawnShape("ge_mission_barriere.shape", 10149.306, -11811.162, 6.574, -0.05, 0.21, 0.54, '{"col pos x":10148.966,"col orientation":0.67,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"scale z":1,"col pos y":-11811.308,"scale x":1}')

-- 7cae531af4dd2a6e96bd58449482bed0fb8bebb3
SceneEditor:spawnShape("ge_mission_barriere.shape", 10146.832, -11812.926, 7.228, 0, 0.23, 0.7, '{"col pos x":10147.075,"col orientation":0.64,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"scale z":1,"col pos y":-11812.86,"scale x":1}')

-- 4cdad5c1762bc57cecd4758bf2740e7f155bca24
SceneEditor:spawnShape("ge_mission_barriere.shape", 10144.948, -11814.67, 7.495, 0, 0, 0.8, '{"col pos x":10144.948,"col orientation":0.78,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"scale z":1,"col pos y":-11814.67,"scale x":1}')

-- 3a1308ef3c4d4fc5447d790bec0f03c3b2d51269
SceneEditor:spawnShape("ge_mission_barriere.shape", 10142.752, -11816.773, 7.488, 0, 0, 0.73, '{"col pos x":10142.752,"col orientation":0.74,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"scale z":1,"col pos y":-11816.773,"scale x":1}')

