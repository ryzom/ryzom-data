#!/bin/bash

if [[ "$OSTYPE" == "msys" ]]
then
	winpty /r/external/python27/python $*
else
	python $*
fi
