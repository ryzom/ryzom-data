import os, sys, json
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from .config_reader import ConfigReader

config = ConfigReader()
json_privdata_path = config.get("paths", "json-priv-data", True)

def toList(e):
	e = e.split(" ")
	return (int(e[0]), int(e[1]))

class RyzomWorld:
	
	def __init__(self):
		with open(json_privdata_path+"/world.json", "r") as outfile:
			self.world = json.load(outfile)
			
	def point_in_polygon(self, x, y, polygon):
		point = Point(x, y)
		poly = Polygon(polygon)
		return poly.contains(point)

	def get_infos(self, x, y):
		continent = ""
		region = ""
		places = []
		for c, ci in self.world.items():
			if self.point_in_polygon(x, y, ci[1]):
				continent = c
			if ci[2]:
				for r, ri in ci[2].items():
					if r[:7] == "region_" and self.point_in_polygon(x, y, ri[1]):
						region = r
					if ri[2]:
						for p, pi in ri[2].items():
							if self.point_in_polygon(x, y, pi[1]):
								places.append(p)
		return (continent, region, places)
