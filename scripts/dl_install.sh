#!/bin/bash

echo "$2"
if [[ ! -z "$2" ]]
then
	rm -rf "$2"
	mkdir "$2"
	cd "$2"
fi

while read -d $'\r' line
do
	value="${line##* }"
	echo "${value%,*}"
	echo "# Downloading $(basename $1)..."
done < <(curl --progress-bar --anyauth --create-dirs -O "$1" 2>&1) | zenity --progress --auto-close

ext=$(basename "$1" | cut -d. -f2)

if [[ "$ext" == "zip" ]]
then
	(unzip -o $(basename $1); echo "100") |  zenity --progress --text="Extracting $(basename $1)..." --pulsate --auto-close
elif [[ "$ext" == "7z" ]]
then
	(
		7z -bsp1 -bso2 -y x $(basename $1) |
		while read -d $'\r\n' line
		do
			echo "$line"
		done
		echo "100"
	) | zenity --progress --text="Extracting $(basename $1)..." --auto-close

fi
rm $(basename $1)
